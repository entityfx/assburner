#!/usr/bin/python

import os,sys,shutil
sys.path.insert(0,os.path.join(os.getcwd(),"python/"))
path = os.path.dirname(os.path.abspath(__file__))

import cpp
import python
from blur.build import *
import blur.build

# Setup config file replacements, could read from multiple files or a more
# dynamic system if needed(database)
blur.build.Config_Replacement_File = os.path.join(path,'config_replacements.ini')

All_Targets.append( RPMTarget('blurperlrpm','blur-perl',os.path.join(path,'perl'),'../rpm/spec/blur-perl.spec.template','1.0') )
All_Targets.append( RPMTarget("blurreporpm","blur-repo",os.path.join(path,'rpm/repos'),'../../rpm/spec/blur-repo.spec.template','1.0') )

All_Targets.append( Target('allrpms',os.path.abspath(os.getcwd()),["blurpythonbuildrpm"], [
		"qjsonrpm","qgvrpm","stonerpm","stoneguirpm","classesrpm","classesuirpm","libassfreezerrpm","libabsubmitrpm",
		"pystonerpm","pystoneguirpm", "pyclassesrpm","pyclassesuirpm",
#		"blurdevrpm","traxrpm",
		"blurpythonrpm","assburnerdaemonsrpm",
#		"joberrorhandlerrpm","rrdstatscollectorrpm", #"perlqtrpm","blurperlrpm",
		"classmakerrpm",  "assfreezerrpm", 
#'abscriptsrpms',
		"assburnerrpm","absubmitrpm"]) )

class WinDistTarget(Target):
	def build_run(self):
		import sysconfig
		rev = GetRevision('.')
		dist_root = '../blur_builds/blur_dist_%s' % rev.replace(':','_')
		debug_dir = os.path.join(os.path.join(dist_root,'debug'),self.platform() + '_python' + sysconfig.get_python_version())
		if not os.path.exists(debug_dir):
			os.makedirs(debug_dir)
		for name, file in Generated_Installers.iteritems():
			print name, file
			if name.endswith('_installer'):
				name = name[:-len('_installer')]
			python = None
			compiler = None
			bits = None
			m = re.search(r'((?:python\d\.\d)?)_win32-([^_]+)((?:_64)?).exe$',file)
			if m:
				python = m.group(1)
				compiler = m.group(2)
				bits = m.group(3).lstrip('_')
				if not bits:
					bits = '32'
			if python:
				name = '%s_%s' % (name,python)
			dir = '%s_%s_%s' % (name, compiler, bits)
			dir = os.path.join(dist_root,dir)
			if not os.path.exists(dir):
				os.mkdir(dir)
			shutil.copy(file,dir)
		for debug_file in Generated_DebugFiles:
			shutil.copy(debug_file,debug_dir)
			
	def selected(self):
		for arg in ('full',None):
			if arg not in blur.build.Args and arg != None:
				blur.build.Args.append(arg)

winDistDeps = ['blur_python','absubmit','assburner','assfreezer']

All_Targets.append( WinDistTarget('win_dist',path,winDistDeps) )

if __name__ == "__main__":
	build()

