
import sys, os
import subprocess
import cPickle

Python_Version = 'Python2.4'
BitDepth = 32

source_dirs = {
	32 : 'c:\\source\\blur\\',
	64 : 'c:\\source\\blur_64\\' }
	
qt_dirs = {
	32 : 'c:\\Qt\\4.6.1\\',
	64 : 'c:\\Qt\\4.6.1_64\\' }

python_paths = {
	'Python2.4' : 'c:/python24/',
	'Python2.5' : 'c:/python25/',
	'Python2.5_64' : 'c:/python25_64/',
	'Python2.6' : 'c:/python26/',
	'Python2.6_64' : 'c:/python26_64/' }

software_map = {
	'qt_installer': 'Qt ${BITDEPTH}',
	'pyqt_installer':'PyQt4 ${PYTHON} ${BITDEPTH}',
	'pyqt_modules_installer' : 'Blur Python ${PYTHON} ${BITDEPTH}',
	'assburner_installer' : 'Assburner',
	'assfreezer_installer' : 'Assfreezer',
	'absubmit_installer' : 'Absubmit',
	'classmaker_installer' : 'Classmaker' }

bitdepth_replace = {
	32 : 'x86',
	64 : 'x86_64' }

def platform_path(path):
	if sys.platform == 'win32':
		return path
	return path.replace('c:','/mnt/morlock006/').replace('\\','/')

def software_name_from_target(target):
	swName = software_map[target]
	swName = swName.replace( '${BITDEPTH}', bitdepth_replace[BitDepth] )
	swName = swName.replace( '${PYTHON}', Python_Version )
	return swName

def generate_batch(file,build_args):
	vcvars_arg = { 32 : 'x86', 64 : 'amd64' }
	f = open( file, 'w' )
	f.write( 'call "C:\\Program Files (x86)\\Microsoft Visual Studio 9.0\\VC\\vcvarsall.bat" %s\n' % vcvars_arg[BitDepth] )
	f.write( 'set QTDIR=%s\n' % qt_dirs[BitDepth] )
	f.write( 'set PYTHONPATH=%s\n' % python_paths[Python_Version].replace('/','\\') )
	f.write( 'set PATH=%QTDIR%\\bin;%PYTHONPATH%;%PATH%\n' )
	f.write( 'python.exe build.py %s\n' % build_args )
	f.close()

def run_build(build_args):
	file = 'build_run.bat'
	generate_batch(file,build_args)
	process = subprocess.Popen(args = ['cmd','/Q','/C',file])
	process.wait()
	
def upload_installers():
	f = open( 'new_installers.pickle', 'rb' )
	installers = cPickle.load(f)
	f.close()
	for target,installerPath in installers.iteritems():
		softwareName = software_name_from_target(target)
		installerPath = platform_path(installerPath)
		print 'Found Installer for %s: %s' % (softwareName,installerPath)
		process = subprocess.Popen(args = ['python','upload.py',softwareName,installerPath])
		process.wait()

if __name__=='__main__':
	command = sys.argv[1].lower()
	BitDepth = int(sys.argv[2])
	os.chdir( platform_path(source_dirs[BitDepth]) )
	if command == 'build':
		Python_Version = sys.argv[3]
		build_args = ' '.join(sys.argv[4:])
		print BitDepth,Python_Version,build_args
		run_build(build_args)
	elif command == 'upload':
		upload_installers()
	