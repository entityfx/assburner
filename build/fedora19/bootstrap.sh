#!/usr/bin/env bash

yum install -y vim gcc-c++.x86_64 sip.x86_64 sip-devel.x86_64 PyQt4-devel.x86_64 qt-devel.x86_64 qt-postgresql.x86_64 subversion libXScrnSaver-devel.x86_64 graphviz-devel.x86_64 qtwebkit-devel.x86_64 rpm-build rpmdevtools fedora-packager

