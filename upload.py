
import sys, subprocess
from blur.quickinit import *

def upload(softwareName,installerPath):
	software = Software.recordByName(softwareName)
	if not software.isRecord():
		raise Exception("Unable to find software record with name: " + softwareName);
	uploadPath = software.installerPath().replace('\\\\snake.blur.com\\','snake:/mnt/snake-library/production/').replace('\\','/')
	print "Uploading software %s installer %s to %s" % (softwareName,installerPath,uploadPath)
	uploadProcess = subprocess.Popen(args = ['scp',installerPath,uploadPath])
	uploadProcess.wait()

if __name__=="__main__":
	upload(sys.argv[1],sys.argv[2])