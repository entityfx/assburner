
from PyQt4.QtCore import *
from blur.Stone import *
import sys, os

app = QCoreApplication.instance()
args = []
if app is None:
	try:
		args = sys.argv
	except:
		pass
	app = QCoreApplication(args)

if sys.platform == 'win32':
	configpath = 'c:/blur/assburner/assburner.ini'
else:
	configpath = '/etc/db.ini'

if not os.path.exists( configpath ):
	raise IOError( 'Config path does not exist: %s' % configpath )
	
initConfig(configpath)

initStone(args)

from blur.Classes import *

# Double check that the connection was created.  If blur.Classes was imported
# before initStone was called, then only the schema was created, not the
# default Database and Connection
createConnection()
