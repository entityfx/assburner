import os, sys, subprocess, time
from optparse import OptionParser
from blur.quickinit import *

add_hosts_path = os.path.join( os.path.dirname( __file__ ), 'add_hosts.py' )
bladeNumbers = [ 1, 11, 2, 12, 3, 13, 4, 14, 5, 15, 6, 16, 7, 17, 8, 18, 9, 19, 10, 20 ]

def buildOptionsParser():
	parser = OptionParser(usage="usage: %prog [options]")
	parser.add_option( '-a', '--chassis-ip', 		dest="chassisIP", 		default=None, help="IP address of the blade chassis" )
	parser.add_option( "-n", "--host-name", 		dest="hostName", 		default=None, help="Base name of the host records to be created, the host number will be appended" )
	parser.add_option( "-i", "--ip-base", 			dest="ipBase", 			default=None, help="If specified, hostinterface records will be created.  The last number will match the number of the host if the --start-number is included and should be in the format 192.168.0., otherwise should be the full ip address.  If you specify 'copy', it will copy the base from the host specified with the -e option." )
	parser.add_option( "-e", "--copy-from-host", 	dest="copyFromHost", 	default=None, help="Copies the services and optionally ip-base from the the specified host" )
	parser.add_option( "-b", "--start-number", 		dest="startNumber", 	default=None, 	help="The first number to create, if this is excluded a single host will be created" )
	parser.add_option( "-c", "--count", 			dest="count", 			default=1, 	help="The number of host records to create" )
	parser.add_option( "-d", "--dry-run", 			dest="dryRun", 			action="store_true", default=None, help="Print the host names but dont actually commit" )
	parser.add_option( "-l", "--location", 			dest="location", 		default=None, help="Set Location of hosts, must exist in location table." )
	parser.add_option( "-p", "--power-on", 			dest="powerOn", 		action="store_true", default=None, help="Powers on the hosts if specified." )
	parser.add_option( "-w", "--consecutive", 		dest="consecutive", 	action="store_true", default=None, help="If power-on is specified, uses consecutive order." )
	return parser

def getMacs( chassisIP ):
	os.chdir( r'C:\Program Files (x86)\SMCIPMITool' )
	macs = []
	for i in range( 1, 21 ):
		command = r'SMCIPMITool.exe %s ADMIN ADMIN blade %s config' % ( chassisIP, bladeNumbers[i-1] )
		process = subprocess.Popen( command.split(), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE )
		stdout, stderr = process.communicate()
		lines = stdout.splitlines()
		for line in lines:
			if 'MAC' in line:
				mac = line.split( '=' )[1].strip()
				print mac
				macs.append( mac )
				break

	return macs

def powerOn( chassisIP, consecutive ):
	os.chdir( r'C:\Program Files (x86)\SMCIPMITool' )
	for i in range( 1, 21 ):
		if consecutive:
			command = r'SMCIPMITool.exe %s ADMIN ADMIN blade %s power up' % ( chassisIP, bladeNumbers[i-1] )
			time.sleep( 60 )
		else:
			command = r'SMCIPMITool.exe %s ADMIN ADMIN blade %s power up' % ( chassisIP, i )
		os.system( command )
		print command

def main():
	parser = buildOptionsParser()
	parser.parse_args()
	opts = parser.values

	print repr( opts.dryRun )

	if opts.chassisIP is None:
		parser.error( 'chassisIP is required' )
	if opts.ipBase is None:
		parser.error( 'ipBase is required' )
	if opts.copyFromHost is None:
		parser.error( 'copyFromHost is required' )
	if opts.startNumber is None:
		parser.error( 'startNumber is required' )
	if opts.count is None:
		parser.error( 'count is required' )
	if opts.hostName is None:
		parser.error( "hostName is required" )
	if opts.location is None:
		parser.error( "location is required" )
	else:
		location = (Location.c.Name == opts.location).select()[0]
		if not location.isRecord():
			parser.error( "Location '%s' not found in database" % opts.location )

	macs = getMacs( opts.chassisIP )

	argDict = { 'add_hosts': add_hosts_path,
				'host_name': opts.hostName,
				'ip_base': opts.ipBase,
				'copy_from_host': opts.copyFromHost,
				'start_number': opts.startNumber,
				'count': opts.count,
				'macs': ','.join( macs ),
				'location': opts.location }

	command = r'C:\python27\python.exe %(add_hosts)s --host-name %(host_name)s --ip-base %(ip_base)s --copy-from-host %(copy_from_host)s --start-number %(start_number)s --count %(count)s --mac-addresses %(macs)s --location %(location)s' % argDict
	if opts.dryRun:
		command += ' --dry-run'

	print command
	ret = os.system( command )
	if ret:
		print 'Adding hosts failed, exiting', ret
		sys.exit( ret )

	if not opts.dryRun:
		powerOn( opts.chassisIP, opts.consecutive )

if __name__ == '__main__':
	main()