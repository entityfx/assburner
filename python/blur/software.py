##
#	\namespace	software.py
#
#	\remarks	[REMARKS]
#	
#	\author		beta@blur.com
#	\author		Blur Studio
#	\date		03/05/10
#

def updateVersion( software, version ):
	from PyQt4.QtCore 	import QDateTime
	from blur.Stone 	import IniConfig
	
	# record installation information to the c:/blur/software.ini file
	config = IniConfig('c:/blur/software.ini')
	config.setSection( software )  													# software installed
	config.writeString( 'version', version ) 										# specific installation
	config.writeString( 'installed', QDateTime.currentDateTime().toString() )		# datetime installed
	config.writeToFile()

def removeVersion( software ):
	from blur.Stone 	import IniConfig

	# remove installation information from the software.ini file
	config = IniConfig('c:/blur/software.ini')
	config.removeSection( software ) # software removed
	config.writeToFile()


if ( __name__ == '__main__' ):
	import sys
	
	if ( sys.argv[1] == 'update_version' ):
		# update the version with the Software and Version keys
		updateVersion( sys.argv[2], sys.argv[3] )

	elif ( sys.argv[1] == 'remove_version' ):
		# remove the version information for software which has been uninstalled
		removeVersion( sys.argv[2] )