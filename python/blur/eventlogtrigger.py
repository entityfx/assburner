
from blur.Stone import Trigger, Record, Field, LOG_5
from blur._Classes import EventLog, EventLogType, EventLogList, User
from PyQt4.QtCore import QDate, QDateTime, QTime, QVariant, QString

class EventLogTrigger(Trigger):
	def __init__(self):
		super(EventLogTrigger, self).__init__(Trigger.PreInsertTrigger|Trigger.PreDeleteTrigger|Trigger.PreUpdateTrigger)
	
	@classmethod
	def createLog(cls, record, logType):
		event = EventLog()
		event.setUser(User.currentUser())
		event.setDatetime(QDateTime.currentDateTime())
		event.setTablename(unicode(record.table().schema().className()))
		# Ensure that the record has a valid key
		if not record.key():
			record.key(True)
		event.setTablekey(record.key())
		eventdata = {}
		event.setEventlogtype(EventLogType.recordByName(logType))
		eventdata = cls.toDictStr(record, logType == 'UPDATE')

		if not eventdata:
			return None
		#LOG_5( "eventdata => %s" % eventdata )
		event.setEventdata(unicode(eventdata))
		return event

	def preTrigger(self, records, kind):
		ret = EventLogList()
		for record in records:
			event = self.createLog( record, kind )
			if event:
				ret.append( event )
		if ret.count():
			LOG_5( "Committing for pre%s" % kind )
			ret.commit()
		else:
			LOG_5( "Nothing to commit for pre%s" % kind )
		return records

	def preDelete(self, records):
		self.preTrigger( records, 'DELETE' )
		return records
	
	def preInsert(self, records):
		self.preTrigger( records, 'CREATE' )
		return records
	
	def preUpdate(self, updated, before):
		self.preTrigger( [updated], 'UPDATE' )
		return updated
	
	@staticmethod
	def toDictStr(record, isChangeset):
		# TODO: properly escape aditional characters
		def qs(s):
			return repr(unicode(s))
		
		def ba(s):
			return "QByteArray.fromBase64(%s)" % qs(s)

		def convertToString(field, record):
			value = field.dbPrepare(record.getValue(field.name()))
			ftype = field.type()
			if (ftype in (Field.Image, Field.ByteArray) and value.type() == QVariant.ByteArray):
				value = QString(value.toByteArray().toBase64())
			else:
				value = value.toString()
			if ftype == Field.String:
				value = qs(value)
			elif ftype == Field.Date:
				value = "QDate(%s)" % ', '.join([unicode(val).lstrip('0') for val in value.split('-')])
			elif ftype == Field.DateTime:
				formated = ''
				if value:
					# TODO: add timezone support?
					date, time = value.split('T')
					args = [unicode(val).lstrip('0') for val in date.split('-')]
					args += [unicode(val).lstrip('0') for val in time.split(':')]
					formated = ', '.join(args)
				value = "QDateTime(%s)" % formated
			elif ftype == Field.Interval:
				if value and value != "00:00:00":
					value = 'Interval.fromString("%s")[0]' % value
				else:
					value ='Interval()'
			elif ftype == Field.Bool:
				if value:
					value = value[0].toUpper() + value[1:]
				else:
					value = 'False'
			elif ftype == Field.ByteArray:
				value = ba(value)
			elif ftype == Field.Color:
				if value == '(0,0,0,255)':
					value = '()'
				value = 'QColor%s' % value
			elif ftype == Field.Image:
				value = "(lambda data,img=QImage(): img.loadFromData(data) and img)(%s)" % ba(value)
			# Field.Time is not exposed to python in sip
			elif ftype == 14:
				time = ''
				if value != '00:00:00':
					time = ', '.join([unicode(val).lstrip('0') for val in value.split(':')])
				value = 'QTime(%s)' % time
			# If the field does not have a value set and is a int or float store None
			elif ftype in (Field.Int, Field.UInt, Field.ULongLong, field.Double, field.Float):
				if value == '':
					value = 'None'
			return value
		
		dict_str = lambda vals: '{%s}' % ','.join([unicode(val) for val in vals])
		dict_pair = lambda k,v: '%s:%s' % (k,v)
		tuple_str = lambda vals: '(%s)' % ','.join([unicode(val) for val in vals])
		
		parts = []
		if isChangeset and record.isRecord():
			# Dump a list of changes to a existing record
			header = 'changeset'
			# Look up changes in record data
			for field in record.table().schema().fields():
				# Test for column literals i.e. values to be interpreted by the database like 'now()'
				if record.isUpdated(field) and not record.columnLiteral( field.name() ):
					parts.append(dict_pair( qs(field.name()), tuple_str([convertToString(field,record.pristine()),convertToString(field,record)]) ))
		else:
			header = 'record_dump'
			# dump the current state of the record
			for field in sorted(record.table().schema().fields(), key=lambda i: i.name()):
				# Again test for fucking column literals
				if not record.columnLiteral( field.name() ):
					parts.append(dict_pair( qs(field.name()), convertToString(field,record) ))


		if not parts:
			return None

		ret = dict_str( [dict_pair( qs( header ), dict_str( parts ) )] )
		#LOG_5( "Result from eventlogtrigger => '%s'" % ret )
		return ret

