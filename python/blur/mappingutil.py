
from PyQt4.QtCore import QString, QRegExp

# --exclude '/temp_AUTODELETED/'
# --exclude '/Blur_Potential_Projects/'
# --exclude '/_virtual_project/'
# --include '*/'
# --exclude '*.[Pp][Ss][Dd]'
# --exclude 'temp/'
# --include '*.[Rr][Pp][Ss]'
# --include '*_MASTER.max'
# --include '*.flw' 
# --include '**/00_Scripting/**'
# --include '**/_MASTER/**'
# --include '**/_Master/**'
# --include '**/maps/**'
# --include '**/Maps/**'
# --include '**/050_maps/**'
# --include '**/Texture/**'
# --include '**/point_cache/read/*pc2'
# --include '**/point_cache/read/*tmc'
# --include '**/point_cache/read/*mdd'
# --include '**/FinalGather/**'
# --include '**/Mesh*bin'
# --include '**/Star_Wars/00_Mocap/Body/_Final/Layout/**'
# --include '**/Star_Wars2/00_Mocap/Body/_Final/Layout/**'
# --include '**/Star_Wars_Intro/00_Mocap/Body/_Final/Layout/**'
# --include '**/max_plugin.ini/*ini'
# --exclude '*'

def isMirrorSyncPath( fullPath ):
	fullPath = QString(fullPath)
	drive = fullPath[0].toLower()
	path = fullPath.mid(2).replace("\\","/").replace("//","/")
	if drive == "g":
		extMatch = QRegExp( "\\.(.*)$" )
		extMatch.exactMatch(path)
		ext = extMatch.cap(1)
		pathL = path.toLower()
		if path.startsWith( "/temp_AUTODELETED/" ) or path.startsWith( "/Blur_Potential_Projects/" ) or path.startsWith( "/_virtual_project/" ):
			return False
		# Match G: or G:/
		if len(path) == 0 or path.endsWith( '/' ):
			return True
		if ext.toLower() == "psd":
			return False
		if ext.toLower() == "rps" or ext.toLower() == "flw":
			return True
		if path.endsWith( "_MASTER.max" ):
			return True
		if pathL.contains( "/_master/" ) or pathL.contains( "/00_Scripting/" ) or pathL.contains( "/maps/" ) or pathL.contains( "/050_maps/" ) or pathL.contains( "/Texture/" ) or pathL.contains( "/FinalGather/" ):
			return True
		if pathL.contains( "/point_cache/read/" ) and (ext == "pc2" or ext == "tmc" or ext == "mdd" ):
			return True
		if QRegExp( "Mesh[^/]*bin$" ).exactMatch( path ):
			return True
		if path.contains( "/max_plugin.ini/" ) and ext == "ini":
			return True

# Star Wars temp
		if path.contains( "/Star_Wars/00_Mocap/Body/_Final/Layout/" ) or path.contains( "/Star_Wars2/00_Mocap/Body/_Final/Layout/" ) or path.contains( "/Star_Wars_Intro/00_Mocap/Body/_Final/Layout/" ):
			return True
# End Star Wars

	return False

