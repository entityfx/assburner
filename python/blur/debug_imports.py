
def debug_imports(ignore_prefixes = []):
	import __builtin__
	import traceback
	real_import = __builtin__.__import__
	imported = []
	def noisyImport(*args,**kwargs):
		name = args[0]
		if not name in imported and not any(map(lambda x: name.startswith(x),ignore_prefixes)):
			print name
			traceback.print_stack()
			imported.append(name)
		return real_import(*args,**kwargs)
	__builtin__.__import__ = noisyImport

