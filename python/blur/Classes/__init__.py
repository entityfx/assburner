
from .._Classes import *

def createPythonTypes():
	import sys, os
	from blur.Stone import createPythonTypesFromSchema
	schemaFile = os.path.join(os.path.dirname(__file__),'ng_schema.xml')
	createPythonTypesFromSchema( schemaFile, sys.modules[__name__], classesSchema() )

initializeSchema()
createConnection()
createPythonTypes()

from blur.Stone import LOG_5, sendWOLPacket
from ..eventlogtrigger import EventLogTrigger
eventLogTrigger = EventLogTrigger()

def installEventLogTrigger():
	classNames = []
	for table in (t for t in classesSchema().tables()):
		if table.parent() is None and table.logEvents() and table != EventLog.schema() and not table.triggers().count(eventLogTrigger):
			classNames.append( str(table.className()) )
			table.addTrigger( eventLogTrigger )

def Host_ipmiClass(self):
	from blur.ipmi import IPMIClass
	return IPMIClass(self.manufacturer(), self.model())

def Host_ipmi(self):
	return self.ipmiClass().ipmi(self.ipmiIP(), self.chassis().ipmiIP(), self.chassisSlot())


def Host_interfaces(self,ifaceType = 'Ethernet'):
	"ifaceType = Ethernet or IPMI"
	return ((HostInterface.c.Host == self) & (HostInterface.c.Type == HostInterfaceType(ifaceType))).select()

def Host_interface(self,ifaceType = 'Ethernet'):
	"ifaceType = Ethernet or IPMI"
	ret = self.interfaces(ifaceType)
	return ret and ret[0] or HostInterface()

def Host_ipmiIP(self):
	return self.interface('IPMI').ip()

# Starts a host via ipmi or wake-on-lan
# Should have no side-effects for hosts that are already on
def Host_start(self):
	import blur.ipmi as ipmi
	hostName = self.name()
	print "Waking up", hostName
	host_ipmi = self.ipmi()
	success = False
	if host_ipmi.isValid() and host_ipmi.hasCap(ipmi.Start):
		try:
			success = host_ipmi.start()
		except: pass
	elif self.wakeOnLan():
		for iface in self.interfaces():
			mac = str(self.interface().mac())
			if mac:
				try:
					sendWOLPacket(mac, '255.255.255.255', 9)
					success = True
				except ValueError, ve:
					print "Host " + hostName + " had invalid mac address: " + mac
	if success:
		self.hostStatus().setSlaveStatus( 'waking' ).commit()

def Host_reboot(self):
	import blur.ipmi as ipmi
	import subprocess
	hostName = self.name()
	print "Rebooting", hostName
	host_ipmi = self.ipmi()
	success = False
	if host_ipmi.isValid() and host_ipmi.hasCap(ipmi.Reboot):
		try:
			success = host_ipmi.reboot()
		except: pass
	else:
		import os
		try:
			subprocess.check_output('ssh -oPasswordAuthentication=no -oConnectTimeout=10 -oStrictHostKeyChecking=no %s "shutdown /f /r" &' % hostName)
			success = True
		except: pass
	if success:
		self.hostStatus().setSlaveStatus( 'reboot' ).commit()

Host.ipmiClass = Host_ipmiClass
Host.ipmi = Host_ipmi
Host.ipmiIP = Host_ipmiIP
Host.start = Host_start
Host.reboot = Host_reboot
Host.interface = Host_interface
Host.interfaces = Host_interfaces

installEventLogTrigger()
