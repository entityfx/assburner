
"""
	A simple framework for being able to easily provide project specific python
	code to customize things without hacking the main codebase for transient functionality.
"""

import types
__all__ = ['customize']

projectPaths = []
projectPaths.append('blur.Projects')
projectPaths.append('blur_projects')

class MethodDescriptor(object):
	def __init__(self,func):
		self.Func = func
	def __get__(self,obj,cls):
		ret = self.Func
		try:
			# Get the project's module for the specific class.
			module = projectModule(unicode(cls._get_project(obj)).lower(),cls.__name__)
			# Get the override method from the override class in the loaded module
			ret = getattr(getattr(module,cls.__name__),ret.__name__)
			if isinstance(ret,types.MethodType):
				ret = ret.im_func
		except: pass
		# Create an instance method since we aren't calling the method directly
		if obj:
			ret = types.MethodType(ret,obj,cls)
		return ret

def customize(func):
	""" Decorator function used to mark a method as being customizable per project.
	"""
	return MethodDescriptor(func)

def projectModule(projectName,clsName):
	assert(projectName and clsName)
	def tryLoad(mod,clsName=None):
		for path in projectPaths:
			import importlib
			try:
				root = path + '.' + mod
				if clsName:
					path = root
					root += '.' + clsName
				return importlib.import_module(root,path)
			except Exception, e: pass
	return tryLoad(projectName,clsName) or tryLoad(projectName)
	
def __main__():
	import blur.Projects
	class Test(object):
		def _get_project(self):
			return '_Test_Project'
		@blur.Projects.customize
		def test(self):
			pass
	t = Test()
	t.test()
	
if __name__ == "__main__":
	__main__()