
import sys, os
from optparse import OptionParser

dryRun = False

mirrorModes = {
	"real_g" : "UPDATE mapping SET fkeyhost=2597 WHERE keymapping=4",
	"mirrors" : "UPDATE mapping SET fkeyhost=NULL WHERE keymapping=4" }

def updateMirrorMode( mirrorMode ):
	if not mirrorMode in mirrorModes:
		print mirrorMode + " is not a valid mirror mode"
		usage()
		return
	cmd = "psql -h lion -p 5432 -c \"%s\" blur brobison " % mirrorModes[mirrorMode]
	print cmd
	if not dryRun:
		os.system( cmd )

def buildOptionsParser():
	parser = OptionParser(usage="usage: %prog [options]")
	parser.add_option( "-d", "--dry-run", 			dest="dryRun", 			action="store_true", default=None, help="Print the sql but dont actually execute." )
	parser.add_option( "-m", "--mirror-mode", 		dest="mirrorMode", 		default=None, help="Set the current mirror mode. Available values include %s" % ",".join(mirrorModes.keys()) )
	return parser

if __name__=="__main__":
	parser = buildOptionsParser()
	parser.parse_args()
	opts = parser.values

	dryRun = opts.dryRun
	
	if opts.mirrorMode is not None:
		updateMirrorMode( opts.mirrorMode )

