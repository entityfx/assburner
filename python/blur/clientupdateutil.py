
from blur.quickinit import *
import sys

def recordOutput( outputFile, task, host, job ):
	output = readFullFile( outputFile )[0]
	
	jta = task.jobTaskAssignment()
	ja = jta.jobAssignment()
	# Insert JobCommandHistory record
	jch = ja.jobCommandHistory()
	jch.setJob(job)
	jch.setStdOut(output)
	jch.setHost(host)
	jch.commit()
	ja.setJobCommandHistory(jch)
	ja.commit()

	return jch

def reportSuccess( host, task, jch ):
	taskAssignment = task.jobTaskAssignment()
	Database.current().exec_("SELECT jobtaskassignments_completed(ARRAY[%i])" % taskAssignment.key())
	assignment = taskAssignment.jobAssignment()
	assignment.setJobAssignmentStatus( JobAssignmentStatus("done") )
	assignment.setColumnLiteral( JobAssignment.c.Ended, "now()" )
	assignment.commit()

def reportError( host, task, returnCode, jch ):
	# Return the task
	task.setStatus( "new" )
	task.setColumnLiteral( "endedts", "now()" )
	task.commit()
	
	# Insert JobError record
	error = JobError()
	error.setHost(host)
	error.setJob(job)
	error.setFrames(str(task.frameNumber()))
	error.setColumnLiteral( "lastOccurrence", "now()" )
	error.setMessage( "Client Update Failed, return code was " + str(returnCode) )
	error.setCount(1)
	error.setJobCommandHistory(jch)
	error.commit()

	errorStatus = JobAssignmentStatus("error")
	taskAssignment = task.jobTaskAssignment()
	taskAssignment.setJobError(error)
	taskAssignment.setJobAssignmentStatus( errorStatus )
	taskAssignment.setColumnLiteral( JobTaskAssignment.c.Ended, "now()" )
	taskAssignment.commit()
	
	assignment = taskAssignment.jobAssignment()
	assignment.setJobError( error )
	assignment.setJobAssignmentStatus( errorStatus )
	assignment.setColumnLiteral( JobAssignment.c.Ended, "now()" )
	assignment.setJobCommandHistory(jch)
	assignment.commit()
	

def usage():
	print 'python -m blur.clientupdateutil {success|error} [args]'
	print 'Command Args:'
	print 'success TASK_KEY OUTPUT_FILE'
	print 'error TASK_KEY OUTPUT_FILE RETURN_CODE'
		
if __name__=="__main__":
	if len(sys.argv) < 4:
		print "Insufficient number of arguments"
		usage()
		sys.exit(1)
	taskKey = int(sys.argv[2])
	task = JobTask(taskKey)
	job = task.job()
	host = Host.currentHost()
	outputFile = sys.argv[3]
	jch = recordOutput( outputFile, task, host, job )
	if sys.argv[1] == "success":
		reportSuccess( host, task, jch )
	elif sys.argv[1] == "error":
		if len(sys.argv) < 5:
			print "Insufficient number of arguments for error mode, 5 expected"
			usage()
			sys.exit(1)
		returnCode = int(sys.argv[4])
		reportError( host, task, returnCode, jch )
