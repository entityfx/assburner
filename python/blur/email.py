
#import email.MIMEText
import smtplib
# Have to load these like this, because this module is
# called email
MIMEText = __import__('email.MIMEText').MIMEText.MIMEText
MIMEMultipart = __import__('email.MIMEMultipart').MIMEMultipart.MIMEMultipart
MIMEBase = __import__('email.MIMEBase').MIMEBase.MIMEBase
MIMEImage = __import__('email.MIMEImage').MIMEImage.MIMEImage
Encoders = __import__('email.Encoders').Encoders
import sys
from blur.Stone import *
from blur.Classes import *
from PyQt4.QtCore import *
import quopri
import time

def formatdate(timeval=None, localtime=False, usegmt=False):
    """Returns a date string as specified by RFC 2822, e.g.:

    Fri, 09 Nov 2001 01:08:47 -0000

    Optional timeval if given is a floating point time value as accepted by
    gmtime() and localtime(), otherwise the current time is used.

    Optional localtime is a flag that when True, interprets timeval, and
    returns a date relative to the local timezone instead of UTC, properly
    taking daylight savings time into account.

    Optional argument usegmt means that the timezone is written out as
    an ascii string, not numeric one (so "GMT" instead of "+0000"). This
    is needed for HTTP, and is only used when localtime==False.
    """
    # Note: we cannot use strftime() because that honors the locale and RFC
    # 2822 requires that day and month names be the English abbreviations.
    if timeval is None:
        timeval = time.time()
    if localtime:
        now = time.localtime(timeval)
        # Calculate timezone offset, based on whether the local zone has
        # daylight savings time, and whether DST is in effect.
        if time.daylight and now[-1]:
            offset = time.altzone
        else:
            offset = time.timezone
        hours, minutes = divmod(abs(offset), 3600)
        # Remember offset is in seconds west of UTC, but the timezone is in
        # minutes east of UTC, so the signs differ.
        if offset > 0:
            sign = '-'
        else:
            sign = '+'
        zone = '%s%02d%02d' % (sign, hours, minutes // 60)
    else:
        now = time.gmtime(timeval)
        # Timezone offset is always -0000
        if usegmt:
            zone = 'GMT'
        else:
            zone = '-0000'
    return '%s, %02d %s %04d %02d:%02d:%02d %s' % (
        ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][now[6]],
        now[2],
        ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][now[1] - 1],
        now[0], now[3], now[4], now[5],
        zone)

# We have to initialize qt and stone before accessing the db
def ensure_qapp():
	if not QCoreApplication.instance():
		# First Create a Qt Application
		app = QCoreApplication(sys.argv)
		# Load database config
		if sys.platform=='win32':
			initConfig("c:\\blur\\resin\\resin.ini")
		else:
			initConfig("/etc/db.ini")

def send( sender, recipients, subject, body, attachments=[], html = False, cc=[], bcc=[] ):
	#Ensure python strings
	recipients = [str(r) for r in recipients]
	cc = [str(r) for r in cc]
	bcc = [str(r) for r in bcc]
	subject = str(subject)
	body = unicode(body)
	sender = str(sender)
	
	if not sender:
		return
	
	# Read from config file first
	cfg = IniConfig(config())
	cfg.pushSection("Email")
	domain = cfg.readString("Domain")
	server = cfg.readString("Server")
	cfg.popSection()
	
	# Check database if not found in config and we have a connection
	if len(domain) == 0 and Database.current().connection().isConnected():
		domain = Config.getString("emailDomain")
	if len(server) == 0 and Database.current().connection().isConnected():
		server = Config.getString("emailServer")
		
	# process any Employee records that may have been passed in.
	for emails in (recipients, cc, bcc):
		for (i,v) in enumerate(emails):
			if not v.count('@'):
				ensure_qapp()
				emp = Employee.recordByUserName( v )
				if emp.isRecord() and not emp.email().isEmpty():
					emails[i] = emp.email()
				if not v.count('@'):
					emails[i] = v + str(domain)
	
	email = MIMEMultipart( _subtype = 'related' )
	email['Subject'] = subject
	email['From'] = sender
	email['To'] = ', '.join(recipients)
	email['Cc'] = ', '.join(cc)
	email['Date'] = formatdate()
	email['Content-type']="Multipart/mixed"
	email.preamble = 'This is a multi-part message in MIME format.'
	email.epilogue = ''
	
	# build the body - allow for inline images
	eattach		= []
	
	# look for inline images
	import re, os.path
	fnames = re.findall( '<img[ \t]*src="(file:///[^"]+)"[^/>]*/?>', body )
	for fname in fnames:
		filename 	= fname.replace( 'file:///', '' )
		if ( os.path.exists( filename ) and not filename in attachments ):
			html 		= True		# force this email to true if it has html content
			body 		= body.replace( fname, 'cid:%s' % os.path.basename( filename ) )
			
			fp = open( str(filename), 'rb' )
			msgImage = MIMEImage(fp.read())
			fp.close()
			
			msgImage.add_header( 'Content-ID', '<%s>' % os.path.basename( filename ) )
			msgImage.add_header( 'Content-Disposition', 'inline; filename="%s"' % os.path.basename(filename) )
			eattach.append(msgImage)
			attachments.append( filename )
	
	#msgAlternative = MIMEMultipart('alternative')
	#email.attach(msgAlternative)
	msgText = None
	if html:
		msgText = MIMEText(body,'html')
		msgText['Content-type'] = "text/html"
	else:
		msgText = MIMEText(body)
	
	#msgAlternative.attach(msgText)
	email.attach(msgText)
	
	for a in attachments:
		fp = open( str(a), 'rb' )
		txt = MIMEBase("application", "octet-stream")
		txt.set_payload(fp.read())
		fp.close()
		
		Encoders.encode_base64(txt)
		txt.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(a))
		eattach.append(txt)
	
	for eat in eattach:
		email.attach(eat)
	
	# combine recipients, cc, and bcc so the email is sent to everyone
	recipients.extend(cc)
	recipients.extend(bcc)
	
	s = smtplib.SMTP()
	mailServer = str(server)
	s.connect(mailServer)
	print email.get_content_maintype()
	print "Sending email to ", recipients
	s.sendmail(sender, recipients, email.as_string())
	s.close()
	return True
