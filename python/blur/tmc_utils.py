
from math import sqrt, fabs
from array import array
import struct
import sys

def writeLong(fh,l):
	fh.write( struct.pack('=l',l) )
	
def readLong(fh):
	return struct.unpack('=l',fh.read(4))[0]
	
def writeFloat(fh,f):
	fh.write( struct.pack('=f',f) )
	
def readFloat(fh):
	return struct.unpack('=f',fh.read(4))[0]

def writeFloatArray(fh,vals):
	a = array('f',vals)
	fh.write(a.tostring())

def readFloatArray(f,cnt):
	a = array('f')
	a.fromstring(f.read(4 * cnt))
	return a.tolist()

class Tmc(object):
	Signature = 'TRANSFORMCACHE\x00'
	def __init__(self):
		self.Version = 1
		self.StartFrame = 0.0
		self.SamplesPerFrame = 1
		self.Samples = 0
		
	def readHeader(self,fh):
		sig = fh.read(15)
		self.Version = readLong(fh)
		self.StartFrame = readFloat(fh)
		self.SamplesPerFrame = readLong(fh)
		self.Samples = readLong(fh)
		print "Signature %s, Version %i, startFrame %f, samplesPerFrame %i, samples %i" % (sig, self.Version, self.StartFrame, self.SamplesPerFrame, self.Samples)

	def writeHeader(self,fh):
		fh.write(self.Signature)
		writeLong(fh,self.Version)
		writeFloat(fh,self.StartFrame)
		writeLong(fh,self.SamplesPerFrame)
		writeLong(fh,self.Samples)
		
	def readValues(self,fh,cnt):
		self.Values = readFloatArray(fh, 12 * cnt)
	
	def writeValues(self,fh):
		writeFloatArray(fh,self.Values)
	
	def read(self,fileName):
		fh = open(fileName, 'r')
		self.readHeader(fh)
		self.readValues(fh,self.Samples)
	
	def write(self,fileName):
		fh = open(fileName,'wb')
		self.writeHeader(fh)
		self.writeValues(fh)
		
	def makePositiveZeros(self):
		for i in range(len(self.Values)):
			# Convert any possible negative zeros to positive zero
			if fabs(self.Values[i]) < 0.000000000001:
				self.Values[i] = 0.0

	def printTransformMatrix(self,i):
		vals = self.Values[i*12:(i+1)*12]
		for i in range(4):
			a, b, c = vals[i*3:i*3+3]
			if i == 3:
				print "%f %f %f" % (a, b, c)
			else:
				mag = sqrt( a*a + b*b + c*c )
				print "%f %f %f    %f" % (a, b, c, mag)
		print

	def printValues(self):
		for i in xrange(self.Samples):
			print 'Frame %f' % (self.StartFrame + i/float(self.SamplesPerFrame))
			self.printTransformMatrix(i)

tmc = Tmc()
tmc.read(sys.argv[1])
tmc.printValues()

if len(sys.argv) > 2:
	tmc.makePositiveZeros()
	tmc.printValues()
	tmc.write(sys.argv[2])

