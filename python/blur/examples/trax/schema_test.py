
from blur.quickinit import *
from blur.DynamicTableTypes import createTableTypes

# blur.Stone provides config(), and userConfig() methods
# for getting at the default config files. 
# Here is how to use the IniConfig class by itself
config = IniConfig()
config.readFromFile('trax.ini')

# Use settings from [Database] section of config file
# to setup the connection
connection = Connection.createFromIni(config,'Database')
# Connects if not already connected, otherwise it would connect
# automatically when first statement is executed
connection.checkConnection()

# lib/pyClasses has blurSchema function, that
# returns the builtin schema, but here we create
# one from scratch
schema = Schema.createFromXmlSchema( '../../../apps/trax/definitions/schemas/default/schema.xml' )
print schema.mergeXmlSchema( "", False )

# A database is defined by it's schema, but holds all the cache,
# and table instances
db = Database(schema)
db.setConnection(connection)

# This sets the database as the default for this thread
Database.setCurrent(db)

# Use the DynamicTableTypes module to create real python
# classes from the table definitions.  Add them to our
# globals dict, could put them in a specific module if needed
createTableTypes(schema.tables(),globals())

print issubclass(Employee,User)

print User

u = User.recordByUserName('magma')

print u

print Employee(u).isRecord()

emps = Employee.select()
print emps.count()

# Uncomment this to dump all records from every table in the schema
if False:
	for tableSchema in schema.tables():
		print 20 * "-" + tableSchema.tableName() + 20 * '-'
		print tableSchema.table().select().dump()
		print 3 * '\n'

# Test using an index
print User.recordByUserName('matt').dump()

# Create an asset
ass = Asset()
ass.setName( 'test' )
ass.setUniqueId( 'abc' )
ass.setComponentId( 'component' )
ass.setComponentType( 'asdf' )
ass.setSuperType('superType')
ass.commit()

# Create a custom property
prop = CustomProperty()
prop.setAsset(ass)
prop.setName('test')
prop.setValue('value')
prop.setType('string')
prop.commit()

# Test reverse accessor
print ass.customProperties()

# Now we can actually use the tables
dep = Department()
dep.setName( 'Design' )

rol = Role()
rol.setDepartment(dep)

# Is valid shows it's got a table behind it
# Is record shows that it's committed to the db
print dep.name(), dep.isValid(), dep.isRecord()

# Show fkey function usage, works even without
# the values being committed first
print rol.department().name()
