
from blur.quickinit import *

Database.current().setEchoMode( Database.EchoSelect | Database.EchoInsert | Database.EchoUpdate | Database.EchoDelete )

def cse(f):
	def run(*args, **kwargs):
		cse = ChangeSetEnabler(args[0].changeSet)
		ret = f(*args,**kwargs)
		del cse
		return ret
	return run

class Example(object):
	def __init__(self):
		self.changeSet = ChangeSet.create()
		csn = ChangeSetNotifier(self.changeSet)
		csn.added.connect(self.added)
		csn.removed.connect(self.removed)
		csn.updated.connect(self.updated)
		self.csn = csn
		self.setup()
		self.doStuff()
		self.doStuff2()
		self.committed.undo()
		self.doStuff()
		self.doStuff2()
		
	def added(self,records):
		print "Added", records
		
	def removed(self,records):
		print "Removed", records
		
	def updated(self,old,up):
		print "Updated:"
		print old.debug()
		print up.debug()

	# Create the record without using a changeset, if it doesn't exist
	def setup(self):
		c = Config.recordByName('test_entry')
		if not c.isRecord():
			c.setConfig('test_entry')
			c.commit()

	# Automatically enable the changeset for this function
	@cse
	def doStuff(self):
		# Modify a record
		c = Config.recordByName('test_entry')
		c.setConfig('test_entry2')
		c.setValue('value')
		c.commit()

		print 'Checking if modified record is found in index with old value, should not be found'
		print Config.recordByName('test_entry').dump()

		print 'Checking if modified record is found in index with new value, should be found'
		print Config.recordByName('test_entry2').dump()

	# Changeset not enabled
	def doStuff2(self):
		print 'Checking if pristine record is found in index with old value, changeset disabled, should be found'
		print Config.recordByName('test_entry').dump()
		# Commit the changeset, we don't have to enable it to commit it
		self.committed = self.changeSet.commit(True)
		print 'Checking if pristine record is found in index with old value, changeset committed and disabled, should not be found'
		print Config.recordByName('test_entry').dump()
		print 'Checking if pristine record is found in index with updated value, changeset committed and disabled, should be found'
		print Config.recordByName('test_entry2').dump()


Example()
