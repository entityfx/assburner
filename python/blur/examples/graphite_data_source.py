from PyQt4.QtGui import *

app = QApplication([])

from blur.quickinit import *
from blur.Stonegui import *

initConfig( '/etc/db.ini' )

class MyGDS(GraphiteDataSource):
	def __init__(self):
		GraphiteDataSource.__init__(self)
		self.getDataResult.connect(self.slotDataResult)
		
	def getProjectHosts(self, projectName):
		self.getData( 'projects.Far_Cry_3.renderfarm.hosts,projects.Doom_4.renderfarm.hosts' , QDateTime.currentDateTime().addDays(-1), QDateTime.currentDateTime() )
	
	def printResult(self, result):
		for t in result.data:
			dt, v = t
			print dt.toString(), v
	
	def slotDataResult(self,result):
		# Print raw data
		self.printResult(result)
		# Print in hourly increments
		result = result.adjustToInterval( Interval.fromString( '1 hour' )[0] )
		self.printResult(result)
		app.exit()
		
gds = MyGDS()
gds.getProjectHosts( '' )
app.exec_()

