
from blur.quickinit import *

def checkMacSlot( mac, slot, chassis ):
	try:
		hi = (HostInterface.c.Mac == mac).select()[0]
	except IndexError:
		#print "No hostinterface found for mac", mac
		return Host()
	host = hi.host()
	if host.chassis() != chassis:
		host.setChassis(chassis).commit()
		print "Host %s is in the wrong chassis %s, should be %s" % (host.name(), host.chassis().name(), chassis.name())
	if host.chassisSlot() != slot:
		print "Host %s has wrong slot %i, should be %i" % (host.name(), host.chassisSlot(), slot)
	return host

for h in Host.c.Name.regexSearch('diner').select():
	hosts = (Host.c.Chassis == h).select()
	for info in h.ipmi().chassisBladeInfo().itervalues():
		try:
			hosts -= checkMacSlot( info['mac1'], info['chassisSlot'], h )
			hosts -= checkMacSlot( info['mac2'], info['chassisSlot'], h )
		except KeyError:
			print h.name(), info
	if hosts:
		print "The following hosts have fkeychassis pointing to %s, but no matching mac address" % h.name()
		for h in hosts:
			print h.name()
