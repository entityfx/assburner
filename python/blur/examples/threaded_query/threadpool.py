
import sys, time
from blur.Stone import *
from blur.Stonegui import *
from blur.Classes import *
from PyQt4.QtCore import Qt, QEvent, QStringList, pyqtSignal, QObject, QRunnable, QThreadPool, QThread
from PyQt4.QtGui import QApplication, QDialog
from PyQt4.uic import loadUi

class SignalProxy(QObject):
	returned = pyqtSignal(object)
	finished = pyqtSignal()

class SignalingRunnable( QRunnable ):
	def __init__(self,function):
		QRunnable.__init__(self)
		self.function = function
		self.proxy = SignalProxy()
		
	def run(self):
		r = self.function()
		self.proxy.returned.emit(r)
		self.proxy.finished.emit()

	@property
	def finished(self):
		return self.proxy.finished

	@property
	def returned(self):
		return self.proxy.returned


class DBRunnable(SignalingRunnable):
	def __init__(self,function,db):
		SignalingRunnable.__init__(self,function)
		self.db = db
	# Customize or override these
	def getConnection(self):
		return Connection.createFromIni( config(), "Database" )
	def checkDb(self):
		if not Database.current():
			conn = self.getConnection()
			self.db.setConnection(conn)
			conn.checkConnection()
			Database.setCurrent(self.db)

	def run(self):
		self.checkDb()
		super(DBRunnable,self).run()

class MainDialog( QDialog ):
	tableComboDefault = 'Choose Table'
	def __init__(self):
		QDialog.__init__(self)
		loadUi("maindialogui.ui",self)
		self.mExecQueryButton.clicked.connect(self.execQuery)
		self.mTableCombo.currentIndexChanged.connect(self.tableComboTextChanged)
		for table in Database.current().schema().tables():
			self.mTableCombo.addItem( table.className() )
	
	def slotResult(self,result):
		(table,results) = result
		model = RecordListModel(self.mResultsView)
		model.setColumns( [field.name() for field in table.schema().fields()] )
		model.setRecordList(results)
		self.mResultsView.setModel(model)
	
	def getResults(self,table,where):
		return (table,table.select(where))

	def tableComboTextChanged(self,text):
		self.mExecQueryButton.setEnabled( text != self.tableComboDefault )
	
	def execQuery(self):
		func = lambda: self.getResults(Database.current().tableByName( self.mTableCombo.currentText() ), self.mWhereEdit.text())
		dbr = DBRunnable(func,blurDb())
		dbr.returned.connect(self.slotResult)
		QThreadPool.globalInstance().start(dbr)
	
def main():
	app = QApplication(sys.argv)

	# Initialize the config object
	if sys.platform == 'win32':
		initConfig('c:/blur/assburner/assburner.ini')
	else:
		initConfig('/etc/db.ini')

	# Initialize libstone
	initStone(sys.argv)
	# Initialize libclasses
	blurqt_loader()

	initStoneGui()

	dialog = MainDialog()
	dialog.show()
	app.exec_()
	
if __name__ == "__main__":
	main()

