
import sys, time
from blur.Stone import *
from blur.Stonegui import *
from blur.Classes import *
from PyQt4.QtCore import Qt, QEvent, QStringList
from PyQt4.QtGui import QApplication, QDialog, QTreeWidgetItem
from PyQt4.uic import loadUi

class CustomQuery( ThreadTask ):
	EventType = QEvent.User
	def __init__(self,reciever,table,where):
		ThreadTask.__init__(self,self.EventType,reciever)
		self.Table = table
		self.Where = where

	def run(self):
		self.Results = self.Table.select(self.Where)
		for result in self.Results:
			time.sleep(0.05)	# simulate a long process
			QApplication.postEvent(self.reciever(), UpdateData(result))

class UpdateData(QEvent):
	EventType = QEvent.User
	def __init__(self, data):
		QEvent.__init__(self, self.EventType)
		self.data = data

class MainDialog( QDialog ):
	tableComboDefault = 'Choose Table'
	def __init__(self):
		QDialog.__init__(self)
		loadUi("updating.ui",self)
		self.mExecQueryButton.clicked.connect(self.execQuery)
		self.mTableCombo.currentIndexChanged.connect(self.tableComboTextChanged)
		for table in Database.current().schema().tables():
			self.mTableCombo.addItem( table.className() )
			
	def tableComboTextChanged(self,text):
		self.mExecQueryButton.setEnabled( text != self.tableComboDefault )
	
	def execQuery(self):
		self.uiTREE.clear()
		FreezerCore.addTask( CustomQuery( self, Database.current().tableByName( self.mTableCombo.currentText() ), self.mWhereEdit.text() ) )
	
	def customEvent(self,event):
		if isinstance(event,UpdateData):
			self.uiTREE.addTopLevelItem(QTreeWidgetItem([event.data.name()]))
		
def main():
	app = QApplication(sys.argv)

	# Initialize the config object
	if sys.platform == 'win32':
		initConfig('c:/blur/assburner/assburner.ini')
	else:
		initConfig('/etc/db.ini')

	# Initialize libstone
	initStone(sys.argv)
	# Initialize libclasses
	blurqt_loader()

	initStoneGui()

	# Share the database across threads, each with their own connection
	# Here we create the duplicate connection for the thread to use
	threadConn = Connection.createFromIni( config(), "Database" )

	# The ConnectionWatcher::connectionLost slot is thread safe and can
	# properly handle the main gui thread and worker thread(s) losing
	# their connection at the same time, without popping up multiple dialogs
	threadConn.connectionLost2.connect( ConnectionWatcher.connectionLost , Qt.DirectConnection )

	# Pass the thread an a pointer to the shared database to the worker thread
	FreezerCore.setDatabaseForThread( blurDb(), threadConn )

	dialog = MainDialog()
	dialog.show()
	app.exec_()
	
if __name__ == "__main__":
	main()

