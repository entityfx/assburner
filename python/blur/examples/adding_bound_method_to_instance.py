
import types

#
# This code demonstrates adding a bound method to a class instance
#


# Normal function
def fun(self):
	print "I am",self
	
class A:
	# Class member function
	def fun(self):
		print "I am",self
	
	# Class static function
	@staticmethod
	def static_fun(self):
		print "I am",self

# Empty Class
class B:
	pass

# Instances
a = A()
b = B()

# Adding a regular function
print fun # <function fun at 0xb636210c>
b.fun = types.MethodType(fun, b, B)
b.fun()

# Adding an unbound method
print A.fun # <unbound method A.fun>
print A.fun.im_func # <function fun at 0xb631317c>
b.fun = types.MethodType(A.fun.im_func, b, B)
b.fun()

# Adding a static class function
print A.static_fun # <function static_fun at 0xb63131b4>
b.fun = types.MethodType(A.static_fun, b, B)
b.fun()

# Adding a bound method
print a.fun # <bound method A.fun of <__main__.A instance at 0xb62d234c>>
print a.fun.im_func # <function fun at 0xb631317c>
b.fun = types.MethodType(a.fun.im_func, b, B)
b.fun()


# Here we can also add unbound members to a Class simply by assigning a function
# Get the function the same way as you would with the first arg to types.MethodType
# in the above examples
B.fun = fun
b = B()
b.fun()

print "Done"
