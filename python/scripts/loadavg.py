#!/usr/bin/env python

from blur.quickinit import *
from blur.daemonize import createDaemon
from optparse import OptionParser
import time

def load():
	return float(open('/proc/loadavg').read().split(' ')[0])

def update():
	hostLoad.setLoadAvg(load()).setHost(host).commit()
	print "Updating HostLoad Host: %s Load: %f" % (host.name(), hostLoad.loadAvg())

def buildOptionsParser():
	parser = OptionParser(usage="usage: %prog [options]")
	parser.add_option( "-i", "--interval", 			dest="interval", 		default=0, help="Update Interval in seconds.  0 updates once then exits." )
	parser.add_option( "-d", "--daemonize", 		dest="daemonize", 		action="store_true", default=None, help="Fork as a deamon" )
	return parser

def main():
	global host, hostLoad
	parser = buildOptionsParser()
	parser.parse_args()
	opts = parser.values
	if opts.daemonize:
		createDaemon()
	host = Host.currentHost()
	hostLoad = host.hostLoad()
	while True:
		update()
		if opts.interval <= 0:
			break
		time.sleep(float(opts.interval))

if __name__=="__main__":
	main()

