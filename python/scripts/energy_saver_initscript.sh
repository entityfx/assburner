#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          ab_energy_saver
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Assburner energy saver daemon
# Description:       
#                    
### END INIT INFO
# -*- coding: utf-8 -*-
# Debian init.d script for assburner energy saver
# Copyright © 2014 Matt Newell <newellm@blur.com>

DAEMON=/usr/bin/energy_saver.py
DAEMON_ARGS=-daemonize
NAME=ab_energy_saver
DESC="assburner energy saver"

