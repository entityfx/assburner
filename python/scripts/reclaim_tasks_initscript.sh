#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          ab_reclaim_tasks
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Assburner reclaim tasks daemon
# Description:       
#                    
### END INIT INFO
# -*- coding: utf-8 -*-
# Debian init.d script for assburner reclaim tasks daemon
# Copyright © 2014 Matt Newell <newellm@blur.com>

DAEMON=/usr/bin/reclaim_tasks.py
DAEMON_ARGS=-daemonize
NAME=ab_reclaim_tasks
DESC="assburner reclaim tasks"

