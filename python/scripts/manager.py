#!/usr/bin/python

from PyQt4.QtCore import *
from PyQt4.QtSql import *
from blur.Stone import *
from blur.Classes import *
import blur
import sys, time, bisect, os
from math import ceil, floor, log10, pow
from blur.defaultdict import DefaultDict
from exceptions import Exception
import traceback

if sys.argv.count('-daemonize'):
	from blur.daemonize import createDaemon
	createDaemon(pidFilePath='/var/run/ab_manager.pid')

app = QCoreApplication(sys.argv)

initConfig( "/etc/manager.ini", "/var/log/ab/manager.log" )
# Read values from db.ini, but dont overwrite values from manager.ini
# This allows db.ini defaults to work even if manager.ini is non-existent
config().readFromFile( "/etc/db.ini", False )

initStone(sys.argv)

blur.RedirectOutputToLog()

# Load all the database tables
blurqt_loader()

# Enable loading of job.hostlist by default
Job.c.HostList.field().setFlag( Field.NoDefaultSelect, False )

# Don't log the periodic changes to assburnerAutoPacketTarget
Config.schema().removeTrigger( blur.Classes.eventLogTrigger )

VERBOSE_DEBUG = sys.argv.count('-verbose') or False
DEBUG_LICENSES = sys.argv.count('-debug-licenses') or False

sendGraphiteStats = True

if sys.argv.count('-no-graphite'):
	sendGraphiteStats = False

if VERBOSE_DEBUG:
	Database.current().setEchoMode( Database.EchoUpdate | Database.EchoDelete ) #| Database.EchoSelect )

# Connect to the database
Database.current().connection().reconnect()

# Automatically setup the AB_manager service record
# the hostservice record for our host, and enable it
# if no other hosts are enabled for this service
service = Service.ensureServiceExists('AB_manager')
hostService = service.byHost(Host.currentHost(),True)
hostService.enableUnique()

# create_jobassignment will throw an error if the host is no longer
# ready.  This is by design so we don't want sql error emails sent.
class ManagerSqlErrorHandler(SqlErrorHandler):
	def handleError(self,errorMessage):
		if 'Host no longer ready for assignment' in errorMessage:
			return
		SqlErrorHandler.handleError(errorMessage)
errorHandler = ManagerSqlErrorHandler()
SqlErrorHandler.setInstance(errorHandler)

# Stores/Updates Config vars needed for manager
class ManagerConfig:
	def __init__(self):
		self._AUTOPACKET_TARGET = Config.getFloat( 'assburnerAutoPacketTarget', 600 )
		self.lastAutoPacketTargetUpdate = None
		self.update()
		
	def update(self):
		# Sort parameters
		self._SORT_METHOD = Config.getString( 'assburnerSortMethod', 'key_default' )
		try:
			JobAssign.SortMethod = getattr(JobAssign,str(self._SORT_METHOD))
			if VERBOSE_DEBUG:
				print ("Using %s job sort method" % self._SORT_METHOD)
		except:
			if VERBOSE_DEBUG:
				print "Config sort method %s not found, using key_default" % self._SORT_METHOD
			JobAssign.SortMethod = JobAssign.key_default
			
		self._PRIO_SORT_MUL = Config.getFloat( 'assburnerPrioSort', 500 )
		self._ERROR_SORT_MUL = Config.getFloat( 'assburnerErrorSort', 1 )
		self._SUB_TIME_SORT_MUL = Config.getFloat( 'assburnerSubmissionSort', .01 )
		self._HOSTS_SORT_MUL = Config.getFloat( 'assburnerHostsSort', -.5 )
		self._COMPLETION_PERCENT_SORT_MUL = Config.getFloat( 'assburnerCompletionPercentSort', -.5 )
		
		# Mapserver weight parameters
		self._MAPS_WEIGHT_MUL = Config.getFloat( 'assburnerMapWeightMultiply', 1.0 )
		self._PC_WEIGHT_MUL = Config.getFloat( 'assburnerPointCacheWeightMultiply', 0.2 )
		self._XREF_WEIGHT_MUL = Config.getFloat( 'assburnerXrefWeightMultiply', 1.0 )
		self._WEIGHT_DIVIDER = Config.getFloat( 'assburnerWeightDivider', 1024 * 1024 * 50 )
	
		# Rate of assignment parameters
		self._ASSIGN_RATE = Config.getFloat( 'assburnerAssignRate', 15 )
		self._ASSIGN_PERIOD = Config.getFloat( 'assburnerAssignPeriod', 10 )

		# Exponent used for scaling priority to compute the auto packet target time for each job
		self._AUTOPACKET_TARGET_EXP = Config.getFloat( 'assburnerAutoPacketTargetExponent', 1.5 )
		
		# Packet size heuristics parameters
		self._AUTOPACKET_MIN = Config.getFloat( 'assburnerAutoPacketMin', 100 )
		self._AUTOPACKET_MAX = Config.getFloat( 'assburnerAutoPacketMax', 2400 )
		# Increment and decrement are % of the difference between min and max per hour
		self._AUTOPACKET_INC = Config.getFloat( 'assburnerAutoPacketInc', 2.0 )
		self._AUTOPACKET_DEC = Config.getFloat( 'assburnerAutoPacketDec', 8.0 )
		self._AUTOPACKET_DEFAULT = Config.getFloat( 'assburnerAutoPacketDefault', 60 )
		# Commit the latest computed value, set the name in case the record doesn't yet exist
		Config( 'assburnerAutoPacketTarget' ).setConfig( 'assburnerAutoPacketTarget' ).setValue( QString.number( self._AUTOPACKET_TARGET ) ).commit()
		
		# We modify autopacket target for project weighted assignments so that project weights can optionally be more strictly enforced
		self._AUTOPACKET_TARGET_PROJECT_WEIGHT_SCALE = Config.getFloat( 'assburnerAutoPacketTargetProjectWeightScale', .5 )
		
		#misc
		self._HOST_ERROR_LIMIT = Config.getFloat( 'assburnerHostErrorLimit', 2 )
		
		self._OUTER_TASKS_ASSIGNMENT_LIMIT = Config.getInt( 'assburnerOuterTasksAssignmentLimit', 4 )
		
		# Set this just below the maximum memory available on the entire farm
		# This will allow jobs that require more memory to still pick up hosts (thrashing is better than not running at all in most cases)
		self._MAX_MEMORY_CUTOFF = Config.getInt('assburnerMaxMemoryCutoffMb', 46 * 1024 )
		
		# The product of this multiplier and the average or estimated memory of a job is used for the mininum memory
		# value a machine must have to be considered a candidate to run that job
		self._REQUIRED_MEMORY_MULTIPLIER = Config.getFloat('assburnerRequiredMemoryMultiplier', 1.0)
		
	def updateAutoPacketTarget(self, load):
		now = QDateTime.currentDateTime()
		if self.lastAutoPacketTargetUpdate:
			load_mult = 0
			if load > 1.0:
				load_mult = config._AUTOPACKET_INC * min(1.0, log10(load))
			else:
				load_mult = -config._AUTOPACKET_DEC * (1.0 - load)
			load_mult = load_mult * (Interval(now,self.lastAutoPacketTargetUpdate).microseconds() / float(Interval.fromString('1 hour')[0].microseconds())) / 100.0
			newAutoPacketTarget = min(self._AUTOPACKET_MAX, max(self._AUTOPACKET_MIN, self._AUTOPACKET_TARGET + (load_mult * (self._AUTOPACKET_MAX - self._AUTOPACKET_MIN)) ) )
			if VERBOSE_DEBUG:
				print "Load %f, Autopacket target changed from %f to %f" % (load, self._AUTOPACKET_TARGET,newAutoPacketTarget)
			self._AUTOPACKET_TARGET = newAutoPacketTarget
		self.lastAutoPacketTargetUpdate = now
		
# Utility Functions
def numberListToString(number_list):
	return ','.join([str(i) for i in number_list])

def unique(alist):
	return [alist[i] for i in range(0,len(alist)) if alist.index(alist[i])==i]

# Wraps a single job that needs tasks assigned
class JobAssign:
	def __init__( self, job, jobStatus, erroredHosts ):
		self._hostOk = None
		self.sortKey = None
		self.Job = job
		self.JobStatus = jobStatus
		self.hostsOnJob = jobStatus.hostsOnJob()
		self.lastPriorityHostsOnJob = None
		self.tasksUnassigned = self.JobStatus.tasksUnassigned()
		self.tasksAssigned = self.JobStatus.tasksAssigned()
		self.servicesRequired = ServiceList()
		self.licensedServicesRequired = ServiceList()
		# Populated by the farm resource snapshot
		self.IsPreassigned = job.packetType() == 'preassigned'
		self.preassignedHosts = set() if self.IsPreassigned else None
		# Optimization
		if self.IsPreassigned:
			self.hostWhiteList = self.hostWhiteListPreassigned
			
		self._mapServerWeight = None
		self.logString = ''
		self.ErroredHosts = set(erroredHosts)
		self.JobTargetSeconds = None
		self.activeHostAssignments = set()
		self.MemoryRequired = None
		
	def allowUnassignment( self ):
		# Allow smaller packets defaults to false, but auto packet size means we can do what we want
		allowSmallerPackets = self.Job.packetSize() <= 0 or self.Job.allowSmallerPackets()
		return allowSmallerPackets and (self.Job.packetType() != 'preassigned') and (self.Job.jobType().name() != 'Fusion')
	
	# TODO: Convert host list to a keyHost int[]
	def loadHosts( self ):
		if self._hostOk is None:
			self._hostOk = [s for s in str.split(str(self.Job.hostList()),',') if len(s)]
	
	# Return value of None means there is no white list, all non-blacklisted hosts that 
	# satisfy this jobs service list are valid.
	def hostWhiteList( self ):
		ret = HostList()
		self.loadHosts()
		if len(self._hostOk)==0:
			return None
		for hostName in self._hostOk:
			host = Host( hostName )
			if host.isRecord():
				ret.append( host )
		return (ret - self.hostBlackList())
	
	def hostWhiteListPreassigned( self ):
		return HostList(self.preassignedHosts)
	
	# Returns the list of hosts that cannot be assigned to this job
	# Currently this is only errored out hosts
	def hostBlackList( self ):
		ret = HostList()
		for fkeyhost in self.ErroredHosts:
			ret.append( Host(fkeyhost) )
		return ret
	
	def memoryRequired( self ):
		# We need an idea of how much memory we require, in kb
		if self.Job.minMemory() > 0:
			memoryRequired = self.Job.minMemory()
		elif self.JobStatus.averageMemory():
			# Adjust the needed memory by a configurable muiltipler, that can account for memory used by the OS
			# and how much will be allowed to swap, etc.
			memoryRequired = self.JobStatus.averageMemory() * config._REQUIRED_MEMORY_MULTIPLIER
		else:
			# Only use 80% of the estimated memory as a minimum, just in case the estimate is low
			memoryRequired = self.Job.estimatedMemory() * config._REQUIRED_MEMORY_MULTIPLIER * 1.2

			# Skip the 16 and 24 Gig machines if the estimate is not from a previous render.
			# These estimates are notoriously innaccurate and so it should be safer to require the 48+
			# gig machines until and actual average is found for the job
			#if self.Job.jobType().name().startsWith('Max') and not self.Job.stats().contains( "memoryEstimateType=JobStat" ):
			#	memoryRequired = max( 1024 * 1024 * 45, memoryRequired )
		
		# Use config var to ensure high mem jobs can always get some set of hosts
		memoryRequired = min( config._MAX_MEMORY_CUTOFF * 1024, memoryRequired )
		self.MemoryRequired = memoryRequired
		self.memoryRequired = self.memoryRequiredCached
		return self.MemoryRequired
	
	def memoryRequiredCached( self ):
		return self.MemoryRequired
	
	# If hostStatus is passed, then the function will check for currently
	# available memory.  Otherwise the function will only check if the host
	# has enough memory installed.
	def hostMemoryOk( self, host, hostStatus=None ):

		memoryRequired = self.memoryRequired()

		if hostStatus:
			if memoryRequired > (hostStatus.availableMemory() * 1024):
				if VERBOSE_DEBUG: Log( 'Not enough memory, %i required, %i available' % (memoryRequired, hostStatus.availableMemory() * 1024) )
				return False

		elif memoryRequired > (host.memory() * 1024):
			if VERBOSE_DEBUG: Log( 'Not enough memory, %i required, %i available' % (memoryRequired, host.memory() * 1024) )
			return False

		# memory is ok to assign
		return True

	# If resourcesReady == True, this function will only 
	# return true if the host currently has enough availabe
	# resources for the job.  Otherwise it well test if
	# the host has enough resources if there are no competing
	# jobs running on the host
	def hostOk( self, hostInfo, resourcesReady=True ):

		host = hostInfo.host
		# Check for preassigned job list
		if self.IsPreassigned:
			if not host in self.preassignedHosts:
				return False

		if resourcesReady and hostInfo.activeAssignmentCount > 0:
			# These are normal code paths, not indicative of logic fault elsewhere,
			# because after an assignment the hosts will stay in the list of idle hosts
			# for a given set of services, to avoid recomputing those lists
			if hostInfo.hasExclusiveAssignment:
				#if VERBOSE_DEBUG: Log( 'Host %s already has exclusive assignment' % hostInfo.host.name() )
				return False
			if self.Job.exclusiveAssignment():
				#if VERBOSE_DEBUG: Log( 'Host %s already has an assignment, can\'t assign an exclusive job' )
				return False

		# Only check available memory if resourcesReady is true, and there is already an active assignment
		# Since it can take a bit for memory to get freed, then reported as available, it will cause a delay
		# between a job finishing/erroring and the next job getting picked up, so we just take into account
		# the host's memory if there are no active assignments
		if not self.hostMemoryOk( host, resourcesReady and hostInfo.activeAssignmentCount and hostInfo.hostStatus or None ):
			return False

		# Check in host list
		self.loadHosts()
		if len(self._hostOk) > 0 and not host.name() in self._hostOk:
			#if VERBOSE_DEBUG: print "Skipping Host, Not In Host List: ", host.name()
			return False
			
		# Check for errored hosts
		if host.key() in self.ErroredHosts:
			#if VERBOSE_DEBUG: print "Job %i errored out on host %s with %i errors." % (self.Job.key(),host.name(),self.ErroredHosts[host.key()])
			return False
		
		if not hostInfo.jobOk(self):
			return False
		
		return True
	
	def priority( self ):
		if self.lastPriorityHostsOnJob != self.hostsOnJob:
			computePriority = self.Job.priority()

			# Hack so that fusion jobs with host lists get their hosts before the ones without
			if self.Job.jobType().name() == 'Fusion':
				self.loadHosts()
				if len(self._hostOk):
					computePriority -= 1

			if self.Job.prioritizeOuterTasks() and not self.JobStatus.outerTasksAssigned() and self.JobStatus.outerTasksAssignedCount() < config._OUTER_TASKS_ASSIGNMENT_LIMIT:
				computePriority = min(20,computePriority)
			self._priority = \
				computePriority                 * config._PRIO_SORT_MUL \
				+ self.JobStatus.errorCount()   * config._ERROR_SORT_MUL \
				+ self.Job.submittedts().toTime_t()     * config._SUB_TIME_SORT_MUL \
				+ self.hostsOnJob   * config._HOSTS_SORT_MUL \
				+ (self.JobStatus.tasksDone() * 100.0 / max(.001,self.JobStatus.tasksCount()))  * config._COMPLETION_PERCENT_SORT_MUL

			#if self.Job.jobType().name() == 'Fusion':
			#	print "Job %s has computed priority %f" % (self.Job.name(), self._priority)

			self.lastPriorityHostsOnJob = self.hostsOnJob
		return self._priority

	def __cmp__( self, other ):
		if id(self) == id(other):
			return 0
		if JobAssign.SortMethod(self) > JobAssign.SortMethod(other):
			return 1
		return -1
	
	def key_default( self ):
		sortKey = self.priority()
		#if VERBOSE_DEBUG: print 'job %s has key %s' % (self.Job.name(), sortKey)
		return sortKey

	def key_even_by_priority( self ):
		hasHost = 0
		if self.hostsOnJob > 0: hasHost = 1
		sortKey = '%01d-%03d-%04d-%04d-%10d' % (hasHost,self.Job.priority(), self.hostsOnJob, self.JobStatus.errorCount(), self.Job.submittedts().toTime_t())
		if VERBOSE_DEBUG: print 'job %s has sortKey %s' % (self.Job.name(), sortKey)
		return sortKey
	
	def mapServerWeight( self ):
		if self._mapServerWeight == None:
			ret = 0
			if not self.Job.stats().isEmpty():
				stats = str.split( str(self.Job.stats()), ';' )
				for s in stats:
					pieces = str.split(s,"=")
					if len(pieces) == 2:
						try:
							(key,val) = pieces
							val = float(val) * 1024 * 1024
							if key == 'totalMapSize':
								ret += val * config._MAPS_WEIGHT_MUL
							elif key == 'totalXrefSize':
								ret += val * config._XREF_WEIGHT_MUL
							elif key == 'totalPointCacheSize':
								ret += val * config._PC_WEIGHT_MUL
						except: pass  # Fail gracefully when the value isn't a valid float
						
			ret = ret / config._WEIGHT_DIVIDER
			if ret <= 0:
				ret = max(1,min(config._ASSIGN_RATE / 2.0,self.JobStatus.tasksAverageTime() / 120.0) )
				#if VERBOSE_DEBUG: print "Job %s has weight(calculated from average task time, no stats available): %g" % (self.Job.name(), ret)
			ret = min( 20, ret )
			self._mapServerWeight = ret
		return self._mapServerWeight
		
	def calculateAutoPacketSize( self, totalHosts, totalTasks ):
		if self.Job.jobType().name() == 'Fusion':
			return min(10,max(3,totalTasks / totalHosts))
		return self.calculateAutoPacketSizeByAvgTime( totalHosts, totalTasks )
	
	def calculateAutoPacketSizeByAvgTime( self, totalHosts, totalTasks ):
		tasksByHost = ceil( totalTasks / float(totalHosts) )
		maxSize = 50
		target = self.jobTargetSeconds()
		idealSize = ceil(target / self.calculateAverageTaskTime())
		packetSize = min( idealSize, tasksByHost )
		return max(packetSize,1)
	
	def calculateAverageTaskTime( self ):
		if self.JobStatus.tasksAverageTime() > 0 and self.JobStatus.tasksDone() > 0:
			return self.JobStatus.tasksAverageTime()
		if self.Job.jobType().name() == 'Fusion':
			return 300
		return config._AUTOPACKET_DEFAULT
	
	def calculateNeededHosts( self ):
		self.loadHosts()
		packetSize = self.Job.packetSize()
		if packetSize < 1:
			packetSize = self.calculateAutoPacketSize( 1, self.tasksUnassigned + self.tasksAssigned )
		maxHosts = int( (self.tasksUnassigned + self.tasksAssigned - self.hostsOnJob) / float(packetSize) + 0.9 )
		# Can't assign more hosts than what in our host list
		if self._hostOk:
			maxHosts = min( len(self._hostOk) - self.hostsOnJob, maxHosts )
		return maxHosts
	
	def jobTargetSeconds( self ):
		if self.JobTargetSeconds is None:
			# Target packet time duration, smaller time for higher(lower #) priority jobs
			target = config._AUTOPACKET_TARGET * pow(max(self.Job.priority(),3),config._AUTOPACKET_TARGET_EXP) / 50
			# Log( "Calculated Job Target Time for job %s: %i" % (self.Job.name(),target) )
			if self.Job.jobType().name() == 'Fusion':
				target = target / 5.0
			self.JobTargetSeconds = target
		return self.JobTargetSeconds
	
	def jobTargetTime( self ):
		return QDateTime.currentDateTime().addSecs(self.jobTargetSeconds())
	
	def printCompletionTimes( self ):
		Log( "Calculated Job Target Time for job %s: estimated %s, target %s" % (self.Job.name(),self.JobStatus.estimatedCompletionTime().toString(),self.jobTargetTime().toString()) )
		
	def checkNeedsMoreHosts( self, excessHosts = False ):
		if self.tasksUnassigned > 0:
			return True
		if self.tasksAssigned == 0:
			return False
		flexiblePackets = self.allowUnassignment()
		if excessHosts and flexiblePackets:
			return True
		# We have assigned tasks that aren't busy.  Check to see if we are going to hit the target time
		# Only want more hosts if we have auto packet size
		if flexiblePackets and self.JobStatus.estimatedCompletionTime().isValid():
			target = self.jobTargetTime()
			est = self.JobStatus.estimatedCompletionTime()
			if est > target:
				return True
		return False
		
	def retrieveRandomTasks(self, limit):
		self.logString += 'Random [limit=%i] ' % limit
		return JobTask.select("fkeyjob=%i AND status='new' ORDER BY random() ASC LIMIT %i" % (self.Job.key(),limit))
	
	def retrieveSequentialTasks(self, limit):
		self.logString += 'Sequential [limit=%i] ' % limit
		return JobTask.select("fkeyjob=%i AND status='new' ORDER BY jobtask ASC LIMIT %i" % (self.Job.key(),limit))
	
	def retrieveContinuousTasks(self, limit):
		self.logString += 'Continuous [limit=%i] ' % limit
		return JobTask.select("keyjobtask IN (SELECT * FROM get_continuous_tasks(%i,%i))" % (self.Job.key(),limit))
	
	def retrievePreassignedTasks(self, host):
		self.logString += 'Preassigned '
		return JobTask.select("fkeyjob=%i AND fkeyhost=%i AND status='new'" % (self.Job.key(),host.key()))
	
	# Returns (JobTaskList,allOuterTasksAssigned)
	def retrieveOuterTasks(self,limit=None):
		# Make Unique
		tasks = unique([self.Job.minTaskNumber(), self.Job.midTaskNumber(), self.Job.maxTaskNumber()])
		self.logString += ('Outer Tasks [%s] ' % str(tasks))
		taskList = JobTask.select( "jobtask IN (%s) AND fkeyjob=%i AND status='new'" % ( numberListToString(tasks), self.Job.key() ) )
		if limit > 0 and len(taskList) > limit:
			return (taskList[:limit],False)
		return (taskList, True)
		
	def assignHost( self, hostInfo, totalHosts, totalTasks ):
	
		host = hostInfo.host
		hostStatus = hostInfo.hostStatus
		tasks = None
		packetSize = self.Job.packetSize()
		
		self.logString = ' PT: '
		if self.Job.prioritizeOuterTasks() and not self.JobStatus.outerTasksAssigned() and self.JobStatus.outerTasksAssignedCount() < config._OUTER_TASKS_ASSIGNMENT_LIMIT:
			tasks,allAssigned = self.retrieveOuterTasks(limit=packetSize)
			if allAssigned:
				self.JobStatus.setOuterTasksAssigned(True)
			self.JobStatus.setOuterTasksAssignedCount(self.JobStatus.outerTasksAssignedCount()+1)
		else:
			if packetSize < 1:
				packetSize = self.calculateAutoPacketSize( totalHosts, totalTasks )
			packetType = self.Job.packetType()
			if VERBOSE_DEBUG: print "Finding Tasks For PacketType:", packetType, " packetSize: ", packetSize, " totalTasks: ", totalTasks
			if packetType == 'random':
				tasks = self.retrieveRandomTasks(limit = packetSize)
			elif packetType == 'preassigned':
				tasks = self.retrievePreassignedTasks(host)
			elif packetType == 'continuous':
				tasks = self.retrieveContinuousTasks(limit=packetSize)
			else: # sequential
				tasks = self.retrieveSequentialTasks(limit=packetSize)
		
		if not tasks or tasks.isEmpty():
			raise NonCriticalAssignmentError( "No tasks returned to assign" )
		
		q = Database.current().exec_("SELECT create_job_assignment( %i, %i, ARRAY[%s] )" % (self.Job.key(), host.key(), tasks.keyString()))
		
		if not q.next():
			raise NonCriticalAssignmentError( "Failed to create job assignment: " + q.lastError().text() )

		jobAssignmentKey = q.value(0).toInt()[0]
		
		# Commit in case we changed outerTasksAssigned[Count] above
		self.JobStatus.commit()
		self.hostsOnJob += 1
		
		# Refresh activeAssignmentCount
		hostStatus.reload()
		# deduct memory requirement from available so we don't over allocate the host
		memoryRequired = self.memoryRequired()
		hostStatus.setAvailableMemory( (hostStatus.availableMemory()*1024 - memoryRequired) / 1024 )
		
		snapshot.takeLicenses( hostInfo, self )
		
		hostInfo.addJobAssignment(self.Job,jobAssignmentKey)
		hostInfo.activeAssignmentCount = hostStatus.activeAssignmentCount()

		self.tasksUnassigned -= tasks.size()
		self.tasksAssigned += tasks.size()
		
		Log( "Assigned %s(%i assignments) to %i[%s] priority %i, project %s, submitted %s with options %s" % ( hostStatus.host().name(), hostStatus.activeAssignmentCount(), self.Job.key(), self.Job.name(), self.Job.priority(), self.Job.project().name(), self.Job.submittedts().toString(), self.logString ) )
		
		Log( "Tasks:" + numberListToString(tasks.frameNumbers()) )
		return tasks.size()

	def unassign_tasks(self,surplusHosts):
		
		if not self.allowUnassignment():
			Log( 'JobAssign.unassign_tasks called on job with JobAssign.allowUnassignment()=False' )
			return 0
		
		#self.printCompletionTimes()
		
		job = self.Job

		# Used to store how many assigned tasks each host has for this job
		# TODO: Should be per assignment, not per host
		class AssignmentInfo(object):
			def __init__(self,hostKey,jobTaskKeys,busyCount):
				self.host = Host(hostKey)
				self.jobTaskKeys = [int(jtak) for jtak in jobTaskKeys[1:-1].split(',')]
				self.busyCount = busyCount
				self.canUnassign = len(self.jobTaskKeys) + min(busyCount,1) - 1
				
		# Get the hosts, and the number of busy and assigned frames
		# Only hosts that have at least one assigned frame and one busy frame are selected
		# because we don't want to unassign the last assigned frame if the host is loading 
		# the job or already finished the previous frame.
		q_hosts = Database.current().exec_("select * FROM (select fkeyhost, array_remove(array_agg(keyjobtask),null) as task_keys, sum(busy) as busy_count from (select keyjobassignment, fkeyhost, CASE WHEN jta.fkeyjobassignmentstatus=1 THEN jta.fkeyjobtask ELSE NULL END as keyjobtask, CASE WHEN jta.fkeyjobassignmentstatus=3 THEN 1 ELSE 0 END as busy from jobassignment ja inner join jobtaskassignment jta on keyjobassignment=jta.fkeyjobassignment AND jta.fkeyjobassignmentstatus IN (1,3) WHERE fkeyjob=%i order by keyjobtaskassignment ASC) as iq group by keyjobassignment, fkeyhost) as iq2 where array_length(task_keys,1) >= 1" % job.key())
			
		
		assignmentInfoList = []
		ival = lambda x: q_hosts.value(x).toInt()[0]
		
		maxCanUnassign = 0
		available = 0
		while q_hosts.next():
			ai = AssignmentInfo( ival(0), q_hosts.value(1).toString(), ival(2) )
			maxCanUnassign = max(ai.canUnassign,maxCanUnassign)
			available += ai.canUnassign
			assignmentInfoList.append( ai )
		
		if not maxCanUnassign:
			return 0
		
		assigned_count = len(assignmentInfoList)
		
		ideal_assigned_count_per_host = floor(available / float(assigned_count + surplusHosts))
		to_unassign = surplusHosts * ideal_assigned_count_per_host
		to_unassign_orig = to_unassign
		
		print "Reassigning %i frames out of %i available from %i hosts" % (to_unassign,available,assigned_count)
		
		# Each time through the loop, we reassign only the hosts with max_assigned frames
		# that was we don't reassign from a host that only has 1 task left, when there another with 10
		# If we get to a point where none are reassigned, then we can break out of the loop
		cont = True
		resetFrameNumbers = []
		resetHostNames = []
		while cont and to_unassign > 0 and maxCanUnassign > ideal_assigned_count_per_host:
			cont = False
			resetFrames = JobTaskList()
			for assignmentInfo in assignmentInfoList:
				#print "Max Frame == %i, assigned frames == %i" % (max_frames, h.errorTempo())
				if len(assignmentInfo.jobTaskKeys) + min(assignmentInfo.busyCount,1) - 1 == maxCanUnassign:
					jobTask = JobTask(assignmentInfo.jobTaskKeys.pop())
					#print "Setting frameList %s" % (frameList)
		#			print "JobTask host %s, Host %s" % (h.name(), jt.host().name())
					if jobTask.host() == assignmentInfo.host and jobTask.status() == 'assigned':
						if VERBOSE_DEBUG:
							print "Unassigning Frame %i of job %s from host %s" % (jobTask.frameNumber(), job.name(), assignmentInfo.host.name())
						resetFrameNumbers.append(jobTask.frameNumber())
						hostNameStr = str(assignmentInfo.host.name())
						if not hostNameStr in resetHostNames:
							resetHostNames.append(hostNameStr)
						jobTask.setStatus('new')
						jobTask.setHost( Host() )
						resetFrames += jobTask
						to_unassign -= 1
						cont = True
				
				# If we've already
				if to_unassign == 0:
					break
					
			# Commit the reassigned frames, lower max_frames
			resetFrames.commit()
			maxCanUnassign -= 1
			
		print "Unassigned %i 'assigned' frames of job %s from hosts %s" % (len(resetFrameNumbers), job.name(), ",".join( resetHostNames ) )
		#print "Unassigned frames %s" % (",".join( map( lambda x: str(x), resetFrameNumbers ) ))
		
		unassignedCount = to_unassign_orig - to_unassign
		self.tasksUnassigned += unassignedCount
		self.tasksAssigned -= unassignedCount
		self.JobStatus.setMaxHostTasks( maxCanUnassign )
		return unassignedCount

def updateProjectTempo():
	Database.current().exec_( "SELECT * from update_project_tempo()" )

# Returns [hostsTotal int, hostsActive int, hostsReady int, jobsTotal int, jobsActive int, jobsDone int]
def getCounter():
	q = Database.current().exec_( "SELECT * FROM getcounterstate()" )
	if not q.next(): return map(lambda x:0,range(0,6))
	return map(lambda x: q.value(x).toInt()[0], range(0,6))

class AssignRateThrottler(object):
	def __init__(self):
		self.assignLeft = config._ASSIGN_RATE
		self.lastAssignTime = QDateTime.currentDateTime()
	
	def update(self):
		# Number of hosts to assign per period(seconds) * weight of job assigned
		self.assignRate = config._ASSIGN_RATE
		self.assignPeriod = config._ASSIGN_PERIOD
		now = QDateTime.currentDateTime()
		self.assignLeft = self.assignLeft + self.assignRate * (self.lastAssignTime.secsTo(now)) / self.assignPeriod
		if self.assignLeft > self.assignRate:
			self.assignLeft = self.assignRate
		self.lastAssignTime = now
	
	def statusString(self):
		return "Assign Rate: %g Assign Period: %g Assign Left: %g" % ( float(self.assignRate), float(self.assignPeriod), float(self.assignLeft) )

class HostInfo(object):
	def __init__(self,host):
		self.host = host
		self.hostStatus = host.hostStatus()
		self.services = None
		
	def reset(self):
		self.activeAssignmentCount = self.hostStatus.activeAssignmentCount()
		# Job : [keyjobassignment,...] on this host
		self.activeJobAssignments = DefaultDict(list)
		self.hasExclusiveAssignment = False
		self.nearlyFree = False
		self.nearlyFreeReserved = False
		self.hasUser = self.host.getValue(Host.c.User).toPyObject() > 0
		self.sleeping = self.hostStatus.slaveStatus() == 'sleeping'
	
	def addJobAssignment(self,job,assignmentKey):
		self.activeJobAssignments[job].append(assignmentKey)
		if job.exclusiveAssignment():
			self.hasExclusiveAssignment = True

	# Checks to see if we should assign this job to this host at this time
	# If a user is likely to be using the machine before the job finishes
	# then it may waste the hosts time working on the job, and can cause
	# the job to finish later because the task will have to be restarted
	def jobOk(self,jobAssign):
		# TODO: Implement checking against average login time of the user
		# for each day of the week, and maybe try to account for project
		# schedules and how late the user worked the night before to determine
		# the likely login time.
		if not self.hasUser:
			return True
		
		hasDoneTasks = jobAssign.JobStatus.tasksDone() > 0
		avgTaskTime = jobAssign.JobStatus.tasksAverageTime()
		# Don't worry about jobs with task times less than 15 minutes.
		if hasDoneTasks and avgTaskTime < 15 * 60:
			return True
		
		# Need EmployeeStatus table with more info about the users login/logout times
		# to make more accurate decisions
		
		cdt = QDateTime.currentDateTime()
		dow = cdt.date().dayOfWeek()
		curHour = cdt.time().hour()
		
		# For now just consider nobody is working on weekends
		if dow == 6 or (dow == 5 and curHour > 18):
			return True
		
		# 10am login time until we are using per-user stats
		avgLoginTime = QTime(10,0)
		timeUntilLogin = None
		
		# Before noon, assume they will login at avgLoginTime, or immediately if past that
		if curHour < 12:
			timeUntilLogin = Interval(cdt,QDateTime(cdt.date(),max(cdt.time(),avgLoginTime)))
		# after 6:59pm...
		elif curHour > 18:
			 timeUntilLogin = Interval(cdt,QDateTime(cdt.date().addDays(1),avgLoginTime))
		# Lunch time, 12pm to 2:59pm
		elif curHour >= 12 and curHour <= 14:
			emp = self.host.user()
			if isinstance(emp,Employee):
				# 30 minutes for now, until we know when they went to lunch
				timeUntilLogin = Interval(30*60)
		
		# At this point we assume they aren't coming in today
		if timeUntilLogin is None:
			return True
		
		# Should have enough time to finish a task
		if hasDoneTasks and Interval(avgTaskTime) < timeUntilLogin:
			return True
		
		return False
	
	def __repr__(self):
		return '<%s %i>' % (self.host.name(), self.activeAssignmentCount)

def unassign_host_busy(fkeyhost,fkeyjob,job_reason):
	Database.current().exec_( "INSERT INTO PreemptionWaste (fkeyuser, fkeyjob, fkeyjobreason, timewasted, created) SELECT ?, ?, ?, SUM(NOW()-startedts), now() FROM JobTask WHERE fkeyjob=? AND fkeyhost=? AND startedts IS NOT NULL AND status='busy'", [QVariant(job_reason.user().key()),QVariant(fkeyjob),QVariant(job_reason.key()),QVariant(fkeyjob), QVariant(fkeyhost)] )
	q = Database.current().exec_( "UPDATE JobTask SET status='new' WHERE status IN ('assigned','busy') AND fkeyjob=? AND fkeyhost=?", [QVariant(fkeyjob),QVariant(fkeyhost)] )
	Database.current().exec_( "UPDATE HostStatus SET slaveStatus='starting' WHERE fkeyhost=?", [QVariant(fkeyhost)] )
	return q.numRowsAffected()

def unassign_host(fkeyhost,fkeyjob):
	sql = "UPDATE JobTask SET status='new'"
	if Job(fkeyjob).packetType() != 'preassigned':
		sql += ", fkeyhost=NULL"
	sql += " WHERE status='assigned' AND fkeyhost=%i AND fkeyjob=%i"
	q = Database.current().exec_( sql % (fkeyhost,fkeyjob) )
	tasksUnassigned = q.numRowsAffected()
	return tasksUnassigned

class ThrottleLimitException(Exception): pass
class AllHostsAssignedException(Exception): pass
class AllJobsAssignedException(Exception): pass
class NonCriticalAssignmentError(Exception): pass

class FarmResourceSnapshot(object):
	def __init__(self):
		# Host Info
		self.hostInfoByHost = {}
		self.reset()
	
	def reset(self):
		# Project Weighted Assigning variables

		# Weight per project (initially percentages, then changes to hosts available after scaleProjectWeights is run)
		self.projectWeights = {}
		# Current weight used by each project
		self.projectTempos = {}
		# The sum of all weight assigned to all projects
		self.projectWeightTotal = 0.0
		# The current amount of weight being used by all projects with weight
		self.projectTempoTotal = 0.0
		# Current sum of host.renderpower for all online hosts
		self.totalRenderPower = 0.0
		# The amount of weight not assigned to any project (1.0 - self.projectWeightTotal)
		self.projectWeightLeftover = 0.0
		# Each project that has jobs that can use more hosts has an entry in this dict
		self.projectsWithJobs = {}
		
		# All active jobs
		self.jobList = JobList()
		# Statuses for all active jobs
		self.jobStatusList = JobStatusList()
		
		# All active jobs
		self.jobAssignByJob = {}
		# Dict of Job : JobAssign of jobs that actually need assigned
		self.jobsToAssign = {}
		
		# Service info, only for jobs needing assignments
		self.servicesNeeded = ServiceList()
		# Services for all active jobs
		self.servicesByJob = DefaultDict(ServiceList)
		# All active jobs, not just those that need assignments
		self.jobsByService = DefaultDict(JobList)
		
		# Basic Farm Counter
		self.hostsTotal, self.hostsActive, self.hostsReady = getCounter()[:3]
		
		self.resetHostInfo()

		# Used to keep track of whether or not assignments have been
		# completed or were halted for some reason(throttling).
		# If we have throttled assignments, then we wont do unassignments
		# in order to free up tasks for idle hosts
		self.currentAssignmentsCompleted = False
		self.AssignExcessHosts = False

	def resetHostInfo(self):
		self.hostInfosByService = DefaultDict(set)
		# Populated on demand in availableHostsByServices, key is set of service keys
		self.hostInfosByServiceKeys = {}
		
	def refresh(self):
		self.reset()
		self.refreshProjectWeights()
		self.refreshJobList()
		if not self.jobsToAssign:
			return
		self.refreshServiceData()
		self.refreshHostData()
		
	def refreshProjectWeights(self):
		# Gather current project tempo(percentage of queue the project has)
		q = Database.current().exec_( "SELECT project.keyelement as fkeyproject, assburnerweight as weight, coalesce(tempo,0) as tempo FROM projecttempo RIGHT JOIN project ON keyelement=projecttempo.fkeyproject WHERE project.assburnerweight > 0" )
		while q.next():
			fkeyproject = q.value(0).toInt()[0]
			weight = q.value(1).toDouble()[0]
			tempo = q.value(2).toDouble()[0]
			self.projectWeights[fkeyproject] = weight
			self.projectTempos[fkeyproject] = tempo
			self.projectWeightTotal += weight
			self.projectTempoTotal += tempo
		self.projectWeightLeftover = 1.0 - self.projectWeightTotal
		self.totalRenderPower = Config.getFloat('assburnerTotalRenderPower')

	def scaleProjectWeights(self):
		hostsActive = self.hostsActive + self.hostsReady
		# If there is more than 1.0(100%) projectweight, than
		# we will scale down the project weights so that they
		# add up to 1.0
		# Multiply by available hosts
		if self.projectWeightTotal > 1.0:
			self.projectWeightTotal = hostsActive / self.projectWeightTotal
		else:
			self.projectWeightTotal = hostsActive
		
		# Here is where the weights go from a percentage to a host count
		for fkeyproject in self.projectWeights:
			self.projectWeights[fkeyproject] *= self.projectWeightTotal

	def printProjectWeights(self):
		if VERBOSE_DEBUG:
			for fkeyproject,weight in self.projectWeights.iteritems():
				print "%s has weight %f" % (Project(fkeyproject).name(), weight)

	def getErroredHosts(self, jobList, errorLimit):
		ret = DefaultDict(list)
		if jobList.isEmpty(): return ret
		q = Database.current().exec_("SELECT fkeyjob, fkeyhost FROM JobError WHERE fkeyJob IN (%s) AND cleared=false GROUP by fkeyJob, fkeyHost HAVING sum(count) > %i" % (jobList.keyString(),errorLimit))
		while q.next():
			ret[q.value(0).toInt()[0]].append(q.value(1).toInt()[0])
		return ret
	
	def loadPreassignedHosts(self):
		# Load preassigned hosts lists
		q = Database.current().exec_( "SELECT fkeyjob, array_agg(fkeyhost) FROM JobTask WHERE status='new' AND fkeyJob IN (%s) GROUP BY fkeyJob" % \
			self.jobList.filter(Job.c.PacketType == 'preassigned').keyString() )
		
		while q.next():
			job_key = q.value(0).toInt()[0]
			jobAssign = self.jobAssignByJob[Job(job_key)]
			host_key_string = q.value(1).toString()[1:-1]
			hosts = HostList()
			# This will return empty lists if the preassigned job has no 'new' tasks
			if host_key_string:
				host_keys = [int(s) for s in host_key_string.split(',') if s != 'NULL']
				hosts = Host.table().records(host_keys)
			jobAssign.preassignedHosts = set(hosts)

	def refreshJobList(self):
		
		self.jobList = Job.c.Status.in_( ['ready','started'] ).select()
		self.jobStatusList = self.jobList.jobStatuses(Index.UseSelect)
		
		# Load all errored hosts in one select( actually two since we use a different limit for fusion jobs )
		fusionJobs = self.jobList.filter(Job.c.JobType == JobType('Fusion'))
		erroredHostsByJob = self.getErroredHosts(self.jobList - fusionJobs, config._HOST_ERROR_LIMIT)
		erroredHostsByJob.update(self.getErroredHosts(fusionJobs,20))
		
		jobsToAssign = []
		for jobStatus in self.jobStatusList:
			job = jobStatus.job()

			# Create Job Assign class
			jobAssign = JobAssign(job,jobStatus,erroredHostsByJob[job.key()])
			self.jobAssignByJob[job] = jobAssign
			
			# We can assign unassigned or assigned (reassign to new host) tasks
			# otherwise all tasks are done/suspended/canceled/busy
			if jobAssign.tasksUnassigned + jobAssign.tasksAssigned == 0:
				continue
			
			# We can't assign more hosts to a job if it's already at the maximum
			# maxHosts <= 0 means there is no limit
			if job.maxHosts() > 0 and jobAssign.hostsOnJob >= job.maxHosts():
				continue

			self.jobsToAssign[job] = jobAssign
			
			if jobAssign.tasksUnassigned > 0 or jobAssign.checkNeedsMoreHosts():
				self.projectsWithJobs[job.getValue(Job.c.Project).toPyObject()] = 1

		self.loadPreassignedHosts()
		
		self.TotalUnassignedTasks = 0
		self.TotalAssignedTasks = 0
		for jobAssign in self.jobsToAssign.itervalues():
			# Don't account for preassigned tasks when calculating farm load. WHY??
			if jobAssign.IsPreassigned != 'preassigned':
				self.TotalUnassignedTasks += jobAssign.tasksUnassigned
				self.TotalAssignedTasks += jobAssign.tasksAssigned
		
		weightOfProjectsWithJobs = 0.0
		leftOverWeight = 0.0
		# Delete projects with weight but no jobs, sum the total competing weight
		for k in self.projectWeights.keys():
			if not k in self.projectsWithJobs:
				leftOverWeight += max(self.projectWeights[k] - self.projectTempos[k], 0.0)
				del self.projectWeights[k]
				continue
			weightOfProjectsWithJobs += self.projectWeights[k]
		
		# This chunk of code implements the new algorithm to scale up weights of projects with jobs in order
		# to use up the total weight allocated to all projects.  By changing the True to False, it will revert to the old behavior
		if False and leftOverWeight > 0:
			for k in self.projectWeights.keys():
				self.projectWeights[k] += leftOverWeight * (self.projectWeights[k] / weightOfProjectsWithJobs)

		# Remove the current usage from the project weights
		self.projectWeightTotal = 0.0
		for k in self.projectWeights.keys():
			self.projectWeights[k] -= self.projectTempos[k]
			self.projectWeightTotal += self.projectWeights[k]
		# Weights now represent the % of the active farm that each project has left to be assigned
		print self.projectWeights
		if VERBOSE_DEBUG: Log( "Found %i jobs to assign" % len(self.jobList) )
	
	def addJobService(self,job,service):
		self.jobsByService[service] += job
		self.servicesByJob[job] += service
		if job in self.jobsToAssign and not service in self.servicesNeeded:
			self.servicesNeeded += service
		
	def refreshServiceData(self):
		# Gather required services
		for jobService in JobService.c.Job.in_(self.jobList).select():
			self.addJobService(jobService.job(), jobService.service())
		
		# Backward compat for jobs that have no jobservice records
		for jobAssign in self.jobAssignByJob.itervalues():
			jobAssign.servicesRequired = self.servicesByJob[jobAssign.Job]
			if jobAssign.servicesRequired.isEmpty():
				service = jobAssign.Job.jobType().service()
				if not service.isRecord():
					service = Service('Assburner')
				self.addJobService(jobAssign.Job,service)
				jobAssign.servicesRequired += service
		
		# Filter out services that have no available licenses
		# Service : [licenceCount, license_record]
		self.licCountByService = {}
		
		Database.current().exec_("SELECT resetLicenseCounts()")
		
		licenses = self.servicesNeeded.licenses(Index.UseSelect)
		licensedServices = ServiceList()
		for service in self.servicesNeeded:
			lic = service.license()
			if lic.isRecord():
				self.licCountByService[service] = [lic.total() - lic.reserved() - lic.inUse(),lic]
				licensedServices += service
		
		for jobAssign in self.jobAssignByJob.itervalues():
			jobAssign.licensedServicesRequired = ServiceList(jobAssign.servicesRequired & licensedServices)
		
				#Log( "%i licenses available for %s" % (self.licCountByService[service], lic.license()) )
		if self.licCountByService:
			print "License Counts " + ",".join(map( lambda x,y: "%s:%i" % (x.service(),y[0]), self.licCountByService.keys(), self.licCountByService.values() ))
	
	def refreshHostData(self):
		self.resetHostInfo()
		hostServices = HostServiceList()
		if VERBOSE_DEBUG: Log( "Fetching Hosts for required services, %s" % self.servicesNeeded.services().join(',') )
		
		# TODO: Have this join return the HostStatus records also,
		#       instead of a separate query below
		if not self.servicesNeeded.isEmpty():
			hostServices = HostService.select( """INNER JOIN HostStatus ON HostStatus.fkeyHost=HostService.fkeyHost 
													WHERE HostStatus.slaveStatus IN ('ready','copy','busy','assigned','sleeping')
													AND HostService.enabled=true AND HostService.fkeyService IN (%s)""" % self.servicesNeeded.keyString()  )
		
		hosts = hostServices.hosts().unique()
		hostStatuses = hosts.hostStatuses(Index.UseSelect)
		hostServicesByHost = hostServices.groupedByForeignKey(HostService.c.Host)
		
		for host in hosts:
			services = hostServicesByHost[host].services()
			hostInfo = self.hostInfoByHost.get(host,None)
			if hostInfo is None:
				hostInfo = self.hostInfoByHost[host] = HostInfo(host)
			hostInfo.reset()
			hostInfo.services = services
			for service in services:
				self.hostInfosByService[service].add(hostInfo)
		
		if VERBOSE_DEBUG:
			for service in self.servicesNeeded:
				print "%i hosts available for service %s" % (len(self.hostInfosByService[service]),service.service())

		q = Database.current().exec_("SELECT ja.fkeyhost, ja.fkeyjob, count(*), array_agg(keyjobassignment) FROM JobAssignment ja WHERE ja.fkeyjobassignmentstatus IN (1,2,3) group by ja.fkeyjob, ja.fkeyhost")
		while q.next():
			host = Host(q.value(0).toInt()[0])
			job = Job(q.value(1).toInt()[0])
			assignmentKeys = [int(key) for key in q.value(3).toString()[1:-1].split(',')]
			try:
				hostInfo = self.hostInfoByHost[host]
			except KeyError:
				print "Host %s has active assignment but inactive status %s" % (host.name(), host.hostStatus().slaveStatus())
				continue
			# Job Assignments are mostly static (most fields could be made immutable),
			# so we could probably keep them cached so that we don't have to look
			# them up later for proper unassignments
			hostInfo.activeJobAssignments[job] = assignmentKeys
			if job.exclusiveAssignment():
				hostInfo.hasExclusiveAssignment = True
			jobAssign = self.jobAssignByJob.get(job,None)
			if jobAssign is not None:
				jobAssign.activeHostAssignments.add(hostInfo)

	def availableHostsByServices(self,requiredServices,idleOnly):
		key = ','.join([str(key) for key in sorted(requiredServices.keys())])
		hst = self.hostInfosByServiceKeys.get(key)
		if hst is None:
			# Start the list off with the first service
			# Only return hosts that provide ALL required services
			# Make sure to take a copy so we don't modify the original
			hostInfos = self.hostInfosByService[requiredServices[0]].copy()
			for service in requiredServices[1:]:
				# Check if there are licenses available for the service
				hostInfos.intersection_update(self.hostInfosByService[service])
				if not hostInfos:
					if VERBOSE_DEBUG: print "No hosts available with required services: " + requiredServices.services().join(',')
					break
			
			def hostInfoSortKey(hostInfo):
				return int(hostInfo.sleeping) * 1000 + hostInfo.activeAssignmentCount

			hostInfos = list(hostInfos)
			hostInfos.sort( key = hostInfoSortKey )
			
			# Bisect doesn't support a function to get the value from the list entry...
			assignmentCounts = [hostInfoSortKey(hi) for hi in hostInfos]
			idleHostInfos = hostInfos[:bisect.bisect_right(assignmentCounts,0)]
			hst = self.hostInfosByServiceKeys[key] = (hostInfos,idleHostInfos)
		if idleOnly:
			return hst[1]
		return hst[0]
		
	# Get a list of host statuses that have all services required
	# If resourcesReadyOnly==True, then only hosts that have available
	# resources for the given job will be returned.  
	# resourcesReadyOnly=False is used by the job unassignment 
	# code to preempt jobs to make room for other higher priority jobs
	def availableHosts( self, jobAssign, resourcesReady=True, debug=False ):
		requiredServices = jobAssign.servicesRequired
		ret = []
		
		# Services are always required
		if requiredServices.isEmpty():
			if VERBOSE_DEBUG: print "Job has no service requirements, skipping"
			return ret
		
		job = jobAssign.Job
		exclusive = job.exclusiveAssignment()
		
		# service : count_avail
		licensesNeeded = {}
		minLicensesAvail = None
		existingHostsOnly = False
		if resourcesReady:
			for licensedService in jobAssign.licensedServicesRequired:
				licCount, lic_record = self.licCountByService[licensedService]
				
				# If we are out of one of the required licenses, skip out early
				# If the license is per host we can still assign the job to hosts
				# already assigned to the same job (theoretically could extend that
				# to any host with a job assigned that is using the same perhost
				# license)
				if licCount == 0:
					if lic_record.perHost() and not exclusive:
						existingHostsOnly = True
					else:
						return ret
				
				licensesNeeded[licensedService] = licCount
				if minLicensesAvail is not None:
					minLicensesAvail = min(licCount, minLicensesAvail)
				else:
					minLicensesAvail = licCount

		if existingHostsOnly:
			hostInfos = jobAssign.activeHostAssignments
		else:
			hostInfos = self.availableHostsByServices(requiredServices, idleOnly=(exclusive and resourcesReady))

		if jobAssign.IsPreassigned:
			hostInfos = [hi for hi in hostInfos if hi.host in jobAssign.preassignedHosts]

		if debug: print "Found %i hosts for possible assignment" % len(hostInfos)

		# We have the hosts that match the service requirements, now
		# we can check against the job's white/black list of hosts
		# and also make sure there are licenses available.

		for hostInfo in hostInfos:
			# Checks memory requirements, exclusive assignment conflicts,
			# and host lists (black and white)
			if not jobAssign.hostOk( hostInfo, resourcesReady ):
				continue
			
			activeAssignments = hostInfo.activeAssignmentCount
			# Hardcoded limit of concurrent jobs for now
			if resourcesReady and activeAssignments >= 5:
				continue
			
			# TODO: Use JobAssignments and proper resource checking
			if not resourcesReady:
				# Skip if it's already reserved
				if hostInfo.nearlyFreeReserved:
					continue
				
				hasActiveBlocker = False
				for activeJob in hostInfo.activeJobAssignments:
					if not activeJob.allowPreemption() or activeJob.priority() <= job.priority():
						hasActiveBlocker = True
						break
				
				if hasActiveBlocker:
					continue
				
			ret.append(hostInfo)
		
		jobStatus = jobAssign.JobStatus
		longTask = jobStatus.tasksDone() == 0 or jobStatus.tasksAverageTime() > Interval( seconds=60 * 30 )
		
		# Order the hosts so that those with the least amount of capabilities are
		# assigned first.
		if resourcesReady:
			# Machines without users assigned first
			# TODO: This is fine when there are a lot of idle hosts, but when the farm
			# is busy we really need to take into account the task time and try to assign
			# shorter tasks to hosts where a user may be returning.  Ideally this will
			# take into account the average times the user arrives for work, how long the
			# user takes for a lunch break, any vacation days scheduled, etc.
			def hostInfoSortKey(hostInfo):
				# Return 1 if it's a long task and user machine, or short task and non-user machine
				# So that user machines get the short tasks first
				# TODO: If farm load is low enough always pick a farm host
				return 1000 - len(hostInfo.services) * 5 + int( hostInfo.hasUser == longTask )

			ret.sort(key=hostInfoSortKey)
		else:
			# Wake hosts before kicking off existing jobs, non-user hosts first
			def hostInfoSortKey(hostInfo):
				if hostInfo.sleeping:
					return int(hostInfo.hasUser)
				return 2 # Take into account assigned job's priorities
			ret.sort(key=hostInfoSortKey)
		
		# Limit
		return ret[:minLicensesAvail]
	
	def canReserveLicenses(self, hostInfo, jobAssign):
		ret = False
		hostUsingServices = ServiceList()
		for job in hostInfo.activeJobAssignments:
			hostUsingServices += self.servicesByJob[job]
		hostUsingServices = hostUsingServices.unique()
		for service in jobAssign.licensedServicesRequired:
			lic_tup = self.licCountByService[service]
			if lic_tup[1].perHost() and service in hostUsingServices:
				continue
			if lic_tup[0] <= 0:
				return False
		return True
	
	def takeLicenses(self, hostInfo, jobAssign):
		hostUsingServices = ServiceList()
		for job in hostInfo.activeJobAssignments:
			hostUsingServices += self.servicesByJob[job]
		hostUsingServices = hostUsingServices.unique()
		for service in jobAssign.licensedServicesRequired:
			lic_tup = self.licCountByService[service]
			if lic_tup[1].perHost() and service in hostUsingServices:
				continue
			lic_tup[0] -= 1
			if DEBUG_LICENSES:
				print "Assignment took license:", lic_tup[1].license(), ' new count:', lic_tup[0]

	def reserveNearlyFreeHost(self,hostInfo,jobAssign):
		if not hostInfo.nearlyFree:
			hostInfo.nearlyFree = True
			return True
		return False

	def jobListByServiceList( self, serviceList ):
		jobs = JobList()
		for service in serviceList:
			for job in self.jobsByService[service]:
				jobs += job
		return jobs.unique()
	
	def taskCountByServices( self, serviceList ):
		taskCount = 0
		for job in self.jobListByServiceList(serviceList):
			if job in self.jobsToAssign:
				taskCount += self.jobsToAssign[job].tasksUnassigned
		return taskCount
	
	def calculateFarmLoad( self ):
		if self.hostsActive + self.hostsReady == 0:
			return 0
		return (self.TotalUnassignedTasks + self.TotalAssignedTasks + self.hostsActive) / float(self.hostsActive + self.hostsReady)
		
	# Remove the job as a job that needs more hosts assigned
	def removeJob( self, job ):
		if job in self.jobsToAssign:
			del self.jobsToAssign[job]
	
	# Returns (assignedCount, tasksUnassigned)
	def assignSingleHost(self, jobAssign, hostInfo, weight, availableHosts):
		# This should already be checked, but should be free from selects, so do it again here
		if not jobAssign.hostOk( hostInfo ):
			raise NonCriticalAssignmentError("Host %s failed redundant jobAssign.hostOk check" % hostInfo.host.name())
		
		tasksAssigned = jobAssign.assignHost(hostInfo, availableHosts, self.taskCountByServices(jobAssign.servicesRequired))
	
		if tasksAssigned:
			throttler.assignLeft -= weight

		return tasksAssigned
	
	def assignSingleJob(self,jobAssign):
		#if VERBOSE_DEBUG: print 'job %s has sortKey %s' % (jobAssign.Job.name(), jobAssign.key_even_by_priority())
		
		# Check maxhosts
		if jobAssign.Job.maxHosts() > 0 and jobAssign.hostsOnJob >= jobAssign.Job.maxHosts():
			self.removeJob(jobAssign.Job)
			return False
		
		# Check assignment throttling
		weight = jobAssign.mapServerWeight()
		if weight > throttler.assignLeft:
			raise ThrottleLimitException()

		# Gather available hosts for this job
		if VERBOSE_DEBUG: print "Finding Hosts to Assign to %i tasks to Job %s with weight: %g" % (jobAssign.tasksUnassigned, jobAssign.Job.name(),weight)

		hostInfos = self.availableHosts( jobAssign )
		if not hostInfos:
			return False
		
		job = jobAssign.Job
		if jobAssign.allowUnassignment() and jobAssign.tasksUnassigned < jobAssign.JobStatus.maxHostTasks() - 1:
			jobAssign.unassign_tasks(1)
			
		if jobAssign.tasksUnassigned == 0:
			self.removeJob(jobAssign.Job)
			return False

		renderPowerAssigned = 0.0
		for hostInfo in hostInfos:
			try:
				tasksAssigned = self.assignSingleHost( jobAssign, hostInfo, weight, len(hostInfos) )
			except NonCriticalAssignmentError:
				if VERBOSE_DEBUG:
					traceback.print_exc()
				break
			
			# Return so that we can recalculate assignment priorities
			if tasksAssigned > 0:
				renderPowerAssigned += hostInfo.host.renderPower() or 1.0
				break
			
			# This shouldn't get hit, because there should be tasks available if we
			# are assigning, and we should already break above if they get assigned
			if jobAssign.tasksUnassigned <= 0:
				if VERBOSE_DEBUG:
					print "Job %s has tasksLeft <= 0 before any assignments made" % jobAssign.Job.name()
					break
		
		return renderPowerAssigned

	def performAssignmentsHelper(self, minPriority=None, checkProjectWeight=False):
		jobs = []
		# Assign from all ready jobs
		for jobAssign in self.jobsToAssign.itervalues():
			if (minPriority is None or jobAssign.Job.priority() <= minPriority) and jobAssign.checkNeedsMoreHosts(excessHosts = self.AssignExcessHosts):
				jobs.append( jobAssign )

		# We loop while there are hosts, and we are assigning hosts
		# Its possible to have free hosts that can't provide a service
		# for our current jobs
		while True:
			assignCount = 0
			jobs.sort()
			for jobAssign in jobs:
				if checkProjectWeight:
					job = jobAssign.Job
					if not job.applyProjectWeight():
						continue
					# Make sure that this project deserves more hosts
					fkeyproject = jobAssign.Job.getValue("fkeyproject").toInt()[0]
					if self.projectWeights.get( fkeyproject, 0 ) <= 0:
						continue
				# Recalc priority and resort job list after every assignment
				if self.assignSingleJob(jobAssign):
					assignCount += 1
					break
			if assignCount == 0:
				break
	
	def performHighPriorityAssignments(self):
		self.performAssignmentsHelper( minPriority = 20 )
		
	def performProjectWeightedAssignments(self):
		try:
			autoPacketTargetRestore = config._AUTOPACKET_TARGET
			config._AUTOPACKET_TARGET *= config._AUTOPACKET_TARGET_PROJECT_WEIGHT_SCALE
			self.performAssignmentsHelper( minPriority = 69, checkProjectWeight = True )
		finally:
			config._AUTOPACKET_TARGET = autoPacketTargetRestore
	
	def performAssignments(self):
		self.printProjectWeights()

		assignMethods = [
				(self.performHighPriorityAssignments, "high priority"),
				(self.performProjectWeightedAssignments, "project weighted"),
				(self.performAssignmentsHelper, "normal priority") ]
		
		try:
			for mode in range(2):
				self.AssignExcessHosts = mode == 1
				try:
					for assignMethod, methodName in assignMethods:
						try:
							timer.start()
							assignMethod()
						finally:
							if VERBOSE_DEBUG:
							    print "Finished assigning %s jobs, took %i" % (methodName, timer.elapsed())
					self.currentAssignmentsCompleted = True
				except AllJobsAssignedException:
					# Catch this exception, no need to print the time if this is the case
					if VERBOSE_DEBUG: print "All jobs have been assigned"
					self.currentAssignmentsCompleted = True
			
		except ThrottleLimitException, e:
			print "Assignment has been throttled: " + throttler.statusString()
		except AllHostsAssignedException:
			if VERBOSE_DEBUG: print "All relevant 'ready' hosts have been assigned"
			self.currentAssignmentsCompleted = True
		except Exception, e:
			# Print the time but let the exception pass through
			if VERBOSE_DEBUG: print "Unknown error while assigning"
			traceback.print_exc()
		
	# Clears assigned tasks from existing jobs that have a lower priority(higher number) than priority,
	# and are currently assigned to hosts that can handle jobType
	def unassign_hosts_by_services(self,jobAssign):
		job = jobAssign.Job
		debug = False
		if debug:
			print "Debugging job %s(%i) unassignment" % (job.name(), job.key())
		priority = job.priority()
		unassignBusy = (priority < 5)
		hostsNeeded = jobAssign.calculateNeededHosts()
		serviceList = jobAssign.servicesRequired
		licensedServices = jobAssign.licensedServicesRequired
		rationale = 'unassign_hosts_by_services, %i hosts needed for job %i %s, priority %i, services %s, licensedSevices %s' % \
			(hostsNeeded, job.key(), job.name(), priority, serviceList.services().join(','), licensedServices.services().join(','))
		if not len(serviceList):
			if debug: print "No services, can''t unassign"
			return 0
		job_fkeyproject = jobAssign.Job.getValue("fkeyproject").toInt()[0]
		ignoreProjectWeighting = (priority <= 20 or not job.applyProjectWeight())
		hostsUnassigned = 0
		
		for hostInfo in self.availableHosts(jobAssign,resourcesReady=False, debug=debug):
			if hostsUnassigned >= hostsNeeded:
				if debug: print "hostsUnassigned >= hostNeeded, we're done here"
				break
			
			if hostInfo.nearlyFree and not hostInfo.nearlyFreeReserved:
				if debug: print "reserved nearly free host %s" % hostInfo.host.name()
				hostInfo.nearlyFreeReserved = True
				hostsNeeded -= 1
				continue
			
			if hostInfo.sleeping:
				print 'Waking host', hostInfo.host.name()
				hostInfo.host.start()
				hostInfo.nearlyFree = hostInfo.nearlyFreeReserved = True
				continue
			
			activeJobs = hostInfo.activeJobAssignments.keys()
			
			# TODO: Support multiple assignments
			if len(activeJobs) != 1 or len(hostInfo.activeJobAssignments[activeJobs[0]]) > 1:
				if debug: print "Can't unassign from host %s, has %i assignments" % (hostInfo.host.name(), len(activeJobs))
				continue
			
			un_job = activeJobs[0]
			un_jobAssign = self.jobAssignByJob.get(un_job)
			un_jobAssignment = hostInfo.activeJobAssignments[un_job][0]
			if un_jobAssign is None:
				if debug: print "No jobAssign found for job %s" % un_job.name()
				continue
			
			un_jobStatus = un_jobAssign.JobStatus
			# Don't bother if there's no assigned tasks left to unassign
			if not unassignBusy and un_jobStatus.tasksAssigned() == 0:
				if debug: print "Not unassigning busy tasks, and no assigned tasks on job %s" % un_job.name()
				continue
			
			prioritizeOuterTasks = un_job.prioritizeOuterTasks()
			# Check to ensure we aren't unassigning the high-priority outer tasks assignment
			if prioritizeOuterTasks:
				q = Database.current().exec_( "SELECT min(jobTask), max(jobTask), count(*) FROM JobTask INNER JOIN JobTaskAssignment ON fkeyJobTask=keyJobTask AND fkeyJobTaskAssignment=keyJobTaskAssignment AND fkeyJobAssignment=%i" % un_jobAssignment )
				if q.next():
					ival = lambda i: q.value(i).toInt()[0]
					# Prioritized outer packet assignments only have 2 or 3 tasks, and always include the min and max
					if ival(2) <= 3 and ival(0) == un_job.minTaskNumber() and ival(1) == un_job.maxTaskNumber():
						if debug: print "Not unassigning a prioritized outer packet from host %s, job %s" % (hostInfo.host.name(), un_job.name())
						continue
			
			fkeyproject = un_job.getValue(Job.c.Project).toPyObject()
			target_priority = un_job.priority()
			
			# Check project weighting, make sure we have some
			if target_priority < 70 and not ignoreProjectWeighting and fkeyproject != job_fkeyproject and (job_fkeyproject not in self.projectWeights or self.projectWeights[job_fkeyproject] < 1):
				if debug: print "Not unassigning from host %s, job %s since we don't have project weighting" % (hostInfo.host.name(), un_job.name())
				continue
			
			# Make sure we don't unassign a job that will be reassigned first because it has project weight and we don't
			if ignoreProjectWeighting and priority > 20 and fkeyproject in self.projectWeights and self.projectWeights[fkeyproject] > 0:
				if debug: print "Not unassigning from host %s, job %s since it has project weighting" % (hostInfo.host.name(), un_job.name())
				continue

			# We need licenses, so check to see if the job is using the license we need
			if licensedServices:
				# All the services we need that have licenses and aren't going to become available from the unassigned job
				needLicensedServices = licensedServices - un_jobAssign.servicesRequired
				if needLicensedServices:
					if not self.canReserveLicenses(hostInfo, jobAssign):
						if debug or DEBUG_LICENSES: print "Not unassigning from host %s, job %s, because we can't get a license" % (hostInfo.host.name(), un_job.name())
						continue
					self.takeLicenses(hostInfo, jobAssign)
					
			# Double check that the computed priority is actually lower than the job we are getting hosts for
			if jobAssign >= un_jobAssign:
				if debug: print "Currently assigned job %s has higher priority" % un_job.name()
				continue

			host = hostInfo.host
			fkeyhost = host.key()
			if unassignBusy or target_priority >= 70:
				unassignedCount = unassign_host_busy(fkeyhost,un_job.key(),job)
			else:
				unassignedCount = unassign_host(fkeyhost,un_job.key())

			if unassignedCount >= 1:
				averageTaskTime = un_jobStatus.tasksAverageTime()
				if rationale:
					Log(rationale)
					rationale = None
				Log( "Unassigned %i assigned tasks from host %s job %s, priority %i average task time %i seconds" % (unassignedCount, host.name(), un_job.name(), target_priority, averageTaskTime) )
				hostInfo.nearlyFree = hostInfo.nearlyFreeReserved = True
				if not ignoreProjectWeighting and job_fkeyproject in self.projectWeights:
					self.projectWeights[job_fkeyproject] += 1
				hostsUnassigned += 1
				
		if hostsUnassigned > 0:
			Log( "Free'd up %i hosts for service list %s for job %s" % (hostsUnassigned, serviceList.services().join(','), job.name()) )
		return hostsUnassigned

	def postMigrateHosts(self):
		jobAssignList = self.jobsToAssign.values()
		jobAssignList.sort()
		
		# Unassign Hosts from lower priority jobs to work on higher priority jobs
		for jobAssign in jobAssignList:
			job = jobAssign.Job
			if not jobAssign.checkNeedsMoreHosts():
				continue
			self.unassign_hosts_by_services(jobAssign)
		
	def recordGraphiteStats(self, load, autoPacketTarget):
		if sendGraphiteStats:
			graphiteRecord( "renderfarm.load", load )
			graphiteRecord( "renderfarm.auto_packet_target", autoPacketTarget )

	def adjustServices(self, load):
		pcpService = Service('PCPreview')
		pcpLicense = pcpService.license()
		if pcpLicense.isRecord():
			try:
				pcpLoad = 0
				for job in self.jobsByService[pcpService]:
					try:
						ja = self.jobsToAssign[job]
					except:
						continue
					pcpLoad += ja.tasksUnassigned + ja.taskAssigned + ja.hostsOnJob
				pcpLoad /= float(self.hostsActive + self.hostsReady)
				pcpHosts = (pcpLoad / load) * (self.hostsActive + self.hostsReady)
				pcpHosts = min( 200, pcpHosts )
				pcpHosts = max( 30, pcpHosts )
				# Only update when the value has diverged more than 5 from the last update
				if abs(pcpLicense.total() - pcpHosts) > 5:
					pcpLicense.setTotal( pcpHosts ).commit()
					Database.current().exec_( "SELECT * FROM resetlicensecounts();" )
			except:
				pass
	
snapshot = None

# Single Instance
config = ManagerConfig()

throttler = AssignRateThrottler()
timer = QTime()

def serviceCheck():
	hostService.pulse()
	service.reload()
	return service.enabled() and hostService.enabled()

def runLoop(innerLoopCnt=10, innerLoopSleepTime=0.0, outerLoopSleepTime=1):
	global snapshot
	
	User.schema().setPreloadEnabled(True)
	
	while True:
		config.update()
		throttler.update()
		updateProjectTempo()

		if VERBOSE_DEBUG:
			print "Manager: Beginning Loop.  Assign Rate: %g Assign Period: %g Assign Left: %g" % ( float(throttler.assignRate), float(throttler.assignPeriod), float(throttler.assignLeft) )
	
		# We reuse the same snapshot instance because the nearlyfreehost entries
		# need to be kept until they become invalid
		if snapshot is None:
			snapshot = FarmResourceSnapshot()
		
		# Complete Job / Host snapshot
		#print "Starting job refresh"
		snapshot.refresh()
		snapshot.scaleProjectWeights()
		for i in range(innerLoopCnt):
			if i:
				#print "Starting host refresh"
				snapshot.refreshHostData()
			#print "Starting assignments"
			snapshot.performAssignments()
			if VERBOSE_DEBUG: print "Loop Assigning Jobs, took: %i, Waiting 1 second" % (timer.elapsed())
			time.sleep(innerLoopSleepTime)
		snapshot.performAssignments()
		snapshot.postMigrateHosts()
		load = snapshot.calculateFarmLoad()
		config.updateAutoPacketTarget( load )
		snapshot.adjustServices(load)
		snapshot.recordGraphiteStats( load, config._AUTOPACKET_TARGET )
		if not serviceCheck() or '-run-once' in sys.argv:
			return
		#Database.current().printStats()
		time.sleep(outerLoopSleepTime)

def manager():
	print "Manager: Starting up"
	if serviceCheck():
		try:
			if '-profile' in sys.argv:
				import cProfile
				cProfile.run('runLoop()')
			else:
				runLoop()
		# All exceptions except KeyboardInterrupt cause an automatic restart
		except KeyboardInterrupt:
			return
		except:
			import traceback
			traceback.print_exc()
	
	if not '-run-once' in sys.argv:
		os.execv(__file__,sys.argv)

if __name__=="__main__":
	manager()

