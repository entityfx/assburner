#!/bin/python

from blur.quickinit import *

db = Database.current()
#db.setEchoMode(db.EchoInsert|db.EchoUpdate|db.EchoSelect)

endDate = QDate.currentDate()
startDate = endDate.addDays(-100)
#startDate = QDate( 2014, 9, 19 )
#endDate = QDate( 2014, 9, 27 )

leftOver = Interval()

byProj = {}
while startDate < endDate:
	q = db.exec_("SELECT fkeyproject, count(*), interval_normalize(sum(coalesce(totaltasktime,'0'::interval) + coalesce(totalloadtime,'0'::interval) + coalesce(totalerrortime,'0'::interval) + coalesce(totalcopytime,'0'::interval))) as totaltime FROM jobstat WHERE date_trunc('day',ended) = ? GROUP BY fkeyproject", [startDate])
	total = Interval()
	while q.next():
		time = Interval.fromString(q.value(2).toPyObject())[0]
		jobCount = q.value(1).toInt()[0]
		fkeyproj = q.value(0).toInt()[0]
		if fkeyproj in byProj:
			oldVals = byProj[fkeyproj]
			byProj[fkeyproj] = (jobCount, time + oldVals[1])
		else:
			byProj[fkeyproj] = (jobCount, time)
		total += time
		print Project(fkeyproj).name(), time.toString()
	
	q = db.exec_("SELECT interval_normalize(sum(copytime+loadtime+busytime)),  sum(host.renderpower*(copytime+loadtime+busytime)) FROM hostdailystat INNER JOIN host ON host.keyhost=hostdailystat.fkeyhost WHERE date=?", [startDate])
	if q.next():
		if q.value(0).isNull():
			hostsTotal = total
		else:
			hostTotal = Interval.fromString(q.value(0).toPyObject())[0]
		if q.value(1).isNull():
			hostsTotalAdjusted = hostsTotal
		else:
			hostsTotalAdjusted = Interval.fromString(q.value(1).toPyObject())[0]
		avgRenderPower = hostsTotalAdjusted / hostTotal
	else:
		continue
	
	total += leftOver
	ratio = (hostTotal / total) * avgRenderPower
	leftOver = max(total - hostTotal,Interval(0))
	leftOverRatio = leftOver / total
	total = hostTotal
	
	print startDate.toString(), total.toString(), hostTotal.toString(), ratio
	for k,v in byProj.iteritems():
		if db.exec_("UPDATE jobstat_daily_summary SET totaltimestarted=? WHERE fkeyproject%s AND day=?" % (k and '=%i' % k or ' IS NULL'), [(v[1] * ratio).toString(),startDate]).numRowsAffected() == 0:
			db.exec_("INSERT INTO jobstat_daily_summary (totaltimestarted, fkeyproject, day) values (?, %s, ?)" % (k and str(k) or 'NULL'), [(v[1] * ratio).toString(),startDate])
		byProj[k] = (0,leftOver > 0 and v[1] * leftOverRatio or 0)
	startDate = startDate.addDays(1)
