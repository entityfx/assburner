#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          ab_host_kicker
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Assburner host kicker daemon
# Description:       
#                    
### END INIT INFO
# -*- coding: utf-8 -*-
# Debian init.d script for assburner host kicker
# Copyright © 2014 Matt Newell <newellm@blur.com>

DAEMON=/usr/bin/host_kicker.py
DAEMON_ARGS=
NAME=ab_host_kicker
DESC="assburner host kicker"

