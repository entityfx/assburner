
from optparse import OptionParser
import re, sys, os, subprocess

tables = """
client config department 
element user employee project
userrole usergroup group usermapping userpassword
permission phoneno *phonetype

*service software
license location
mapping mappingtype

eventlog eventlogtype

notification notificationcomponent notificationdestination notificationevent
*notificationmethod notificationroute notificationuserpref

filetracker rangefiletracker versionfiletracker

graph graphds graphpage graprelationship
graphitesaveddesc

hostgroup dynamichostgroup hostgroupitem

host hosthistory hostinterface *hostinterfacetype hostlicenseinstance hostload
hostmapping hostservice hostsoftware hoststatus

job
job3delight jobaftereffects jobautodeskburn jobbatch jobclientupdate
jobcinema4d jobmaxscript jobfumefxsim jobmax jobfusion jobhoudini
jobhoudiniscript jobmantra jobmaya jobnuke jobproxy jobrealflow
jobrenderman jobribgen jobshake jobsync jobvray jobvrayspawner
jobxsi jobxsiscript

jobstatus joboutput jobstat jobtask *jobtype jobtypemapping
jobdep jobmapping jobservice jobhistory

jobassignment *jobassignmentstatus jobtaskassignment jobcommandhistory
joberror joberrorhandler joberrorhandlerscript jobscreenshot

abdownloadstat hostdailystat

jobcannedbatch
"""

def buildOptionsParser():
	parser = OptionParser(usage="usage: %prog [options] outputFileName",add_help_option=False)
	parser.add_option( "-h", "--host",			dest="hostName", 		help="Host Name of the Database Server" )
	parser.add_option( "-d", "--dbname", 		dest="databaseName", 	help="Database Name" )
	parser.add_option( "-p", "--port",			dest="port",			help="Database port number" )
	parser.add_option( "-U", "--username",		dest="userName",		help="User name to connect as" )
	parser.add_option( "-P", "--password",		dest="password",		help="Password for database account" )
	parser.add_option( "-?", "--help",			dest="help", action="store_true", help="Show help" )
	return parser

def parseArgs():
	op = buildOptionsParser()
	(options, args) = op.parse_args()
	if options.help:
		op.print_help()
		return None
	return (options, args)
	
def dumpStarterDatabase( outputFileName, host, databaseName, userName, password, port = None ):
	tableNames = []
	dataTables = []
	for table in re.split('\s+',tables):
		if not table: continue
		if table.startswith('*'):
			table = table[1:]
			dataTables.append(table)
		tableNames.append(table)

	options = dict(
		tables = ' '.join(map(lambda t: '-t ' + t, tableNames)),
		databaseName = databaseName,
		outFile = outputFileName,
		userNameOpt = userName and '-U ' + userName or '',
		portOpt = port and '-p ' + str(port) or '',
		hostOpt = host and '-h ' + host or ''
	)
	
	if password:
		os.environ["PGPASSWORD"] = password
	
	cmd = 'pg_dump --schema-only %(tables)s %(userNameOpt)s %(portOpt)s %(hostOpt)s %(databaseName)s > %(outFile)s' % options
	print cmd
	subprocess.call(cmd,shell=True)
	
	if dataTables:
		options['tables'] = ' '.join(map(lambda t: '-t ' + t, dataTables))
		cmd = 'pg_dump --data-only %(tables)s %(userNameOpt)s %(portOpt)s %(hostOpt)s %(databaseName)s >> %(outFile)s' % options
		print cmd
		subprocess.call(cmd,shell=True)

if __name__=="__main__":
	options = parseArgs()
	if options is not None:
		options, args = options
		dumpStarterDatabase( args[0], options.hostName, options.databaseName, options.userName, options.password, options.port )

