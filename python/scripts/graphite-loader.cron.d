
# Ensure graphite is always running by checking the process list every 5 minutes and starting it if needed

*/5 * * * * root if ! ps -C graphite-loader > /dev/null; then /usr/local/bin/graphite-loader.py; fi
