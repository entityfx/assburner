#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          ab_rrd_stats_collector
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Assburner rrd stats collector
# Description:       
#                    
### END INIT INFO
# -*- coding: utf-8 -*-
# Debian init.d script for assburner rrd stats collector
# Copyright © 2014 Matt Newell <newellm@blur.com>

DAEMON=/usr/bin/rrd_stats_collector.py
DAEMON_ARGS=-daemonize
NAME=ab_rrd_stats_collector
DESC="assburner rrd stats collector"

