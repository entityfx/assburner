#!/lib/init/init-d-script-python
### BEGIN INIT INFO
# Provides:          ab_job_output_chown
# Required-Start:    $remote_fs $syslog $network
# Required-Stop:     $remote_fs $syslog $network
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Assburner job output chown daemon
# Description:       
#                    
### END INIT INFO
# -*- coding: utf-8 -*-
# Debian init.d script for assburner job output chown
# Copyright © 2014 Matt Newell <newellm@blur.com>

DAEMON=/usr/bin/job_output_chown.py
DAEMON_ARGS=-daemonize
NAME=ab_job_output_chown
DESC="assburner job output chown"

