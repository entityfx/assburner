
from collections import namedtuple
from optparse import OptionParser
from blur.quickinit import *
from blur.defaultdict import DefaultDict

PRC = ProjectResolution.c

Verbose = False
DryRun = False

JobStatSummary = namedtuple('JobStatSummary', 'fkeyproj, fkeypr, res, totalTime, jobCount')

def recomputeProject( project, projectResolutions, jobStatSummaries ):
	primary = None
	primaryRes = (0,0)
	totalTimes = DefaultDict(Interval)
	def findMatch(key_pr, res):
		match = projectResolutions.filter( PRC.Key == key_pr )
		if not match:
			match = projectResolutions.filter( (PRC.Width == res[0]) & (PRC.Height == res[1]) )
		return match and match[0] or primary
	# Put all of the matching AND non-marked resolutions into the highest primary-output
	for pr in projectResolutions:
		res = (pr.width(),pr.height())
		if pr.primaryOutput() and res > primaryRes:
			primary = pr
			primaryRes = res
	for jss in jobStatSummaries:
		pr = findMatch(jss.fkeypr,jss.res)
		if not pr:
			if Verbose:
				print "No match found for project resolution %i, (%ix%i) and no default primary resolution" % (jss.fkeypr, jss.res[0], jss.res[1])
			continue
		if Verbose:
			print "ProjectResolution %i (%ix%i) total time %s, job count %i" % (jss.fkeypr, jss.res[0], jss.res[1], jss.totalTime.toString(), jss.jobCount)
		totalTimes[pr] += jss.totalTime
	lines = []
	for pr, tt in totalTimes.iteritems():
		frames = pr.runLength().seconds() * pr.fps()
		pixels = frames * pr.width() * pr.height()
		if frames <= 0 or pixels <= 0:
			if Verbose:
				lines.append( "ProjectResolution %i (%ix%i) runLength %s, fps %s has invalid data"% (pr.key(), pr.width(), pr.height(), pr.runLength().toString(), pr.fps() ) )
			continue
		lines.append( "ProjectResolution %i (%ix%i) has %i frames, %i pixels, and has rendered %s per pixel" % (pr.key(), pr.width(), pr.height(), frames, pixels, (tt / float(pixels)).toString() ) )
		if not DryRun:
			pr.setPixelRenderTimeActual(  tt / float(pixels) )
			pr.commit()
	if lines:
		print( "Summary of %s" % project.name() + "\n\t" + "\n\t".join(lines) )
	elif Verbose:
		print( "Project %s has no valid data" % project.name() )
	

def recomputeProjectsHelper( q ):
	byProject = DefaultDict(list)
	while q.next():
		key_p = q.value(0).toInt()[0]
		key_pr = q.value(1).toInt()[0]
		res = (q.value(2).toInt()[0],q.value(3).toInt()[0])
		totalTime,parseSuccess = Interval.fromString(q.value(4).toString())
		if not parseSuccess:
			print( "Could not parse interval: " + q.value(3).toString() )
			continue
		jobCount = q.value(5).toInt()[0]
		byProject[key_p].append( JobStatSummary( key_p, key_pr, res, totalTime, jobCount ) )
	projects = Project.c.Key.in_(byProject.keys()).select()
	projectResolutions = PRC.Project.in_(projects).select()
	for pkey, sums in byProject.iteritems():
		project = Project(pkey)
		recomputeProject( Project(pkey), projectResolutions.filter( PRC.Project == project ), sums )

queryString = "SELECT fkeyproject, fkeyprojectresolution, framewidth, frameheight, sum(totaltasktime+totalerrortime+totalcopytime+totalloadtime), count(*) as jobCount FROM \
			jobstat%s GROUP BY fkeyproject, fkeyprojectresolution, framewidth, frameheight"

def recomputeProjects( projects ):
	recomputeProjectsHelper( Database.current().exec_( queryString % " WHERE fkeyproject IN (%s)" % projects.keyString() ) )
	
def recomputeAllProjects():
	recomputeProjectsHelper( Database.current().exec_( queryString % "") )
	
def buildOptionsParser():
	parser = OptionParser(usage="usage: %prog [options]\nComputes ProjectResolution.pixelRenderTimeActual")
	parser.add_option( "-p", "--recompute-project", 	dest="scanProjects", action="append",	default=[], help="Project Name. Recomputes for each project listed.  Can be used multiple times." )
	parser.add_option( "-k", "--recompute-project-key", dest="scanProjectKeys", action="append",	default=[], help="Project Name. Recomputes for each project listed.  Can be used multiple times." )
	parser.add_option( "-r", "--recompute-all", dest="recomputeAll", action="store_true", default=False, help="Recomputes all projects" )
	parser.add_option( "-d", "--dry-run", dest="dryRun", action="store_true", default=False, help="Computes and prints summaries but doesn't commit to the database." )
	parser.add_option( "-v", "--verbose", dest="verbose", action="store_true", default=False, help="Prints data about the JobStat records, in addition to the normal per project-resolution summary.")
	parser.add_option( "-m", "--modified-since", dest="modifiedSince", default=None, help="Pass an interval string and all projects that have jobs since the interval will be recomputed.")
	return parser

def main():
	parser = buildOptionsParser()
	parser.parse_args()
	opts = parser.values

	Database.current().setEchoMode( Database.EchoSelect )

	global Verbose, DryRun
	Verbose = opts.verbose
	DryRun = opts.dryRun
	modExp = Expression()

	if opts.modifiedSince:
		interval, parseSuccess = Interval.fromString( opts.modifiedSince )
		if not parseSuccess:
			print 'Error: "%s" is not a valid interval' % opts.modifiedSince
			return
		if interval < 0:
			interval = interval * -1.0
		modExp = Project.c.Key.in_( Expression.sql( "SELECT distinct(fkeyproject) FROM JobStat WHERE ended > now() - '%s'::interval" % interval.toString() ) )
		
	if opts.recomputeAll:
		recomputeAllProjects()
	else:
		projects = Project.select(Project.c.Name.in_(opts.scanProjects) | Project.c.Key.in_(opts.scanProjectKeys) | modExp)
		if projects:
			recomputeProjects(projects)
	
	
if __name__=="__main__":
	main()
