

from pprint import pprint
import re
import os
import glob
import sys
sys.path.append(r'C:\Program Files (x86)\Tweak\RV-3.12.18-32\lib\python2.6\site-packages')
sys.path.append(r'C:\Python26\Lib\site-packages')
sys.path.append(r'D:\src\shotgun-api3')
import rv.rvtypes
import rv.commands 
import rv.extra_commands
from pymu import MuSymbol

from PyQt4.QtGui import QWidget, QDockWidget, QApplication, QTreeWidgetItem
from PyQt4 import uic
import blurdev
import blurdev.gui



class PyRVImageLoader(rv.rvtypes.MinorMode):
	
	def __init__(self):
		rv.rvtypes.MinorMode.__init__(self)
		self.init('py-rvblur-mode',
				  [],
				  None,
				  [("Blur",
					[("ImageLoader", self.launch_imageloader, '', None)] )] )
		self.widget = None
						
	def launch_imageloader(self, event):
		parent = QApplication.activeWindow()
		widget = ImageLoaderWidget(parent)
		widget.show()
		self.widget = widget
		
	
	
			
def createMode():
	return PyRVImageLoader()



class ImageLoaderWidget(QDockWidget):
	
	root = 'S:\\'
	
	def support_path(self):
		rootdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		mode_name = os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0]
		return os.path.join(rootdir, 'SupportFiles', mode_name)
	
	def ui_path(self):
		mode_name = os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0]
		return os.path.join(self.support_path(), '%s.ui' % mode_name)
		
	def __init__(self, *args, **kwargs):
		super(ImageLoaderWidget, self).__init__(*args, **kwargs)
		uic.loadUi(self.ui_path(), self)
#		blurdev.gui.loadUi(__file__, self)
		self.setWindowTitle('RV ImageLoader')
		self.uiTREE.setHeaderLabels(['Shot'])		
		self.project_dict = {}
		self.sequence_dict = {}
		self.shot_dict = {}

		projects = self.get_projects()
		self.uiProjectDDL.clear()
		for project in projects:
			self.uiProjectDDL.addItem(project)			
			
		self.uiProjectDDL.activated.connect(self._uiProjectDDL_activated)
		self.uiSequenceDDL.activated.connect(self._uiSequenceDDL_activated)
		self.uiLoadBTN.clicked.connect(self._uiLoadBTN_clicked)
		self.uiClearBTN.clicked.connect(self._uiClearBTN_clicked)
	
	def get_projects(self):
		excluded = ['tank', 'temp_AUTODELETED', '__BACKUPS'] 
		return sorted([f for f in os.listdir(self.root) if f not in excluded])
	
	def get_sequences(self, project):
		path = os.path.join(self.root, project, 'Render')
		if not os.path.isdir(path):
			return []
		return sorted([f for f in os.listdir(path) if f.startswith('Sc')])
	
	def get_shots(self, project, sequence):
		path = os.path.join(self.root, project, 'Render', sequence)
		if not os.path.isdir(path):
			return []
		return sorted([f for f in os.listdir(path) if f.startswith('S')])
	
	def _uiProjectDDL_activated(self, arg):
		project = str(self.uiProjectDDL.currentText())
		sequences = self.get_sequences(project)
		self.uiSequenceDDL.clear()
		self.uiTREE.clear()
		for sequence in sequences:
			self.uiSequenceDDL.addItem(sequence)
	
	def _uiSequenceDDL_activated(self, arg):
		project = str(self.uiProjectDDL.currentText())
		sequence = str(self.uiSequenceDDL.currentText())
		shots = self.get_shots(project, sequence)
		self.uiTREE.clear()
		for shot in shots:
			item = QTreeWidgetItem(self.uiTREE.invisibleRootItem(), [shot])
			
	def _uiLoadBTN_clicked(self):
		template_dir = r'S:\%(project)s\Render\%(sequence)s\%(shot)s\AnimationRender'
		template_fn = r'%(shot)s_PointCachePreview'
		items = self.uiTREE.selectedItems()
		rv.commands.clearSession()
		for item in items:
			d = {}
			d['project'] = str(self.uiProjectDDL.currentText())
			d['sequence'] = str(self.uiSequenceDDL.currentText())
			d['shot'] = str(item.text(0))
			
			dirpath = template_dir % d
			fn = template_fn % d
			fp = os.path.join(dirpath, fn)
			globfp = fp + "[0-9][0-9][0-9][0-9]*"
			glob_fps = sorted(glob.glob(globfp))
			if not glob_fps:
				continue
			
			framenums = sorted([int(re.match(r'.+(\d\d\d\d)\.jpg', f).group(1)) for f in glob_fps])
			fmin = framenums[0]
			fmax = framenums[-1]
			token = "%s%s-%s#.jpg" % (fp, fmin, fmax)
			rv.commands.addSource(token, "")
			
	def _uiClearBTN_clicked(self):
		rv.commands.clearSession()
			
			
			
			
			
			
			
			