
import operator
import os
import sys
from pprint import pprint, pformat
import platform

if platform.architecture()[0] == '32bit':
	sys.path.append(r'C:\Python26\Lib\site-packages')
else:
	sys.path.append(r'C:\Python26_64\Lib\site-packages')

import rv.rvtypes
import rv.commands 
import rv.extra_commands
from pymu import MuSymbol

from PyQt4.QtGui import QWidget, QDockWidget, QApplication, QTreeWidgetItem, QPalette
from PyQt4.QtCore import Qt
from PyQt4 import uic


def createMode():
	return PyColorEditor()


class PyColorEditor(rv.rvtypes.MinorMode):
	
	def __init__(self):
		rv.rvtypes.MinorMode.__init__(self)
		self.init('py-rvblur-coloreditor-mode',
				  [],
				  None,
				  [("Blur",
					[("Color Editor", self.launch, '[', None)])])
		self.widget = None
						
	def launch(self, event):
		parent = QApplication.activeWindow()
		widget = ColorEditorWidget(parent)
		widget.show()
		widget.setFloating(True)
		self.widget = widget
		
		
			
class ColorEditorWidget(QDockWidget):
	
	global_defaults = {
		'active': True,
		'gamma': 1.0,
		'brightness': 0.0,
	}
	
	defaults = {
		'active': True,
		'exposure': 0.0,
		'gamma': 1.0,
		'hue': 0.0,
		'contrast': 0.0,
		'saturation': 1.0,
		'offset': 0.0,		
	}
	
	def __init__(self, *args, **kwargs):
		super(ColorEditorWidget, self).__init__(*args, **kwargs)
		uic.loadUi(self.ui_path(), self)
		self.setWindowTitle('Color Editor')
		palette = self.palette()
		palette.setColor(QPalette.Text, Qt.white)
		palette.setColor(QPalette.WindowText, Qt.white)
		self.setPalette(palette)
		self.uiLog.setPalette(palette)
		self.uiLog.setVisible(False)
		
		self.uiGlobalGRP.toggled.connect(self._set_global_active)
		self.uiGlobalGammaSPIN.valueChanged.connect(self._set_global_gamma)
		self.uiGlobalBrightnessSPIN.valueChanged.connect(self._set_global_brightness)
		
		self.uiSourceGRP.toggled.connect(self._set_active)
		self.uiExposureSPIN.valueChanged.connect(self._set_exposure)
		self.uiGammaSPIN.valueChanged.connect(self._set_gamma)
		self.uiHueSPIN.valueChanged.connect(self._set_hue)
		self.uiContrastSPIN.valueChanged.connect(self._set_contrast)
		self.uiSaturationSPIN.valueChanged.connect(self._set_saturation)
		self.uiColorOffsetSPIN.valueChanged.connect(self._set_offset)
		
		self.uiResetBTN.clicked.connect(self._reset_defaults)
		self.uiApplyToAllBTN.clicked.connect(self._apply_to_all_sources)
		
		rv.commands.bind("default", "global", "frame-changed", self._frame_changed, "")
#		rv.commands.bind("default", "global", "frame-changed", self._refresh_values, "")
		rv.commands.bind("default", "global", "media-relocated", self._refresh_values, "")
		rv.commands.bind("default", "global", "source-modified", self._refresh_values, "")
		rv.commands.bind("default", "global", "source-media-set", self._refresh_values, "")
		rv.commands.bind("default", "global", "new-source", self._refresh_values, "")
		self._refresh_values()
		self._start_wid = None
		self._start_pos = None
		self._delta_value = None
			
		for wid in [self.uiGlobalGammaSPIN,	
					self.uiGlobalBrightnessSPIN, 
					self.uiExposureSPIN, 
					self.uiGammaSPIN, 
					self.uiHueSPIN, 
					self.uiContrastSPIN, 
					self.uiSaturationSPIN, 
					self.uiColorOffsetSPIN]:
			wid.mousePressEvent = self._mousePressEventFactory(wid)
			wid.mouseMoveEvent = self._mouseMoveEvent
			wid.mouseReleaseEvent = self._mouseReleaseEvent
			
		
	def _mousePressEventFactory(self, wid):
		# Create unique mouse press event handlers for each Spin widget, it
		# acts as a closure for the widget handle, so that the same handler
		# can essentially be used for each spin widget.
		
		def _mousePressEvent(event):
			self._start_wid = wid
			self._start_pos = event.pos()
			self._start_value = wid.value()
			
		return _mousePressEvent
				
	def _mouseMoveEvent(self, event):
		if self._start_pos is None or self._start_wid is None:
			return
		delta_x = event.pos().x() - self._start_pos.x()
		self._delta_value = operator.truediv(delta_x, 100)
		self._start_wid.setValue(self._start_value + self._delta_value)
	
	def _mouseReleaseEvent(self, event):
		self._start_wid = None
		self._start_pos = None
		self._start_value = None
		self._delta_value = None
			
	def _frame_changed(self, *args, **kwargs):
#		self._display_info()
		self._refresh_values()
			
	def _reset_defaults(self):
		for name, value in self.defaults.items():
			prop_name = self._get_current_color_property_name(name)
			if prop_name:
				self._set_property(prop_name, value)
		
		for name, value in self.global_defaults.items():
			prop_name = self._get_global_color_property_name(name)
			if prop_name:
				self._set_property(prop_name, value)
		
		self._refresh_values()
		
	def _apply_to_all_sources(self):
		# Applies current color values to all sources in session		
		d = {
			'active': self.uiSourceGRP.isChecked(),
			'exposure': self.uiExposureSPIN.value(),
			'gamma': self.uiGammaSPIN.value(),
			'hue': self.uiHueSPIN.value(),
			'contrast': self.uiContrastSPIN.value(),
			'saturation': self.uiSaturationSPIN.value(),
			'offset': self.uiColorOffsetSPIN.value(),			
		}
		color_nodes = rv.commands.nodesOfType('RVColor')
		for node in color_nodes:
			for name, value in d.items():
				prop_name = '%s.color.%s' % (node, name)
				self._set_property(prop_name, value)
						
	def _refresh_values(self, *args, **kwargs):
		# Refreshes the UI with the current source color values
		self.uiGlobalGRP.blockSignals(True)
		self.uiGlobalGammaSPIN.blockSignals(True)
		self.uiGlobalBrightnessSPIN.blockSignals(True)
		self.uiSourceGRP.blockSignals(True)
		self.uiExposureSPIN.blockSignals(True)
		self.uiGammaSPIN.blockSignals(True)
		self.uiHueSPIN.blockSignals(True)
		self.uiContrastSPIN.blockSignals(True)
		self.uiSaturationSPIN.blockSignals(True)
		self.uiColorOffsetSPIN.blockSignals(True)
		
		self._refresh_current_values()
		self._refresh_global_values()
			
		self.uiGlobalGRP.blockSignals(False)
		self.uiGlobalGammaSPIN.blockSignals(False)
		self.uiGlobalBrightnessSPIN.blockSignals(False)
		self.uiSourceGRP.blockSignals(False)
		self.uiExposureSPIN.blockSignals(False)
		self.uiGammaSPIN.blockSignals(False)
		self.uiHueSPIN.blockSignals(False)
		self.uiContrastSPIN.blockSignals(False)
		self.uiSaturationSPIN.blockSignals(False)
		self.uiColorOffsetSPIN.blockSignals(False)
		
	def _refresh_current_values(self):
		cur_frame = rv.commands.frame()
		meta_eval_info = rv.commands.metaEvaluate(cur_frame, None, None)
		for meta_d in meta_eval_info:
			if meta_d.get('nodeType') == 'RVColor':
				prop_name_base = '%s.color.' % (meta_d['node'])
				self.uiSourceGRP.setChecked(bool(rv.commands.getIntProperty(prop_name_base + 'active', 0, 1)[0]))
				self.uiExposureSPIN.setValue(rv.commands.getFloatProperty(prop_name_base + 'exposure', 0, 1)[0])
				self.uiGammaSPIN.setValue(rv.commands.getFloatProperty(prop_name_base + 'gamma', 0, 1)[0])
				self.uiHueSPIN.setValue(rv.commands.getFloatProperty(prop_name_base + 'hue', 0, 1)[0])
				self.uiContrastSPIN.setValue(rv.commands.getFloatProperty(prop_name_base + 'contrast', 0, 1)[0])
				self.uiSaturationSPIN.setValue(rv.commands.getFloatProperty(prop_name_base + 'saturation', 0, 1)[0])
				self.uiColorOffsetSPIN.setValue(rv.commands.getFloatProperty(prop_name_base + 'offset', 0, 1)[0])
				return
		
		# If no properties are found, use the defaults
		self.uiSourceGRP.setChecked(self.defaults['active'])
		self.uiExposureSPIN.setValue(self.defaults['exposure'])
		self.uiGammaSPIN.setValue(self.defaults['gamma'])
		self.uiHueSPIN.setValue(self.defaults['hue'])
		self.uiContrastSPIN.setValue(self.defaults['contrast'])
		self.uiSaturationSPIN.setValue(self.defaults['saturation'])
		self.uiColorOffsetSPIN.setValue(self.defaults['offset'])
			
	def _refresh_global_values(self):
		nodes = rv.commands.nodesOfType('RVDisplayColor')
		if nodes:
			prop_name_base = '%s.color.' % (nodes[0])
			self.uiGlobalGRP.setChecked(bool(rv.commands.getIntProperty(prop_name_base + 'active', 0, 1)[0]))
			self.uiGlobalGammaSPIN.setValue(rv.commands.getFloatProperty(prop_name_base + 'gamma', 0, 1)[0])
			self.uiGlobalBrightnessSPIN.setValue(rv.commands.getFloatProperty(prop_name_base + 'brightness', 0, 1)[0])
			return
		
		# If no properties, use the defaults
		self.uiGlobalGRP.setChecked(self.global_defaults['active'])
		self.uiGlobalGammaSPIN.setValue(self.global_defaults['gamma'])
		self.uiGlobalBrightnessSPIN.setValue(self.global_defaults['brightness'])				
					
	def _set_global_active(self, value):
		prop_name = self._get_global_color_property_name('active')
		if prop_name:
			self._set_property(prop_name, int(self.uiGlobalGRP.isChecked()))
	
	def _set_global_gamma(self, value):
		prop_name = self._get_global_color_property_name('gamma')
		if prop_name:
			self._set_property(prop_name, self.uiGlobalGammaSPIN.value())
	
	def _set_global_brightness(self, value):
		prop_name = self._get_global_color_property_name('brightness')
		if prop_name:
			self._set_property(prop_name, self.uiGlobalBrightnessSPIN.value())

	def _set_active(self, value):
		prop_name = self._get_current_color_property_name('active')
		if prop_name:
			self._set_property(prop_name, int(self.uiSourceGRP.isChecked()))
					
	def _set_exposure(self, value):
		prop_name = self._get_current_color_property_name('exposure')
		if prop_name:
			self._set_property(prop_name, self.uiExposureSPIN.value())
		
	def _set_gamma(self, value):
		prop_name = self._get_current_color_property_name('gamma')
		if prop_name:
			self._set_property(prop_name, self.uiGammaSPIN.value())
	
	def _set_hue(self, value):
		prop_name = self._get_current_color_property_name('hue')
		if prop_name:
			self._set_property(prop_name, self.uiHueSPIN.value())
	
	def _set_contrast(self, value):
		prop_name = self._get_current_color_property_name('contrast')
		if prop_name:
			self._set_property(prop_name, self.uiContrastSPIN.value())
	
	def _set_saturation(self, value):
		prop_name = self._get_current_color_property_name('saturation')
		if prop_name:
			self._set_property(prop_name, self.uiSaturationSPIN.value())
	
	def _set_offset(self, value):
		prop_name = self._get_current_color_property_name('offset')
		if prop_name:
			self._set_property(prop_name, self.uiColorOffsetSPIN.value())
		
	def _get_current_color_property_name(self, name):
		# Gets the first RVColor node for the current frame and returns the
		# property specified by "name"
		cur_frame = rv.commands.frame()
		meta_eval_info = rv.commands.metaEvaluate(cur_frame, None, None)
		for meta_d in meta_eval_info:
			if meta_d.get('nodeType') == 'RVColor':
				return '%s.color.%s' % (meta_d['node'], name)
		return None
	
	def _get_global_color_property_name(self, name):
		# Gets the first RVDisplayColor node for the current frame and returns the
		# property specified by "name"
		nodes = rv.commands.nodesOfType('RVDisplayColor')
		if nodes:
			node = nodes[0]
			return '%s.color.%s' % (node, name)
		return None
			
	def _set_property(self, name, value):
		if isinstance(value, bool):
			value = int(value)
		prop_info = rv.commands.propertyInfo(name)
		width = prop_info['width']
		prop_value = [value] * width
		if isinstance(value, int):
			rv.commands.setIntProperty(name, prop_value, False)
		else:
			rv.commands.setFloatProperty(name, prop_value, False)
		
							
	def support_path(self):
		rootdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		mode_name = os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0]
		return os.path.join(rootdir, 'SupportFiles', mode_name)
	
	def ui_path(self):
		mode_name = os.path.splitext(os.path.basename(os.path.abspath(__file__)))[0]
		return os.path.join(self.support_path(), '%s.ui' % mode_name)
	
	
	def _display_info(self, *args, **kwargs):
		txt = []
		
#		nodes = rv.commands.nodes()
#		nodes_ = [(n, rv.commands.nodeType(n)) for n in nodes]
#		txt.append('Nodes: \n%s' % pformat(nodes_))
		
		cur_frame = rv.commands.frame()
		
		txt.append('MetaColor:')
		meta_eval_info = rv.commands.metaEvaluate(cur_frame, None, None)
		for meta_d in meta_eval_info:
			if meta_d.get('nodeType') == 'RVColor':
				txt.append(pformat(meta_d)) 
				properties = rv.commands.properties(meta_d['node'])
				txt.append('Prop: \n%s' % pformat(properties)) 
		
		
		sources = rv.commands.sourcesAtFrame(cur_frame)	
		if sources:
			source = sources[0]
			if source.endswith('_source'):
				for name in ['active']:
					prop_name  = source[:-7] + '_color.color.' + name
					prop_value = rv.commands.getIntProperty(prop_name, 0, 1)
					prop_info = rv.commands.propertyInfo(prop_name)
					txt.append('%s: %s' % (prop_name, prop_value))
					txt.append('INFO: %s' % str(prop_info))
				for name in ['exposure', 'gamma', 'offset', 'contrast', 'saturation', 'hue']:
					prop_name  = source[:-7] + '_color.color.' + name
					prop_value = rv.commands.getFloatProperty(prop_name, 0, 1)
					prop_info = rv.commands.propertyInfo(prop_name)
					txt.append('%s: %s' % (prop_name, prop_value))
					txt.append('INFO: %s' % str(prop_info))
				
		
		txt = '\n'.join(txt)		
		self.uiLog.setPlainText(txt)

	
	