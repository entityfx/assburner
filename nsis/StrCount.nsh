!include LogicLib.nsh

!define StrCount "!insertmacro StrCount"
 
!macro StrCount str look si
    Push `${str}`
    Push `${look}`
    Push `${si}`
	;takes the following parameters by the stack:
	; case sensitive ('s') or insensitive
    ; string to lookup
    ; string where to search
 
	Exch $2	;Stack = ($2 test str)
    Exch	;Stack = (test $2 str)
    Exch $1	;Stack = ($1 $2 str)
    Exch	;Stack = ($2 $1 str)
    Exch 2	;Stack = (str $1 $2)
    Exch $0	;Stack = ($0 $1 $2)
    Exch 2	;Stack = ($2 $1 $0) just to pop in natural order
    Push $3
    Push $4
    Push $5	
    Push $6	;Stack = ($6 $5 $4 $3 $2 $1 $0)
 
	StrLen $4 $1    
    StrCpy $5 0
    StrCpy $6 0
 
    ;now $0=str, $1=test, $2=s/i, $3=tmp str, $4=lookup len, $5=index, $6=count
 
    loop:
    StrCpy $3 $0 $4 $5
    StrCmp $3 "" end
    ${if} $2 == 's'
    	StrCmpS $3 $1 count ignore
    ${else}
    	StrCmp $3 $1 count ignore
    ${endif}
    count:
    IntOp $6 $6 + 1	;count++
    ignore:
    IntOp $5 $5 + 1 ;index++
	goto loop
	end:
 
    Exch 6	;Stack = ($0 $5 $4 $3 $2 $1 $6)
	Pop $0
    Pop $5
    Pop $4
    Pop $3
    Pop $2
    Pop $1	;Stack = ($6)
    Exch $6	;count is on top stack
!macroend
