; ReplaceSubStr.nsh
; This script is derived of a script Written by dirtydingus :
; "Another String Replace (and Slash/BackSlash Converter)" 
;
; for more information please see :
; http://nsis.sourceforge.net/Another_String_Replace_(and_Slash/BackSlash_Converter)

!ifndef REPLACESUBSTR_FUNCTION
!define REPLACESUBSTR_FUNCTION

!include StrFunc.nsh

; ---- Fix for conflict if StrFunc.nsh is already includes in main file -----------------------
; Code Pulled from EnvVarUpdate. Added here in case EnvVarUpdate is not imported
; Make sure you import EnvVarUpdate.nsh before importing this file.
!ifmacrondef _IncludeStrFunction
!macro _IncludeStrFunction StrFuncName
  !ifndef ${StrFuncName}_INCLUDED
    ${${StrFuncName}}
  !endif
  !ifndef Un${StrFuncName}_INCLUDED
    ${Un${StrFuncName}}
  !endif
  !ifndef un.${StrFuncName}
	!define un.${StrFuncName} "${Un${StrFuncName}}"
  !endif
!macroend
!endif

!insertmacro _IncludeStrFunction StrRep

Var MODIFIED_STR

!macro ReplaceSubStr OLD_STR SUB_STR REPLACEMENT_STR
 
	Push "${OLD_STR}" ;String to do replacement in (haystack)
	Push "${SUB_STR}" ;String to replace (needle)
	Push "${REPLACEMENT_STR}" ; Replacement
	Call StrRep
	Pop $R0 ;result
	StrCpy $MODIFIED_STR $R0
 
!macroend

!macro un.ReplaceSubStr OLD_STR SUB_STR REPLACEMENT_STR
 
	Push "${OLD_STR}" ;String to do replacement in (haystack)
	Push "${SUB_STR}" ;String to replace (needle)
	Push "${REPLACEMENT_STR}" ; Replacement
	Call un.StrRep
	Pop $R0 ;result
	StrCpy $MODIFIED_STR $R0
 
!macroend
 !endif