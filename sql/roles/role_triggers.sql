

--
-- UsrGrp triggers --
--

CREATE OR REPLACE FUNCTION check_add_remove_membership(ug usrgrp, is_insert boolean) RETURNS VOID AS $$
DECLARE
	g grp;
	u usr;
BEGIN
	SELECT INTO g * FROM grp WHERE keygrp=ug.fkeygrp;
	IF g.databaseRole THEN
		SELECT INTO u * FROM usr WHERE keyelement=ug.fkeyusr;
		IF u.databaseRole AND u.disabled=0 THEN
			IF is_insert THEN
				EXECUTE 'GRANT ' || g.grp || ' TO ' || u.name;
			ELSE
				EXECUTE 'REVOKE ' || g.grp || ' FROM ' || u.name;
			END IF;
		END IF;
	END IF;
END
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION user_group_insert() RETURNS trigger AS $$
DECLARE
BEGIN
	PERFORM check_add_remove_membership(NEW,True);
	RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION user_group_delete() RETURNS trigger AS $$
DECLARE
BEGIN
	PERFORM check_add_remove_membership(OLD,False);
	
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS user_group_insert_trigger ON usrgrp;
CREATE TRIGGER user_group_insert_trigger AFTER INSERT ON usrgrp FOR EACH ROW
EXECUTE PROCEDURE user_group_insert();

DROP TRIGGER IF EXISTS user_group_delete_trigger ON usrgrp;
CREATE TRIGGER user_group_delete_trigger AFTER DELETE ON usrgrp FOR EACH ROW
EXECUTE PROCEDURE user_group_delete();



--
-- Grp Triggers --
--

CREATE OR REPLACE FUNCTION group_insert() RETURNS trigger AS $$
BEGIN
	EXECUTE 'CREATE ROLE ' || NEW.grp;
	RETURN NULL; -- Fired after
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS group_insert_trigger ON grp;
CREATE TRIGGER group_insert_trigger AFTER INSERT ON grp FOR EACH ROW
WHEN (NEW.databaseRole IS TRUE)
EXECUTE PROCEDURE group_insert();

CREATE OR REPLACE FUNCTION group_delete() RETURNS trigger AS $$
BEGIN
	EXECUTE 'DROP ROLE ' || OLD.grp;
	RETURN NULL; -- Fired after
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS group_delete_trigger ON grp;
CREATE TRIGGER group_delete_trigger AFTER DELETE ON grp FOR EACH ROW
WHEN (OLD.databaseRole IS TRUE)
EXECUTE PROCEDURE group_delete();

CREATE OR REPLACE FUNCTION group_update() RETURNS trigger AS $$
DECLARE
	u usr;
BEGIN
	IF OLD.databaseRole IS TRUE THEN
		EXECUTE 'DROP ROLE ' || OLD.grp;
	END IF;
	IF NEW.databaseRole IS TRUE THEN
		EXECUTE 'CREATE ROLE ' || NEW.grp;
		FOR u IN SELECT * FROM usr WHERE databaseRole=true and keyelement IN (SELECT fkeyusr FROM usrgrp WHERE fkeygrp=NEW.keygrp) LOOP
			EXECUTE 'GRANT ' || NEW.grp || ' TO ' || u.name;
		END LOOP;
	END IF;
	RETURN NULL; -- Fired after
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS group_update_trigger ON grp;
CREATE TRIGGER group_update_trigger AFTER UPDATE ON grp FOR EACH ROW
WHEN (NEW.databaseRole IS DISTINCT FROM OLD.databaseRole)
EXECUTE PROCEDURE group_update();



--
-- Usr Triggers --
--

CREATE OR REPLACE FUNCTION user_insert() RETURNS trigger AS $$
BEGIN
	IF NEW.databaseRole IS TRUE AND NEW.disabled=0 THEN
		EXECUTE 'CREATE ROLE ' || NEW.name || ' WITH LOGIN';
	END IF;
	IF NEW.password IS NOT NULL THEN
		NEW.encryptedpassword := crypt(NEW.password,gen_salt('bf',8));
		INSERT INTO UserPassword (fkeyuser, password) VALUES (NEW.keyelement, NEW.password);
	END IF;
	RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS user_insert_trigger ON usr;
CREATE TRIGGER user_insert_trigger BEFORE INSERT ON usr FOR EACH ROW 
EXECUTE PROCEDURE user_insert();

DROP TRIGGER IF EXISTS employee_insert_trigger ON employee;
CREATE TRIGGER employee_insert_trigger BEFORE INSERT ON employee FOR EACH ROW 
EXECUTE PROCEDURE user_insert();

CREATE OR REPLACE FUNCTION user_delete() RETURNS trigger AS $$
BEGIN
	IF OLD.databaseRole AND OLD.disabled=0 THEN
		EXECUTE 'DROP ROLE ' || OLD.name;
	END IF;
	DELETE FROM UserPassword WHERE fkeyuser=OLD.keyelement;
	RETURN OLD;
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS user_delete_trigger ON usr;
CREATE TRIGGER user_delete_trigger AFTER DELETE ON usr FOR EACH ROW
EXECUTE PROCEDURE user_delete();

DROP TRIGGER IF EXISTS employee_delete_trigger ON employee;
CREATE TRIGGER employee_delete_trigger AFTER DELETE ON employee FOR EACH ROW
EXECUTE PROCEDURE user_delete();

CREATE OR REPLACE FUNCTION user_update() RETURNS trigger AS $$
DECLARE
	g grp;
	up userpassword;
	create_role_str text;
BEGIN
	-- If we need to drop or create the database role
	IF (OLD.databaseRole IS TRUE AND OLD.disabled=0) != (NEW.databaseRole IS TRUE AND NEW.disabled=0) THEN
		IF OLD.databaseRole IS TRUE AND OLD.disabled=0 THEN
			EXECUTE 'DROP ROLE ' || OLD.name;
		END IF;
		IF NEW.databaseRole IS TRUE AND NEW.disabled=0 THEN
			create_role_str := 'CREATE ROLE ' || NEW.name || ' WITH LOGIN';
			SELECT INTO up * FROM UserPassword WHERE fkeyuser=NEW.keyelement;
			IF FOUND THEN
				create_role_str := create_role_str || ' ENCRYPTED PASSWORD ''' || up.password || '''';
			END IF;
			EXECUTE create_role_str;
			FOR g IN SELECT * FROM grp WHERE databaserole=true AND keygrp IN (SELECT fkeygrp FROM usrgrp WHERE fkeyusr=NEW.keyelement) LOOP
				EXECUTE 'GRANT ' || g.grp || ' TO ' || NEW.name;
			END LOOP;
		END IF;
	END IF;

	-- If we need to update userpassword
	IF OLD.password IS DISTINCT FROM NEW.password THEN
		IF NEW.password IS NULL THEN
			DELETE FROM UserPassword WHERE fkeyuser=NEW.keyelement;
			NEW.encryptedpassword := NULL;
		ELSE
			NEW.encryptedpassword := crypt(NEW.password,gen_salt('bf',8));
			UPDATE UserPassword SET password=NEW.password WHERE fkeyuser=NEW.keyelement;
			IF NOT FOUND THEN
				INSERT INTO UserPassword (fkeyuser,password) VALUES (NEW.keyelement, NEW.password);
			END IF;
		END IF;
	END IF;
	RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS user_update_trigger ON usr;
CREATE TRIGGER user_update_trigger BEFORE UPDATE ON usr FOR EACH ROW
WHEN (
	NEW.databaseRole IS DISTINCT FROM OLD.databaseRole
	OR NEW.disabled IS DISTINCT FROM OLD.disabled
	OR NEW.password IS DISTINCT FROM OLD.password )
EXECUTE PROCEDURE user_update();

DROP TRIGGER IF EXISTS employee_update_trigger ON employee;
CREATE TRIGGER employee_update_trigger BEFORE UPDATE ON employee FOR EACH ROW
WHEN (
	NEW.databaseRole IS DISTINCT FROM OLD.databaseRole
	OR NEW.disabled IS DISTINCT FROM OLD.disabled
	OR NEW.password IS DISTINCT FROM OLD.password )
EXECUTE PROCEDURE user_update();


--
-- UserPassword Triggers --
--

CREATE OR REPLACE FUNCTION user_password_insert_update() RETURNS trigger AS $$
DECLARE
	u usr;
BEGIN
	SELECT INTO u * FROM usr WHERE keyelement=NEW.fkeyuser;
	IF FOUND THEN
		IF u.databaseRole THEN
			EXECUTE 'ALTER ROLE ' || u.name || ' ENCRYPTED PASSWORD ''' || NEW.password || '''';
		END IF;
	END IF;
	RETURN NEW;
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS user_password_insert_trigger ON userpassword;
CREATE TRIGGER user_password_insert_trigger AFTER INSERT OR UPDATE ON userpassword FOR EACH ROW
EXECUTE PROCEDURE user_password_insert_update();

CREATE OR REPLACE FUNCTION user_password_delete() RETURNS trigger AS $$
DECLARE
	u usr;
BEGIN
	SELECT INTO u * FROM usr WHERE keyelement=OLD.fkeyuser;
	IF FOUND THEN
		EXECUTE 'ALTER ROLE ' || u.name || ' ENCRYPTED PASSWORD ''''';
	END IF;
	RETURN OLD;
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS user_password_delete_trigger ON userpassword;
CREATE TRIGGER user_password_delete_trigger AFTER DELETE ON userpassword FOR EACH ROW
EXECUTE PROCEDURE user_password_delete();


--
-- Authentication Functions --
--

CREATE OR REPLACE FUNCTION authenticate_user(_name text, _password text) RETURNS boolean AS $$
DECLARE
	ret boolean;
BEGIN
	SELECT INTO ret u.encryptedpassword=crypt(_password, u.encryptedpassword) FROM usr u WHERE u.name=_name AND u.disabled=0;
	RETURN coalesce(ret,false);
END
$$ LANGUAGE 'plpgsql';
