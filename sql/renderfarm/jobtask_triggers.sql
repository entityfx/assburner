
-- Sets host.fkeyjobtask when a host begins working on a jobtask
-- Sets hosthistory.success when a task is completed
CREATE OR REPLACE FUNCTION update_JobTask() RETURNS trigger AS $$
DECLARE
BEGIN
	-- fkeyhost must be filled in for a task to be assigned or busy
	IF (NEW.fkeyhost IS NULL AND NEW.status IN ('assigned','busy')) THEN
		RETURN NULL;
	END IF;

	IF (NEW.status = 'done') THEN
		UPDATE hosthistory set success=true WHERE fkeyjob=NEW.fkeyjob AND fkeyjobtask=NEW.keyjobtask AND duration IS NULL;
	END IF;

	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION setup_jobtask_temp_table() RETURNS trigger AS $$
BEGIN
	BEGIN
		CREATE LOCAL TEMPORARY TABLE job_need_task_counts( fkeyjob int primary key ) ON COMMIT DROP;
	EXCEPTION WHEN duplicate_table THEN END;
	RETURN NULL;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION after_insert_jobtask() RETURNS trigger AS $$
BEGIN
	BEGIN
		EXECUTE 'INSERT INTO job_need_task_counts (fkeyjob) VALUES ($1)' USING NEW.fkeyjob;
	EXCEPTION WHEN unique_violation THEN END;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION after_update_jobtask() RETURNS trigger AS $$
DECLARE
	js RECORD;
	j RECORD;
BEGIN
	-- Update Job Counters when a tasks status changes
	IF (NEW.status IS DISTINCT FROM OLD.status) THEN
		
		BEGIN
			EXECUTE 'INSERT INTO job_need_task_counts (fkeyjob) VALUES ($1)' USING NEW.fkeyjob;
		EXCEPTION WHEN unique_violation THEN END;

		-- RAISE NOTICE 'job % added to temp table', NEW.fkeyjob;

		-- Check to see if we need to reset outerTasksAssigned because of an error or re-render
		IF (NEW.status = 'new') THEN
			SELECT INTO js * FROM jobstatus WHERE fkeyjob= NEW.fkeyjob;
			IF FOUND AND js.outerTasksAssigned=true THEN
				SELECT INTO j * FROM job WHERE keyjob = NEW.fkeyjob;
				IF FOUND AND (j.minTaskNumber = NEW.jobTask OR j.maxTaskNumber = NEW.jobTask OR (j.midTaskNumber IS NOT NULL AND j.midTaskNumber=NEW.jobTask) ) THEN
					UPDATE jobstatus SET outerTasksAssigned=false WHERE fkeyjob=NEW.fkeyjob;
				END IF;
			END IF;
		END IF;
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

-- Sets host.fkeyjobtask when a host begins working on a jobtask
-- Sets hosthistory.success when a task is completed
CREATE OR REPLACE FUNCTION after_update_stmt_jobtask() RETURNS trigger AS $$
DECLARE
	keyjob integer := 0;
	js jobstatus;
BEGIN

	FOR keyjob IN DELETE FROM job_need_task_counts RETURNING fkeyjob LOOP
		UPDATE JobStatus SET
			taskscount = job_counts.total_tasks,
			tasksUnassigned = coalesce((job_counts.status_counts->'new')::int,0),
			tasksAssigned = coalesce((job_counts.status_counts->'assigned')::int,0),
			tasksBusy = coalesce((job_counts.status_counts->'busy')::int,0),
			tasksDone = coalesce((job_counts.status_counts->'done')::int,0),
			tasksCancelled = coalesce((job_counts.status_counts->'cancelled')::int,0),
			tasksSuspended = coalesce((job_counts.status_counts->'suspended')::int,0),
			tasksHolding = coalesce((job_counts.status_counts->'holding')::int,0)
		FROM (
			SELECT fkeyjob, sum(count) as total_tasks, hstore(array_agg(status),array_agg(count::text)) AS status_counts FROM (
				SELECT fkeyjob, status, count(*) FROM jobtask WHERE fkeyjob=keyjob GROUP BY fkeyjob, status) AS status_group GROUP BY status_group.fkeyjob
			) AS job_counts
		WHERE JobStatus.fkeyjob=job_counts.fkeyjob;
	END LOOP;
	--RETURNING JobStatus.* LOOP
		--IF js.tasksCount = js.tasksDone + js.tasksCancelled THEN
		--	UPDATE Job SET status='done' WHERE Job.keyjob=js.fkeyjob;
		--END IF;
	--END LOOP;

	-- Because this may trigger more jobtask updates, which will cause this
	-- function to be re-entered, we must ensure that job_need_task_counts
	-- is empty when jobdep_update_task_dependents is called, otherwise
	-- infinite recursion will result.  Hence the delete returning.
	FOR keyjob IN DELETE FROM job_need_task_counts RETURNING fkeyjob LOOP
		PERFORM jobdep_update_task_dependents(keyjob);
	END LOOP;

	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

-- Replaced by update_job_initial_tasks_count, called by job_Update trigger

-- CREATE OR REPLACE FUNCTION insert_JobTask() RETURNS trigger AS $$
-- BEGIN
-- 	UPDATE JobStatus SET tasksCount = tasksCount + 1, tasksUnassigned = tasksUnassigned + 1 WHERE fkeyJob = NEW.fkeyjob;
-- RETURN NEW;
-- END;
-- $$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS setup_jobtask_temp_table ON jobtask;
CREATE TRIGGER setup_jobtask_temp_table
BEFORE INSERT OR UPDATE OR DELETE
ON jobtask
FOR EACH STATEMENT
EXECUTE PROCEDURE setup_jobtask_temp_table();

DROP TRIGGER IF EXISTS update_jobtask ON jobtask;
CREATE TRIGGER update_jobtask
BEFORE UPDATE ON jobtask
FOR EACH ROW EXECUTE PROCEDURE update_jobtask();

DROP TRIGGER IF EXISTS after_insert_jobtask ON jobtask;
CREATE TRIGGER after_insert_jobtask
AFTER INSERT ON jobtask
FOR EACH ROW EXECUTE PROCEDURE after_insert_jobtask();

DROP TRIGGER IF EXISTS after_update_jobtask ON jobtask;
CREATE TRIGGER after_update_jobtask
AFTER UPDATE ON jobtask
FOR EACH ROW EXECUTE PROCEDURE after_update_jobtask();

DROP TRIGGER IF EXISTS after_update_stmt_jobtask ON jobtask;
CREATE TRIGGER after_update_stmt_jobtask
AFTER INSERT OR UPDATE ON jobtask
FOR EACH STATEMENT EXECUTE PROCEDURE after_update_stmt_jobtask();


-- Replaced by update_job_initial_tasks_count, called by job_Update trigger

-- CREATE TRIGGER insert_JobTask
-- BEFORE INSERT
-- ON jobtask
-- FOR EACH ROW
-- EXECUTE PROCEDURE insert_JobTask();
DROP TRIGGER IF EXISTS insert_jobtask ON jobtask;

