

CREATE OR REPLACE FUNCTION reset_jobassignments_summary_fields() RETURNS VOID AS $$
BEGIN
	UPDATE JobAssignment SET
		donetasktime = iq.donetime,
		successtime = CASE 
			WHEN fkeyjobassignmentstatus IN (3,4) THEN COALESCE(COALESCE(ended,NOW())-started,'0'::interval)
			WHEN iq.donetime IS NOT NULL THEN COALESCE(firsttaskstarted-started,'0'::interval)+iq.donetime END,
		errortime = CASE
			WHEN fkeyjobassignmentstatus=5 THEN COALESCE(ended - started,'0'::interval) - COALESCE(iq.donetime,'0'::interval) ELSE '0'::interval END,
		canceltime = CASE 
			WHEN fkeyjobassignmentstatus=6 THEN COALESCE(ended - started,'0'::interval) - COALESCE(iq.donetime,'0'::interval) ELSE '0'::interval END
	FROM (SELECT 
		fkeyJobAssignment,
		SUM( CASE WHEN fkeyjobassignmentstatus=4 THEN COALESCE(ended-started,'0'::interval) WHEN fkeyjobassignmentstatus=3 AND started IS NOT NULL THEN now()-started ELSE '0'::interval END ) as donetime
		FROM JobTaskAssignment GROUP BY fkeyJobAssignment) AS iq
	WHERE
		iq.fkeyJobAssignment=keyJobAssignment;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION update_jobassignment_summary_fields( _keyjobassignment int ) RETURNS VOID AS $$
BEGIN
	UPDATE JobAssignment SET
		donetasktime = iq.donetime,
		successtime = CASE WHEN fkeyjobassignmentstatus IN (3,4) THEN COALESCE(ended,NOW())-started WHEN iq.donetime IS NOT NULL THEN firsttaskstarted-started+iq.donetime END,
		errortime = CASE WHEN fkeyjobassignmentstatus=5 THEN ended - started - COALESCE(iq.donetime,'0'::interval) ELSE '0'::interval END,
		canceltime = CASE WHEN fkeyjobassignmentstatus=6 THEN ended - started - COALESCE(iq.donetime,'0'::interval) ELSE '0'::interval END
	FROM (SELECT 
		SUM( CASE WHEN fkeyjobassignmentstatus=4 THEN ended-started WHEN fkeyjobassignmentstatus=3 AND started IS NOT NULL THEN now()-started ELSE '0'::interval END ) as donetime
		FROM JobTaskAssignment WHERE fkeyJobAssignment=_keyjobassignment) AS iq
	WHERE
		keyJobAssignment=_keyJobAssignment;
END;
$$ LANGUAGE 'plpgsql';

CREATE TYPE jobassignment_summary_return AS (keyjobassignment int, elapsed interval, load_time interval, fkeyjobassignmentstatus int,
	fkeyjoberror int, done_task_time interval, success_time interval, error_time interval, cancel_time interval );

CREATE OR REPLACE FUNCTION jobassignment_summaries( jobkey int ) RETURNS SETOF jobassignment_summary_return AS $$
DECLARE
BEGIN
	RETURN QUERY 
		SELECT keyJobAssignment,
			CASE WHEN started IS NOT NULL THEN coalesce(ended,now()) - started ELSE '0'::interval END AS elapsed,
			CASE WHEN firsttaskstarted IS NOT NULL THEN firsttaskstarted - started ELSE '0'::interval END AS loadtime,
			fkeyjobassignmentstatus,
			fkeyjoberror,
			donetasktime,
			CASE WHEN fkeyjobassignmentstatus IN (3,4) THEN coalesce(ended,now()) - started ELSE (
				CASE WHEN donetasktime IS NOT NULL THEN donetasktime+(firsttaskstarted-started) ELSE '0'::interval END) END AS success_time,
			errortime,
			canceltime
		FROM JobAssignment WHERE fkeyJob=jobkey;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobassignment_active_summaries( jobkey int ) RETURNS SETOF jobassignment_summary_return AS $$
DECLARE
BEGIN
	RETURN QUERY 
		SELECT keyJobAssignment,
			CASE WHEN started IS NOT NULL THEN coalesce(ended,now()) - started ELSE '0'::interval END AS elapsed,
			CASE WHEN firsttaskstarted IS NOT NULL THEN firsttaskstarted - started ELSE '0'::interval END AS loadtime,
			fkeyjobassignmentstatus,
			fkeyjoberror,
			donetasktime,
			now() - started AS success_time,
			errortime,
			canceltime
		FROM JobAssignment WHERE fkeyJob=jobkey AND fkeyjobassignmentstatus BETWEEN 1 AND 3;
END;
$$ LANGUAGE 'plpgsql';

CREATE TYPE job_summary_return AS (error_time interval, success_time interval, cancel_time interval, total_time interval, health float);

CREATE OR REPLACE FUNCTION job_summary( jobkey int ) RETURNS SETOF job_summary_return AS $$
DECLARE
BEGIN
	RETURN QUERY SELECT
		COALESCE(iq2.error_time,'0'::interval),
		COALESCE(iq1.success_time,'0'::interval) + COALESCE(iq2.success_time,'0'::interval),
		COALESCE(iq2.cancel_time,'0'::interval),
		COALESCE(iq1.total_time,'0'::interval) + COALESCE(iq2.total_time,'0'::interval),
		CASE WHEN iq2.error_time + iq1.success_time + iq2.success_time > '0'::interval THEN
			COALESCE((iq1.success_time+iq2.success_time) / (iq2.error_time+iq1.success_time+iq2.success_time),1.0)
			ELSE 1.0 END AS health
		FROM ( SELECT
			sum(COALESCE(success_time,'0'::interval)) AS success_time,
			sum(COALESCE(elapsed,'0'::interval)) AS total_time
			FROM
			jobassignment_active_summaries(jobkey) ) as iq1,
			( SELECT
			errortime as error_time,
			successtime as success_time,
			canceltime as cancel_time,
			totaltime as total_time
			FROM JobStatus WHERE fkeyJob=jobkey ) as iq2;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION reset_jobstatus_times() RETURNS VOID AS $$
DECLARE
BEGIN
	UPDATE JobStatus SET
		totaltime = iq.totaltime,
		successtime = iq.successtime,
		donetasktime = iq.donetasktime,
		errortime = iq.errortime,
		canceltime = iq.canceltime,
		loadtime = iq.loadtime
	FROM ( SELECT
		fkeyJob,
		SUM(CASE WHEN ended IS NOT NULL AND started IS NOT NULL THEN ended-started ELSE '0'::interval END) AS totaltime,
		SUM(successtime) AS successtime,
		SUM(donetasktime) AS donetasktime,
		SUM(errortime) AS errortime,
		SUM(canceltime) AS canceltime,
		SUM(CASE WHEN firsttaskstarted IS NOT NULL THEN firsttaskstarted-started ELSE '0'::interval END) AS loadtime
		FROM JobAssignment WHERE fkeyJobAssignmentStatus > 3 GROUP BY fkeyJob ) AS iq
	WHERE JobStatus.fkeyJob=iq.fkeyJob;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION job_compute_health( jobkey int ) RETURNS float AS $$
DECLARE
BEGIN
	RETURN (SELECT health FROM job_summary(jobkey));
END;
$$ LANGUAGE 'plpgsql';


CREATE TYPE renderfarm_summary_return AS (job_time interval, assignments int, load_time interval, load_error_time interval, load_errors int, task_time interval, task_assignments int, cancelled_tasks int, cancelled_task_time interval, done_tasks int, done_task_time interval, error_tasks int, error_task_time interval);

CREATE OR REPLACE FUNCTION renderfarm_summary( starttime timestamp, endtime timestamp ) RETURNS renderfarm_summary_return AS $$
DECLARE
	ret renderfarm_summary_return;
BEGIN
	SELECT INTO 
		ret.task_assignments, ret.task_time, ret.cancelled_tasks, ret.cancelled_task_time,
		ret.done_tasks, ret.done_task_time, ret.error_tasks, ret.error_task_time
		sum(task_count) as total_tasks, sum(duration) as total_time,
		sum(case when status=6 then task_count else 0 end) as cancelled_tasks,
		sum(case when status=6 then duration else '0'::interval end) as cancelled_time,
		sum(case when status=4 then task_count else 0 end) as done_tasks,
		sum(case when status in (3,4) then duration else '0'::interval end) as task_time,
		sum(case when status=5 then task_count else 0 end) as error_tasks,
		sum(case when status=5 then duration else '0'::interval end) as error_time
		FROM (
			select status, count(*) as task_count, sum(ended-started) as duration 
			from (
				select fkeyjobassignmentstatus as status, greatest(started,starttime) as started, least(endtime,ended) as ended
				from jobtaskassignment
				where coalesce(ended,endtime) >= starttime and started < endtime and started is not null) as iq group by status) as iq2;

	SELECT INTO ret.job_time, ret.assignments, ret.load_time, ret.load_error_time, ret.load_errors
		sum(ended-started) as job_time, count(*) as assignments, sum(load_ended-started) as load_time,
		sum(case when load_error=1 then load_ended-started else '0'::interval end) as load_error_time,
		sum(load_error) as load_errors
		from (
			select greatest(started,starttime::timestamp) as started,
				least(ended,endtime) as ended, greatest(least(firsttaskstarted,endtime,ended),starttime) load_ended,
				case when fkeyjobassignmentstatus=5 and firsttaskstarted is null then 1 else 0 end as load_error,
				fkeyjobassignmentstatus, firsttaskstarted
				from jobassignment 
				where started < endtime and coalesce(ended > starttime,true) and
				started is not null and (ended is not null or fkeyjobassignmentstatus between 1 and 3)) as iq;
	RETURN ret;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION update_single_job_health( j job ) RETURNS VOID AS $$
DECLARE
	job_health float;
BEGIN
	SELECT INTO job_health * FROM job_compute_health( j.keyjob );
	UPDATE jobstatus SET health = job_health where jobstatus.fkeyjob=j.keyjob;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION update_job_health_by_key( jobkey int ) RETURNS VOID AS $$
DECLARE
	job_health float;
BEGIN
	SELECT INTO job_health * FROM job_compute_health( jobkey );
	UPDATE jobstatus SET health = job_health where jobstatus.fkeyjob=jobkey;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION update_job_health() RETURNS VOID AS $$
DECLARE
	j job;
BEGIN
	FOR j IN SELECT * FROM job WHERE status='started' ORDER BY keyjob ASC LOOP
		PERFORM update_single_job_health( j );
	END LOOP;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION job_has_unfinished_deps( jobKey int ) RETURNS BOOLEAN AS $$
DECLARE
	j job;
BEGIN
	FOR j IN SELECT * FROM job WHERE keyJob IN (SELECT fkeydep FROM JobDep WHERE fkeyjob=jobKey) LOOP
		IF j.status NOT IN ('done','deleted','archived') THEN
			RETURN TRUE;
		END IF;
	END LOOP;
	RETURN FALSE;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION job_check_hold_dependents( jobKey int ) RETURNS VOID AS $$
DECLARE
	j job;
BEGIN
	FOR j IN SELECT * FROM job WHERE status IN ('ready','started') AND keyJob IN (SELECT fkeyjob FROM JobDep WHERE fkeydep=jobKey) LOOP
		IF job_has_unfinished_deps(j.keyJob) THEN
			UPDATE Job SET status='holding' WHERE keyJob=j.keyJob;
			UPDATE HostStatus SET slavestatus='starting' WHERE fkeyjob=j.keyJob;
			IF j.packettype = 'preassigned' THEN
				UPDATE JobTask SET status='new', startedts=NULL, endedts=NULL WHERE fkeyjob=j.keyJob;
			ELSE
				UPDATE JobTask SET status='new', startedts=NULL, endedts=NULL, fkeyhost=NULL WHERE status IN ('assigned','busy') AND fkeyjob=j.keyJob;
			END IF;
		END IF;
	END LOOP;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION job_check_start_dependents( jobKey int ) RETURNS VOID AS $$
DECLARE
	j job;
BEGIN
	FOR j in SELECT * FROM job WHERE status='holding' AND keyJob IN (SELECT fkeyjob FROM JobDep WHERE fkeydep=jobKey) LOOP
		IF NOT job_has_unfinished_deps(j.keyJob) THEN
			UPDATE Job SET status='ready' WHERE keyJob=j.keyJob;
		END IF;
	END LOOP;
END;
$$ LANGUAGE 'plpgsql';
