
CREATE OR REPLACE FUNCTION jobdep_tests() RETURNS BOOLEAN AS $$
DECLARE
	primary_job_1 job;
	primary_job_2 job;
	dep_job_1 job; -- Depends on primary_job_1(per task)
	dep_job_2 job; -- Depends on both primary_job_1(per task) and primary_job_2(per task)
	dep_job_3 job; -- Depends on primary_job_1(per job)
	job_keys int[];

BEGIN
	INSERT INTO job(job,status) VALUES ('primary_job_1','started') RETURNING * INTO primary_job_1;
	INSERT INTO job(job,status) VALUES ('primary_job_2','started') RETURNING * INTO primary_job_2;
	INSERT INTO job(job,status) VALUES ('dep_1','started') RETURNING * INTO dep_job_1;
	INSERT INTO job(job,status) VALUES ('dep_2','started') RETURNING * INTO dep_job_2;
	INSERT INTO job(job,status) VALUES ('dep_3','started') RETURNING * INTO dep_job_3;

	INSERT INTO JobTask(fkeyjob, status, jobtask) VALUES (primary_job_1.keyjob, 'new', 1), (primary_job_1.keyjob, 'new', 2);
	INSERT INTO JobTask(fkeyjob, status, jobtask) VALUES (primary_job_2.keyjob, 'new', 1), (primary_job_2.keyjob, 'new', 2);
	INSERT INTO JobTask(fkeyjob, status, jobtask) VALUES (dep_job_1.keyjob, 'new', 1), (dep_job_1.keyjob, 'new', 2);
	INSERT INTO JobTask(fkeyjob, status, jobtask) VALUES (dep_job_2.keyjob, 'new', 1), (dep_job_2.keyjob, 'new', 2);
	INSERT INTO JobTask(fkeyjob, status, jobtask) VALUES (dep_job_3.keyjob, 'new', 1), (dep_job_3.keyjob, 'new', 2);

	INSERT INTO JobDep(fkeyjob, fkeydep, deptype) VALUES
		(dep_job_1.keyjob, primary_job_1.keyjob, 2),
		(dep_job_2.keyjob, primary_job_1.keyjob, 2),
		(dep_job_2.keyjob, primary_job_2.keyjob, 2),
		(dep_job_3.keyjob, primary_job_1.keyjob, 1);
	
	IF EXISTS(SELECT 1 FROM JobTask WHERE status!='holding' AND fkeyJob=ANY(ARRAY[dep_job_1.keyjob,dep_job_2.keyjob])) THEN
		RAISE NOTICE 'Dependent job tasks not holding';
		RETURN FALSE;
	END IF;

	-- Check that dep_job_3's status is now holding
	IF EXISTS(SELECT 1 FROM Job WHERE status!='holding' AND keyJob=dep_job_3.keyjob) THEN
		RAISE NOTICE 'dep_job_3''s status did not go to holding when a job based dependency was added';
		RETURN FALSE;
	END IF;

	UPDATE jobtask SET status='done' WHERE jobtask=1 AND fkeyjob=primary_job_1.keyjob;

	IF EXISTS(SELECT 1 FROM JobTask WHERE status!='new' AND fkeyJob=dep_job_1.keyjob AND jobtask=1) THEN
		RAISE NOTICE 'Dependent task 1 of dep_job_1 did not change from holding to new';
		RETURN FALSE;
	END IF;

	UPDATE jobtask SET status='done' WHERE jobtask=1 AND fkeyjob=primary_job_2.keyjob;

	IF EXISTS(SELECT 1 FROM JobTask WHERE status!='new' AND fkeyJob=dep_job_2.keyjob AND jobtask=1) THEN
		RAISE NOTICE 'Dependent task 1 of dep_job_2 did not change from holding to new';
		RETURN FALSE;
	END IF;

	UPDATE jobtask SET status='done' WHERE jobtask=2 AND fkeyjob=primary_job_1.keyjob;

	-- Check that dep_job_1's dependent task is now marked new
	IF EXISTS(SELECT 1 FROM JobTask WHERE status!='new' AND fkeyJob=dep_job_1.keyjob AND jobtask=2) THEN
		RAISE NOTICE 'Dependent task 1 of dep_job_1 did not change from holding to new';
		RETURN FALSE;
	END IF;

	-- Check that primary_job_1's status is now done
	IF EXISTS(SELECT 1 FROM Job WHERE status!='done' AND keyJob=primary_job_1.keyjob) THEN
		RAISE NOTICE 'primary_job_1''s status did not go to done when all tasks were done';
		RETURN FALSE;
	END IF;

	-- Check that dep_job_3's status is now ready
	IF EXISTS(SELECT 1 FROM Job WHERE status!='ready' AND keyJob=dep_job_3.keyjob) THEN
		RAISE NOTICE 'dep_job_3''s status did not go to ready when it''s dependency completed';
		RETURN FALSE;
	END IF;

	job_keys := ARRAY[primary_job_1.keyjob, primary_job_2.keyjob, dep_job_1.keyjob, dep_job_2.keyjob];
	
	DELETE FROM Job WHERE keyJob=ANY(job_keys);
	DELETE FROM JobTask WHERE fkeyJob=ANY(job_keys);
	DELETE FROM JobDep WHERE fkeyJob=ANY(job_keys) OR fkeyDep=ANY(job_keys);
	RETURN TRUE;
END;
$$ LANGUAGE 'plpgsql';