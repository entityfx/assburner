
CREATE OR REPLACE FUNCTION jobassignment_test_1() RETURNS BOOLEAN AS $$
DECLARE
	test_host host;
	test_job job;
	jobtask_keys int[];
	jobassignment_key int;
BEGIN
	-- Prepare the host for assignment
	INSERT INTO host(host) VALUES ('test_host') RETURNING * INTO test_host;
	UPDATE HostStatus SET slaveStatus='ready' WHERE fkeyhost=test_host.keyhost;

	-- Prepare the job for assignment
	INSERT INTO job(job,status) VALUES ('test_job','ready') RETURNING * INTO test_job;

	WITH inserted(keyjobtask) AS (
		INSERT INTO JobTask(fkeyjob, status, jobtask) VALUES (test_job.keyjob, 'new', 1), (test_job.keyjob, 'new', 2)
		RETURNING keyJobTask
	)
	SELECT array_agg(keyjobtask) FROM inserted INTO jobtask_keys;

	SELECT INTO jobassignment_key * FROM create_job_assignment( test_job.keyjob, test_host.keyhost, jobtask_keys );

	-- Ensure JobTask records are 'assigned'
	IF (SELECT count(*) FROM JobTask WHERE status='assigned' AND keyJobTask=ANY(jobtask_keys)) != 2 THEN
		RAISE NOTICE 'JobTask records not at assigned status';
	END IF;

	PERFORM jobtaskassignments_completed( jobtaskassignments.keys ) FROM (
		SELECT array_agg(keyjobtaskassignment) keys FROM JobTaskAssignment
			WHERE fkeyJobAssignment=jobassignment_key
	) AS jobtaskassignments;

	-- Ensure JobTask records are 'done'
	IF (SELECT count(*) FROM JobTask WHERE status='done' AND keyJobTask=ANY(jobtask_keys)) != 2 THEN
		RAISE NOTICE 'JobTask records not at done status';
	END IF;

	-- Ensure JobTaskAssignment records are 'done'
	IF (SELECT count(*) FROM JobTaskAssignment WHERE fkeyjobassignmentstatus=4 AND fkeyJobTask=ANY(jobtask_keys)) != 2 THEN
		RAISE NOTICE 'JobTaskAssignment records not at done status';
	END IF;

	-- Ensure JobTask records are 'done'
	IF (SELECT count(*) FROM Job WHERE status='done' AND keyJob=test_job.keyjob) != 1 THEN
		RAISE NOTICE 'Job status did not go to done';
	END IF;

	UPDATE Job SET status='ready' WHERE keyJob=test_job.keyjob;

	IF (SELECT count(*) FROM Job WHERE keyJob=test_job.keyJob AND fkeyJobStat=test_job.fkeyJobStat) > 0 THEN
		RAISE NOTICE 'JobStat was not reset when job was reset';
	END IF;

	DELETE FROM Job WHERE keyJob=test_job.keyJob;
	DELETE FROM Host WHERE keyHost=test_host.keyHost;
	DELETE FROM JobTask WHERE fkeyJob=test_job.keyJob;
	WITH JobAssignment_deleted(keyJobAssignment) AS (
		DELETE FROM JobAssignment WHERE fkeyJob=test_job.keyJob RETURNING keyJobAssignment
	)
	DELETE FROM JobTaskAssignment WHERE fkeyJobAssignment IN (SELECT keyJobAssignment FROM JobAssignment_deleted);
	RETURN TRUE;
END;
$$ LANGUAGE 'plpgsql';
