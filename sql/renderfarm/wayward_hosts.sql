CREATE OR REPLACE FUNCTION get_wayward_hosts()
  RETURNS SETOF int4 AS
$BODY$
DECLARE
	hs hoststatus;
BEGIN
	FOR hs IN SELECT * FROM HostStatus WHERE slavestatus IS NOT NULL AND slavestatus NOT IN ('offline','restart','sleeping','waking','no-ping','stopping') AND now() - slavepulse > '10 minutes'::interval LOOP
		RETURN NEXT hs.fkeyhost;
	END LOOP;

	FOR hs in SELECT * FROM HostStatus WHERE slavestatus='assigned' AND now() - laststatuschange > '5 minutes'::interval LOOP
		RETURN NEXT hs.fkeyhost;
	END LOOP;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;

-- reason is 1 for laststatuschange, 2 for pulse
DROP TYPE IF EXISTS WaywardHostRet CASCADE;
CREATE TYPE WaywardHostRet AS ( keyhost int, reason int );

CREATE OR REPLACE FUNCTION get_wayward_hosts_2( pulse_period interval, loop_time interval )
  RETURNS SETOF WaywardHostRet AS
$BODY$
DECLARE
	hs hoststatus;
	ret WaywardHostRet;
	copy_timeout_seconds int;
BEGIN
	SELECT INTO copy_timeout_seconds value::int FROM config WHERE config='assburnerDownloadTimeoutSeconds';
	IF NOT FOUND THEN
		copy_timeout_seconds := 300;
	END IF;
	
	-- Give a host 15 minutes to wake up
	FOR hs IN SELECT * FROM HostStatus WHERE slavestatus='waking' AND now() - laststatuschange > '15 minutes'::interval LOOP
		ret := ROW(hs.fkeyhost,1);
		RETURN NEXT ret;
	END LOOP;

	-- 5 minutes for assburner to react to a status that requires an action
	FOR hs IN SELECT * FROM HostStatus WHERE (slavestatus IN ('stopping','starting','restart','assigned','sleep') AND now() - laststatuschange > '5 minutes'::interval)
	-- 10 minutes for a client update or a reboot
	OR (slavestatus IN ('reboot','client-update','client-update-offline') AND now() - laststatuschange > '10 minutes'::interval)
	-- Use the config variable for copy timeouts, plus a little buffer so that assburner can deal with it on its own
	OR (slavestatus = 'copy' AND interval_to_seconds(now() - laststatuschange + '20 seconds'::interval) > copy_timeout_seconds) LOOP
		ret := ROW(hs.fkeyhost,1);
		RETURN NEXT ret;
	END LOOP;

	FOR hs IN SELECT * FROM HostStatus WHERE slavestatus IS NOT NULL AND slavestatus IN ('ready','copy','busy','offline') AND (slavepulse IS NULL OR (now() - slavepulse) > (pulse_period + loop_time)) LOOP
		ret := ROW(hs.fkeyhost,2);
		RETURN NEXT ret;
	END LOOP;

	FOR hs IN SELECT * FROM HostStatus INNER JOIN JobAssignment ja ON ja.fkeyHost=hoststatus.fkeyhost AND ja.fkeyJobAssignmentStatus=ANY(ARRAY[1,2,3]) WHERE slavestatus IN ('assigned','copy','busy') AND ja.fkeyjob NOT IN (SELECT keyjob FROM job WHERE status IN ('ready','started')) LOOP
		ret := ROW(hs.fkeyhost,3);
		RETURN NEXT ret;
	END LOOP;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
