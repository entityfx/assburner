
-- stored proc and trigger to auto-update hoststatus.lastStatusChange
-- with NOW() whenever hoststatus.slavestatus changes
-- Also Performs inserts and updates to hosthistory when the slavestatus
-- or fkeyjobtask changes

CREATE OR REPLACE FUNCTION resetLicenseCounts() RETURNS VOID AS $$
DECLARE
	lic_status record;
BEGIN
	FOR lic_status IN 
		SELECT iq1.fkeylicense, coalesce(sum(iq2.instanceCount),0) as instanceCount, coalesce(sum(iq2.hostCount),0) as hostCount FROM 
			(SELECT distinct(s.fkeylicense) FROM service s WHERE fkeylicense IS NOT NULL) as iq1
		LEFT JOIN (
			SELECT s.fkeylicense, count(*) as instanceCount, count(distinct(ja.fkeyhost)) as hostCount
			FROM jobassignment ja
			INNER JOIN JobService js ON ja.fkeyjob=js.fkeyjob
			INNER JOIN Service s ON js.fkeyservice=s.keyservice 
			WHERE ja.fkeyjobassignmentstatus IN (1,2,3)
			AND s.fkeylicense IS NOT NULL
			GROUP BY s.fkeylicense
		) as iq2 ON iq1.fkeylicense=iq2.fkeylicense GROUP BY iq1.fkeylicense
	LOOP
		UPDATE License SET inUse = CASE WHEN perhost=true THEN lic_status.hostCount ELSE lic_status.instanceCount END, available=total-COALESCE(reserved,0)- CASE WHEN perhost=true THEN lic_status.hostCount ELSE lic_status.instanceCount END WHERE keylicense=lic_status.fkeylicense;
	END LOOP;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION updateJobLicenseCounts( _fkeyjob int, countChange int ) RETURNS VOID AS $$
DECLARE
BEGIN
	UPDATE License 
		SET inUse=greatest(0,coalesce(inUse,0) + countChange), available=available - countChange 
		WHERE keylicense IN (
			SELECT s.fkeylicense FROM Service s JOIN JobService js ON (s.keyService = js.fkeyService AND js.fkeyjob=_fkeyjob) WHERE fkeyLicense IS NOT NULL
		)
	;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION hoststatus_update() RETURNS trigger AS $$
DECLARE
BEGIN
	--IF NEW.slavestatus='busy' AND coalesce(OLD.fkeyjob,0)!=0 AND coalesce(NEW.fkeyjob,0)=0 THEN
		--RAISE EXCEPTION 'Clearing fkeyjob on busy hoststatus record keyhost: %, keyjob: %', NEW.fkeyhost, OLD.fkeyjob;
	--END IF;

	IF NEW.slavestatus IS DISTINCT FROM OLD.slaveStatus THEN
		-- Don't allow setting sleeping hosts to 'restart', 'offline', etc.
		IF OLD.slavestatus = 'sleeping' AND NEW.slavestatus NOT IN ('ready','waking','no-pulse','no-ping') THEN
			RETURN NULL;
		END IF;
		NEW.lastStatusChange = NOW();
	END IF;

	IF NEW.activeAssignmentCount=0 AND OLD.activeAssignmentCount > 0 AND NEW.slaveStatus IN ('assigned','busy') THEN
		NEW.slaveStatus:='ready';
	END IF;

	-- Temp work around for assburner order of operations issue (status -> 'ready' before the jobassignment is marked done)
	-- Automatically set to assigned status to prevent two hosthistory records, one for active assignment count 0 -> 1,
	-- and another when slaveStatus ready -> assigned
	IF NEW.slaveStatus='ready' AND NEW.activeAssignmentCount > 0 THEN
		IF OLD.slaveStatus='busy' THEN
			NEW.slaveStatus := 'busy';
		ELSIF OLD.slaveStatus='ready' THEN
			NEW.slaveStatus := 'assigned';
		END IF;
	END IF;

	
	IF (NEW.slavestatus IS DISTINCT FROM OLD.slaveStatus)
		OR (OLD.activeAssignmentCount != NEW.activeAssignmentCount)
	THEN
		UPDATE hosthistory SET duration = now() - datetime, nextstatus=NEW.slavestatus WHERE duration is null and fkeyhost=NEW.fkeyhost;
		INSERT INTO hosthistory (datetime,fkeyhost, status,laststatus, change_from_ip, activeassignmentcount)
		VALUES (now(),NEW.fkeyhost,NEW.slavestatus,OLD.slavestatus, inet_client_addr(),NEW.activeassignmentcount);
	END IF;

	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS hoststatus_update_trigger ON hoststatus;

CREATE TRIGGER hoststatus_update_trigger
BEFORE UPDATE
ON hoststatus
FOR EACH ROW
EXECUTE PROCEDURE hoststatus_update();

CREATE OR REPLACE FUNCTION resetActiveAssignmentCounts() RETURNS VOID AS $$
DECLARE
	hostAssignmentCount RECORD;
BEGIN
	FOR hostAssignmentCount IN SELECT fkeyhost, count(*) as activeAssignmentCount FROM JobAssignment WHERE fkeyjobassignmentstatus IN (1,2,3) GROUP BY fkeyhost LOOP
		UPDATE HostStatus SET activeAssignmentCount=hostAssignmentCount.activeAssignmentCount WHERE fkeyhost=hostAssignmentCount.fkeyhost;
	END LOOP;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';

