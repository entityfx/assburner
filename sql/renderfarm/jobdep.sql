

CREATE OR REPLACE FUNCTION jobdep_recursive(keylist text) RETURNS SETOF jobdep AS $$
BEGIN
	RETURN QUERY EXECUTE 
		'WITH RECURSIVE job_dep_rec AS ( 
			SELECT jobdep.* from jobdep WHERE fkeyjob IN (' || keylist || ')
			UNION 
			SELECT jd.* FROM job_dep_rec jdr, jobdep jd WHERE jd.fkeyjob=jdr.fkeydep
		) SELECT * FROM job_dep_rec';
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobdep_update_perjob(_fkeyjob int) RETURNS VOID AS $$
BEGIN
	UPDATE Job
		SET status = CASE WHEN iq.count > 0 THEN 'holding' ELSE 'ready' END
	FROM 
		(SELECT count(*) FROM Job WHERE status NOT IN ('done','deleted','archived') AND
			keyJob IN (SELECT fkeyDep FROM JobDep WHERE depType=1 AND fkeyJob=_fkeyjob))
		AS iq
	WHERE status IN ('ready','started','holding') AND keyJob=_fkeyjob;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobdep_update_pertask(_fkeyjob int) RETURNS VOID AS $$
BEGIN
	UPDATE JobTask jt
		SET
			status=CASE WHEN jt.jobTask=ANY(depends_on.pending) THEN 'holding' ELSE 'new' END
		FROM
			(SELECT array_agg(jtdo.jobtask) as pending FROM JobTask jtdo
				WHERE 
					fkeyJob = ANY(SELECT fkeyDep FROM JobDep WHERE depType=2 AND fkeyJob=_fkeyjob)
					AND status = ANY(ARRAY['new','verify','ready','started','holding','suspended'])
			) as depends_on
		WHERE status IN ('holding','new') AND fkeyjob=_fkeyjob;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobdep_update_task_dependents(_fkeyjob int) RETURNS VOID AS $$
DECLARE
	dependent_job_key int;
BEGIN
	FOR dependent_job_key IN SELECT fkeyjob FROM JobDep WHERE fkeydep=_fkeyjob AND depType=2 LOOP
		PERFORM jobdep_update_pertask(dependent_job_key);
	END LOOP;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobdep_update_job_dependents(_fkeyjob int) RETURNS VOID AS $$
DECLARE
	dependent_job_key int;
BEGIN
	FOR dependent_job_key IN SELECT fkeyjob FROM JobDep WHERE fkeydep=_fkeyjob AND depType=2 LOOP
		PERFORM jobdep_update_pertask(dependent_job_key);
	END LOOP;
END;
$$ LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION jobdep_insert() RETURNS trigger AS $$
DECLARE
BEGIN
	IF NEW.deptype=1 THEN
		PERFORM jobdep_update_perjob(NEW.fkeyJob);
	ELSIF NEW.deptype=2 THEN
		PERFORM jobdep_update_pertask(NEW.fkeyJob);
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobdep_update() RETURNS trigger AS $$
DECLARE
BEGIN
	IF OLD.deptype!=NEW.deptype THEN
		PERFORM jobdep_update_pertask(NEW.fkeyJob);
		PERFORM jobdep_update_perjob(NEW.fkeyJob);
	END IF;
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION jobdep_delete() RETURNS trigger AS $$
DECLARE
BEGIN
	IF OLD.deptype=1 THEN
		PERFORM jobdep_update_perjob(OLD.fkeyJob);
	ELSIF OLD.deptype=2 THEN
		PERFORM jobdep_update_pertask(OLD.fkeyJob);
	END IF;
	RETURN OLD;
END;
$$ LANGUAGE 'plpgsql';


DROP TRIGGER IF EXISTS jobdep_insert_trigger ON jobdep;
CREATE TRIGGER jobdep_insert_trigger
AFTER INSERT
ON jobdep
FOR EACH ROW
EXECUTE PROCEDURE jobdep_insert();

DROP TRIGGER IF EXISTS jobdep_update_trigger ON jobdep;
CREATE TRIGGER jobdep_update_trigger
AFTER UPDATE
ON jobdep
FOR EACH ROW
EXECUTE PROCEDURE jobdep_update();

DROP TRIGGER IF EXISTS jobdep_delete_trigger ON jobdep;
CREATE TRIGGER jobdep_delete_trigger
AFTER DELETE
ON jobdep
FOR EACH ROW
EXECUTE PROCEDURE jobdep_delete();
