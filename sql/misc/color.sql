
DROP TYPE IF EXISTS COLOR;

CREATE TYPE COLOR AS (
	r	int,
	g	int,
	b	int,
	a	int
);

CREATE OR REPLACE FUNCTION color_cmp(c1 color, c2 color) RETURNS int AS $$
DECLARE
	val int;
BEGIN
	val := c1.r + c1.g + c1.b + c1.a - (c2.r + c2.g + c2.b + c2.a);
	IF val = 0 THEN
		IF c1.r = c2.r AND c1.g = c2.g AND c1.b = c2.b AND c1.a = c2.a THEN
			RETURN 0;
		END IF;
		IF c1.r > c2.r THEN
			RETURN 1;
		ELSIF c1.r < c2.r THEN
			RETURN -1;
		END IF;
		IF c1.g > c2.g THEN
			RETURN 1;
		ELSIF c1.g < c2.g THEN
			RETURN -1;
		END IF;
		IF c1.b > c2.b THEN
			RETURN 1;
		ELSIF c1.b < c2.b THEN
			RETURN -1;
		END IF;
		IF c1.a > c2.a THEN
			RETURN 1;
		END IF;
		RETURN -1;
	END IF;
	RETURN val;
END;
$$ LANGUAGE 'PLPGSQL' IMMUTABLE;

CREATE OR REPLACE FUNCTION color_lt(c1 color, c2 color) RETURNS boolean AS $$
BEGIN
	RETURN color_cmp(c1,c2) < 0;
END;
$$ LANGUAGE 'PLPGSQL' IMMUTABLE;

CREATE OR REPLACE FUNCTION color_lte(c1 color, c2 color) RETURNS boolean AS $$
BEGIN
	RETURN color_cmp(c1,c2) <= 0;
END;
$$ LANGUAGE 'PLPGSQL' IMMUTABLE;

CREATE OR REPLACE FUNCTION color_gt(c1 color, c2 color) RETURNS boolean AS $$
BEGIN
	RETURN color_cmp(c1,c2) > 0;
END;
$$ LANGUAGE 'PLPGSQL' IMMUTABLE;

CREATE OR REPLACE FUNCTION color_gte(c1 color, c2 color) RETURNS boolean AS $$
BEGIN
	RETURN color_cmp(c1,c2) >= 0;
END;
$$ LANGUAGE 'PLPGSQL' IMMUTABLE;

CREATE OR REPLACE FUNCTION color_eq(c1 color, c2 color) RETURNS boolean AS $$
BEGIN
	RETURN color_cmp(c1,c2) = 0;
END;
$$ LANGUAGE 'PLPGSQL' IMMUTABLE;

CREATE OPERATOR > (
	leftarg = color,
	rightarg = color,
	procedure = color_gt
);

CREATE OPERATOR >= (
	leftarg = color,
	rightarg = color,
	procedure = color_gte
);

CREATE OPERATOR < (
	leftarg = color,
	rightarg = color,
	procedure = color_lt
);

CREATE OPERATOR <= (
	leftarg = color,
	rightarg = color,
	procedure = color_lte
);

CREATE OPERATOR = (
	leftarg = color,
	rightarg = color,
	procedure = color_eq
);

CREATE OPERATOR CLASS color_ops DEFAULT FOR TYPE color USING btree AS
	OPERATOR        1       < ,
	OPERATOR        2       <= ,
	OPERATOR        3       = ,
	OPERATOR        4       >= ,
	OPERATOR        5       > ,
	FUNCTION        1       color_cmp(color, color);

