--
-- PostgreSQL database cluster dump
--

\connect postgres

--
-- Roles
--

CREATE ROLE brobison;
ALTER ROLE brobison WITH SUPERUSER INHERIT NOCREATEROLE CREATEDB LOGIN;
CREATE ROLE jabberd;
ALTER ROLE jabberd WITH SUPERUSER INHERIT NOCREATEROLE CREATEDB LOGIN;
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN;






--
-- Database creation
--

CREATE DATABASE blur WITH TEMPLATE = template0 OWNER = postgres ENCODING = 'SQL_ASCII';
REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM postgres;
GRANT ALL ON DATABASE template1 TO postgres;


\connect blur

--
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: jabberd; Type: SCHEMA; Schema: -; Owner: jabberd
--

CREATE SCHEMA jabberd;


ALTER SCHEMA jabberd OWNER TO jabberd;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'Standard public schema';


--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: 
--

CREATE PROCEDURAL LANGUAGE plpgsql;


SET search_path = public, pg_catalog;

--
-- Name: hosthistory_status_percentages_return; Type: TYPE; Schema: public; Owner: brobison
--

CREATE TYPE hosthistory_status_percentages_return AS (
	status text,
	loading boolean,
	wasted boolean,
	total_time interval,
	total_perc double precision,
	non_idle_perc double precision,
	non_idle_non_wasted_perc double precision
);


ALTER TYPE public.hosthistory_status_percentages_return OWNER TO brobison;

--
-- Name: hosthistory_status_summary_return; Type: TYPE; Schema: public; Owner: brobison
--

CREATE TYPE hosthistory_status_summary_return AS (
	status text,
	loading boolean,
	wasted boolean,
	count integer,
	total_time interval,
	avg_time interval,
	max_time interval,
	min_time interval
);


ALTER TYPE public.hosthistory_status_summary_return OWNER TO brobison;

--
-- Name: hosthistory_user_slave_summary_return; Type: TYPE; Schema: public; Owner: brobison
--

CREATE TYPE hosthistory_user_slave_summary_return AS (
	usr text,
	host text,
	hours numeric
);


ALTER TYPE public.hosthistory_user_slave_summary_return OWNER TO brobison;

--
-- Name: job_gatherstats_result; Type: TYPE; Schema: public; Owner: brobison
--

CREATE TYPE job_gatherstats_result AS (
	mintasktime interval,
	avgtasktime interval,
	maxtasktime interval,
	totaltasktime interval,
	taskcount integer,
	minloadtime interval,
	avgloadtime interval,
	maxloadtime interval,
	totalloadtime interval,
	loadcount integer,
	minerrortime interval,
	avgerrortime interval,
	maxerrortime interval,
	totalerrortime interval,
	errorcount integer,
	mincopytime interval,
	avgcopytime interval,
	maxcopytime interval,
	totalcopytime interval,
	copycount integer,
	totaltime interval
);


ALTER TYPE public.job_gatherstats_result OWNER TO brobison;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: countercache; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE countercache (
    hoststotal integer,
    hostsactive integer,
    hostsready integer,
    jobstotal integer,
    jobsactive integer,
    jobsdone integer,
    lastupdated timestamp without time zone
);


ALTER TABLE public.countercache OWNER TO brobison;

--
-- Name: getcounterstate(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION getcounterstate() RETURNS countercache
    AS $$
	DECLARE
		cache countercache;
	BEGIN
		SELECT INTO cache * FROM countercache LIMIT 1;
		IF NOT FOUND THEN
			INSERT INTO countercache (hoststotal) values (null);
			cache.lastUpdated := now() - interval'1 year';
		END IF;
		IF now() - cache.lastUpdated > interval'10 seconds' THEN
			DECLARE
				hosts_total int;
				hosts_active int;
				hosts_ready int;
				jobs_total int;
				jobs_active int;
				jobs_done int;
			BEGIN
				SELECT count(*) INTO hosts_total FROM HostService INNER JOIN Host ON fkeyHost=keyHost WHERE fkeyService=23 AND online=1;
				SELECT count(keyHost) INTO hosts_active FROM Host WHERE slaveStatus IN('busy','copy', 'assigned') AND online=1;
				SELECT count(keyHost) INTO hosts_ready FROM Host WHERE slaveStatus='ready' AND online=1 and keyhost in (select fkeyhost from hostservice where fkeyservice in (select fkeyservice from jobtype));
				SELECT count(keyJob) INTO jobs_total FROM Job WHERE status IN('ready', 'busy', 'started','done');
				SELECT count(keyJob) INTO jobs_active FROM Job WHERE status IN ('ready', 'new', 'started');
				SELECT count(keyJob) INTO jobs_done FROM Job WHERE status='done';
				UPDATE CounterCache SET hoststotal=hosts_total, hostsactive=hosts_active, hostsReady=hosts_ready,
					jobsTotal=jobs_total, jobsActive=jobs_active, jobsDone=jobs_done, lastUpdated=now();
				SELECT INTO cache * FROM countercache LIMIT 1;
			END;
		END IF;
		RETURN cache;
	END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.getcounterstate() OWNER TO brobison;

--
-- Name: hosthistory_status_percentages(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION hosthistory_status_percentages(history_start timestamp without time zone, history_end timestamp without time zone) RETURNS SETOF hosthistory_status_percentages_return
    AS $$
DECLARE
	rec hosthistory_status_summary_return;
	ret hosthistory_status_percentages_return;
	total_time interval := '0 minutes';
	non_idle_time interval := '0 minutes';
	non_idle_non_wasted_time interval := '0 minutes';
BEGIN
	FOR rec IN SELECT * FROM hosthistory_status_summary(history_start,history_end) LOOP
		IF rec.total_time IS NOT NULL THEN
			total_time := total_time + rec.total_time;
			IF rec.status IN ('busy','copy','assigned') THEN
				non_idle_time := non_idle_time + rec.total_time;
				IF rec.wasted=false THEN
					non_idle_non_wasted_time := non_idle_non_wasted_time + rec.total_time;
				END IF;
			END IF;
		END IF;
	END LOOP;
	-- Can I keep the values from the first select and loop through them twice, seems i should be able to...
	FOR rec IN SELECT * FROM hosthistory_status_summary(history_start,history_end) LOOP
		ret.status := rec.status;
		ret.loading := rec.loading;
		ret.wasted := rec.wasted;
		ret.total_time := rec.total_time;
		ret.total_perc := rec.total_time / total_time;
		ret.non_idle_perc := rec.total_time / non_idle_time;
		ret.non_idle_non_wasted_perc := rec.total_time / non_idle_non_wasted_time;
		RETURN NEXT ret;
	END LOOP;
	RETURN;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.hosthistory_status_percentages(history_start timestamp without time zone, history_end timestamp without time zone) OWNER TO brobison;

--
-- Name: hosthistory_status_summary(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION hosthistory_status_summary(history_start timestamp without time zone, history_end timestamp without time zone) RETURNS SETOF hosthistory_status_summary_return
    AS $$
DECLARE
 rec hosthistory_status_summary_return;
BEGIN
	FOR rec IN SELECT 
		status,
		(fkeyjobtask is null and status='busy') as loading,
		status='busy' AND ((fkeyjobtask is null AND nextstatus!='busy' AND nextstatus IS NOT NULL) OR (fkeyjobtask IS NOT null AND success is null)) as error,
		count(*),
		sum(duration),
		avg(duration),
		max(duration),
		min(duration)
		FROM hosthistory_timespan_duration_adjusted(history_start,history_end)
		GROUP BY status, loading, error
		ORDER BY status LOOP
		RETURN NEXT rec;
	END LOOP;
	RETURN;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.hosthistory_status_summary(history_start timestamp without time zone, history_end timestamp without time zone) OWNER TO brobison;

--
-- Name: hosthistory; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE hosthistory (
    keyhosthistory serial NOT NULL,
    fkeyhost integer,
    fkeyjob integer,
    fkeyjobstat integer,
    status text,
    laststatus text,
    datetime timestamp without time zone,
    duration interval,
    fkeyjobtask integer,
    fkeyjobtype integer,
    nextstatus text,
    success boolean,
    fkeyjoberror integer
);


ALTER TABLE public.hosthistory OWNER TO brobison;

--
-- Name: hosthistory_timespan_duration_adjusted(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION hosthistory_timespan_duration_adjusted(time_start timestamp without time zone, time_end timestamp without time zone) RETURNS SETOF hosthistory
    AS $$
DECLARE
	rec hosthistory;
	dur interval;
BEGIN
	FOR rec IN SELECT
		*
		FROM HostHistory
		WHERE
			-- Starts inside the timespan
			(	(datetime > time_start)
			AND	(datetime < time_end))
			OR
			-- Ends inside the timespan
			(	(datetime + duration < time_end)
			AND (datetime + duration > time_start))
			OR
			-- Covors the entire timespan
			(	(datetime < time_start)
			AND (
					(datetime + duration > time_end)
				OR 	(duration IS NULL) )
			)
		LOOP
			dur := rec.duration;

			-- Account for hosthistory records that havent finished
			IF dur IS NULL THEN
				dur := now() - rec.datetime;
			END IF;

			-- Cut off front
			IF rec.datetime < time_start THEN
				dur := dur - (time_start - rec.datetime);
			END IF;
			
			-- Cut off the end
			IF rec.datetime + rec.duration > time_end THEN
				dur := dur - ((rec.datetime + rec.duration) - rec.datetime);
			END IF;

			rec.duration := dur;

			RETURN NEXT rec;
		END LOOP;
	RETURN;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.hosthistory_timespan_duration_adjusted(time_start timestamp without time zone, time_end timestamp without time zone) OWNER TO brobison;

--
-- Name: hosthistory_user_slave_summary(timestamp without time zone, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION hosthistory_user_slave_summary(history_start timestamp without time zone, history_end timestamp without time zone) RETURNS SETOF hosthistory_user_slave_summary_return
    AS $$
DECLARE
	ret hosthistory_user_slave_summary_return;
BEGIN
	FOR ret IN
		SELECT
			usr.name,
			host.host,
			(extract(epoch from sum) / 3600)::numeric(8,2) as totalHours
			FROM
				(SELECT
					sum(duration),
					fkeyhost
				FROM
					hosthistory_timespan_duration_adjusted(history_start,history_end) as hosthistory
				WHERE
					hosthistory.status in ('ready','busy','copy','assigned')
				GROUP BY fkeyhost)
				AS iq,
			host,
			usr
			WHERE
				host.keyhost=iq.fkeyhost
			AND usr.fkeyhost=host.keyhost
			ORDER BY sum desc
	LOOP
		RETURN NEXT ret;
	END LOOP;
	RETURN;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.hosthistory_user_slave_summary(history_start timestamp without time zone, history_end timestamp without time zone) OWNER TO brobison;

--
-- Name: increment_loadavgadjust(integer); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION increment_loadavgadjust(_fkeyhost integer) RETURNS void
    AS $$
DECLARE
	loadavgadjust_inc float;
BEGIN
	SELECT INTO loadavgadjust_inc value::float FROM config WHERE config='assburnerLoadAvgAdjustInc';
	UPDATE HostLoad SET loadavgadjust=loadavgadjust_inc+loadavgadjust WHERE fkeyhost=_fkeyhost;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.increment_loadavgadjust(_fkeyhost integer) OWNER TO brobison;

--
-- Name: insert_jobtask(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION insert_jobtask() RETURNS "trigger"
    AS $$
BEGIN
UPDATE Job SET tasksCount = tasksCount + 1, tasksUnassigned = tasksUnassigned + 1 WHERE keyJob = NEW.fkeyjob;
RETURN NEW;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.insert_jobtask() OWNER TO brobison;

--
-- Name: interval_divide(interval, interval); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION interval_divide(numerator interval, denominator interval) RETURNS double precision
    AS $$
BEGIN
	RETURN extract(epoch from numerator) / extract(epoch from denominator)::float;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.interval_divide(numerator interval, denominator interval) OWNER TO brobison;

--
-- Name: job_gatherstats(integer); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION job_gatherstats(_keyjob integer) RETURNS job_gatherstats_result
    AS $$
DECLARE
	ret Job_GatherStats_Result;
BEGIN
	SELECT INTO ret.mintasktime, ret.avgtasktime, ret.maxtasktime, ret.totaltasktime, ret.taskcount
				min(duration) as mintasktime,
				avg(duration) as avgtasktime,
				max(duration) as maxtasktime,
				sum(duration) as totaltasktime,
				count(*) as taskcount
			FROM hosthistory
			WHERE
				fkeyjob=_keyjob
				AND status='busy'
				AND fkeyjobtask IS NOT NULL
				AND success='true'::boolean;
		-- Load Times
	SELECT INTO ret.minloadtime, ret.avgloadtime, ret.maxloadtime, ret.totalloadtime, ret.loadcount
				min(duration) as minloadtime,
				avg(duration) as avgloadtime,
				max(duration) as maxloadtime,
				sum(duration) as totalloadtime,
				count(*) as loadcount
			FROM hosthistory
			WHERE
				fkeyjob=_keyjob
				AND status='busy'
				AND fkeyjobtask IS NULL
				AND nextstatus!='ready';
		-- Error Times
	SELECT INTO ret.minerrortime, ret.avgerrortime, ret.maxerrortime, ret.totalerrortime, ret.errorcount
				min(duration) as minerrortime,
				avg(duration) as avgerrortime,
				max(duration) as maxerrortime,
				sum(duration) as totalerrortime,
				count(*) as errorcount
			FROM hosthistory
			WHERE
				fkeyjob=_keyjob
				AND status='busy'
				AND (
					( fkeyjobtask IS NOT NULL AND (success!='true'::boolean OR success IS NULL) )
					OR (
						fkeyjobtask IS NULL
						AND nextstatus='ready'));
	SELECT INTO ret.mincopytime, ret.avgcopytime, ret.maxcopytime, ret.totalcopytime, ret.copycount
				min(duration) as mincopytime,
				avg(duration) as avgcopytime,
				max(duration) as maxcopytime,
				sum(duration) as totalcopytime,
				count(*) as copycount
			FROM hosthistory
			WHERE
				fkeyjob=_keyjob
				AND status='copy';
	ret.totaltime := '0 minutes'::interval;
	IF ret.totaltasktime IS NOT NULL THEN
		ret.totaltime := ret.totaltime + ret.totaltasktime;
	END IF;
	IF ret.totalloadtime IS NOT NULL THEN
		ret.totaltime := ret.totaltime + ret.totalloadtime;
	END IF;
	IF ret.totalerrortime IS NOT NULL THEN
		ret.totaltime := ret.totaltime + ret.totalerrortime;
	END IF;
	RETURN ret;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.job_gatherstats(_keyjob integer) OWNER TO brobison;

--
-- Name: job_update(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION job_update() RETURNS "trigger"
    AS $$
DECLARE
	stat RECORD;
BEGIN
	IF (NEW.fkeyjobstat IS NOT NULL) THEN
		IF (NEW.status='started' AND OLD.status='ready') THEN
			UPDATE jobstat SET started=NOW() WHERE keyjobstat=NEW.fkeyjobstat AND started IS NULL;
		END IF;
		IF (NEW.status='ready' AND OLD.status='done') THEN
			SELECT INTO stat * from jobstat where keyjobstat=NEW.fkeyjobstat;
			INSERT INTO jobstat (fkeyelement, fkeyproject, frameCount, fkeyusr, started, pass) VALUES (stat.fkeyelement, stat.fkeyproject, stat.frameCount, stat.fkeyusr, NOW(), stat.pass);
			SELECT INTO NEW.fkeyjobstat currval("jobstat_keyjobstat_seq");
		END IF;
		IF (NEW.status IN ('done','deleted') AND OLD.status IN ('ready','started') ) THEN
			UPDATE jobstat
			SET
				ended=NOW(),
				errorcount=iq.errorcount,
				totaltasktime=iq.totaltasktime, mintasktime=iq.mintasktime, maxtasktime=iq.maxtasktime, avgtasktime=iq.avgtasktime,
				totalloadtime=iq.totalloadtime, minloadtime=iq.minloadtime, maxloadtime=iq.maxloadtime, avgloadtime=iq.avgloadtime,
				totalerrortime=iq.totalerrortime, minerrortime=iq.minerrortime, maxerrortime=iq.maxerrortime, avgerrortime=iq.avgerrortime,
				totalcopytime=iq.totalcopytime, mincopytime=iq.mincopytime, maxcopytime=iq.maxcopytime, avgcopytime=iq.avgcopytime
			FROM (select * FROM Job_GatherStats(NEW.keyjob)) as iq
			WHERE keyjobstat=NEW.fkeyjobstat;
		END IF;
	END IF;
	RETURN NEW;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.job_update() OWNER TO brobison;

--
-- Name: joberror_inc(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION joberror_inc() RETURNS "trigger"
    AS $$
DECLARE
	j RECORD;
BEGIN
	SELECT INTO j * FROM job WHERE keyjob=NEW.fkeyjob;
	IF (j.fkeyjobstat IS NOT NULL) THEN
		UPDATE jobstat SET errorcount=errorcount+1 WHERE keyjobstat=j.fkeyjobstat;
	END IF;
	RETURN NULL;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.joberror_inc() OWNER TO brobison;

--
-- Name: plpgsql_call_handler(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION plpgsql_call_handler() RETURNS language_handler
    AS '$libdir/plpgsql', 'plpgsql_call_handler'
    LANGUAGE c;


ALTER FUNCTION public.plpgsql_call_handler() OWNER TO brobison;

--
-- Name: update_host(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION update_host() RETURNS "trigger"
    AS $$
DECLARE
currentjob job;
BEGIN
IF (NEW.slavestatus IS NOT NULL)
 AND (NEW.slavestatus != OLD.slavestatus) THEN
	NEW.lastStatusChange = NOW();
	IF (NEW.slavestatus != 'busy') THEN
		NEW.fkeyjobtask = NULL;
	END IF;
END IF;

-- Increment Job.hostsOnJob
--IF (NEW.fkeyjob IS NOT NULL AND (NEW.fkeyjob != OLD.fkeyjob OR OLD.fkeyjob IS NULL) ) THEN
--	UPDATE Job SET hostsOnJob=hostsOnJob+1 WHERE keyjob=NEW.fkeyjob;
--END IF;

-- Decrement Job.hostsOnJob
--IF (OLD.fkeyjob IS NOT NULL AND (OLD.fkeyjob != NEW.fkeyjob OR NEW.fkeyjob IS NULL) ) THEN
--	UPDATE Job SET hostsOnJob=hostsOnJob-1 WHERE keyjob=OLD.fkeyjob;
--END IF;

IF (NEW.slavestatus IS NOT NULL)
 AND ((NEW.slavestatus != OLD.slavestatus) OR (NEW.fkeyjobtask != OLD.fkeyjobtask) OR ((OLD.fkeyjobtask IS NULL) != (NEW.fkeyjobtask IS NULL))) THEN
	SELECT INTO currentjob * from job where keyjob=NEW.fkeyjob;
	UPDATE hosthistory SET duration = now() - datetime, nextstatus=NEW.slavestatus WHERE duration is null and fkeyhost=NEW.keyhost;
	INSERT INTO hosthistory (datetime,fkeyhost,fkeyjob,fkeyjobstat,status,laststatus,fkeyjobtype,fkeyjobtask) values (now(),NEW.keyhost,NEW.fkeyjob,currentjob.fkeyjobstat,NEW.slavestatus,OLD.slavestatus,currentjob.fkeyjobtype,NEW.fkeyjobtask);
END IF;
RETURN NEW;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.update_host() OWNER TO brobison;

--
-- Name: update_hostload(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION update_hostload() RETURNS "trigger"
    AS $$
DECLARE
	seconds float;
BEGIN
	IF NEW.loadavg != OLD.loadavg THEN
		IF OLD.loadavgadjusttimestamp IS NULL THEN
			seconds := 1;
		ELSE
			seconds := extract(epoch FROM (NOW() - OLD.loadavgadjusttimestamp)::interval);
		END IF;
		IF seconds > 0 THEN
			-- 20 Second Half-Life
			NEW.loadavgadjust = OLD.loadavgadjust / ( 1.0 + (seconds * .05) );
			IF NEW.loadavgadjust < .01 THEN
				NEW.loadavgadjust = 0.0;
			END IF;
			NEW.loadavgadjusttimestamp = NOW();
		END IF; 

		-- Remove this once all the clients are updated
		UPDATE Host SET loadavg=NEW.loadavg + NEW.loadavgadjust WHERE keyhost=NEW.fkeyhost;

	END IF;
	RETURN NEW;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.update_hostload() OWNER TO brobison;

--
-- Name: update_hostservice(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION update_hostservice() RETURNS "trigger"
    AS $$
DECLARE
BEGIN
IF (NEW.pulse != OLD.pulse) OR (NEW.pulse IS NOT NULL AND OLD.pulse IS NULL) THEN
	UPDATE Host Set slavepulse=NEW.pulse WHERE keyhost=NEW.fkeyhost;
END IF;
RETURN NEW;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.update_hostservice() OWNER TO brobison;

--
-- Name: update_jobtask(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION update_jobtask() RETURNS "trigger"
    AS $$
DECLARE
	unassigned_change integer := 0;
	busy_change integer := 0;
	assigned_change integer := 0;
	done_change integer := 0;
	cancelled_change integer := 0;
	suspended_change integer := 0;
BEGIN
	IF (NEW.status = 'busy') THEN
		UPDATE host set fkeyjobtask=NEW.keyjobtask WHERE keyhost=NEW.fkeyhost AND (fkeyjobtask IS NULL OR fkeyjobtask!=NEW.keyjobtask);
	END IF;

	IF (NEW.status = 'done') THEN
		UPDATE hosthistory set success=true WHERE fkeyjob=NEW.fkeyjob AND fkeyjobtask=NEW.keyjobtask AND duration IS NULL;
	END IF;

	-- Reset Host if someone changes the status of a busy frame
	IF (OLD.status = 'busy' AND NEW.status IN ('new','suspended','cancelled')) THEN
		UPDATE Host SET slaveStatus='starting' WHERE keyhost=OLD.fkeyhost AND fkeyjob=NEW.fkeyjob AND slaveStatus='busy';
	END IF;

	-- Update Job Counters when a tasks status changes
	IF (NEW.status != OLD.status) THEN
		IF( NEW.status = 'new' ) THEN
			unassigned_change := unassigned_change + 1;
		END IF;
		IF( NEW.status = 'assigned' ) THEN
			assigned_change := assigned_change + 1;
		END IF;
		IF( NEW.status = 'busy' ) THEN
			busy_change := busy_change + 1;
		END IF;
		IF( NEW.status = 'done' ) THEN
			done_change := done_change + 1;
		END IF;
		IF( NEW.status = 'cancelled' ) THEN
			cancelled_change := cancelled_change + 1;
		END IF;
		IF( NEW.status = 'suspended' ) THEN
			suspended_change := suspended_change + 1;
		END IF;

		IF( OLD.status = 'new' ) THEN
			unassigned_change := unassigned_change - 1;
		END IF;
		IF( OLD.status = 'assigned' ) THEN
			assigned_change := assigned_change - 1;
		END IF;
		IF( OLD.status = 'busy' ) THEN
			busy_change := busy_change - 1;
		END IF;
		IF( OLD.status = 'done' ) THEN
			done_change := done_change - 1;
		END IF;
		IF( OLD.status = 'cancelled' ) THEN
			cancelled_change := cancelled_change - 1;
		END IF;
		IF( OLD.status = 'suspended' ) THEN
			suspended_change := suspended_change - 1;
		END IF;

		UPDATE Job SET
				tasksUnassigned	=	tasksUnassigned + unassigned_change,
				tasksAssigned	=	tasksAssigned + assigned_change,
				tasksBusy		=	tasksbusy + busy_change,
				tasksDone		= 	tasksDone + done_change,
				tasksCancelled	= 	tasksCancelled + cancelled_change,
				tasksSuspended  =   tasksSuspended + suspended_change
			WHERE
				keyjob = NEW.fkeyjob;
	END IF;
RETURN new;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.update_jobtask() OWNER TO brobison;

--
-- Name: update_laststatuschange(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION update_laststatuschange() RETURNS "trigger"
    AS $$
DECLARE
currentjob job;
BEGIN
IF (NEW.slavestatus IS NOT NULL)
 AND (NEW.slavestatus != OLD.slavestatus) THEN
	NEW.lastStatusChange = NOW();
	IF (NEW.slavestatus != 'busy') THEN
		NEW.fkeyjobtask = NULL;
	END IF;
END IF;

-- Increment Job.hostsOnJob
IF (NEW.fkeyjob IS NOT NULL AND (NEW.fkeyjob != OLD.fkeyjob OR OLD.fkeyjob IS NULL) ) THEN
	UPDATE Job SET hostsOnJob=hostsOnJob+1 WHERE keyjob=NEW.fkeyjob;
END IF;

-- Decrement Job.hostsOnJob
IF (OLD.fkeyjob IS NOT NULL AND (OLD.fkeyjob != NEW.fkeyjob OR NEW.fkeyjob IS NULL) ) THEN
	UPDATE Job SET hostsOnJob=hostsOnJob-1 WHERE keyjob=OLD.fkeyjob;
END IF;

IF (NEW.slavestatus IS NOT NULL)
 AND ((NEW.slavestatus != OLD.slavestatus) OR (NEW.fkeyjobtask != OLD.fkeyjobtask) OR ((OLD.fkeyjobtask IS NULL) != (NEW.fkeyjobtask IS NULL))) THEN
	SELECT INTO currentjob * from job where keyjob=NEW.fkeyjob;
	UPDATE hosthistory SET duration = now() - datetime, nextstatus=NEW.slavestatus WHERE duration is null and fkeyhost=NEW.keyhost;
	INSERT INTO hosthistory (datetime,fkeyhost,fkeyjob,fkeyjobstat,status,laststatus,fkeyjobtype,fkeyjobtask) values (now(),NEW.keyhost,NEW.fkeyjob,currentjob.fkeyjobstat,NEW.slavestatus,OLD.slavestatus,currentjob.fkeyjobtype,NEW.fkeyjobtask);
END IF;

RETURN new;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.update_laststatuschange() OWNER TO brobison;

--
-- Name: update_project_tempo(); Type: FUNCTION; Schema: public; Owner: brobison
--

CREATE FUNCTION update_project_tempo() RETURNS void
    AS $$
	DECLARE
		cur RECORD;
		hosts RECORD;
		newtempo float;
	BEGIN
		SELECT INTO hosts count(*) from host where slavestatus in ('ready','assigned','busy','copy');
		FOR cur IN
			SELECT projecttempo.fkeyproject, max(projecttempo.tempo) as tempo, sum(hostsonjob) as hostcount, count(job.keyjob) as jobs
			FROM projecttempo LEFT JOIN job ON projecttempo.fkeyproject=job.fkeyproject AND status IN('started','ready')
			GROUP BY projecttempo.fkeyproject
			ORDER BY hostcount ASC
		LOOP
			IF cur.hostcount IS NULL OR cur.hostcount=0 THEN
				IF cur.tempo < .0001 AND (cur.jobs IS NULL OR cur.jobs=0)  THEN
					DELETE FROM projecttempo WHERE fkeyproject=cur.fkeyproject;
				ELSE
					UPDATE projecttempo SET tempo=tempo*.8 where fkeyproject=cur.fkeyproject;
				END IF;
			ELSE
				UPDATE projecttempo SET tempo=tempo + ((cur.hostcount::float/hosts.count)-tempo) where fkeyproject=cur.fkeyproject;
			END IF;
		END LOOP;
		FOR cur IN
			SELECT job.fkeyproject, sum(hostsonjob) as hostcount
			FROM job
			WHERE status IN('started','ready') AND fkeyproject NOT IN (SELECT fkeyproject FROM projecttempo)
			GROUP BY job.fkeyproject
		LOOP
			newtempo := cur.hostcount*.1;
			IF newtempo < .001 THEN
				newtempo := .001;
			END IF;
			INSERT INTO projecttempo (fkeyproject, tempo) values (cur.fkeyproject,newtempo);
		END LOOP;
		RETURN;
	END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.update_project_tempo() OWNER TO brobison;

--
-- Name: /; Type: OPERATOR; Schema: public; Owner: brobison
--

CREATE OPERATOR / (
    PROCEDURE = interval_divide,
    LEFTARG = interval,
    RIGHTARG = interval
);


ALTER OPERATOR public./ (interval, interval) OWNER TO brobison;

SET search_path = jabberd, pg_catalog;

--
-- Name: active; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE active (
    "collection-owner" text,
    "object-sequence" integer,
    "time" integer
);


ALTER TABLE jabberd.active OWNER TO jabberd;

--
-- Name: authreg; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE authreg (
    username text,
    realm text,
    "password" text,
    token text,
    "sequence" integer,
    hash text
);


ALTER TABLE jabberd.authreg OWNER TO jabberd;

--
-- Name: logout; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE logout (
    "collection-owner" text,
    "object-sequence" integer,
    "time" integer
);


ALTER TABLE jabberd.logout OWNER TO jabberd;

--
-- Name: object-sequence; Type: SEQUENCE; Schema: jabberd; Owner: postgres
--

CREATE SEQUENCE "object-sequence"
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE jabberd."object-sequence" OWNER TO postgres;

--
-- Name: privacy-items; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE "privacy-items" (
    "collection-owner" text,
    "object-sequence" integer,
    list text,
    "type" text,
    value text,
    deny integer,
    "order" integer,
    block integer
);


ALTER TABLE jabberd."privacy-items" OWNER TO jabberd;

--
-- Name: private; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE private (
    "collection-owner" text,
    "object-sequence" integer,
    ns text,
    xml text
);


ALTER TABLE jabberd.private OWNER TO jabberd;

--
-- Name: queue; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE queue (
    "collection-owner" text,
    "object-sequence" integer,
    xml text
);


ALTER TABLE jabberd.queue OWNER TO jabberd;

--
-- Name: roster-groups; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE "roster-groups" (
    "collection-owner" text,
    "object-sequence" integer,
    jid text,
    "group" text
);


ALTER TABLE jabberd."roster-groups" OWNER TO jabberd;

--
-- Name: roster-items; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE "roster-items" (
    "collection-owner" text,
    "object-sequence" integer,
    jid text,
    name text,
    ask integer,
    "to" boolean,
    "from" boolean
);


ALTER TABLE jabberd."roster-items" OWNER TO jabberd;

--
-- Name: vcard; Type: TABLE; Schema: jabberd; Owner: jabberd; Tablespace: 
--

CREATE TABLE vcard (
    "collection-owner" text,
    "object-sequence" integer,
    fn text,
    nickname text,
    url text,
    tel text,
    email text,
    title text,
    "role" text,
    bday text,
    "desc" text,
    "n-given" text,
    "n-family" text,
    "adr-street" text,
    "adr-extadd" text,
    "adr-locality" text,
    "adr-region" text,
    "adr-pcode" text,
    "adr-country" text,
    "org-orgname" text,
    "org-orgunit" text
);


ALTER TABLE jabberd.vcard OWNER TO jabberd;

SET search_path = public, pg_catalog;

--
-- Name: a; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE a (
    "key" integer NOT NULL
);


ALTER TABLE public.a OWNER TO brobison;

--
-- Name: abdownloadstat; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE abdownloadstat (
    keyabdownloadstat serial NOT NULL,
    "type" text,
    size integer,
    fkeyhost integer,
    "time" integer,
    abrev integer,
    finished timestamp without time zone,
    fkeyjob integer
);


ALTER TABLE public.abdownloadstat OWNER TO brobison;

--
-- Name: annotation; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE annotation (
    keyannotation serial NOT NULL,
    notes text,
    "sequence" text,
    framestart integer,
    frameend integer,
    markupdata text
);


ALTER TABLE public.annotation OWNER TO brobison;

--
-- Name: element; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE element (
    keyelement serial NOT NULL,
    daysbid double precision,
    description text,
    fkeyelement integer,
    fkeyelementstatus integer,
    fkeyelementtype integer,
    fkeyproject integer,
    fkeythumbnail integer,
    name text,
    daysscheduled double precision,
    daysestimated double precision,
    status text,
    filepath text,
    fkeyassettype integer,
    fkeypathtemplate integer,
    fkeystatusset integer,
    allowtime boolean,
    datestart date,
    datecomplete date,
    fkeyassettemplate integer
);


ALTER TABLE public.element OWNER TO postgres;

--
-- Name: asset; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE asset (
    fkeyassettype integer
)
INHERITS (element);


ALTER TABLE public.asset OWNER TO postgres;

--
-- Name: assetgroup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE assetgroup (
    fkeyassettype integer
)
INHERITS (element);


ALTER TABLE public.assetgroup OWNER TO postgres;

--
-- Name: assetset; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE assetset (
    keyassetset serial NOT NULL,
    fkeyproject integer,
    fkeyelementtype integer,
    fkeyassettype integer,
    name text
);


ALTER TABLE public.assetset OWNER TO brobison;

--
-- Name: assetsetitem; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE assetsetitem (
    keyassetsetitem serial NOT NULL,
    fkeyassetset integer,
    fkeyassettype integer,
    fkeyelementtype integer,
    fkeytasktype integer
);


ALTER TABLE public.assetsetitem OWNER TO brobison;

--
-- Name: assettemplate; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE assettemplate (
    keyassettemplate serial NOT NULL,
    fkeyassettype integer,
    fkeyelement integer,
    fkeyproject integer,
    name text
);


ALTER TABLE public.assettemplate OWNER TO brobison;

--
-- Name: assettype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE assettype (
    keyassettype serial NOT NULL,
    assettype text,
    deleted boolean
);


ALTER TABLE public.assettype OWNER TO postgres;

--
-- Name: attachment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE attachment (
    keyattachment serial NOT NULL,
    caption text,
    created timestamp without time zone,
    filename text,
    fkeyelement integer,
    fkeyuser integer,
    origpath text,
    attachment text,
    url text,
    description text,
    fkeyauthor integer,
    fkeyattachmenttype integer
);


ALTER TABLE public.attachment OWNER TO postgres;

--
-- Name: attachmenttype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE attachmenttype (
    keyattachmenttype serial NOT NULL,
    attachmenttype text
);


ALTER TABLE public.attachmenttype OWNER TO postgres;

--
-- Name: b; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE b (
    val integer
)
INHERITS (a);


ALTER TABLE public.b OWNER TO brobison;

--
-- Name: calendar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE calendar (
    keycalendar serial NOT NULL,
    repeat integer,
    fkeycalendarcategory integer,
    url text,
    fkeyauthor integer,
    fieldname text,
    notifylist text,
    notifybatch text,
    leadtime integer,
    notifymask integer,
    fkeyusr integer,
    private integer,
    date timestamp without time zone,
    calendar text
);


ALTER TABLE public.calendar OWNER TO postgres;

--
-- Name: calendarcategory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE calendarcategory (
    keycalendarcategory serial NOT NULL,
    calendarcategory text
);


ALTER TABLE public.calendarcategory OWNER TO postgres;

--
-- Name: checklistitem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE checklistitem (
    keychecklistitem serial NOT NULL,
    body text,
    checklistitem text,
    fkeyproject integer,
    fkeythumbnail integer,
    fkeytimesheetcategory integer,
    "type" text,
    fkeystatusset integer
);


ALTER TABLE public.checklistitem OWNER TO postgres;

--
-- Name: checkliststatus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE checkliststatus (
    keycheckliststatus serial NOT NULL,
    fkeychecklistitem integer,
    fkeyelement integer,
    state integer,
    fkeyelementstatus integer
);


ALTER TABLE public.checkliststatus OWNER TO postgres;

--
-- Name: client; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE client (
    keyclient serial NOT NULL,
    client text,
    textcard text
);


ALTER TABLE public.client OWNER TO postgres;

--
-- Name: config; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config (
    keyconfig serial NOT NULL,
    config text,
    value text
);


ALTER TABLE public.config OWNER TO postgres;

--
-- Name: demoreel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE demoreel (
    keydemoreel serial NOT NULL,
    demoreel text,
    datesent date,
    projectlist text,
    contactinfo text,
    notes text,
    playlist text,
    shippingtype integer
);


ALTER TABLE public.demoreel OWNER TO postgres;

--
-- Name: hostgroup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hostgroup (
    keyhostgroup serial NOT NULL,
    hostgroup text,
    fkeyusr integer,
    private boolean
);


ALTER TABLE public.hostgroup OWNER TO postgres;

--
-- Name: dynamichostgroup; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE dynamichostgroup (
    keydynamichostgroup serial NOT NULL,
    hostwhereclause text
)
INHERITS (hostgroup);


ALTER TABLE public.dynamichostgroup OWNER TO brobison;

--
-- Name: elementdep; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE elementdep (
    keyelementdep serial NOT NULL,
    fkeyelement integer,
    fkeyelementdep integer,
    relationtype text
);


ALTER TABLE public.elementdep OWNER TO postgres;

--
-- Name: elementstatus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE elementstatus (
    keyelementstatus serial NOT NULL,
    name text,
    color text,
    fkeystatusset integer,
    "order" integer
);


ALTER TABLE public.elementstatus OWNER TO postgres;

--
-- Name: elementthread; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE elementthread (
    keyelementthread serial NOT NULL,
    datetime timestamp without time zone,
    elementthread text,
    fkeyelement integer,
    fkeyusr integer,
    skeyreply integer,
    topic text,
    todostatus integer,
    hasattachments integer
);


ALTER TABLE public.elementthread OWNER TO postgres;

--
-- Name: elementtype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE elementtype (
    keyelementtype serial NOT NULL,
    elementtype text,
    sortprefix text
);


ALTER TABLE public.elementtype OWNER TO postgres;

--
-- Name: elementtypetasktype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE elementtypetasktype (
    keyelementtypetasktype serial NOT NULL,
    fkeyelementtype integer,
    fkeytasktype integer,
    fkeyassettype integer
);


ALTER TABLE public.elementtypetasktype OWNER TO postgres;

--
-- Name: elementuser; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE elementuser (
    keyelementuser serial NOT NULL,
    fkeyelement integer,
    fkeyuser integer,
    active boolean,
    fkeyassettype integer
);


ALTER TABLE public.elementuser OWNER TO brobison;

--
-- Name: usr; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usr (
    dateoflastlogon date,
    email text,
    fkeyhost integer,
    gpgkey text,
    jid text,
    pager text,
    "password" text,
    remoteips text,
    schedule text,
    shell text,
    uid integer,
    threadnotifybyjabber integer,
    threadnotifybyemail integer,
    fkeyclient integer,
    intranet integer,
    homedir text,
    disabled integer DEFAULT 0,
    gid integer,
    usr text,
    keyusr integer,
    rolemask text,
    usrlevel integer,
    remoteok integer,
    requestcount integer,
    sessiontimeout integer,
    logoncount integer,
    useradded integer,
    oldkeyusr integer,
    sid text
)
INHERITS (element);


ALTER TABLE public.usr OWNER TO postgres;

--
-- Name: employee; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE employee (
    namefirst text,
    namelast text,
    dateofhire date,
    dateoftermination date,
    dateofbirth date,
    logon text,
    lockedout integer,
    bebackat timestamp without time zone,
    "comment" text,
    userlevel integer,
    nopostdays integer,
    initials text,
    missingtimesheetcount integer,
    namemiddle text
)
INHERITS (usr);


ALTER TABLE public.employee OWNER TO postgres;

--
-- Name: filetemplate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filetemplate (
    keyfiletemplate serial NOT NULL,
    fkeyelementtype integer,
    fkeyproject integer,
    fkeytasktype integer,
    name text,
    sourcefile text,
    templatefilename text,
    trackertable text
);


ALTER TABLE public.filetemplate OWNER TO postgres;

--
-- Name: filetracker; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filetracker (
    keyfiletracker serial NOT NULL,
    fkeyelement integer NOT NULL,
    name text,
    path text,
    filename text,
    fkeypathtemplate integer
);


ALTER TABLE public.filetracker OWNER TO postgres;

--
-- Name: filetrackerdep; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filetrackerdep (
    keyfiletrackerdep serial NOT NULL,
    fkeyinput integer,
    fkeyoutput integer
);


ALTER TABLE public.filetrackerdep OWNER TO postgres;

--
-- Name: fileversion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE fileversion (
    keyfileversion serial NOT NULL,
    version integer,
    iteration integer,
    path text,
    oldfilenames text,
    filename text,
    filenametemplate text,
    automaster integer,
    fkeyelement integer,
    fkeyfileversion integer
);


ALTER TABLE public.fileversion OWNER TO postgres;

--
-- Name: folder; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE folder (
    keyfolder serial NOT NULL,
    folder text,
    mount text,
    tablename text,
    fkey integer,
    online integer,
    alias text,
    host text,
    link text
);


ALTER TABLE public.folder OWNER TO postgres;

--
-- Name: gridtemplate; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE gridtemplate (
    keygridtemplate serial NOT NULL,
    fkeyproject integer,
    gridtemplate text
);


ALTER TABLE public.gridtemplate OWNER TO postgres;

--
-- Name: gridtemplateitem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE gridtemplateitem (
    keygridtemplateitem serial NOT NULL,
    fkeygridtemplate integer,
    fkeytasktype integer,
    checklistitems text,
    columntype integer,
    headername text,
    "position" integer
);


ALTER TABLE public.gridtemplateitem OWNER TO postgres;

--
-- Name: groupmapping; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE groupmapping (
    keygroupmapping serial NOT NULL,
    fkeygrp integer,
    fkeymapping integer
);


ALTER TABLE public.groupmapping OWNER TO brobison;

--
-- Name: grp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE grp (
    keygrp serial NOT NULL,
    grp text,
    alias text
);


ALTER TABLE public.grp OWNER TO postgres;

--
-- Name: gruntscript; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE gruntscript (
    keygruntscript serial NOT NULL,
    runcount integer,
    lastrun date,
    scriptname text
);


ALTER TABLE public.gruntscript OWNER TO brobison;

--
-- Name: history; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE history (
    keyhistory serial NOT NULL,
    date timestamp without time zone,
    fkeyelement integer,
    fkeyusr integer,
    history text
);


ALTER TABLE public.history OWNER TO postgres;

--
-- Name: host; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE host (
    keyhost serial NOT NULL,
    backupbytes text,
    cpus integer,
    description text,
    diskusage text,
    fkeyjob integer,
    host text NOT NULL,
    manufacturer text,
    model text,
    os text,
    rendertime text,
    slaveframes text,
    slavestatus text,
    slavepluginlist text,
    sn text,
    version text,
    renderrate double precision,
    dutycycle double precision,
    memory integer,
    mhtz integer,
    online integer,
    uid integer,
    slavepacketweight double precision,
    framecount integer,
    viruscount integer,
    virustimestamp date,
    errortempo integer,
    slavejobtype text,
    fkeyhost_backup integer,
    oldkey integer,
    abversion text,
    slavepulse timestamp without time zone,
    laststatuschange timestamp without time zone,
    loadavg double precision,
    allowmapping boolean DEFAULT false,
    allowsleep boolean DEFAULT false,
    fkeyjobtask integer,
    wakeonlan boolean DEFAULT false,
    architecture text
);


ALTER TABLE public.host OWNER TO postgres;

--
-- Name: hostgroupitem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hostgroupitem (
    keyhostgroupitem serial NOT NULL,
    fkeyhostgroup integer,
    fkeyhost integer
);


ALTER TABLE public.hostgroupitem OWNER TO postgres;

--
-- Name: hostinterface; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hostinterface (
    keyhostinterface serial NOT NULL,
    fkeyhost integer,
    mac text,
    ip text,
    fkeyhostinterfacetype integer,
    switchport integer,
    fkeyswitch integer,
    inst text
);


ALTER TABLE public.hostinterface OWNER TO postgres;

--
-- Name: hostinterfacetype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hostinterfacetype (
    keyhostinterfacetype serial NOT NULL,
    hostinterfacetype text
);


ALTER TABLE public.hostinterfacetype OWNER TO postgres;

--
-- Name: hostload; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE hostload (
    keyhostload serial NOT NULL,
    fkeyhost integer,
    loadavg double precision,
    loadavgadjust double precision DEFAULT 0.0,
    loadavgadjusttimestamp timestamp without time zone
);


ALTER TABLE public.hostload OWNER TO brobison;

--
-- Name: hostmapping; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE hostmapping (
    keyhostmapping serial NOT NULL,
    fkeyhost integer NOT NULL,
    fkeymapping integer NOT NULL
);


ALTER TABLE public.hostmapping OWNER TO brobison;

--
-- Name: hostresource; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hostresource (
    keyhostresource serial NOT NULL,
    fkeyhost integer,
    hostresource text
);


ALTER TABLE public.hostresource OWNER TO postgres;

--
-- Name: hosts_ready; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE hosts_ready (
    count bigint
);


ALTER TABLE public.hosts_ready OWNER TO brobison;

--
-- Name: hosts_total; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE hosts_total (
    count bigint
);


ALTER TABLE public.hosts_total OWNER TO brobison;

--
-- Name: hostservice; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hostservice (
    keyhostservice serial NOT NULL,
    fkeyhost integer,
    fkeyservice integer,
    hostserviceinfo text,
    hostservice text,
    fkeysyslog integer,
    enabled boolean,
    pulse timestamp without time zone
);


ALTER TABLE public.hostservice OWNER TO postgres;

--
-- Name: job; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE job (
    keyjob serial NOT NULL,
    fkeyelement integer,
    fkeyhost integer,
    fkeyjobtype integer,
    fkeyproject integer,
    fkeyusr integer,
    hostlist text,
    job text,
    jobtime text,
    outputpath text,
    status text DEFAULT 'new'::text,
    submitted integer,
    started integer,
    ended integer,
    expires integer,
    deleteoncomplete integer,
    hostsonjob integer DEFAULT 0,
    taskscount integer DEFAULT 0,
    tasksunassigned integer DEFAULT 0,
    tasksdone integer DEFAULT 0,
    tasksaveragetime integer,
    priority integer,
    errorcount integer,
    queueorder integer,
    packettype text DEFAULT 'random'::text,
    packetsize integer,
    queueeta integer,
    notifyonerror text,
    notifyoncomplete text,
    maxtasktime integer DEFAULT 3600,
    cleaned integer,
    filesize integer,
    btinfohash text,
    rendertime integer,
    abversion text,
    deplist text,
    args text,
    filename text,
    filemd5sum text,
    fkeyjobstat integer,
    username text,
    "domain" text,
    "password" text,
    stats text,
    currentmapserverweight double precision,
    loadtimeaverage integer,
    tasksassigned integer DEFAULT 0,
    tasksbusy integer DEFAULT 0,
    prioritizeoutertasks boolean,
    outertasksassigned boolean,
    lastnotifiederrorcount integer,
    taskscancelled integer DEFAULT 0,
    taskssuspended integer DEFAULT 0
);


ALTER TABLE public.job OWNER TO postgres;

--
-- Name: jobbatch; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobbatch (
    cmd text,
    restartafterfinish boolean DEFAULT false,
    restartaftershutdown boolean
)
INHERITS (job);


ALTER TABLE public.jobbatch OWNER TO brobison;

--
-- Name: jobcannedbatch; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobcannedbatch (
    keyjobcannedbatch serial NOT NULL,
    name text,
    "group" text,
    cmd text
);


ALTER TABLE public.jobcannedbatch OWNER TO brobison;

--
-- Name: joberror; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE joberror (
    keyjoberror serial NOT NULL,
    fkeyhost integer,
    fkeyjob integer,
    frames text,
    message text,
    errortime integer,
    count integer DEFAULT 1,
    cleared boolean DEFAULT false
);


ALTER TABLE public.joberror OWNER TO postgres;

--
-- Name: jobfusion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobfusion (
    filename text,
    frameend integer,
    framestart integer,
    framelist text
)
INHERITS (job);


ALTER TABLE public.jobfusion OWNER TO postgres;

--
-- Name: jobhistory; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobhistory (
    keyjobhistory serial NOT NULL,
    fkeyjobhistorytype integer,
    fkeyjob integer,
    fkeyhost integer,
    fkeyuser integer,
    message text,
    created timestamp without time zone
);


ALTER TABLE public.jobhistory OWNER TO brobison;

--
-- Name: jobhistorytype; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobhistorytype (
    keyjobhistorytype serial NOT NULL,
    "type" text
);


ALTER TABLE public.jobhistorytype OWNER TO brobison;

--
-- Name: jobmax; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobmax (
    fkeyjobtype integer DEFAULT 6,
    filename text,
    camera text,
    elementfile text,
    fileoriginal text,
    flag_h integer,
    flag_v integer,
    flag_w integer,
    flag_x2 integer,
    flag_xa integer,
    flag_xc integer,
    flag_xd integer,
    flag_xe integer,
    flag_xf integer,
    flag_xh integer,
    flag_xk integer,
    flag_xn integer,
    flag_xo integer,
    flag_xp integer,
    flag_xv integer,
    frameend integer,
    framelist text,
    framestart integer,
    framenth integer
)
INHERITS (job);


ALTER TABLE public.jobmax OWNER TO postgres;

--
-- Name: jobmax5; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobmax5 (
    rendertime integer,
    abversion text,
    filename text,
    camera text,
    fileoriginal text,
    flag_h integer,
    framestart integer,
    frameend integer,
    framenth integer,
    flag_w integer,
    flag_xv integer,
    flag_x2 integer,
    flag_xa integer,
    flag_xe integer,
    flag_xk integer,
    flag_xd integer,
    flag_xh integer,
    flag_xo integer,
    flag_xf integer,
    flag_xn integer,
    flag_xp integer,
    flag_xc integer,
    flag_v integer,
    elementfile text,
    framelist text
)
INHERITS (job);


ALTER TABLE public.jobmax5 OWNER TO postgres;

--
-- Name: jobmax6; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobmax6 (
    filename text,
    camera text,
    elementfile text,
    fileoriginal text,
    flag_h integer,
    flag_v integer,
    flag_w integer,
    flag_x2 integer,
    flag_xa integer,
    flag_xc integer,
    flag_xd integer,
    flag_xe integer,
    flag_xf integer,
    flag_xh integer,
    flag_xk integer,
    flag_xn integer,
    flag_xo integer,
    flag_xp integer,
    flag_xv integer,
    frameend integer,
    framelist text,
    framenth integer,
    framestart integer
)
INHERITS (job);


ALTER TABLE public.jobmax6 OWNER TO postgres;

--
-- Name: jobmax7; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobmax7 (
)
INHERITS (jobmax);


ALTER TABLE public.jobmax7 OWNER TO brobison;

--
-- Name: jobmax8; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobmax8 (
)
INHERITS (jobmax);


ALTER TABLE public.jobmax8 OWNER TO brobison;

--
-- Name: jobmax9; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobmax9 (
)
INHERITS (jobmax);


ALTER TABLE public.jobmax9 OWNER TO brobison;

--
-- Name: jobmaxscript; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobmaxscript (
    fkeyjobtype integer DEFAULT 3,
    filename text,
    script text,
    maxtime integer,
    outputfiles text,
    filelist text,
    silent boolean DEFAULT true,
    maxversion text
)
INHERITS (job);


ALTER TABLE public.jobmaxscript OWNER TO postgres;

--
-- Name: jobspool; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobspool (
    flowfile text,
    flowpath text,
    holdend integer,
    inputfile text,
    inputpath text,
    outputfile text,
    inputstart integer,
    inputend integer,
    holdstart integer,
    importmode integer,
    keepinqueue integer,
    lastscanned integer,
    jobpath text
)
INHERITS (job);


ALTER TABLE public.jobspool OWNER TO postgres;

--
-- Name: jobstat; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobstat (
    keyjobstat serial NOT NULL,
    fkeyelement integer,
    fkeyproject integer,
    fkeyusr integer,
    pass text,
    taskcount integer,
    taskscompleted integer DEFAULT 0,
    tasktime integer DEFAULT 0,
    started timestamp without time zone,
    ended timestamp without time zone,
    name text,
    errorcount integer DEFAULT 0,
    mintasktime interval,
    maxtasktime interval,
    avgtasktime interval,
    totaltasktime interval,
    minerrortime interval,
    maxerrortime interval,
    avgerrortime interval,
    totalerrortime interval,
    mincopytime interval,
    maxcopytime interval,
    avgcopytime interval,
    totalcopytime interval,
    copycount integer,
    minloadtime interval,
    maxloadtime interval,
    avgloadtime interval,
    totalloadtime interval,
    loadcount integer,
    submitted timestamp without time zone DEFAULT now()
);


ALTER TABLE public.jobstat OWNER TO brobison;

--
-- Name: jobsystem; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobsystem (
)
INHERITS (job);


ALTER TABLE public.jobsystem OWNER TO postgres;

--
-- Name: jobtask; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobtask (
    keyjobtask serial NOT NULL,
    ended integer,
    fkeyhost integer,
    fkeyjob integer,
    status text DEFAULT 'new'::text,
    stderr text,
    "stdin" text,
    "stdout" text,
    jobtask integer,
    started integer,
    label text
);


ALTER TABLE public.jobtask OWNER TO postgres;

--
-- Name: jobtype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE jobtype (
    keyjobtype serial NOT NULL,
    jobtype text,
    fkeyservice integer
);


ALTER TABLE public.jobtype OWNER TO postgres;

--
-- Name: jobtypemapping; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobtypemapping (
    keyjobtypemapping serial NOT NULL,
    fkeyjobtype integer,
    fkeymapping integer
);


ALTER TABLE public.jobtypemapping OWNER TO brobison;

--
-- Name: jobxsi; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE jobxsi (
    framestart integer,
    frameend integer,
    framelist text,
    framenth integer,
    pass text,
    flag_w integer,
    flag_h integer
)
INHERITS (job);


ALTER TABLE public.jobxsi OWNER TO brobison;

--
-- Name: mapping; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE mapping (
    keymapping serial NOT NULL,
    fkeyhost integer,
    "share" text,
    mount text,
    fkeymappingtype integer,
    description text
);


ALTER TABLE public.mapping OWNER TO brobison;

--
-- Name: mappingtype; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE mappingtype (
    keymappingtype serial NOT NULL,
    name text
);


ALTER TABLE public.mappingtype OWNER TO brobison;

--
-- Name: methodperms; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE methodperms (
    keymethodperms serial NOT NULL,
    method text,
    users text,
    groups text,
    fkeyproject integer
);


ALTER TABLE public.methodperms OWNER TO postgres;

--
-- Name: notification; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE notification (
    keynotification serial NOT NULL,
    created timestamp without time zone,
    subject text,
    message text,
    component text,
    event text,
    routed timestamp without time zone
);


ALTER TABLE public.notification OWNER TO brobison;

--
-- Name: notificationdestination; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE notificationdestination (
    keynotificationdestination serial NOT NULL,
    fkeynotification integer,
    fkeynotificationmethod integer,
    delivered timestamp without time zone,
    destination text,
    fkeyuser integer,
    routed timestamp without time zone
);


ALTER TABLE public.notificationdestination OWNER TO brobison;

--
-- Name: notificationmethod; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE notificationmethod (
    keynotificationmethod serial NOT NULL,
    name text
);


ALTER TABLE public.notificationmethod OWNER TO brobison;

--
-- Name: notificationroute; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE notificationroute (
    keynotificationuserroute serial NOT NULL,
    eventmatch text,
    componentmatch text,
    fkeyuser integer,
    subjectmatch text,
    messagematch text,
    actions text,
    priority integer
);


ALTER TABLE public.notificationroute OWNER TO brobison;

--
-- Name: notify; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "notify" (
    keynotify serial NOT NULL,
    "notify" text,
    fkeyusr integer,
    fkeysyslogrealm integer,
    severitymask text,
    starttime timestamp without time zone,
    endtime timestamp without time zone,
    threshhold integer,
    notifyclass text,
    notifymethod text,
    fkeynotifymethod integer,
    threshold integer
);


ALTER TABLE public."notify" OWNER TO postgres;

--
-- Name: notifymethod; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notifymethod (
    keynotifymethod serial NOT NULL,
    notifymethod text
);


ALTER TABLE public.notifymethod OWNER TO postgres;

--
-- Name: notifysent; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notifysent (
    keynotifysent serial NOT NULL,
    fkeynotify integer,
    fkeysyslog integer
);


ALTER TABLE public.notifysent OWNER TO postgres;

--
-- Name: notifywho; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notifywho (
    keynotifywho serial NOT NULL,
    "class" text,
    fkeynotify integer,
    fkeyusr integer,
    fkey integer
);


ALTER TABLE public.notifywho OWNER TO postgres;

--
-- Name: pathtemplate; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE pathtemplate (
    keypathtemplate serial NOT NULL,
    name text,
    pathtemplate text,
    pathre text,
    filenametemplate text,
    filenamere text,
    version integer,
    pythoncode text
);


ALTER TABLE public.pathtemplate OWNER TO brobison;

--
-- Name: permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE permission (
    keypermission serial NOT NULL,
    methodpattern text,
    fkeyusr integer,
    permission text,
    fkeygrp integer,
    "class" text
);


ALTER TABLE public.permission OWNER TO postgres;

--
-- Name: phoneno; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phoneno (
    keyphoneno serial NOT NULL,
    phoneno text,
    fkeyphonetype integer,
    fkeyemployee integer,
    "domain" text
);


ALTER TABLE public.phoneno OWNER TO postgres;

--
-- Name: phonetype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phonetype (
    keyphonetype serial NOT NULL,
    phonetype text
);


ALTER TABLE public.phonetype OWNER TO postgres;

--
-- Name: project; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE project (
    datestart date,
    compoutputdrive text,
    datedue date,
    filetype text,
    fkeyclient integer,
    notes text,
    renderoutputdrive text,
    script text,
    shortname text,
    wipdrive text,
    projectnumber integer,
    frames integer,
    nda integer,
    dayrate double precision,
    usefilecreation integer,
    dailydrive text,
    lastscanned timestamp without time zone,
    fkeyprojectstatus integer,
    assburnerweight double precision,
    project text,
    fps integer,
    resolution text,
    resolutionwidth integer,
    resolutionheight integer,
    archived integer,
    deliverymedium text,
    renderpixelaspect text
)
INHERITS (element);


ALTER TABLE public.project OWNER TO postgres;

--
-- Name: projectresolution; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE projectresolution (
    keyprojectresolution serial NOT NULL,
    deliveryformat text,
    fkeyproject integer,
    height integer,
    outputformat text,
    projectresolution text,
    width integer,
    pixelaspect double precision,
    fps integer
);


ALTER TABLE public.projectresolution OWNER TO postgres;

--
-- Name: projectstatus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE projectstatus (
    keyprojectstatus serial NOT NULL,
    projectstatus text,
    chronology integer
);


ALTER TABLE public.projectstatus OWNER TO postgres;

--
-- Name: projecttempo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE projecttempo (
    fkeyproject integer,
    tempo double precision
);


ALTER TABLE public.projecttempo OWNER TO postgres;

--
-- Name: rangefiletracker; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rangefiletracker (
    filenametemplate text NOT NULL,
    framestart integer,
    frameend integer,
    fkeyresolution integer,
    renderelement text
)
INHERITS (filetracker);


ALTER TABLE public.rangefiletracker OWNER TO postgres;

--
-- Name: renderframe; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE renderframe (
    keyrenderframe serial NOT NULL,
    fkeyshot integer,
    frame integer,
    fkeyresolution integer,
    status text
);


ALTER TABLE public.renderframe OWNER TO postgres;

--
-- Name: schedule; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schedule (
    keyschedule serial NOT NULL,
    fkeyuser integer,
    date date NOT NULL,
    starthour integer,
    hours integer,
    fkeyelement integer,
    fkeyassettype integer,
    fkeycreatedbyuser integer
);


ALTER TABLE public.schedule OWNER TO postgres;

--
-- Name: service; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE service (
    keyservice serial NOT NULL,
    service text,
    description text
);


ALTER TABLE public.service OWNER TO postgres;

--
-- Name: sessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sessions (
    id text,
    length integer,
    a_session text,
    "time" timestamp without time zone
);


ALTER TABLE public.sessions OWNER TO postgres;

--
-- Name: shot; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE shot (
    dialog text,
    frameend integer,
    framestart integer,
    shot double precision,
    framestartedl integer,
    frameendedl integer
)
INHERITS (element);


ALTER TABLE public.shot OWNER TO postgres;

--
-- Name: shotgroup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE shotgroup (
    shotgroup text
)
INHERITS (element);


ALTER TABLE public.shotgroup OWNER TO postgres;

--
-- Name: statusset; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE statusset (
    keystatusset serial NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.statusset OWNER TO brobison;

--
-- Name: syslog; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE syslog (
    keysyslog serial NOT NULL,
    fkeyhost integer,
    fkeysyslogrealm integer,
    fkeysyslogseverity integer,
    message text,
    count integer DEFAULT nextval(('syslog_count_seq'::text)::regclass),
    lastoccurrence timestamp without time zone DEFAULT now(),
    created timestamp without time zone DEFAULT now(),
    "class" text,
    method text
);


ALTER TABLE public.syslog OWNER TO postgres;

--
-- Name: syslog_count_seq; Type: SEQUENCE; Schema: public; Owner: brobison
--

CREATE SEQUENCE syslog_count_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.syslog_count_seq OWNER TO brobison;

--
-- Name: syslogrealm; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE syslogrealm (
    keysyslogrealm serial NOT NULL,
    syslogrealm text
);


ALTER TABLE public.syslogrealm OWNER TO postgres;

--
-- Name: syslogseverity; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE syslogseverity (
    keysyslogseverity serial NOT NULL,
    syslogseverity text,
    severity text
);


ALTER TABLE public.syslogseverity OWNER TO postgres;

--
-- Name: task; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE task (
    fkeytasktype integer,
    shotgroup integer
)
INHERITS (element);


ALTER TABLE public.task OWNER TO postgres;

--
-- Name: tasktype; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tasktype (
    keytasktype serial NOT NULL,
    tasktype text,
    iconcolor text
);


ALTER TABLE public.tasktype OWNER TO postgres;

--
-- Name: taskuser; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE taskuser (
    keytaskuser serial NOT NULL,
    fkeytask integer,
    fkeyuser integer,
    active integer
);


ALTER TABLE public.taskuser OWNER TO postgres;

--
-- Name: thread; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE thread (
    keythread serial NOT NULL,
    thread text,
    topic text,
    tablename text,
    fkey integer,
    datetime timestamp without time zone,
    fkeyauthor integer,
    skeyreply integer,
    fkeyusr integer,
    fkeythreadcategory integer
);


ALTER TABLE public.thread OWNER TO postgres;

--
-- Name: threadcategory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE threadcategory (
    keythreadcategory serial NOT NULL,
    threadcategory text
);


ALTER TABLE public.threadcategory OWNER TO postgres;

--
-- Name: threadnotify; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE threadnotify (
    keythreadnotify serial NOT NULL,
    fkeythread integer,
    fkeyuser integer,
    options integer
);


ALTER TABLE public.threadnotify OWNER TO postgres;

--
-- Name: thumbnail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE thumbnail (
    keythumbnail serial NOT NULL,
    cliprect text,
    date timestamp without time zone,
    fkeyelement integer,
    fkeyuser integer,
    originalfile text
);


ALTER TABLE public.thumbnail OWNER TO postgres;

--
-- Name: timesheet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE timesheet (
    keytimesheet serial NOT NULL,
    datetime timestamp without time zone,
    fkeyelement integer,
    fkeyemployee integer,
    fkeyproject integer,
    fkeytimesheetcategory integer,
    scheduledhour double precision,
    datetimesubmitted timestamp without time zone,
    unscheduledhour double precision,
    "comment" text
);


ALTER TABLE public.timesheet OWNER TO postgres;

--
-- Name: timesheetcategory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE timesheetcategory (
    keytimesheetcategory serial NOT NULL,
    timesheetcategory text,
    iconcolor integer,
    hasdaily integer,
    chronology text,
    disabled integer,
    istask boolean,
    fkeypathtemplate integer,
    fkeyelementtype integer DEFAULT 4,
    nameregexp text,
    allowtime boolean,
    color text,
    description text,
    sortcolumn text DEFAULT 'displayName'::text,
    tags text
);


ALTER TABLE public.timesheetcategory OWNER TO postgres;

--
-- Name: tracker; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tracker (
    keytracker serial NOT NULL,
    tracker text,
    fkeysubmitter integer,
    fkeyassigned integer,
    fkeycategory integer,
    fkeyseverity integer,
    fkeystatus integer,
    datetarget date,
    datechanged timestamp without time zone,
    datesubmitted timestamp without time zone,
    description text,
    timeestimate integer,
    fkeytrackerqueue integer
);


ALTER TABLE public.tracker OWNER TO postgres;

--
-- Name: trackercategory; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trackercategory (
    keytrackercategory serial NOT NULL,
    trackercategory text
);


ALTER TABLE public.trackercategory OWNER TO postgres;

--
-- Name: trackerlog; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trackerlog (
    keytrackerlog serial NOT NULL,
    fkeytracker integer,
    fkeyusr integer,
    datelogged integer,
    message text
);


ALTER TABLE public.trackerlog OWNER TO postgres;

--
-- Name: trackerqueue; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trackerqueue (
    keytrackerqueue serial NOT NULL,
    trackerqueue text
);


ALTER TABLE public.trackerqueue OWNER TO postgres;

--
-- Name: trackerseverity; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trackerseverity (
    keytrackerseverity serial NOT NULL,
    trackerseverity text
);


ALTER TABLE public.trackerseverity OWNER TO postgres;

--
-- Name: trackerstatus; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE trackerstatus (
    keytrackerstatus serial NOT NULL,
    trackerstatus text
);


ALTER TABLE public.trackerstatus OWNER TO postgres;

--
-- Name: userelement; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE userelement (
    keyuserelement serial NOT NULL,
    fkeyelement integer,
    fkeyusr integer,
    fkeyuser integer
);


ALTER TABLE public.userelement OWNER TO postgres;

--
-- Name: usermapping; Type: TABLE; Schema: public; Owner: brobison; Tablespace: 
--

CREATE TABLE usermapping (
    keyusermapping serial NOT NULL,
    fkeyusr integer,
    fkeymapping integer
);


ALTER TABLE public.usermapping OWNER TO brobison;

--
-- Name: userrole; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE userrole (
    keyuserrole serial NOT NULL,
    fkeytasktype integer,
    fkeyusr integer
);


ALTER TABLE public.userrole OWNER TO postgres;

--
-- Name: usrgrp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usrgrp (
    keyusrgrp serial NOT NULL,
    fkeyusr integer,
    fkeygrp integer,
    usrgrp text
);


ALTER TABLE public.usrgrp OWNER TO postgres;

--
-- Name: versionfiletracker; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE versionfiletracker (
    filenametemplate text,
    fkeyversionfiletracker integer,
    oldfilenames text,
    version integer,
    iteration integer,
    automaster integer
)
INHERITS (filetracker);


ALTER TABLE public.versionfiletracker OWNER TO postgres;

--
-- Name: a_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY a
    ADD CONSTRAINT a_pkey PRIMARY KEY ("key");


--
-- Name: abdownloadstat_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY abdownloadstat
    ADD CONSTRAINT abdownloadstat_pkey PRIMARY KEY (keyabdownloadstat);


--
-- Name: annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY annotation
    ADD CONSTRAINT annotation_pkey PRIMARY KEY (keyannotation);


--
-- Name: assetset_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY assetset
    ADD CONSTRAINT assetset_pkey PRIMARY KEY (keyassetset);


--
-- Name: assetsetitem_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY assetsetitem
    ADD CONSTRAINT assetsetitem_pkey PRIMARY KEY (keyassetsetitem);


--
-- Name: assettemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY assettemplate
    ADD CONSTRAINT assettemplate_pkey PRIMARY KEY (keyassettemplate);


--
-- Name: assettype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY assettype
    ADD CONSTRAINT assettype_pkey PRIMARY KEY (keyassettype);


--
-- Name: attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY attachment
    ADD CONSTRAINT attachment_pkey PRIMARY KEY (keyattachment);


--
-- Name: attachmenttype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY attachmenttype
    ADD CONSTRAINT attachmenttype_pkey PRIMARY KEY (keyattachmenttype);


--
-- Name: calendar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY calendar
    ADD CONSTRAINT calendar_pkey PRIMARY KEY (keycalendar);


--
-- Name: calendarcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY calendarcategory
    ADD CONSTRAINT calendarcategory_pkey PRIMARY KEY (keycalendarcategory);


--
-- Name: checklistitem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY checklistitem
    ADD CONSTRAINT checklistitem_pkey PRIMARY KEY (keychecklistitem);


--
-- Name: checkliststatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY checkliststatus
    ADD CONSTRAINT checkliststatus_pkey PRIMARY KEY (keycheckliststatus);


--
-- Name: client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (keyclient);


--
-- Name: config_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY config
    ADD CONSTRAINT config_pkey PRIMARY KEY (keyconfig);


--
-- Name: demoreel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY demoreel
    ADD CONSTRAINT demoreel_pkey PRIMARY KEY (keydemoreel);


--
-- Name: dynamichostgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY dynamichostgroup
    ADD CONSTRAINT dynamichostgroup_pkey PRIMARY KEY (keydynamichostgroup);


--
-- Name: element_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY element
    ADD CONSTRAINT element_pkey PRIMARY KEY (keyelement);


--
-- Name: elementdep_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY elementdep
    ADD CONSTRAINT elementdep_pkey PRIMARY KEY (keyelementdep);


--
-- Name: elementstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY elementstatus
    ADD CONSTRAINT elementstatus_pkey PRIMARY KEY (keyelementstatus);


--
-- Name: elementthread_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY elementthread
    ADD CONSTRAINT elementthread_pkey PRIMARY KEY (keyelementthread);


--
-- Name: elementtype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY elementtype
    ADD CONSTRAINT elementtype_pkey PRIMARY KEY (keyelementtype);


--
-- Name: elementtypetasktype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY elementtypetasktype
    ADD CONSTRAINT elementtypetasktype_pkey PRIMARY KEY (keyelementtypetasktype);


--
-- Name: elementuser_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY elementuser
    ADD CONSTRAINT elementuser_pkey PRIMARY KEY (keyelementuser);


--
-- Name: filetemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filetemplate
    ADD CONSTRAINT filetemplate_pkey PRIMARY KEY (keyfiletemplate);


--
-- Name: filetracker_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filetracker
    ADD CONSTRAINT filetracker_pkey PRIMARY KEY (keyfiletracker);


--
-- Name: filetrackerdep_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filetrackerdep
    ADD CONSTRAINT filetrackerdep_pkey PRIMARY KEY (keyfiletrackerdep);


--
-- Name: fileversion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fileversion
    ADD CONSTRAINT fileversion_pkey PRIMARY KEY (keyfileversion);


--
-- Name: folder_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY folder
    ADD CONSTRAINT folder_pkey PRIMARY KEY (keyfolder);


--
-- Name: gridtemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY gridtemplate
    ADD CONSTRAINT gridtemplate_pkey PRIMARY KEY (keygridtemplate);


--
-- Name: gridtemplateitem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY gridtemplateitem
    ADD CONSTRAINT gridtemplateitem_pkey PRIMARY KEY (keygridtemplateitem);


--
-- Name: groupmapping_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY groupmapping
    ADD CONSTRAINT groupmapping_pkey PRIMARY KEY (keygroupmapping);


--
-- Name: grp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY grp
    ADD CONSTRAINT grp_pkey PRIMARY KEY (keygrp);


--
-- Name: gruntscript_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY gruntscript
    ADD CONSTRAINT gruntscript_pkey PRIMARY KEY (keygruntscript);


--
-- Name: history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY history
    ADD CONSTRAINT history_pkey PRIMARY KEY (keyhistory);


--
-- Name: host_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY host
    ADD CONSTRAINT host_pkey PRIMARY KEY (keyhost);


--
-- Name: hostgroup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hostgroup
    ADD CONSTRAINT hostgroup_pkey PRIMARY KEY (keyhostgroup);


--
-- Name: hostgroupitem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hostgroupitem
    ADD CONSTRAINT hostgroupitem_pkey PRIMARY KEY (keyhostgroupitem);


--
-- Name: hosthistory_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY hosthistory
    ADD CONSTRAINT hosthistory_pkey PRIMARY KEY (keyhosthistory);


--
-- Name: hostinterface_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hostinterface
    ADD CONSTRAINT hostinterface_pkey PRIMARY KEY (keyhostinterface);


--
-- Name: hostinterfacetype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hostinterfacetype
    ADD CONSTRAINT hostinterfacetype_pkey PRIMARY KEY (keyhostinterfacetype);


--
-- Name: hostload_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY hostload
    ADD CONSTRAINT hostload_pkey PRIMARY KEY (keyhostload);


--
-- Name: hostmapping_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY hostmapping
    ADD CONSTRAINT hostmapping_pkey PRIMARY KEY (keyhostmapping);


--
-- Name: hostresource_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hostresource
    ADD CONSTRAINT hostresource_pkey PRIMARY KEY (keyhostresource);


--
-- Name: hostservice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hostservice
    ADD CONSTRAINT hostservice_pkey PRIMARY KEY (keyhostservice);


--
-- Name: job_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_pkey PRIMARY KEY (keyjob);


--
-- Name: jobcannedbatch_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY jobcannedbatch
    ADD CONSTRAINT jobcannedbatch_pkey PRIMARY KEY (keyjobcannedbatch);


--
-- Name: joberror_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY joberror
    ADD CONSTRAINT joberror_pkey PRIMARY KEY (keyjoberror);


--
-- Name: jobhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY jobhistory
    ADD CONSTRAINT jobhistory_pkey PRIMARY KEY (keyjobhistory);


--
-- Name: jobhistorytype_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY jobhistorytype
    ADD CONSTRAINT jobhistorytype_pkey PRIMARY KEY (keyjobhistorytype);


--
-- Name: jobstat_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY jobstat
    ADD CONSTRAINT jobstat_pkey PRIMARY KEY (keyjobstat);


--
-- Name: jobtask_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY jobtask
    ADD CONSTRAINT jobtask_pkey PRIMARY KEY (keyjobtask);


--
-- Name: jobtype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY jobtype
    ADD CONSTRAINT jobtype_pkey PRIMARY KEY (keyjobtype);


--
-- Name: jobtypemapping_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY jobtypemapping
    ADD CONSTRAINT jobtypemapping_pkey PRIMARY KEY (keyjobtypemapping);


--
-- Name: mapping_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY mapping
    ADD CONSTRAINT mapping_pkey PRIMARY KEY (keymapping);


--
-- Name: mappingtype_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY mappingtype
    ADD CONSTRAINT mappingtype_pkey PRIMARY KEY (keymappingtype);


--
-- Name: methodperms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY methodperms
    ADD CONSTRAINT methodperms_pkey PRIMARY KEY (keymethodperms);


--
-- Name: notification_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (keynotification);


--
-- Name: notificationdestination_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY notificationdestination
    ADD CONSTRAINT notificationdestination_pkey PRIMARY KEY (keynotificationdestination);


--
-- Name: notificationmethod_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY notificationmethod
    ADD CONSTRAINT notificationmethod_pkey PRIMARY KEY (keynotificationmethod);


--
-- Name: notificationroute_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY notificationroute
    ADD CONSTRAINT notificationroute_pkey PRIMARY KEY (keynotificationuserroute);


--
-- Name: notify_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "notify"
    ADD CONSTRAINT notify_pkey PRIMARY KEY (keynotify);


--
-- Name: notifymethod_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notifymethod
    ADD CONSTRAINT notifymethod_pkey PRIMARY KEY (keynotifymethod);


--
-- Name: notifysent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notifysent
    ADD CONSTRAINT notifysent_pkey PRIMARY KEY (keynotifysent);


--
-- Name: notifywho_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notifywho
    ADD CONSTRAINT notifywho_pkey PRIMARY KEY (keynotifywho);


--
-- Name: pathtemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY pathtemplate
    ADD CONSTRAINT pathtemplate_pkey PRIMARY KEY (keypathtemplate);


--
-- Name: permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY permission
    ADD CONSTRAINT permission_pkey PRIMARY KEY (keypermission);


--
-- Name: phoneno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phoneno
    ADD CONSTRAINT phoneno_pkey PRIMARY KEY (keyphoneno);


--
-- Name: phonetype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phonetype
    ADD CONSTRAINT phonetype_pkey PRIMARY KEY (keyphonetype);


--
-- Name: projectresolution_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY projectresolution
    ADD CONSTRAINT projectresolution_pkey PRIMARY KEY (keyprojectresolution);


--
-- Name: projectstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY projectstatus
    ADD CONSTRAINT projectstatus_pkey PRIMARY KEY (keyprojectstatus);


--
-- Name: renderframe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY renderframe
    ADD CONSTRAINT renderframe_pkey PRIMARY KEY (keyrenderframe);


--
-- Name: schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_pkey PRIMARY KEY (keyschedule);


--
-- Name: service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_pkey PRIMARY KEY (keyservice);


--
-- Name: statusset_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY statusset
    ADD CONSTRAINT statusset_pkey PRIMARY KEY (keystatusset);


--
-- Name: syslog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY syslog
    ADD CONSTRAINT syslog_pkey PRIMARY KEY (keysyslog);


--
-- Name: syslogrealm_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY syslogrealm
    ADD CONSTRAINT syslogrealm_pkey PRIMARY KEY (keysyslogrealm);


--
-- Name: syslogseverity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY syslogseverity
    ADD CONSTRAINT syslogseverity_pkey PRIMARY KEY (keysyslogseverity);


--
-- Name: tasktype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tasktype
    ADD CONSTRAINT tasktype_pkey PRIMARY KEY (keytasktype);


--
-- Name: taskuser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY taskuser
    ADD CONSTRAINT taskuser_pkey PRIMARY KEY (keytaskuser);


--
-- Name: thread_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY thread
    ADD CONSTRAINT thread_pkey PRIMARY KEY (keythread);


--
-- Name: threadcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY threadcategory
    ADD CONSTRAINT threadcategory_pkey PRIMARY KEY (keythreadcategory);


--
-- Name: threadnotify_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY threadnotify
    ADD CONSTRAINT threadnotify_pkey PRIMARY KEY (keythreadnotify);


--
-- Name: thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY thumbnail
    ADD CONSTRAINT thumbnail_pkey PRIMARY KEY (keythumbnail);


--
-- Name: timesheet_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY timesheet
    ADD CONSTRAINT timesheet_pkey PRIMARY KEY (keytimesheet);


--
-- Name: timesheetcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY timesheetcategory
    ADD CONSTRAINT timesheetcategory_pkey PRIMARY KEY (keytimesheetcategory);


--
-- Name: tracker_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tracker
    ADD CONSTRAINT tracker_pkey PRIMARY KEY (keytracker);


--
-- Name: trackercategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trackercategory
    ADD CONSTRAINT trackercategory_pkey PRIMARY KEY (keytrackercategory);


--
-- Name: trackerlog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trackerlog
    ADD CONSTRAINT trackerlog_pkey PRIMARY KEY (keytrackerlog);


--
-- Name: trackerqueue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trackerqueue
    ADD CONSTRAINT trackerqueue_pkey PRIMARY KEY (keytrackerqueue);


--
-- Name: trackerseverity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trackerseverity
    ADD CONSTRAINT trackerseverity_pkey PRIMARY KEY (keytrackerseverity);


--
-- Name: trackerstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY trackerstatus
    ADD CONSTRAINT trackerstatus_pkey PRIMARY KEY (keytrackerstatus);


--
-- Name: userelement_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY userelement
    ADD CONSTRAINT userelement_pkey PRIMARY KEY (keyuserelement);


--
-- Name: usermapping_pkey; Type: CONSTRAINT; Schema: public; Owner: brobison; Tablespace: 
--

ALTER TABLE ONLY usermapping
    ADD CONSTRAINT usermapping_pkey PRIMARY KEY (keyusermapping);


--
-- Name: userrole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY userrole
    ADD CONSTRAINT userrole_pkey PRIMARY KEY (keyuserrole);


--
-- Name: usrgrp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usrgrp
    ADD CONSTRAINT usrgrp_pkey PRIMARY KEY (keyusrgrp);


--
-- Name: x_hostservice_fkeyhost_fkeyservice; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hostservice
    ADD CONSTRAINT x_hostservice_fkeyhost_fkeyservice UNIQUE (fkeyhost, fkeyservice);


--
-- Name: x_annotation_sequence; Type: INDEX; Schema: public; Owner: brobison; Tablespace: 
--

CREATE INDEX x_annotation_sequence ON annotation USING btree ("sequence");


--
-- Name: x_filetracker_fkeyelement; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_filetracker_fkeyelement ON filetracker USING btree (fkeyelement);


--
-- Name: x_filetracker_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_filetracker_name ON filetracker USING btree (name);


--
-- Name: x_filetracker_path; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_filetracker_path ON filetracker USING btree (path);


--
-- Name: x_filetrackerdep_fkeyinput; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_filetrackerdep_fkeyinput ON filetrackerdep USING btree (fkeyinput);


--
-- Name: x_filetrackerdep_fkeyoutput; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_filetrackerdep_fkeyoutput ON filetrackerdep USING btree (fkeyoutput);


--
-- Name: x_host_online_slavestatus; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_host_online_slavestatus ON host USING btree (online, slavestatus);


--
-- Name: x_hosthistory_datetime; Type: INDEX; Schema: public; Owner: brobison; Tablespace: 
--

CREATE INDEX x_hosthistory_datetime ON hosthistory USING btree (datetime);


--
-- Name: x_hosthistory_datetimeplusduration; Type: INDEX; Schema: public; Owner: brobison; Tablespace: 
--

CREATE INDEX x_hosthistory_datetimeplusduration ON hosthistory USING btree (((datetime + duration)));


--
-- Name: x_hosthistory_fkeyhost; Type: INDEX; Schema: public; Owner: brobison; Tablespace: 
--

CREATE INDEX x_hosthistory_fkeyhost ON hosthistory USING btree (fkeyhost);


--
-- Name: x_hosthistory_fkeyjob; Type: INDEX; Schema: public; Owner: brobison; Tablespace: 
--

CREATE INDEX x_hosthistory_fkeyjob ON hosthistory USING btree (fkeyjob);


--
-- Name: x_hostmapping_fkeymapping; Type: INDEX; Schema: public; Owner: brobison; Tablespace: 
--

CREATE INDEX x_hostmapping_fkeymapping ON hostmapping USING btree (fkeymapping);


--
-- Name: x_hostservice_fkeyhost; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_hostservice_fkeyhost ON hostservice USING btree (fkeyhost);


--
-- Name: x_hostservice_fkeyservice; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_hostservice_fkeyservice ON hostservice USING btree (fkeyservice);


--
-- Name: x_job_fkeyusr; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_job_fkeyusr ON job USING btree (fkeyusr) WHERE ((((status = 'new'::text) OR (status = 'ready'::text)) OR (status = 'started'::text)) OR (status = 'done'::text));


--
-- Name: x_job_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_job_status ON job USING btree (status) WHERE ((((status = 'new'::text) OR (status = 'ready'::text)) OR (status = 'started'::text)) OR (status = 'done'::text));


--
-- Name: x_joberror_fkeyjob; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_joberror_fkeyjob ON joberror USING btree (fkeyjob);


--
-- Name: x_jobmax7_fkeyusr; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_jobmax7_fkeyusr ON jobmax USING btree (fkeyusr) WHERE ((((status = 'new'::text) OR (status = 'ready'::text)) OR (status = 'started'::text)) OR (status = 'done'::text));


--
-- Name: x_jobmax7_status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_jobmax7_status ON jobmax USING btree (status) WHERE ((((status = 'new'::text) OR (status = 'ready'::text)) OR (status = 'started'::text)) OR (status = 'done'::text));


--
-- Name: x_jobmax8_status; Type: INDEX; Schema: public; Owner: brobison; Tablespace: 
--

CREATE INDEX x_jobmax8_status ON jobmax8 USING btree (status);


--
-- Name: x_jobtask_fkeyhost; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_jobtask_fkeyhost ON jobtask USING btree (fkeyhost);


--
-- Name: x_jobtask_fkeyjob; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_jobtask_fkeyjob ON jobtask USING btree (fkeyjob);


--
-- Name: x_rangefiletracker_fkeyelement; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_rangefiletracker_fkeyelement ON rangefiletracker USING btree (fkeyelement);


--
-- Name: x_rangefiletracker_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_rangefiletracker_name ON rangefiletracker USING btree (name);


--
-- Name: x_rangefiletracker_path; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_rangefiletracker_path ON rangefiletracker USING btree (path);


--
-- Name: x_schedule_fkeyelement; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_schedule_fkeyelement ON schedule USING btree (fkeyelement);


--
-- Name: x_schedule_fkeyuser; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_schedule_fkeyuser ON schedule USING btree (fkeyuser);


--
-- Name: x_timesheet_fkeyemployee; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_timesheet_fkeyemployee ON timesheet USING btree (fkeyemployee);


--
-- Name: x_timesheet_fkeyproject; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_timesheet_fkeyproject ON timesheet USING btree (fkeyproject);


--
-- Name: x_versionfiletracker_fkeyelement; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_versionfiletracker_fkeyelement ON versionfiletracker USING btree (fkeyelement);


--
-- Name: x_versionfiletracker_name; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_versionfiletracker_name ON versionfiletracker USING btree (name);


--
-- Name: x_versionfiletracker_path; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX x_versionfiletracker_path ON versionfiletracker USING btree (path);


--
-- Name: insert_jobtask; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER insert_jobtask
    BEFORE INSERT ON jobtask
    FOR EACH ROW
    EXECUTE PROCEDURE insert_jobtask();


--
-- Name: job_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER job_update
    BEFORE UPDATE ON job
    FOR EACH ROW
    EXECUTE PROCEDURE job_update();


--
-- Name: jobbatch_update; Type: TRIGGER; Schema: public; Owner: brobison
--

CREATE TRIGGER jobbatch_update
    BEFORE UPDATE ON jobbatch
    FOR EACH ROW
    EXECUTE PROCEDURE job_update();


--
-- Name: joberror_inc; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER joberror_inc
    AFTER INSERT OR UPDATE ON joberror
    FOR EACH ROW
    EXECUTE PROCEDURE joberror_inc();


--
-- Name: jobmax7_update; Type: TRIGGER; Schema: public; Owner: brobison
--

CREATE TRIGGER jobmax7_update
    BEFORE UPDATE ON jobmax7
    FOR EACH ROW
    EXECUTE PROCEDURE job_update();


--
-- Name: jobmax8_update; Type: TRIGGER; Schema: public; Owner: brobison
--

CREATE TRIGGER jobmax8_update
    BEFORE UPDATE ON jobmax8
    FOR EACH ROW
    EXECUTE PROCEDURE job_update();


--
-- Name: jobxsi_update; Type: TRIGGER; Schema: public; Owner: brobison
--

CREATE TRIGGER jobxsi_update
    BEFORE UPDATE ON jobxsi
    FOR EACH ROW
    EXECUTE PROCEDURE job_update();


--
-- Name: update_host; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_host
    BEFORE UPDATE ON host
    FOR EACH ROW
    EXECUTE PROCEDURE update_host();


--
-- Name: update_hostload; Type: TRIGGER; Schema: public; Owner: brobison
--

CREATE TRIGGER update_hostload
    BEFORE UPDATE ON hostload
    FOR EACH ROW
    EXECUTE PROCEDURE update_hostload();


--
-- Name: update_hostservice; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_hostservice
    BEFORE UPDATE ON hostservice
    FOR EACH ROW
    EXECUTE PROCEDURE update_hostservice();


--
-- Name: update_jobtask; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_jobtask
    BEFORE UPDATE ON jobtask
    FOR EACH ROW
    EXECUTE PROCEDURE update_jobtask();


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect postgres

--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'Standard public schema';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect template1

--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON DATABASE template1 IS 'Default template database';


--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'Standard public schema';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

