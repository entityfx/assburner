
CREATE OR REPLACE FUNCTION check_temp_table_exists(tablename text) RETURNS boolean AS
$$
DECLARE
BEGIN
    -- check the table exist in database and is visible
	IF EXISTS(	SELECT n.nspname ,c.relname FROM pg_catalog.pg_class c 
					LEFT JOIN pg_catalog.pg_namespace n ON n.oid=c.relnamespace
					WHERE n.nspname LIKE 'pg_temp_%' AND pg_catalog.pg_table_is_visible(c.oid)
					AND relname=tablename)
			THEN
		RETURN TRUE;
	END IF;
	RETURN FALSE;
 END;
$$ LANGUAGE 'plpgsql' VOLATILE;