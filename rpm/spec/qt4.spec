
# Fedora Review
# http://bugzilla.redhat.com/188180

#define beta rc1
#define preview -preview
#define rel %{beta}

#define snap 20060927
#define beta snapshot-%{snap}
#define rel  %{snap}

Summary: Qt toolkit
Name:	 qt4
Version: 4.2.1
Release: 99%{?dist}

License: GPL/QPL
Group: 	 System Environment/Libraries
Url:   	 http://www.trolltech.com/products/qt/
%if "%{?snap:1}" == "1"
Source0: ftp://ftp.trolltech.com/qt/snapshots/qt-x11-opensource-src-%{version}%{?beta:-%{beta}}.tar.bz2
%else
Source0: ftp://ftp.trolltech.com/qt/source/qt-x11%{?preview}-opensource-src-%{version}%{?beta:-%{beta}}.tar.gz
%endif
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# search for assistant-qt4 instead of (qt3's) assistant in $PATH 
Patch1: qt-x11-opensource-src-4.1.2-assistant4.patch
Patch2: qt-x11-opensource-src-4.2.1.psql_reconnect_fix.patch

Source10: qt4-wrapper.sh

Source20: assistant.desktop
Source21: designer.desktop
Source22: linguist.desktop
Source23: qtdemo.desktop
Source24: qtconfig.desktop

## optional plugin bits
# set to -no-sql-<driver> to disable
# set to -qt-sql-<driver> to enable *in* qt library
#if "%{?fedora}" > "3" || "%{?rhel}" > "3"
%define mysql  -plugin-sql-mysql
#endif
%define odbc   -plugin-sql-odbc 
%define psql   -plugin-sql-psql 
%define sqlite -plugin-sql-sqlite 

# undefine to disable these
#%define nas     -system-nas-sound
%undefine nas
# watch http://bugzilla.redhat.com/207923 so we may be able to include this on FC-5 someday
%if "%{?fedora}" > "5"
%define qdbus	-qdbus
BuildRequires: dbus-devel >= 0.62
%endif

%define qt_dirname %{name} 
%define qtdir %{_libdir}/%{qt_dirname}
# See http://bugzilla.redhat.com/196901
%define qt_datadir %{_datadir}/%{qt_dirname}
%define qt_docdir %{_docdir}/%{qt_dirname}
%define qt_headerdir %{_includedir} 
# Be careful about ever changing this, some 3rd-party libs install here (like qt4-qsa)
#define qt_libdir %{qtdir}/%{_lib}
%define qt_libdir %{_libdir}
%define qt_sysconfdir %{_sysconfdir}
%define qt_translationdir %{qt_datadir}/translations

%if "%{qt_libdir}" != "%{_libdir}"
# needed for runtime, as well as pre,post
Prereq:  /etc/ld.so.conf.d
%endif

BuildRequires: cups-devel
BuildRequires: desktop-file-utils
BuildRequires: findutils
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: libjpeg-devel
BuildRequires: libmng-devel
BuildRequires: libpng-devel
BuildRequires: libungif-devel
BuildRequires: freetype-devel
BuildRequires: zlib-devel
BuildRequires: glib2-devel

## In theory, should be as simple as:
#define x_deps libGL-devel libGLU-devel
## but, "xorg-x11-devel: missing dep on libGL/libGLU" - http://bugzilla.redhat.com/211898 
%define x_deps xorg-x11-devel xorg-x11-Mesa-libGL xorg-x11-Mesa-libGLU
%if "%{?fedora}" > "4" || "%{?rhel}" > "4"
%define x_deps libICE-devel libSM-devel libXcursor-devel libXext-devel libXfixes-devel libXft-devel libXi-devel libXinerama-devel libXrandr-devel libXrender-devel libXt-devel libX11-devel xorg-x11-proto-devel libGL-devel libGLU-devel
%endif
BuildRequires: %{x_deps} %{?x_deps_GL_hack}

%if "%{?nas}" == "-system-nas-sound"
BuildRequires: nas-devel
%endif

%if "%{?mysql}" != "-no-sql-mysql"
# mysql-devel < 4 build fails on 4.1.3+, not sure why... yet.  -- Rex
BuildRequires: mysql-devel >= 4.0
# If we get mysql3 to work, use mysql_config --cflags instead -- Rex
%global mysql_include $(mysql_config --include 2> /dev/null || echo "-I%{_includedir}/mysql")
%global mysql_libs    $(mysql_config --libs 2> /dev/null || echo "-L%{_libdir}/mysql")
%global mysql_ldflags $(echo %{mysql_libs} | perl -pi -e "s, -l/?\\\S+,,g")
%endif

%if "%{?psql}" != "-no-sql-psql"
BuildRequires: postgresql-devel
%endif

%if "%{?odbc}" != "-no-sql-odbc"
BuildRequires: unixODBC-devel
%endif

%if "%{?sqlite:1}" != "-no-sql-sqlite"
# FIXME: currently BR not used, uses 3rd-party internal sources
#BuildRequires: sqlite-devel
%endif

BuildConflicts: qt4-devel

Obsoletes: %{name}-config < %{version}-%{release}
Provides:  %{name}-config = %{version}-%{release}

%description 
Qt is a software toolkit for developing applications.

This package contains base tools, like string, xml, and network
handling.

%package devel
Summary: Development files for the Qt toolkit
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: %{name}-x11 = %{version}-%{release}
Requires: %{x_deps}
Requires: libpng-devel
Requires: libjpeg-devel
Requires: pkgconfig
# Short-lived pkg, temporary 
Obsoletes: %{name}-debug < %{version}-%{release}
Obsoletes: %{name}-designer < %{version}-%{release}
Provides:  %{name}-designer = %{version}-%{release}
%description devel
This package contains the files necessary to develop
applications using the Qt toolkit.  Includes:
Qt Linguist

%package doc
Summary: API documentation, demos and example programs for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
Provides: %{name}-assistant = %{version}-%{release}
# For docs -> doc rename, temporary, since it was a short-lived subpkg
#Obsoletes: %{name}-docs < %{version}-%{release}
#Provides:  %{name}-docs = %{version}-%{release}
%description doc
%{summary}.  Includes:
Qt Assistant, Qt Demo

%package odbc 
Summary: ODBC driver for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: %{name}-ODBC < %{version}-%{release}
Provides:  %{name}-ODBC = %{version}-%{release}
%description odbc 
%{summary}.

%package mysql 
Summary: MySQL driver for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: %{name}-MySQL < %{version}-%{release}
Provides:  %{name}-MySQL = %{version}-%{release}
%description mysql 
%{summary}.

%package postgresql 
Summary: PostgreSQL driver for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: %{name}-PostgreSQL < %{version}-%{release}
Provides:  %{name}-PostgreSQL = %{version}-%{release}
%description postgresql 
%{summary}.

%package sqlite 
Summary: SQLite driver for Qt's SQL classes
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
Obsoletes: %{name}-SQLite < %{version}-%{release}
Provides:  %{name}-SQLite = %{version}-%{release}
%description sqlite 
%{summary}.

%package x11
Summary: Qt GUI-related libraries
Group: System Environment/Libraries
Requires: %{name} = %{version}-%{release}
%description x11
Qt libraries which are used for drawing widgets and OpenGL items.


%prep
%setup -q -n qt-x11%{?preview}-opensource-src-%{version}%{?beta:-%{beta}}

%patch1 -p1 -b .assistant4
%patch2 -p1 

# drop -fexceptions from $RPM_OPT_FLAGS
RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS | sed 's|-fexceptions||g'`

## Looks like we don't need this anymore -- Rex
# add -fno-strict-aliasing, for now (fc6+)
#if "%{?fedora}" > "5"
#RPM_OPT_FLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
#endif

# use $RPM_OPT_FLAGS for our platform
%if "%{_lib}" == "lib64"
%define platform linux-g++-64
%else
%define platform linux-g++
%endif
sed -i -e "s|-O2|$RPM_OPT_FLAGS|g" mkspecs/%{platform}/qmake.conf
sed -i -e "s|-O2|$RPM_OPT_FLAGS|g" mkspecs/common/*.conf ||:

# undefine QMAKE_STRIP, so we get useful -debuginfo pkgs
sed -i -e "s|^QMAKE_STRIP.*=.*|QMAKE_STRIP             =|" mkspecs/linux-g++*/qmake.conf
sed -i -e "s|^QMAKE_STRIP.*=.*|QMAKE_STRIP             =|" mkspecs/common/*.conf ||:

# set correct lib path
if [ "%{_lib}" == "lib64" ] ; then
  sed -i -e "s,/usr/lib /lib,/usr/%{_lib} /%{_lib},g" config.tests/{unix,x11}/*.test
  sed -i -e "s,/lib /usr/lib,/%{_lib} /usr/%{_lib},g" config.tests/{unix,x11}/*.test
fi


%build

# There's a bug in the qdbus build somewhere, but we'll hack around it here for now -- Rex
%if "%{?qdbus}" == "-qdbus"
LD_RUN_PATH=$(pwd)/lib;export LD_RUN_PATH
%endif

# build shared, threaded (default) libraries
echo yes | ./configure -v \
  -prefix %{qtdir} \
  -datadir %{qt_datadir} \
  -docdir %{qt_docdir} \
  -headerdir %{qt_headerdir} \
  -libdir %{qt_libdir} \
  -sysconfdir %{qt_sysconfdir} \
  -translationdir %{qt_translationdir} \
  -platform %{platform} \
  -release \
  -shared \
  -cups \
  -no-exceptions \
  -fontconfig \
  -largefile \
  -qt-gif \
  -no-reduce-exports \
  -no-rpath \
  -no-separate-debug-info \
  -sm \
  -stl \
  -system-libmng \
  -system-libpng \
  -system-libjpeg \
  -system-zlib \
  -tablet \
  -xcursor \
  -xfixes \
  -xinerama \
  -xshape \
  -xrandr \
  -xrender \
  -xkb \
  -glib \
  %{?qdbus} %{!?qdbus:-no-qdbus} \
  %{?nas} %{!?nas:-no-nas-sound} \
  %{?mysql} %{?mysql_include} %{?mysql_ldflags} \
  %{?psql} \
  %{?odbc} \
  %{?sqlite} 

make %{?_smp_mflags}


%install
rm -rf %{buildroot}

make install INSTALL_ROOT=%{buildroot}

# Add desktop file(s)
desktop-file-install \
  --dir %{buildroot}%{_datadir}/applications \
  --vendor="%{name}" \
  --add-category="X-Fedora" \
  %{SOURCE20} %{SOURCE21} %{SOURCE22} %{SOURCE23} %{SOURCE24}

## pkg-config
# strip extraneous dirs/libraries -- Rex
# safe ones
for dep in -laudio -ldbus-1 -lglib-2.0 -lmng -ljpeg -lpng -lz -lfreetype -lm %{?mysql_ldflags} \
  -L%{_builddir}/qt-x11%{?preview}-opensource-src-%{version}%{?beta:-%{beta}}/lib ; do
  sed -i -e "s|$dep ||g" %{buildroot}%{qt_libdir}/lib*.la ||:
  sed -i -e "s|$dep ||g" %{buildroot}%{qt_libdir}/*.pc
  sed -i -e "s|$dep ||g" %{buildroot}%{qt_libdir}/*.prl
done
# not-so-safe, but we can try -- Rex
# -L/usr/X11R6/%{_lib}
for dep in -lXrender -lXrandr -lXcursor -lXinerama -lXi -lXft -lfontconfig -lXext -lX11 -lSM -lICE -ldl -lpthread ; do
  sed -i -e "s|$dep ||g" %{buildroot}%{qt_libdir}/lib*.la ||:
  sed -i -e "s|$dep ||g" %{buildroot}%{qt_libdir}/*.pc
  sed -i -e "s|$dep ||g" %{buildroot}%{qt_libdir}/*.prl
done
sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" %{buildroot}%{qt_libdir}/*.prl

mkdir -p %{buildroot}%{_libdir}/pkgconfig/
mv %{buildroot}/%{qt_libdir}/*.pc %{buildroot}%{_libdir}/pkgconfig/

# -doc make symbolic link to qt_docdir
rm -rf %{buildroot}%{qtdir}/doc
ln -s  ../../share/doc/%{name} %{buildroot}%{qtdir}/doc

# put LICENSE.* files in %%qtdir, some apps' configure scripts expect to find them here
install -p -m644 LICENSE.* %{buildroot}%{qtdir}/

## Make symlinks in %%_bindir
mkdir -p %{buildroot}%{_bindir}
pushd %{buildroot}%{qtdir}/bin 
for i in *; do
  case "${i}" in
    assistant|designer|linguist|lrelease|lupdate|moc|qmake|qtconfig|qtdemo|uic)
      LINK="${i}-qt4"
      ln -s "${i}" "%{buildroot}%{qtdir}/bin/${LINK}"
      ;;
    *)
      LINK="${i}"
      ;;
  esac
  install -p -m755 -D %{SOURCE10} %{buildroot}%{_bindir}/${LINK}
done
popd

# _debug lib symlinks (see bug #196513)
pushd %{buildroot}%{qt_libdir}
for lib in libQt*.so ; do
  ln -s $lib $(basename $lib .so)_debug.so
done
for lib in libQt*.a ; do
  ln -s $lib $(basename $lib .a)_debug.a
done
popd

# .la files, die, die, die.
rm -f %{buildroot}%{qt_libdir}/lib*.la

%if "%{qt_libdir}" != "%{_libdir}"
mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{qt_libdir}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf
%endif


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc README* 
%{!?beta:%doc OPENSOURCE-NOTICE.TXT}
%{qtdir}/LICENSE.*
%if "%{qt_libdir}" != "%{_libdir}"
/etc/ld.so.conf.d/*
%dir %{qt_libdir}
%endif
%dir %{qtdir}
%dir %{qtdir}/bin/
%dir %{qt_datadir}
%{qt_libdir}/libQtCore.so.*
%if "%{?qdbus}" == "-qdbus"
%{qt_libdir}/libQtDBus.so.*
%endif
%{qt_libdir}/libQtNetwork.so.*
%{qt_libdir}/libQtSql.so.*
%{qt_libdir}/libQtTest.so.*
%{qt_libdir}/libQtXml.so.*
%dir %{qtdir}/plugins/
%dir %{qtdir}/plugins/sqldrivers/
%{qt_translationdir}/

%files x11 
%defattr(-,root,root,-)
%{qt_libdir}/libQt3Support.so.*
%{qt_libdir}/libQtGui.so.*
%{qt_libdir}/libQtOpenGL.so.*
%{qt_libdir}/libQtSvg.so.*
%if "%{version}" >= "4.2.0"
%{qt_libdir}/libQtAssistantClient.so.*
%endif
%{qtdir}/plugins/*
%exclude %{qtdir}/plugins/designer
%exclude %{qtdir}/plugins/sqldrivers
#-config bits
%{qtdir}/bin/qt*config*
%{_bindir}/qt*config*
%{_datadir}/applications/*qtconfig*.desktop

%files devel
%defattr(-,root,root,-)
%{qtdir}/bin/lrelease*
%{qtdir}/bin/lupdate*
%{qtdir}/bin/moc*
%if "%{version}" < "4.2.0"
%{qtdir}/bin/qm2ts*
%{_bindir}/qm2ts*
%endif
%if "%{?qdbus}" == "-qdbus"
%{qtdir}/bin/qdbus*
%{_bindir}/qdbus*
%endif
%{qtdir}/bin/pixeltool*
%{qtdir}/bin/qmake*
%{qtdir}/bin/qt3to4
%{qtdir}/bin/rcc*
%{qtdir}/bin/uic*
%{_bindir}/lrelease*
%{_bindir}/lupdate*
%{_bindir}/pixeltool*
%{_bindir}/moc*
%{_bindir}/qmake*
%{_bindir}/qt3to4
%{_bindir}/rcc*
%{_bindir}/uic*
%if "%{qt_headerdir}" != "%{_includedir}"
%dir %{qt_headerdir}/
%endif
%{qt_headerdir}/*
%{qt_datadir}/mkspecs/
%{qt_datadir}/phrasebooks/
%{qt_datadir}/q3porting.xml
%{qt_libdir}/libQtDesigner.so.*
%{qt_libdir}/libQtDesignerComponents.so.*
%{qt_libdir}/libQt*.so
# remaining static lib: libQtUiTools.a 
%{qt_libdir}/libQt*.a
%{qt_libdir}/libQt*.prl
%{_libdir}/pkgconfig/*.pc
# Qt designer
%{_bindir}/designer*
%{qtdir}/bin/designer*
%dir %{qtdir}/plugins
%{qtdir}/plugins/designer/
%{_datadir}/applications/*designer*.desktop
# Qt Linguist
%{qtdir}/bin/linguist*
%{_bindir}/linguist*
%{_datadir}/applications/*linguist*.desktop

%files doc
%defattr(-,root,root,-)
%dir %{qt_docdir}/
%{qt_docdir}/html
%{qtdir}/doc
%{qtdir}/demos/
%{qtdir}/examples/
# Qt Assistant
%{qtdir}/bin/assistant*
%{_bindir}/assistant*
%{_datadir}/applications/*assistant*.desktop
# Qt Demo
%{qtdir}/bin/qt*demo*
%{_bindir}/qt*demo*
%{_datadir}/applications/*qtdemo*.desktop

%if "%{?odbc}" == "-plugin-sql-odbc"
%files odbc 
%defattr(-,root,root,-)
%{qtdir}/plugins/sqldrivers/libqsqlodbc*
%endif

%if "%{?psql}" == "-plugin-sql-psql"
%files postgresql 
%defattr(-,root,root,-)
%{qtdir}/plugins/sqldrivers/libqsqlpsql*
%endif

%if "%{?mysql}" == "-plugin-sql-mysql"
%files mysql 
%defattr(-,root,root,-)
%{qtdir}/plugins/sqldrivers/libqsqlmysql*
%endif

%if "%{?sqlite}" == "-plugin-sql-sqlite"
%files sqlite 
%defattr(-,root,root,-)
%{qtdir}/plugins/sqldrivers/libqsqlite*
%endif


%changelog
* Mon Oct 23 2006 Rex Dieter 4.2.1-2
- use repun upstream 4.2.1 tarball
- fix pre-modular-X libGL/libGLU deps (#211898)

* Sun Oct 22 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.1-1
- 4.2.1

* Wed Oct 04 2006 Rex Dieter 4.2.0-1
- 4.2.0(final)

* Thu Sep 28 2006 Kevin Kofler <Kevin@tigcc.ticalc.org> 4.2.0-0.9.20060927
- update to 4.2.0-snapshot-20060927
- update QDBus executable names
- -x11: exclude plugins/designer (which belong to -devel)
- BuildConflicts: qt4-devel
- drop -fno-strict-aliasing hack (fc5+)

* Wed Sep 27 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.8.rc1
- qtconfig.desktop: Categories=+AdvancedSettings;Settings

* Fri Sep 08 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.7.rc1
- 4.2.0-rc1

* Fri Aug 28 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.6.20060821
- update to 4.2.0-snapshot-20060821 (same as today's qt-copy)
- -no-separate-debug-info
- - ./configure -xfixes, BR: libXfixes-devel

* Mon Aug 07 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.5.tp1
- fix empty -debuginfo
- use $RPM_OPT_FLAGS

* Thu Jul 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.4.tp1
- strip -laudio, -ldbus-1, -lglib-2.0 from .pc files

* Thu Jul 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.3.tp1
- -no-reduce-exports (for now)
- -fno-strict-aliasing (fc5+)

* Fri Jul 07 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.2.tp1
- -system-nas-sound, BR: nas-devel (bug # 197937)
- -qdbus (fc6+, BR: dbus-devel >= 0.62)
- -glib (BR: glib2-devel)

* Fri Jun 30 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.2.0-0.1.tp1
- 4.2.0-tp1 (technology preview 1)

* Thu Jun 29 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-5
- make FHS-friendly (bug #196901)
- cleanup %%_bindir symlinks, (consistently) use qt4 postfix

* Wed Jun 28 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-4
- x11: split-out gui(x11) from non-gui bits (bug #196899)

* Mon Jun 26 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-3
- -debug: drop, adds nothing over -debuginfo, make lib..._debug 
  symlinks instead (bug #196513)
- assistant.desktop: fix tooltip (bug #197039)

* Mon Jun 26 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-2
- -devel: include -debug libs (bug #196513)
- -devel: move libQtDesigner here
- -config: mash into main pkg, should be multilib friendly now

* Fri Jun 23 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.4-1
- 4.1.4

* Tue Jun 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-9
- make each sql plugin optional

* Fri Jun 09 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-8
- qmake.conf: undefine QMAKE_STRIP to get useful -debuginfo (bug #193602)
- move (not symlink) .pc files into %%_libdir/pkgconfig

* Thu Jun 08 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-7
- *really* fix qt4-wrapper.sh for good this time.

* Mon May 29 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-6
- make qt4-wrapper.sh use rpm when pkg-config/qt4-devel isn't
  installed (#193369)

* Fri May 26 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-5
- strip -lXi from .pc files (#193258)
- simplify sql plugin builds via %%buildSQL macro
- -libdir %%qt_libdir 

* Wed May 24 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-4
- move (most) %%dir ownership (back) to main pkg

* Sun May 21 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-3
- fix %%mysql_libs macro

* Sat May 20 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-2
- -mysql: use mysql_config for setting cflags/ldflags.
- -mysql: BR: mysql-devel > 4.0

* Sat May 20 2006 Laurent Rineau <laurent.rineau__fc_extra@normalesup.org>
- Fix the last reference to %{qtdir}/lib: use %{_lib} instead of "lib".
- Fix the ownership of subpackages: they need to own parents of directories they install files in.

* Fri May 19 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.3-1
- 4.1.3
- %%qtdir/lib/*.pc -> %%qtdir/%%_lib/*.pc 
  (hopefully, the last hardcoded reference to %%qtdir/lib)

* Fri May 19 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-20
- fix some unowned dirs
- try harder to purge %%builddir from .pc,.prl files
- -docdir %%_docdir/%%name-doc-%%version, since we use %%doc macro in main pkg
- -doc: own %%qt_docdir
- use qt4-wrapper.sh to ensure launch of qt4 versions of apps that
  (may) overlap with those from qt3 
- use %%qtdir/%%_lib in ld.so.conf.d/*.conf files too

* Tue May 16 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-19
- drop libQtAssistantClient,libQtUiTools shlib patches

* Tue May 16 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-18
- %%_bindir symlinks: qtconfig4 -> qt4config, qtdemo4 -> qt4demo
- -libdir %%qtdir/%%_lib, simplifies %%_lib != lib case
- -docdir %%_docdir/%%name-%%version
- build shared versions of libQtAssistantClient,libQtUiTools too
- strip extraneous -L paths, libs from *.prl files too

* Tue May 16 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-17
- .desktop: Qt -> Qt4, and Comment= (where missing)
- -devel: include -designer here, Obsoletes/Provides: %%name-designer.
   It's small, simplifies things... one less subpkg to worry about.
- -doc: include %%qtdir/doc symlink here
- -docdir %%_docdir/%%name-doc-%%version

* Mon May 15 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-16
- set/use RPM_OPT_FLAGS only for our platform
- (really) don't give %%_bindir symlink for qt3to4 another "4" suffix
- don't add 4 suffix to uic3, rcc (they don't conflict with qt(3)-devel)
- -devel: add  linguist.desktop
- -doc: move assistant here, Provides: %%{name}-assistant, add assistant.desktop
- -doc: add qtdemo.desktop
- -doc: Requires qt4 (instead of qt4-devel)
- assistant4.patch: search for assistant4 instead of (qt3's) assistant in $PATH 
- -qtconfig: add qtconfig.desktop
- updated %%sumaries to mention where (some) tools are, including assistant, linguist,
  qtdemo

* Mon May 15 2006 Laurent Rineau <laurent.rineau__fc_extra@normalesup.org> - 4.1.2-15
- Rename -docs to -doc.
- Files in the -doc subpackage are no longer in %%doc.
- Move qtdemo to the subpackage -doc.
- Fix symlinks in %%{_bindir}.
- Only modify mkspecs/linux-g++*/qmake.conf, instead of all mkspecs/*/qmake.conf.

* Sun May 14 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-14
- remove MapNotify from .desktop file(s).
- install -m644 LICENSE.*
- -docs: don't mark examples as %doc
- drop unused %%debug macro

* Sat May 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-13
- include unpackaged pkgconfig files

* Sat May 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-12
- fix typos so it actually builds.

* Sat May 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-11
- drop optional ld.so.conf.d usage, make mandatory
- make %%_bindir symlinks to all %%qtdir/bin stuff (even qt3to4)
- pkgconfig files: hardlinks -> relative symlinks, strip -L%{_libdir}/mysql
  and -L%%{_builddir}/qt-x11-opensource-src-%%version/lib
- cleanup/simplify Summary/%%description entries
- $RPM_BUILD_ROOT -> %%buildroot, $RPM_BUILD_DIR -> %%_builddir

* Sat May 13 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-10
- cleanup/simplify license bits, include LICENSE.QPL
- drop unused -styles/-Xt subpkg reference
- drop unused motif extention bits
- drop initialpreference from .deskstop files

* Fri May 12 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-9
- drop reference to non-existent config.test/unix/checkavail

* Fri May 12 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-8
- simplify build* macros
- lower-case all subpkgs (ie, -MySQL -> -mysql )
- drop BR: perl, sed

* Thu May 11 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-7
- rework %%post/%%postun, mostly to placate rpmlint
- drop Prefix:
- drop use of qt4.(sh|csh), they're empty atm anyway
- use Source'd designer.desktop (instead of inline cat/echo)
- symlinks to %%_bindir: qmake4, designer4, qtconfig4
- drop qtrc, qt4 doesn't use it.
- -docs subpkg for API html docs, demos, examples.
- BR: libXcursor-devel libXi-devel (fc5+)

* Thu Apr 27 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-6
- devel: Requires: pkgconfig

* Sat Apr 15 2006 Simon Perreault <nomis80@nomis80.org> 4.1.2-5
- Disable C++ exceptions.

* Mon Apr 10 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-4
- qt4.(sh|csh): place-holders only, don't define QTDIR (and QTLIB)
  as that (potentially) conflicts with qt-3.x.

* Thu Apr 06 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-2
- -devel: Drop (artificial) Conflicts: qt-devel
- fix %%ld_so_conf_d usage
- %%qtdir/%%_lib symlink

* Wed Apr 05 2006 Rex Dieter <rexdieter[AT]users.sf.net> 4.1.2-1
- drop Epoch
- cleanup (a lot!)

* Tue Dec 20 2005 Than Ngo <than@redhat.com> 1:4.1.0-0.1
- update to 4.1.0

* Fri Sep 09 2005 Than Ngo <than@redhat.com> 1:4.0.1-0.1
- update to 4.0.1

