#!/bin/sh

export QTDIR=/usr/lib64/qt4/
export QMAKESPEC=linux-g++
export PATH=$QTDIR/bin:$PATH
export PATH=/opt/centos/devtoolset-1.0/root/usr/bin/:$PATH
export MAKEFLAGS=-j4

OUTFILE=${0//sh/output}
UPLOAD_HOST=repo.blur.com
REPO_PATH=/mnt/repository/blur/6/

svn up 
stdbuf -o 0 python build.py ${*:-allrpms install} | tee $OUTFILE
grep 'Wrote.*rpm$' $OUTFILE | sed 's/Wrote:\s//' > rpms_built.txt

while read line; do
	echo $line
	if [[ $line == *noarch.rpm ]]; then
		scp $line $UPLOAD_HOST:$REPO_PATH/noarch/
	fi
	
	if [[ $line == *x86_64.rpm ]]; then
		scp $line $UPLOAD_HOST:$REPO_PATH/x86_64/
	fi
done < rpms_built.txt

ssh repo.blur.com "cd /mnt/repository/blur/6 && createrepo noarch && createrepo x86_64"
