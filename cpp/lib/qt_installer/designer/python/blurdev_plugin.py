class Logger:
	def __init__( self, stdhandle, logfile, _print = True ):
		self._stdhandle = stdhandle
		self._logfile = logfile
		self._print = _print
		# clear the log file
		open(logfile, 'w').close()

		
	def flush( self ):
		self._stdhandle.flush()
		
	def write( self, msg ):
		f = open( self._logfile, 'a' )
		f.write( msg )
		f.close()
		if self._print:
			self._stdhandle.write( msg )
			
import sys, datetime, os
path = os.path.join(os.path.split(__file__)[0], r'pluginlog.log')
sys.stderr = Logger( sys.stderr, path, False )
sys.stdout = Logger( sys.stdout, path, False )
print '--------- Date: %s Version: %s ---------' % (datetime.datetime.today(), sys.version)
#-------------------------------------------------------------------------------------------------------------
# initialize the plugins and import them

import blurdev.gui.designer
blurdev.gui.designer.init()
from blurdev.gui.designer import *