
#include <qapplication.h>
#include <qclipboard.h>
#include <qdebug.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qlistview.h>
#include <qpushbutton.h>
#include <qtableview.h>
#include <qtextedit.h>
#include <qtextstream.h>
#include <qtreeview.h>
#include <qstandarditemmodel.h>

#include "modelcopydialog.h"
#include "modeliter.h"

ModelCopyDialog::ModelCopyDialog( QWidget * parent )
: QDialog( parent )
, mModel( 0 )
, mView( 0 )
, mColumnListModel( 0 )
, mSingleColumn( false )
{
	init();
}

ModelCopyDialog::ModelCopyDialog( QAbstractItemModel * model, QWidget * parent )
: QDialog( parent )
, mModel( model )
, mView( 0 )
, mColumnListModel( 0 )
, mSingleColumn( false )
{
	init();
}

ModelCopyDialog::ModelCopyDialog( QAbstractItemView * view, QWidget * parent )
: QDialog( parent ? parent : view )
, mModel( view ? view->model() : nullptr )
, mView( view )
, mColumnListModel( 0 )
, mSingleColumn( false )
{
	init();
}

void ModelCopyDialog::init()
{
	setupUi(this);
	
	QPushButton * copyButton = new QPushButton( "&Copy To Clipboard" );
	copyButton->setShortcut( QKeySequence::Copy );
	connect( copyButton, SIGNAL(clicked()), SLOT(copyToClipboard()) );
	mButtonBox->addButton( copyButton, QDialogButtonBox::AcceptRole );
	
	QPushButton * saveButton = new QPushButton( "&Save to File" );
	saveButton->setShortcut( QKeySequence::Save );
	connect( saveButton, SIGNAL(clicked()), SLOT(saveToFile()) );
	mButtonBox->addButton( saveButton, QDialogButtonBox::ActionRole );

	QPushButton * previewButton = new QPushButton( "&Preview" );
	previewButton->setShortcut( QKeySequence::Paste );
	connect( previewButton, SIGNAL(clicked()), SLOT(preview()) );
	mButtonBox->addButton( previewButton, QDialogButtonBox::ActionRole );
	
	connect( mCheckSelectedButton, SIGNAL(clicked()), SLOT(checkSelected()) );
	connect( mUncheckSelectedButton, SIGNAL(clicked()), SLOT(uncheckSelected()) );
	
	/* See if we can extract a QHeaderView from the view, to
	 * duplicate the current column visibility. */
	QHeaderView * header = nullptr;
	if( mView ) {
		if( QTableView * tv = qobject_cast<QTableView*>(mView) )
			header = tv->horizontalHeader();
		else if( QTreeView * tv = qobject_cast<QTreeView*>(mView) )
			header = tv->header();
	}

	/* Create the model and items for the column selection list */
	mColumnListModel = new QStandardItemModel( mColumnList );
	connect( mColumnListModel, SIGNAL(itemChanged(QStandardItem*)), SLOT(columnItemChanged(QStandardItem*)) );
	mColumnListModel->invisibleRootItem()->setDropEnabled( true );

	int checkedCount = 0;
	if( mModel ) {
		for( int i = 0, end = mModel->columnCount(); i < end; ++i ) {
			QStandardItem * item = new QStandardItem(mModel->headerData(i, Qt::Horizontal).toString());
			item->setCheckable(true);
			const bool checked = (header ? !header->isSectionHidden(i) : true);
			if( checked ) checkedCount++;
			item->setCheckState( checked ? Qt::Checked : Qt::Unchecked );
			item->setData( i );
			item->setDropEnabled( false );
			mColumnListModel->appendRow( item );
		}
	}
	
	mSingleColumn = checkedCount == 1;
	
	/* Set the model to the QListView */
	mColumnList->setModel(mColumnListModel);

	/* Drag and drop to reorder columns */
	mColumnList->setDragDropMode(QListView::InternalMove);
	mColumnList->setDragDropOverwriteMode(false);
	mColumnList->setDefaultDropAction(Qt::MoveAction);
	mColumnList->setDropIndicatorShown(true);
	
	/* Quicker to be able to move multiple items at once */
	mColumnList->setSelectionMode(QListView::ExtendedSelection);
}

void ModelCopyDialog::extractModelData( QTextStream & ts ) const
{
	if( !mModel ) return;
	
	const QString columnSep = mColumnSeparatorEdit->text();
	const QString rowSep = mRowSeparatorEdit->text().replace("\\r","\r").replace("\\n","\n").replace("\\t","\t");
	bool needRowSep = false;
	QList<int> columns;
	{
		QStringList headers;
		const bool incHeaders = mIncludeHeadersCheck->isChecked();
		for( ModelIter it(mColumnListModel, ModelIter::Checked); it.isValid(); ++it ) {
			columns.append( (*it).data(Qt::UserRole+1).toInt() );
			if( incHeaders )
				headers += (*it).data().toString();
		}
		if( incHeaders ) {
			ts << headers.join( columnSep );
			needRowSep = true;
		}
	}
	for( ModelIter it(mModel, mSelectedRowsRadio->isChecked() ? ModelIter::Selected : ModelIter::None, mView ? mView->selectionModel() : 0); it.isValid(); ++it ) {
		QModelIndex idx(*it);
		QStringList cols;
		foreach( int col, columns )
			cols += idx.sibling(idx.row(),col).data().toString();
		if( needRowSep )
			ts << rowSep;
		ts << cols.join( columnSep );
		needRowSep = true;
	}
	if( columns.size() > 1 && needRowSep )
		ts << rowSep;
}

QString ModelCopyDialog::extractModelData() const
{
	QString ret;
	{
		QTextStream ts(&ret,QIODevice::WriteOnly);
		extractModelData(ts);
	}
	return ret;
	
}

void ModelCopyDialog::preview()
{
	QTextEdit * edit = new QTextEdit(this);
	edit->setWindowFlags( Qt::Dialog );
	edit->setAttribute( Qt::WA_DeleteOnClose );
	edit->setText( extractModelData() );
	edit->show();
}

void ModelCopyDialog::copyToClipboard()
{
	QApplication::clipboard()->setText(extractModelData());
}

void ModelCopyDialog::saveToFile( const QString & fileName )
{
	QFile file(fileName);
	if( file.open( QFile::ReadWrite ) ) {
		QTextStream ts(&file);
		extractModelData(ts);
	}
}

void ModelCopyDialog::saveToFile()
{
	saveToFile( QFileDialog::getSaveFileName( parentWidget() ) );
}

void ModelCopyDialog::columnItemChanged( QStandardItem *)
{
	const bool oldSingleColumn = mSingleColumn;
	mSingleColumn = ModelIter::collect(mColumnListModel, ModelIter::Checked).size() == 1;
	
	if( mSingleColumn == oldSingleColumn ) return;
	
	mColumnSeparatorEdit->setEnabled( !mSingleColumn );
	mIncludeHeadersCheck->setChecked( !mSingleColumn );
	
	QString rowSep("\\n"), colSep(",");
	if( mSingleColumn )
		qSwap(rowSep,colSep);

	if( mRowSeparatorEdit->text() == colSep )
		mRowSeparatorEdit->setText( rowSep );
}

void ModelCopyDialog::checkSelected()
{
	for( ModelIter it(mColumnListModel, ModelIter::Selected, mColumnList->selectionModel()); it.isValid(); ++it )
		mColumnListModel->itemFromIndex(*it)->setCheckState( Qt::Checked );
}

void ModelCopyDialog::uncheckSelected()
{
	for( ModelIter it(mColumnListModel, ModelIter::Selected, mColumnList->selectionModel()); it.isValid(); ++it )
		mColumnListModel->itemFromIndex(*it)->setCheckState( Qt::Unchecked );
}

ModelCopyShortcut::ModelCopyShortcut( QAbstractItemView * view, QWidget * parent )
: QShortcut( QKeySequence("Ctrl+Shift+C"/*QKeySequence::Copy*/), parent ? parent : view )
, mView( view )
{
	connect( this, SIGNAL(activated()), SLOT(showCopyDialog()) );
	setContext( Qt::WidgetWithChildrenShortcut );
}

void ModelCopyShortcut::showCopyDialog()
{
	if( mView )
		ModelCopyDialog(mView, qobject_cast<QWidget*>(parent())).exec();
}

