
#include "intervaledit.h"

IntervalEdit::IntervalEdit( Interval i, QWidget * parent )
: QLineEdit( parent )
, mValid( false )
{
	init();
	setInterval(i);
}

IntervalEdit::IntervalEdit( QWidget * parent )
: QLineEdit( parent )
, mValid( false )
{
	init();
}

void IntervalEdit::init()
{
	QColor o = mOriginalBase = palette().color( QPalette::Base );
	mInvalidBase.setHsv( 0, qMax(o.saturation(),80), 255 );
	connect( this, SIGNAL(textChanged(const QString &)), SLOT(slotTextChanged(const QString &)));
}
	
void IntervalEdit::setInterval( Interval i )
{
	mInterval = i;
	setText( i.toString() );
}

void IntervalEdit::slotTextChanged( const QString & str )
{
	bool oldValid = mValid;
	Interval i = Interval::fromString(str,&mValid);
	if( mValid ) {
		mInterval = i;
		emit intervalChanged(i);
	}
	if( oldValid != mValid ) {
		emit validityChanged(mValid);
		QPalette p = palette();
		p.setColor( QPalette::Base, mValid ? mOriginalBase : mInvalidBase );
		setPalette( p );
	}
}
