
#ifndef MODEL_COPY_DIALOG_H
#define MODEL_COPY_DIALOG_H

#include <qdialog.h>
#include <qshortcut.h>

#include "blurqt.h"
#include "stonegui.h"

#include "ui_modelcopydialogui.h"

class QAbstractItemModel;
class QAbstractItemView;
class QStandardItemModel;
class QStandardItem;
class QTextStream;

class STONEGUI_EXPORT ModelCopyDialog : public QDialog, Ui::ModelCopyDialogUI
{
Q_OBJECT
public:
	ModelCopyDialog( QWidget * parent = nullptr );
	ModelCopyDialog( QAbstractItemModel * model, QWidget * parent = nullptr );
	// Uses the view's model.  Uses the view as parent if parent is nullptr
	ModelCopyDialog( QAbstractItemView * view, QWidget * parent = nullptr );
	
	QAbstractItemView * view() { return mView; }
	QAbstractItemModel * model() { return mModel; }
	
public slots:
	void extractModelData( QTextStream & ts ) const;
	QString extractModelData() const;

	void preview();
	
	void copyToClipboard();

	void saveToFile( const QString & fileName );
	void saveToFile();

	void checkSelected();
	void uncheckSelected();
	
protected slots:
	void columnItemChanged( QStandardItem * item );
	
protected:
	void init();
	
	QAbstractItemModel * mModel;
	QAbstractItemView * mView;
	
	QStandardItemModel * mColumnListModel;
	
	bool mSingleColumn;
};

class STONEGUI_EXPORT ModelCopyShortcut : public QShortcut
{
Q_OBJECT
public:
	ModelCopyShortcut( QAbstractItemView * view, QWidget * parent = nullptr );
	
public slots:
	void showCopyDialog();

protected:
	QAbstractItemView * mView;
};

#endif // MODEL_COPY_DIALOG_H

