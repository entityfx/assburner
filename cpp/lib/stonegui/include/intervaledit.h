
#ifndef INTERVAL_EDIT_H
#define INTERVAL_EDIT_H

#include <qlineedit.h>

#include "interval.h"

#include "stonegui.h"

class STONEGUI_EXPORT IntervalEdit : public QLineEdit
{
Q_OBJECT
public:
	IntervalEdit( Interval i, QWidget * parent = nullptr );
	IntervalEdit( QWidget * parent = nullptr );
	
	// Always the last valid interval, does not update
	// when the text changes to an invalid interval
	Interval interval() const { return mInterval; }
	
	// Reflects the current state of the text, not interval().isValid()
	bool valid() const { return mValid; }
	
public slots:
	void setInterval( Interval i );
	
signals:
	// Only called when the interval has changed to a new *valid* interval
	void intervalChanged( Interval i );
	
	// Called anytime validity changes
	void validityChanged( bool valid );
	
protected slots:
	void slotTextChanged( const QString & );
	
protected:
	void init();
	
	bool mValid;
	Interval mInterval;
	QColor mOriginalBase, mInvalidBase;
};

#endif // INTERVAL_EDIT_H

