

class RecordDataTranslatorInterface : ModelDataTranslator /Abstract/
{
%TypeHeaderCode
#include <recordsupermodel.h>
%End
public:
	RecordDataTranslatorInterface(ModelTreeBuilder * builder /TransferThis/ );

	virtual QVariant recordData(const MappedRecord &, const QModelIndex &, int role) const = 0;
	virtual bool setRecordData(MappedRecord, const QModelIndex &, const QVariant &, int role) = 0;
	virtual Qt::ItemFlags recordFlags(const MappedRecord &, const QModelIndex &) const = 0;
	virtual int recordCompare( const MappedRecord &, const MappedRecord &, const QModelIndex &, const QModelIndex &, int, bool ) const = 0;
	virtual RecordList recordChildren( const MappedRecord &, const QModelIndex & ) const = 0;

	virtual MappedRecord getRecord(const QModelIndex &) = 0;
	virtual void setup(const QModelIndex & idx, const Record & record ) = 0;
	virtual QModelIndexList insertRecordList(int row, RecordList & rl, const QModelIndex & parent = QModelIndex() ) = 0;
	virtual QModelIndexList appendRecordList(RecordList rl, const QModelIndex & parent = QModelIndex() );
};

class RecordDataTranslatorBase : RecordDataTranslatorInterface /Abstract/
{
%TypeHeaderCode
#include <recordsupermodel.h>
%End
public:
	RecordDataTranslatorBase(ModelTreeBuilder * builder /TransferThis/ );
	~RecordDataTranslatorBase();

	virtual QVariant recordData(const MappedRecord &, const QModelIndex &, int role) const;
	virtual bool setRecordData(MappedRecord, const QModelIndex &, const QVariant &, int role);
	virtual Qt::ItemFlags recordFlags(const MappedRecord &, const QModelIndex &) const;
	virtual int recordCompare( const MappedRecord &, const MappedRecord &, const QModelIndex &, const QModelIndex &, int, bool ) const;
	virtual RecordList recordChildren( const MappedRecord &, const QModelIndex & ) const;

	void setRecordColumnList( FieldList columns, bool defaultEditable = false );
	void setColumnEditable( int column, bool editable );
	void setColumnShowFkeyName( int column, bool showFkeyName=true );
	QString recordColumnName( int column ) const;
	int recordColumnPos( int column, const Record & ) const;
	Field * recordColumnField( int column ) const;
	bool columnIsEditable( int column ) const;
	bool columnShowsFkeyName( int column ) const;
	TableSchema * columnForeignKeyTable( int column ) const;
	Field * columnForeignKeyDisplayField( int column ) const;
};

class RecordDataTranslator : RecordDataTranslatorBase
{
%TypeHeaderCode
#include <recordsupermodel.h>
%End
public:
	RecordDataTranslator(ModelTreeBuilder * builder /TransferThis/ );
	static MappedRecord getRecordStatic(const QModelIndex &idx);
	virtual MappedRecord getRecord(const QModelIndex &idx);
	virtual void setup(const QModelIndex & idx, const Record & record );
	virtual QModelIndexList insertRecordList( int row, RecordList & rl, const QModelIndex & parent );
};

class VirtualRecordItem
{
%TypeHeaderCode
#include <recordsupermodel.h>
%End
public:
	virtual Record getRecord() const;
	virtual void setup( const Record & r, const QModelIndex & );
	virtual bool operator==(const VirtualRecordItem & other) const;
	virtual QVariant modelData( const QModelIndex & idx, int role ) const;
	virtual bool setModelData( const QModelIndex &, const QVariant &, int );
	virtual void recordChanged( const MappedRecord & );
	virtual Qt::ItemFlags modelFlags( const QModelIndex & );
	virtual int compare( const QModelIndex & idx, const QModelIndex & idx2, int column, bool ) const;
};

%If (SIP_4_15 - )

class PyRecordDataTranslator : public RecordDataTranslator
{
%TypeHeaderCode

#include <recordsupermodel.h>
#if 0
static PyObject * findMethod(PyObject * obj, const char * methodName)
{
	if( !obj ) return 0;
	if( PyObject_HasAttrString( obj, methodName ) ) {
		PyObject * att = PyObject_GetAttrString( obj, methodName );
		if( att && PyCallable_Check( att ) ) {
			return att;
		}
		Py_XDECREF(att);
	}
	return 0;
}

class PyRecordItem : public VirtualRecordItem
{
public:
	PyObject * mPyObject;
	PyRecordItem( PyObject * po = 0 ) : mPyObject(po) { Py_XINCREF(mPyObject); }
	PyRecordItem(const PyRecordItem & other) : mPyObject(other.mPyObject) { Py_XINCREF(mPyObject); }
	PyRecordItem & operator=(const PyRecordItem & other) { Py_XDECREF(mPyObject); mPyObject = other.mPyObject; Py_XINCREF(mPyObject); return *this; }
	~PyRecordItem() { Py_XDECREF(mPyObject); }
	virtual Record getRecord() const {
		Record ret = record;
		SIP_BLOCK_THREADS
		if( PyObject * method = findMethod(mPyObject,"getRecord") ) {
			int isErr = 0;
			PyObject * sipRes = sipCallMethod(&isErr, method, "");
			if( sipRes )
				sipParseResult(&isErr, method, sipRes, "H5", sipType_Record, &ret);
			else
				PyErr_Print();
			Py_XDECREF(sipRes);
			Py_DECREF(method);
		}
		SIP_UNBLOCK_THREADS
		return ret;
	}
	/*virtual void setup( const Record & r, const QModelIndex & ) { record = r; }*/
	/*virtual RecordList children( const QModelIndex & ) { return RecordList(); }*/
	virtual Qt::ItemFlags modelFlags( const QModelIndex & i )
	{
		Qt::ItemFlags ret(0);
		SIP_BLOCK_THREADS
		if( PyObject * method = findMethod(mPyObject,"modelFlags") ) {
			int isErr = 0;
			PyObject *sipRes = sipCallMethod(&isErr, method, "N",new QModelIndex(i),sipType_QModelIndex,NULL);
			if( sipRes )
				sipParseResult(&isErr, method, sipRes, "H5", sipType_Qt_ItemFlags, &ret);
			else
				PyErr_Print();
			Py_XDECREF(sipRes);
			Py_DECREF(method);
		}
		SIP_UNBLOCK_THREADS
		return ret;
	}
	virtual QVariant modelData( const QModelIndex & idx, int role ) const
	{
		QVariant ret;
		SIP_BLOCK_THREADS
		if( PyObject * method = findMethod(mPyObject,"modelData") ) {
			int isErr = 0;
			PyObject *sipRes = sipCallMethod(&isErr, method, "Ni",new QModelIndex(idx),sipType_QModelIndex,NULL,role);
			if( sipRes )
				sipParseResult(&isErr, method, sipRes, "H5", sipType_QVariant, &ret);
			else
				PyErr_Print();
			Py_XDECREF(sipRes);
			Py_DECREF(method);
		}
		SIP_UNBLOCK_THREADS
		return ret;

	}
	virtual bool setModelData( const QModelIndex & idx, const QVariant & var, int role )
	{
		bool ret;
		SIP_BLOCK_THREADS
		if( PyObject * method = findMethod(mPyObject,"setModelData") ) {
			int isErr = 0;
			PyObject *sipRes = sipCallMethod(&isErr, method, "NNi",new QModelIndex(idx),sipType_QModelIndex,NULL,new QVariant(var),sipType_QVariant,NULL,role);
			if( sipRes )
				sipParseResult(&isErr, method, sipRes, "b", &ret);
			else
				PyErr_Print();
			Py_XDECREF(sipRes);
			Py_DECREF(method);
		}
		SIP_UNBLOCK_THREADS
		return ret;
	}

};
#endif

class sipVirtualRecordItem : public VirtualRecordItem
{
public:
    sipVirtualRecordItem();
    sipVirtualRecordItem(const VirtualRecordItem&);
    virtual ~sipVirtualRecordItem();

public:
    sipSimpleWrapper *sipPySelf;

private:
    sipVirtualRecordItem(const sipVirtualRecordItem &);
    sipVirtualRecordItem &operator = (const sipVirtualRecordItem &);

    char sipPyMethods[7];
};

class PyRecordDataTranslator : public RecordDataTranslatorBase
{
public:
	PyObject * mPyClass;
	PyRecordDataTranslator(ModelTreeBuilder * builder, PyObject * pyClass)
	: RecordDataTranslatorBase(builder)
	, mPyClass(pyClass)
	{
		Py_XINCREF(mPyClass);
	}
	
	int dataSize()
	{ return sizeof(sipVirtualRecordItem*); }

	void constructData( void * dataPtr, void * copy )
	{
		sipVirtualRecordItem * item = new sipVirtualRecordItem();
		*((sipVirtualRecordItem**)dataPtr) = item;
		if( !sipConvertFromNewPyType(item,(PyTypeObject*)mPyClass,NULL,&(item->sipPySelf),"") )
			PyErr_Print();
	}

	QVariant modelData( void * dataPtr, const QModelIndex & idx, int role ) const
	{
		QVariant ret = (*(sipVirtualRecordItem**)dataPtr)->modelData(idx,role);
		if( !ret.isValid() )
			return this->recordData(getRecord(idx),idx,role);
		return ret;
	}

	bool setModelData( void * dataPtr, const QModelIndex & idx, const QVariant & value, int role )
	{
		bool ret = (*(sipVirtualRecordItem**)dataPtr)->setModelData( idx, value, role );
		if( !ret ) {
			Record r = (*(sipVirtualRecordItem**)dataPtr)->getRecord();
			ret = this->setRecordData(r,idx,value,role);
			if( ret )
				(*(sipVirtualRecordItem**)dataPtr)->recordChanged(r);
		}
		return ret;
	}

	Qt::ItemFlags modelFlags( void * dataPtr, const QModelIndex & idx ) const
	{
		Qt::ItemFlags ret = Qt::ItemFlags((*(sipVirtualRecordItem**)dataPtr)->modelFlags( idx ));
		if( ret == 0 ) return this->recordFlags(getRecord(idx),idx);
		return ret;
	}

	int compare( void * dataPtr, void * , const QModelIndex & idx1, const QModelIndex & idx2, int column, bool asc ) const
	{
		int ret = (*(sipVirtualRecordItem**)dataPtr)->compare( idx1, idx2, column, asc );
		if( ret == 0 ) return this->recordCompare(getRecord(idx1),getRecord(idx2),idx1,idx2,column,asc);
		return ret;
	}

	void deleteData( void * dataPtr )
	{ delete (*(sipVirtualRecordItem**)dataPtr); }

	void copyData( void * dataPtr, void * copy )
	{ *((sipVirtualRecordItem**)dataPtr) = *((sipVirtualRecordItem**)copy);}

	static bool isType( const QModelIndex & idx )
	{ return dynamic_cast<PyRecordDataTranslator *>( translator(idx) ) != 0; }

	static sipVirtualRecordItem ** data( const QModelIndex & idx )
	{
		if( isType(idx) ) return (sipVirtualRecordItem**)dataPtr(idx);
		const char * myType = typeid(PyRecordDataTranslator).name();
		ModelDataTranslator * other = translator(idx);
		const char * otherType = other ? typeid(*other).name() : "unknown type";
		printf( "WARNING: Calling %s::data on %s index\n", myType, otherType );
		abort();
		return 0;
	}

	static Record getRecordStatic(const QModelIndex &idx) { return (*data(idx))->getRecord(); }
	static RecordList getChildrenStatic(const QModelIndex & idx) { return (*data(idx))->children(idx); }

	virtual Record getRecord(const QModelIndex &idx) const { return getRecordStatic(idx); }
	virtual RecordList children(const QModelIndex &idx) const {
		RecordList ret = getChildrenStatic(idx);
		if( ret.isEmpty() )
			return recordChildren(getRecord(idx),idx);
		return ret;
	}

	virtual void setup(const QModelIndex & idx, const Record & record )
	{ (*data(idx))->setup(record,idx); }

	virtual QModelIndex insert( int row, VirtualRecordItem * item, const QModelIndex & parent ) {
		SuperModel * model = this->model();
		SuperModel::InsertClosure closure(model);
		QModelIndexList ret = model->insert( parent, row, 1, this, /*skipDataConstruction=*/true );
		*data(ret[0]) = (sipVirtualRecordItem*)item;
		return ret[0];
	}

	QModelIndex append( VirtualRecordItem * item, const QModelIndex & parent = QModelIndex() ) {
		return insert( model()->rowCount(parent), item, parent );
	}

	VirtualRecordItem * item(const QModelIndex & idx) {
		sipVirtualRecordItem ** d = isType(idx) ? data(idx) : 0;
		return d ? *d : 0;
	}

};

%End
public:
	PyRecordDataTranslator(ModelTreeBuilder * builder /TransferThis/, SIP_PYOBJECT );

	QModelIndex insert( int row, VirtualRecordItem * /Transfer/, const QModelIndex & parent = QModelIndex() );
	QModelIndex append( VirtualRecordItem * /Transfer/, const QModelIndex & parent = QModelIndex() );
	VirtualRecordItem * item(const QModelIndex &);
};

%End


class RecordTreeBuilder : ModelTreeBuilder
{
%TypeHeaderCode
#include <recordsupermodel.h>
%End
public:
	RecordTreeBuilder( SuperModel * model );

	virtual bool hasChildren( const QModelIndex & parentIndex, SuperModel * model );
	virtual void loadChildren( const QModelIndex & parentIndex, SuperModel * model );
};

class RecordSuperModel : SuperModel
{
%TypeHeaderCode
#include <recordsupermodel.h>
%End
public:
	
	RecordSuperModel( QObject * parent );

	MappedRecordList rootList() const;

	MappedRecord getRecord(const QModelIndex & i) const;
	MappedRecordList getRecords( const QModelIndexList & list ) const;

	void updateIndex( const QModelIndex & i );

	bool setupIndex( const QModelIndex & i, const Record & r );

	QModelIndex findIndex( const Record & r, bool recursive = true, const QModelIndex & parent = QModelIndex(), bool loadChildren = true );
	QModelIndexList findIndexes( RecordList, bool recursive = true, const QModelIndex & parent = QModelIndex(), bool loadChildren = true );
	QModelIndex findFirstIndex( RecordList, bool recursive = true, const QModelIndex & parent = QModelIndex(), bool loadChildren = true );

	MappedRecordList listFromIS( const QItemSelection & is );

	QMimeData * mimeData(const QModelIndexList &indexes) const;
	QStringList mimeTypes() const;
	virtual bool dropMimeData ( const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent );
	
	virtual void setupChildren( const QModelIndex & parent, const RecordList & rl );

	void listen( Table * t );

	QModelIndex append( const QModelIndex & par = QModelIndex(), ModelDataTranslator * trans = 0 );
	QList<QModelIndex> append( const QModelIndex & par, int cnt, ModelDataTranslator * trans = 0 );

	QModelIndex insert( const QModelIndex & par, int row, ModelDataTranslator * = 0 );
	QModelIndexList insert( const QModelIndex & par, int rowStart, int cnt, ModelDataTranslator * = 0 );

	void remove( const QModelIndex & i );
	void remove( QModelIndexList );

public slots:

	virtual QModelIndexList insert( RecordList rl, int row = 0, const QModelIndex & parent = QModelIndex(), RecordDataTranslatorInterface * trans = 0 );
	virtual QModelIndex append( const Record &, const QModelIndex & parent = QModelIndex(), RecordDataTranslatorInterface * trans = 0 );
	virtual QModelIndexList append( RecordList rl, const QModelIndex & parent = QModelIndex(), RecordDataTranslatorInterface * trans = 0 );
	virtual void remove( RecordList rl, bool recursive = false, const QModelIndex & parent = QModelIndex() );
	virtual void updated( RecordList rl, bool recursive = false, const QModelIndex & parent = QModelIndex(), bool loadChildren = true );
	virtual void updated( Record r );

	virtual void setRootList( const RecordList & );

	virtual void updateRecords( RecordList, const QModelIndex & parent = QModelIndex(), bool recursive = false, bool loadChildren = true );

protected:
	RecordDataTranslatorInterface * recordDataTranslator( const QModelIndex & i ) const;
	QModelIndexList findIndexesHelper( RecordList rl, bool recursive, const QModelIndex & index, bool retAfterOne = false, bool loadChildren = true );
};
