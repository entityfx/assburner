
TARGET=exr
TEMPLATE=lib

include(../qimageio.pri)

SOURCES+=exr.cpp
HEADERS+=exr.h


unix:!macx {
	LIBS+=-lIex -lImath -lHalf -lIlmImf
	INCLUDEPATH+=/usr/include/OpenEXR/
} macx {
	LIBS+=-L/usr/local/lib -lIex -lImath -lHalf -lIlmImf -flat_namespace
	INCLUDEPATH+=/usr/local/include/OpenEXR/
} else {
	DEFINES+=OPENEXR_DLL
	CONFIG+=exceptions
	OPENEXR="C:/Program Files/OpenEXR"
	ILMBASE="C:/Program Files/OpenEXR"
	INCLUDEPATH+=$${OPENEXR}/Include/OpenExr
	INCLUDEPATH+=$${ILMBASE}/Include/OpenExr
	LIBS+=-L$${OPENEXR}\Lib -L$${ILMBASE}\Lib
	LIBS+=-lIex-2_2 -lImath-2_2 -lHalf -lIlmImf-2_2 -lIlmThread-2_2
}

