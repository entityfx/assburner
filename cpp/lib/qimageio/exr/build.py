import sys
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))

class ExrPluginTarget(QMakeTarget):
	def is_buildable(self):
		#return not self.has_arg('X86_64')
		return True

ExrPluginTarget("exrplugin",path,"exr.pro")
