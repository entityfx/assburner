
import os

from . import (
	exr, rla, sgi, tga )
#	hdr )

from blur.build import *

All_Targets.append( Target('qimageioplugins',os.path.abspath(os.getcwd()),post_deps=[
		"exrplugin","rlaplugin","sgiplugin","tgaplugin"]) )
