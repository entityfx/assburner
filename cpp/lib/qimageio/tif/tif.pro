
TARGET=tif

include(../qimageio.pri)

SOURCES+=tif.cpp
HEADERS+=tif.h
LIBS+=-ltiff

win32 {
	INCLUDEPATH += C:/GnuWin32/include
	LIBS+=-LC:/GnuWin32/bin -LC:/GnuWin32/lib
}


