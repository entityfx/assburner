
#ifndef JOB_TYPE_PLUGIN_H
#define JOB_TYPE_PLUGIN_H

#include <qmap.h>

#include <job.h>
#include <jobtype.h>

#include "absubmit.h"

class QWidget;
class SubmitCollection;

class AB_SUBMIT_EXPORT JobTypePlugin
{
public:
	virtual ~JobTypePlugin();
	
	virtual JobType jobType() = 0;

	virtual Job createJob(SubmitCollection *) = 0;

	virtual QWidget * submitPage( const QString & pageName, QWidget * parent, SubmitCollection * );

	virtual QStringList submitPages();
};

class AB_SUBMIT_EXPORT JobTypePluginFactory
{
public:
	JobTypePluginFactory();

	JobTypePlugin * plugin( const JobType & jobType );
	
	JobTypeList jobTypes();

	void registerPlugin( JobTypePlugin * plugin );

	static JobTypePluginFactory * instance();
	
protected:
	QMap<JobType,JobTypePlugin*> mPlugins;
};

#endif // JOB_TYPE_PLUGIN_H

