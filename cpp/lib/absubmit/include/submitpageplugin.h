

#ifndef SUBMIT_PAGE_FACTORY_H
#define SUBMIT_PAGE_FACTORY_H

#include <qstringlist.h>
#include <qmap.h>

#include "absubmit.h"

class QWidget;
class SubmitCollection;

class AB_SUBMIT_EXPORT SubmitPagePlugin
{
public:
	virtual QStringList submitPages() = 0;
	virtual QWidget * submitPage( const QString & name, QWidget * parent, SubmitCollection * ) = 0;
};

class AB_SUBMIT_EXPORT SubmitPageFactory
{
public:
	QWidget * submitPage( const QString & name, QWidget * parent, SubmitCollection * );
	
	QStringList submitPages();

	void registerPlugin( SubmitPagePlugin * plugin );

	static SubmitPageFactory * instance();

protected:
	QMap<QString,SubmitPagePlugin*> mPluginsByPageName;
	QList<SubmitPagePlugin*> mPlugins;
};

#endif // SUBMIT_PAGE_FACTORY_H
