
#ifndef SUBMIT_COLLECTION_H
#define SUBMIT_COLLECTION_H

#include <qobject.h>
#include <qdom.h>

#include <job.h>
#include <jobdep.h>
#include <jobtype.h>
#include <jobtask.h>
#include <service.h>

#include "absubmit.h"

class AB_SUBMIT_EXPORT SubmitCollection : public QObject
{
Q_OBJECT
public:
	SubmitCollection( QObject * parent = 0 ): QObject(parent){}

	virtual void submit(bool copyMade=false)=0;

	virtual Job createJob(const JobType & jobType)=0;
	virtual JobList jobs()=0;
	virtual void addJob(const Job & job)=0;
	virtual void removeJob(const Job & job)=0;
	
	virtual Job jobParent( const Job & )=0;
	virtual void setJobParent( const Job &, const Job & parent )=0;
	virtual JobList jobChildren( const Job & )=0;

	virtual JobTaskList tasks(const Job & job)=0;
	virtual void setTasks(const Job & job, JobTaskList tasks)=0;
		
	virtual ServiceList services(const Job & job)=0;
	virtual void addService(const Job & job, const Service & service)=0;
	virtual void setServices(const Job & job, ServiceList services)=0;
	virtual void removeService(const Job & job, const Service & service)=0;

	virtual QStringList files(const Job & job)=0;
	virtual void setFiles(const Job & job, QStringList files)=0;
	
	virtual void addJobDep(const JobDep & jobDep)=0;
	virtual void removeJobDep(const JobDep & jobDep)=0;
	virtual JobDepList deps(const Job & job)=0;
	virtual JobList jobsWithDep(const Job & job)=0;
	virtual void clear()=0;
	
	virtual QDomDocument saveToDom()=0;
	virtual void loadFromDom(const QDomDocument & domDoc)=0;
	
	virtual void saveToFile(const QString & fileName)=0;
	virtual void loadFromFile(const QString & fileName)=0;
	
	virtual SubmitCollection * copy()=0;

//signals:
// These signals are implemented in the derived class
//	void JobAddedSignal( const Job & );
//	void JobRemovedSignal( const Job & );
//	void JobDepAddedSignal( const JobDep & );
//	void JobDepRemovedSignal(const JobDep & );
//	void JobParentChangedSignal( const Job &, const Job & parent );

//	def zipFiles(self,fileList):
};

#endif // SUBMIT_COLLECTION_H
