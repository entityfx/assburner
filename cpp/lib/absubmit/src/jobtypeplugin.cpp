
#include <database.h>
#include <schema.h>
#include <tableschema.h>
#include <table.h>

#include "jobtypeplugin.h"
#include "submitcollection.h"
#include "submitpageplugin.h"

JobTypePlugin::~JobTypePlugin()
{}

QWidget * JobTypePlugin::submitPage( const QString & pageName, QWidget * parent, SubmitCollection * submitCollection )
{
	return SubmitPageFactory::instance()->submitPage( pageName, parent, submitCollection );
}

QStringList JobTypePlugin::submitPages()
{
	return QStringList() << "Settings" << "Services" << "Files" << "Tasks";
}

class BuiltinJobTypePlugin : public JobTypePlugin
{
public:
	BuiltinJobTypePlugin( const JobType & jobType )
	: mJobType( jobType )
	, mTable( 0 )
	{
		TableSchema * ts = Database::current()->schema()->tableByName( jobType.name() );
		if( ts )
			mTable = ts->table();
	}

	bool isValid()
	{
		return bool(mTable);
	}

	JobType jobType()
	{
		return mJobType;
	}

	Job createJob(SubmitCollection * submitCollection)
	{
		if( mTable ) {
			Job j = mTable->load();
			j.setName( "New Job" );
			j.setPriority( 50 );
			j.setJobType(jobType());
			submitCollection->addJob(j);
			return j;
		}
		return Record();
	}

protected:
	JobType mJobType;
	Table * mTable;
};


static JobTypePluginFactory * sJobTypePluginFactory = 0;
static bool sBuiltinPluginsLoaded = false;

JobTypePluginFactory::JobTypePluginFactory()
{
}

JobTypePlugin * JobTypePluginFactory::plugin( const JobType & jobType )
{
	/* Load default plugins for all the jobtypes that don't have custom plugins
	 * Later we may want to make it configurable(maybe a jobtype field) which
	 * job types get default plugsin
	 */
	if( !sBuiltinPluginsLoaded ) {
		// Set this here so we can call this function
		sBuiltinPluginsLoaded = true;
		JobTypeList jtl = JobType::select();
		foreach( JobType jt, jtl ) {
			if( !plugin(jt) )
				registerPlugin( new BuiltinJobTypePlugin( jt ) );
		}
	}

	if( mPlugins.contains(jobType) )
		return mPlugins[jobType];

	return 0;
}

JobTypeList JobTypePluginFactory::jobTypes()
{
	JobTypeList ret;
	foreach( JobType jt, mPlugins.keys() )
		ret += jt;
	return ret;
}

void JobTypePluginFactory::registerPlugin( JobTypePlugin * plugin )
{
	JobType jobType = plugin->jobType();
	if( mPlugins[jobType] )
		delete mPlugins[jobType];
	mPlugins[jobType] = plugin;
}

JobTypePluginFactory * JobTypePluginFactory::instance()
{
	if( !sJobTypePluginFactory )
		sJobTypePluginFactory = new JobTypePluginFactory();
	return sJobTypePluginFactory;
}
