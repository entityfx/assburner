
#include "submitpageplugin.h"
#include "submitcollection.h"


QWidget * SubmitPageFactory::submitPage( const QString & name, QWidget * parent, SubmitCollection * submitCollection )
{
	if( mPluginsByPageName.contains(name) )
		return mPluginsByPageName[name]->submitPage(name,parent,submitCollection);
	return 0;
}
	
QStringList SubmitPageFactory::submitPages()
{
	return QStringList( mPluginsByPageName.keys() );
}

void SubmitPageFactory::registerPlugin( SubmitPagePlugin * plugin )
{
	mPlugins.append( plugin );
	foreach( QString page, plugin->submitPages() )
		mPluginsByPageName[page] = plugin;
}

static SubmitPageFactory * sSubmitPageFactory = 0;

SubmitPageFactory * SubmitPageFactory::instance()
{
	if( !sSubmitPageFactory )
		sSubmitPageFactory = new SubmitPageFactory();
	return sSubmitPageFactory;
}
