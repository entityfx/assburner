
TARGET=absubmit
TEMPLATE=lib

include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/python.pri)
include($$(PRI_SHARED)/classes.pri)
include($$(PRI_SHARED)/stone.pri)

INCLUDEPATH+=include

SOURCES += \
	src/submitter.cpp \
	src/jobtypeplugin.cpp \
	src/submitpageplugin.cpp

HEADERS += \
	include/submitter.h \
	include/absubmit.h \
	include/jobtypeplugin.h \
	include/submitcollection.h \
	include/submitpageplugin.h

LIBS += -L../../lib/classes -lclasses -L../../lib/stone -lstone
win32:LIBS += -lpsapi -lMpr

DEFINES += AB_SUBMIT_MAKE_DLL

CONFIG += qt thread warn_on dll
QT += xml sql network widgets ftp

target.path=$$LIB_PREFIX
INSTALLS += target
