
#include <qtimer.h>
#include <qfile.h>

#include "config.h"
#include "jobcommandhistorywidget.h"

JobCommandHistoryWidget::JobCommandHistoryWidget( QWidget * parent )
: QWidget( parent )
, mRefreshRequested( false )
{
	setupUi( this );

	setAttribute( Qt::WA_DeleteOnClose, true );

//	connect(mRefreshButton, SIGNAL(pressed()), SLOT(refresh()));
	resize(600, 800);
	mLogRoot = Config::getString("assburnerLogRootDir", "");
}

JobCommandHistoryWidget::~JobCommandHistoryWidget()
{}

JobCommandHistory JobCommandHistoryWidget::jobCommandHistory()
{
	return mJobCommandHistory;
}

void JobCommandHistoryWidget::setJobCommandHistory(const JobCommandHistory & jch)
{
	mJobCommandHistory = jch;
	refresh();
}

void JobCommandHistoryWidget::refresh()
{
	if( !mRefreshRequested ) {
		mRefreshRequested = true;
		QTimer::singleShot( 0, this, SLOT( doRefresh() ) );
	}
}

void JobCommandHistoryWidget::doRefresh()
{
	mRefreshRequested = false;
	mJobCommandHistory.reload();
	mCommandEdit->setText( mJobCommandHistory.command() );
	if( mLogRoot.isEmpty() ) {
		mLogText->document()->setPlainText( mJobCommandHistory.stdOut() );
	} else {
		QFile read(mJobCommandHistory.stdOut());
		if( !read.open(QIODevice::ReadOnly | QIODevice::Text) )
			return;
		mLogText->document()->setPlainText( read.readAll() );
	}
	mLogText->moveCursor(QTextCursor::End);
}

