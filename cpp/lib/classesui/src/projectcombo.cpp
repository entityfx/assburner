
#include <qtimer.h>

#include "projectcombo.h"

ProjectCombo::ProjectCombo( QWidget * parent )
: RecordCombo( parent )
, mShowSpecialItem( true )
, mSpecialItemText( "All" )
, mRefreshRequested( false )
{
	connect( this, SIGNAL( currentIndexChanged( int ) ), SLOT( indexChanged( int ) ) );
	mModel = new RecordListModel( this );
	mModel->setColumn( "name" );
	setModel( mModel );
	connect( Project::table(), SIGNAL( added( RecordList ) ), SLOT( refresh() ) );
	connect( Project::table(), SIGNAL( removed( RecordList ) ), SLOT( refresh() ) );
	connect( Project::table(), SIGNAL( updated( Record, Record ) ), SLOT( refresh() ) );
	refresh();
}

Project ProjectCombo::project() const
{
	return mCurrent;
}

void ProjectCombo::setProject( const Project & p )
{
	mCurrent = p;
	int idx = mModel->recordList().findIndex( p );
	if( idx < 0 )
		idx = mModel->insert( p, mShowSpecialItem ? 0 : 1 )[0].row();
	setCurrentIndex(idx);
}

ProjectStatusList ProjectCombo::statusFilters() const
{
	return mStatusFilters;
}

bool ProjectCombo::showingSpecialItem() const
{
	return mShowSpecialItem;
}

void ProjectCombo::setShowSpecialItem( bool sai )
{
	if( sai != mShowSpecialItem ) {
		mShowSpecialItem = sai;
		refresh();
	}
}

void ProjectCombo::setSpecialItemText( const QString & text )
{
	mSpecialItemText = text;
}

QString ProjectCombo::specialItemText() const
{
	return mSpecialItemText;
}

void ProjectCombo::setStatusFilters( ProjectStatusList filters )
{
	mStatusFilters = filters;
	refresh();
}

void ProjectCombo::indexChanged( int i )
{
	if( signalsBlocked() ) return;
	ProjectList ptl( mModel->recordList() );
	if( i < 0 || i >= (int)ptl.size() ) return;
	mCurrent = ptl[i];
	emit projectChanged( mCurrent );
}

void ProjectCombo::refresh()
{
	if( !mRefreshRequested ) {
		QTimer::singleShot(0,this,SLOT(doRefresh()));
		mRefreshRequested = true;
	}
}

void ProjectCombo::doRefresh()
{
	mRefreshRequested = false;

	// Filter out templates, all real projects have non null fkeyprojectstatus
	Expression e = Project::c.ProjectStatus.isNotNull();
	if( !mStatusFilters.isEmpty() )
		e &= Project::c.ProjectStatus.in( mStatusFilters );
	else {
		ExpressionFuture future = ProjectStatus::c.ProjectStatus.in( QStringList() << "Production" << "Pre-Production" ).future()
			.then(this, SLOT(projectStatusFutureReady(ExpressionFuture)));
		e &= Project::c.ProjectStatus.in(future);
	}
	
	e.future().then(this, SLOT(projectFutureReady(ExpressionFuture)));
}

void ProjectCombo::projectStatusFutureReady(ExpressionFuture future)
{
	mStatusFilters = future.get();
}

void ProjectCombo::projectFutureReady(ExpressionFuture projectFuture)
{
	ProjectList ptl = ProjectList(projectFuture.get()).sorted(Project::c.Name);

	if( mCurrent.isValid() && !ptl.contains(mCurrent) )
		ptl.insert( ptl.begin(), mCurrent );

	if( mShowSpecialItem )
		ptl.insert( ptl.begin(), Project().setName(mSpecialItemText) );
	
	bool bs = blockSignals(true);
	mModel->setRecordList( ptl );
	blockSignals(bs);
	
	setCurrentIndex( mCurrent.isValid() ? qMax(0,ptl.findIndex(mCurrent)) : 0 );
}


