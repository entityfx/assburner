
#include "user.h"

#include "notificationrouteview.h"

NotificationRouteModel::NotificationRouteModel( QObject * parent )
: RecordSuperModel( parent )
{
	RecordDataTranslator * rdt = new RecordDataTranslator(treeBuilder());
	FieldList fields;
#define NRC NotificationRoute::c
	fields << NRC.EventMatch << NRC.ComponentMatch << NRC.User << NRC.SubjectMatch << NRC.MessageMatch << NRC.Actions << NRC.Priority;
#undef NRC
	QStringList headerLabels;
	foreach( Field * f, fields ) headerLabels << f->displayName();
	setHeaderLabels( headerLabels );
	rdt->setRecordColumnList( fields, true /*defaultEditable*/ );
	
	listen( NotificationRoute::table() );
}

NotificationRouteView::NotificationRouteView( QWidget * parent )
: RecordTreeView( parent )
{
	mModel = new NotificationRouteModel( this );
	setModel( mModel );
	/// Set all columns to auto resize
	setColumnAutoResize( -1, true );
	
}

void NotificationRouteView::setUserFilter( const User & user )
{
	mUserFilter = user;
	mModel->setRootList( NotificationRoute::recordsByUser( user ) );
}

void NotificationRouteView::addRoute()
{
	mModel->append( NotificationRoute() );
}

void NotificationRouteView::commit()
{
	mModel->rootList().commit();
}
