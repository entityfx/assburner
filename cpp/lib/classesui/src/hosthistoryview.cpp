
#include <qinputdialog.h>
#include <qmenubar.h>
#include <qsplitter.h>
#include <qtimer.h>
#include <qtoolbar.h>

#include "blurqt.h"
#include "iniconfig.h"
#include "interval.h"
#include "tableschema.h"

#include "hosthistory.h"
#include "jobassignmentstatus.h"
#include "jobtaskassignment.h"
#include "joberror.h"
#include "jobservice.h"
#include "jobtype.h"
#include "service.h"

#include "recordsupermodel.h"
#include "recordpropvaltree.h"

#include "hosthistoryview.h"
#include "jobassignmentwindow.h"

class HostHistoryItem : public RecordItem
{};

HostHistoryView::HostHistoryView( QWidget * parent )
: RecordTreeView( parent )
, mRefreshScheduled( false )
{
	FieldList cols;
	QStringList col_names;
	QList<int> showFkeyCols;
	foreach( Field * field, HostHistory::table()->schema()->fields() ) {
		if( field == HostHistory::c.Host )
			showFkeyCols.append( cols.size() );
		cols << field;
		col_names << field->displayName();
	}
	mModel = new RecordSuperModel(this);
	mModel->setHeaderLabels( col_names );
	mTrans = new HostHistoryTranslator(mModel->treeBuilder());
	mTrans->setRecordColumnList( cols );
	foreach( int col, showFkeyCols )
		mTrans->setColumnShowFkeyName( col, true );
	setModel( mModel );

	connect( this, SIGNAL( doubleClicked( const QModelIndex & ) ), SLOT( slotDoubleClicked( const QModelIndex & ) ) );
}

void HostHistoryView::slotDoubleClicked( const QModelIndex & idx )
{
	HostHistory hh( mModel->getRecord(idx) );
	if( hh.isRecord() ) {
		QString col = mTrans->recordColumnName( idx.column() ).toLower();
	}
}

void HostHistoryView::setFilter( const Expression & exp )
{
	mFilter = exp;
}

void HostHistoryView::refresh()
{
	if( !mRefreshScheduled ) {
		mRefreshScheduled = true;
		QTimer::singleShot( 0, this, SLOT( doRefresh() ) );
	}
}

void HostHistoryView::doRefresh()
{
	mRefreshScheduled = false;
	HostHistoryList hhl = HostHistory::select( mFilter.orderBy(HostHistory::c.Key,Expression::Descending) ); //e.limit(mLimit) );
	mModel->updateRecords( hhl );
}

class JobAssignmentItem : public RecordItem
{
public:
	QVariant bg;
	QString services;
	static int JobServicesColumn, JobTypeColumn;
	void setup( const Record & record, const QModelIndex & idx )
	{
		RecordItem::setup(record,idx);
		JobAssignment ja(record);
		if( ja.jobAssignmentStatus().status() == "done" )
			bg = QColor(Qt::green).lighter(120);
		else if( ja.jobError().isRecord() )
			bg = QColor(Qt::red).lighter(120);
		Job j = ja.job();
		if( j.isRecord() )
			services = j.jobServices().services().services().join(",");
	}

	QVariant modelData( const QModelIndex & idx, int role )
	{
		if( role == Qt::BackgroundRole )
			return bg;
		if( role == Qt::DisplayRole && idx.column() == JobServicesColumn )
			return services;
		if( role == Qt::DisplayRole && idx.column() == JobTypeColumn ) {
			JobAssignment ja(record);
			if( !ja.isRecord() ) ja = JobTaskAssignment(record).jobAssignment();
			return ja.job().jobType().name();
		}
		return RecordItem::modelData(idx,role);
	}
	
	RecordList children( const QModelIndex & )
	{
		return JobAssignment(record).jobTaskAssignments(Index::UseSelect);
	}
};

int JobAssignmentItem::JobServicesColumn = 0;
int JobAssignmentItem::JobTypeColumn = 0;

class JobTaskAssignmentItem : public RecordItem
{
public:
	QVariant bg;
	void setup( const Record & record, const QModelIndex & idx )
	{
		RecordItem::setup(record,idx);
		JobTaskAssignment jta(record);
		QString status = jta.jobAssignmentStatus().status();
		if( status == "done" )
			bg = QColor(Qt::green).lighter(120);
		else if( status == "busy" )
			bg = QColor(Qt::blue).lighter(120);
		else if( jta.jobError().isRecord() )
			bg = QColor(Qt::red).lighter(120);
	}
	QVariant modelData( const QModelIndex & idx, int role )
	{
		if( role == Qt::BackgroundRole )
			return bg;
		return RecordItem::modelData(idx,role);
	}
};

JobAssignmentView::JobAssignmentView( QWidget * parent )
: RecordTreeView( parent )
, mRefreshScheduled( false )
{
	FieldList cols, task_cols;
	QStringList col_names;
	QList<int> showFkeyCols, taskShowFkeyCols;
	foreach( Field * field, JobAssignment::schema()->fields() ) {
		bool showFkey = false, taskShowFkey = false;
		Field * taskField = 0;
		if( field == JobAssignment::c.Key )
			taskField = JobTaskAssignment::c.Key;
		else if( field == JobAssignment::c.JobAssignmentStatus ) {
			showFkey = taskShowFkey = true;
			taskField = JobTaskAssignment::c.JobAssignmentStatus;
		} else if( field == JobAssignment::c.Started ) {
			taskField = JobTaskAssignment::c.Started;
		} else if( field == JobAssignment::c.Ended ) {
			taskField = JobTaskAssignment::c.Ended;
		} else if( field == JobAssignment::c.CpuTime ) {
			taskField = JobTaskAssignment::c.CpuTime;
		} else if( field == JobAssignment::c.MaxMemory ) {
			taskField = JobTaskAssignment::c.Memory;
		} else if( field == JobAssignment::c.JobError ) {
			showFkey = true;
			taskField = JobTaskAssignment::c.JobError;
		} else if( field == JobAssignment::c.Host
				|| field == JobAssignment::c.Job
				|| field == JobAssignment::c.JobCommandHistory )
			showFkey = true;
		if( showFkey )
			showFkeyCols.append( cols.size() );
		if( taskShowFkey )
			taskShowFkeyCols.append( cols.size() );
		cols << field;
		task_cols << taskField;
		col_names << field->displayName();
	}
	JobAssignmentItem::JobServicesColumn = cols.size();
	JobAssignmentItem::JobTypeColumn = cols.size() + 1;
	col_names << "Job Services" << "Job Type";
	mModel = new RecordSuperModel(this);
	mModel->setHeaderLabels( col_names );
	ModelTreeBuilder * tb = mModel->treeBuilder();
	tb->setDefaultHasChildren(0);
	mTrans = new JobAssignmentTranslator(tb);
	mTrans->setRecordColumnList( cols );
	foreach( int col, showFkeyCols )
		mTrans->setColumnShowFkeyName( col, true );
	mTaskTrans = new JobTaskAssignmentTranslator(tb);
	mTaskTrans->setRecordColumnList( task_cols, JobTaskAssignment::schema() );
	foreach( int col, taskShowFkeyCols )
		mTaskTrans->setColumnShowFkeyName( col, true );
	mTrans->setChildrenTranslator(mTaskTrans);
	setModel( mModel );
	setRootIsDecorated(true);
	connect( this, SIGNAL( doubleClicked( const QModelIndex & ) ), SLOT( slotDoubleClicked( const QModelIndex & ) ) );
}

void JobAssignmentView::slotDoubleClicked( const QModelIndex & idx )
{
	JobAssignment ja( mModel->getRecord(idx) );
	JobTaskAssignment jta( mModel->getRecord(idx) );
	if( !ja.isRecord() && !jta.isRecord() ) return;

	QString col = mTrans->recordColumnName( idx.column() ).toLower();
	if( col == "fkeyjoberror" ) {
		RecordPropValTree * tree = new RecordPropValTree(0);
		tree->setAttribute( Qt::WA_DeleteOnClose, true );
		tree->setWindowTitle( "Job Error" );
		tree->setRecords( ja.isRecord() ? ja.jobError() : jta.jobError() );
		tree->show();
	} else if( ja.isRecord() ) {
		JobAssignmentWindow * jaw = new JobAssignmentWindow();
		jaw->setAttribute( Qt::WA_DeleteOnClose, true );
		jaw->setJobAssignment( ja );
		jaw->setWindowTitle("Job Executation Log: ");
		jaw->show();
	}
}

void JobAssignmentView::setFilter( const Expression & exp )
{
	mFilter = exp;
}

void JobAssignmentView::refresh()
{
	if( !mRefreshScheduled ) {
		mRefreshScheduled = true;
		QTimer::singleShot( 0, this, SLOT( doRefresh() ) );
	}
}

void JobAssignmentView::doRefresh()
{
	mRefreshScheduled = false;

	/*
	Expression e;
	if( mHostFilter.size() )
		e = JobAssignment::c.Host.in(mHostFilter);
	if( mJobFilter.isRecord() )
		e &= JobAssignment::c.Job == mJobFilter;
	if( mJobTaskFilter.isRecord() )
		e &= JobAssignment::c.Key.in( Query(JobTaskAssignment::c.JobAssignment,JobTaskAssignment::c.JobTask ==mJobTaskFilter)); */
	
	JobAssignmentList jal = JobAssignment::select( mFilter.orderBy(JobAssignment::c.Key,Expression::Descending) ); //e.limit(mLimit) );

	Index * jsi = JobService::table()->indexFromField( JobService::c.Job );
	bool jsiRestore = false;
	if( jsi ) {
		jsiRestore = jsi->cacheEnabled();
		jsi->setCacheEnabled( true );
	}
	JobList jobs = jal.jobs();
	mCache = jobs;
	// Cache the job services
	jobs.jobServices();

	//mCache += jal.jobTasks();
	mCache += jal.jobErrors();
	//mCache += jal.jobTaskAssignments();

	QModelIndexList indexes;
	for( ModelIter it(mModel,ModelIter::Filter(ModelIter::Recursive|ModelIter::DescendLoadedOnly)); it.isValid(); ++it ) {
		if( it.depth() == 1 )
			indexes += *it;
	}
	mModel->getRecords(indexes).reload();
	
	foreach( QModelIndex i, indexes )
		mModel->updateIndex(i);
	
	mModel->updateRecords( jal );
	
	// Restore jobservice.fkeyjob caching state
	jsi->setCacheEnabled( jsiRestore );
}

static QWidget * createLabelLayout( QWidget * parent, QWidget * view, const QString & name )
{
	QWidget * container = new QWidget(parent);
	QLayout * layout = new QVBoxLayout(container);
	QLabel * label = new QLabel(name,container);
	layout->addWidget(label);
	view->setParent(container);
	layout->addWidget(view);
	return container;
}

HostHistoryWindow::HostHistoryWindow( QWidget * parent )
: QMainWindow( parent )
, mLimit( 500 )
, mBlockCurrentSync( false )
{
	setAttribute( Qt::WA_DeleteOnClose, true );

	QSplitter * splitter = new QSplitter( Qt::Horizontal, this );
	mHostHistoryView = new HostHistoryView(splitter);
	splitter->addWidget(createLabelLayout(splitter,mHostHistoryView,"Status History"));
	mJobAssignmentView = new JobAssignmentView(splitter);
	splitter->addWidget(createLabelLayout(splitter,mJobAssignmentView,"Job Assignment History"));
	
	connect( mHostHistoryView, SIGNAL(currentChanged(const Record &)), SLOT(slotCurrentHostHistoryChanged(const Record &)) );
	connect( mJobAssignmentView, SIGNAL(currentChanged(const Record &)), SLOT(slotCurrentJobAssignmentChanged(const Record &)) );

	setCentralWidget( splitter );

	mCloseAction = new QAction( "&Close", this );
	connect( mCloseAction, SIGNAL( triggered() ), SLOT( close() ) );
	
	mRefreshAction = new QAction( "&Refresh", this );
	connect( mRefreshAction, SIGNAL( triggered() ), SLOT( refresh() ) );

	mSetLimitAction = new QAction( "Set &Limit", this );
	connect( mSetLimitAction, SIGNAL( triggered() ), this, SLOT( setLimit() ) );
	
	QMenu * fileMenu = menuBar()->addMenu( "&File" );
	fileMenu->addAction( mCloseAction );

	QMenu * viewMenu = menuBar()->addMenu( "&View" );
	viewMenu->addAction( mRefreshAction );
	viewMenu->addAction( mSetLimitAction );
	
	QToolBar * tb = addToolBar("Main");
	tb->addAction( mRefreshAction );
	tb->addAction( mSetLimitAction );
	
	IniConfig ini(userConfig());
	ini.pushSection( "HostHistoryWindow" );
	QByteArray geometry = ini.readByteArray( "WindowGeometry" );
	ini.popSection();
	if( geometry.isEmpty() )
		resize( 700, 600 );
	else
		restoreGeometry( geometry );
}

HostHistoryView * HostHistoryWindow::hostHistoryView() const
{
	return mHostHistoryView;
}

JobAssignmentView * HostHistoryWindow::jobAssignmentView() const
{
	return mJobAssignmentView;
}

void HostHistoryWindow::showJobs( JobList jobs )
{
	mHostHistoryView->setFilter( ev_(false) );
	mJobAssignmentView->setFilter( JobAssignment::c.Job.in(jobs).limit(mLimit) );
	refresh();
}

void HostHistoryWindow::showHosts( HostList hosts )
{
	mHostHistoryView->setFilter( HostHistory::c.Host.in(hosts).limit(mLimit) );
	mJobAssignmentView->setFilter( JobAssignment::c.Host.in(hosts).limit(mLimit) );
	refresh();
}

void HostHistoryWindow::showTasks( JobTaskList tasks )
{
	mHostHistoryView->setFilter( ev_(false) );
	mJobAssignmentView->setFilter( JobAssignment::c.Key.in( Query(JobTaskAssignment::c.JobAssignment,JobTaskAssignment::c.JobTask.in(tasks)) ).limit(mLimit) );
	refresh();
}

void HostHistoryWindow::refresh()
{
	mHostHistoryView->refresh();
	mJobAssignmentView->refresh();
}

void HostHistoryWindow::setLimit()
{
	bool okay;
	int limit = QInputDialog::getInt( this, "Set history limit", "Set history limit", mLimit, 0, 100000, 1, &okay );
	if( okay ) {
		mLimit = limit;
		refresh();
	}
}

void HostHistoryWindow::closeEvent( QCloseEvent * ce )
{
	IniConfig ini(userConfig());
	ini.pushSection( "HostHistoryWindow" );
	ini.writeByteArray( "WindowGeometry", saveGeometry() );
	ini.popSection();
	QMainWindow::closeEvent(ce);
}

void HostHistoryWindow::slotCurrentHostHistoryChanged( const Record & r )
{
	if( mBlockCurrentSync ) return;
	HostHistory hh(r);
	Interval prevDiff(Interval::null());
	JobAssignment prev;
	for( ModelIter it(mJobAssignmentView->model()); it.isValid(); ++it ) {
		JobAssignment ja(mJobAssignmentView->model()->getRecord(*it));
		Interval diff = Interval(hh.dateTime(),ja.started()).abs();
		if( ja.host() == hh.host() ) {
			if (!prevDiff.isNull() && diff > prevDiff)
				break;
			prevDiff = diff;
			prev = ja;
		}
	}
	if( prev.isRecord() ) {
		mBlockCurrentSync = true;
		mJobAssignmentView->setCurrent(prev,false,false);
		mJobAssignmentView->setSelection(prev,false,false);
		mBlockCurrentSync = false;
	}
}

void HostHistoryWindow::slotCurrentJobAssignmentChanged( const Record & r )
{
	if( mBlockCurrentSync ) return;
	JobAssignment ja(r);
	Interval prevDiff(Interval::null());
	HostHistory prev;
	for( ModelIter it(mHostHistoryView->model()); it.isValid(); ++it ) {
		HostHistory hh(mHostHistoryView->model()->getRecord(*it));
		Interval diff = Interval(hh.dateTime(),ja.started()).abs();
		if( ja.host() == hh.host() ) {
			if (!prevDiff.isNull() && diff > prevDiff)
				break;
			prevDiff = diff;
			prev = hh;
		}
	}
	if( prev.isRecord() ) {
		mBlockCurrentSync = true;
		mHostHistoryView->setCurrent(prev,false,false);
		mHostHistoryView->setSelection(prev,false,false);
		mBlockCurrentSync = false;
	}
}
