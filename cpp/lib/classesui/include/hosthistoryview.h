
#ifndef HOST_HISTORY_VIEW_H
#define HOST_HISTORY_VIEW_H

#include <qmainwindow.h>

#include "host.h"
#include "job.h"
#include "jobtask.h"

#include "recordtreeview.h"

#include "classesui.h"

class RecordSuperModel;

class HostHistoryItem;
typedef TemplateRecordDataTranslator<HostHistoryItem> HostHistoryTranslator;

class JobAssignmentItem;
typedef TemplateRecordDataTranslator<JobAssignmentItem> JobAssignmentTranslator;

class JobTaskAssignmentItem;
typedef TemplateRecordDataTranslator<JobTaskAssignmentItem> JobTaskAssignmentTranslator;

class CLASSESUI_EXPORT HostHistoryView : public RecordTreeView
{
Q_OBJECT
public:
	HostHistoryView( QWidget * parent );

public slots:
	void refresh();
	void setFilter( const Expression & );

protected slots:
	void doRefresh();
	void slotDoubleClicked( const QModelIndex & );
	
protected:
	void clearFilters();

	RecordSuperModel * mModel;
	HostHistoryTranslator * mTrans;
	bool mRefreshScheduled;
	Expression mFilter;
	RecordList mCache;
};

class CLASSESUI_EXPORT JobAssignmentView : public RecordTreeView
{
Q_OBJECT
public:
	JobAssignmentView( QWidget * parent );

	HostList hostFilter() const;

	Job jobFilter() const;

	JobTask taskFilter() const;

	int limit() const;

public slots:
	void refresh();
	void setFilter( const Expression & );

protected slots:
	void doRefresh();
	void slotDoubleClicked( const QModelIndex & );

protected:
	void clearFilters();

	RecordSuperModel * mModel;
	JobAssignmentTranslator * mTrans;
	JobTaskAssignmentTranslator * mTaskTrans;
	bool mRefreshScheduled;
	Expression mFilter;
	RecordList mCache;
};

class CLASSESUI_EXPORT HostHistoryWindow : public QMainWindow
{
Q_OBJECT
public:
	HostHistoryWindow( QWidget * parent = 0 );
	
	HostHistoryView * hostHistoryView() const;
	JobAssignmentView * jobAssignmentView() const;
	
	void showJobs( JobList );
	void showHosts( HostList );
	void showTasks( JobTaskList );
	
public slots:
	void setLimit();
	void refresh();
	void slotCurrentHostHistoryChanged( const Record & );
	void slotCurrentJobAssignmentChanged( const Record & );

protected:
	void closeEvent( QCloseEvent * );

	HostHistoryView * mHostHistoryView;
	JobAssignmentView * mJobAssignmentView;
	QAction * mCloseAction, * mRefreshAction, * mSetLimitAction;
	int mLimit;
	bool mBlockCurrentSync;
};

#endif // HOST_HISTORY_VIEW_H
