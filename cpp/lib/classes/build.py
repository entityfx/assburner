
import sys
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
sippath = os.path.join(path,'sipClasses')

try:
	os.mkdir(sippath)
except:	pass

pre_deps = ["stone"]
post_deps = []

classgen = ClassGenTarget("classgen",path,path + "/schema.xml", ["classmaker"])
pre_deps.append(classgen)

# If we are in the build tree(non-rpm build), then copy the ng_schema.xml so that it can be read by blur.quickinit
# This will make it available in the windows blur_python(cpp/lib/blur_python) installer, and for dev systems on linux
# that have a symlink to python/blur/ in their site/dist-packages
blurPythonInTree = os.path.join(path,'../../../python/blur/Classes/')
ngCopy = CopyTarget('classes_ng_schema_install',path,os.path.join(path,'ng_schema.xml'),os.path.join(blurPythonInTree,'ng_schema.xml'))
if os.path.exists( blurPythonInTree ):
	classgen.post_deps.append(ngCopy)

# By making it an external dependency it won't fail
classgen.post_deps.append('classes_ng_schema_install')


sipIncludes = ['..','../autocore/',
	'../../stone/include','/usr/include/stone',
	'../../qjson/','/usr/include/qjson',
	'/usr/include/classes','/usr/include/classes/base']
sipLibDirs = ['../../stone','../../qjson/']

# Python module targets, both depend on classes
pc = SipTarget2("pyclasses",path,'Classes')
pc.pre_deps = [classgen,"classes","pystone:install"]
pc.ExtraIncludes += sipIncludes
pc.ExtraLibs += [ 'stone', 'classes', 'qjson4' ]
pc.ExtraLibDirs += sipLibDirs

sst = SipTarget2("pyclassesstatic",path,'Classes',True)
sst.ExtraIncludes += sipIncludes
sst.ExtraLibDirs += sipLibDirs

sst.pre_deps = [classgen,"classes"]

# Install the libraries
if sys.platform != 'win32':
	LibInstallTarget("classesinstall",path,"classes","/usr/lib/")

QMakeTarget("classes",path,"classes.pro",pre_deps,post_deps)

# Create versioned dll and lib file
svnpri = WCRevTarget("classeslibsvnrevpri",path,"../..","svnrev-template.pri","svnrev.pri")
#post_deps.append(LibVersionTarget("classeslibversion","lib/classes","../..","classes"))
sv = QMakeTarget("classesversioned",path,"classes.pro",pre_deps + [svnpri],post_deps)
sv.Defines = ["versioned"]

rpm = RPMTarget('classesrpm','libclasses',path,'../../../rpm/spec/classes.spec.template','1.0')
rpm.pre_deps = ['classmakerrpm']

pyrpm = RPMTargetSip('pyclassesrpm','pyclasses',path,'../../../rpm/spec/pyclasses.spec.template','1.0')
pyrpm.Files += ['schema.xml','base']
pyrpm.Excludes += ['base/*.cpp']

if __name__ == "__main__":
	build()
