

SOURCES+=test.cpp

include(../../../qmake_shared/common.pri)

INCLUDEPATH+=../../stone/include ../autocore ..
LIBS+=-L../../stone -L../ -lclasses -lstone
TARGET=tests
CONFIG=qt thread warn_on debug qtestlib
QT+=xml network qt3support sql gui
DESTDIR=./
