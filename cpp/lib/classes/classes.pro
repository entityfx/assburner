
TARGET=classes
TEMPLATE=lib

include( svnrev.pri )
include( auto.pri )
include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/stone.pri)
include($$(PRI_SHARED)/python.pri)

# Move to auto
INCLUDES += \
	classes.h

SOURCES += \
	classes.cpp

DEFINES+=CLASSES_MAKE_DLL

INCLUDEPATH+=../stone/include autocore autoimp base /usr/include/stone .

win32 {
	LIBS+=-lMpr -lws2_32 -ladvapi32 -luser32
}

unix {
	INCLUDEPATH += ../sip/siplib/
}

CONFIG+=qt thread

target.path=$$LIB_PREFIX
INSTALLS += target

QT+=xml sql network widgets
