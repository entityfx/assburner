#ifndef COMMIT_CODE

#include "contract.h"
#include "contractproject.h"
#include "contractuser.h"
#include "elementuser.h"
#include "user.h"
ContractList Contract::forUser( User & user, QVariant isSigned ) {
	ContractList contracts;
	if (!user.isRecord()) {
		return ContractList();
	}
	ContractUserList cuList = ContractUser::recordsByUser(user);
	if (isSigned.toBool() == true) {
		return cuList.filter(ContractUser::c.SignedAt.isNotNull()).contracts();
	}
	Expression eleUserQuery, query, baseQuery, filterQuery;
	eleUserQuery = Query(ElementUser::c.Element, ElementUser::table(), (ElementUser::c.User == user));
	// All contracts for projects the user is linked to
	baseQuery = Contract::c.Key.in(Query(ContractProject::c.Contract, ContractProject::table(), ContractProject::c.Project.in(eleUserQuery)));
	// or contracts that all users must sign
	baseQuery |= Contract::c.AllUsersMustSign;
	
	// The Conatract's citizenship matches the users citizenship
	filterQuery = Contract::c.AppliesToCitizensRegex.isNull() | Expression(user.primaryCitizenship()).regexSearch(Contract::c.AppliesToCitizensRegex);
	// And include userType has not been set, or the include UserType is found in the user's dataType
	filterQuery &= Contract::c.IncludeUserType.coalesce(ev_(0)) == ev_(0) || (Contract::c.IncludeUserType & user.dataType()) != ev_(0);
	// And the exclude UserType is not found in the user's data type.
	filterQuery &= (Contract::c.ExcludeUserType.coalesce(ev_(0)) & user.dataType()) == ev_(0);
	
	// Include contracts the user is specificly linked to. And make sure all returned contracts are enabled
	query = ((baseQuery && filterQuery) || Contract::c.Key.in(cuList.contracts())) && Contract::c.Enabled;
	contracts = Contract::select(query);
	if (!isSigned.isNull() && isSigned.toBool() == false) {
		return contracts.filter(!(Contract::c.Key.in(cuList.filter(ContractUser::c.SignedAt.isNotNull()).contracts())));
	}
	return contracts;
}

#endif