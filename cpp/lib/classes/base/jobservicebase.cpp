
#include "jobservice.h"
#include "job.h"
#include "service.h"

JobServiceTrigger::JobServiceTrigger()
: Trigger( Trigger::PostInsertTrigger | Trigger::PostDeleteTrigger | Trigger::PostUpdateTrigger )
{}

void JobServiceTrigger::postInsert( RecordList jsl )
{
	foreach( JobService js, jsl )
		js.job().addHistory( "Service Added: " + js.service().service() );
}

void JobServiceTrigger::postUpdate( const Record & updated, const Record & old )
{
	JobService js(updated), jso(old);
	js.job().addHistory( "Service changed from " + jso.service().service() + " to " + js.service().service() );
}

void JobServiceTrigger::postDelete( RecordList jsl )
{
	foreach( JobService js, jsl )
		js.job().addHistory( "Service Removed: " + js.service().service() );
}

