
#ifndef COMMIT_CODE
#include "deliveryproblem.h"

DeliveryProblemList DeliveryProblem::recordsByIndex( const uint index ) {
	DeliveryProblemList ret = DeliveryProblemList();
	if (index != 0) {
		DeliveryProblemList all = DeliveryProblem::select();
		for (uint i=0; i < all.count(); i++) {
			DeliveryProblem item = all[i];
			if ( item.key() & index) {
				ret.append(item);
			}
		}
	}
	return ret;
}

#endif