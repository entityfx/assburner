
#include <qlayout.h>
#include <qmessagebox.h>
#include <qtoolbar.h>

#include "database.h"
#include "freezercore.h"
#include "process.h"

#include "busywidget.h"
#include "filteredit.h"
#include "modelgrouper.h"
#include "recordtreeview.h"
#include "viewcolors.h"

#include "afcommon.h"
#include "assfreezermenus.h"
#include "batchsubmitdialog.h"
#include "hostlistwidget.h"
#include "hostservice.h"
#include "items.h"
#include "joblistwidget.h"
#include "mainwindow.h"
#include "remotetailwindow.h"
#include "remotetailwidget.h"
#include "threadtasks.h"

HostListWidget::HostListWidget( QWidget * parent )
: AssfreezerView( parent )
, mHostFilterEdit(0)
, mHostTree(0)
, mServiceDataRetrieved( false )
, mShowOfflineHosts( false )
, mHostTaskRunning( false )
, mQueuedHostRefresh( false )
, mToolBar( 0 )
, mHostMenu( 0 )
, mTailServiceLogMenu( 0 )
, mHostServiceFilterMenu( 0 )
, mCannedBatchJobMenu( 0 )
{
	RefreshHostsAction = new QAction( "Refresh Hosts", this );
	RefreshHostsAction->setIcon( QIcon( ":/images/refresh" ) );
	HostOnlineAction = new QAction( "Set Host(s) Status to Online", this );
	HostOnlineAction->setIcon( QIcon( ":/images/host_online" ) );
	HostOfflineAction = new QAction( "Set Host(s) Status to Offline", this );
	HostOfflineAction->setIcon( QIcon( ":/images/host_offline" ) );
	HostRestartAction = new QAction( "Restart Host(s) Now", this );
	HostRestartAction->setIcon( QIcon( ":/images/host_restart" ) );
	HostRestartWhenDoneAction = new QAction( "Restart Host(s) When Done", this );
	HostRestartWhenDoneAction->setIcon( QIcon( ":/images/host_restart" ) );

	HostRebootAction = new QAction( "Reboot Host(s) Now", this );
	HostRebootAction->setIcon( QIcon( ":/images/host_reboot" ) );
	HostRebootWhenDoneAction = new QAction( "Reboot Host(s) When Done", this );
	HostRebootWhenDoneAction->setIcon( QIcon( ":/images/host_reboot" ) );

	VNCHostsAction = new QAction( "VNC Hosts", this );
	VNCHostsAction->setIcon( QIcon( ":/images/vnc_hosts.png" ) );
	ClientUpdateAction = new QAction( "Old[deprecated] Client Update", this );
	ClientUpdateAction->setIcon( QIcon( ":/images/client_update.png" ) );

	SubmitBatchJobAction = new QAction( "Assign Batch Job", this );
	ShowHostInfoAction = new QAction( "Host Info...", this );
	ClearHostErrorsSetOfflineAction = new QAction( "Clear All Errors From Host and Set It Offline", this );
	ClearHostErrorsAction = new QAction( "Clear All Errors From Host", this );
	ShowHostErrorsAction = new QAction( "Show Host Errors", this );
	ShowJobsAction = new QAction( "Show Assigned Jobs", this );

	connect( RefreshHostsAction, SIGNAL( triggered(bool) ), SLOT( refresh() ) );
	connect( HostOnlineAction, SIGNAL( triggered(bool) ), SLOT( setHostsOnline() ) );
	connect( HostOfflineAction, SIGNAL( triggered(bool) ), SLOT( setHostsOffline() ) );
	connect( HostRestartAction, SIGNAL( triggered(bool) ), SLOT( setHostsRestart() ) );
	connect( HostRestartWhenDoneAction, SIGNAL( triggered(bool) ), SLOT( setHostsRestartWhenDone() ) );
	connect( HostRebootAction, SIGNAL( triggered(bool) ), SLOT( setHostsReboot() ) );
	connect( HostRebootWhenDoneAction, SIGNAL( triggered(bool) ), SLOT( setHostsRebootWhenDone() ) );
	connect( ClientUpdateAction, SIGNAL( triggered(bool) ), SLOT( setHostsClientUpdate() ) );
	connect( VNCHostsAction, SIGNAL( triggered(bool) ), SLOT( vncHosts() ) );
	connect( ShowJobsAction, SIGNAL(triggered(bool)), SLOT( showAssignedJobs() ) );

	mHostFilterEdit = new FilterEdit( this, Host::c.Name, "Host Name Filter:" );
	connect( mHostFilterEdit, SIGNAL(filterChanged(const Expression &)), SLOT(refresh()) );

	mHostTree = new RecordTreeView(this);
	QLayout * hbox = new QHBoxLayout(this);
	hbox->setMargin(0);
	hbox->addWidget(mHostTree);

	/* ListView connections */
	connect( mHostTree, SIGNAL( selectionChanged(RecordList) ), SLOT( hostListSelectionChanged() ) );

	mHostTree->setContextMenuPolicy( Qt::CustomContextMenu );
	connect( mHostTree, SIGNAL( customContextMenuRequested( const QPoint & ) ), SLOT( showHostPopup( const QPoint & ) ) );
	//connect( mHostTree, SIGNAL( itemDoubleClicked( QTreeWidgetItem *, int ) ), SLOT( vncHosts() ) );

	RecordSuperModel * hm = new RecordSuperModel(mHostTree);
	new HostTranslator( hm->treeBuilder() );
	// Enable grouping
	hm->grouper()->setColumnGroupRegex( 0, QRegExp( "[^\\d]+" ) );
	connect( hm->grouper(), SIGNAL( groupingChanged(bool) ), SLOT( slotGroupingChanged(bool) ) );
	
	hm->setAutoSort( true );
	mHostTree->setModel( hm );
	mHostTree->setUniformRowHeights(true);
	
	mTailServiceLogMenu = new TailServiceLogMenu( this );
	mHostServiceFilterMenu = new HostServiceFilterMenu( this );
	mCannedBatchJobMenu = new CannedBatchJobMenu( this );

	IniConfig temp;
	restore(temp);

	FreezerCore::addTask( new StaticHostListDataTask( this ) );
}

HostListWidget::~HostListWidget()
{
}

QString HostListWidget::viewType() const
{
	return "HostList";
}

RecordTreeView * HostListWidget::hostTreeView() const
{
	return mHostTree;
}

void HostListWidget::save( IniConfig & ini, bool forceFullSave )
{
	ini.writeString("ServiceFilter", mServiceFilter.keyString());
	ini.writeBool("ShowOfflineHosts", mShowOfflineHosts);
	saveHostView(mHostTree,ini);
	AssfreezerView::save(ini,forceFullSave);
}

void HostListWidget::restore( IniConfig & ini, bool forceFullRestore )
{
	mShowOfflineHosts = ini.readBool( "ShowOfflineHosts", false );
	setupHostView(mHostTree,ini);
	AssfreezerView::restore(ini,forceFullRestore);
}

//#include <valgrind/callgrind.h>

void HostListWidget::customEvent( QEvent * evt )
{
	if( ((ThreadTask*)evt)->mCancel )
		return;
		
	switch( (int)evt->type() ) {
		case HOST_LIST:
		{
			//LOG_5( "Updating host view items" );
			//QTime t;
			//t.start();
			HostList updated = ((HostListTask*)evt)->mReturn;
			//LOG_5( "updated "+ QString::number(updated.size()) + " hosts from db" );
			// Recursive so the update works with grouping
			//CALLGRIND_START_INSTRUMENTATION;
			mHostTree->model()->updateRecords( updated, QModelIndex(), /*recursive=*/true );
			//CALLGRIND_STOP_INSTRUMENTATION;
			
			//LOG_5( "Enabling updates took " + QString::number( t.elapsed() ) + " ms" );

			if( mHostsToSelect.size() ) {
				mHostTree->setSelection( mHostsToSelect );
				mHostTree->sortBySelection();
				mHostTree->scrollTo( mHostsToSelect );
				mHostsToSelect = HostList();
			}

			mHostTree->busyWidget()->stop();
			
			mHostTaskRunning = false;
			if( mQueuedHostRefresh ) {
				mQueuedHostRefresh = false;
				refresh();
			} else
				clearStatusBar();
			break;
		}
		case STATIC_HOST_LIST_DATA:
		{
			mServiceDataRetrieved = true;
			StaticHostListDataTask * sdt = (StaticHostListDataTask*)evt;
			mServiceList = sdt->mServices;
			IniConfig ini(viewConfig());
			// Show All Services by default
			mServiceFilter = Service::table()->records( ini.readUIntList("ServiceFilter", mServiceList.keys()) );
			if( mQueuedHostRefresh ) {
				mQueuedHostRefresh = false;
				doRefresh();
			}
			break;
		}
		default:
			break;
	}
}

void HostListWidget::doRefresh()
{
	AssfreezerView::doRefresh();
	bool needStatusBarMsg = false, needHostListTask = false;

	if( !mServiceDataRetrieved )
		needStatusBarMsg = mQueuedHostRefresh = true;
	else if( mHostTaskRunning )
		mQueuedHostRefresh = true;
	else
		needStatusBarMsg = needHostListTask = true;

	if( needStatusBarMsg )
		setStatusBarMessage( "Refreshing Host List..." );

	if( needHostListTask ) {
		mHostTaskRunning = true;
		mHostTree->busyWidget()->start();
		FreezerCore::addTask( new HostListTask( this, mServiceFilter, mServiceList, mHostFilterEdit->expression(), !mShowOfflineHosts, !mHostTree->isColumnHidden(15) /*Services Column*/, !mHostTree->isColumnHidden(17) /*IP Address*/ ) );
		FreezerCore::wakeup();
	}
}

void HostListWidget::hostListSelectionChanged()
{
	HostList hosts = mHostTree->selection();
	if( hosts.size() == 1 ) {
		setStatusBarMessage( hosts[0].name() + " Selected" );
	} else if( hosts.size() )
		setStatusBarMessage( QString::number( hosts.size() ) + " Hosts Selected" );
	else
		clearStatusBar();
}

void HostListWidget::selectHosts( HostList hosts )
{
	mHostTree->setSelection( hosts );
	mHostTree->scrollTo( hosts );
	if( mHostTaskRunning || mQueuedHostRefresh || refreshCount() == 0 )
		mHostsToSelect = hosts;
}

QToolBar * HostListWidget::toolBar( QMainWindow * mw )
{
	if( !mToolBar ) {
		mToolBar = new QToolBar( mw );
		mToolBar->addAction( RefreshHostsAction );
		mToolBar->addSeparator();
		mToolBar->addAction( HostOnlineAction );
		mToolBar->addAction( HostOfflineAction );
		mToolBar->addAction( HostRestartAction );
		mToolBar->addAction( VNCHostsAction );
		mToolBar->addWidget( mHostFilterEdit );
	}
	return mToolBar;
}

void HostListWidget::populateViewMenu( QMenu * viewMenu )
{
	viewMenu->addMenu( mHostServiceFilterMenu );
}

void HostListWidget::setHostsStatus(const QString & status)
{
	// Get a list of ids for the selected hosts
	HostList hosts = mHostTree->selection();
	if( hosts.size()==0 )
		return;

	Database::current()->beginTransaction();
	QString hostString = hosts.keyString();
	if( !Database::current()->exec( "UPDATE HostStatus SET slaveStatus = '" + status + "', fkeyJob=NULL, slaveFrames=NULL,fkeyjobcommandhistory=NULL, fkeyjobtype=NULL, fkeyjobtask=NULL WHERE fkeyHost IN(" + hostString + ");" )
		.isActive() )
		return;
	QStringList returnTasksSql;
	foreach( Host h, hosts )
		returnTasksSql += "return_slave_tasks_2(" + QString::number( h.key() ) + ")";
	Database::current()->exec("SELECT " + returnTasksSql.join(",") + ";");
	Database::current()->commitTransaction();
}

void HostListWidget::setHostsOnline()
{
	setHostsStatus("starting");
	refresh();
}

void HostListWidget::setHostsOffline()
{
	setHostsStatus("stopping");
	refresh();
}

void HostListWidget::setHostsRestart()
{
	HostList hosts = mHostTree->selection();
	if( hosts.hostStatuses().filter("status","busy").size() > 0 ) {
		QString hostNames = hosts.names().join(",");
		if( hostNames.size() > 100 )
			hostNames = QString::number(hosts.size()) + " hosts [" + hostNames.left(80) + "]";
		if( QMessageBox::warning( this, "Restart Hosts", "Are you sure that you want to restart the following hosts?\n" + hostNames + "\nSome of the hosts are busy and the current progress will be lost.",
				QMessageBox::Yes, QMessageBox::Cancel ) != QMessageBox::Yes )
			return;
	}
	setHostsStatus("restart");
	refresh();
}

void HostListWidget::setHostsRestartWhenDone()
{
	HostList hosts = mHostTree->selection();
	if( hosts.size()==0 )
		return;

	Database::current()->exec( "UPDATE HostStatus SET slaveStatus = 'restart-when-done' WHERE fkeyHost IN(" + hosts.keyString() + ");" );
	refresh();
}

void HostListWidget::setHostsReboot()
{
	setHostsStatus("reboot");
	refresh();
}

void HostListWidget::setHostsRebootWhenDone()
{
	HostList hosts = mHostTree->selection();
	if( hosts.size()==0 )
		return;

	Database::current()->exec( "UPDATE HostStatus SET slaveStatus = 'reboot-when-done' WHERE fkeyHost IN(" + hosts.keyString() + ");" );
	refresh();
}

void HostListWidget::setHostsClientUpdate()
{
	setHostsStatus("client-update");
	refresh();
}

void HostListWidget::vncHosts()
{
	foreach( Host h, mHostTree->selection() )
		vncHost( h.name() );
}

void HostListWidget::showAssignedJobs()
{
	MainWindow * mw = qobject_cast<MainWindow*>(window());
	if( mw ) {
		JobListWidget * jobList = new JobListWidget(mw);
		jobList->setJobList( Host::activeAssignments(mHostTree->selection()).jobs() );
		mw->insertView(jobList);
		mw->setCurrentView(jobList);
	}
}

void HostListWidget::applyOptions()
{
	options.mHostColors->apply(mHostTree);
	mHostTree->setFont( options.jobFont );

	ColorOption * co = options.mHostColors->getColorOption("default");
	if (co) {
		QPalette p = mHostTree->palette();
		p.setColor(QPalette::Active, QPalette::AlternateBase, co->bg.darker(120));
		p.setColor(QPalette::Inactive, QPalette::AlternateBase, co->bg.darker(120));
		mHostTree->setPalette( p );
	}

	mHostTree->update();
}

QString jobErrorAsString( const JobError & je )
{
	QDateTime dt;
	dt.setTime_t(je.errorTime());
	return "[" + je.host().name() + "; " + dt.toString() + "; Frames " + je.frames() + "]\n" + je.message();
}

QString verifyKeyList( const QString & list, Table * table )
{
	QList<uint> keys;
	QStringList sl = list.split(',');
	foreach( QString key, sl )
		keys += key.toInt();
	return table->records(keys).keyString();
}

QList<uint> verifyKeyList( const QList<uint> & keys, Table * table )
{
	return table->records(keys).keys();
}

void clearHostErrorsAndSetOffline( HostList hosts, bool offline)
{
	if( hosts.isEmpty() ) return;

	if (offline) {
		QString subject = "Setting Hosts Offline for repair: " + hosts.names().join(",");
		QString body = subject + "\\nAll Current Errors have been cleared";
		foreach( Host h, hosts ) {
			body += h.name() + "\\n";
			JobErrorList errors = JobError::select("fkeyhost=? order by errortime desc limit 10", VarList() << h.key());
			if( !errors.isEmpty() ) {
				body += "\\n";
				foreach(JobError je, errors)
					body += jobErrorAsString(je).replace("\n","\\n").replace("'","") + "\\n";
			}
			body += "\\n\\n";
		}

		QString from = User::currentUser().name() + "@blur.com";
		sendEmail( QStringList() << "it@blur.com", subject, body, from );
	}

	Database::current()->exec( "UPDATE JobError SET cleared=true WHERE fkeyhost IN (" + hosts.keyString() + ")" );
	if (offline) {
		HostStatusList hsl = hosts.hostStatuses();
		hsl.setSlaveStatuses( "offline" );
		hsl.commit();
	}
}

void HostListWidget::showHostPopup(const QPoint & point)
{
	if( !mHostMenu ) mHostMenu = new AssfreezerHostMenu( this );
	mHostMenu->popup( mHostTree->mapToGlobal(point) );
}

void HostListWidget::slotGroupingChanged(bool grouped)
{
	mHostTree->setRootIsDecorated(grouped);
}
