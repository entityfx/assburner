
#include <qapplication.h>
#include <qaction.h>
#include <qfileinfo.h>
#include <qinputdialog.h>
#include <qmainwindow.h>
#include <qmessagebox.h>
#include <qprocess.h>
#include <qsplitter.h>
#include <qtimer.h>
#include <qtoolbar.h>
#include <qclipboard.h>

#include <stdlib.h>

#include "blurqt.h"
#include "database.h"
#include "freezercore.h"
#include "path.h"
#include "process.h"

#include "jobbatch.h"
#include "jobcommandhistory.h"
#include "jobhistory.h"
#include "jobhistorytype.h"
#include "jobtype.h"
#include "projectstatus.h"
#include "service.h"
#include "user.h"

#include "busywidget.h"
#include "modelgrouper.h"
#include "recordtreeview.h"
#include "recordpropvaltree.h"

#include "assfreezermenus.h"
#include "batchsubmitdialog.h"
#include "hostlistwidget.h"
#include "imageview.h"
#include "items.h"
#include "joblistwidget.h"
#include "jobstatwidget.h"
#include "mainwindow.h"
#include "tabtoolbar.h"
#include "threadtasks.h"

#ifdef LoadImage
#undef LoadImage
#endif

JobListFilterEdit::JobListFilterEdit( QWidget * parent, Field * matchField, const QString & label )
: FilterEdit(parent,matchField,label)
{}

Expression JobListFilterEdit::generateExpression(const QString & editText)
{
	mJobList.clear();
	Expression ret;
	QList<uint> jobIds;
	UserList users;
	QString et(editText);
	QRegExp userJobExp( "^[\\s,]?((?:\\d+)|(?:[^\\d,\\s]+))(?:\\s|,|$)" );
	while( userJobExp.indexIn(et) == 0 ) {
		QString cap = userJobExp.cap(1);
		if( cap[0].isDigit() ) {
			Job j = Job(cap.toUInt());
			if( !j.isRecord() )
				break;
			mJobList.append(j);
		} else {
			User u = User::recordByUserName(cap);
			if( u.isRecord() )
				users += u;
			else
				break;
		}
		et = et.mid(userJobExp.matchedLength());
	}
	ret = FilterEdit::generateExpression(et);
	if( users.size() )
		ret &= Job::c.User.in(users);
	return ret;
}

JobListWidget::SharedData * JobListWidget::mSharedData = 0;

JobListWidget::JobListWidget( QWidget * parent )
: AssfreezerView( parent )
, mJobFilterEdit( 0 )
, mToolBar( 0 )
, mViewsInitialized( false )
, mJobTaskRunning( false )
, mQueuedJobRefresh( false )
, mShowClearedErrors( false )
, mFrameTask( 0 )
, mPartialFrameTask( 0 )
, mStaticDataRetrieved( false )
, mJobMenu( 0 )
, mStatusFilterMenu( 0 )
, mProjectFilterMenu( 0 )
, mJobTypeFilterMenu( 0 )
, mTaskMenu( 0 )
, mErrorMenu( 0 )
{
	if( !mSharedData ) {
		mSharedData = new SharedData;
		mSharedData->mRefCount = 1;
	} else
		mSharedData->mRefCount++;

	setupUi(this);
}

JobListWidget::~JobListWidget()
{
	if( mSharedData->mRefCount-- == 0 )
		delete mSharedData;
}

QString JobListWidget::viewType() const
{
	return "JobList";
}

void JobListWidget::initializeViews(IniConfig & ini)
{
	if( !mViewsInitialized ) {
		mViewsInitialized = true;
		FreezerCore::addTask( new StaticJobListDataTask( this ) );

		mTabToolBar = new TabToolBar( mJobTabWidget, mImageView );
		connect( mTabToolBar, SIGNAL(outputChanged(const QString &)), SLOT(currentOutputChanged(const QString &)) );
		
		RefreshAction = new QAction( "Refresh Job(s)", this );
		RefreshAction->setIcon( QIcon( ":/images/refresh" ) );

		KillAction = new QAction( "Remove Selected Jobs", this );
		KillAction->setIcon( QIcon( ":/images/kill" ) );
		PauseAction = new QAction( "Pause Selected Jobs", this );
		PauseAction->setIcon( QIcon( ":/images/pause" ) );
		ResumeAction = new QAction( "Resume Selected Jobs", this );
		ResumeAction->setIcon( QIcon( ":/images/resume" ) );
		RestartAction = new QAction( "Restart Job(s)", this );
		RestartAction->setIcon( QIcon( ":/images/restart" ) );
		ShowOutputAction = new QAction( "Show Output in Explorer", this );
		ShowOutputAction->setIcon( QIcon( ":/images/explorer" ) );
		ShowMineAction = new QAction( "View My Jobs", this );
		ShowMineAction->setCheckable( true );
		ShowMineAction->setIcon( QIcon( ":/images/show_mine" ) );
		FrameCyclerAction = new QAction( "Frame Cycler", this );
		FrameCyclerAction->setIcon( QIcon( ":/images/imagecycler.png" ) );
		PdPlayerAction = new QAction( "Pd Player", this );
		//PdPlayerAction->setIcon( QIcon( ":/images/imagecycler.png" ) );
		ClearErrorsAction = new QAction( "Clear Job Errors", this );
		ExploreJobFile = new QAction( "Show Job File in Explorer", this );
		ExploreJobFile->setIcon( QIcon( ":/images/explorer" ) );
		DependencyTreeEnabledAction = new QAction( "Show Dependency Tree", this );
		DependencyTreeEnabledAction->setCheckable( true );
		connect( DependencyTreeEnabledAction, SIGNAL( toggled( bool ) ), SLOT( setDependencyTreeEnabled( bool ) ) );

		NewViewFromSelectionAction = new QAction( "New View From Selection", this );
		connect( NewViewFromSelectionAction, SIGNAL( triggered(bool) ), SLOT( createNewViewFromSelection() ) );

		mJobFilterEdit = new JobListFilterEdit( this, Job::c.Name, "Job Name Filter:" );
		connect( mJobFilterEdit, SIGNAL( filterChanged( const Expression & ) ), SLOT( jobFilterChanged( const Expression & ) ) );

		connect( RefreshAction, SIGNAL( triggered(bool) ), SLOT( refresh() ) );

		connect( RestartAction, SIGNAL( triggered(bool) ), SLOT( restartJobs() ) );
		connect( ResumeAction, SIGNAL( triggered(bool) ), SLOT( resumeJobs() ) );
		connect( PauseAction, SIGNAL( triggered(bool) ), SLOT( suspendJobs() ) );
		connect( KillAction, SIGNAL( triggered(bool) ), SLOT( deleteJobs() ) );
		connect( ShowMineAction, SIGNAL( toggled(bool) ), SLOT( showMine(bool) ) );
		connect( FrameCyclerAction, SIGNAL( triggered(bool) ), SLOT( frameCycler() ) );
		connect( PdPlayerAction, SIGNAL( triggered(bool) ), SLOT( pdPlayer() ) );
		connect( ShowOutputAction, SIGNAL( triggered(bool) ), SLOT( outputPathExplorer() ) );
		connect( ClearErrorsAction, SIGNAL( triggered(bool) ), SLOT( clearErrors() ) );
		connect( ExploreJobFile, SIGNAL( triggered(bool) ), SLOT( exploreJobFile() ) );

		connect( mJobTree, SIGNAL( selectionChanged(RecordList) ), SLOT( jobListSelectionChanged() ) );
		connect( mJobTree, SIGNAL( currentChanged( const Record & ) ), SLOT( currentJobChanged() ) );
		connect( mJobTree, SIGNAL( expanded( const QModelIndex & ) ), SLOT( jobDependenciesExpanded( const QModelIndex & ) ) );

		connect( mFrameTree, SIGNAL( currentChanged( const Record & ) ), SLOT( frameSelected(const Record &) ) );
		connect( mFrameTree,  SIGNAL( selectionChanged(RecordList) ), SLOT( frameListSelectionChanged() ) );
		connect( mImageView, SIGNAL( frameStatusChange(int,int) ), SLOT( setFrameCacheStatus(int,int) ) );

		mJobTree->setContextMenuPolicy( Qt::CustomContextMenu );
		mFrameTree->setContextMenuPolicy( Qt::CustomContextMenu );
		mErrorTree->setContextMenuPolicy( Qt::CustomContextMenu );

		connect( mJobTree, SIGNAL( customContextMenuRequested( const QPoint & ) ), SLOT( showJobPopup( const QPoint & ) ) );
		connect( mFrameTree, SIGNAL( customContextMenuRequested( const QPoint & ) ), SLOT( showFramePopup( const QPoint & ) ) );
		connect( mErrorTree, SIGNAL( customContextMenuRequested( const QPoint & ) ), SLOT( showErrorPopup( const QPoint & ) ) );
		connect( mErrorTree, SIGNAL( aboutToShowHeaderMenu( QMenu * ) ), SLOT( populateErrorTreeHeaderMenu( QMenu * ) ) );

		connect( mJobTabWidget, SIGNAL( currentChanged( int ) ), SLOT( currentTabChanged() ) );

		{
			JobModel * jm = new JobModel( mJobTree );
			jm->setAutoSort( true );
			mJobTree->setModel( jm );
			mJobTree->setUniformRowHeights(true);
			mJobTree->setItemDelegate( new ProgressDelegate( mJobTree ) );
			mJobTree->setDragEnabled( true );
			mJobTree->setAcceptDrops( true );
			mJobTree->setDropIndicatorShown(true);
			connect( jm, SIGNAL( dependencyAdded( const QModelIndex & ) ), mJobTree, SLOT( expand( const QModelIndex & ) ) );
		}

		{
			//mErrorTree->setItemDelegate( new MultiLineDelegate( mErrorTree ) );
			mErrorTree->setUniformRowHeights( false );
			mErrorTree->setVerticalScrollMode( QTreeView::ScrollPerPixel );
			mErrorTree->setRootIsDecorated(true);
			JobErrorModel * em = new JobErrorModel( mErrorTree );
			em->setAutoSort( true );
			mErrorTree->setModel( em );
			for( int i=0; i<7; i++ )
				mErrorTree->setColumnAutoResize(i,true);
		}

		{
			RecordSuperModel * fm = new RecordSuperModel( mFrameTree );
			new FrameTranslator( fm->treeBuilder() );
			fm->setAutoSort( true );
			mFrameTree->setModel( fm );
			mFrameTree->setItemDelegate( new LoadedDelegate( mJobTree ) );
			for( int i=0; i<7; i++ )
				mFrameTree->setColumnAutoResize( i, true );
		}
		mStatusFilterMenu = new StatusFilterMenu( this );
		mProjectFilterMenu = new ProjectFilterMenu( this );
		mJobTypeFilterMenu = new JobListJobTypeFilterMenu( this );
		mJobServiceFilterMenu = new JobServiceFilterMenu( this );

		jobListSelectionChanged();
		
		setupJobView(mJobTree, ini);
		setupJobErrorView(mErrorTree, ini);
		setupFrameView(mFrameTree, ini);

		setDependencyTreeEnabled( ini.readBool( "DependencyTreeEnabled", false ), /* allowRefresh= */ false );

		QStringList sl = ini.readString( "JobSplitterPos" ).split(',');
		QList<int> vl;
		foreach( QString s, sl )
			vl << s.toInt();
		
		// Reasonable defaults for the splitter
		if( vl.size()!=2 ) {
			vl.clear();
			int h = height();
			if( h < 300 )
				vl << h << 0;
			else
				vl << (int)(h * .6) << (int)(h * .4);
		}

		mJobSplitter->setSizes( vl );

		sl = ini.readString( "TaskImageSplitterPos" ).split(',');
		vl.clear();
		foreach( QString s, sl )
			vl << s.toInt();
		
		if( vl.size() != 2 ) {
			vl.clear();
			vl << 400 << width() - 400;
		}
		
		mTaskImageSplitter->setSizes( vl );
		
		ShowMineAction->blockSignals(true);
		ShowMineAction->setChecked( !ini.readString( "UserList", "" ).isEmpty() );
		ShowMineAction->blockSignals(false);
		
		// Filter any empty entries.  And empty string split with ',' returns a string list with one empty entry
		mJobFilter.typesToShow = ini.readUIntList( "TypeToShow" );
		mJobFilter.statusToShow = 	ini.readString( "StatusToShow", "submit,verify,ready,holding,started,suspended" ).split(',',QString::SkipEmptyParts);
		mJobFilter.userList = 		ini.readUIntList( "UserList" );
		mJobFilter.allProjectsShown = ini.readBool( "AllProjectsShown", false );
		if( ini.keys().contains( "VisibleProjects" ) ) {
			mJobFilter.visibleProjects = ini.readUIntList( "VisibleProjects" );
		} else {
			mJobFilter.hiddenProjects = ini.readUIntList( "HiddenProjects" );
			if( mJobFilter.hiddenProjects.isEmpty() )
				mJobFilter.allProjectsShown = true;
		}
		mJobFilter.showNonProjectJobs = ini.readBool( "ShowNonProjectJobs", true );
		mJobFilter.allServicesShown = ini.readBool( "AllServicesShown", true );
		mJobFilter.servicesToShow = ini.readUIntList( "ServicesToShow" );
		mJobFilterEdit->lineEdit()->setText(ini.readString( "ExtraFilters", "" ));
		mJobFilter.mExtraFilters = mJobFilterEdit->expression();
		mJobFilter.mLimit = options.mLimit;
		applyOptions();

		// This only changes layout properties and hidden/shown mTabToolBar
		currentTabChanged( /*refresh=*/ false );
	}
}

void JobListWidget::save( IniConfig & ini, bool forceFullSave )
{
	// We use the viewConfig() for doing the actual restore now, since the ini
	// passed to this function may be a blank one used to make a copy of this view
	if( forceFullSave && !mViewsInitialized ) {
		IniConfig cfg(viewConfig());
		initializeViews(cfg);
	}

	if( mViewsInitialized ) {
		saveJobView(mJobTree,ini);
		saveJobErrorView(mErrorTree,ini);
		saveFrameView(mFrameTree,ini);
		ini.writeString( "StatusToShow", mJobFilter.statusToShow.join(",") );
		ini.writeUIntList( "UserList", mJobFilter.userList );
		ini.writeUIntList( "VisibleProjects", mJobFilter.visibleProjects );
		ini.removeKey( "HiddenProjects" );
		ini.writeBool( "AllProjectsShown", mJobFilter.allProjectsShown );
		ini.writeBool( "ShowNonProjectJobs", mJobFilter.showNonProjectJobs );
		ini.writeString( "ExtraFilters", mJobFilterEdit->lineEdit()->text() );
		ini.writeUIntList( "TypeToShow", mJobFilter.typesToShow );
		ini.writeUIntList( "TypesHidden", (mSharedData->mJobTypeList - JobType::table()->records( mJobFilter.typesToShow )).keys() );
		ini.writeBool( "AllServicesShown", mJobFilter.servicesToShow.size() == activeServices().size() );
		ini.writeUIntList( "ServicesToShow", mJobFilter.servicesToShow );
		ini.writeBool( "DependencyTreeEnabled", isDependencyTreeEnabled() );
		// Save the splitter position by making a string of ints separated by commas
		// Output string
		QString jsps;
		// Use comma
		QList<int> list = mJobSplitter->sizes();
		// Stupid qsplitter doesn't return proper sizes if this tab hasn't been shown
		// It returns 12/12 or 13/13 in that case so we won't save
		if( list.size() == 2 && !(list[0] == list[1] && list[0] < 20)) {
			QStringList sizes;
			foreach( int i, list ) sizes += QString::number(i);
			ini.writeString( "JobSplitterPos", sizes.join(",") );
		}
		list = mTaskImageSplitter->sizes();
		// Stupid qsplitter doesn't return proper sizes if this tab hasn't been shown
		// It returns 12/12 or 13/13 in that case so we won't save
		if( list.size() == 2 && !(list[0] == list[1] && list[0] < 20)) {
			QStringList sizes;
			foreach( int i, list ) sizes += QString::number(i);
			ini.writeString( "TaskImageSplitterPos", sizes.join(",") );
		}
	}
	AssfreezerView::save(ini,forceFullSave);
}

void JobListWidget::restore( IniConfig & ini, bool forceFullRestore )
{
	AssfreezerView::restore(ini,forceFullRestore);
	if( forceFullRestore )
		initializeViews(ini);
}

ProjectList JobListWidget::activeProjects()
{
	return mSharedData ? mSharedData->mProjectList : ProjectList();
}

JobTypeList JobListWidget::activeJobTypes()
{
	return mSharedData ? mSharedData->mJobTypeList : JobTypeList();
}

ServiceList JobListWidget::activeServices()
{
	return mSharedData ? mSharedData->mServiceList : ServiceList();
}

bool JobListWidget::isDependencyTreeEnabled() const
{
	return ((JobModel*)mJobTree->model())->isDependencyTreeEnabled();
}

void JobListWidget::setDependencyTreeEnabled( bool dte, bool allowRefresh )
{
	if( dte != isDependencyTreeEnabled() ) {
		((JobModel*)mJobTree->model())->setDependencyTreeEnabled( dte );
		DependencyTreeEnabledAction->setChecked( dte );
		mJobTree->setRootIsDecorated( dte );
		if( allowRefresh && dte ) refresh();
	}
}

void JobListWidget::jobDependenciesExpanded( const QModelIndex & idx )
{
	if( !idx.parent().isValid() )
		mJobTree->expandRecursive( idx );
}

bool JobListWidget::event( QEvent * event )
{
	if( event->type() == QEvent::Show ) {
		if( !mViewsInitialized ) {
			IniConfig cfg(viewConfig());
			initializeViews(cfg);
		}
	}
	return QWidget::event(event);
}

//#define TIME_JOBLIST_UPDATE
void JobListWidget::customEvent( QEvent * evt )
{
	switch( (int)evt->type() ) {
		case JOB_LIST:
		{
			JobListTask * jlt = ((JobListTask*)evt);
			JobModel * jm = (JobModel*)mJobTree->model();
			JobList existing, toAdd;
			QModelIndexList toRemove;
			QMap<int,QPair<QModelIndex,bool> > existingMap;
#ifdef TIME_JOBLIST_UPDATE
			QTime t;
			t.start();
#endif
			JobList topLevelJobs = jlt->mReturn - jlt->mDependentJobs;
			//LOG_5( QString("Took %1 ms to subtract dependant jobs. %2 top level jobs").arg(t.elapsed()).arg(topLevelJobs.size()) );
			
#ifdef TIME_JOBLIST_UPDATE
			t.start();
#endif
			ModelGrouper * grouper = jm->grouper();
			int topLevelDepth = 0;
			if( grouper && grouper->isGrouped() )
				topLevelDepth = 1;

			for( ModelIter it(jm,ModelIter::Filter(ModelIter::Recursive|ModelIter::DescendLoadedOnly)); it.isValid(); ++it ) {
				QModelIndex idx = *it;
				if( topLevelDepth == it.depth() ) {
					Job j = jm->getRecord(idx);
					existingMap[j.key()] = qMakePair<QModelIndex,bool>(idx,false);
				}
			}
#ifdef TIME_JOBLIST_UPDATE
			LOG_5( QString("Took %1 ms to map existing jobs, %2 jobs mapped").arg(t.elapsed()).arg(existingMap.size()) );
			t.start();
#endif
			
			foreach( Job j, topLevelJobs ) {
				QMap<int,QPair<QModelIndex,bool> >::Iterator it = existingMap.find(j.key());
				if( it != existingMap.end() ) {
					existing += j;
					jm->updateIndex(it.value().first);
					it.value().second = true;
				} else
					toAdd += j;
			}
#ifdef TIME_JOBLIST_UPDATE
			LOG_5( QString("Took %1 ms to update existing indexes").arg(t.elapsed()) );
			t.start();
#endif
			
			for( QMap<int,QPair<QModelIndex,bool> >::Iterator it = existingMap.begin(); it != existingMap.end(); ++it )
				if( !it.value().second )
					toRemove += it.value().first;
			
			LOG_5( "Removing " + QString::number(toRemove.size()) + " jobs" );
			jm->remove(toRemove);
#ifdef TIME_JOBLIST_UPDATE
			LOG_5( QString("Took %1 ms to remove old jobs").arg(t.elapsed()) );
			LOG_5( "Appending " + QString::number(toAdd.size()) + " jobs to the list" );
			t.start();
#endif
			
			jm->append( toAdd );
#ifdef TIME_JOBLIST_UPDATE
			LOG_5( QString("Took %1 ms to append new jobs").arg(t.elapsed()) );
			t.start();
#endif
			
			QMap<Record, JobServiceList> jobServicesByJob;
			if( jlt->mFetchJobServices ) {
				jobServicesByJob = jlt->mJobServices.groupedBy<Record,JobServiceList,uint,Job>( "fkeyjob" );
				//LOG_5( QString("Got %1 services for %2 jobs").arg(jlt->mJobServices.size()).arg(jobServicesByJob.size()) );
			}

#ifdef TIME_JOBLIST_UPDATE
			LOG_5( QString("Took %1 ms to group services").arg(t.elapsed()) );
			t.start();
#endif

			QMap<uint,JobDepList> jobDepsByJob = jlt->mJobDeps.groupedBy<uint,JobDepList>("fkeyjob");

#ifdef TIME_JOBLIST_UPDATE
			LOG_5( QString("Took %1 ms to group deps").arg(t.elapsed()) );
			t.start();
#endif

			for( ModelIter it(jm,ModelIter::Filter(ModelIter::Recursive|ModelIter::DescendLoadedOnly)); it.isValid(); ++it ) {
				Job j = jm->getRecord(*it);
				if( j.isRecord() ) {
					JobDepList deps = jobDepsByJob[j.key()];
					if( deps.size() )
						jm->updateRecords( deps.deps(), *it, false );
					if( jlt->mFetchJobServices ) {
						// Update services
						JobItem & ji = JobTranslator::data(*it);
						QMap<Record,JobServiceList>::iterator jsit = jobServicesByJob.find(ji.job);
						if( jsit != jobServicesByJob.end() )
							ji.services = jsit.value().services().services().join(",");
					}
				}
			}
#ifdef TIME_JOBLIST_UPDATE
			LOG_5( QString("Took %1 ms to setup deps and services").arg(t.elapsed()) );
#endif

			mJobTree->busyWidget()->stop();
			mJobTaskRunning = false;
			// This will update the status bar and action states
			// since they selected jobs may have changed
			jobListSelectionChanged();

			if( mQueuedJobRefresh ) {
				mQueuedJobRefresh = false;
				refresh();
			}
			break;
		}
		case FRAME_LIST:
		{
			int minFrame = -1, maxFrame = -1;
			FrameListTask * flt = (FrameListTask*)evt;
			JobTaskList jtl = flt->mReturn;
			FrameItem::CurTime = flt->mCurTime;
			mCurrentOutputs = flt->mOutputs;
			foreach( JobTask jt, jtl )
			{
				if( minFrame==-1 || (int)jt.frameNumber() < minFrame )
					minFrame = jt.frameNumber();
				if( maxFrame==-1 || (int)jt.frameNumber() > maxFrame )
					maxFrame = jt.frameNumber();
			}
			mMinFrame = minFrame;
			mMaxFrame = maxFrame;
			mFrameTree->model()->updateRecords( jtl );
			mTabToolBar->slotPause();
			mTabToolBar->setOutputs( mCurrentOutputs.names() );
			if( mCurrentOutputs.isEmpty() )
				mImageView->setFrameRange( mCurrentJob.outputPath(), minFrame, maxFrame, true /*!mCurrentJob.jobType().name().startsWith("Max")*/ );
			mFrameTree->setColumnHidden( 6, mCurrentOutputs.size() <= 1 );
			mFrameTree->busyWidget()->stop();
			mFrameTask = 0;
			break;
		}
		case PARTIAL_FRAME_LIST:
		{
			JobTaskList jtl = ((PartialFrameListTask*)evt)->mReturn;
			LOG_3("got partial frame list back:"+QString::number(jtl.size()));
			FrameItem::CurTime = ((PartialFrameListTask*)evt)->mCurTime;
			mFrameTree->model()->updated( jtl );
			mTabToolBar->slotPause();
			mFrameTree->busyWidget()->stop();
			mPartialFrameTask = 0;
			break;
		}
		case ERROR_LIST:
		{
			JobErrorList jer = ((ErrorListTask*)evt)->mReturn;
			//((ErrorModel*)mErrorTree->model())->updateRecords( jer );
			mErrorTree->busyWidget()->stop();
			mErrorTree->model()->updateRecords(jer);
			break;
		}
		case STATIC_JOB_LIST_DATA:
		{
			mStaticDataRetrieved = true;
			StaticJobListDataTask * sdt = (StaticJobListDataTask*)evt;

			// Only the first StaticJobListDataTask actual does the select and fills in these static structures
			// any subsequent ones just ensure that the data is retreived before doing the rest of this logic
			if( sdt->mHasData ) {
				mSharedData->mJobTypeList = sdt->mJobTypes;
				mSharedData->mProjectList = sdt->mProjects;
				mSharedData->mServiceList = sdt->mServices;
			}

			// Default to showing all of the services and job types
			IniConfig ini(viewConfig());
			
			if( mJobFilter.typesToShow.isEmpty() )
				// For now only filter by primary job types, not sub-jobtypes
				mJobFilter.typesToShow = mSharedData->mJobTypeList.filter( "fkeyparentjobtype", QVariant(), false ).keys();
			else {
				QList<uint> verifiedKeyList = verifyKeyList( mJobFilter.typesToShow, JobType::table() );
				JobTypeList toShow = JobType::table()->records( verifiedKeyList );
				if( ini.keys().contains( "TypesHidden" ) ) {
					JobTypeList hidden = JobType::table()->records( ini.readUIntList( "TypesHidden" ) );
					JobTypeList newTypes = mSharedData->mJobTypeList - hidden - toShow;
					if( newTypes.size() && toShow.size() > 1 )
						toShow += newTypes;
				} else {
					QStringList toShowNames = toShow.names();
					if( toShowNames.contains( "Max2009" ) && !toShowNames.contains( "Max2010" ) )
						toShow += JobType( "Max2010" );
				}
				mJobFilter.typesToShow = toShow.unique().keys();
			}
			
			if( mJobFilter.allServicesShown )
				mJobFilter.servicesToShow = mSharedData->mServiceList.keys();
			else
				mJobFilter.servicesToShow = verifyKeyList( mJobFilter.servicesToShow, Service::table() );
				
			if( mJobFilter.hiddenProjects.size() ) {
				mJobFilter.visibleProjects = mSharedData->mProjectList.keys();
				foreach( uint hidden, mJobFilter.hiddenProjects )
					mJobFilter.visibleProjects.removeAll( hidden );
				mJobFilter.hiddenProjects.clear();
			}

			// If we haven't retrieved the static data, then mJobTaskRunning indicates
			// that we need to refresh the job list.
			if( mJobTaskRunning ) {
				mJobTaskRunning = false;
				refresh();
			}
			break;
		}
		case JOB_HISTORY_LIST:
		{
			mHistoryView->setHistory( ((JobHistoryListTask*)evt)->mReturn );
			mHistoryView->busyWidget()->stop();
		}
		default:
			break;
	}
}

QToolBar * JobListWidget::toolBar( QMainWindow * mw )
{
	if( !mToolBar ) {
		IniConfig cfg(viewConfig());
		initializeViews(cfg);
		mToolBar = new QToolBar( mw );
		mToolBar->addAction( RefreshAction );
		mToolBar->addAction( FrameCyclerAction );
		//mToolBar->addAction( VNCHostsAction );
		mToolBar->addSeparator();
		mToolBar->addAction( ResumeAction );
		mToolBar->addAction( PauseAction );
		mToolBar->addAction( KillAction );
		mToolBar->addAction( RestartAction );
		mToolBar->addSeparator();
		mToolBar->addAction( ShowMineAction );
		mToolBar->addSeparator();
		mToolBar->addWidget( mJobFilterEdit );
	}
	return mToolBar;
}

void JobListWidget::populateViewMenu( QMenu * viewMenu )
{
	IniConfig cfg(viewConfig());
	initializeViews(cfg);
	viewMenu->addAction( DependencyTreeEnabledAction );
	viewMenu->addAction( NewViewFromSelectionAction );
	viewMenu->addSeparator();
	viewMenu->addAction( ShowMineAction );
	{
		QMenu * filterMenu = viewMenu->addMenu( "Job Filters" );
		filterMenu->addMenu( mProjectFilterMenu );
		filterMenu->addMenu( mStatusFilterMenu );
		filterMenu->addMenu( mJobTypeFilterMenu );
		filterMenu->addMenu( mJobServiceFilterMenu );
	}
}

void JobListWidget::filtersChanged()
{
	refresh();
	saveToFile();
}

void JobListWidget::setLimit()
{
	bool ok;
	int limit = QInputDialog::getInt( this, "Set Job Limit", "Enter Maximum number of jobs to display", options.mLimit, 1, 100000, 1, &ok );
	if( ok ) {
		options.mLimit = limit;
		applyOptions();
		refresh();
	}
}

void JobListWidget::applyOptions()
{
	if( mViewsInitialized ) {
		mJobFilter.mLimit = options.mLimit;
		mJobTree->setFont( options.jobFont );
		mJobTree->setDragEnabled( !options.mControlModifierDepDragCheck );
		mFrameTree->setFont( options.frameFont );
		mErrorTree->setFont( options.frameFont );
		mSummaryTab->setFont( options.summaryFont );
		options.mJobColors->apply(mJobTree);
		options.mFrameColors->apply(mFrameTree);
		options.mErrorColors->apply(mErrorTree);

		ColorOption * co = options.mJobColors->getColorOption("default");
		if (co) {
			QPalette p = mJobTree->palette();
			p.setColor(QPalette::Active, QPalette::AlternateBase, co->bg.darker(120));
			p.setColor(QPalette::Inactive, QPalette::AlternateBase, co->bg.darker(120));
			mJobTree->setPalette( p );
		}

		mHistoryView->applyOptions();
		mJobTree->update();
		mFrameTree->update();
		mErrorTree->update();
	}
}

void JobListWidget::setJobFilter( const JobFilter & jf )
{
	mJobFilter = jf;
}

void JobListWidget::setHiddenProjects( ProjectList hiddenProjects )
{
	mJobFilter.hiddenProjects = hiddenProjects.keys();
}

void JobListWidget::setElementList( ElementList el )
{
	mJobFilter.elementList = el;
}

void JobListWidget::setStatusToShow( QStringList statii )
{
	mJobFilter.statusToShow = statii;
}

void JobListWidget::doRefresh()
{
	AssfreezerView::doRefresh();
	bool needJobListTask = false;
	bool needStatusBarMsg = false;

	if( !mStaticDataRetrieved )
		mJobTaskRunning = needStatusBarMsg = true;
	else if( mJobTaskRunning )
		mQueuedJobRefresh = true;
	else
		needJobListTask = needStatusBarMsg = true;

	if( needStatusBarMsg )
		setStatusBarMessage( "Refreshing Job List..." );

	if( needJobListTask ) {
		mJobTaskRunning = true;
		LOG_5( "Statuses to show: "	+ mJobFilter.statusToShow.join(",") );
		LOG_5( "Types to show: " + JobTypeList(JobType::table()->records(mJobFilter.typesToShow)).names().join(",") );
		mJobTree->busyWidget()->start();
		FreezerCore::addTask( new JobListTask( this, mJobFilter, mJobList, activeProjects(), activeServices(), !mJobTree->isColumnHidden(19) /*Service column*/, isDependencyTreeEnabled()) );
		FreezerCore::wakeup();
		// Refresh frame or error list too
		currentJobChanged();
	}
}

void JobListWidget::jobTreeColumnVisibilityChanged( int column, bool visible )
{
	// Services column
	if( column == 19 && visible )
		refresh();
}

void JobListWidget::setJobList( JobList jobList )
{
	mJobList = jobList;
	refresh();
}

void JobListWidget::refreshErrorList()
{
	mErrorTree->busyWidget()->start();
	FreezerCore::addTask( new ErrorListTask( this, mCurrentJob, mShowClearedErrors ) );
	FreezerCore::wakeup();
}

void JobListWidget::currentJobChanged()
{
	bool jobChange = mCurrentJob != mJobTree->current();
	mCurrentJob = mJobTree->current();
	LOG_5( "JobListWidget::currentJobChanged: " + QString::number( mCurrentJob.key() ) );

	QWidget * curTab = mJobTabWidget->currentWidget();
	if( curTab==mFrameTab )
		refreshFrameList(jobChange);
	else if( curTab == mErrorsTab )
		refreshErrorList();
}

void JobListWidget::refreshFrameList( bool jobChange )
{
	LOG_3(QString("refreshFrameList: jobChange: %1").arg(jobChange));
	// Cancel the current task if there is one
	if( mFrameTask )
		mFrameTask->mCancel = true;
	
	if( jobChange )
		mFrameTree->model()->setRootList( RecordList() );

	if( mCurrentJob.isRecord() ) {
		if( jobChange || mCurrentJob.jobStatus().tasksCount() < 2000 ) {
			mFrameTask = new FrameListTask( this, mCurrentJob );
			mFrameTree->busyWidget()->start();
			FreezerCore::addTask( mFrameTask );
			FreezerCore::wakeup();
		} else {
			// only update visible items
			QModelIndex start = mFrameTree->indexAt(QPoint(1,1));
			QModelIndex end = mFrameTree->indexAt(QPoint( 1, mFrameTree->viewport()->height()+1 ));
			JobTaskList jtl;
			for(ModelIter it(start); *it != end; ++it)
				jtl += FrameTranslator::getRecordStatic(*it);
			mPartialFrameTask = new PartialFrameListTask( this, jtl );
			mFrameTree->busyWidget()->start();
			FreezerCore::addTask( mPartialFrameTask );
			FreezerCore::wakeup();
		}
	}
}

void JobListWidget::jobListSelectionChanged()
{
	JobList selection = mJobTree->selection();
	if( selection.size() == 1 ) {
		setStatusBarMessage( selection[0].name() + " Selected" );
	} else if( selection.size() )
		setStatusBarMessage( QString::number( selection.size() ) + " Jobs Selected" );
	else
		clearStatusBar();
	
	mSelectedJobs = selection;
	LOG_5( "Selection is " + mSelectedJobs.keyString() + " -- Current is " + QString::number( mCurrentJob.key() ) );

	QWidget * curTab = mJobTabWidget->currentWidget();
	if( curTab==mSummaryTab ) {
		mJobSettingsWidget->setSelectedJobs( selection );
	} else if( curTab == mHistoryTab ) {
		// Clear the view
		mHistoryView->setHistory( JobHistoryList() );
		mHistoryView->busyWidget()->start();
		FreezerCore::addTask( new JobHistoryListTask( this, selection ) );
		FreezerCore::wakeup();
	}
	
	int cnt = mSelectedJobs.size();
	QMap<QString,RecordList> byStatus = mSelectedJobs.groupedBy( "status" );
	int newCnt = byStatus["submit"].size() + byStatus["verify"].size();
	int activeCnt = byStatus["ready"].size() + byStatus["started"].size();
	int doneCnt = byStatus["done"].size();
	int suspCnt = byStatus["suspended"].size();
	int holdCnt = byStatus["holding"].size();
	int archivedCnt = byStatus["archived"].size();
	
	// Allow resume if all selected jobs are suspended
	ResumeAction->setEnabled( cnt && (suspCnt == cnt) );
	// Allow pause if all selected jobs are active( ready/started )
	PauseAction->setEnabled( cnt && (activeCnt + holdCnt + suspCnt == cnt) );
	// Allow Remove if all selected jobs are active, done, or suspended
	bool allowDelete = cnt && (archivedCnt + activeCnt + doneCnt + suspCnt + newCnt + holdCnt == cnt);
	// Permission check.  Only allow deleting a user's own jobs unless they have permission to modify jobs
	if( allowDelete )
		allowDelete &= (mSelectedJobs.users().unique() == RecordList(User::currentUser()) || User::hasPerms( "Job", true ));
	KillAction->setEnabled( allowDelete );
	// Allow Restart if all selected jobs are active, done, or suspended
	RestartAction->setEnabled( cnt && (activeCnt + doneCnt + suspCnt == cnt) );
}

void JobListWidget::refreshCurrentTab()
{
	QWidget * curTab = mJobTabWidget->currentWidget();
	if( curTab==mSummaryTab || curTab == mHistoryTab )
		jobListSelectionChanged();
	else if( curTab==mFrameTab )
		refreshFrameList(true);
	else
		currentJobChanged();
}

void JobListWidget::currentTabChanged(bool refresh)
{
	QWidget * curTab = mJobTabWidget->currentWidget();

	// This hack allows the bottom part of the splitter to be smaller than
	// the SummaryTab's minimum height, when the SummaryTab is not shown
	if( curTab == mSummaryTab )
		mJobTabWidget->setSizePolicy( QSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding ) );
	else
		mJobTabWidget->setSizePolicy( QSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored ) );

	if( curTab==mFrameTab )
		mTabToolBar->show();
	else
		mTabToolBar->hide();

	if( refresh )
		refreshCurrentTab();
}

void JobListWidget::clearErrors()
{
	JobList jobs = mJobTree->selection();
	if( jobs.size() ) {
		QString history = " Errors cleared";
		QSqlQuery q = Database::current()->exec("SELECT fkeyjob, sum(count) FROM JobError WHERE (cleared=false OR cleared IS NULL) AND fkeyjob IN (" + jobs.keyString() + ") GROUP BY fkeyjob");
	
		while( q.next() ) {
			Job j( q.value(0).toInt() );
			j.addHistory( QString::number(q.value(1).toInt()) + history );
		}
		Database::current()->exec("UPDATE JobError SET cleared=true WHERE fkeyJob IN(" + jobs.keyString() + ")");
	}
}

void JobListWidget::restartJobs()
{
	if( !(qApp->keyboardModifiers() & (Qt::ControlModifier|Qt::AltModifier)) && (QMessageBox::warning( this, "Restart Job Confirmation", "Are you sure that you want to restart the selected jobs?\n  All tasks will be set to 'new'.", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) )
		!= QMessageBox::Yes )
		return;
	Job::restartJobs( mJobTree->selection() );
}

void JobListWidget::resumeJobs()
{
	Job::resumeJobs( mJobTree->selection() );
	refresh();
}

void JobListWidget::suspendJobs()
{
	Job::suspendJobs( mJobTree->selection() );
	refresh();
}

void JobListWidget::deleteJobs()
{
	bool archive = false;
	JobList jl = mJobTree->selection();
	JobTypeList jobTypes = JobList(mJobTree->selection()).jobTypes().unique();
	JobType jt;
	if( jobTypes.size() == 1 )
		jt = jobTypes[0];
	while( jt.parentJobType().isRecord() )
		jt = jt.parentJobType();
	// Give Archive option for batch jobs
	if( jt.isRecord() && jt.name() == "Batch" ) {
		QMessageBox * mb = new QMessageBox(this);
		mb->setWindowTitle( "Archive or Delete Job Confirmation" );
		mb->setText( "You have the option to Archive or Delete the selected Batch Jobs.\n  Archived jobs will remain viewable, including their tasks, errors, and history." );
		mb->setDefaultButton( mb->addButton( "Archive", QMessageBox::AcceptRole ) );
		mb->addButton( "Delete", QMessageBox::DestructiveRole );
		mb->addButton( "Cancel", QMessageBox::RejectRole );
		mb->exec();
		int role =  mb->buttonRole(mb->clickedButton());
		delete mb;
		if( role == QMessageBox::RejectRole )
			return;
		if( role == QMessageBox::AcceptRole )
			archive = true;
	} else if( !(qApp->keyboardModifiers() & (Qt::ControlModifier|Qt::AltModifier)) && (QMessageBox::warning( this, "Delete Job Confirmation", "Are you sure that you want to delete the selected jobs?\n  Once deleted a job cannot be restarted.", QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes) )
		!= QMessageBox::Yes )
		return;
	
	JobList holdingJobs;
	JobDepList depsToDelete;
	foreach( Job j, jl ) {
		depsToDelete = JobDep::recordsByDep(j);
		foreach( Job dependent, depsToDelete.jobs() ) {
			if( dependent.status() == "holding" ) {
				bool isOnlyDep = true;
				foreach( Job dependency, dependent.jobDeps().jobs() ) {
					if( dependency == j || dependency.status() == "done" ) continue;
					isOnlyDep = false;
					break;
				}
				if( isOnlyDep )
					holdingJobs += dependent;
			}
		}
	}

	if( holdingJobs.size() ) {
		QMessageBox * mb = new QMessageBox(this);
		mb->setWindowTitle( "Start or Suspend Dependent Jobs!" );
		mb->setText( "You have the option to Start or Suspend the jobs that are dependents of those you are deleting." );
		mb->setDefaultButton( mb->addButton( "Start", QMessageBox::AcceptRole ) );
		mb->addButton( "Suspend", QMessageBox::DestructiveRole );
		mb->addButton( "Cancel Job Deletion", QMessageBox::RejectRole );
		mb->exec();
		int role =  mb->buttonRole(mb->clickedButton());
		delete mb;
		if( role == QMessageBox::RejectRole )
			return;
		depsToDelete.remove();
		if( role == QMessageBox::AcceptRole )
			Job::resumeJobs( holdingJobs );
		else
			Job::suspendJobs( holdingJobs );
	} else
		depsToDelete.remove();

	if( archive )
		Job::archiveJobs(jl);
	else
		Job::deleteJobs(jl);

	refresh();
}

void JobListWidget::showMine(bool sm)
{
	User u = User::currentUser();

	/* Edit the mJobFilter */
	mJobFilter.userList.clear();
	if( sm && u.isRecord() )
		mJobFilter.userList += u.key();

	/* Refresh the list */
	refresh();
}

void JobListWidget::jobFilterChanged( const Expression & jobFilter )
{
	mJobFilter.mExtraFilters = jobFilter;
	mJobList = mJobFilterEdit->mJobList;
	refresh();
}

void JobListWidget::frameSelected( const Record & frameRecord )
{
	JobTask frame(frameRecord);
	if( frame.isRecord() ) {
		int frameNumber = frame.frameNumber();
		if( frame.jobOutput().name() != mTabToolBar->currentOutput() )
			mTabToolBar->setCurrentOutput( frame.jobOutput().name() );
		mImageView->setImageNumber( frameNumber );
	}
}

void JobListWidget::currentOutputChanged(const QString & output)
{
	JobOutputList nameMatch = mCurrentOutputs.filter("name",output);
	if( nameMatch.size() >= 1 ) {
		JobOutput cur = nameMatch[0];
		LOG_5( "Setting image view frame path to: " + cur.fileTracker().filePath() );
		mImageView->setFrameRange( cur.fileTracker().filePath(), mMinFrame, mMaxFrame, true /*!mCurrentJob.jobType().name().startsWith("Max")*/ );
	}
}

void JobListWidget::frameListSelectionChanged()
{
	JobTaskList jtl = mFrameTree->selection();
	if( jtl.size() == 1 ) {
		int num = jtl[0].frameNumber();
		setStatusBarMessage( mCurrentJob.name() + " Frame " + QString::number( num ) );
	} else if( jtl.size() > 1 ) {
		setStatusBarMessage( mCurrentJob.name() + " " + QString::number( jtl.size() ) + " Frames Selected" );
	} else
		clearStatusBar();
}

void JobListWidget::frameCycler()
{
	if( mCurrentJob.isRecord() ) {
		// We should know (per jobtype?) whether the output path is a frame sequence or not
		// Could check too since we can stat the files if frame cycler can
		JobTask cur = mFrameTree->current();
		JobOutput output = cur.jobOutput();
		QString path = output.isRecord() ? output.fileTracker().filePath() : mCurrentJob.outputPath();
		int frameNumber = cur.isRecord() ? cur.frameNumber() : mCurrentJob.minTaskNumber();
		if( frameNumber >= 0 )
			path = makeFramePath(path, frameNumber, 4, !mCurrentJob.jobType().name().startsWith("Max") );
		else
			path.replace(QRegExp("[^\\\\/]*$"), "");
		LOG_5( "ImageView::showFrameCycler: " + path );
		openFrameCycler( path );
	}
}

void JobListWidget::pdPlayer()
{
	if( mCurrentJob.isRecord() ) {
		JobTask cur = mFrameTree->current();
		int frame = cur.isRecord() && cur.job() == mCurrentJob ? cur.frameNumber() : mCurrentJob.minTaskNumber();
		JobOutput output = cur.jobOutput();
		QString imagePath = makeFramePath( output.isRecord() ? output.fileTracker().filePath() : mCurrentJob.outputPath(), frame, 4, true /*!mCurrentJob.jobType().name().startsWith("Max")*/ );
		QStringList args;
		args << imagePath;
		if (imagePath.endsWith(".exr",Qt::CaseInsensitive))
			args << "--color_space=linear";
		else
			args << "--color_space=srgb";
		args << "--alpha=pm";
		IniConfig cfg(userConfig());
		cfg.pushSection( "Assfreezer_Preferences" ); // Should be changed
		QString path;
		path = cfg.readString( "PdPlayer64Path", "C:/Program Files/Pdplayer 64/Pdplayer64.exe" );
		// TODO: Need to use this for quicktimes
		//path = cfg.readString( "PdPlayerPath" );
		cfg.popSection();
		QProcess::startDetached( path, args );
	}
}


void JobListWidget::outputPathExplorer()
{
	JobTask cur = mFrameTree->current();
	if( cur.isRecord() ){
		mImageView->showOutputPath(cur.frameNumber());
	} else {
		Job j = mJobTree->current();
		if( j.isRecord() )
			exploreFile( j.outputPath() );
	}
}

void JobListWidget::exploreJobFile()
{
	if( mCurrentJob.isRecord() ) {
		QString fileName = mCurrentJob.fileName().replace( "/", "\\" ).replace( "N:", "\\\\stryfe\\assburner" );
		exploreFile( fileName );
	}
}

void JobListWidget::changeFrameSelection(int /*frameNumber*/)
{
	/*QTreeWidgetItem * item = mFrameTree->firstChild();
	while(item){
		if( item->isSelected() || (item->text(0).toInt()==frameNumber) )
			FrameListView->setSelected(item, item->text(0).toInt()==frameNumber);
		item = item->nextSibling();
	}*/
}

void JobListWidget::showJobPopup( const QPoint & point )
{
	if( !mJobMenu ) mJobMenu = new AssfreezerJobMenu(this);
	mJobMenu->popup( mJobTree->mapToGlobal( point ) );
}

void JobListWidget::saveCannedBatchJob()
{
	JobList jobs = mJobTree->selection();
	if( jobs.size() == 1 ) {
		JobBatch jb(jobs[0]);
		BatchSubmitDialog * bsd = new BatchSubmitDialog( this );
		bsd->setSaveCannedBatchMode( true );
		bsd->setName( jb.name() );
		bsd->setCommand( jb.cmd() );
		bsd->exec();
		delete bsd;
	}
}

void JobListWidget::clearJobList()
{
	setJobList(JobList());
}

void JobListWidget::showJobInfo()
{
	bool canEdit = User::hasPerms( "Job", true ) || (mCurrentJob.user() == User::currentUser());
	if( mCurrentJob.getValue( "password" ).isNull() || canEdit )
		restorePopup( RecordPropValTree::showRecords( mCurrentJob, this, canEdit ) );
}

void JobListWidget::showJobStatistics()
{
	JobStatWidget * jsw = new JobStatWidget(0);
	jsw->setJobs( mJobTree->selection() );
	jsw->show();
	restorePopup(jsw);
}

void JobListWidget::setJobPriority()
{
	int total = 0, count = 0;
	JobList jl = mJobTree->selection();
	foreach( Job j, jl ) {
		count++;
		total += j.priority();
	}
	bool ok;
	int priority = QInputDialog::getInt(this,"Set Job(s) Priority","Set Job(s) Priority", total / count, 1, 99, 1, &ok);
	if( ok ) {
		Database::current()->beginTransaction();
		jl.setPriorities( priority );
		jl.commit();
		Database::current()->commitTransaction();
	}
}

void JobListWidget::showFramePopup(const QPoint & point)
{
	if( !mTaskMenu ) mTaskMenu = new AssfreezerTaskMenu(this);
	mTaskMenu->popup( mErrorTree->mapToGlobal(point) );
}

void JobListWidget::showErrorPopup(const QPoint & point)
{
	if( !mErrorMenu )
		mErrorMenu = new AssfreezerErrorMenu( this, mErrorTree->selection(), mErrorTree->model()->rootList() );
	else
		mErrorMenu->setErrors( mErrorTree->selection(), mErrorTree->model()->rootList() );
	mErrorMenu->popup( mErrorTree->mapToGlobal(point) );
}

ImageView * JobListWidget::imageView() const
{
	return mImageView;
}

void JobListWidget::setFrameCacheStatus(int fn, int status)
{
	LOG_3("setFrameCacheStatus: fn: "+QString::number(fn) + " status:"+QString::number(status));
	QModelIndex idx = ModelIter(mFrameTree->model()).findFirst(0,fn);
	if( idx.isValid() )
		FrameTranslator::data(idx).loadedStatus = status;
}

void JobListWidget::createNewViewFromSelection()
{
	JobList sel = mJobTree->selection();
	if( sel.isEmpty() ) return;
	MainWindow * mw = qobject_cast<MainWindow*>(window());
	if( mw ) {
		JobListWidget * jobListView = new JobListWidget(mw);
		jobListView->setJobList( sel );
		mw->insertView( jobListView );
		mw->setCurrentView( jobListView );
	}
}

void JobListWidget::populateErrorTreeHeaderMenu( QMenu * menu )
{
	menu->addSeparator();
	QAction * showClearedErrorsAction = menu->addAction( "Show Cleared Errors" );
	showClearedErrorsAction->setCheckable( true );
	showClearedErrorsAction->setChecked( mShowClearedErrors );
	connect( showClearedErrorsAction, SIGNAL( toggled( bool ) ), SLOT( setShowClearedErrors( bool ) ) );
}

void JobListWidget::setShowClearedErrors( bool showClearedErrors )
{
	if( showClearedErrors != mShowClearedErrors ) {
		mShowClearedErrors = showClearedErrors;
		if( mJobTabWidget->currentWidget() == mErrorsTab )
			refreshErrorList();
	}
}

