
/* $Author$
 * $LastChangedDate$
 * $Rev$
 * $HeadURL$
 */

#include <qevent.h>

#include "freezercore.h"

#include "host.h"
#include "hostinterface.h"
#include "hoststatus.h"
#include "job.h"
#include "jobassignment.h"
#include "joberror.h"
#include "jobhistory.h"
#include "jobtask.h"
#include "jobtaskassignment.h"
#include "jobtype.h"
#include "jobdep.h"
#include "jobservice.h"
#include "project.h"
#include "service.h"

#include "afcommon.h"

enum {
	JOB_LIST = QEvent::User+1,
	HOST_LIST,
	FRAME_LIST,
	PARTIAL_FRAME_LIST,
	ERROR_LIST,
	COUNTER,
	STATIC_JOB_LIST_DATA,
	STATIC_HOST_LIST_DATA,
	HOST_ERROR_LIST,
	JOB_HISTORY_LIST
};

class JobListTask : public ThreadTask
{
public:
	JobListTask( QObject * rec, const JobFilter & jf, const JobList & jobList, ProjectList, ServiceList activeServices, bool fetchJobServices = false, bool needDeps = false );
	void run();
	
	JobList mReturn;
	JobDepList mJobDeps;
	JobFilter mJobFilter;
	JobList mJobList, mJobsNeedingDeps, mDependentJobs;
	ProjectList mProjects;
	bool mFetchJobServices, mFetchJobDeps;
	ServiceList mActiveServices;
	JobServiceList mJobServices;
};


class HostListTask : public ThreadTask
{
public:
	HostListTask( QObject * rec, ServiceList serviceFilter, ServiceList activeServices, const Expression & extraFilters, bool onlineOnly, bool loadHostServices, bool loadHostInterfaces );
	
	void run();
	
	ServiceList mServiceFilter, mActiveServices;
	Expression mExtraFilters;
	HostList mReturn;
	HostStatusList mHostStatuses;
	JobAssignmentList mJobAssignments;
	JobTaskAssignmentList mJobTaskAssignments;
	JobList mJobs;
	HostInterfaceList mHostInterfaces;
	ProjectList mProjects; // Linked to active jobs
	UserList mUsers; // Employee table should be preloaded, but some hosts might be assigned to non-employee users
	bool mOnlineOnly;
	bool mLoadHostServices;
	bool mLoadHostInterfaces;
};

class FrameListTask : public ThreadTask
{
public:
	FrameListTask( QObject * rec, const Job & );
	void run();
	Job mJob;
	JobTaskList mReturn;
	JobTaskAssignmentList mTaskAssignments;
	JobAssignmentList mJobAssignments;
	FileTrackerList mFileTrackers;
	JobOutputList mOutputs;
	QDateTime mCurTime;
};

class PartialFrameListTask : public ThreadTask
{
public:
	PartialFrameListTask( QObject * rec, const JobTaskList & );
	void run();
	JobTaskList mJtl;
	JobTaskList mReturn;
	QDateTime mCurTime;
};

class ErrorListTask : public ThreadTask
{
public:
	// Used for the Job error list
	ErrorListTask( QObject * rec, const Job &, bool fetchClearedErrors = false );
	
	// Used for the Host error list
	ErrorListTask( QObject * rec, const Host &, int limit, bool showCleared );
	
	// Used for the Error View
	ErrorListTask( QObject * rec, JobList jobFilter, HostList hostFilter, ServiceList serviceFilter, JobTypeList jobTypeFilter, const QString & messageFilter, bool showServices, int limit, const Expression & extraFilters );
	
	void run();
	
	// Filters
	bool mFetchJobs, mFetchServices, mFetchClearedErrors;
	int mLimit;
	HostList mHostFilter;
	JobList mJobFilter;
	ServiceList mServiceFilter;
	JobTypeList mJobTypeFilter;
	QString mMessageFilter;
	Expression mExtraFilters;
	
	JobErrorList mReturn;
	JobList mJobs;
	JobServiceList mJobServices;
};


class StaticJobListDataTask : public ThreadTask
{
public:
	StaticJobListDataTask( QObject * rec );
	void run();
	JobTypeList mJobTypes;
	ProjectList mProjects;
	ServiceList mServices;
	bool mHasData;
};

class StaticHostListDataTask : public ThreadTask
{
public:
	StaticHostListDataTask( QObject * rec );
	void run();
	ServiceList mServices;
};

class ASSFREEZER_EXPORT CounterTask : public ThreadTask
{
public:
	CounterTask( QObject * rec );
	
	void run();
	CounterState mReturn;
	Service mManagerService;
};

class JobHistoryListTask : public ThreadTask
{
public:
	JobHistoryListTask( QObject * rec, JobList jobs );
	void run();
	JobList mJobs;
	JobHistoryList mReturn;
};

