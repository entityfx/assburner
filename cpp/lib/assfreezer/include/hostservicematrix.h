
#ifndef HOST_SERVICE_MATRIX_H
#define HOST_SERVICE_MATRIX_H

#include <qmainwindow.h>

#include "host.h"
#include "hostservice.h"
#include "hoststatus.h"
#include "service.h"

#include "recordsupermodel.h"
#include "recordtreeview.h"

#include "afcommon.h"

#include "ui_hostservicematrixwidgetui.h"

class HostServiceModel;

class ASSFREEZER_EXPORT HostServiceModel : public RecordSuperModel
{
Q_OBJECT
public:
	HostServiceModel( QObject * parent = 0 );

	HostService findHostService( const QModelIndex & idx );
	Service serviceByColumn( int column ) const;

	void setHostList( HostList hosts );
	void updateHosts( HostList hosts );
	void refreshIndexes( QModelIndexList indexes );
	
public slots:
	void updateServices();

	void hostServicesAdded(RecordList);
	void hostServicesRemoved(RecordList);
	void hostServiceUpdated(Record up, Record);

protected:
	HostStatusList mStatuses;
	HostServiceList mHostServices;
	QMap<Host,HostServiceList> mHostServicesByHost;
	ServiceList mServices;
};

class ASSFREEZER_EXPORT HostServiceMatrix : public RecordTreeView
{
Q_OBJECT
public:
	HostServiceMatrix( QWidget * parent = 0 );

	bool hostFilterCS() const { return mHostFilterCS; }
	bool serviceFilterCS() const { return mServiceFilterCS; }
	
public slots:
	void setHostFilter( const QString & );
	void setServiceFilter( const QString & );
	void setHostFilterCS( bool cs );
	void setServiceFilterCS( bool cs );
	
	void slotShowMenu( const QPoint & pos, const QModelIndex & underMouse );

	void updateServices();
protected:

	QString mHostFilter, mServiceFilter;
	bool mHostFilterCS, mServiceFilterCS;
	HostServiceModel * mModel;
};

class ASSFREEZER_EXPORT HostServiceMatrixWidget : public QWidget, public Ui::HostServiceMatrixWidgetUi
{
Q_OBJECT
public:
	HostServiceMatrixWidget( QWidget * parent = 0 );

public slots:
	void newService();

protected:
	virtual bool eventFilter( QObject * o, QEvent * e );
	
	HostServiceMatrix * mView;
};

class ASSFREEZER_EXPORT HostServiceMatrixWindow : public QMainWindow
{
Q_OBJECT
public:
	HostServiceMatrixWindow( QWidget * parent = 0 );
};

#endif // HOST_SERVICE_MATRIX_H
