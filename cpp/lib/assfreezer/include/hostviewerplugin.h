
/* $Author: brobison $
 * $LastChangedDate: 2010-02-03 15:24:32 -0800 (Wed, 03 Feb 2010) $
 * $Rev: 9301 $
 * $HeadURL: svn://svn.blur.com/blur/branches/concurrent_burn/cpp/lib/assfreezer/include/jobsettingswidgetplugin.h $
 */

#ifndef HOST_VIEWER_PLUGIN_H
#define HOST_VIEWER_PLUGIN_H

#include "host.h"

class QAction;
class HostList;

class HostViewerPlugin
{
public:
    HostViewerPlugin(){}
	virtual ~HostViewerPlugin(){}
    virtual QString name(){return QString();};
    virtual QString icon(){return QString();};
    virtual void view(const HostList &){};
    virtual bool enabled(const HostList &){return true;};
};

#endif // HOST_VIEWER_PLUGIN_H
