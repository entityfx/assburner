
import sys
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))

batch = StaticTarget( "qtwinmigrate_configure", path, "configure.bat -library" )
qmake = QMakeTarget("qtwinmigrate_qmake",path,"qtwinmigrate.pro")

Target( "qtwinmigrate", path, [batch,qmake] )
