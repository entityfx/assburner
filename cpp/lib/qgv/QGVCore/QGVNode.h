/***************************************************************
QGVCore
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/
#ifndef QGVNODE_H
#define QGVNODE_H

#include <qgv.h>
#include <QGraphicsItem>
#include <QPen>

class QGVEdge;
class QGVScene;
class QGVNodePrivate;
class QGVSubGraph;

/**
 * @brief Node item
 *
 */
class QGVCORE_EXPORT QGVNode : public QGraphicsItem
{
public:
	QGVNode(QGVScene *scene);
	QGVNode(QGVSubGraph *subgraph);

    ~QGVNode();

    QString label() const;
    void setLabel(const QString &label);

    QRectF boundingRect() const;
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
    void setAttribute(const QString &label, const QString &value);
    QString getAttribute(const QString &name) const;

    void setIcon(const QImage &icon);

	virtual QPen pen() const;
	virtual QBrush brush() const;
	virtual QColor fontColor() const;
	
    enum { Type = UserType + 2 };
    int type() const
    {
        return Type;
    }

protected:

private:
	QGVNode(QGVNodePrivate * priv, QGVScene * scene);
    friend class QGVScene;
    friend class QGVSubGraph;
    void updateLayout();

	
		// Not implemented in QGVNode.cpp
//		QPainterPath makeShape(Agnode_t* node) const;
//		QPolygonF makeShapeHelper(Agnode_t* node) const;

	QPainterPath _path;
	QPen _pen;
	QBrush _brush;
	QImage _icon;

	QGVScene *_scene;
	QGVNodePrivate* _node;
};


#endif // QGVNODE_H
