/***************************************************************
QGVCore
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/
#include "QGVSubGraph.h"
#include <QGVCore.h>
#include <QGVScene.h>
#include <QGVGraphPrivate.h>
#include <QGVNodePrivate.h>
#include <QGVNode.h>
#include <QDebug>
#include <QPainter>

QGVSubGraph::QGVSubGraph(QGVScene *scene, const QString & name, bool cluster): _scene(scene), _sgraph(0)
{
	_scene->addSubGraph(this,name,cluster);
    //setFlag(QGraphicsItem::ItemIsSelectable, true);
}

QGVSubGraph::QGVSubGraph(QGVSubGraph *parent, const QString & name, bool cluster): _scene(0), _sgraph(0)
{
	_scene = qobject_cast<QGVScene*>(parent->scene());
	_scene->addSubGraph(this,name,cluster,parent);
    //setFlag(QGraphicsItem::ItemIsSelectable, true);
}

QGVSubGraph::QGVSubGraph(QGVGraphPrivate * priv, QGVScene * scene) : _scene(scene), _sgraph(priv)
{}

QGVSubGraph::~QGVSubGraph()
{
    _scene->removeItem(this);
	agdelete(_scene->_graph->graph(),_sgraph->graph());
	delete _sgraph;
}

QString QGVSubGraph::name() const
{
		return QString::fromLocal8Bit(GD_label(_sgraph->graph())->text);
}

QGVNode *QGVSubGraph::addNode(const QString &label)
{
	QGVNode * ret = new QGVNode(this);
	ret->setLabel(label);
	return ret;
}

QGVSubGraph *QGVSubGraph::addSubGraph(const QString &name, bool cluster)
{
	return new QGVSubGraph(this, name, cluster);
}

QRectF QGVSubGraph::boundingRect() const
{
    return QRectF(0,0, _width, _height);
}

void QGVSubGraph::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
    painter->save();

    painter->setPen(_pen);
    painter->setBrush(_brush);

    painter->drawRect(boundingRect());
    painter->drawText(_label_rect, Qt::AlignCenter, _label);
    painter->restore();
}

void QGVSubGraph::setAttribute(const QString &name, const QString &value)
{
		agsafeset(_sgraph->graph(), name.toLocal8Bit().data(), value.toLocal8Bit().data(), "");
}

QString QGVSubGraph::getAttribute(const QString &name) const
{
		char* value = agget(_sgraph->graph(), name.toLocal8Bit().data());
    if(value)
        return value;
    return QString();
}

void QGVSubGraph::updateLayout()
{
    prepareGeometryChange();

    //SubGraph box
		boxf box = GD_bb(_sgraph->graph());
    pointf p1 = box.UR;
    pointf p2 = box.LL;
    _width = p1.x - p2.x;
    _height = p1.y - p2.y;

		qreal gheight = QGVCore::graphHeight(_scene->_graph->graph());
    setPos(p2.x, gheight - p1.y);

    _pen.setWidth(1);
    _brush.setStyle(QGVCore::toBrushStyle(getAttribute("style")));
    _brush.setColor(QGVCore::toColor(getAttribute("fillcolor")));
    _pen.setColor(QGVCore::toColor(getAttribute("color")));

    //SubGraph label
		textlabel_t *xlabel = GD_label(_sgraph->graph());
    if(xlabel)
    {
        _label = xlabel->text;
        _label_rect.setSize(QSize(xlabel->dimen.x, xlabel->dimen.y));
				_label_rect.moveCenter(QGVCore::toPoint(xlabel->pos, QGVCore::graphHeight(_scene->_graph->graph())) - pos());
    }
}
