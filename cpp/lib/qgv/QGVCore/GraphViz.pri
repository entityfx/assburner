
#GraphViz librairie
DEFINES += WITH_CGRAPH
INCLUDEPATH += private
QMAKE_CXXFLAGS += -DQGVCORE_LIB

unix:!macx {
# CONFIG += link_pkgconfig
# PKGCONFIG += libcdt libgvc libcgraph libgraph
	INCLUDEPATH+=/usr/include/graphviz
	LIBS+=-lcgraph -lgvc
}
macx {
	INCLUDEPATH+=/usr/local/include/graphviz
	LIBS+=-lcgraph -lgvc -L/usr/local/lib/graphviz
}
win32 {
 #Configure Windows GraphViz path here :
 GRAPHVIZ_PATH = "C:/Program Files (x86)/Graphviz2.38/"
 DEFINES += WIN32_DLL
 DEFINES += GVDLL
 INCLUDEPATH += $$GRAPHVIZ_PATH/include/graphviz
 LIBS += -L$$GRAPHVIZ_PATH/lib/release/lib -lgvc -lcgraph -lgraph -lcdt
}
