
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))
qgv = QMakeTarget( 'qgv', path, 'QGraphViz.pro' )

rpm = RPMTarget('qgvrpm','libqgraphviz',path,'../../../rpm/spec/qgv.spec.template','1.0')
rpm.pre_deps = ["blurpythonbuildrpm"]

if __name__ == "__main__":
	build()
