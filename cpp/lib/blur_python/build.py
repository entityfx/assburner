
import os
from blur.build import *
from distutils import sysconfig

path = os.path.dirname(os.path.abspath(__file__))
rev_path = os.path.join(path,'..')

# Replace revision numbers in the nsi template
svnnsi = WCRevTarget("blur_python_modulessvnrevnsi",path,"../..","blur_python-svnrev-template.nsi","blur_python-svnrev.nsi")
svntxt = WCRevTarget("blur_python_installersvnrevtxt",path,rev_path,"blur_python_version_template.txt","blur_python_version.txt")

# Create the nsi installer
makensis_extra_options = ['/DPYTHON_PATH=%s' % sysconfig.get_config_vars()['prefix'], '/DPYTHON_VERSION=%s' % sysconfig.get_python_version() ]
blur_python_installer = NSISTarget( "blur_python_installer", path, "blur_python.nsi", [svnnsi,svntxt], makensis_extra_options )

blur_python_deps = ["pystone","pyclasses","pystonegui","pyclassesui","pyabsubmit","pyassfreezer"]
if os.path.exists(os.path.join(path,'../ctrax')):
	blur_python_deps += ["ctrax", "pyctrax"]
Target( "blur_python", path, blur_python_deps)

