

SOURCES+=test.cpp

include(../../../qmake_shared/common.pri)

INCLUDEPATH+=../include
LIBS+=-L.. -lstone
TARGET=tests
CONFIG=qt thread warn_on debug qtestlib
QT+=xml network sql gui
DESTDIR=./
