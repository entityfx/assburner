
#ifndef CHANGE_SET_WEAK_REF_H
#define CHANGE_SET_WEAK_REF_H

namespace Stone {

class ChangeSet;
class ChangeSetPrivate;

class STONE_EXPORT ChangeSetWeakRef
{
public:
	ChangeSetWeakRef();
	ChangeSetWeakRef(const ChangeSet & cs);
	ChangeSetWeakRef(const ChangeSetWeakRef & cs);
	~ChangeSetWeakRef();
	
	bool isValid() const { return p!=0; }
	
	void operator=( const ChangeSet & cs );
	void operator=( const ChangeSetWeakRef & cswr );
	
	bool operator==( const ChangeSet & other ) const;
	bool operator==( const ChangeSetWeakRef & other ) const;
	
	bool operator!=( const ChangeSet & other ) const;
	bool operator!=( const ChangeSetWeakRef & other ) const;
	
	ChangeSet operator()() const;
	operator ChangeSet() const;
	
	QString pointerString() { return QString::number( (qulonglong)p, 16 ); }
private:
	ChangeSetPrivate * p;
	ChangeSetWeakRef * next;
	friend class ChangeSetPrivate;
};

} // namespace Stone


#endif // CHANGE_SET_WEAK_REF_H

