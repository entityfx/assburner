
/*
 *
 * Copyright 2010 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id: pgconnection.h 6738 2008-06-26 02:05:21Z newellm $
 */

#ifndef SQLITE_CONNECTION_H
#define SQLITE_CONNECTION_H

#include "connection.h"

class STONE_EXPORT SqliteConnection : public QSqlDbConnection
{
public:
	SqliteConnection();
	
	virtual Capabilities capabilities() const;

	//
	// Db table verification and creation
	//
	virtual bool tableExists( TableSchema * schema );
	/// Verify that the table and needed columns exist in the database
	virtual bool verifyTable( TableSchema * schema, bool createMissingColumns = false, QString * output=0 );
	/// Trys to create the table in the database
	virtual bool createTable( TableSchema * schema, QString * output = 0 );

	virtual TableSchema * importTableSchema(){return 0;}

	virtual Schema * importDatabaseSchema(){return 0;}

	virtual uint newPrimaryKey( TableSchema * );

	virtual QString prepareColumnLiteral( const QString & cl );

	virtual QVariant prepareValue( const QVariant & v );
	//virtual QString prepareValueString( const QVariant & v );

	/// Same as above, but won't select from offspring
	virtual RecordList selectOnly( Table *, const QString & where = QString::null, const QList<QVariant> & vars = QList<QVariant>() );

	virtual QList<RecordList> joinedSelect( const JoinedSelect &, QString where, QList<QVariant> vars );

	virtual void selectFields( Table * table, RecordList records, FieldList fields );

	virtual int remove( Table *, const QString &, QList<int> * );

	virtual bool createIndex( IndexSchema * schema );

	virtual QSqlQuery fakePrepare( const QString & sql );

	virtual QString lastInsertPrimaryKey( TableSchema * tableSchema );
protected:
	QString generateSelectSql( TableSchema * schema );

	QHash<TableSchema*,QString> mSqlFields;
};

#endif // SQLLITE_CONNECTION_H

