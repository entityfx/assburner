
#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <assert.h>

#include <qlist.h>

#include "blurqt.h"
#include "field.h"
#include "interval.h"
#include "index.h"

#ifdef _HAVE_INIT_LIST
//#include <initializer_list>
#endif

namespace Stone {
// Proposed usage:
//.select( Delivery.Project == 1 && Delivery.DateStart >= dateStart && Delivery.DateStart <= dateEnd )

class Connection;
class Record;
class RecordList;
class Table;
struct RecordReturn;
typedef QList<Table*> TableList;
typedef QMap<QString,QVariant> QStringQVariantMap;

class ExpressionList;
class ExpressionPrivate;
class TableExpression;

/**
 *  This class represents the future result of an expression that will
 *  be executed in another thread.
 * 
 *  This class releases all references to the original expression after
 *  it has been run.
 */
class STONE_EXPORT ExpressionFuture
{
public:
	inline ExpressionFuture() : p(nullptr) {}
	inline ExpressionFuture( const ExpressionFuture & );
	inline explicit ExpressionFuture( ExpressionPrivate * );
#ifdef Q_COMPILER_RVALUE_REFS
	inline ExpressionFuture( ExpressionFuture && );
	inline ExpressionFuture & operator=(ExpressionFuture &&) noexcept;
#endif
	inline ~ExpressionFuture();
	
	bool isNull() { return p == nullptr; }
	
	RecordList get();
	bool valid() const;
	void wait();
	
	// When the future completes(valid=true), the slot is called
	// on the qobject.  The signal is declared as:
	// void then( ExpressionFuture future );
	// The ExpressionFuture arg is optional for the slot passed in
	ExpressionFuture & then(QObject * object, const char * slotName);
	
protected:
	ExpressionPrivate * p;
	friend class Expression;
};

class STONE_EXPORT Expression
{
public:
	inline Expression();
	inline Expression( const Expression & );
#ifdef Q_COMPILER_RVALUE_REFS
	inline Expression( Expression && ) noexcept;
#endif
	Expression( const QVariant & );
	Expression( const Record & );
	Expression( Table * table, bool only = false );

	inline explicit Expression( ExpressionPrivate * );
	
	inline ~Expression();

	Expression & operator=( const Expression & );
#ifdef Q_COMPILER_RVALUE_REFS
	inline Expression & operator=( Expression && ) noexcept;
#endif

	enum Type {
		Invalid_Expression,
		Field_Expression,
		Value_Expression,
		RecordValue_Expression,
		And_Expression,
		Or_Expression,
		Equals_Expression,
		NEquals_Expression,
		Not_Expression,
		Larger_Expression,
		LargerOrEquals_Expression,
		Less_Expression,
		LessOrEquals_Expression,
		BitAnd_Expression,
		BitOr_Expression,
		BitXor_Expression,
		Plus_Expression,
		Minus_Expression,
		//Concat_Expression,
		IsNull_Expression,
		IsNotNull_Expression,
		In_Expression,
		NotIn_Expression,
		Like_Expression,
		ILike_Expression,
		OrderBy_Expression,
		RegEx_Expression,
		Limit_Expression,
		Query_Expression,
		Table_Expression,
		// _CombinationExpression depends on these being in this order(starting index doesn't matter)
		Union_Expression,
		UnionAll_Expression,
		Intersect_Expression,
		IntersectAll_Expression,
		Except_Expression,
		ExceptAll_Expression,
		
		Alias_Expression,
		Values_Expression,
		Null_Expression,
		TableOid_Expression,
		Sql_Expression,
		Cast_Expression,
		PlaceHolder_Expression,
		SetPlaceHolder_Expression,
		Cache_Expression,
		GroupBy_Expression,
		Coalesce_Expression,
		TempTable_Expression,
		Future_Expression,
		
		Is_Expression,
		IsNot_Expression,
		IsDistinctFrom_Expression,
		IsNotDistinctFrom_Expression,

		Between_Expression,
		Overlaps_Expression,
		OverlapsOpen_Expression,
		
		Replace_Expression,
		Lower_Expression,
		Upper_Expression,
		Position_Expression,
		Length_Expression,
		Left_Expression,
		Right_Expression,
		
		GetValue_Expression,
		Contains_Expression
	};

	bool isValid() const { return bool(p); }
	
	bool isConstant() const;
	
	Type type() const;
	
	enum StringType {
		QueryString,
		InfoString
	};
	
	QString toString(StringType=InfoString,Connection*conn=0) const;
	QVariant value(const Record & record) const;
	bool matches( const Record & record ) const;

	// Logical, sql AND
	Expression operator&&( const Expression & );
	Expression operator&&( const QVariant & v );
	// Logical, sql OR
	Expression operator||( const Expression & );
	Expression operator||( const QVariant & v );
	// Equals, sql =
	Expression operator==( const Expression & ) const;
	Expression operator==( const QVariant & v );
	
	// Not Equal, sql !=, <>
	Expression operator!=( const Expression & );
	Expression operator!=( const QVariant & v );
	Expression operator !() const;

	// Larger, sql >
	Expression operator>( const Expression & );
	Expression operator>( const QVariant & v );
	// Lesser, sql <
	Expression operator<( const Expression & );
	Expression operator<( const QVariant & v );
	// Larger or Equal, sql >=
	Expression operator>=( const Expression & );
	Expression operator>=( const QVariant & v );
	// Lesser or Equal, sql <=
	Expression operator<=( const Expression & );
	Expression operator<=( const QVariant & v );
	// Binary and, sql &
	// Dual purpose in python, becomes logical AND
	// if either of the arguments are boolean
	Expression operator&( const Expression & );
	Expression operator&( const QVariant & );
	Expression & operator&=( const Expression & );
	
	// Binary or, sql |
	// Dual purpose in python, becomes logical OR
	// if either of the arguments are boolean
	Expression operator|( const Expression & );
	Expression operator|( const QVariant & );
	Expression & operator|=( const Expression & );

	// Current XOR, should this be changed to pow
	// to match sql?
	Expression operator^( const Expression & );
	Expression operator^( const QVariant & );
	// 
	Expression operator+( const Expression & );
	Expression operator+( const QVariant & );

	Expression operator-( const Expression & );
	Expression operator-( const QVariant & );
	
	RecordList operator()(const QList<QVariant> & positionalArgs = QList<QVariant>(), const QMap<QString,QVariant> & namedArgs = QStringQVariantMap(), int lookupMode=Index::UseCache|Index::UseSelect);
	RecordList operator()(const QVariant &, int lookupMode=Index::UseCache|Index::UseSelect);
	RecordList operator()(const QVariant &,const QVariant &, int lookupMode=Index::UseCache|Index::UseSelect);
	RecordList operator()(const QVariant &,const QVariant &,const QVariant &, int lookupMode=Index::UseCache|Index::UseSelect);

	// Used for both array access with integer argument,
	// and hstore/stringmap access with a string argument
	Expression operator[](const Expression & e);

	// Supports:
	//  StringMap : String
	//  StringMap : StringList
	//  StringMap : StringMap
	//  StringList : String
	//  StringList : StringList
	//  String : String
	//  IntList : Int
	//  IntList : IntList
	Expression contains(const Expression & e);

	Expression cast( QVariant::Type type );

	Expression coalesce( const Expression & );
	
	// Valid arguments include (Null, True, False)
	Expression is( const Expression & );
	Expression isNot( const Expression & );
	
	Expression isDistinctFrom( const Expression & );
	Expression isNotDistinctFrom( const Expression & );
	
	Expression isNull();
	Expression isNotNull();
	
#ifdef _HAVE_INIT_LIST
	Expression in( std::initializer_list<Expression> list );
#endif // _HAVE_INIT_LIST
	Expression in( const ExpressionList & expressions );
	Expression in( const RecordList & records );
	Expression in( const ExpressionFuture & future );

	Expression between( const Expression & left, const Expression & right );
	

	/* Conforms to sql standard where
	 * "Each time period is considered to represent the half-open interval
	 * start <= time < end, unless start and end are equal
	 * in which case it represents that single time instant.
	 * This means for instance that two time periods with only an endpoint
	 * in common do not overlap.
	 * */
	static Expression overlaps( const Expression & start1, const Expression & end1, const Expression & start2, const Expression & end2 );
	
	/* Like overlaps, but treats each time period as an open interval
	 * which results in a true return value if start1 == end2 or
	 * start2 == end1, as expected in many cases */
	static Expression overlapsOpen( const Expression & start1, const Expression & end1, const Expression & start2, const Expression & end2 );
	
	/* String functions */
	
	// Config.Config.like( 'Assburner%' )
	Expression like( const Expression & e );
	Expression ilike( const Expression & e );
	Expression regexSearch( const QString & regex, Qt::CaseSensitivity cs = Qt::CaseSensitive );
	Expression regexSearch( const Expression & regex, Qt::CaseSensitivity cs = Qt::CaseSensitive );
//	Expression regexMatches( const Expression & regex, const Expression & flags = Expression() );
//	Expression regexReplace( const Expression & regex, const Expression & replacement, const Expression & flags = Expression() );
//	Expression regexSplitToArray( const Expression & regex, const Expression & flags = Expression() );
	
	//Expression regex();
	Expression replace( const Expression & search, const Expression & replacement );
	Expression lower();
	Expression upper();
	// Matches postgres, returns 1 based index, 0 if substring not found
	Expression position( const Expression & substring );
	Expression length();
	Expression left( const Expression & cnt );
	Expression right( const Expression & cnt );
	
	// These are mainly used to support contextual operator | and &
	// in python because operator && and || cannot be overloaded
	// They determine whether to do a binary op or logical comparison
	// depending on the output types of the involved expressions
	Expression contextualAnd( const Expression & other );
	Expression contextualOr( const Expression & other );
	
	enum OrderByDirection {
		Ascending,
		Descending
	};
	
	//Results update( const ExpressionList & setExpressions, const ExpressionList & returning );
	//Results remove();
	
	Expression orderBy( const Expression &, OrderByDirection direction = Descending ) const;
	Expression groupBy( const Expression & ) const;
	Expression limit( int limit, int offset=0 ) const;
	
	// If table is a valid pointer it will be set to the table that is selected from
	RecordList select(const QList<QVariant> & positionalArgs = QList<QVariant>(), const QMap<QString,QVariant> & namedArgs = QStringQVariantMap(), int lookupMode=Index::UseCache|Index::UseSelect, Table ** table = 0, Connection * conn = 0) const;
	
	ExpressionFuture future(const QList<QVariant> & positionalArgs = QList<QVariant>(), const QMap<QString,QVariant> & namedArgs = QStringQVariantMap(), int lookupMode=Index::UseCache|Index::UseSelect );
	
	Expression toTempTable( const QString & tempTableName = QString() ) const;
	
	Expression alias( const QString & ) const;

	// Returns true if the expression only contains orderby and/or limit
	bool isOnlySupplemental() const;
	bool containsWhereClause() const;
	bool isQuery() const;
	// Returns true if the expression/query can be executed locally against preloaded values
	//bool isLocallyExecutable(Database * db) const;
	
	// Returns returnField from table(which could be a child of returnFields table)
	// If table is 0 returnField's table() is used.
	// If returns is empty a star(*) is used
	static Expression createQuery( const ExpressionList & returns, const ExpressionList & from, const Expression & exp );
	// Each expression in queries must be of type Query_Expression, Union_Expression, Intersects_Expression
	static Expression createCombination( Expression::Type type, const ExpressionList & queries );
	static Expression createField( Field * field, TableSchema * table=0 );
	static Expression createValue( const QVariant & value, bool escaped = false );
	static Expression create( Expression::Type type, const Expression & child );
	static Expression create( Expression::Type type, const Expression & child1, const Expression & child2 );
	static Expression createValues( RecordList records, const QString & alias, FieldList * fields = 0 );
	static Expression createPlaceHolder( const QString & name, const QVariant & defaultValue = QVariant(), bool cacheAll = false );
	Expression setPlaceHolder( const QString & name, const QVariant & value );
	
	static Expression tableOid();
	static Expression null( Field::Type castType = Field::Invalid );
	
	// This is unable to reflect changeset changes, use with caution
	static Expression sql( const QString & );
		
	static bool isValidVariantType( QVariant::Type type );
	
	// Creates a deep copy of this expression tree
	Expression copy() const;
	
	void save( QDataStream & );
	static Expression load( QDataStream & );
	
	Expression prepareForExec( Connection * conn, const QList<QVariant> & positionalArgs = QList<QVariant>(), const QMap<QString,QVariant> & namedArgs = QStringQVariantMap(),
		bool * reflectsChangeset = 0, bool makeCopy = true, bool removeCacheAllExpressions = false, bool * constantNoResults = 0 ) const;
		
	Expression transformToInheritedUnion(TableList tables, QList<RecordReturn> & rr);

	int placeHolderCount() const;
	QList<QVariant> placeHoldersToPositional( const QList<QVariant> & positionalArgs, const QMap<QString,QVariant> & namedArgs ) const;
	void placeHolderInfo( bool & canExtractValues, int & cacheAllCount );
	QList<QVariant> extractPlaceHolderValues(const Record &) const;
	
	Expression cache(const Interval & maxAge = Interval(), bool useParentCache = true, bool fillParentCache = false);
	
	quint64 hash() const;
public:
	ExpressionPrivate * p;
	static ExpressionPrivate * _create( Expression::Type type );

	friend class ExpressionPrivate;
	friend class TableAlias;
	friend class ExpressionContainer;
};

#include "expression_p.h"

ExpressionFuture::ExpressionFuture( const ExpressionFuture & other )
: p(other.p) { if(p) p->ref(); }

ExpressionFuture::ExpressionFuture( ExpressionPrivate * _p )
: p(0) { if(_p && _p->type == Expression::Future_Expression) (p = _p)->ref(); }

#ifdef Q_COMPILER_RVALUE_REFS
ExpressionFuture::ExpressionFuture( ExpressionFuture && other )
: p(0) { std::swap(p,other.p); }
ExpressionFuture & ExpressionFuture::operator=(ExpressionFuture && other) noexcept
{ std::swap(p,other.p); return *this; }
#endif
ExpressionFuture::~ExpressionFuture()
{ if(p) p->deref(); }

Expression::Expression()
: p( 0 )
{
}

Expression::Expression( ExpressionPrivate * _p )
: p( _p )
{
	if( p )
		p->ref();
}

Expression::Expression( const Expression & other )
: p( other.p )
{
	if( p )
		p->ref();
}

#ifdef Q_COMPILER_RVALUE_REFS
Expression::Expression( Expression && other ) noexcept
: p( other.p )
{
	other.p = 0;
}
#endif

#ifdef Q_COMPILER_RVALUE_REFS
Expression & Expression::operator=( Expression && other ) noexcept
{
	std::swap(p,other.p);
	return *this;
}
#endif

Expression::~Expression()
{
	if( p )
		p->deref();
}

class STONE_EXPORT TableAlias
{
public:
	TableAlias( Table * table, const QString & alias );
	
	Expression operator()(const Expression & expression);
	operator Expression();
protected:
	Table * mTable;
	QString mAlias;
};

class STONE_EXPORT ExpressionContainer : public Expression
{
public:
	ExpressionContainer(const Expression & e = Expression()) : Expression(e) {}
	bool operator==(const Expression & e) const { return e.p == p; }
	// Cause MS visual studio sucks ass once again.  Soon I will get mingw-w64 up and ditch this microshit garbage for good
	uint hash() const { return qHash(p); }
};

inline uint qHash( const ExpressionContainer & ec ) { return ec.hash(); }

class FieldExpression;
class StaticFieldExpression;

class STONE_EXPORT ExpressionList : public QList<ExpressionContainer>
{
public:
	ExpressionList() {}
	ExpressionList(const QList<uint> & l);
	ExpressionList(const QList<ExpressionContainer> & ecl)
	: QList<ExpressionContainer>(ecl) {}
	ExpressionList(const QList<Expression> & ecl)
	: QList<ExpressionContainer>( *(QList<ExpressionContainer>*)(&ecl) ) {}
	ExpressionList(const QList<Table*> & tl);
	ExpressionList(const Expression & e)
	{ if( e.isValid() ) append(e); }
	inline ExpressionList(const FieldExpression & e);
	inline ExpressionList(const StaticFieldExpression & e);
	ExpressionList(const FieldList & fl);
	ExpressionList(const QStringList & sl);
	ExpressionList(const QList<QVariant> & vl);

	Expression operator[](int i) const { return Expression(QList<ExpressionContainer>::operator[](i)); }
	ExpressionList & operator<<( const Expression & e ) { append(e); return *this; }
};

inline Expression Query( ExpressionList returns, const ExpressionList & from, const Expression & exp )
{ return Expression::createQuery(returns,from,exp); }

inline Expression Query( ExpressionList returns, const Expression & from, const Expression & exp )
{ return Expression::createQuery(returns,from,exp); }

inline Expression Query( ExpressionList returns, const Expression & exp )
{ return exp.type() == Expression::Table_Expression ? Expression::createQuery(returns,exp,Expression()) : Expression::createQuery(returns,Expression(),exp); }

/* inline Expression Query( const Expression & exp )
{ return Expression::createQuery(Expression(),Expression(),exp); } */

inline Expression PlaceHolder( const QString & name = QString(), const QVariant & defaultValue = QVariant(), bool cacheAll = false )
{ return Expression::createPlaceHolder(name,defaultValue,cacheAll); }

inline Expression AndExpression( const Expression & e1, const Expression & e2 )
{ return e1.isValid() ? Expression::create( Expression::And_Expression, e1, e2 ) : e2; }

inline Expression OrExpression( const Expression & e1, const Expression & e2 )
{ return e1.isValid() ? Expression::create( Expression::Or_Expression, e1, e2 ) : e2; }

inline Expression EqualsExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::Equals_Expression, e1, e2 ); }

inline Expression NEqualsExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::NEquals_Expression, e1, e2 ); }

inline Expression NotExpression( const Expression & e1)
{ return Expression::create( Expression::Not_Expression, e1 ); }

inline Expression LargerExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::Larger_Expression, e1, e2 ); }

inline Expression LargerOrEqualsExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::LargerOrEquals_Expression, e1, e2 ); }

inline Expression LessExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::Less_Expression, e1, e2 ); }

inline Expression LessOrEqualsExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::LessOrEquals_Expression, e1, e2 ); }

inline Expression BitAndExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::BitAnd_Expression, e1, e2 ); }

inline Expression BitOrExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::BitOr_Expression, e1, e2 ); }

inline Expression BitXorExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::BitXor_Expression, e1, e2 ); }

inline Expression PlusExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::Plus_Expression, e1, e2 ); }

inline Expression MinusExpression( const Expression & e1, const Expression & e2 )
{ return Expression::create( Expression::Minus_Expression, e1, e2 ); }

inline Expression OrderByExpression( const Expression & e1, const Expression & e2, Expression::OrderByDirection direction = Expression::Descending )
{ return e1.orderBy( e2, direction ); }

inline Expression LimitExpression( const Expression & e1, int limit )
{ return e1.limit( limit ); }

inline Expression ValueExpression( const QVariant & v )
{ return Expression::createValue( v ); }

inline Expression ev_( const QVariant & v, bool escaped=false )
{ return Expression::createValue( v, escaped ); }

template<typename T> Expression _evt( const T & t )
{ return Expression::createValue( QVariant::fromValue<T>(t) ); }

inline Expression Expression::operator&&( const Expression & e ) { return AndExpression( *this, e ); }
inline Expression Expression::operator&&( const QVariant & v ) { return AndExpression( *this, ev_(v) ); }
inline Expression Expression::operator||( const Expression & e ) { return OrExpression( *this, e ); }
inline Expression Expression::operator||( const QVariant & v ) { return OrExpression( *this, ev_(v) ); }
inline Expression Expression::operator==( const Expression & e ) const { return EqualsExpression( *this, e ); }
inline Expression Expression::operator==( const QVariant & v ) { return EqualsExpression( *this, ev_(v) ); }
inline Expression Expression::operator!=( const Expression & e ) { return NEqualsExpression( *this, e ); }
inline Expression Expression::operator!=( const QVariant & v ) { return NEqualsExpression( *this, ev_(v) ); }
inline Expression Expression::operator!() const { return NotExpression( *this ); }
inline Expression Expression::operator>( const Expression & e ) { return LargerExpression( *this, e ); }
inline Expression Expression::operator>( const QVariant & v ) { return LargerExpression( *this, ev_(v) ); }
inline Expression Expression::operator<( const Expression & e ) { return LessExpression( *this, e ); }
inline Expression Expression::operator<( const QVariant & v ) { return LessExpression( *this, ev_(v) ); }
inline Expression Expression::operator>=( const Expression & e ) { return LargerOrEqualsExpression( *this, e ); }
inline Expression Expression::operator>=( const QVariant & v ) { return LargerOrEqualsExpression( *this, ev_(v) ); }
inline Expression Expression::operator<=( const Expression & e ) { return LessOrEqualsExpression( *this, e ); }
inline Expression Expression::operator<=( const QVariant & v ) { return LessOrEqualsExpression( *this, ev_(v) ); }
inline Expression Expression::operator&( const Expression & e ) { return contextualAnd( e ); }
inline Expression Expression::operator&( const QVariant & v ) { return contextualAnd( ev_(v) ); }
inline Expression & Expression::operator&=( const Expression & e ) { *this = contextualAnd(e); return *this; }
inline Expression Expression::operator|( const Expression & e ) { return contextualOr( e ); }
inline Expression Expression::operator|( const QVariant & v ) { return contextualOr( ev_(v) ); }
inline Expression & Expression::operator|=( const Expression & e ) { *this = contextualOr(e); return *this; }
inline Expression Expression::operator^( const Expression & e ) { return BitXorExpression( *this, e ); }
inline Expression Expression::operator^( const QVariant & v ) { return BitXorExpression( *this, ev_(v) ); }
inline Expression Expression::operator+( const Expression & e ) { return PlusExpression( *this, e ); }
inline Expression Expression::operator+( const QVariant & v ) { return PlusExpression( *this, ev_(v) ); }
inline Expression Expression::operator-( const Expression & e ) { return MinusExpression( *this, e ); }
inline Expression Expression::operator-( const QVariant & v ) { return MinusExpression( *this, ev_(v) ); }
inline RecordList Expression::operator()(const QVariant & v, int lookupMode)
{ return operator()(QList<QVariant>() << v,QStringQVariantMap(),lookupMode); }
inline RecordList Expression::operator()(const QVariant &v1,const QVariant &v2, int lookupMode)
{ return operator()(QList<QVariant>() << v1 << v2,QStringQVariantMap(),lookupMode); }
inline RecordList Expression::operator()(const QVariant &v1,const QVariant &v2,const QVariant &v3, int lookupMode)
{ return operator()(QList<QVariant>() << v1 << v2 << v3,QStringQVariantMap(),lookupMode); }

class STONE_EXPORT FieldExpression : public Expression
{
public:
	FieldExpression( Field * field, TableSchema * tableSchema = 0 ) : Expression(Expression::createField(field,tableSchema)) {}
	FieldExpression() {}
	
	//operator ExpressionList() const { return ExpressionList(Expression(*this)); }

	Field * operator->() { return fe()->field; }

	operator Field*() const { return fe()->field; }
	operator Field() const { return *fe()->field; }
	TableSchema * table() const { return fe()->table; }
	
private:
	inline _FieldExpression * fe() const { return (_FieldExpression*)p; }
};

class STONE_EXPORT StaticFieldExpression : public FieldExpression
{
public:
	StaticFieldExpression() { p = &_private; p->ref(); }
	StaticFieldExpression( Field * field, TableSchema * tableSchema = 0 ) : _private( field, tableSchema ) { p = &_private; p->ref(); }
	// Work around for msvc2008(maybe later) bug where _ExpressionPrivate's destory method is called
	// instead of _StaticFieldExpression's, the latter of which is supposed to prevent deref'ing p
	~StaticFieldExpression() { p=0; }
	
	Field * operator->() { return _private.field; }

	operator Field*() const { return _private.field; }
	operator Field() const { return *_private.field; }
	TableSchema * table() const { return _private.table; }

	_StaticFieldExpression _private;
	inline void setup( Field * field, TableSchema * tableSchema ) { _private.table = tableSchema; _private.field = field; }
};

/* seriously, how bad can visual studio suck (2012 this time) */
ExpressionList::ExpressionList(const FieldExpression & e)
{ if( e.isValid() ) append((const Expression &)e); }
ExpressionList::ExpressionList(const StaticFieldExpression & e)
{ if( e.isValid() ) append((const Expression &)e); }

template<class KEY,class LIST> QMap<KEY,LIST> RecordList::groupedByForeignKey( const FieldExpression & expr ) const
{
	QMap<KEY,LIST> ret;
	foreach( Record r, (*this) ) {
		ret[KEY(r.foreignKey(expr))] += r;
	}
	return ret;
}

class FutureSignalObject : public QObject
{
Q_OBJECT
public:
	FutureSignalObject(QObject * parent=nullptr);
signals:
	void then( ExpressionFuture future );
protected:
friend class _FutureExpression;
};

}

using Stone::Expression;
using Stone::ExpressionFuture;

#endif // EXPRESSION_H
