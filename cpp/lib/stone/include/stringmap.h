
#ifndef STRING_MAP_H
#define STRING_MAP_H

#include <qstring.h>
#include <qmap.h>
#include <qvariant.h>

#include "blurqt.h"

class STONE_EXPORT StringMap : public QMap<QString,QString>
{
	typedef QMap<QString,QString> Base;
public:
	StringMap();
	StringMap(const QMap<QString,QString> & o);
	~StringMap();

	static StringMap fromString( const QString & );
	QString toString() const;
};

Q_DECLARE_METATYPE(StringMap)

#endif // STRING_MAP_H

