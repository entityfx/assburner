
#include "expression.h"

#ifndef EXPRESSION_P_H
#define EXPRESSION_P_H

#include <qobject.h>

struct ToStrContext;
struct GatherPlaceHolders;
struct PrepContext;
class _CacheExpression;

class ExpressionPrivate {
public:
	
	inline void ref() { refCount.ref(); }
	inline void deref() { if( !refCount.deref() ) destroy(); }
	
	ExpressionPrivate( Expression::Type _type ) : type(_type) {}
	ExpressionPrivate( const ExpressionPrivate & other ) : type(other.type) {}
	virtual void destroy() { delete this; }
	virtual ExpressionPrivate * copy() const { return new ExpressionPrivate(*this); }
	virtual ~ExpressionPrivate() {}
	virtual QList<ExpressionPrivate*> children() const { return QList<ExpressionPrivate*>(); }
	virtual void setChildren(const QList<ExpressionPrivate*> & /*children*/){}
	virtual QVariant::Type valueType() const { return QVariant::Invalid; }
	virtual QString toString(const ToStrContext &) const { return QString(); }
	virtual QString separator() const { assert(0); return QString(); }
	virtual int operatorPrecedence() const;
	virtual bool needParens(const ToStrContext & pc) const;
	virtual QVariant value(const Record & ) const { return QVariant(); }
	virtual quint64 hash() const { return type; }
	virtual ExpressionPrivate * prepareForExec(const PrepContext & pc, Connection * conn);
	
	void gatherPlaceHolders(GatherPlaceHolders * gph);
	
	_CacheExpression * findParentCache(int & placeHoldersOnLeft,int & placeHolderCount, bool inValidNode = true);
	
	bool matches( const Record & record ) const { return value(record).toBool(); }
	virtual void append( ExpressionPrivate * ) { assert(0); }
	virtual bool isOnlySupplemental() const;
	bool containsWhereClause();
	bool isQuery()
	{
		return type == Expression::Query_Expression || (type >= Expression::Union_Expression && type <= Expression::ExceptAll_Expression);
	}
	virtual bool isConstant()
	{ return false; }
	
	virtual void save( QDataStream & ) {}
	virtual void load( QDataStream & ) {}
	bool _load( QDataStream & ds );
	void _save( QDataStream & ds );
	/*
	void visit_worker(std::function<bool(ExpressionPrivate*,const ExpPrivVec &)> visitor, QList<Expression::Type> * typesToVisit, std::vector<ExpressionPrivate*> parentStack)
	{
		if( !typesToVisit || typesToVisit->contains(type) ) {
			if( !visitor(this,parentStack) )
				return;
		}
		parentStack.push_back(this);
		foreach( ExpressionPrivate * c, children() )
			c->visit_worker(visitor,typesToVisit,parentStack);
		parentStack.pop_back();
	}
	void visit(std::function<bool(ExpressionPrivate*,const ExpPrivVec &)> visitor, QList<Expression::Type> * typesToVisit=0)
	{
		std::vector<ExpressionPrivate*> parents;
		visit_worker(visitor,typesToVisit,parents);
	}
	QList<ExpressionPrivate*> collect(QList<Expression::Type> types)
	{
		QList<ExpressionPrivate*> ret;
		visit( [&] (ExpressionPrivate * p,const ExpPrivVec &) -> bool { ret.append(p); return true; }, &types );
		return ret;
	}
	*/
	bool isMe( ExpressionPrivate * other, quint64 * myHashPtr=0 );
	Expression::Type type;
private:
	QAtomicInt refCount;
};

class _FieldExpression : public ExpressionPrivate
{
public:
	_FieldExpression( Field * f = 0, TableSchema * t = 0 ) : ExpressionPrivate( Expression::Field_Expression ), field(f), table(t) {}
	_FieldExpression( const _FieldExpression & other ) : ExpressionPrivate(other), field(other.field), table(other.table) {}
	ExpressionPrivate * copy() const;
	virtual quint64 hash() const;

	QVariant::Type valueType() const;
	virtual QVariant value( const Record & record ) const;
	virtual QString toString(const ToStrContext & pc) const;
	void save( QDataStream & ds );
	void load( QDataStream & ds );

	Field * field;
	TableSchema * table;
};

class _StaticFieldExpression : public _FieldExpression
{
public:
	_StaticFieldExpression( Field * f = 0, TableSchema * t = 0 ) : _FieldExpression( f, t ) {}
	_StaticFieldExpression( const _FieldExpression & other ) : _FieldExpression( other ) {}
	
	virtual void destroy() {}
};

#endif // EXPRESSION_P_H
