
#ifndef INT_LIST_H
#define INT_LIST_H

#include <qlist.h>
#include <qvariant.h>

#include "blurqt.h"

class STONE_EXPORT IntList : public QList<int>
{
public:
	IntList();
	IntList(const QList<int> & o);
	
	static int Null;
	
	QString toString( bool addArrayBraces=false );
	static IntList fromString(const QString &);
};

Q_DECLARE_METATYPE(IntList);

#endif // INT_LIST_H