
#ifndef _TRIGGER_H_
#define _TRIGGER_H_

#include "record.h"
#include "recordlist.h"

namespace Stone {

class Table;
class TableSchema;

class STONE_EXPORT Trigger
{
public:
	enum TriggerType
	{
		CreateTrigger			= 0x01,
		IncomingTrigger			= 0x02,
		PreInsertTrigger		= 0x04,
		PreUpdateTrigger		= 0x08,
		PreDeleteTrigger		= 0x10,
		PostInsertTrigger		= 0x20,
		PostUpdateTrigger		= 0x40,
		PostDeleteTrigger		= 0x80,
	};

	Trigger( int triggerTypes ) : mTriggerTypes( triggerTypes ) {}
	virtual ~Trigger() {}
	
	int triggerTypes() const { return mTriggerTypes; }
	
	virtual Record create( Record r ) { return r; }
	virtual RecordList incoming( RecordList rl ) { return rl; }
	virtual RecordList preInsert( RecordList rl ) { return rl; }
	virtual Record preUpdate( const Record & updated, const Record & /*before*/ ) { return updated; }
	virtual RecordList preDelete( RecordList rl ) { return rl; }
	virtual void postInsert( RecordList ) {}
	virtual void postUpdate( const Record & /*updated*/, const Record & /*before*/ ) {}
	virtual void postDelete( RecordList ) {}

	void ref() {
		refCount.ref();
	}
	void deref() {
		bool neZero = refCount.deref();
		if( !neZero ) {
			delete this;
		}
	}
private:
	QAtomicInt refCount;
	int mTriggerTypes;
	friend class Table;
	friend class TableSchema;
};

}; // namespace Stone

using Stone::Trigger;

#endif // _TRIGGER_H_

