
#ifndef CHANGE_SET_CHANGE_H 
#define CHANGE_SET_CHANGE_H

#include "recordlist.h"

namespace Stone {
	
struct STONE_EXPORT ChangeSet::Change {
	ChangeSet::ChangeType type;
	ChangeSet changeSet() const;
	RecordList records() const;
protected:
	Change( ChangeType type, RecordList records );
	Change( ChangeSet );
	ChangeSetWeakRef changeset;
	RecordList mRecords;
	void * commitState;
	friend class ChangeSet;
	friend class ChangeSetPrivate;
};

}; // namespace Stone

#endif // CHANGE_SET_CHANGE_H