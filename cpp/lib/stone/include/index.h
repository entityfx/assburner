
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef INDEX_H
#define INDEX_H

#include <qlist.h>
#include <qvariant.h>
#include <qreadwritelock.h>
#include <qmutex.h>
#include <qhash.h>

#include "field.h"
#include "indexschema.h"
#include "interval.h"

#define _INDEX_USE_MUTEX_
// #define _INDEX_USE_RWLOCK_

uint qHash( const QVariant & var );
QString qVariantListToString( const QList<QVariant> & vars );

namespace Stone {

/* Class allows easy and efficient iteration of args that have embedded
 * lists of qvariants. [a,[b,c]] will iterate as [a,b], [a,c].
 * [a,[b,c],[d,e]] will iterate as [a,b,d],[a,b,e],[a,c,d],[a,c,e].
 */
class ArgIter {
public:
	ArgIter(const QList<QVariant> & args);
	QList<QVariant> & current() { return mCurrent; }
	int size() const { return mSize; }
	int pos() const { return mPos; }
	bool isValid() const { return mPos < mSize; }
	bool next(); /// Returns true if the new position is valid
protected:
	QList<QVariant> mArgs, mCurrent;
	// (position in args, index in sub-list)
	QVector<int> mSubListIndexes, mSubListSizes;
	int mSize, mPos;
};


class Record;
class RecordImp;
class RecordList;
class Table;
class ChangeSet;
class Expression;

/**
 * \ingroup Stone
 * @{
 * Base class for Index implementations.
 */
class STONE_EXPORT Index
{
public:
	enum Flags {
		HoldsListFlag = 0x2,
		FillFromIncomingFlag = 0x4
	};

	Index( Table * table, Flags flags, int depth, bool useCache = false );
	virtual ~Index();

	enum {
		NoInfo,
		RecordsFound
	};
	
	enum LookupMode {
		// Allow getting values from the cache if available
		UseCache = 0x1,
		// Allow selecting from the database if not all values are available in the cache(or not using the cache)
		UseSelect = 0x2,
		// Only select values missing from the cache.  Useful only with both UseCache and UseSelect defined.
		// Only applicable to recordsByIndexMulti
		PartialSelect = 0x4,
	};
	
	Table * table() const { return mTable; }

	// Setting this to false will clear all cached values
	void setCacheEnabled( bool cacheEnabled );
	bool cacheEnabled() const;

	bool debug() const { return mDebug; }
	void setDebug( bool debug );
	
	// This returns results for multiple index entries at once.
	// The size of args must be a multiple of the number of columns
	// used in this index.
	RecordList recordsByIndexMulti( const QList<QVariant> & args, int lookupMode = UseCache | UseSelect | PartialSelect, const Interval & maxAge = Interval() );
	RecordList recordsByIndex( const QList<QVariant> & args, int lookupMode = UseCache | UseSelect, const Interval & maxAge = Interval() );
	Record recordByIndex( const QList<QVariant> & args, int lookupMode = UseCache | UseSelect, const Interval & maxAge = Interval() );
	
	// Optimized for single record, single arg, which is a common case
	Record recordByIndex( const QVariant & arg, int lookupMode = UseCache | UseSelect, const Interval & maxAge = Interval() );
	
	// Returns the number of variables used for each index entry
	// The number of columns for simple indexes, or the number of placeholders
	// for indexed expressions
	int depth() const { return mDepth; }
	bool holdsList() const { return mFlags & HoldsListFlag; }
	bool canFillFromIncoming() const { return mFlags & FillFromIncomingFlag; }
	
	virtual QList<QVariant> extractValues( const Record & ) const { return QList<QVariant>(); }
	virtual RecordList select( const QList<QVariant> & vars );
	virtual RecordList records( const QList<QVariant> & vars, int & status, QDateTime * timestamp = 0, bool needLock=true );
	virtual Record record( const QVariant & var, int & status, QDateTime * timestamp = 0 );
	virtual void setEmptyEntry( const QList<QVariant> & vars )=0;
	virtual void setEmptyEntry( const QVariant & var )=0;
	virtual void setFilledEntry( const QList<QVariant> & vars ){ Q_UNUSED(vars); }
	virtual void clearEntries( const QList<QVariant> & vars, bool needLock=true ){ Q_UNUSED(vars); Q_UNUSED(needLock); }
	virtual void clearEntries( const QVariant & var, bool needLock=true ){ Q_UNUSED(var); Q_UNUSED(needLock); }
	
	// Atomically clears all entries associated with args, then refills with entries
	// The index must support FillFromIncoming
	void setEntries( const QList<QVariant> & args, RecordList entries );

	// Records that have been created locally but not committed to the database
	// This function is (currently) called when a record needs a primary key, though
	// in the future it may be called the first time a record is assigned
	virtual void recordsCreated( const RecordList & );
	virtual void recordAdded( const Record & )=0;
	virtual void recordRemoved( const Record & )=0;
	virtual void recordUpdated( const Record &, const Record & )=0;
	virtual void recordsIncoming( const RecordList &, bool ci = false, bool needLock=true )=0;
	virtual void clear()=0;
	virtual void reserve(int size);

	virtual QString description() const;
	virtual QString stats() const;
	
	void printStats();

protected:
	Table * mTable;
	Flags mFlags;
	int mDepth;
	bool mCacheIncoming, mUseCache;
	
	virtual bool checkMatch( const Record & record, const QList<QVariant> & args, int entryCount );
	RecordList applyChangeSetHelper(ChangeSet cs, RecordList, const QList<QVariant> & args, int entryCount);
	RecordList applyChangeSet(RecordList, const QList<QVariant> & args, int entryCount);
	
	virtual bool checkMatch( const Record & record, const QVariant & arg );
	Record applyChangeSetHelper(ChangeSet cs, const Record &, const QVariant & arg);
	Record applyChangeSet(const Record &, const QVariant & args);

	virtual bool recordIncomingNode( const QList<QVariant> &, const RecordList &, bool=true ) { return true; }
	virtual bool recordIncomingNode( const QList<QVariant> &, const Record &, bool=true ) { return true; }
	virtual bool recordIncomingNode( const QVariant &, const Record &, bool=true ) { return true; }

	QString argString(const QList<QVariant> & args);
	void reportDuplicate(const QList<QVariant> & args, RecordList records);
	void reportDuplicate(const QList<QVariant> & args, const Record & r1, const Record & r2);
	
private:
	Index(const Index &) : mFlags() {}

	
public:
	QDateTime mCurrentTimestamp;
#ifdef _INDEX_USE_MUTEX_
	QMutex mMutex;
#endif
#ifdef _INDEX_USE_RWLOCK_
	QReadWriteLock mRWLock;
#endif
	bool mDebug;
};

/**
 * Uses variable levels of QHash's to index records by arbitrary values.
 * This is also a base class with derived classes controlling how the
 * indexing values are calculated and other customizations.
 */
class STONE_EXPORT HashIndex : public Index
{
public:
	enum FindNodeFlags
	{
		CreateNever,
		CreateAlways,
		CreateIfFilled
	};
	
	struct BranchNode {
		BranchNode() : mFlags(0) {}
		enum {
			Filled = 0x1
		};
		int mFlags;
		QDateTime mFilledTimestamp;
		QHash<QVariant,void*> mValues;
	};
	struct LeafNode {
		LeafNode( const QDateTime & timestamp ) : mTimestamp(timestamp), mRecord(0) {}
		QDateTime mTimestamp;
		union {
			RecordImp * mRecord;
			RecordList * mRecordList;
		};
	};
	
	HashIndex( Table * parent, Flags flags, int depth, bool useCache = false );
	~HashIndex();

	virtual RecordList records( const QList<QVariant> & vars, int & status, QDateTime * timestamp = 0, bool needLock = true );
	virtual Record record( const QVariant & var, int & status, QDateTime * timestamp = 0 );
	
	virtual void setEmptyEntry( const QList<QVariant> & vars );
	virtual void setEmptyEntry( const QVariant & var );
	virtual void setFilledEntry( const QList<QVariant> & vars );
	virtual void clearEntries( const QList<QVariant> & vars, bool needLock=true );
	virtual void clearEntries( const QVariant & var, bool needLock=true );
	virtual void recordAdded( const Record & );
	virtual void recordRemoved( const Record & );
	virtual void recordUpdated( const Record &, const Record & );
	virtual void recordsIncoming( const RecordList &, bool ci = false, bool needLock=true );
	virtual void clear();
	virtual void reserve(int size);
	
	virtual QString stats() const;
protected:
	void * findNode(const QList<QVariant> & values, int findNodeFlags);
	BranchNode * findBranch(const QList<QVariant> & values, int findNodeFlags);
	LeafNode * findLeaf(const QList<QVariant> & values, int findNodeFlags);
	LeafNode * findLeaf(const QVariant & value, int findNodeFlags);
	
	bool recordIncomingNode( const QList<QVariant> & values, const RecordList &, bool create=true );
	bool recordIncomingNode( const QList<QVariant> & values, const Record &, bool create=true );
	bool recordIncomingNode( const QVariant & value, const Record & record, bool create=true );
	int recordRemovedNode( const QList<QVariant> & values, RecordImp * record );
	void clearNode( BranchNode *, int depthToGo );
	
	struct Stats {
		Stats() : branchNodes(0), leafNodes(0), records(0) {}
		int branchNodes, leafNodes, records;
	};

	void collectStats( LeafNode * node, Stats & s ) const;
	void collectStats( BranchNode * node, int depthToGo, Stats & s ) const;

	BranchNode * mRoot;
	int mHashSize;
};

/**
 * Simple Field <-> Index value mapping that uses IndexSchemas
 * to define which Fields are indexed.
 */
class STONE_EXPORT SimpleIndex : public HashIndex
{
public:
	SimpleIndex( Table * parent, IndexSchema * );
	~SimpleIndex();
	IndexSchema * schema() const { return mSchema; }
	virtual RecordList select( const QList<QVariant> & vars );
	virtual QList<QVariant> extractValues( const Record & record ) const;
	virtual void recordsIncoming( const RecordList &, bool ci = false, bool needLock=true );

	virtual QString description() const;

protected:
	virtual bool checkMatch( const Record & record, const QList<QVariant> & args, int entryCount );
	virtual bool checkMatch( const Record & record, const QVariant & arg );

	IndexSchema * mSchema;
};

/**
 * This is the class responsible for keeping a primary key -> record
 * mapping for all tables.  It optionally uses a weak ref to avoid
 * keeping all committed records from ever being released.
 * The table itself controls that behavior through the expire key cache
 * option.
 */

class STONE_EXPORT KeyIndex : public Index
{
public:
	KeyIndex( Table * parent, IndexSchema * schema );
	virtual ~KeyIndex();

	Record record( uint key, bool * foundEntry );

	virtual RecordList records( const QList<QVariant> & vars, int & status, QDateTime *, bool needLock=true );
	virtual Record record( const QVariant & var, int & status, QDateTime * );
	virtual void setEmptyEntry( const QList<QVariant> & vars );
	virtual void setEmptyEntry( const QVariant & var );
	
	virtual void recordsCreated( const RecordList & );
	virtual void recordAdded( const Record & );
	virtual void recordRemoved( const Record & );
	virtual void recordUpdated( const Record &, const Record & );
	virtual void recordsIncoming( const RecordList &, bool ci = false, bool needLock=true );
	virtual void clear();
	bool expire( uint recordKey, RecordImp * imp );

	RecordList values();
	
protected:

	QHash<uint, RecordImp*> mDict;
	uint mPrimaryKeyColumn;
};

} //namespace

using Stone::Index;
using Stone::KeyIndex;
using Stone::HashIndex;

typedef QList<Index*> IndexList;
typedef QList<Index*>::Iterator IndexIter;

#include "record.h"

/// @}

#endif // INDEX_H

