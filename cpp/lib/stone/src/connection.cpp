
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include "Python.h"

#include <qdatetime.h>
#include <qsqldatabase.h>
#include <qsqldriver.h>
#include <qsqlerror.h>
#include <qtimer.h>

// QSql can suck it
#define protected public
#define private public
#include <qsqlresult.h>
#undef protected
#undef private

#include <qsqlfield.h>
#include <qsqlquery.h>
#include <qsqlrecord.h>

#include "connection.h"
#include "database.h"
#include "expression.h"
#include "iniconfig.h"
#include "sqlerrorhandler.h"
#include "pgconnection.h"
#include "sqliteconnection.h"
#include "pyembed.h"

#ifdef Q_OS_WIN
#include <windows.h>
#else
#include <unistd.h>
#endif

namespace Stone {

SqlException::SqlException( const QString & sql, const QString & error )
: mSql(sql)
, mError(error)
{}

QString SqlException::sql() const
{ return mSql; }

QString SqlException::error() const
{ return mError; }

const char * SqlException::what() const throw()
{
	if( mCString.isNull() )
		mCString = QString("%1: %2").arg(mError).arg(mSql).toUtf8();
	return mCString.constData();
}

LostConnectionException::LostConnectionException()
{}

const char * LostConnectionException::what() const throw()
{ return "Lost Connection"; }

Connection::Connection(const QString & dbType, QObject * parent)
: QObject( parent )
, mDatabaseType( dbType )
, mPort( 0 )
, mReconnectDelay(10)
, mConnectionAttempts(0)
, mMaxConnectionAttempts(-1)
, mAutoDisconnectDelay(0)
, mAuthChange(0)
, mAutoDisconnectTimer(0)
, mReservedNames(0)
{}

QString Connection::prepareColumnLiteral( const QString & cl )
{ return cl; }

QVariant Connection::prepareValue( const QVariant & v )
{ return v; }

QString Connection::prepareValueString( const QVariant & v )
{
	QVariant p = prepareValue(v);
	if( p.type() == QVariant::Bool )
		return p.toString().toUpper();
	return p.toString();
}

Connection * Connection::create( const QString & dbType )
{
	if( dbType == "QPSQL7" ) {
		PGConnection * c = new PGConnection();
		return c;
	} else if( dbType == "QSQLITE" ) {
		SqliteConnection * c = new SqliteConnection();
		return c;
	}	
	LOG_1( "Unable to create connection for dbType: " + dbType );
	return 0;
}

Connection * Connection::createFromIni( IniConfig & _cfg, const QString & section )
{
	IniConfig cfg(_cfg);
	cfg.pushSection( section );
	Connection * ret = create( cfg.readString( "DatabaseDriver" ) );
	if( ret )
		ret->setOptionsFromIni(cfg);
	cfg.popSection();
	Q_ASSERT(ret != 0);
	return ret;
}

void Connection::setHost( const QString & host )
{
	mHost = host;
}

void Connection::setPort( int port )
{
	//LOG_3( "Port set to " + QString::number(port) );
	mPort = port;
}

void Connection::setDatabaseName( const QString & dbName )
{
	mDatabaseName = dbName;
}

void Connection::setUserName( const QString & userName )
{
	mUserName = userName;
	mAuthChange++;
}

void Connection::setPassword( const QString & password )
{
	mPassword = password;
	mAuthChange++;
}

void Connection::setReconnectDelay( int reconnectDelay )
{
	mReconnectDelay = reconnectDelay;
}

void Connection::setMaxConnectionAttempts( int maxConnectionAttempts )
{
	mMaxConnectionAttempts = maxConnectionAttempts;
}

void Connection::setAutoDisconnectDelay( int autoDisconnectDelay )
{
	mAutoDisconnectDelay = autoDisconnectDelay;
	if( mAutoDisconnectTimer && autoDisconnectDelay <= 0 )
		mAutoDisconnectTimer->stop();
	else if( autoDisconnectDelay > 0 ) {
		if( !mAutoDisconnectTimer ) {
			mAutoDisconnectTimer = new QTimer(this);
			connect( mAutoDisconnectTimer, SIGNAL(timeout()), SLOT(closeConnection()) );
		}
		if( isConnected() )
			mAutoDisconnectTimer->start( autoDisconnectDelay * 1000 );
	}
}

void Connection::setOptionsFromIni( IniConfig cfg )
{
	setHost( cfg.readString( "Host" ) );
	setPort( cfg.readInt( "Port" ) );
	setDatabaseName( cfg.readString( "DatabaseName" ) );
	setUserName( cfg.readString( "User" ) );
	setPassword( cfg.readString( "Password" ) );
	setReconnectDelay( cfg.readInt( "ReconnectDelay", 30 ) );
	setMaxConnectionAttempts( cfg.readInt( "MaxConnectionAttempts", -1 ) );
	setAutoDisconnectDelay( cfg.readInt( "AutoDisconnectDelay", 0 ) );
	mPythonStackTraceOnTables = cfg.readString( "PythonStackTraceOnTables" ).split(",",QString::SkipEmptyParts);
}

bool Connection::checkConnection()
{
	// Retry once right away
	if( !isConnected() && !reconnect() )
	{
		emit connectionLost();
		emit connectionLost2();
		// Now either quit should have been called
		// or we are reconnected
	}
	return isConnected();
}

bool Connection::closeConnection( bool transactionSafe )
{
	if( transactionSafe && insideTransaction() )
		return false;
	mAutoDisconnectTimer->stop();
	return true;
}

QString Connection::prepareFieldName( Field * field )
{
	if( !mReservedNames ) return field->name();
	uint hash = field->fieldNameHash();
	QHash<uint,QString>::const_iterator it = mReservedNames->constFind(hash);
	return it == mReservedNames->constEnd() ? field->name() : *it;
}

void Connection::addReservedNames( QHash<uint,QString> * hash, const char * const names [] )
{
	for( int i=0; names[i]; ++i ) {
		QString name(names[i]);
		(*hash)[qHash(name)] = "\"" + name + "\"";
	}
}

QMutex * Connection::lock() const
{
	int tries=0;
	while( !mDbMutex.tryLock(500) ) {
		LOG_1( "Mutex lock timed out, possible deadlock" );
		if( tries++ > 10 )
			*((int*)0x1) = 1; // Crash!
	}
	return &mDbMutex;
}

static int qSqlConnNumber()
{
	static int sConnNumber = 1;
	return sConnNumber++;
}

QSqlDbConnection::QSqlDbConnection( const QString & driverName )
: Connection( driverName )
, mInsideTransaction( false )
{
	mDb = QSqlDatabase::addDatabase( driverName, "QSqlDbConnection" + QString::number(qSqlConnNumber()) );
}

QSqlDbConnection::~QSqlDbConnection()
{
	if( mDb.isOpen() )
		mDb.close();
}

void QSqlDbConnection::setOptionsFromIni( IniConfig cfg )
{
	QStringList opts = cfg.readString( "Options" ).split( ";" );
	int timeout = cfg.readInt( "Timeout", 0 );
	if( timeout > 0 )
		opts += ("connect_timeout=" + QString::number(timeout));
	mExtraConnectOptions = opts.join( ";" );
	Connection::setOptionsFromIni(cfg);
}

void QSqlDbConnection::setupSqlDbOptions()
{
	Locker locker(lock());
	mDb.setHostName( mHost );
	mDb.setPort( mPort );
	mDb.setDatabaseName( mDatabaseName );
	mDb.setUserName( mUserName );
	mDb.setPassword( mPassword );
	mDb.setConnectOptions( mExtraConnectOptions );
}

Connection::Capabilities QSqlDbConnection::capabilities() const
{
	Locker locker(lock());
	return mDb.driver()->hasFeature( QSqlDriver::Transactions ) ? Cap_Transactions : static_cast<Connection::Capabilities>(0);
}

static int getQueryType( const QSqlQuery & query )
{
	int i = 0;
	QString queryString(query.lastQuery());
	while( i < queryString.size() && queryString[i].isSpace() ) i++;
	QStringRef q = queryString.midRef(i);
	static QString select("select"), update("update"), insert("insert"), delet("delete");
	if( q.startsWith( select, Qt::CaseInsensitive ) )
		return Database::EchoSelect;
	else if( q.startsWith( update, Qt::CaseInsensitive ) )
		return Database::EchoUpdate;
	else if( q.startsWith( insert, Qt::CaseInsensitive ) )
		return Database::EchoInsert;
	else if( q.startsWith( delet, Qt::CaseInsensitive ) )
		return Database::EchoDelete;
	return -1;
}

static QString queryNameFromType( int queryType )
{
	switch( queryType ) {
		case Database::EchoSelect:
			return QString("SELECT");
		case Database::EchoUpdate:
			return QString("UPDATE");
		case Database::EchoInsert:
			return QString("INSERT");
		case Database::EchoDelete:
			return QString("DELETE");
	}
	return QString("UNKNOWN");
}

QSqlQuery QSqlDbConnection::fakePrepare( const QString & sql )
{
// We can use the QSqlResult hack on windows because the symbols arent exported
#ifdef Q_OS_WIN
	checkConnection();
	Locker locker(lock());
	QSqlQuery q(mDb);
	q.prepare(sql);
#else
	Locker locker(lock());
	QSqlResult * result = mDb.driver()->createResult();
	QSqlQuery q( result );
	result->QSqlResult::prepare( sql );
#endif
	return q;
}

QSqlQuery QSqlDbConnection::exec( const QString & sql, const QList<QVariant> & vars, bool reExecLostConn, Table * table )
{
	QSqlQuery query = fakePrepare(sql);
	foreach( QVariant v, vars )
		query.addBindValue( v );
	exec( query, reExecLostConn, table, true );
	return query;
}

static QString boundValueString( const QSqlQuery & q )
{
	QStringList valueStrings;
	QMapIterator<QString, QVariant> i(q.boundValues());
	while (i.hasNext()) {
		QString valueString;
		i.next();
		if( !i.key().isEmpty() && i.key() != ":f" )
			valueString = i.key() + ":";
		valueString += i.value().toString();
		valueStrings += valueString;
	}
	if( valueStrings.size() )
		return "(" + valueStrings.join(",") + ")";
	return QString();
}

bool QSqlDbConnection::exec( QSqlQuery & query, bool reExecLostConn, Table * table )
{
	return exec( query, reExecLostConn, table, false );
}

bool QSqlDbConnection::exec( QSqlQuery & query, bool reExecLostConn, Table * table, bool usingFakePrepareHack )
{
	if( mAutoDisconnectTimer && mAutoDisconnectDelay > 0 )
		mAutoDisconnectTimer->stop();
	
	bool result = false, ptb = false;
	int queryType = getQueryType( query );
	int connectionErrorCount = 0;
	
	if( table && mPythonStackTraceOnTables.contains(table->tableName(),Qt::CaseInsensitive) ) {
		LOG_1("ThrowOnTables contains " + table->tableName().toLower() );
		LOG_1(pythonStackTrace());
		ptb = true;
		//throw SqlException( query.lastQuery().simplified(), "ThrowOnTables contains " + table->tableName().toLower() );
	}
#if 0
	PyThreadState *_save = 0;
	if( Py_IsInitialized() && PyEval_ThreadsInitialized() ) {
		PyThreadState * ts = PyGILState_GetThisThreadState();
		if( ts && PyThreadState_IsCurrent(ts) )
			_save = PyEval_SaveThread();
	}
	try {
#endif
		do {
			// If gui dialog is connected to the connectionLost signal
			// then it will block in checkConnection until a connection
			// is made(or the user quits the app).
			if( isConnected() || (((mMaxConnectionAttempts < 0) || (mConnectionAttempts < mMaxConnectionAttempts)) && checkConnection()) ) {
				QTime time;
				time.start();
				{
					bool fakePrepare = usingFakePrepareHack && (capabilities() & Cap_FakePrepareHack);
					Locker locker(lock());
#ifndef Q_OS_WIN
					if( fakePrepare ) {
						QSqlResult * qsr = (QSqlResult*)query.result();
						qsr->resetBindCount();

						if (qsr->lastError().isValid())
							qsr->setLastError(QSqlError());

						result = qsr->QSqlResult::exec();
					} else
#endif
						result = query.exec();
				}
				
				int elapsed = time.elapsed();
				
				if( result ) {
					if( table )
						table->addSqlTime( elapsed, queryType );
		
					Database * db = table ? table->database() : Database::current();
					if( ptb || (queryType > 0 && db && (db->echoMode() & queryType)) ) {
						QString msg = QString("%1 ms, %2").arg(elapsed).arg(query.executedQuery());
						QString bnd = boundValueString(query);
						if( !bnd.isEmpty() )
							msg += ", " + bnd;
						LOG_3( msg );
					}
					break;
				} else {
					QSqlError error = query.lastError();
					if( !isConnected() || isConnectionError(error) ) {
						LOG_1( "Connection Error During " + queryNameFromType( queryType ) + (reExecLostConn ? (", retrying") : QString()) );
						LOG_1( "Error was: " + error.text() );
						LOG_1( "Query was: " + query.lastQuery() );
						if( mInsideTransaction || connectionErrorCount >= 10 ) {
							throw LostConnectionException();
						}
						connectionErrorCount++;
						reconnect();
						continue;
					}
					mLastErrorText = query.executedQuery() + "\n" + error.text();
					LOG_1( mLastErrorText );
					SqlErrorHandler::instance()->handleError( mLastErrorText );
					throw SqlException(query.executedQuery(), mLastErrorText);
					break;
				}
			} else {
				if( mMaxConnectionAttempts >= 0 && mConnectionAttempts++ >= mMaxConnectionAttempts )
					break;
				QDateTime now = QDateTime::currentDateTime();
				do {
					// This will only happen in a non-gui application, otherwise the lost connection
					// dialog in stonegui will enter it's event loop inside the check connection call above.
					//QTime t;
					//t.start();
					//QCoreApplication::instance()->processEvents( QEventLoop::WaitForMoreEvents, 50 );
					//if( t.elapsed() < 50 ) {
						//LOG_5( "QApplication::processEvents returned before the timeout period, we must be outside the event loop" );
						#ifdef Q_OS_WIN
						Sleep( 1000 );
						#else
						sleep( 1 );
						#endif
					//}
				} while( now.secsTo( QDateTime::currentDateTime() ) < mReconnectDelay );
			}
		} while( reExecLostConn );
#if 0
		if( _save ) {
			PyEval_RestoreThread(_save);
			_save = 0;
		}
	}
	catch (...) {
		if( _save )
			PyEval_RestoreThread(_save);
		throw;
	}
#endif
	if( mAutoDisconnectTimer && mAutoDisconnectDelay > 0 )
		mAutoDisconnectTimer->start(mAutoDisconnectDelay * 1000);

	return result;
}

RecordList Connection::executeQuery( const QString & queryString, Table * table, const QList<QVariant> & vars )
{
	RecordList ret;
	QSqlQuery sql = exec( queryString, vars, true );

	while( sql.next() )
		ret += Record( new RecordImp( table, sql ), false );
	
	return ret;
}

RecordList Connection::executeQuery( const QString & queryString, Schema * schema, const QList<QVariant> & vars )
{
	RecordList ret;
	TableSchema * tableSchema = new TableSchema(schema,"anon","anon",false);
	
	QSqlQuery sql = exec( queryString, vars, true );
	QSqlRecord r = sql.record();
	for( int i = 0; i < r.count(); ++i ) {
		QSqlField f = r.field(i);
		new Field( tableSchema, f.name(), Field::variantTypeToFieldType(f.type()) );
	}
	
	Table * table = new Table(Database::current(), tableSchema);
	// Ref and deref to avoid leaking table and tableSchema if the query returns no results
	table->ref();
	
	while( sql.next() )
		ret += Record( new RecordImp( table, sql ), false );
	
	table->deref();
	return ret;
}

RecordList Connection::executeExpression( const Expression & exp )
{
	return executeQuery( exp.toString(Expression::QueryString,this) );
}

RecordList Connection::executeExpression( Table * table, FieldList fields, const Expression & exp )
{
	RecordList ret;
	
	QSqlQuery sq = exec( exp.toString(Expression::QueryString,this), QList<QVariant>(), true /*retry*/, table );
	while( sq.next() )
		ret += Record( new RecordImp( table, sq, 0, &fields ), false );
	return ret;
}

QMap<Table*,RecordList> Connection::executeExpression( Table * table, const RecordReturn & rr, const Expression & exp )
{
	Schema * schema = table->schema()->schema();
	QSqlQuery sq = exec( exp.toString(Expression::QueryString,this), QList<QVariant>(), true /*retry*/, table );
	QMap<Table*,RecordList> ret;
	while( sq.next() ) {
		Table * t = rr.table;
		if( rr.tableOidPos >= 0 ) {
			TableSchema * ts = tableByOid( sq.value(rr.tableOidPos).toUInt(), schema );
			if( ts )
				t = ts->table();
			else {
				LOG_1( "Unable to load record without table" );
				continue;
			}
		}
		if( t ) {
			QMap<Table*,QVector<int> >::const_iterator it = rr.columnPositions.find(t);
			if( it != rr.columnPositions.end() )
				ret[t] += Record( new RecordImp( t, sq, *it ), false );
		}
	}
	return ret;
}

QString Connection::tableQuoted( const QString & table )
{
	if( table.contains( "-" ) || table == "user" )
		return "\"" + table + "\"";
	return table;
}

QString Connection::getSqlFields( TableSchema * schema, const QString & _tableAlias, bool needTableOid, FieldList * usedFields, int * pkeyPos )
{
	bool hasAlias = !_tableAlias.isEmpty();
	QString alias( tableQuoted(hasAlias ? _tableAlias : schema->tableName()) );
	bool useCache = !(hasAlias || usedFields || pkeyPos);
	QHash<TableSchema*,QString>::iterator it =  useCache ? mSqlFields.find( schema ) : mSqlFields.end();
	QString ret;
	if( it == mSqlFields.end() ) {
		QStringList fields;
		int i = 0;
		foreach( Field * f, schema->fields() ) {
			if( pkeyPos && f->flag( Field::PrimaryKey ) )
				*pkeyPos = i;
			if( !f->flag( Field::NoDefaultSelect | Field::LocalVariable ) ) {
				fields += f->name().toLower();
				i++;
				if( usedFields ) *usedFields += f;
			}
		}
		QString sql = alias + ".\"" + fields.join( "\", " + alias + ".\"" ) + "\"";
		if( useCache )
			mSqlFields.insert( schema, sql );
		ret = sql;
	} else
		ret = it.value();
	if( needTableOid )
		return alias + ".tableoid, " + ret;
	return ret;
}

QString Connection::prepareWhereClause(const QString & where)
{
	if( !where.isEmpty() ) {
		QString w(where.trimmed());
		bool needsWhere = true;
		const char * keyWords[] = {"LIMIT","ORDER","WHERE","INNER","OUTER","LEFT","RIGHT","FULL","JOIN", 0};
		for( int i = 0; keyWords[i]; ++i ) {
			if( w.startsWith(keyWords[i], Qt::CaseInsensitive) ) {
				needsWhere = false;
				break;
			}
		}
		if( needsWhere )
			w = " WHERE " + w;
		else
			w = " " + w;
		return w;
	}
	return where;
}


RecordList Connection::selectFrom( Table * table, const QString & _from, const QList<QVariant> & args )
{
	RecordList ret;
	if( !table )
		return ret;
	
	TableSchema * schema = table->schema();
	if( !schema )
		return ret;
	
	QString from(_from.trimmed());
	if( !from.startsWith( "FROM", Qt::CaseInsensitive ) )
		from = "FROM " + from;
	
	QString select( "SELECT " + getSqlFields(schema) + " " + from );

	QSqlQuery sq = exec( select, args, true /*retry*/, table );
	while( sq.next() )
		ret += Record( new RecordImp( table, sq ), false );
	return ret;
}

bool Connection::update( Table * table, RecordList records, RecordList * returnValues )
{
	foreach( Record r, records ) {
		Record ret;
		update( table, r.imp(), returnValues ? & ret : 0 );
		if( returnValues )
			returnValues->append(ret);
	}
	return true;
}

TableSchema * Connection::tableByOid( uint /*oid*/, Schema * )
{
	return 0;
}

uint Connection::oidByTable( TableSchema * )
{
	return -1;
}

QStringList Connection::pythonStackTraceOnTables() const
{
	return mPythonStackTraceOnTables;
}

void Connection::setPythonStackTraceOnTables( const QStringList & tables )
{
	mPythonStackTraceOnTables = tables;
}

void Connection::addPythonStackTraceOnTable( const QString & tableName )
{
	if( !mPythonStackTraceOnTables.contains(tableName) )
		mPythonStackTraceOnTables.append(tableName);
}

void Connection::removePythonStackTraceOnTable( const QString & tableName )
{
	mPythonStackTraceOnTables.removeAll(tableName);
}

void Connection::_connected()
{
	emit connected();
}


#if 0
QList<RecordList> Connection::executeExpression( Table * table, QList<RecordReturn> rrl, const Expression & exp )
{
	Schema * schema = table->schema()->schema();
	QList<RecordList> ret;
	while(ret.size()) < rrl.size()) ret.append(RecordList());
	
	QSqlQuery sq = exec( exp.toString(Expression::QueryString), QList<QVariant>(), true /*retry*/, table );
	while( sq.next() ) {
		int retCol = 0;
		foreach( const RecordReturn & rr, rrl ) {
			Table * t = rr.table;
			if( rr.tableOidPos >= 0 ) {
				TableSchema * ts = tableByOid( sq.value(rr.tableOidPos).toUInt(), schema );
				if( ts )
					t = ts->table();
				else {
					LOG_1( "Unable to load record without table" );
					continue;
				}
			}
			if( t ) {
				QMap<Table*,QVector<int> >::iterator it = rr.columnPositions.find(t);
				if( it != rr.columnPositions.end() )
					ret[retCol] += Record( new RecordImp( t, sq, *it ), false );
			}
			retCol++;
		}
	}
	return ret;
}
#endif

bool QSqlDbConnection::reconnect()
{
	do
	{
		Locker locker(lock());
		if( mDb.isOpen() )
			mDb.close();
		mInsideTransaction = false;
		mDb.setHostName( mHost );
		mDb.setPort( mPort );
		mDb.setDatabaseName( mDatabaseName );
		// Default to OS user if username is empty
		if( mUserName.isEmpty() )
			mUserName = getUserName();
		mDb.setUserName( mUserName );
		mDb.setPassword( mPassword );
		mDb.setConnectOptions( mExtraConnectOptions );
		if( !mDb.open() ){
			QSqlError error = mDb.lastError();
			if( error.databaseText().contains("no password supplied") ) {
				int lastAuthChange = mAuthChange;
				emit authenticate(this);
				if( lastAuthChange != mAuthChange )
					continue;
			}
			mLastErrorText = error.driverText() + "(" + error.databaseText() + ")";
			LOG_3( "Could not reconnect to database: " + mLastErrorText );
			return false;
		}
		mConnectionAttempts = 0;
	} while(false);
	_connected();
	return true;
}


bool QSqlDbConnection::isConnected()
{
	// This should be thread-safe as it only does a read and
	// internally the object it reads has the same lifetime
	// as mDb
	return mDb.isOpen();
}

bool QSqlDbConnection::closeConnection( bool transactionSafe )
{
	Locker locker(lock());
	if( !Connection::closeConnection(transactionSafe) )
		return false;
	mDb.close();
	return true;
}

QString QSqlDbConnection::connectString()
{
	Locker locker(lock());
	return "Host: " + mDb.hostName() + " Port: " + QString::number(mDb.port()) + " Database: " + mDb.databaseName() + " User: " + mDb.userName() + " Options: " + mDb.connectOptions();
}

bool QSqlDbConnection::isConnectionError( const QSqlError & )
{
	return false;
}

bool QSqlDbConnection::beginTransaction()
{
	Database * db = Database::current();
	if( db && (db->echoMode() & Database::EchoTransactions ) )
		LOG_3( "BEGIN;" );
	
	Locker locker(lock());
	return (mInsideTransaction = mDb.transaction());
}

bool QSqlDbConnection::commitTransaction()
{
	Database * db = Database::current();
	if( db && (db->echoMode() & Database::EchoTransactions ) )
		LOG_3( "COMMIT;" );
	mInsideTransaction = false;
	Locker locker(lock());
	return mDb.commit();
}

bool QSqlDbConnection::rollbackTransaction()
{
	Database * db = Database::current();
	if( db && (db->echoMode() & Database::EchoTransactions ) )
		LOG_3( "ROLLBACK;" );
	mInsideTransaction = false;
	Locker locker(lock());
	return mDb.rollback();
}

bool QSqlDbConnection::insideTransaction()
{
	return mInsideTransaction;
}
	
void QSqlDbConnection::listen( const QString & notificationName )
{
	//LOG_3( "Listening on notification: " + notificationName );
	Locker locker(lock());
	mDb.driver()->subscribeToNotification(notificationName);
}

void QSqlDbConnection::insert( Table * table, const RecordList & rl )
{
	if( rl.size() <= 0 )
		return;

	TableSchema * schema = table->schema();

	Capabilities caps = capabilities();
	bool multiInsert = caps & Cap_MultipleInsert;

	FieldList fields = schema->fields();
	FieldList literalsAndDefaults;
	
	// Generate the first half of the statement INSERT INTO table (col1, col2, col3) VALUES
	QString sql( "INSERT INTO " + tableQuoted(schema->tableName()) + " ( " );
	{
		QStringList cols;
		foreach( Field * f, fields )
			if( !f->flag(Field::LocalVariable ) )
				cols += f->name().toLower();
		sql += "\"" + cols.join( "\", \"" ) + "\" ) VALUES ";
	}

	// multiInsert means the database supports inserting multiple rows with on statement
	// INSERT INTO table (column1,column2) VALUES (data1,data2), (data11,data22);
	if( multiInsert ) {
		QStringList recVals;
		st_foreach( RecordIter, it, rl ) 
		{
			QStringList vals;
			RecordImp * rb = it.imp();
			foreach( Field * f, fields )
			{
				if( f->flag(Field::LocalVariable) ) continue;
				int pos = f->pos();
				// A deleted record wont have any columns marked as modified, so we'll commit them all
				if( (f->flag(Field::PrimaryKey) && rb->getColumn(f).toInt() != 0) || rb->isColumnModified( pos ) || rb->mState == RecordImp::COMMIT_ALL_FIELDS ) {
					if( rb->isColumnLiteral( pos ) ) {
						if( !literalsAndDefaults.contains(f) )
							literalsAndDefaults += f;
						vals += rb->getColumn( pos ).toString();
					} else {
						vals += "?";
					}
				} else {
					vals += "DEFAULT";
					if( !literalsAndDefaults.contains(f) )
						literalsAndDefaults += f;
				}
			}
			recVals +="( " + vals.join( "," ) + " ) ";
		}
		sql += recVals.join( ", " );

		// if we generated a new primary key, return it for the key cache
		sql += "RETURNING \"" + schema->primaryKey().toLower() + "\"";
		foreach( Field * f, literalsAndDefaults )
			sql += ", \"" + f->name().toLower() + "\"";
		sql += ";";
		
		VarList args;

		st_foreach( RecordIter, it, rl ) 
		{
			RecordImp * rb = it.imp();
			foreach( Field * f, fields ) {
				if( f->flag(Field::LocalVariable) ) continue;
				int pos = f->pos();
				if( !rb->isColumnLiteral( pos ) && ((f->flag( Field::PrimaryKey ) && rb->getColumn(f).toInt() != 0) || (rb->isColumnModified( pos ) || rb->mState == RecordImp::COMMIT_ALL_FIELDS)) ) {
					QVariant v = f->dbPrepare( rb->getColumn( f->pos() ) );
					// Null date if ISO can't produce a valid string
					if( v.type() == QVariant::Date && v.toDate().toString(Qt::ISODate).isEmpty() )
						v = QVariant(QVariant::Date);
					args += v;
				}
			}
		}
		QSqlQuery q = exec( sql, args, true /*retry*/, table );
		if( q.isActive() ) {
			if( q.size() != (int)rl.size() ) {
				LOG_1( "INSERT succeeded but rows returned(primary keys) doesnt match number of records inserted" );
				return;
			}
			
			uint i = 0;
			while( q.next() ) {
				Record r(rl[i]);
				r.imp()->values()->replace(schema->primaryKeyIndex(),q.value(0));
				int ri = 1;
				foreach( Field * f, literalsAndDefaults )
					r.imp()->values()->replace(f->pos(),q.value(ri++));
				i++;
			}
		}
	} else {
		st_foreach( RecordIter, it, rl ) 
		{
			TableSchema * base = schema;
			if( schema->inherits().size() )
				base = schema->inherits()[0];

			RecordImp * rb = (*it).imp();
			QString sql( "INSERT INTO %1 (" ), values( ") VALUES (" );
			sql = sql.arg( tableQuoted(schema->tableName()) );
			FieldList fields = schema->fields();
			QString returnValuesSql;

			bool nc = false;
			foreach( Field * f, fields )
			{
				if( f->flag(Field::LocalVariable) ) continue;
				
				int pos = f->pos();
				
				// If we are inserting into a child table and are on a database that doesn't
				// support native inheritance (likely everything but postgres), then we need to fetch
				// a primary key from the base table before the insert
				if( (base != schema) && !(caps & Cap_Inheritance) && f->flag(Field::PrimaryKey) && rb->getColumn(f).toInt() == 0 )
					rb->setColumn( f, newPrimaryKey(base) );
				
				if( (f->flag(Field::PrimaryKey) && rb->getColumn(f).toInt() != 0) || rb->isColumnModified( pos ) || rb->mState == RecordImp::COMMIT_ALL_FIELDS ) {
					if( nc ) { // Need a comma ?
						sql += ", ";
						values += ", ";
					}
					sql += "\"" + f->name().toLower() + "\"";
					if( rb->isColumnLiteral( pos ) ) {
						if( !literalsAndDefaults.contains(f) )
							literalsAndDefaults += f;
						values += prepareColumnLiteral(rb->getColumn( pos ).toString());
					} else {
						values += f->placeholder();
					}
					nc = true;
				} else {
					if( !literalsAndDefaults.contains(f) )
						literalsAndDefaults += f;
				}
			}
		
			if( values.isEmpty() ) continue;
		
			sql = sql + values + ")";
		
			if( caps & Cap_Returning ) {
				sql += " RETURNING \"" + schema->primaryKey() + "\"";
				foreach( Field * f, literalsAndDefaults )
					sql += ", \"" + f->name().toLower() + "\"";
				sql += ";";
			} else {
				returnValuesSql = "SELECT " + schema->primaryKey();
				foreach( Field * f, literalsAndDefaults )
					returnValuesSql += ", \"" + f->name().toLower() + "\"";
				returnValuesSql += " FROM " + tableQuoted(schema->tableName()) + " WHERE " + schema->primaryKey() + "=" + lastInsertPrimaryKey(base);
				if( caps & Cap_MultipleStatement )
					sql += "; " + returnValuesSql;
			}
			QSqlQuery q = fakePrepare(sql);
		
			foreach( Field * f, fields ) {
				if( f->flag(Field::LocalVariable) ) continue;

				int pos = f->pos();
				if( !rb->isColumnLiteral( pos ) && ((f->flag( Field::PrimaryKey ) && rb->getColumn(f).toInt() != 0) || (rb->isColumnModified( pos ) || rb->mState == RecordImp::COMMIT_ALL_FIELDS)) ) {
					QVariant v = f->dbPrepare( rb->getColumn( pos ) );
					q.bindValue( f->placeholder(), v );
				}
			}
		
			if( exec( q, true /*retry*/, table, /* Using fake prepare */ true ) ) {
				if( !returnValuesSql.isEmpty() )
					q = exec( returnValuesSql );
				if( q.next() ) {
	//				qWarning( "Table::insert: Setting primary key to " + QString::number( q.value(0).toUInt() ) );
					rb->values()->replace(schema->primaryKeyIndex(),q.value(0));
					int ri = 1;
					foreach( Field * f, literalsAndDefaults )
						rb->values()->replace(f->pos(),q.value(ri++));
				} else {
					LOG_1( "Table::insert: Failed to get primary key after insert" );
					return;
				}
			}
		}

	}
	
	return;
}

bool QSqlDbConnection::update( Table * table, RecordImp * imp, Record * returnValues )
{
	TableSchema * schema = table->schema();
	bool needComma = false;
	FieldList fields = schema->fields();
	
	QString up("UPDATE %1 SET "), returnValuesSql;
	up = up.arg( tableQuoted(schema->tableName()) );
	for( FieldIter it = fields.begin(); it != fields.end(); ++it ){
		Field * f = *it;
		// if the field is the primary key we don't want to change it
		// or if the field hasn't been modified locally, don't change it
		if( f->flag( Field::PrimaryKey | Field::LocalVariable ) || !imp->isColumnModified( f->pos() ) )
			continue;
		if( needComma ) up += ", ";

		// column name is wrapped in quotes
		up += "\"" + f->name().toLower() + "\"";

		// if we're updating to a literal value ( such as NOW() or something )
		// put that value in, otherwise put in a bind marker
		if( imp->isColumnLiteral( f->pos() ) ) {
			up += "=" + prepareColumnLiteral(imp->getColumn( f->pos() ).toString());
		} else
			up += "=" + f->placeholder();
		needComma = true;
	}
	
	// There were no columns to update
	if( !needComma ) {
		//qWarning( "Table::update: Record had MODIFIED field set, but no modified columns" );
		return false;
	}
		
	up += QString(" WHERE ") + schema->primaryKey() + "=:primarykey"; // + QString::number( imp->key() );

	if( returnValues ) {
		returnValuesSql = getSqlFields(schema);
		if(capabilities() & Cap_Returning) {
			up += " RETURNING " + returnValuesSql;
			returnValuesSql.clear();
		} else
			returnValuesSql = "SELECT " + returnValuesSql + " FROM " + tableQuoted(schema->tableName()) + " WHERE " + schema->primaryKey() + "=" + QString::number(imp->key());
	}
	
	QSqlQuery q = fakePrepare( up );
	for( FieldIter it = fields.begin(); it != fields.end(); ++it ){
		Field * f = *it;
		if( f->flag( Field::PrimaryKey | Field::LocalVariable ) || !imp->isColumnModified( f->pos() ) || imp->isColumnLiteral( f->pos() ) )
			continue;
		QVariant var = f->dbPrepare( imp->getColumn(f->pos()) );
		q.bindValue( f->placeholder(), var );
	}
    q.bindValue( ":primarykey", imp->key() );

	if( exec( q, true /*retryLostConn*/, table, /*usingFakePrepare =*/ true ) ) {
		if( !returnValuesSql.isEmpty() )
			q = exec( returnValuesSql );
		if( returnValues && q.next() )
			*returnValues = Record( new RecordImp( table, q ), false );
		return true;
	}
	return false;
}


} //namespace

