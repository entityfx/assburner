
/*
 *
 * Copyright 2010 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id: SqliteConnection.cpp 8853 2009-09-30 21:43:23Z newellm $
 */

#include <qsqlerror.h>
#include <qsqlquery.h>
#include <qregexp.h>

#include "sqliteconnection.h"
#include "table.h"
#include "tableschema.h"

static const char * reservedNames [] = { "date", "time", "timestamp", "union", "unique", "table", "sum", "select", "returning", "primary", "over", "overlaps", "or", "order", "on", "only", "not", "natural", "limit", "like", "join", "into", "intersect", "in", "ilike", "group", "full", "freeze", "for", "foreign", "fetch", "except", "else", "do", "distinct", "default", "create", "constraint", "column", "collate", "check", "case", "cast", "both", "binary", "array", "all", "any", "and", "analyze", "true", "false", "to", "using", "user", "where", "when", "reserved", "offset", 0 };

static QHash<uint,QString> * sReservedNamesHash = 0;
SqliteConnection::SqliteConnection()
: QSqlDbConnection( "QSQLITE" )
{
	if( !sReservedNamesHash ) {
		sReservedNamesHash = new QHash<uint,QString>();
		addReservedNames( sReservedNamesHash, reservedNames );
	}
	mReservedNames = sReservedNamesHash;
}

Connection::Capabilities SqliteConnection::capabilities() const
{
	Connection::Capabilities ret = static_cast<Connection::Capabilities>(QSqlDbConnection::capabilities()); // No easy way to get the id's back | Cap_MultipleInsert);
	return ret;
}

QSqlQuery SqliteConnection::fakePrepare( const QString & sql )
{
	checkConnection();
	QSqlQuery q(mDb);
	q.prepare(sql);
	return q;
}

QVariant SqliteConnection::prepareValue( const QVariant & v )
{
	if( v.type() == QVariant::Bool )
		return v.toBool() ? 1 : 0;
	return v;
}

uint SqliteConnection::newPrimaryKey( TableSchema * schema )
{
	while( schema->parent() ) schema = schema->parent();
	QString name = schema->tableName().toLower();
	uint ret = 0;
	beginTransaction();
	QSqlQuery q = exec( "SELECT seq FROM sqlite_sequence WHERE name='" + name + "'" );
	if(  q.next() )
		ret = q.value(0).toUInt();
	exec( "UPDATE sqlite_sequence SET seq=seq+1 WHERE name='" + name + "'" );
	commitTransaction();
	return ret+1;
}

QString SqliteConnection::prepareColumnLiteral( const QString & cl )
{
	QString ret(cl);
	return ret.replace("now()", "date('now')",Qt::CaseInsensitive);
}

static QString warnAndRet( const QString & s ) { LOG_3( s ); return s + "\n"; }

bool SqliteConnection::tableExists( TableSchema * schema )
{
	return exec("SELECT * FROM sqlite_master where name='" + schema->tableName().toLower() + "'").next();
}

bool SqliteConnection::verifyTable( TableSchema * schema, bool createMissingColumns, QString * output )
{
	bool success = true;
	QString out;
	QSqlQuery q = exec("SELECT * FROM sqlite_master where name='" + schema->tableName().toLower() + "'");
	if( q.next() ) {
		FieldList fields = schema->fields();
		QString sql = q.value(4).toString();
		int colIdx = sql.indexOf('(');
		sql = sql.mid(colIdx+1,sql.size()-colIdx-2);
		LOG_1(sql);
		QStringList cols = sql.split(',');
		foreach( QString col, cols ) {
			col = col.trimmed();
			col = col.mid(0,col.indexOf(' '));
			if( col[0] == '"' )
				col = col.mid(1,col.size()-2);
			LOG_1( col );
			Field * f = schema->field(col);
			if( f )
				fields.removeAll(f);
		}
		QStringList missing,created;
		foreach( Field * f, fields )
			missing += f->name();
		
		if( fields.size() )
			out += warnAndRet( "Couldn't find the following columns for " + schema->tableName() + ": " + missing.join(",") ) + "\n";

		if( createMissingColumns ) {
			foreach( Field * f, fields ) {
				QString cre = "ALTER TABLE " + tableQuoted(schema->tableName()) + " ADD COLUMN \"" + f->name() + "\" " + f->dbTypeString();
				exec(cre);
				created.append(cre);
			}
		} else
			success = false;
		out += created.join("\n");
	} else {
		success = false;
		out = "Table not found: " + schema->tableName() + "\n";
	}
	if( output )
		*output = out;
	return success;
}

bool SqliteConnection::createTable( TableSchema * schema, QString * output )
{
	QString out;
	bool ret = false;
	QString cre("CREATE TABLE ");
	cre += schema->tableName().toLower() + "  (";
	QStringList columns;
	FieldList fl = schema->fields();
	foreach( Field * f, fl ) {
		QString ct("\"" + f->name().toLower() + "\"");
		if( f->flag( Field::PrimaryKey ) ) {
			ct += " INTEGER PRIMARY KEY AUTOINCREMENT";
			columns.push_front( ct );
		} else {
			ct += " " + f->dbTypeString();
			columns += ct;
		}
	}
	cre += columns.join(",") + ")";
	cre += ";";
	out += warnAndRet( "Creating table: " + cre );
	
	QSqlQuery query = exec( cre );
	if( !query.isActive() ){
		out += warnAndRet( "Unable to create table: " + schema->tableName() );
		out += warnAndRet( "Error was: " + query.lastError().text() );
		goto OUT;
	}
	
	ret = true;
	OUT:
	if( output )
		*output = out;
	return ret;
}

QString SqliteConnection::generateSelectSql( TableSchema * schema )
{
	return "SELECT " + getSqlFields(schema) + " FROM " + tableQuoted(schema->tableName());
}

RecordList SqliteConnection::selectOnly( Table * table, const QString & where, const QList<QVariant> & args )
{
	TableSchema * schema = table->schema();
	RecordList ret;
	
	QString select( generateSelectSql(schema) ), w(where);

	if( !w.isEmpty() ) {
		if( !w.contains("WHERE") )
			select += " WHERE";
		select += " " + w;
	}

	select += ";";

	QSqlQuery sq = exec( select, args, true, table );
	while( sq.next() )
		ret += Record( new RecordImp( table, sq ), false );
	return ret;
}

void SqliteConnection::selectFields( Table * table, RecordList records, FieldList fields )
{
	TableSchema * schema = table->schema();
	RecordList ret;
	
	QStringList fieldNames;
	foreach( Field * f, fields )
		if( !f->flag( Field::LocalVariable ) )
			fieldNames += f->name().toLower();

	QString tableName = tableQuoted(schema->tableName());
	QString sql = "SELECT " + tableName + ".\"" + fieldNames.join( "\", " + tableName + ".\"" ) + "\"" + " FROM " + tableName + " WHERE " + schema->primaryKey() + " IN (" + records.keyString() + ");";

	QSqlQuery sq = exec( sql, VarList(), true /*retry*/, table );
	RecordIter it = records.begin();
	while( sq.next() ) {
		if( it == records.end() ) break;
		int i = 0;
		Record r(*it);
		foreach( Field * f, fields )
			r.imp()->fillColumn( f->pos(), sq.value(i++) );
	}
}

QList<RecordList> SqliteConnection::joinedSelect( const JoinedSelect & joined, QString where, QList<QVariant> args )
{
	Schema * schema = joined.table()->schema()->schema();
	Database * db = joined.table()->database();
	QList<RecordList> ret;
	QList<JoinCondition> joinConditions = joined.joinConditions();
	QList<Table*> tables;
	QList<FieldList> fieldsPerTable;
	QList<int> pkeyPositions;
	
	tables += joined.table();
	QString query( "SELECT " );
	// Generate column list
	QStringList fields;

	{
		// Add initial table
		ret += RecordList();
		int pkeyPos = -1;
		FieldList fl;
		fields += getSqlFields(joined.table()->schema(), joined.alias(), !joined.joinOnly(), &fl, &pkeyPos);
		fieldsPerTable += fl;
		pkeyPositions += pkeyPos;
	}
	
	foreach( JoinCondition jc, joinConditions ) {
		if( jc.ignoreResults )
			continue;
		tables += jc.table;
		ret += RecordList();
		FieldList fl;
		int pkeyPos = -1;
		fields += getSqlFields(jc.table->schema(), jc.alias, !jc.joinOnly, &fl, &pkeyPos);
		fieldsPerTable += fl;
		pkeyPositions += pkeyPos;
		fl.clear();
	}
	
	query += fields.join(", ");
	query += " FROM ";
	query += tableQuoted(joined.table()->schema()->tableName());
	
	// Generate join list
	int idx = 0;
	foreach( JoinCondition jc, joinConditions ) {
		switch( jc.type ) {
			case InnerJoin:
				query += " INNER";
				break;
			case LeftJoin:
				query += " LEFT";
				break;
			case RightJoin:
				query += " RIGHT";
				break;
			case OuterJoin:
				query += " OUTER";
				break;
		}
		query += " JOIN ";
		query += tableQuoted(jc.table->schema()->tableName());
		if( !jc.alias.isEmpty() && jc.alias != jc.table->schema()->tableName() )
			query += " AS " + jc.alias;

		query += " ON " + jc.condition;
		++idx;
	}
	
	query += prepareWhereClause(where) + ";";
	
	QSqlQuery sq = exec( query, args, true /*retry*/, tables[0] );
	
	while( sq.next() ) {
		int tableIndex = 0;
		int colOffset = 0;
		foreach( Table * table, tables ) {
			Table * destTable = table;
			
			if( destTable ) {
				int pkeyPos = pkeyPositions[tableIndex];
				if( sq.value(colOffset + pkeyPos).toInt() != 0 )
					ret[tableIndex] += Record( new RecordImp( destTable, sq, colOffset, &fieldsPerTable[tableIndex] ), false );
				else
					ret[tableIndex] += Record(table);
			}
			
			colOffset += fieldsPerTable[tableIndex].size();
			
			++tableIndex;
		}
	}
	return ret;
}

QString SqliteConnection::lastInsertPrimaryKey( TableSchema * tableSchema )
{
	return "last_insert_rowid()";
}

int SqliteConnection::remove( Table * table, const QString & keys, QList<int> * )
{
	TableSchema * schema = table->schema();
	TableSchema * base = schema;
	if( schema->inherits().size() )
		base = schema->inherits()[0];
	QString del("DELETE FROM ");
	del += tableQuoted(base->tableName());
	del += " WHERE ";
	del += base->primaryKey();
	del += " IN(" + keys + ");";
	QSqlQuery q = exec( del, QList<QVariant>(), true /*retryLostConn*/, table );
	if( !q.isActive() ) return -1;
	return q.numRowsAffected();
}

bool SqliteConnection::createIndex( IndexSchema * schema )
{
	QString cmd;
	QStringList cols;
	foreach( Field * f, schema->columns() )
		if( !f->flag( Field::LocalVariable ) )
			cols += f->name();
	cmd += "CREATE INDEX " + schema->name();
	cmd += " ON " + tableQuoted(schema->table()->tableName()) + "(" + cols.join(",") + ")";
	if( !schema->databaseWhere().isEmpty() )
		cmd += " WHERE " + schema->databaseWhere();
	cmd += ";";

	return exec( cmd ).isActive();
}

