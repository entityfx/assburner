
#include <qstringlist.h>

#include "blurqt.h"
#include "stringmap.h"

StringMap::StringMap()
: Base() {}

StringMap::StringMap(const QMap<QString,QString> & o)
: Base(o) {}

StringMap::~StringMap()
{}

StringMap StringMap::fromString( const QString & s )
{
	enum State { Outside, Key, Between, Value };
	static char s_quote = '"', s_escape='\\';
	State state = Outside;
	int start = 0;
	bool escaped = false;
	QString key;
	StringMap ret;
	for( int i=0, e=s.size(); i<e; ++i ) {
		QChar c = s[i];
		switch(state) {
			case Outside:
				if( c==s_quote ) {
					start = i+1;
					state = Key;
				}
				break;
			case Key:
				if( !escaped && c==s_quote ) {
					key = s.mid(start,i-start);
					state = Between;
					start = i+1;
					escaped = false;
				} else if( c == s_escape )
					escaped = !escaped;
				else
					escaped = false;
				break;
			case Between:
				if( c == s_quote ) {
					start = i+1;
					state = Value;
				}
				if( (c == 'N' || c == 'n') && s.mid(i,4).toLower() == "null" ) {
					ret.insert( key, QString() );
					state = Outside;
					i += 3;
					start = i+1;
				}
				break;
			case Value:
				if( !escaped && c==s_quote ) {
					QString val = s.mid(start,i-start);
					ret.insert( key, val );
					state = Outside;
					start = i+1;
				} else if( c == s_escape )
					escaped = !escaped;
				else
					escaped = false;
				break;
		}
	}
	return ret;
}

static QString quoteAndEscape( const QString & s, bool canBeNull )
{ 
	static QString s_null("NULL");
	return s.isNull() ? s_null : "\"" + QString(s).replace("\"","\\\"") + "\""; }

QString StringMap::toString() const
{
	QStringList ret;
	for( StringMap::const_iterator it = begin(), e = end(); it != e; ++it )
		ret.append( quoteAndEscape(it.key(),false) + "=>" + quoteAndEscape(it.value(),true) );
	return ret.join(", ");
}

