
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of libstone.
 *
 * libstone is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * libstone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libstone; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qfile.h>
#include <qtextstream.h>
#include <qstringlist.h>
#include <qlist.h>

#include "blurqt.h"
#include "iniconfig.h"

#ifdef Q_OS_WIN
#include "windows.h"
#endif

typedef QMap<QString,QString> StringMap;
typedef QMap<QString,StringMap> StringStringMap;

IniConfig::IniConfig(QString configFile)
: mPrivate( new IniPrivate )
, mNeedSave( false )
{
	mPrivate->mFile = configFile;
	readFromFile();
}

IniConfig::IniConfig()
: mPrivate( new IniPrivate )
, mNeedSave( false )
{
}

IniConfig::~IniConfig()
{
	if( mNeedSave )
		writeToFile();
}

void IniConfig::setFileName(const QString & fileName)
{
	QWriteLocker wl(&mPrivate->mLock);
	mPrivate->mFile = fileName;
}

void IniConfig::readFromFile( const QString & fileName, bool overwriteExisting )
{
	QWriteLocker wl(&mPrivate->mLock);
	QString file = fileName;
	if( file.isEmpty() )
		file = mPrivate->mFile;

	if( !QFile::exists(file) ) {
		wl.unlock();
		LOG_5( "Config file doesn't exist at: " + file );
		return;
	}
	
	QFile cfgFile(file);
	if( !cfgFile.open(QIODevice::ReadOnly) ) {
		wl.unlock();
		LOG_5( "Unable to open config file for reading at: " + fileName );
		return;
	}
	
	QString section;
	QTextStream in(&cfgFile);
	const static QString no_section("NO_SECTION");
	while(!in.atEnd()){
		QString l = in.readLine();
		if( l.startsWith('[') && l.endsWith(']') ){
			section = l.mid(1,l.length()-2);
			if( section == no_section )
				section = QString::null;
			continue;
		}
		int equalsPos = l.indexOf('=');
		if( equalsPos < 0 )
			continue;
		QString key = l.left(equalsPos);
		QString val = l.mid(equalsPos+1);
		if( key.isEmpty() )
			continue;

		if( overwriteExisting || !mPrivate->mValMap[section].contains( key ) ) {
			mPrivate->mValMap[section][key] = val;
		}
	}
	cfgFile.close();
}

bool IniConfig::writeToFile( const QString & fileName )
{
	if( !mSectionStack.isEmpty() ) {
		mNeedSave = true;
		return false;
	}
	
	mNeedSave = false;
	
	QWriteLocker wl(&mPrivate->mLock);
	QString filePath = fileName;
	if( filePath.isEmpty() )
		filePath = mPrivate->mFile;

	QFile file(filePath);
	if( !file.open(QIODevice::WriteOnly) ) {
		wl.unlock();
		LOG_1( "Unable to open config file for writing at: " + filePath );
		return false;
	}
	QTextStream out(&file);
	StringStringMap::const_iterator it;
	for(it = mPrivate->mValMap.constBegin(); it != mPrivate->mValMap.constEnd(); ++it)
	{
		const StringMap & map = it.value();
		const QString & section = it.key();
		if( !map.isEmpty() ){
			out << '[' << (section.isEmpty() ? QString("NO_SECTION") : section) << ']' << '\n';
			for( StringMap::const_iterator valit = map.constBegin(); valit!=map.constEnd(); ++valit )
				out << valit.key() << '=' << valit.value() << '\n';
			out << "\n";
		}
	}
	wl.unlock();
	LOG_3( "Wrote config file to: " + filePath );
	return true;
}

void IniConfig::setSection( const QString & section )
{
	mSection = section;
}

void IniConfig::pushSection( const QString & section )
{
	mSectionStack.push_back( mSection );
	mSection = section;
}

void IniConfig::pushSubSection( const QString & subsection )
{
	pushSection( mSection.isEmpty() ? subsection : (mSection + ":" + subsection) );
}

QString IniConfig::currentSection() const
{
	return mSection;
}

void IniConfig::popSection()
{
	if( mSectionStack.size() ) {
		mSection = mSectionStack.back();
		mSectionStack.pop_back();
	}
	if( !mSectionStack.size() && mNeedSave ) {
		writeToFile();
	}
}

QStringList IniConfig::sections() const
{
	return mPrivate->mValMap.keys();
}

QStringList IniConfig::keys( const QRegExp & regEx ) const
{
	QStringList ret = mPrivate->mValMap[mSection].keys();
	if( !regEx.isEmpty() ) {
		QStringList ret2;
		foreach( QString it, ret )
			if( regEx.exactMatch( it ) )
				ret2 += it;
		return ret2;
	}
	return ret;
}

// Returns (value, valueExisted)
std::pair<QVariant,bool> IniConfig::_readValue( const QString & key ) const
{
	{
		QReadLocker rl(&mPrivate->mLock);
		StringStringMap::const_iterator sec_it = mPrivate->mValMap.constFind(mSection);
		if( sec_it != mPrivate->mValMap.constEnd() ) {
			const StringMap & map = sec_it.value();
			StringMap::const_iterator it = map.constFind(key);
			if( it != map.constEnd() )
				return std::pair<QVariant,bool>(QVariant(it.value()),true);
		}
	}
	return std::pair<QVariant,bool>(QVariant(),false);
}

template<typename T> QVariant encode( const T & t )
{ return qVariantFromValue<T>(t); }

template<typename T> T decode( const QVariant & v )
{ return qvariant_cast<T>(v); }

template<typename RET> RET IniConfig::defaultOrDecode(const QString & key, const RET & def) const
{
	std::pair<QVariant,bool> ret = _readValue(key);
	if( !ret.second ) {
		if( true ) //mWriteDefaults )
			writeValue(key,encode<RET>(def));
		return def;
	}
	return decode<RET>(ret.first);
}

template<> QVariant encode<QVariant>(const QVariant & v)
{ return v; }

template<> QVariant decode<QVariant>(const QVariant & v)
{ return v; }

QVariant IniConfig::readValue( const QString & key, const QVariant & def ) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<bool>( const bool & b )
{
	return b ? "true" : "false";
}

template<> bool decode<bool>( const QVariant & orig )
{
	QString val = orig.toString().toLower();
	return ( val == "y" || val == "1" || val == "true" || val == "t" );
}

bool IniConfig::readBool(const QString & key, bool def) const
{
	return defaultOrDecode(key,def);
}

int IniConfig::readInt(const QString & key, int def) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QString>( const QString & orig )
{
	QString result = orig;
	result.replace( '\n', "\\n" );
	return result;
}

template<> QString decode<QString>( const QVariant & orig )
{
	QString result = orig.toString();
	result.replace( "\\n", "\n" );
	return result;
}

QString IniConfig::readString(const QString & key, const QString & def) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QColor>( const QColor & def )
{
	return def.isValid() ? def.name() : "";
}

template<> QColor decode<QColor>( const QVariant & v )
{
	QColor ret;
	ret.setNamedColor(v.toString());
	return ret;
}

QColor IniConfig::readColor(const QString & key, const QColor & def) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QFont>( const QFont & def )
{
	return def.toString();
}

template<> QFont decode<QFont>( const QVariant & v )
{
	QFont ret;
	ret.fromString(v.toString());
	return ret;
}

QFont IniConfig::readFont(const QString & key, const QFont & def) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QSize>( const QSize & def )
{
	return def.isValid() ? QString( "%1,%2" ).arg( def.width() ).arg( def.height() ) : "";
}

template<> QSize decode<QSize>( const QVariant & v )
{
	QStringList sl(v.toString().split(','));
	return sl.size() == 2 ? QSize( sl[0].toInt(), sl[1].toInt() ) : QSize();
}

QSize IniConfig::readSize( const QString & key, const QSize & def ) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QRect>( const QRect & val )
{
	return val.isValid() ? QString( "%1,%2,%3,%4" ).arg(val.x()).arg(val.y()).arg(val.width()).arg(val.height()) : "";
}

template<> QRect decode<QRect>( const QVariant & v )
{
	QStringList sl(v.toString().split(','));
	return sl.size() == 4 ? QRect( sl[0].toInt(), sl[1].toInt(), sl[2].toInt(), sl[3].toInt() ) : QRect();
}

QRect IniConfig::readRect( const QString & key, const QRect & def ) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QList<int> >( const QList<int> & val )
{
	QStringList tmp;
	foreach( int i, val )
		tmp.append(QString::number(i));
	return tmp.join(",");
}

template<> QList<int> decode<QList<int> >( const QVariant & v )
{
	QStringList sl(v.toString().split(','));
	QList<int> ret;
	foreach( QString s, sl ) {
		bool okay;
		int i = s.toInt(&okay);
		if( okay ) ret << i;
	}
	return ret;
}

QList<int> IniConfig::readIntList( const QString & key, const QList<int> & def ) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QList<uint> >( const QList<uint> & val )
{
	QStringList tmp;
	foreach( uint i, val )
		tmp.append(QString::number(i));
	return tmp.join(",");
}

template<> QList<uint> decode<QList<uint> >( const QVariant & v )
{
	QStringList sl(v.toString().split(','));
	QList<uint> ret;
	foreach( QString s, sl ) {
		bool okay;
		uint i = s.toUInt(&okay);
		if( okay ) ret << i;
	}
	return ret;
}

QList<uint> IniConfig::readUIntList( const QString & key, const QList<uint> & def ) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QByteArray>( const QByteArray & val )
{
	return QVariant( QString(val.toBase64()) );
}

template<> QByteArray decode<QByteArray>( const QVariant & v )
{
	QByteArray base64;
	base64.append( v.toString() );
	return QByteArray::fromBase64(base64);
}

QByteArray IniConfig::readByteArray( const QString & key, const QByteArray & def ) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<QDateTime>( const QDateTime & val )
{
	return val.toString();
}

template<> QDateTime decode<QDateTime>( const QVariant & v )
{
	return QDateTime::fromString(v.toString());
}

QDateTime IniConfig::readDateTime( const QString & key, const QDateTime & def ) const
{
	return defaultOrDecode(key,def);
}

template<> QVariant encode<Interval>( const Interval & val )
{
	return val.toString();
}

template<> Interval decode<Interval>( const QVariant & v )
{
	return Interval::fromString(v.toString());
}

Interval IniConfig::readInterval( const QString & key, const Interval & def ) const
{
	return defaultOrDecode(key,def);
}

void IniConfig::writeValue(const QString & key, const QVariant & val) const
{
	mPrivate->mValMap[mSection][key] = val.toString();
}

void IniConfig::clear( bool sectionOnly )
{
	QWriteLocker(&mPrivate->mLock);
	if( sectionOnly )
		mPrivate->mValMap[mSection].clear();
	else
		mPrivate->mValMap.clear();
}

void IniConfig::writeBool(const QString & key, bool val)
{
	writeValue(key,encode(val));
}

void IniConfig::writeInt(const QString & key, int val)
{
	writeValue(key,encode(val));
}

void IniConfig::writeString(const QString & key, const QString & val)
{
	writeValue(key,encode(val));
}

void IniConfig::writeColor(const QString & key, const QColor & val)
{
	writeValue(key,encode(val));
}

void IniConfig::writeFont(const QString & key, const QFont & val)
{
	writeValue(key,encode(val));
}

void IniConfig::writeSize( const QString & key, const QSize & val )
{
	writeValue(key,encode(val));
}

void IniConfig::writeRect( const QString & key, const QRect & val )
{
	writeValue(key,encode(val));
}

void IniConfig::writeIntList( const QString & key, const QList<int> & val )
{
	writeValue(key,encode(val));
}

void IniConfig::writeUIntList( const QString & key, const QList<uint> & val )
{
	writeValue(key,encode(val));
}

void IniConfig::writeByteArray( const QString & key, const QByteArray & val )
{
	writeValue(key,encode(val));
}

void IniConfig::writeDateTime( const QString & key, const QDateTime & val )
{
	writeValue(key,encode(val));
}

void IniConfig::writeInterval( const QString & key, const Interval & val )
{
	writeValue(key,encode(val));
}

void IniConfig::removeSection( const QString & group )
{
	QWriteLocker wl(&mPrivate->mLock);
	mPrivate->mValMap.remove( group );
}

void IniConfig::renameSection( const QString & before, const QString & after )
{
	QWriteLocker wl(&mPrivate->mLock);
	QMap<QString,QString> section;
	if( mPrivate->mValMap.contains( before ) ) {
		section = mPrivate->mValMap[before];
		removeSection(before);
		mPrivate->mValMap[after] = section;
	}
}

void IniConfig::copySection( const QString & sectionFrom, const QString & sectionTo, bool clearExisting )
{
	QWriteLocker wl(&mPrivate->mLock);
	QMap<QString,QString> section;
	if( mPrivate->mValMap.contains( sectionFrom ) ) {
		section = mPrivate->mValMap[sectionFrom];
		if( clearExisting )
			removeSection( sectionTo );
		foreach( QString key, section.keys() )
			mPrivate->mValMap[sectionTo][key] = section[key];
	}
}

void IniConfig::removeKey( const QString & key )
{
	QWriteLocker wl(&mPrivate->mLock);
	mPrivate->mValMap[mSection].remove( key );
}
