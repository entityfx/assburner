
Updates requiring selects due to lack of returning capabilities should process the changes completely through the recordUpdated paths
instead of the recordsIncoming paths. Do we even hit this path with postgres?

post update triggers should be able to use changeString/isUpdated

More tests and documentation!
