
QString fieldToDisplayName( const QString & name );
QString pluralizeName( const QString & name );

class Field
{
%TypeHeaderCode
#include <field.h>
%End

public:
	enum Type {
		Invalid=0,
		String,
		UInt,
		ULongLong,
		Int,
		Date,
		DateTime,
		Interval,
		Double,
		Float,
		Bool,
		ByteArray,
		Color,
		Image,
		Time,
		Json,
		StringMap,
		StringArray,
		IntArray
	};

	enum Flags {
		None = 0,
		PrimaryKey = 1,
		ForeignKey = 2,
		NotNull = 4,
		Unique = 8,
		LocalVariable = 16,
		ReverseAccess = 32,
		TableDisplayName = 64,
		NoDefaultSelect = 128,
		DefaultLookup = 256,
		LastFlag = 256
	};

	enum {
		DoNothingOnDelete,
		UpdateOnDelete,
		CascadeOnDelete
	};

	Field( TableSchema * table /TransferThis/, const QString & name, Field::Type type = Field::Int, Field::Flags flags = Field::None, const QString & method = QString() );
	Field( TableSchema * table /TransferThis/, const QString & name, const QString & fkeyTable, Field::Flags flags = Field::ForeignKey, bool index = false, int indexDeleteMode=DoNothingOnDelete );

	TableSchema * table() const;
	QString name() const;
	void setName( const QString & );

	Type type() const;
	void setType( Type type );

	QString methodName() const;
	void setMethodName( const QString & );

	QString generatedPluralMethodName() const;
	QString pluralMethodName() const;
	void setPluralMethodName( const QString & );

	QString generatedDisplayName() const;
	QString displayName() const;
	void setDisplayName( const QString & );

	Flags flags() const;
	bool setFlags( Flags f );
	bool flag( int f ) const;
	void setFlag( int f, bool );
	QString flagString() const;

	QString docs() const;
	void setDocs( const QString & docs );

	int pos() const;
	void setPos( int pos );

	QVariant defaultValue() const;
	void setDefaultValue( const QVariant & );
	QString defaultValueString() const;

	TableSchema * foreignKeyTable() const;
	void setForeignKey( const QString & tableName );
	QString foreignKey() const;
	
	bool hasIndex() const;
	IndexSchema * index();
	void setHasIndex( bool, int indexDeleteMode = DoNothingOnDelete );
	int indexDeleteMode() const;
	QString indexDeleteModeString() const;
	static int indexDeleteModeFromString( const QString & );
	
	static Type stringToType( const QString & );
	static QVariant variantFromString( const QString &, Type );

	void coerce( QVariant & v );

	QVariant dbPrepare( const QVariant & v );

	QString typeString() const;
	QString variantTypeString() const;
	QString listTypeString() const;
	
	QString dbTypeString() const;

	int qvariantType() const;
	static int qvariantType(Field::Type);

	uint fieldNameHash() const;
};

%MappedType FieldArg /NoRelease/
{
%TypeHeaderCode
#include "field.h"
typedef Field FieldArg;
%End

%ConvertToTypeCode
	if( sipCanConvertToType(sipPy, sipType_Field, 0) ) {
		if( sipIsErr == NULL )
			return 1;
		int state, err = 0;
		Field * f = (Field*)sipConvertToType( sipPy, sipType_Field, 0, 0, &state, &err );
		if( f && !err ) {
			*sipCppPtr = f;
			sipReleaseType( f, sipType_Field, state );
		}
	}
	else if( sipCanConvertToType(sipPy, sipType_FieldExpression, 0) ) {
		if( sipIsErr == NULL )
			return 1;
		int state, err = 0;
		FieldExpression * fe = (FieldExpression*)sipConvertToType( sipPy, sipType_FieldExpression, 0, 0, &state, &err );
		if( fe && !err ) {
			*sipCppPtr = static_cast<Field*>(*fe);
			sipReleaseType( fe, sipType_FieldExpression, state );
		}
	} else {
		if( sipIsErr == NULL )
			return 1;
		return 0;
	}

	return sipGetState(sipTransferObj);
%End

%ConvertFromTypeCode
	return 0;
%End
};

%MappedType FieldList
{
%TypeHeaderCode
#include "field.h"
#include "pyembed.h"
#define FieldList QList<Field*>
%End

%ConvertToTypeCode
	QList<Field*> * qvList = 0;

	if( !PySequence_Check(sipPy) ) {
		if( sipIsErr == NULL )
			return 0;
		*sipIsErr = true;
		return 0;
	}

	if( sipIsErr != NULL ) {
		qvList = new QList<Field*>();
		*sipCppPtr = qvList;
	}
	
	SIP_SSIZE_T end = PySequence_Size(sipPy);
	for( SIP_SSIZE_T i = 0; i < end; i++ ) {
		PyObject * ob = PySequence_GetItem(sipPy,i);
		bool validObject = true;
		if( ob == Py_None ) {
			if( sipIsErr != NULL ) {
				qvList->append(0);
			}
		}else if (sipCanConvertToType(ob, sipType_Field, 0)) {
			// Type Check
			if( sipIsErr != NULL ) {
				int state, err = 0;
				Field * f = (Field*)sipConvertToType( ob, sipType_Field, 0, 0, &state, &err );
				if( f && !err ) {
					qvList->append( f );
					sipReleaseType( f, sipType_Field, state );
				}
			}
		} else if(sipCanConvertToType(ob, sipType_FieldExpression, 0)) {
			// Type Check
			if( sipIsErr != NULL ) {
				int state, err = 0;
				FieldExpression * fe = (FieldExpression*)sipConvertToType( ob, sipType_FieldExpression, 0, 0, &state, &err );
				if( fe && !err ) {
					qvList->append((Field*)(*fe));
					sipReleaseType( fe, sipType_FieldExpression, state );
				}
			}
		} else
			validObject = false;
		Py_DECREF(ob);
		if( !validObject )
			return 0;
	}
	if( sipIsErr == NULL )
		return 1;

	return sipGetState(sipTransferObj);
%End
%ConvertFromTypeCode
	return 0;
%End
};
