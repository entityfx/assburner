
class QJsonDocument {
%TypeHeaderCode
#include <qjsondocument.h>
%End

public:
	enum DataValidation {
		Validate         = 0,
		BypassValidation = 1
	};

	enum JsonFormat {
		Indented,
		Compact
	};

	QJsonDocument();
	QJsonDocument(const QJsonObject &object);
	QJsonDocument(const QJsonArray &array);
	QJsonDocument(const QJsonDocument &other);
	~QJsonDocument();

	bool operator!=(const QJsonDocument &other) const;
	bool operator==(const QJsonDocument &other) const;

	bool isArray() const;
	bool isEmpty() const;
	bool isNull() const;
	bool isObject() const;

	QByteArray toBinaryData() const;
	QByteArray toJson(JsonFormat format = Indented) const;
	QVariant toVariant() const;

	QJsonArray array() const;
	QJsonObject object() const;
	const char *rawData(int *size) const;

	void setArray(const QJsonArray &array);
	void setObject(const QJsonObject &object);

	static QJsonDocument fromBinaryData(const QByteArray &data, DataValidation validation = Validate);
	static QJsonDocument fromJson(const QByteArray &json, QJsonParseError *error = 0);
	static QJsonDocument fromRawData(const char *data, int size, DataValidation validation = Validate);
	static QJsonDocument fromVariant(const QVariant &variant);

};

