
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))

sip_deps = [] #"sip:install","pyqt:install"]
if Target.qt_major() == 4:
	qjson = QMakeTarget( 'qjson', path, 'qjson.pro' )
	sip_deps.append('qjson')

pyqjson = SipTarget2('pyqjson', path, 'QtJson',False,None,sip_deps,parentModule='PyQt4')
pyqjson.ExtraLibs.append( 'qjson4' )
pyqjson.ExtraLibDirs.append( path )
pyqjson.ExtraIncludes += ['/usr/include/qjson']

rpm = RPMTarget('qjsonrpm','libqjson',path,'../../../rpm/spec/qjson.spec.template','1.0')
rpm.pre_deps = ["blurpythonbuildrpm"]

pyrpm = RPMTargetSip('pyqjsonrpm','pyqjson',path,'../../../rpm/spec/pyqjson.spec.template','1.0')

if __name__ == "__main__":
	build()
