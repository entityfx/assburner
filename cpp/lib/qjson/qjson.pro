
TARGET   = qjson4
TEMPLATE = lib

include($$(PRI_SHARED)/common.pri)

INSTALLS += target
DEFINES += QJSON_MAKE_DLL

# Input
HEADERS += \
	QJsonArray.h        \
	QJsonDocument.h     \
	QJsonObject.h       \
	QJsonParseError.h   \
	QJsonValue.h        \
	QJsonValueRef.h     \
	QJsonParser.h       \
	QJsonRoot.h

SOURCES += \
	QJsonArray.cpp      \
	QJsonDocument.cpp   \
	QJsonObject.cpp     \
	QJsonParseError.cpp \
	QJsonValue.cpp      \
	QJsonValueRef.cpp   \
	QJsonParser.cpp

