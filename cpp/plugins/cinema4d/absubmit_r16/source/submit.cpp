/////////////////////////////////////////////////////////////
// CINEMA 4D SDK                                           //
/////////////////////////////////////////////////////////////
// (c) 1989-2004 MAXON Computer GmbH, all rights reserved  //
/////////////////////////////////////////////////////////////

// example code for a menu/manager plugin

// be sure to use a unique ID obtained from www.plugincafe.com
#define ID_ABSUBMIT 1000573

#include "c4d.h"
#include "c4d_symbols.h"
#include "c4d_colors.h"

class AbSubmitCommand : public CommandData
{
	BaseFile * optionsFile;
	char * cstr_buf;
	int cstr_len;
	public:
	AbSubmitCommand() : optionsFile( 0 ), cstr_buf( 0 ), cstr_len( 0 ) {}
	~AbSubmitCommand() { if( optionsFile ) BaseFile::Free(optionsFile); delete [] cstr_buf; }

	void WriteOptions(const String & options)
	{
		int len = options.GetLength() + 1;
		if( cstr_len < len ) {
			delete [] cstr_buf;
			// Extra space for null terminating char
			// Round to next kb to prevent a ton of allocs
			cstr_len = len + (1024 - len % 1024);
			cstr_buf = new char[cstr_len];
		}
		options.GetCString( cstr_buf, cstr_len, STRINGENCODING_UTF8 );
		if( optionsFile )
			optionsFile->WriteBytes( cstr_buf, len-1 );
	}

	void WriteFormat(Int32 format)
	{
		String formatName = "Unknown Format";
		if( format == FILTER_TIF )
			formatName = "TIF";
		else if( format == FILTER_TGA )
			formatName = "TGA";
		else if( format == FILTER_BMP )
			formatName = "BMP";
		else if( format == FILTER_IFF )
			formatName = "IFF";
		else if( format == FILTER_JPG )
			formatName = "JPG";
		else if( format == FILTER_PICT )
			formatName = "PICT";
		else if( format == FILTER_PSD )
			formatName = "PSD";
		else if( format == FILTER_RLA )
			formatName = "RLA";
		else if( format == FILTER_RPF )
			formatName = "RPF";
		else if( format == FILTER_B3D )
			formatName = "B3D";
		else if( format == FILTER_AVI )
			formatName = "AVI";
		else if( format == FILTER_MOVIE )
			formatName = "MOV";
		else if( format == FILTER_QTVRSAVER_PANORAMA || format == FILTER_QTVRSAVER_OBJECT )
			formatName = "QTVR";
		else if( format == FILTER_HDR )
			formatName = "HDR";
		else if( format == 1023671 ) // FILTER_PNG )
			formatName = "PNG";
		else if( format == 1016606 )
			formatName = "EXR";
		else {
			GePrint( "Unknown Format ID: " + String::IntToString( format ) );
			return;
		}
		GePrint( "Format:" + formatName );
		WriteOptions( formatName + "\n" );
	}
	
	void WritePath(String path)
	{
		GePrint( "Render Path:" + path );
		WriteOptions( path + "\n" );
	}
	
	Bool FormatSupportsAlpha( Int32 format )
	{
		switch( format ) {
			case FILTER_TIF:
			case FILTER_TGA:
			case FILTER_PSD:
			case FILTER_RLA:
			case FILTER_RPF:
			/* TODO: Test to see if these support alpha channel
			case FILTER_B3D:
			case FILTER_HDR:
			case FILTER_PICT:
			case FILTER_IFF: */
				return true;
		}
		return false;
	}

	String layerNameForType( Int32 bufferType )
	{
		switch( bufferType ) {
			case VPBUFFER_RGBA: return "rgb";
			case VPBUFFER_AMBIENT: return "ambient";
			case VPBUFFER_DIFFUSE: return "diffuse";
			case VPBUFFER_SPECULAR: return "specular";
			case VPBUFFER_SHADOW: return "shadow";
			case VPBUFFER_REFLECTION: return "refl";
			case VPBUFFER_TRANSPARENCY: return "trans";
			case VPBUFFER_RADIOSITY: return "radiosity";
			case VPBUFFER_CAUSTICS: return "caustics";
			case VPBUFFER_ATMOSPHERE: return "atmos";
			case VPBUFFER_ATMOSPHERE_MUL: return "atmosmul";
			case VPBUFFER_DEPTH: return "depth";
			case VPBUFFER_MAT_COLOR: return "matcolor";
			case VPBUFFER_MAT_DIFFUSION: return "matdif";
			case VPBUFFER_MAT_LUMINANCE: return "matlum";
			case VPBUFFER_MAT_TRANSPARENCY: return "mattrans";
			case VPBUFFER_MAT_REFLECTION: return "matrefl";
			case VPBUFFER_MAT_ENVIRONMENT: return "matenv";
			case VPBUFFER_MAT_SPECULAR: return "matspec";
			case VPBUFFER_MAT_SPECULARCOLOR: return "matspeccol";
			case VPBUFFER_ILLUMINATION: return "illum";
			case VPBUFFER_OBJECTBUFFER: return "objectbuffer";
			case VPBUFFER_POSTEFFECT: return "post_1_";
			case VPBUFFER_POSTEFFECT_MUL: return "post_2_";
			case VPBUFFER_AMBIENTOCCLUSION: return "ao";
			case VPBUFFER_MAT_NORMAL: return "normal";
			case VPBUFFER_MAT_UV: return "uv";
			case VPBUFFER_MOTIONVECTOR: return "motion";
			case VPBUFFER_BLEND: return "blend_1_";
			case VPBUFFER_LIGHTBLEND: return "lightblend";
			case VPBUFFER_ALPHA: return "alpha";
		}
		return String();
	}

	void WriteLayer(int layerIndex, String layerName)
	{
		GePrint( "Found Active Layer:" + layerName );
		if( layerIndex > 0 )
			WriteOptions( "," );
		WriteOptions( layerName );
	}

	void WriteMultipassLayer(int & layerIndex, MultipassObject * multi, BaseContainer * rdata_c )
	{
		BaseContainer * multi_c = multi->GetDataInstance();
		// I couldn't find an enumerated ID for the multipass VPBUFFER type...
		String layerName = layerNameForType( multi_c->GetInt32(1000) );
		// Ignore object buffer
		if( layerName == "objectbuffer" ) return;
		if( layerName == "rgb" ) {
			Int32 saveFormat = rdata_c->GetInt32(RDATA_MULTIPASS_SAVEFORMAT);
			Bool alpha = rdata_c->GetBool(RDATA_ALPHACHANNEL);
			if( alpha && FormatSupportsAlpha(saveFormat) ) {
				WriteLayer(layerIndex++,"rgba");
			} else {
				WriteLayer(layerIndex++,"rgb");
				if( alpha )
					WriteLayer(layerIndex++,"alpha");
			}
		} else
			WriteLayer(layerIndex++,layerName);
	}
	
	Bool OpenOptionsFile()
	{
		Filename optionsFN;
		optionsFN.SetString("C:\\blur\\absubmit\\cinema4d\\current_options.txt");
	
		optionsFile = BaseFile::Alloc();
		Bool openSuccess = optionsFile->Open( optionsFN, FILEOPEN_WRITE, FILEDIALOG_NONE, BYTEORDER_INTEL );
		
		if( !openSuccess ) {
			//Error
			GeOutString( "Error opening render options file", GEMB_OK );
			GePrint( "Error opening render options file" );
			return FALSE;
		}
		return TRUE;
	}
	
	virtual Bool Execute(BaseDocument *doc)
	{
		GePrint("Blur Render Submit - Executing" );

		RenderData * rdata = doc->GetActiveRenderData();
		MultipassObject * multi = rdata->GetFirstMultipass();

		
	/*
		// Debug code to try to figure out the IDs for multipassobjects
		if( multi ) {
			BaseContainer * multi_c = multi->GetDataInstance();
			GePrint( "Inspecting multipass object " + multi->GetName() );
			GePrint( "GetAllBits returned " + String::IntToString( multi->GetAllBits() ) );
			GePrint( "Layer Enabled: " + String(multi->GetBit(4) == 0 ? "True" : "False") );
			for( int i=0; multi_c->GetIndexType(i) != DA_NIL; i++ ) {
				LONG id = multi_c->GetIndexId(i);
				LONG type = multi_c->GetIndexType(i);
				if( type == DA_LONG )
					GePrint( "Found long type at index " + String::IntToString(i) + " with ID " + String::IntToString(id) + " value " + String::IntToString(multi_c->GetLong(id)) );
				else if( type == DA_STRING )
					GePrint( "Found string type at index " + String::IntToString(i) + " with ID " + String::IntToString(id) + " value " + multi_c->GetString(id) );
				else
					GePrint( "Found unkown type at index " + String::IntToString(i) + " with ID " + String::IntToString(id) + " type " + String::IntToString(type) );
 
			}
			return TRUE;
		}	
	*/

		if( !OpenOptionsFile() )
			return FALSE;

		Int32 fps = doc->GetFps();
		BaseContainer * rdata_c = rdata->GetDataInstance();

		WriteOptions( doc->GetDocumentPath().GetString() + "\\" + doc->GetDocumentName().GetString() + "\n" );
		Int32 frameSeq = rdata_c->GetInt32(RDATA_FRAMESEQUENCE);
		if( frameSeq == RDATA_FRAMESEQUENCE_ALLFRAMES ) {
			WriteOptions( String::IntToString( doc->GetMinTime().GetFrame(fps) ) + "\n" );
			WriteOptions( String::IntToString( doc->GetMaxTime().GetFrame(fps) ) + "\n" );
		} else if ( frameSeq == RDATA_FRAMESEQUENCE_CURRENTFRAME ) {
			Int32 frame = doc->GetTime().GetFrame(fps);
			WriteOptions( String::IntToString( frame ) + "\n" );
			WriteOptions( String::IntToString( frame ) + "\n" );
		} else {
			WriteOptions( String::IntToString( rdata_c->GetTime(RDATA_FRAMEFROM).GetFrame(fps) ) + "\n" );
			WriteOptions( String::IntToString( rdata_c->GetTime(RDATA_FRAMETO).GetFrame(fps) ) + "\n" );
		}
	
		if( rdata_c->GetBool(RDATA_SAVEIMAGE) ) {
			WriteFormat( rdata_c->GetInt32( RDATA_FORMAT ) );
			WritePath( rdata_c->GetFilename( RDATA_PATH ).GetString() );
		} else {
			WriteOptions( "NONE\nNONE\n" );
		}

		if ( multi && rdata_c->GetBool(RDATA_MULTIPASS_SAVEIMAGE) && rdata_c->GetBool(RDATA_MULTIPASS_ENABLE) ) {
	
			Int32 saveFormat = rdata_c->GetInt32(RDATA_MULTIPASS_SAVEFORMAT);
			Filename fileName = rdata_c->GetFilename(RDATA_MULTIPASS_FILENAME);
			int layerCount = 0;
	
			WriteFormat( saveFormat );
			WritePath( fileName.GetString() );
	
			while( multi ) {
				// This is how we check if the layer is enabled
				if( multi->GetBit(4) == 0 )
					WriteMultipassLayer( layerCount, multi, rdata_c );
				multi = multi->GetNext();
			}

			if( layerCount > 0 ) {
				WriteOptions( "\n" );
			}
		}
	
		BaseFile::Free(optionsFile);
		optionsFile = 0;

		Filename scriptFN;
		scriptFN.SetString("C:\\blur\\absubmit\\cinema4d\\submit.py");
		
		Bool success = GeExecuteFile( scriptFN );//, doc->GetFilename() );
		if ( !success ) {
			GePrint("Error Executing command");
			return FALSE;
		}

		return TRUE;
	}

	virtual Int32 GetState(BaseDocument *doc)
	{
		return CMD_ENABLED;
	}

	virtual Bool RestoreLayout(void *secret)
	{
		return FALSE;
	}
};

AbSubmitCommand * sCmd;

Bool RegisterActiveObjectDlg(void)
{
	// decide by name if the plugin shall be registered - just for user convenience
	String name = GeLoadString(IDS_ABSUBMIT); if (!name.Content()) return TRUE;
	return RegisterCommandPlugin(ID_ABSUBMIT,name,0,nullptr,String("Blur Render Submit"),NewObjClear(AbSubmitCommand));
}

