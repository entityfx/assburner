
import os, sys
from blur.build import *

path = os.path.dirname(os.path.abspath(__file__))

class DoxyTarget(Target):
	def __init__(self,name,dir,pre_deps=[], post_deps=[]):
		pre_deps.append( WCRevTarget(name+"_wcrev",dir,"../..","Doxyfile.template","Doxyfile.post_wcrev.template") )
		Target.__init__(self,name,dir,pre_deps,post_deps)
		
	def build_run(self,args):
		lines = open( "Doxyfile.post_wcrev.template", "rw" )
		file = open( "Doxyfile", "w" )
		for l in lines:
			l = l.replace( "%QTDIR%", os.environ['QTDIR'] )
			file.write(l)
		os.system( "doxygen Doxyfile" )


All_Targets.append(DoxyTarget("docs",path))
