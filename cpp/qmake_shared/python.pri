
win32 {
	PY_PATH=$$system("python -c \"from distutils.sysconfig import get_config_vars; print get_config_vars()['prefix']\"")
	INCLUDEPATH+=$$system("python -c \"from distutils.sysconfig import get_python_inc; print get_python_inc()\"")
	PY_VERSION=$$system("python -c \"from distutils.sysconfig import get_python_version; print get_python_version().replace('.','')\"")
	PY_SITE_ARCH=$$system("python -c \"from distutils.sysconfig import get_python_lib; print get_python_lib(1)\"")
	message(Python Version is $$PY_VERSION Python lib path is $${PY_PATH}\\libs)
	LIBS+=-L$${PY_PATH}\libs -lpython$${PY_VERSION}
}

contains(QT_VERSION, ^5\\.*) {
	PY_EXEC=python3
} else {
	PY_EXEC=python
}

unix {
	INCLUDEPATH+=$$system("$${PY_EXEC} -c \"from distutils.sysconfig import get_python_inc; print(get_python_inc())\"")
	PY_VERSION=$$system("$${PY_EXEC} -c \"from distutils.sysconfig import get_python_version; print(get_python_version())\"")
	PY_SITE_ARCH=$$system("$${PY_EXEC} -c \"from distutils.sysconfig import get_python_lib; print(get_python_lib(1))\"")
	PY_LIB_PATH=$$system("$${PY_EXEC} -c \"from distutils.sysconfig import get_config_var; print(get_config_var('LIBPL'))\"")
	PY_LIB=$$system("$${PY_EXEC} -c \"from distutils.sysconfig import get_config_var; print(get_config_var('BLDLIBRARY'))\"")
	message(Python Version is $$PY_VERSION)
	INCLUDEPATH += /usr/include/python$${PY_VERSION}/
	LIBS+=-L$${PY_LIB_PATH} $${PY_LIB}
}

macx{
	INCLUDEPATH += $$system("python-config --includes")
	LD_FLAGS+=$$system("python-config --ldflags")
	LIBS+=$$system("python-config --libs")
}

