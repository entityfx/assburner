
# Include if building against and linking to libstone

include($$(PRI_SHARED)/python.pri)
include($$(PRI_SHARED)/qjson.pri)

rpm_build() {
	INCLUDEPATH+=/usr/include/stone
	LIBS+=-L$${PY_SITE_ARCH}
} else {
	INCLUDEPATH += \
		../../lib/stone/include \
		../../lib/stone/.out \
		../../lib/sip/siplib/
	LIBS+=-L../../lib/stone -L../../lib/sip/siplib
}

win32 {
	LIBS += -lPsapi -lMpr -ladvapi32 -lshell32
}

LIBS+=-lstone
