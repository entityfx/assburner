
# Include if building against and linking to libabsubmit

rpm_build() {
	INCLUDEPATH += /usr/include/absubmit/
} else {
	INCLUDEPATH += \
		../../lib/absubmit/include \
		../../lib/absubmit/.out
	LIBS += -L../../lib/absubmit/
}

LIBS += -labsubmit
