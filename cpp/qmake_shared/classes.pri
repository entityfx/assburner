
# Include if building against and linking to classes

rpm_build() {
	INCLUDEPATH += /usr/include/classes
} else {
	INCLUDEPATH += \
		../../lib/classes \
		../../lib/classes/autocore \
		../../lib/classes/base
	LIBS+=-L../../lib/classes
}

LIBS+=-lclasses
