
# Include if building against and linking to libstonegui

rpm_build() {
	INCLUDEPATH+=/usr/include/stonegui
} else {
	INCLUDEPATH += \
		../../lib/stonegui/include \
		../../lib/stonegui/.out
	LIBS+=-L../../lib/stonegui
}

LIBS+=-lstonegui
