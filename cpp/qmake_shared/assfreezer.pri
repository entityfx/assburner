
# Include if building against and linking to libassfreezer

rpm_build() {
	INCLUDEPATH+=/usr/include/assfreezer
} else {
	INCLUDEPATH += \
		../../lib/assfreezer/include \
		../../lib/assfreezer/.out
	LIBS+=-L../../lib/assfreezer
}
LIBS+=-lassfreezer


