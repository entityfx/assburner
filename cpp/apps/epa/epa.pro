
HEADERS += \
	mainwindow.h

SOURCES += \
	main.cpp \
	mainwindow.cpp

FORMS += \
	ui/epalaunchui.ui


INCLUDEPATH+=../../lib/epa/include ../../lib/epa/.out
LIBS+=-L../../lib/epa -lepa

unix:!macx:LIBS += -Wl,-rpath .
macx:CONFIG-=app_bundle

TEMPLATE=app

CONFIG+=qt thread
QT+=xml gui
MOC_DIR        = .out
OBJECTS_DIR    = .out
UI_DIR         = .out

DESTDIR=./
TARGET=epa
target.path=/usr/local/bin
INSTALLS += target
