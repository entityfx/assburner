/*
 *
 * Copyright 2008 Dr. Studios
 *
 * This file is part of the Resin software package.
 *
 * Assburner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Assburner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resin; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <qapplication.h>
#include <qfile.h>

#include "mainwindow.h"

static void usage()
{
	printf( "--help, -h\tPrints the command line options\n" );
	printf( "--override, -o key value>\toverride a env variable, ie DISPLAY :0.0\n" );
	printf( "--project <project>, -p\tuse this project\n" );
	printf( "--dept <dept>, -d\tuse this department\n" );
	printf( "--launch <software>, -l\tlaunch and exit\n" );
	printf( "--shell <software>\tstart a shell with this environ\n" );
	printf( "--printEnv <software>, -e\tprint env for software and exit\n" );
	printf( "--xml <path>, -x\tXML root\n" );
	printf( "--sep <separator>, -s\tpath separator\n" );
	printf( "--arch <arch>, -a\tarch\n" );
	printf( "--arg <arg>\targument to pass to exec, can use multiple times\n" );
}

int main( int argc, char * argv[] )
{

#ifdef Q_WS_X11
	bool useGUI = getenv( "DISPLAY" ) != 0;
#else
	bool useGUI = true;
#endif // Q_WS_X11
	QApplication a( argc, argv, useGUI );
	a.setApplicationName("EPA");

	QMap<QString, QString> mOverride;
	QString project, dept, software, xmlRoot, sep, arch;
	QStringList args;
	bool launch = false, printEnv = false, shell = false;
	int i = 0;
	while( i++<=argc )
	{
		QString arg( argv[i] );
		LOG("ARG: read: "+arg);
		if ( arg == "--help" || arg == "-h" ) {
			usage();
			return 1;
		} else if ( arg == "--override" || arg == "-o" ) {
			QString key = argv[++i];
			QString value = argv[++i];
			mOverride[key] = value;
		} else if ( arg == "--project" || arg == "-p" ) {
			project = argv[++i];
			LOG("ARG: project is set to "+project);
		} else if ( arg == "--dept" || arg == "-d" ) {
			dept = argv[++i];
			LOG("ARG: dept is set to "+project);
		} else if ( arg == "--launch" || arg == "-l" ) {
			software = argv[++i];
			launch = true;
			useGUI = false;
			LOG("ARG: launch is set to "+software);
		} else if ( arg == "--shell" ) {
			software = argv[++i];
			shell = true;
			useGUI = false;
			LOG("ARG: shell is set to "+software);
		} else if ( arg == "--printEnv" || arg == "-e" ) {
			software = argv[++i];
			printEnv = true;
			useGUI = false;
			LOG("ARG: printEnv is set to "+software);
		} else if ( arg == "--xml" || arg == "-x" ) {
			xmlRoot = argv[++i];
			LOG("ARG: xml is set to "+xmlRoot);
		} else if ( arg == "--arch" || arg == "-a" ) {
			arch = argv[++i];
			LOG("ARG: arch is set to "+arch);
		} else if ( arg == "--sep" || arg == "-s" ) {
			sep = argv[++i];
			LOG("ARG: sep is set to "+sep);
		} else if ( arg == "--arg" ) {
			args << argv[++i];
		}
	}
	
	if( useGUI ) {
		MainWindow * mw = new MainWindow(xmlRoot);
		mw->setOverride( mOverride );
		mw->setArch( arch );
		mw->setSep( sep );
		mw->populateProjects();
		mw->loadSettings();
		if( !project.isEmpty() )
			mw->setProject( project );

		if( !dept.isEmpty() )
			mw->setDept( dept );

		mw->show();
		int code = a.exec();
		return code;
	} else {
		EpaLoader loader(project+"_"+dept+"_"+software);
		loader.setRoot(xmlRoot);
		loader.setArch(arch);
		loader.setSep(sep);
		loader.setOverride( mOverride );
		loader.setArgs( args );
		loader.mergeEnvironment(QStringList() << project << dept << software);

		if( printEnv ) {
			QFile out;
			out.open(stdout, QIODevice::WriteOnly);
			out.write( QByteArray( loader.envList().join("\n").toUtf8() ) );
			out.write( "\n" );
		}
		if( launch )
			loader.launch(loader.envList());
		if( shell )
			loader.shell(loader.envList());
	}
	return 0;
}

