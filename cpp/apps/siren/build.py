
from blur.build import *
import os

path = os.path.dirname(os.path.abspath(__file__))
rev_path = os.path.join(path,'../..')

ini = IniConfigTarget("sirenini",path,'siren.ini.template','siren.ini')
svn = WCRevTarget("sirensvnrev",path,rev_path,"core/svnrev-template.h","core/svnrev.h")
svnnsi = WCRevTarget("sirensvnrevnsi",path,rev_path,"siren-svnrev-template.nsi","siren-svnrev.nsi")
svntxt = WCRevTarget("sirensvnrevtxt",path,rev_path,"siren_version_template.txt","siren_version.txt");
nsi = NSISTarget("siren_installer",path,"siren.nsi")
QMakeTarget("siren",path,"siren.pro",
    ["stone","classes",svnnsi,svntxt,svn,ini],[nsi])

if __name__ == "__main__":
	build()
