
DEPENDPATH+=core view ui

SOURCES	+= \
	view/sirenmainwindow.cpp \
	view/sirenprojectdialog.cpp \
	view/sirenshotwidget.cpp \
	view/sireneditdialog.cpp \
	view/thumbnailbutton.cpp \
	view/thumbnailchooser.cpp \
	view/thumbnailloader.cpp \
	view/stickitintherect.cpp \
	view/introview.cpp \
	view/shotwidget.cpp \
	view/shotdialog.cpp \
	view/projectwidget.cpp \
	view/assetwidget.cpp \
	view/taskeditwidget.cpp \
	view/elementchecktree.cpp \
	view/threadview.cpp \
	view/deliverytree.cpp \
	view/hostchecktree.cpp \
	view/shotitems.cpp \
	view/scheduleview.cpp \
	core/main.cpp

HEADERS	+= \
	view/sirenprojectdialog.h \
	view/sirenshotwidget.h \
	view/sireneditdialog.h \
	view/thumbnailbutton.h \
	view/thumbnailchooser.h \
	view/thumbnailloader.h \
	view/stickitintherect.h \
	view/introview.h \
	view/shotwidget.h \
	view/shotdialog.h \
	view/projectwidget.h \
	view/assetwidget.h \
	view/taskeditwidget.h \
	view/elementchecktree.h \
	view/threadview.h \
	view/deliverytree.h \
	view/hostchecktree.h \
	view/sirenmainwindow.h \
	view/shotitems.h \
	view/scheduleview.h \
	core/svnrev.h

FORMS	= \
	ui/sirenmainwindow.ui \
	ui/thumbnailchooser.ui \
	ui/introviewui.ui \
	ui/shotwidgetui.ui \
	ui/shotdialog.ui \
	ui/projectwidgetui.ui \
	ui/assetwidgetui.ui \
	ui/taskeditwidgetui.ui \
	ui/editdialog.ui \
	ui/threadviewui.ui \
	ui/projectdialog.ui

macx {
MOC_DIR        = .out_macx
OBJECTS_DIR    = .out_macx
UI_DIR         = .out_macx
} else {
MOC_DIR        = .out
OBJECTS_DIR    = .out
UI_DIR         = .out
}

INCLUDEPATH+=../../lib/classes/autocore ../../lib/classes/autoimp ../../lib/stone
INCLUDEPATH+=core view ui . ../../lib/stone/include ../../lib/stone/.out ../../lib/classes ../../lib/stonegui/include ../../lib/stonegui/.out
INCLUDEPATH+=../../lib/classesui/include ../../lib/classesui/.out
INCLUDEPATH+=../../lib/assfreezer/include ../../lib/assfreezer/.out

win32:debug{
	LIBS+=-L../../lib/classes/sipClassesui -lpyClassesui_d
	LIBS+=-L../../lib/classes/sipClasses -lpyClasses_d
	LIBS+=-L../../lib/stonegui/sipStonegui -lpyStonegui_d
	LIBS+=-L../../lib/stone/sipStone -lpyStone_d
	LIBS+=-LsipResin -lResin_d
}else{
	#LIBS+=-L../../lib/classesui/sipClassesui -lpyClassesui
	#LIBS+=-L../../lib/classes/sipClasses -lpyClasses
	#LIBS+=-L../../lib/stonegui/sipStonegui -lpyStonegui
	#LIBS+=-L../../lib/stone/sipStone -lpyStone
}

#LIBS+=-L../../lib/sip/siplib -lsip

win32{
	#INCLUDEPATH+=c:\nvidia\cg\include
	#LIBS+=-L..\..\lib\datepicker -ldatepicker 
	#LIBS+=-Lc:\nvidia\cg\lib -lcgGL -lcg
	
	LIBS+=-L..\..\lib\assfreezer -lassfreezer
	LIBS+=-L..\..\lib\classesui -lclassesui
	LIBS+=-L..\..\lib\stonegui -lstonegui
	LIBS+=-L..\..\lib\stone -L..\..\lib\classes -lclasses -lstone
	LIBS+=-lpsapi -lMpr
	LIBS+=-lopengl32
	LIBS+=-lws2_32
	
	INCLUDEPATH+="c:/python24/include/"
	
	INCLUDEPATH+=..\..\lib\sip\siplib
	LIBS+=-Lc:/python24/libs -lpython24
}

#win32:LIBS += -lpsapi -lMpr -lws2_32 -lgdi32 -lopengl32

unix{
	#INCLUDEPATH+=/usr/include/python2.4
	#LIBS+=-L../../lib/assfreezer -lassfreezer
	#LIBS+=-lpython2.4
	LIBS+=-L../../lib/classes -lclasses
	LIBS+=-L/mnt/x5/Global/infrastructure/ab/ppc/lib/
	LIBS+=-L../../lib/stonegui -lstonegui
	LIBS+=-L../../lib/stone -lstone
	LIBS+=-L../../lib/classesui -lclassesui
}
macx:CONFIG-=app_bundle

TEMPLATE=app
CONFIG+=qt thread warn_on debug
QT+=network sql xml
DESTDIR=./
macx {
TARGET=siren_macx
} else {
TARGET=siren
}

target.path=/usr/local/bin
INSTALLS += target

