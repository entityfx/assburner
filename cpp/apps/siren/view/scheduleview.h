
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef SCHEDULE_VIEW_H
#define SCHEDULE_VIEW_H

#include "viewbase.h"
#include "calendarcategory.h"
#include "project.h"

class ElementScheduleController;
class ScheduleWidget;
class QComboBox;
class QCheckBox;
class QToolBar;
class QToolButton;
class QSpinBox;

typedef QPair<QAction*,CalendarCategory> CalCatAction;

class ScheduleView : public ViewBase
{
Q_OBJECT
public:
	ScheduleView( QWidget * parent=0 );
	~ScheduleView();

	virtual bool viewForType( ElementList, bool );
	virtual void setElementList( ElementList el );
	virtual QToolBar * toolBar( QMainWindow * parent );
	virtual void setupMenu( QMenu * menu );

public slots:
	void displayModeChange( QAction * action );
	void recordTimeSheet();
	void newSchedule();
	void newEvent();

	void updateToolbarActionStates();

	void calActionTriggered();
	void allCalendarCategories();
	void clearCalendarCategories();

	void startDayActionTriggered( QAction * );
	void zoomActionTriggered( QAction * );

	void globalEventsActionTriggered(bool);
	void projectFilterActionTriggered(bool checked);
	void showAllEventsTriggered(bool);
	void clearEventFiltersTriggered( bool clearEvents );
	void showMyEventsOnly(bool);

	void durationChanged( int );
	void updateDuration();

	void print();

	void timeSheetsAdded(RecordList);
	void schedulesAdded(RecordList);
protected:
	void setupEventCategoryActions( QMenu * );
	void setupProjectFilterActions(QMenu*);
	void updateWeekStartActions();

	ElementScheduleController * mController;
	ScheduleWidget * mScheduleWidget;
	QToolBar * mToolBar;

	QAction * mNewTimeSheet, * mNewSchedule, * mNewEvent, * mPrintAction;
	QAction * mWeekAction, * mMonthAction, * mMonthFlatAction, * mYearAction;
	QAction * mShowWeekendsAction, * mShowTimeSheetsAction, * mShowSchedulesAction, * mShowCalendarEntriesAction, * mShowRecursiveAction;

	QAction * mDurationAction;
	QSpinBox * mDurationSpin;
	QWidget * mDurationContainer;

	CalendarCategoryList mAllCalendarCategories;
	QAction * mAllCategoriesAction, * mClearCategoriesAction;
	QList<QPair<QAction*,CalendarCategory> > mCalCatActions;

	ProjectList mAllProjectFilters;
	QAction * mAllEventsAction, * mClearEventFiltersAction,* mGlobalEventsAction;
	QList<QAction*> mProjectFilterActions;

	QToolButton * mProjectFiltersButton, * mEventCategoryFiltersButton;
	QAction * mMyEventsOnlyAction;

	QList<QAction*> mWeekStartDayActions;
};

#endif // SCHEDULE_VIEW_H

