
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include "stickitintherect.h"

StickItInTheRect::StickItInTheRect( QWidget * parent )
: QWidget( parent )
, showRect( false )
, mSquareAspect( true )
{}

void StickItInTheRect::paintEvent( QPaintEvent * pe )
{
	QPainter p( this );
	p.drawPixmap( pe->rect().topLeft(), mBuffer, pe->rect() );
	if( showRect )
		redrawRect( &p );
}

void StickItInTheRect::resizeEvent( QResizeEvent * )
{
	double oldScale = mScale;
	QRect oldRect = mRect;
	setImage( mOrig );
	oldScale = mScale / oldScale;
	mRect = QRect( int(oldRect.x() * oldScale), int(oldRect.y() * oldScale), int(oldRect.width() * oldScale), int(oldRect.height() * oldScale) );
	update();
}

void StickItInTheRect::mousePressEvent( QMouseEvent * me )
{
	showRect = false;
	redrawRect( 0, QRect() );
	mRect = QRect();
	mRect.setTopLeft( me->pos() );
}

void StickItInTheRect::mouseMoveEvent( QMouseEvent * me )
{
	int w = qMin( me->pos().x(), mBuffer.width() )-mRect.x();
	int h = qMin( me->pos().y(), mBuffer.height() )-mRect.y();
	w = qMax( 0, w );
	h = qMax( 0, h );
	if( mSquareAspect ){
		w = h = qMax( w, h );
		w = qMin( mBuffer.width()-mRect.x(), w );
		h = qMin( mBuffer.height()-mRect.y(), h );
	}
	QRect old = mRect;
	mRect = QRect( mRect.x(), mRect.y(), w, h );
	redrawRect( 0, old );
	emit rectChange( grabRect() );
}

void StickItInTheRect::redrawRect( QPainter * painter, const QRect & oldRect )
{
	QPainter * p = painter;
	if( !p )
		p = new QPainter( this );
	if( oldRect.isValid() )
	{
		p->drawPixmap( oldRect.left(), oldRect.top(), mBuffer, oldRect.left(), oldRect.top(), oldRect.width(), 2 );
		p->drawPixmap( oldRect.right(), oldRect.top(), mBuffer, oldRect.right(), oldRect.top(), 2, oldRect.height() );
		p->drawPixmap( oldRect.left(), oldRect.bottom(), mBuffer, oldRect.left(), oldRect.bottom(), oldRect.width(), 2 );
		p->drawPixmap( oldRect.left(), oldRect.top(), mBuffer, oldRect.left(), oldRect.top(), 2, oldRect.height() );
	}
	if( mRect.isValid() )
	{
//		p->setRasterOp( Qt::XorROP );
		p->setPen( Qt::white );
		p->drawRect( mRect );
	}
	if( !painter )
		delete p;
}

void StickItInTheRect::setRect( const QRect & rect )
{
	mRect = QRect( int(rect.x()*mScale), int(rect.y()*mScale), int(rect.width()*mScale), int(rect.height() * mScale) );
	showRect = true;
	update();
}

QRect StickItInTheRect::grabRect()
{
	return QRect( 
		(int)(mRect.x()/mScale),
		(int)(mRect.y()/mScale),
		(int)(mRect.width()/mScale),
		(int)(mRect.height()/mScale) );
}

void StickItInTheRect::setImage( QImage p )
{
	mOrig = p;
	mRect = p.rect();
	int w = mRect.width(), h = mRect.height();
	mScale = 1.0;
	
	if( w > width() )
	{
		mScale = width()/double(w);
		w = width();
		h = int(h * mScale);
	}
	if( h > height() )
	{
		mScale = height()/double(mRect.height());
		h = height();
		w = int(mRect.width()*mScale);
	}
	
	mBuffer = QPixmap::fromImage( mOrig.scaled( w, h, Qt::KeepAspectRatio, Qt::SmoothTransformation ) );
	update();
}

QPixmap StickItInTheRect::pixmap()
{
	return mBuffer;
}

void StickItInTheRect::setSquareAspect( bool sa )
{
	mSquareAspect = sa;
}

