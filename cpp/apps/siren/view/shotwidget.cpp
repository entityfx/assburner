
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qgroupbox.h>
#include <qlineedit.h>
#include <qspinbox.h>
#include <qtextedit.h>

#include "database.h"
#include "record.h"
#include "fieldlineedit.h"
#include "fieldtextedit.h"
#include "fieldspinbox.h"

#include "threadview.h"

#include "asset.h"
#include "elementdep.h"
#include "elementtype.h"
#include "project.h"
#include "resolution.h"
#include "shot.h"
#include "shotwidget.h"

#include "elementchecktree.h"

ShotWidget::ShotWidget( QWidget * parent )
: IntroBase( parent )
, mIgnoreDepUpdates( false )
{
	setupUi( this );
	mProxy = new RecordProxy( this );
	mShotNumber->setProxy( mProxy );
	mDialogEdit->setProxy( mProxy );
	mFrameStartSpin->setProxy( mProxy );
	mFrameEndSpin->setProxy( mProxy );
	mFrameStartSpin->setMaximum( INT_MAX );
	mFrameEndSpin->setMaximum( INT_MAX );
	mCameraInfoEdit->setProxy( mProxy );
	connect( ElementDep::table(), SIGNAL( added( RecordList ) ), SLOT( slotElementDepsAddedOrRemoved( RecordList ) ) );
	connect( ElementDep::table(), SIGNAL( removed( RecordList ) ), SLOT( slotElementDepsAddedOrRemoved( RecordList ) ) );
}

void ShotWidget::createSideWidget( QWidget * parent )
{
	/*	
	mSideWidget = new QGroupBox( "Notes", parent );
	QLayout * layout = new QHBoxLayout( mSideWidget );
	mThreadView = new ThreadView(this);
	layout->addWidget( mThreadView );
	mCheckTree = new ElementCheckTree( mSideWidget );
	layout->setMargin( 6 );
	ElementTypeList etl;
	etl += Asset::type();
	etl += ElementType::assetGroupType();
	mCheckTree->setElementTypeList( etl );
	mCheckTree->setEnabled(false);
	*/
}

QWidget * ShotWidget::sideWidget()
{
	return mSideWidget;
}
	
void ShotWidget::slotElementDepsAddedOrRemoved( RecordList recs )
{
	if( mIgnoreDepUpdates )
		return;

	ElementDepList list( recs );
	foreach( ElementDep ed, list )
		if( mElements.contains( ed.element() ) ){
			ElementList el = mElements;
			mElements.clear();
			setElementList( el );
			break;
		}
}

void ShotWidget::setElementList( ElementList el )
{
	QStringList shots;
	foreach( Element e, mElements )
		shots += Shot( e ).displayName();

	QString transName = shots.join(", ");
	Database::current()->beginTransaction( transName + " changed" );
	mProxy->setRecordList( el );
	mElements.commit();
	mSpoolTree->saveSpoolTree( el );
	Database::current()->commitTransaction();

	mElements = el;
	mSpoolTree->buildSyncTree( mElements );
	//mThreadView->setElementList( mElements );
}


