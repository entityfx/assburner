
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef SHOT_WIDGET_H
#define SHOT_WIDGET_H

#include "element.h"
#include "elementdep.h"
#include "introview.h"
#include "threadview.h"
#include "recordproxy.h"
#include "ui_shotwidgetui.h"

class ElementCheckTree;
class TasksWidget;
class QGroupBox;

class ShotWidget : public IntroBase, public Ui::ShotWidgetUI
{
Q_OBJECT
public:
	ShotWidget( QWidget * parent );

	virtual void setElementList( ElementList );

	void createSideWidget( QWidget * parent );
	QWidget * sideWidget();

public slots:
	void slotElementDepsAddedOrRemoved( RecordList );

protected:
	ElementList mElements;
	Element mProjectElement;
	QGroupBox * mSideWidget;
//	ElementCheckTree * mCheckTree;
	ThreadView * mThreadView;
	RecordProxy * mProxy;
	bool mIgnoreDepUpdates;

};

#endif // SHOT_WIDGET_H

