
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qaction.h>
#include <qcombobox.h>
#include <qimage.h>
#include <qlabel.h>
#include <qmessagebox.h>
#include <qmenu.h>
#include <qpushbutton.h>
#include <qprintdialog.h>
#include <qprinter.h>
#include <qregexp.h>
#include <qsplitter.h>
#include <qstatusbar.h>
#include <qtabbar.h>
#include <qtabwidget.h>
#include <qmenubar.h>
#include <qtoolbar.h>
#include <qurl.h>
#include <qfontdialog.h>
#include <qscrollbar.h>
#include <qshortcut.h>

#include "blurqt.h"
#include "database.h"
#include "path.h"
#include "iniconfig.h"

#include "updatemanager.h"
#include "undomanager.h"

#include "asset.h"
#include "elementuser.h"
#include "employee.h"
#include "project.h"
#include "projectstatus.h"
#include "task.h"
#include "assettype.h"

#include "svnrev.h"
#include "thumbnailloader.h"
#include "thumbnailchooser.h"
#include "thumbnailui.h"
#include "sirenmainwindow.h"
#include "sirenprojectdialog.h"
#include "sireneditdialog.h"
#include "shotdialog.h"
#include "scenedialog.h"
#include "shotitems.h"
#include "usertaskdialog.h"
#include "scheduleview.h"
#include "threadview.h"



SirenMainWindow::SirenMainWindow( QWidget * parent )
: QMainWindow( parent )
{
	setupUi( this );

	// Caption
	setWindowTitle( QString("Siren 1.0") + ", build "+ SVN_REVSTR );

	 // actions
	mNewProjectAction = new QAction( QIcon( "images/project.png" ), "New Project", this );
	connect( mNewProjectAction, SIGNAL( activated() ), SLOT(sirenNewProject()));
	mEditProjectAction = new QAction( QIcon( "images/project.png" ), "Edit Project", this );
	connect( mEditProjectAction, SIGNAL( activated() ), SLOT(sirenEditProject()));

	mNewAssetAction = new QAction( QIcon("images/Teapot.bmp"), "New Asset", this );
	mNewTaskAction = new QAction( QIcon("images/task.png"), "New Task", this );

	shortcutNew = new QShortcut( QKeySequence::New, this, 0, 0, Qt::ApplicationShortcut );
	connect( shortcutNew, SIGNAL( activated() ), SLOT( sirenNewShot() ) );
	mNewShotAction = new QAction( QIcon("images/shot.png"), "New Shot", this );
	mNewShotAction->setShortcut( shortcutNew->key() );
	connect( mNewShotAction, SIGNAL( activated() ), SLOT( sirenNewShot() ) );
	mEditShotAction = new QAction( QIcon("images/shot.png"), "Edit Shot", this );
	connect( mEditShotAction, SIGNAL( activated() ), SLOT(sirenEditShot()));
	mDeleteShotAction = new QAction( QIcon("images/shot.png"), "Delete Shot", this );
	connect( mDeleteShotAction, SIGNAL( activated() ), SLOT(sirenDeleteShot()));
	connect( mTree, SIGNAL( doubleClicked(const QModelIndex &) ), SLOT(treeEditItem(const QModelIndex &)));

	connect( mSceneCombo, SIGNAL( activated(const QString &) ), SLOT(currentSceneChanged(const QString &)));
	mFontAction = new QAction( "Change &Font...", this );
	connect( mFontAction, SIGNAL( activated() ), SLOT(changeFont()) );
	//mScheduleAction = new QAction( "Show Schedule...", this );
	//connect( mScheduleAction, SIGNAL( activated() ), SLOT(showSchedule()) );
	mPrintAction = new QAction( "&Print...", this );
	connect( mPrintAction, SIGNAL( activated() ), SLOT(print()) );
	mQuitAction = new QAction( "&Quit", this );
	connect( mQuitAction, SIGNAL( activated() ), qApp, SLOT(quit()) );

	connect( Project::table(), SIGNAL( updated( Record, Record ) ), SLOT( populateProjectMenu() ) );
	connect( Thumbnail::table(), SIGNAL( updated( Record, Record ) ), SLOT( projectThumbnailUpdated(Record,Record) ) );
	connect( Project::table(), SIGNAL( added( RecordList ) ), SLOT( populateProjectMenu() ) );

	connect( Shot::table(), SIGNAL( added( RecordList ) ), SLOT( refreshView() ) );
	connect( Shot::table(), SIGNAL( updated( RecordList ) ), SLOT( refreshView() ) );
	connect( Shot::table(), SIGNAL( removed( RecordList ) ), SLOT( refreshView() ) );
	connect( ElementUser::table(), SIGNAL( added( RecordList ) ), SLOT( refreshView() ) );
	connect( ElementUser::table(), SIGNAL( removed( RecordList ) ), SLOT( refreshView() ) );

// 	connect(mTree->header(), SIGNAL(sectionResized ( int, int, int )), mTree->model(), SLOT( layoutChanged ()));
// 	connect(mTree->header(), SIGNAL(sectionResized ( int, int, int )), mTree, SLOT( refresh ()));

	mSearchShotName = new QAction( "Search Shot Name", this );
// 	connect( mSearchShotName, SIGNAL( activated() ), SLOT( selectFilter() ) );
	mSearchShotDesc = new QAction( "Search Shot Description", this );
// 	connect( mSearchShotDesc, SIGNAL( activated() ), SLOT( selectFilter() ) );
	mSearchVfxNotes = new QAction( "Search VFX Notes", this );
// 	connect( mSearchVfxNotes, SIGNAL( activated() ), SLOT( selectFilter() ) );
	mSearchAll = new QAction( "Search All of the Above", this );
// 	connect( mSearchAll, SIGNAL( activated() ), SLOT( selectFilter() ) );
	
	populateMenus();

	IniConfig & config = userConfig();
	config.pushSection("Siren_Display_Prefs");
	Project configProject = Project(config.readInt("CurrentProject"));

	connect( ShotGroup::table(), SIGNAL( updated( Record, Record ) ), SLOT( sceneUpdated( Record ) ) );

 	qApp->setFont( config.readFont("ShotFont") );
	{
		ShotModel * sm = new ShotModel( mTree );
		sm->setAutoSort(true);
		mTree->setModel( sm );
		WrapTextDelegate * wtd = new WrapTextDelegate;
		mTree->setItemDelegate( wtd );
		setupShotView( mTree );
	}

	if( configProject.isRecord() )
		mCurrentProject = configProject;
	setCurrentProject(mCurrentProject);
  	
	connect(mSearchText, SIGNAL(returnPressed()), SLOT(filterShots()));
	mFilterFields["Name"] = 2;
	mFilterFields["Description"] = 9;
	mFilterFields["VFX Notes"] = 10;
	
	initSchedule();
	initNotes();
}

SirenMainWindow::~SirenMainWindow()
{
}

void SirenMainWindow::closeEvent( QCloseEvent * ce )
{
	saveShotView(mTree);
	IniConfig & config = userConfig();
	config.pushSection("Siren_Display_Prefs");
	config.writeString( "FrameGeometry",
	  QString("%1,%2,%3,%4").arg( pos().x() ).arg( pos().y() ).arg( size().width() ).arg( size().height() )
	);
	config.writeFont( "ShotFont", qApp->font() );
	config.writeInt( "CurrentProject", mCurrentProject.key() );
	config.popSection();
	QMainWindow::closeEvent(ce);
}

void SirenMainWindow::changeFont() {
	qApp->setFont( QFontDialog::getFont(0, mTree->font()) );
}

void SirenMainWindow::initSchedule() {
	QBoxLayout * hlayout = new QHBoxLayout( mScheduleTab );
	hlayout->setMargin( 0 );
	hlayout->setSpacing( 0 );

	ScheduleView * sv = new ScheduleView();
	hlayout->addWidget( sv );

	ProjectList pl = Project::select("fkeyprojectstatus="+QString::number( ProjectStatus::recordByName("Production").key() ));
	sv->setElementList(pl);

	QToolBar * tb = sv->toolBar(this);
	tb->show();
}

void SirenMainWindow::initNotes() {
	QBoxLayout * hlayout = new QHBoxLayout( mNoteTab );
	hlayout->setMargin( 0 );
	hlayout->setSpacing( 0 );

	ThreadView * tv = new ThreadView();
	hlayout->addWidget( tv );

	ProjectList pl = Project::select("fkeyprojectstatus="+QString::number( ProjectStatus::recordByName("Production").key() ));
	tv->setElementList(pl);
}

void SirenMainWindow::populateMenus()
{
	QMenuBar * mb = menuBar();
	mb->clear();
	mFileMenu = mb->addMenu( "&File" );
	mFileMenu->addAction( mFontAction );
	//mFileMenu->addAction( mScheduleAction );
	mFileMenu->addAction( mPrintAction );
	mFileMenu->addAction( mQuitAction );

	mProjectMenu = new QMenu;
	populateProjectMenu();
	mTreeMenu = new QMenu;

	mSearchMenu = new QMenu;
	mSearchMenu->addAction( mSearchShotName );
	mSearchMenu->addAction( mSearchShotDesc );
	mSearchMenu->addAction( mSearchVfxNotes );
	mSearchMenu->addAction( mSearchAll );
	mSearchType->setIcon( QIcon("images/search_icon.png") );
	mSearchType->setMenu(mSearchMenu);
	connect( mSearchType, SIGNAL( triggered(QAction *) ), SLOT( selectFilter(QAction *) ) );
}

void SirenMainWindow::populateProjectMenu()
{
	mProjectMenu->clear();

	ProjectList pl = Project::select("fkeyprojectstatus="+QString::number( ProjectStatus::recordByName("Production").key() ));
	foreach( Project p, pl ) {
		if( !mCurrentProject.isRecord() )
			mCurrentProject = p;
		QAction * act = new QAction( p.name(), this );
		act->setData(QVariant("project_name"));
		act->setIcon( ThumbnailLoader::load(p.thumbnail(), QSize(32,24)) );
		mProjectMenu->addAction( act );
	}
	
	mProjectMenu->addSeparator();
	mProjectMenu->addAction( mEditProjectAction );
	mProjectMenu->addAction( mNewProjectAction );

	connect( mProjectButton, SIGNAL( customContextMenuRequested(const QPoint &) ), SLOT(showProjectMenu( const QPoint & )));
	connect( mTree, SIGNAL( customContextMenuRequested(const QPoint &) ), SLOT(showTreeMenu( const QPoint & )));
}

void SirenMainWindow::projectThumbnailUpdated(Record cur, Record old)
{
	if( Project( Thumbnail(cur).element() ).isRecord() )
		populateProjectMenu();
}

void SirenMainWindow::populateSceneCombo()
{
	int idx = 0, selIdx = 0;
	mSceneCombo->clear();
	ShotGroupList scenes = mCurrentProject.children( ShotGroup::type(), true );
	scenes = scenes.sorted("name");
	foreach( ShotGroup scene, scenes ) {
		//qWarning("adding scene: "+scene.name().toAscii() +" at index: "+QString::number(idx).toAscii() );
		if( mCurrentScene == scene )
			selIdx = idx;
		if( !mCurrentScene.isRecord() )
			mCurrentScene = scene;
		mSceneCombo->insertItem( idx++, 
			ThumbnailLoader::load(scene.thumbnail(), QSize(32,24)),
			scene.name(),
			QVariant("scene_name")
		);
	}
	if( mCurrentScene.isRecord() )
		mSceneCombo->addItem("Edit Episode");
	mSceneCombo->addItem("New Episode");

	mSceneCombo->setCurrentIndex( selIdx );
}

void SirenMainWindow::populateTreeMenu()
{
	mTreeMenu->clear();
// 	mTreeMenu->addAction("Open");
	mTreeMenu->addAction(mEditShotAction);
	ElementList selected = mTree->selectedElements();
	if( selected.size() == 1 ) {
		QMenu * taskMenu = new QMenu("Edit Task..");
		ElementList tasks = selected[0].children( Task::type(), false );
		foreach( Element e, tasks ) {
			//qWarning("Element has task: "+e.name().toAscii());
			QAction * act = new QAction( e.name(), this );	
			act->setData(QVariant("edit_task"));
			taskMenu->addAction( act );
		}
		mTreeMenu->addMenu( taskMenu );
	}
	mTreeMenu->addSeparator();
	mTreeMenu->addAction(mNewShotAction);
	mTreeMenu->addAction(mDeleteShotAction);
}

void SirenMainWindow::sirenNewProject()
{
	SirenProjectDialog * spd = new SirenProjectDialog(this);
	if( spd->exec() == QDialog::Accepted ) {
		setCurrentProject(spd->project());
	}
	delete spd;
}

void SirenMainWindow::sirenNewShot()
{
	SirenEditDialog * d = new SirenEditDialog(this);
	d->newShot( mCurrentProject, mCurrentScene );
	d->exec();
	return;
}

void SirenMainWindow::sirenDeleteShot()
{
	mTree->selectedElements().remove();
}

void SirenMainWindow::showProjectMenu( const QPoint & p )
{
	QAction * result = mProjectMenu->exec(mProjectButton->mapToGlobal(p));
	if( !result )
		return;
	if( result->data().toString() == "project_name" )
		setCurrentProject( Project::recordByName(result->text()) );
}

void SirenMainWindow::showTreeMenu( const QPoint & p )
{
	populateTreeMenu();
	QAction * result = mTreeMenu->exec(QCursor::pos());
	if( !result )
		return;
	if( result->data().toString() == "edit_task" ) {
		ElementList selected = mTree->selectedElements();
		ElementList tasks = selected[0].children( Task::type(), false );
		foreach( Element e, tasks )
			if( e.name() == result->text() )
				sirenEditElements( ElementList(e) );
	}
}

void SirenMainWindow::setCurrentProject( const Project & p )
{
	//mTree->clear();
	mTree->setEnabled(false);
	mCurrentProject = p;

	mCurrentScene = ShotGroup();
	populateSceneCombo();
	if( mCurrentScene.isRecord() ) {
		currentSceneChanged( mCurrentScene.name() );
		mTree->setEnabled(true);
		mTree->setCurrentScene(mCurrentScene);
	}
	refreshView();
}

void SirenMainWindow::currentSceneChanged(const QString & name )
{
	if( name == "Edit Episode" ) {
		sirenEditElements( ElementList( mCurrentScene ) );
	}
	else if ( name == "New Episode" ) {
		SceneDialog * sd = new SceneDialog(this);
		if( sd->exec() == QDialog::Accepted ) {
			ShotGroup newScene = sd->shotGroup();
			newScene.setParent( mCurrentProject );
			newScene.setProject( mCurrentProject );
			newScene.commit();
			mCurrentScene = newScene;
		}
	}
	else {
		ShotGroupList scenes = mCurrentProject.children( ShotGroup::type(), true );
		foreach( ShotGroup scene, scenes )
			if( scene.name() == name )
				mCurrentScene = scene;
	}
	mDescriptionLabel->setText( mCurrentScene.description() );
	mAirDateLabel->setText( mCurrentScene.dateComplete().toString() );
	mTree->setCurrentScene(mCurrentScene);
	populateSceneCombo();
	refreshView();
}

void SirenMainWindow::sceneUpdated( Record rec )
{
	if( Record(mCurrentScene) == rec ) {
		mDescriptionLabel->setText( mCurrentScene.description() );
		mAirDateLabel->setText( mCurrentScene.dateComplete().toString() );
		populateSceneCombo();
	}
}

void SirenMainWindow::refreshView()
{
	int currentSlidePos = mTree->verticalScrollBar()->sliderPosition();
	mProjectButton->setElementList( ElementList(mCurrentProject) );
	
	QString text = mSearchText->text();
 	mTree->refresh();
	if( !text.isEmpty() )
		filterShots();
	else {
		statusbar->showMessage("total shots: " + QString::number(mCurrentScene.shots().size()));
	}
	
	mTree->verticalScrollBar()->setSliderPosition(currentSlidePos);
}

void SirenMainWindow::sirenEditProject()
{
	sirenEditElements( ElementList( mCurrentProject ) );
}

void SirenMainWindow::treeEditItem(const QModelIndex & index)
{
	qWarning( "=== enable editing for row " + QString::number(index.row()).toAscii() + ", col " + QString::number(index.column()).toAscii() );
	int col = index.column();
	if( col == 1 ) {
		// edit thumbnail
		ShotModel * sm = (ShotModel*)(mTree->model());
		Shot s = ((Shot)sm->getRecord(index));
		Thumbnail t = s.thumbnail();
		ThumbnailChooser * tc = new ThumbnailChooser( t.originalFile(), QRect(), this );
		if( tc->exec() == QDialog::Accepted )
		{
			qWarning( "new file is " + tc->path().toAscii() );
			QPixmap pix = tc->pixmap();
			if( !pix.isNull() ) {
				t.setOriginalFile( tc->path() );
				t.commit();
				ThumbnailUi(t).setImage( pix );
				mTree->refresh();
			}
		}
		delete tc;
	}
	else if( col == 8 ) {
		// edit 3d artists
		UserTaskDialog * d = new UserTaskDialog(this);
		Shot s = ((ShotModel*)mTree->model())->getRecord(index);
		Task t;
		
		ElementList tasks = s.children( Task::type(), false );
		foreach( t, tasks ) {
			qWarning("available task: " + t.name().toAscii());
			if( t.name().contains("VFX") ) {
				// currently, compositing is synonymous with 2d, and VFX with 3d
				qWarning("found 3d");
				break;
			}
		}
		
		d->setUsers( t.users() );
		if( d->exec() == QDialog::Accepted )
		{
			t.setUsers( d->userList() );
			t.commit();
		}
	}
	else if( col == 7 ) {
		// edit 2d artists
		UserTaskDialog * d = new UserTaskDialog(this);
		Shot s = ((ShotModel*)mTree->model())->getRecord(index);
		Task t;
		
		ElementList tasks = s.children( Task::type(), false );
		foreach( t, tasks ) {
			qWarning("available task: " + t.name().toAscii());
			if( t.name().contains("compositing") ) {
				// currently, compositing is synonymous with 2d, and VFX with 3d
				qWarning("found 2d");
				break;
			}
		}
		
		d->setUsers( t.users() );
		if( d->exec() == QDialog::Accepted )
		{
			t.setUsers( d->userList() );
			t.commit();
		}
	}
	else {
		mTree->setCurrentIndex(index);
		mTree->edit(index);
	}
}

void SirenMainWindow::sirenEditShot()
{
	sirenEditElements( mTree->selectedElements() );
}

void SirenMainWindow::sirenEditElements( ElementList el )
{
	SirenEditDialog * sed = new SirenEditDialog(this);
	sed->setElementList( el );
	if( sed->exec() == QDialog::Accepted ) {
		refreshView();
	}
	delete sed;
}

void SirenMainWindow::selectFilter(QAction * input)
{
	qWarning( "filter pulldown activated, " + input->text().toAscii() + " selected" );
}

void SirenMainWindow::filterShots()
{
        mFilterShotsRunning = true;

        QString text = mSearchText->text();
        if( text.isEmpty() ) {
		refreshView();
		return;
	}

	ShotModel * sm = (ShotModel*)(mTree->model());
	ShotList allShots = mCurrentScene.children( Shot::type(), false );
	
	sm->updateRecords( allShots );

        QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

        if(!text.startsWith("*"))
                text = "*"+text;
        if(!text.endsWith("*"))
                text += "*";

	ShotList found;
        foreach( Shot s, allShots ) {
		QModelIndex mi = sm->findIndex( s );
		QMap< int, QString > siblings;
		while(mi.isValid() && mi.column() < 11) {
			int col = mi.column();
			QVariant displayData = sm->data(mi, Qt::DisplayRole);
			qWarning( "populate map for column "+ QString::number(col).toAscii() + " to " + displayData.toString().toAscii() );
			siblings[col] = displayData.toString();
			mi = mi.sibling( mi.row(), mi.column()+1 );
		}
		for( QMap<QString, int>::Iterator it = mFilterFields.begin(); it != mFilterFields.end(); ++it ) {
                	if( siblings[ mFilterFields[it.key()] ].contains(QRegExp(text, Qt::CaseInsensitive, QRegExp::Wildcard)) )
                        	found += s;
		}
	}

	statusbar->showMessage("total shots: " + QString::number( allShots.size() ).toAscii() + ", filtered shots: " + QString::number( found.size() ).toAscii());
	sm->updateRecords( found );

        mFilterShotsRunning = false;
        QApplication::restoreOverrideCursor();
}

void SirenMainWindow::print()
{
	QPrinter printer;
	statusbar->showMessage("printing...");

	QPrintDialog *dialog = new QPrintDialog(&printer, this);
	dialog->setWindowTitle(tr("Print Document"));
	dialog->setMinMax(1, 4);
	dialog->setFromTo(1, 4);
	dialog->setPrintRange(QAbstractPrintDialog::AllPages);
	if( dialog->exec() != QDialog::Accepted )
		return;
	
	if( printer.outputFormat() == QPrinter::NativeFormat )
		qWarning("printing to actual printers is currently disabled for testing purposes");
		return;
	
	QRect pageArea = printer.pageRect();
	qWarning( "=== size is " + QString::number(pageArea.width()).toAscii() + ", " + QString::number(pageArea.height()).toAscii() );
	
	QPainter painter;

	painter.begin(&printer);
		painter.setFont( QFont("Serif", 60) );
		
		QRect textArea;
		painter.drawText(pageArea, Qt::AlignHCenter | Qt::AlignVCenter, QString("Hello, World"), &textArea);
	painter.end();
	
	statusbar->clearMessage();
}
