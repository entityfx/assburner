
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef INTRO_VIEW_H
#define INTRO_VIEW_H

#include <qstackedwidget.h>

#include "ui_introviewui.h"

#include "recordproxy.h"
#include "viewbase.h"

#include "element.h"
#include "user.h"

class QBoxLayout;
class QWidgetStack;

class AssetWidget;
class ProjectWidget;
class ShotWidget;
class TaskEditWidget;

class IntroBase : public QWidget
{
public:
	IntroBase( QWidget * parent = 0 ) : QWidget( parent ) {}

	virtual void setElementList( ElementList ) = 0;
};

class IntroView : public ViewBase
{
Q_OBJECT
public:
	IntroView( QWidget * parent = 0 );
	~IntroView();

	virtual void setElementList( ElementList );

	bool viewForType( ElementList, bool );

public slots:
	void updateElement( Record );
	void statusChanged( const Record & );

protected:
	/* Generic view for element attributes */
	Ui::IntroViewUI * mIntroView;
	
	bool mChanges;
	bool mProjectMode;
	ElementList mElements;

	User mUser;

	QStackedWidget * mDetailStack;
	QStackedWidget * mLeftWidgetStack;

	IntroBase * mCurrentView;

	/* IntroBase Widgets */
	ProjectWidget  * mProjectWidget;
	ShotWidget     * mShotWidget;
	AssetWidget    * mAssetWidget;
	TaskEditWidget * mTaskEditWidget;
	RecordProxy    * mProxy;
};

#endif // INTRO_VIEW_H

