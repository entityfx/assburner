
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef PROJECT_WIDGET_H
#define PROJECT_WIDGET_H

#include "introview.h"
#include "ui_projectwidgetui.h"

#include "project.h"
#include "resolution.h"

class TasksWidget;

class ProjectWidget : public IntroBase, public Ui::ProjectWidgetUI
{
Q_OBJECT
public:
	ProjectWidget( QWidget * parent );

	virtual void setElementList( ElementList );

public slots:
	void addResolution();
	void editResolution();
	void removeResolution();

	void resolutionsAdded( RecordList );
	void resolutionsDeleted( RecordList );
	void resolutionUpdated( Record );

	void addDelivery();
	void editDelivery();
	void removeDelivery();

protected:
	Project mProject;
	ResolutionList mResolutions;

};

#endif // PROJECT_WIDGET_H

