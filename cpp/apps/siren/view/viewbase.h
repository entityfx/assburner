
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef VIEW_BASE_H
#define VIEW_BASE_H

#include <qwidget.h>

#include "elementtype.h"
#include "element.h"

class QToolBar;
class QMainWindow;
class QMenu;

class ViewBase : public QWidget
{
public:
	ViewBase(QWidget * parent)
	: QWidget( parent ){}

	virtual bool viewForType( ElementList, bool ) = 0;
	virtual void setElementList( ElementList el ) = 0;
	virtual QToolBar * toolBar( QMainWindow * ) { return 0; }
	virtual void setupMenu( QMenu * ) {}
};

#endif // VIEW_BASE_H

