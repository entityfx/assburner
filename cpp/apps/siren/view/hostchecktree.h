
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef SERVICE_CHECK_TREE_H
#define SERVICE_CHECK_TREE_H

#include <qtreeview.h>

#include "host.h"
#include "element.h"
#include "recordtreeview.h"

class HostCheckTree : public RecordTreeView
{
Q_OBJECT
public:
	HostCheckTree( QWidget * parent = 0 );

	void setRootElement();
	HostList checkedElements();
	HostList noChangeElements();

	void setChecked( HostList els );
	void setNoChange( HostList els );
	void setReadOnly( bool );

	void buildSyncTree( ElementList );
	void saveSpoolTree( ElementList );
	
public slots:
	void slotElementsAdded( RecordList );
	void slotElementsRemoved( RecordList );
	void slotElementUpdated( Record, Record );

protected:
	HostList elementsByState( Qt::CheckState );

	bool mReadOnly;
	HostList mHosts;
};

#endif // SOFTWARE_CHECK_TREE_H

