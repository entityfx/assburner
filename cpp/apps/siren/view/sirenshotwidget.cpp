
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <QPixmap>
#include <QLabel>
#include <QBuffer>

#include <qfile.h>
#include <qurl.h>
#include <qmessagebox.h>

#include "sirenshotwidget.h"

#include "elementstatus.h"
#include "elementuser.h"
#include "shot.h"
#include "user.h"
#include "employee.h"
#include "task.h"
#include "assettype.h"

#include "shotitems.h"
#include "thumbnail.h"
#include "filetracker.h"
#include "recordmodel.h"
#include "thumbnailloader.h"
#include "thumbnailchooser.h"

SirenShotWidget::SirenShotWidget( QWidget * parent )
: RecordTreeView( parent )
{
	setIconSize(QSize(120,90));
	setItemDelegateForColumn( 9, new WrapTextDelegate( this ) );
	setItemDelegateForColumn( 10, new WrapTextDelegate( this ) );
	refresh();

	connect( Shot::table(), SIGNAL( update( const Record &, const Record & ) ), SLOT( shotUpdated( const Record &, const Record & ) ) );

	setAcceptDrops(true);
	
}

void SirenShotWidget::dragEnterEvent( QDragEnterEvent * event )
{
	qWarning("dragEnterEvent procedure triggered");
	event->acceptProposedAction();
}
void SirenShotWidget::dragMoveEvent( QDragMoveEvent * event )
{
// 	qWarning("dragMoveEvent procedure triggered");
	event->acceptProposedAction();
}
void SirenShotWidget::dropEvent( QDropEvent * event )
{
	qWarning("dropEvent procedure triggered");

	// just take the first file path from the list
	QString path = event->mimeData()->urls().first().toLocalFile();
	
	if( QFile::exists( path ) ) {
		qWarning( "file drag/dropped! " + path.toAscii() );
		
		QImage image = QImage();
		if( !image.load( path ) ) {
			QMessageBox error( QMessageBox::Warning, "invalid file", "could not load file '" + path + "'\ninvalid image format", QMessageBox::Ok, this);
			error.setModal(true);
			error.exec();
			return;
		}
		
		QModelIndex index = indexAt( event->pos() );
		Shot s = ((ShotModel*)model())->getRecord( index );
		Thumbnail t = s.thumbnail();
		QString oldPath = t.originalFile();
		
		t.setOriginalFile( path );

		QByteArray ba;
		QBuffer buffer(&ba);
		buffer.open(QIODevice::WriteOnly);
		image.save(&buffer, "PNG");
		t.setImage(ba);

		t.commit();
		refresh();
		
		qWarning( "path: " + oldPath.toAscii() + " -> " + t.originalFile().toAscii() );
	}
	event->acceptProposedAction();
}

void SirenShotWidget::shotUpdated( const Record & cur, const Record & old )
{
	((ShotModel*)model())->updated( ShotList(cur) );
}

ElementList SirenShotWidget::selectedElements()
{
	return selection();
}

ElementList SirenShotWidget::allElements()
{
	return mCurrentScene.shots();
}

void SirenShotWidget::setCurrentScene(const ShotGroup & scene)
{
	mCurrentScene = scene;
}

void SirenShotWidget::refresh()
{
	if( mCurrentScene.isRecord() ) {
		ShotList shotsInScene = mCurrentScene.children( Shot::type(), false );
		ShotModel * sm = (ShotModel*)model();
		sm->updateRecords( shotsInScene );
	}
}
