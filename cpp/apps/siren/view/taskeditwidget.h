
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef TASK_EDIT_WIDGET_H
#define TASK_EDIT_WIDGET_H

#include <qdatetime.h>
#include <qmap.h>

#include "introview.h"
#include "task.h"
#include "ui_taskeditwidgetui.h"
#include "elementuser.h"

class KDatePicker;
class QCheckListItem;
class QListWidgetItem;

class TaskEditWidget : public IntroBase, public Ui::TaskEditWidgetUI
{
Q_OBJECT
public:
	TaskEditWidget( QWidget * parent );

	virtual void setElementList( ElementList );

	void refreshUserList();
	
public slots:

	void tuAdded( RecordList );
	void tuRemoved( RecordList );
	void tuUpdated( Record, Record );

	void popupMenu( const QPoint & );
	
	void editUsers();

	void showTimeSheetDialog( QDate );

protected:
	bool mRefreshLock;
	
	ElementList mTasks;
	QMap<User, QMap<Element,ElementUser> > mUserMap;
	UserList mUsers;
	KDatePicker * mDatePicker;
};

#endif // TASK_EDIT_WIDGET_H

