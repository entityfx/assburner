
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef SHOT_ITEMS_H
#define SHOT_ITEMS_H

#include <qtreewidget.h>
#include <qdatetime.h>
#include <qitemdelegate.h>
#include <qpainter.h>
#include <qpixmap.h>

#include "fastmodel.h"
#include "blurqt.h"
#include "recordtreeview.h"

#include "shot.h"
#include "task.h"
#include "user.h"

#include <math.h>

// color if valid
QVariant civ( const QColor & c );

struct ShotCache : public CacheBase
{
	Shot mShot;
	QString mFrames, mUsers2dString, mUsers3dString;
	QPixmap mPreview;

	UserList users2d, users3d;
	Task mVfxTask;

	void setup( const Record & r, const QModelIndex & );
	QVariant data( const QModelIndex & i, int role ) const;
	QString sortKey( const QModelIndex & i ) const;
	int cmp( const ShotCache & other, const QModelIndex & a, const QModelIndex & b, bool );
	Qt::ItemFlags flags( const QModelIndex & );
	Record getRecord();
};

typedef RecordModelImp<TreeNodeT<ShotCache> > ShotModel;

void setupShotView( QTreeView * );
void saveShotView( QTreeView * );

class MultiLineDelegate : public QItemDelegate
{
Q_OBJECT
public:
	MultiLineDelegate( QObject * parent=0 ) : QItemDelegate( parent ) {}
	~MultiLineDelegate() {}

	QSize sizeHint(const QStyleOptionViewItem &option,
                              const QModelIndex &index) const;
};

class WrapTextDelegate : public QItemDelegate
{
Q_OBJECT
public:
	WrapTextDelegate( QObject * parent=0 ) : QItemDelegate( parent ) {};
	~WrapTextDelegate() {}

	void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	QSize sizeHint( const QStyleOptionViewItem & option, const QModelIndex &index ) const;
	QWidget * createEditor( QWidget *, const QStyleOptionViewItem &, const QModelIndex & ) const;
	void setEditorData( QWidget *, const QModelIndex & ) const;
	void updateEditorGeometry(QWidget *editor,
	         const QStyleOptionViewItem &option, const QModelIndex &index) const;
	void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

};

#endif // SHOT_ITEMS_H

