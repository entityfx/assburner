
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef THUMBNAIL_BUTTON_H
#define THUMBNAIL_BUTTON_H

#include <qpushbutton.h>

#include "element.h"
#include "thumbnail.h"

class ThumbnailButton : public QPushButton
{

Q_OBJECT

public:
	ThumbnailButton ( QWidget * parent );
	
	virtual void dropEvent(QDropEvent* event);
	virtual void dragEnterEvent( QDragEnterEvent *e );
	
	void updateThumbnail( ElementList );
	
	Thumbnail thumbnail() const;

public slots:

	void setDefaultPixmap( QPixmap def );
	
	bool setThumbnail( const Thumbnail & tn );
	
	void chooseFile();
	void chooseFile(const QString & startPath, bool=false);
	
	void setElementList( ElementList list );
	void accept();

private:
	bool mDefault, mValid;
	ElementList mElements;
	ElementList commitList;
	Thumbnail mThumbnail;
	QPixmap mPixmap;
	QString mPath;
	QRect mClipRect;
};

#endif

