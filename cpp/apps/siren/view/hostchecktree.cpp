
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include "modeliter.h"

#include "hostchecktree.h"

#include "host.h"
#include "pathtracker.h"
#include "pathsynctarget.h"
#include "element.h"
#include "project.h"

struct HostCheckCache : public CacheBase
{
	Host mElement;
	Qt::CheckState mChecked;

	void setup( const Record & r, const QModelIndex & = QModelIndex() ) {
		mElement = r;
		mChecked = Qt::Unchecked;
	}

	Record getRecord() { return mElement; }

	QVariant data( const QModelIndex & i, int role ) {
		if( i.column() == 0 ) {
			if( role == Qt::CheckStateRole )
				return mChecked;
			else if( role == Qt::DisplayRole )
				return mElement.syncName();
		}
		return QVariant();
	}

	Qt::ItemFlags flags( const QModelIndex & ) {
		return Qt::ItemFlags( Qt::ItemIsUserCheckable | Qt::ItemIsEnabled );
	}

	bool setData( const QModelIndex & i, const QVariant & data, int role ) {
		if( i.column() == 0 && role == Qt::CheckStateRole ) {
			mChecked = Qt::CheckState(data.toInt());
			return true;
		}
		return false;
	}

	int cmp( const HostCheckCache & other, const QModelIndex &, const QModelIndex &, bool ) const {
		return compareRetI(mElement.syncName(), other.mElement.syncName());
	}
};

typedef RecordModelImp<TreeNodeT<HostCheckCache> > HostCheckModel;

HostCheckTree::HostCheckTree( QWidget * parent )
: RecordTreeView( parent )
, mReadOnly( false )
{
	QStringList headerLabels;
	headerLabels << "Host";
	HostCheckModel * ecm = new HostCheckModel( this );
	ecm->setHeaderLabels( headerLabels );

	mHosts = Host::select(" JOIN HostService ON (keyhost=fkeyhost) JOIN Service ON (keyservice=fkeyservice AND service = 'Sync') WHERE service = 'Sync'" );
	ecm->setRootList( mHosts );
	setModel( ecm );
	setSelectionMode( QAbstractItemView::ExtendedSelection );
	Table * et = Host::table();
	connect( et, SIGNAL( added( RecordList ) ), SLOT( slotElementsAdded( RecordList ) ) );
	connect( et, SIGNAL( removed( RecordList ) ), SLOT( slotElementsRemoved( RecordList ) ) );
	connect( et, SIGNAL( updated( Record, Record ) ), SLOT( slotElementUpdated( Record, Record ) ) );
	setColumnAutoResize( 0, true );
}

void HostCheckTree::setRootElement()
{
	model()->setRootList( mHosts );
	expandRecursive();
}

HostList HostCheckTree::elementsByState( Qt::CheckState state )
{
	HostList ret;
	RecordModel * m = model();
	for( ModelIter it(m,ModelIter::Recursive); it.isValid(); ++it )
		if( m->data(*it, Qt::CheckStateRole) == state )
			ret += m->getRecord(*it);
	return ret;
}

HostList HostCheckTree::checkedElements()
{
	return elementsByState( Qt::Checked );
}

HostList HostCheckTree::noChangeElements()
{
	return elementsByState( Qt::PartiallyChecked );
}

void HostCheckTree::setChecked( HostList els )
{
	RecordModel * m = model();
	for( ModelIter it(m,ModelIter::Recursive); it.isValid(); ++it )
		if( els.contains( m->getRecord(*it) ) )
			m->setData(*it,Qt::Checked,Qt::CheckStateRole);
}

void HostCheckTree::setNoChange( HostList els )
{
	RecordModel * m = model();
	for( ModelIter it(m,ModelIter::Recursive); it.isValid(); ++it )
		if( els.contains( m->getRecord(*it) ) )
			m->setData(*it,Qt::PartiallyChecked,Qt::CheckStateRole);
}

void HostCheckTree::slotElementsAdded( RecordList  )
{
}

void HostCheckTree::slotElementUpdated( Record , Record )
{
}

void HostCheckTree::slotElementsRemoved( RecordList  )
{
}

void HostCheckTree::buildSyncTree(ElementList selected) {
  setRootElement();

  QMap<uint, uint> depMap;
  foreach( Element e, selected )
  {
		VarList vl;
		vl << e.key();
		PathSyncTargetList installed = PathSyncTarget::select(" JOIN PathTracker ON ( fkeypathtracker = keypathtracker ) WHERE fkeyelement=?", vl);

    foreach( PathSyncTarget pst, installed )
    {
      uint key = pst.key();
      if( !depMap.contains( key ) )
        depMap[key] = 1;
      else
        depMap[key]++;
    }
  }

  HostList checked, tri;
  for( QMap<uint, uint>::Iterator it = depMap.begin(); it != depMap.end(); ++it ){
    if( it.value() == selected.size() )
      checked += Host( it.key() );
    else
      tri += Host( it.key() );
  }

  setChecked( checked );
  setNoChange( tri );

  resizeColumnToContents(0);
  resizeColumnToContents(1);
  resizeColumnToContents(2);
}

void HostCheckTree::saveSpoolTree(ElementList selected) {
  HostList on = checkedElements();
  HostList nc = noChangeElements();
  foreach( Element e, selected )
  {
		VarList vl;
		vl << e.key();
		PathSyncTargetList installed = PathSyncTarget::select(" JOIN PathTracker ON ( fkeypathtracker = keypathtracker ) WHERE fkeyelement=?", vl);

    PathSyncTargetList toRemove;
    foreach( PathSyncTarget pst, installed )
      if( !on.contains( pst.projectStorage().host() ) && !nc.contains( pst.projectStorage().host() ) )
        toRemove += pst;
		toRemove.remove();

    foreach( Host h, on ) {
      if( !installed.projectStorages().hosts().contains( h ) ) {
				VarList vl;
				vl << e.project().key();
				vl << h.key();
				ProjectStorageList targets = ProjectStorage::select("fkeyproject = ? AND fkeyhost = ?", vl);
				if( !targets.size() )
					continue;
				PathSyncTarget pst = PathSyncTarget();
        pst.setProjectStorage(targets[0]);
				//pst.setPathTracker( e.pathTracker("SMO") );
        pst.commit();
      }
    }
  }
}
