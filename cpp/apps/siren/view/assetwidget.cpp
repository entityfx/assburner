
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qcombobox.h>
#include <qgroupbox.h>
#include <qspinbox.h>

#include "asset.h"
#include "assettype.h"
#include "assetwidget.h"
#include "elementchecktree.h"
#include "elementdep.h"
#include "project.h"
#include "record.h"
#include "shot.h"
#include "shotgroup.h"

AssetWidget::AssetWidget( QWidget * parent )
: IntroBase( parent )
, mIgnoreDepUpdates( false )
{
	setupUi( this );
	connect( ElementDep::table(), SIGNAL( added( RecordList ) ),
		SLOT( slotElementDepsAddedOrRemoved( RecordList ) ) );
	connect( ElementDep::table(), SIGNAL( removed( RecordList ) ),
		SLOT( slotElementDepsAddedOrRemoved( RecordList ) ) );
}

void AssetWidget::createSideWidget( QWidget * parent )
{
	mSideWidget = new QGroupBox( "Shot Deps", parent );
	//mSideWidget->setFixedWidth( 200 );
	QLayout * layout = new QVBoxLayout( mSideWidget );
	mCheckTree = new ElementCheckTree( mSideWidget );
	layout->addWidget( mCheckTree );
	layout->setMargin( 6 );
	ElementTypeList etl;
	etl += Shot::type();
	etl += ShotGroup::type();
	mCheckTree->setElementTypeList( etl );
}

QWidget * AssetWidget::sideWidget()
{
	return mSideWidget;
}

void AssetWidget::slotElementDepsAddedOrRemoved( RecordList recs )
{
	ElementDepList deps( recs );

	if( mIgnoreDepUpdates )
		return;

	// If the intersection of the two lists is not empty...
	if( (deps.elementDeps() & mElements).size() ) {
		ElementList el = mElements;
		mElements.clear();
		setElementList( el );
	}
}

void AssetWidget::setElementList( ElementList el )
{
	if( mElements.size() ){
		mIgnoreDepUpdates = false;
		ElementList on = mCheckTree->checkedElements( Shot::type() ), nc = mCheckTree->noChangeElements( Shot::type() );
		foreach( Element e, mElements )
		{
			ElementList deps = e.dependants();
			for( ElementIter dep = deps.begin(); dep != deps.end(); ){
				if( !on.contains( *dep ) && !nc.contains( *dep ) )
					dep = deps.remove( dep );
				else
					++dep;
			}
			foreach( Element e2, on )
				if( !deps.contains( e2 ) )
					deps += e2;
			e.setDependants( deps );
		}
		mIgnoreDepUpdates = false;
	}

	mElements = el;

	if( el.size() ){
		Element element = el[0];
		Element pe = element.project();
		if( pe.isRecord() && pe != mProjectElement ){
			mProjectElement = pe;
			ElementList sg = pe.children( ShotGroup::type() );
			if( sg.size()==1 )
				mCheckTree->setRootElement( sg[0] );
		}
	}

	QMap<uint, uint> depMap;
	foreach( Element e, mElements )
	{
		ElementList deps = e.dependants();
		foreach( Element dep, deps )
		{
			uint key = dep.key();
			if( !depMap.contains( key ) )
				depMap[key] = 1;
			else
				depMap[key]++;
		}
	}

	ElementList checked, tri;
	for( QMap<uint, uint>::Iterator it = depMap.begin(); it != depMap.end(); ++it ){
		if( it.value() == mElements.size() )
			checked += Element( it.key() );
		else
			tri += Element( it.key() );
	}

	mCheckTree->setChecked( checked );
	mCheckTree->setNoChange( tri );
}

