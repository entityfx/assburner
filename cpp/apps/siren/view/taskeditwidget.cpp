
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qcombobox.h>
#include <qgroupbox.h>
#include <qlistview.h>
#include <qmenu.h>
#include <qpushbutton.h>
#include <qspinbox.h>

#include "database.h"
#include "taskeditwidget.h"
#include "timeentrydialog.h"
#include "usertaskdialog.h"
#include "elementuser.h"
#include "employee.h"

TaskEditWidget::TaskEditWidget( QWidget * parent )
: IntroBase( parent )
, mRefreshLock( false )
{
	setupUi(this);
	connect( mEditUsers, SIGNAL( clicked() ), SLOT( editUsers() ) );
	mUserList->setContextMenuPolicy( Qt::CustomContextMenu );
	connect( mUserList, SIGNAL( customContextMenuRequested( const QPoint & ) ),
				SLOT( popupMenu( const QPoint & ) ) );
	connect( ElementUser::table(), SIGNAL( added( RecordList ) ), SLOT( tuAdded( RecordList ) ) );
	connect( ElementUser::table(), SIGNAL( removed( RecordList ) ), SLOT( tuRemoved( RecordList ) ) );
	connect( ElementUser::table(), SIGNAL( updated( Record, Record ) ), SLOT( tuUpdated( Record, Record ) ) );

	mUserList->setColumnCount(1);
	QStringList headers;
	headers += "User Name";
	//headers += "Role";
	mUserList->setHeaderLabels( headers );
	mUserList->setRootIsDecorated(false);
}

void TaskEditWidget::tuAdded( RecordList recs )
{
	ElementUserList ul( recs );
	// && returns true if the lists share any records
	if( mTasks && ul.elements() )
		refreshUserList();
}

void TaskEditWidget::tuRemoved( RecordList recs )
{
	ElementUserList ul( recs );
	// && returns true if the lists share any records
	if( mTasks && ul.elements() )
		refreshUserList();
}

void TaskEditWidget::tuUpdated( Record r, Record ro )
{
	ElementUser cur( r ), old( ro );
	if( mTasks.contains( cur.element() ) || mTasks.contains( old.element() ) ) {
		refreshUserList();
		return;
	}
}

void TaskEditWidget::setElementList( ElementList list )
{
	mTasks = list;
	refreshUserList();
}

void TaskEditWidget::editUsers()
{
	UserTaskDialog * utd = new UserTaskDialog( this );
	utd->setUsers( mUsers );
	if( utd->exec() == QDialog::Accepted ){
		qWarning("user edit accepted");
		if( mRefreshLock )
			return;

		qWarning("refreshes locked");
		mRefreshLock = true;
		
		Database::current()->beginTransaction( "Edit Assigned Users" );
		UserList users = utd->userList();
		foreach( User u, users )
			if( !mUsers.contains( u ) ){
				foreach( Element task, mTasks )
				{
					task.addUser( u );
					mUserMap[u][task] = u;
				}
				mUsers += u;
			}
		foreach( User user, mUsers )
			if( !users.contains( user ) ) {
				foreach( Element task, mTasks )
				{
					task.removeUser( user );
					mUserMap.remove( user );
				}
			}
		Database::current()->commitTransaction();
		mRefreshLock = false;
		refreshUserList();
	}
	delete utd;
}

void TaskEditWidget::popupMenu( const QPoint & pos )
{
/*	QListWidgetItem * itm = mUserList->itemAt( pos );
	if( itm )
	{
		Database::current()->beginTransaction( "Edit Assigned Users" );
		
		// Selected User
		User u( mUsers[mUserList->row(itm)] );
		
		// Task records for that user, indexed by task
		QMap<Element, ElementUser> taskMap = mUserMap[u];
		
		// Create a checkable popup-menu
		QMenu * menu = new QMenu( this );
		
		// Add each of the tasks, a separator, all, and remove
		int index = 0;
		st_foreach( ElementIter, it, mTasks ) {
			QAction * tmp = menu->addAction( (*it).displayPath() );
			tmp->setProperty( "index", index++ );
			tmp->setCheckable( true );
			tmp->setChecked( mUserMap[u.key()].contains( (*it).key() ) );
		}

		menu->addSeparator();
		QAction * all = menu->addAction( "All" );
		QAction * rem = menu->addAction( "Remove" );
		
		QAction * res = menu->exec( pos );
		if( !res ) {
			delete menu;
			return;
		}

		if( res == all || res == rem ) {
			mRefreshLock = true;
			// All and remove both require a loop
			st_foreach( ElementIter, it, mTasks ) {
				Task t( *it );
				QMap<Element,ElementUser>::Iterator tu_it = taskMap.find( t.key() );
				if( tu_it == taskMap.end() && res == all )
					t.addUser( u );
				else if( tu_it != taskMap.end() && res == rem )
					// Remove form remove - duh
					(*tu_it).remove();
			}
			mRefreshLock = false;
			refreshUserList();
		} else {
			mRefreshLock = true;
			// One of the tasks has been toggled
			Task t( mTasks[res->property( "index" ).toInt()] );
			QMap<Element,ElementUser>::Iterator tu_it = taskMap.find( t.key() );
			if( tu_it == taskMap.end() )
				t.addUser( u );
			else{
				(*tu_it).remove();
				taskMap.remove( t.key() );
			}
			mRefreshLock = false;
			refreshUserList();
		}
		delete menu;
		Database::current()->commitTransaction();
	} */
}

void TaskEditWidget::refreshUserList()
{
	if( mRefreshLock )
		return;
		
	mRefreshLock = true;
	mUserList->clear();
	mUsers.clear();
	foreach( Element e, mTasks ) {
		ElementUserList eul = ElementUser::recordsByElement( e );
		foreach( ElementUser eu, eul ) {
			QTreeWidgetItem * twi  = new QTreeWidgetItem(mUserList);
			twi->setText(0, eu.user().name());
			mUsers += eu.user();
		}
	}
	mUsers.unique();
	mRefreshLock = false;
}

void TaskEditWidget::showTimeSheetDialog( QDate d )
{
	TimeEntryDialog ted( this );
	ted.setAssetType( mTasks[0].assetType() );
	ted.setElementList( mTasks );
	ted.setDateRange( d );
	ted.exec();
}

