
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qcheckbox.h>
#include <qfiledialog.h>
#include <qlabel.h>
#include <qpixmap.h>
#include <qpushbutton.h>
#include <qtextedit.h>
#include <qurl.h>

#include "stickitintherect.h"
#include "thumbnailchooser.h"
#include "path.h"

ThumbnailChooser::ThumbnailChooser ( const QString & path, const QRect & rect, QWidget * parent )
: QDialog( parent )
, mRect( rect )
, mPath( path )
{
	setupUi( this );

	if( QFile::exists( path ) ){
		mImage = QImage( path );
		mThumbnailLabel->setImage( mImage );
		mThumbnailLabel->setRect( mRect );
		setRect( mRect );
	}
	setAcceptDrops( true );
	connect( mChooseFileButton, SIGNAL( clicked() ), SLOT( chooseFile() ) );
	connect( mThumbnailLabel, SIGNAL( rectChange( QRect ) ), SLOT( setRect( QRect ) ) );
	connect( mSquareAspectCheck, SIGNAL( toggled( bool ) ), mThumbnailLabel, SLOT( setSquareAspect( bool ) ) );
}

ThumbnailChooser::~ThumbnailChooser ()
{
}

int ThumbnailChooser::exec()
{
	if( !QFile::exists( mPath ) && !chooseFile() )
		return QDialog::Rejected;
	return QDialog::exec();
}

void ThumbnailChooser::dropEvent(QDropEvent* event)
{
	QStringList files;
	QList<QUrl> urls = event->mimeData()->urls();
	foreach( QUrl url, urls ) {
		QString path = url.toLocalFile();
		if( QFile::exists( path ) ) {
			QImage img( path );
			if( !img.isNull() ) {
				mPath = path;
				mImage = img;
				mThumbnailLabel->setImage( mImage );
				return;
			}
		}
	}
}

void ThumbnailChooser::dragEnterEvent( QDragEnterEvent *e )
{
	if( e->mimeData()->hasUrls() )
		e->accept();
}

void ThumbnailChooser::setRect( QRect r )
{
	//mThumbnail.setRect( r );
	mRect = r;
	QImage preview = mImage.copy( r );
	if( r.size() != QSize(120,120) )
		preview = preview.scaled( 120, 120, Qt::KeepAspectRatio );
	mPreviewLabel->setPixmap( QPixmap::fromImage(preview) );
}

bool ThumbnailChooser::chooseFile()
{
	// Open dialog
#ifdef Q_OS_WIN
	QString dp = "G:";
#else
	QString dp = "/home/newellm/Blur/View/Qt/Resin/images/";
#endif
	if( Path( mPath ).dir().exists() )
		dp = Path( mPath ).dirPath();
	QFileDialog * fd = new QFileDialog( this, "Choose Thumbnail", dp, "Images (*.png *.tga *.tga *.jpg * .exr *.bmp)" );
	fd->setViewMode( QFileDialog::Detail );
	fd->setFileMode( QFileDialog::ExistingFile );
	QStringList fileNames;
	bool exists = false;
	if (fd->exec() == QDialog::Accepted) {
		fileNames = fd->selectedFiles();
		exists = QFile::exists( fileNames[0] );
		if( exists ){
			mPath = fileNames[0];
			mImage = QImage( fileNames[0] );
			mThumbnailLabel->setImage( mImage );
			setRect( mImage.rect() );
		}
	}
	delete fd;

	return exists;
}

QPixmap ThumbnailChooser::pixmap() const
{
	return QPixmap::fromImage( mImage.copy( mThumbnailLabel->grabRect() ) );
}

QString ThumbnailChooser::path() const
{
	return mPath;
}

QRect ThumbnailChooser::rect() const
{
	return mRect;
}

void ThumbnailChooser::setRegionLocked()
{
	// set bounding box to fixed 120x120
}

