
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qfiledialog.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qmessagebox.h>
#include <qpushbutton.h>
#include <qspinbox.h>
#include <qtextedit.h>

#include "statusdialog.h"

#include "asset.h"
#include "assetwidget.h"
#include "blurqt.h"
#include "elementstatus.h"
#include "fieldlineedit.h"
#include "fieldtextedit.h"
#include "fieldspinbox.h"
#include "introview.h"
#include "path.h"
#include "projectwidget.h"
#include "projectstatus.h"
#include "resinerror.h"
#include "shot.h"
#include "shotgroup.h"
#include "shotwidget.h"
#include "statusset.h"
#include "task.h"
#include "taskeditwidget.h"
#include "thumbnail.h"
#include "thumbnailbutton.h"

static const QSizePolicy SPIgnored( QSizePolicy::Ignored, QSizePolicy::Ignored );
static const QSizePolicy SPMinimum( QSizePolicy::Minimum, QSizePolicy::Minimum );
static const QSizePolicy SPExpanding( QSizePolicy::Expanding, QSizePolicy::Expanding );
static const QSizePolicy SPMaximum( QSizePolicy::Maximum, QSizePolicy::Maximum );

class StatusItemCache : public CacheBase
{
public:
	Record status;
	void setup( const Record & r, const QModelIndex & = QModelIndex() ) {
		status = r;
	}
	QVariant data( const QModelIndex & i, int role ) const {
		ElementStatus es(status);
		if( role == Qt::DisplayRole || role == Qt::EditRole )
			return es.isRecord() ? es.name() : ProjectStatus(status).projectStatus();
		else if( es.isRecord() && (role == Qt::BackgroundColorRole || role == Qt::TextColorRole) ) {
			QColor c;
			c.setNamedColor(es.color());
			if( role == Qt::BackgroundColorRole )
				return c;
			return QColor(c.value() < 128 ? Qt::white : Qt::black);
		}
		return QVariant();
	}
	int cmp( const StatusItemCache & other, const QModelIndex &, const QModelIndex &, bool )
	{
		ElementStatus es(status);
		if( es.isRecord() )
			return es.order() - ElementStatus(other.status).order();
		ProjectStatus ps(status);
		return compareRetI(ps.projectStatus(), ProjectStatus(other.status).projectStatus());
	}
	Qt::ItemFlags flags( const QModelIndex & ) { return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled ); }
	Record getRecord() { return status; }
};

typedef RecordModelImp<TreeNodeT<StatusItemCache> > StatusItemModel;

IntroView::IntroView( QWidget * parent )
: ViewBase( parent )
, mChanges( false )
, mProjectMode( false )
, mCurrentView( 0 )
, mProjectWidget( 0 )
, mShotWidget( 0 )
, mAssetWidget( 0 )
, mTaskEditWidget( 0 )
{
	QBoxLayout * hlayout = new QHBoxLayout( this );
	hlayout->setMargin( 0 );
	hlayout->setSpacing( 4 );
	QWidget * s = new QWidget( this );
	hlayout->addWidget( s );

	QBoxLayout * layout = new QVBoxLayout( s );

	QWidget * w = new QWidget( s );
	layout->addWidget( w );
	mIntroView = new Ui::IntroViewUI;
	mIntroView->setupUi(w);

	mDetailStack = new QStackedWidget( s );
	mDetailStack->setSizePolicy( SPIgnored );
	mDetailStack->hide();
	
	layout->addWidget( mDetailStack );

	mLeftWidgetStack = new QStackedWidget( this );
	mLeftWidgetStack->hide();
	
	hlayout->addWidget( mLeftWidgetStack );
	
	connect( Element::table(), SIGNAL( updated( Record, Record ) ), SLOT( updateElement( Record ) ) );
	
	mProxy = new RecordProxy( this );

	mIntroView->mNameEdit->setProxy( mProxy );
	mIntroView->mDescription->setProxy( mProxy );
	mIntroView->mDaysBidSpin->setProxy( mProxy );
	mIntroView->mDaysEstSpin->setProxy( mProxy );
	
	StatusItemModel * sm = new StatusItemModel( mIntroView->mStatusCombo );
	mIntroView->mStatusCombo->setModel( sm );
	connect( mIntroView->mStatusCombo, SIGNAL( currentChanged( const Record & ) ), SLOT( statusChanged( const Record & ) ) );
}

IntroView::~IntroView()
{
	delete mIntroView;
}

bool IntroView::viewForType( ElementList el, bool )
{
	return UserList( el ).isEmpty();
}

void IntroView::setElementList( ElementList el )
{
	// Use a temporary list, so that updates aren't caught in updateElement
	ElementList oldElements = mElements;
	mElements.clear();

	// Commit the bottom view
	if( mCurrentView )
		mCurrentView->setElementList( mElements );
	
	// Commit the stuff from this view
	{
		//
		// Warn the user before changing the name of something
		//
		if( mIntroView->mNameEdit->changed() )
		{
			bool rename = true;
			QString name = mIntroView->mNameEdit->text();
			foreach( Element e, oldElements )
				if( e.elementType() == Project::type() && Project::recordByName( name ).isRecord() )
				{
					ResinError::nameTaken( this, name );
					rename = false;
					break;
				}
			if( rename ) {
				Element::invalidatePathCache();
			} else {
				mIntroView->mNameEdit->reset();
			}
		}
		
		mProxy->applyChanges(false);
		oldElements = mProxy->records();

		mProxy->setRecordList( el );
	
		Record status = mIntroView->mStatusCombo->current();
		if( mProjectMode )
			ProjectList(oldElements).setProjectStatuses( status );
		else
			oldElements.setElementStatuses( status );
	
		if( oldElements.size() == 1 ) {
			oldElements.setStartDates( mIntroView->mDateStart->date() );
			oldElements.setDatesComplete( mIntroView->mDateComplete->date() );
		}

		oldElements.commit();
	}

	//
	// Setup new elements
	mElements = el;

	mIntroView->mPixmapLabel->setElementList( el );

	Element element = el.size() ? el[0] : Element();

	mIntroView->mStatusCombo->clear();
	
	Project p(element);
	mProjectMode = p.isRecord();

	mIntroView->mDateStart->setDate( element.startDate() );
	mIntroView->mDateComplete->setDate( element.dateComplete() );

	if( mProjectMode ) {
		mIntroView->mStatusCombo->setItems( ProjectStatus::select().sorted("chronology") );
		mIntroView->mStatusCombo->setCurrent( p.projectStatus() );
	} else {
		ElementStatus cur = element.elementStatus();
		mIntroView->mStatusCombo->setItems( cur.statusSet().elementStatuses().sorted("order") );
		mIntroView->mStatusCombo->setCurrent(cur);
	}

	bool showSideWidget = false;
	ElementType et = element.elementType();
	
	IntroBase * oldView = mCurrentView;
	
	mCurrentView = 0;
	
	QWidget * lw = mLeftWidgetStack->currentWidget();

	/*
	 * All "assets" can have people assigned to them
	 * now, so we need to merge this into the intro
	 * view
	 */
	if( !mTaskEditWidget ){
		mTaskEditWidget = new TaskEditWidget( mDetailStack );
		mDetailStack->addWidget( mTaskEditWidget );
	}
	mTaskEditWidget->setElementList( mElements );
	mDetailStack->setCurrentWidget( mTaskEditWidget );
	mTaskEditWidget->setSizePolicy( SPMinimum );
	mCurrentView = mTaskEditWidget;

	if( et == Project::type() && el.size() == ProjectList(el).size() ){
		if( !mProjectWidget ){
			mProjectWidget = new ProjectWidget( mDetailStack );
			mDetailStack->addWidget( mProjectWidget );
			mProjectWidget->show();
		}
		mProjectWidget->setElementList( mElements );
		mDetailStack->setCurrentWidget( mProjectWidget );
		mProjectWidget->setSizePolicy( SPMinimum );
		mCurrentView = mProjectWidget;
	}
	else if( et == Shot::type() && el.size() == ShotList(el).size() ){
		if( !mShotWidget ){
			mShotWidget = new ShotWidget( mDetailStack );
			mDetailStack->addWidget( mShotWidget );
			mShotWidget->show();
		}
		mLeftWidgetStack->setCurrentWidget( mShotWidget->sideWidget() );
		mShotWidget->setElementList( mElements );
		mDetailStack->setCurrentWidget( mShotWidget );
		mShotWidget->setSizePolicy( SPMinimum );
		mCurrentView = mShotWidget;
	}
	else if( et == Asset::type() && el.size() == AssetList(el).size() ){
		if( !mAssetWidget ){
			mAssetWidget = new AssetWidget( mDetailStack );
			mDetailStack->addWidget( mAssetWidget );
			mAssetWidget->createSideWidget( mLeftWidgetStack );
			mLeftWidgetStack->addWidget( mAssetWidget->sideWidget() );
		}
		mLeftWidgetStack->setCurrentWidget( mAssetWidget->sideWidget() );
		mAssetWidget->setElementList( mElements );
		mDetailStack->setCurrentWidget( mAssetWidget );
		mAssetWidget->setSizePolicy( SPIgnored );
		mCurrentView = mAssetWidget;
		showSideWidget = true;
	}

	if( oldView && mCurrentView != oldView ){
		if( oldView == mProjectWidget )
			mProjectWidget->setSizePolicy( SPIgnored );
		
		if( oldView == mShotWidget )
			mShotWidget->setSizePolicy( SPIgnored );
		
		if( oldView == mAssetWidget )
			mAssetWidget->setSizePolicy( SPIgnored );
		
		if( oldView == mTaskEditWidget )
			mTaskEditWidget->setSizePolicy( SPIgnored );
	}
	
	if( mCurrentView ){
		mDetailStack->show();
		mDetailStack->setSizePolicy( SPExpanding );
	}else{
		mDetailStack->hide();
		mDetailStack->setSizePolicy( SPIgnored );
	}
	
	if( lw && lw != mLeftWidgetStack->currentWidget() )
		lw->hide();
		
	if( showSideWidget ){
		mLeftWidgetStack->show();
		mLeftWidgetStack->setSizePolicy( SPMinimum );
	}else{
		mLeftWidgetStack->hide();
		mLeftWidgetStack->setSizePolicy( SPIgnored );
	}
}

void IntroView::updateElement( Record r )
{
	Element element( r );
	if( mElements.contains( element ) ){
		ElementList temp = mElements;
		mElements.clear();
		setElementList( temp );
	}
}

void IntroView::statusChanged( const Record & )
{
}

