
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qaction.h>
#include <qactiongroup.h>
#include <qmainwindow.h>
#include <qlayout.h>
#include <qcombobox.h>
#include <qcheckbox.h>
#include <qmenu.h>
#include <qtimer.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>

#include "actions.h"
#include "iniconfig.h"
#include "scheduleprintdialog.h"
#include "scheduleview.h"
#include "schedulewidget.h"
#include "elementschedulecontroller.h"
#include "eventdialog.h"
#include "timeentrydialog.h"
#include "scheduledialog.h"
#include "projectstatus.h"

ScheduleView::ScheduleView( QWidget * parent )
: ViewBase( parent )
, mController( 0 )
, mScheduleWidget( 0 )
, mToolBar( 0 )
, mNewTimeSheet( 0 )
, mNewSchedule( 0 )
, mNewEvent( 0 )
, mPrintAction( 0 )
, mWeekAction( 0 )
, mMonthAction( 0 )
, mMonthFlatAction( 0 )
, mYearAction( 0 )
, mShowWeekendsAction( 0 )
, mShowTimeSheetsAction( 0 )
, mShowSchedulesAction( 0 )
, mShowCalendarEntriesAction( 0 )
, mShowRecursiveAction( 0 )
, mDurationAction( 0 )
, mDurationSpin( 0 )
, mDurationContainer( 0 )
, mAllCategoriesAction( 0 )
, mClearCategoriesAction( 0 )
, mAllEventsAction( 0 )
, mClearEventFiltersAction( 0 )
, mGlobalEventsAction( 0 )
, mProjectFiltersButton( 0 )
, mEventCategoryFiltersButton( 0 )
, mMyEventsOnlyAction( 0 )
{
	// Create controller
	mController = new ElementScheduleController();

	// Create widget
	mScheduleWidget = new ScheduleWidget( mController, this );
	mScheduleWidget->setShowWeekends( true );

	mNewTimeSheet = new QAction( QIcon( "images/newTimesheet.png" ), "Record TimeSheet", this );
	connect( mNewTimeSheet, SIGNAL( triggered( bool ) ), SLOT( recordTimeSheet() ) );

	mNewSchedule = new QAction( QIcon( "images/newSchedule.png" ), "New Schedule", this );
	connect( mNewSchedule, SIGNAL( triggered( bool ) ), SLOT( newSchedule() ) );

	mNewEvent = new QAction( QIcon( "images/newEvent.png" ), "New Event", this );
	connect( mNewEvent, SIGNAL( triggered( bool ) ), SLOT( newEvent() ) );

	mPrintAction = new QAction( "Print", this );
	connect( mPrintAction, SIGNAL( triggered( bool ) ), SLOT( print() ) );

	mWeekAction = new QAction( QIcon( "images/weekView.png" ), "Week View", this );
	mMonthAction = new QAction( QIcon( "images/monthView.png" ), "Month View", this );
	mMonthFlatAction = new QAction( QIcon( "images/monthFlat.png" ), "Month Flat View", this );
	mYearAction = new QAction( QIcon( "images/yearFlat.png" ), "Year Flat View", this );

	QActionGroup * grp = new QActionGroup( this );
	grp->addAction( mWeekAction );
	grp->addAction( mMonthAction );
	grp->addAction( mMonthFlatAction );
	grp->addAction( mYearAction );

	foreach( QAction * a, grp->actions() )
		a->setCheckable( true );

	connect( grp, SIGNAL( triggered( QAction * ) ), SLOT( displayModeChange( QAction * ) ) );
	mMonthAction->setChecked( true );
	
	mShowWeekendsAction = new QAction( QIcon( "images/show_weekends.png" ), "Show Weekends", this );
	mShowWeekendsAction->setCheckable( true );
	mShowWeekendsAction->setChecked( true );
	connect( mShowWeekendsAction, SIGNAL( triggered( bool ) ), mScheduleWidget, SLOT( setShowWeekends( bool ) ) );

	mShowTimeSheetsAction = new QAction( QIcon( "images/showTimesheets.png" ), "Show TimeSheets", this );
	mShowTimeSheetsAction->setCheckable( true );
	mShowTimeSheetsAction->setChecked( true );
	mShowSchedulesAction = new QAction( QIcon( "images/showSchedule.png" ), "Show Schedules", this );
	mShowSchedulesAction->setCheckable( true );
	mShowSchedulesAction->setChecked( true );
	mShowCalendarEntriesAction = new QAction( QIcon( "images/showCalendar.png" ), "Show Events", this );
	mShowCalendarEntriesAction->setCheckable( true );
	mShowCalendarEntriesAction->setChecked( false );

	mShowRecursiveAction = new QAction( QIcon( "images/recursiveAssets.png" ), "Show Assets Recursivly", this );
	mShowRecursiveAction->setCheckable( true );
	mShowRecursiveAction->setChecked( true );

	connect( mShowTimeSheetsAction, SIGNAL( triggered( bool ) ), mController, SLOT( setShowTimeSheets( bool ) ) );
	connect( mShowSchedulesAction, SIGNAL( triggered( bool ) ), mController, SLOT( setShowSchedules( bool ) ) );
	connect( mShowCalendarEntriesAction, SIGNAL( triggered( bool ) ), mController, SLOT( setShowCalendars( bool ) ) );
	connect( mShowCalendarEntriesAction, SIGNAL( triggered( bool ) ), this, SLOT( updateToolbarActionStates() ) );
	connect( mShowRecursiveAction, SIGNAL( triggered( bool ) ), mController, SLOT( setShowRecursive( bool ) ) );
	
	QBoxLayout * layout = new QVBoxLayout( this );
	layout->setMargin( 0 );
	layout->addWidget( mScheduleWidget );

	connect( TimeSheet::table(), SIGNAL( added( RecordList ) ), SLOT( timeSheetsAdded( RecordList ) ) );
	connect( Schedule::table(), SIGNAL( added( RecordList ) ), SLOT( schedulesAdded( RecordList ) ) );

	mDurationContainer = new QWidget( mToolBar );
	QLayout * durLayout = new QHBoxLayout( mDurationContainer );
	QLabel * durLabel = new QLabel( "Duration: ", mDurationContainer );

	mDurationSpin = new QSpinBox( mDurationContainer );
	durLayout->addWidget( durLabel );
	durLayout->addWidget( mDurationSpin );

	mDurationSpin->setRange( 1, 12 );

	IniConfig & c = userConfig();
	c.pushSection( "Calendar" );

	bool showRecursive = c.readBool( "ShowRecursive", true );
	mShowRecursiveAction->setChecked( showRecursive );
	mController->setShowRecursive( showRecursive );

	bool showTimeSheets = c.readBool( "ShowTimeSheets", true );
	mShowTimeSheetsAction->setChecked( showTimeSheets );
	mController->setShowTimeSheets( showTimeSheets );

	bool showSchedules = c.readBool( "ShowSchedules", true );
	mShowSchedulesAction->setChecked( showSchedules );
	mController->setShowSchedules( showSchedules );

	bool showEvents = c.readBool( "ShowEvents", false );
	mShowCalendarEntriesAction->setChecked( showEvents );
	mController->setShowCalendars( showEvents );

	bool showWeekends = c.readBool( "ShowWeekends", true );
	mShowWeekendsAction->setChecked( showWeekends );
	mScheduleWidget->setShowWeekends( showWeekends );

	int displayMode = c.readInt( "DisplayMode", ScheduleWidget::Week );
	mScheduleWidget->setDisplayMode( displayMode );
	mController->setShowTimeSpans( displayMode != ScheduleWidget::Month );

	mWeekAction->setChecked( displayMode == ScheduleWidget::Week );
	mMonthAction->setChecked( displayMode == ScheduleWidget::Month );
	mMonthFlatAction->setChecked( displayMode == ScheduleWidget::MonthFlat );
	mYearAction->setChecked( displayMode == ScheduleWidget::Year );

	mController->setShowMyEventsOnly( c.readBool( "ShowMyEventsOnly", false ) );

	mMyEventsOnlyAction = checkableAction( "My Events Only", this, mController->showMyEventsOnly(), this, SLOT( showMyEventsOnly(bool) ), QIcon( "images/show_mine.png" ) );

	int zoom = c.readInt( "Zoom", 100 );
	mScheduleWidget->setZoom( zoom );

	mController->setShowCalendarCategories( CalendarCategory::table()->records( c.readString( "CalendarCategories", CalendarCategory::select().keyString() ) ) );

	mController->setProjectFilters( Project::table()->records( c.readString( "ProjectFilters", Project::select("fkeyprojectstatus=4").keyString() ) ) );


	mController->setShowGlobalEvents( c.readBool( "ShowGlobalEvents", true ) );

	mDurationSpin->setValue( c.readInt( "Duration", 1 ) );

	c.popSection();

	connect( mDurationSpin, SIGNAL( valueChanged( int ) ), SLOT( durationChanged( int ) ) );
	updateDuration();
}

ScheduleView::~ScheduleView()
{
	IniConfig & c = userConfig();
	c.pushSection( "Calendar" );
	c.writeBool( "ShowRecursive", mController->showRecursive() );
	c.writeBool( "ShowTimeSheets", mController->showTimeSheets() );
	c.writeBool( "ShowSchedules", mController->showSchedules() );
	c.writeBool( "ShowEvents", mController->showCalendars() );
	c.writeBool( "ShowWeekends", mScheduleWidget->showWeekends() );
	c.writeInt( "DisplayMode", mScheduleWidget->displayMode() );
	c.writeInt( "Zoom", mScheduleWidget->zoom() );
	c.writeString( "CalendarCategories", mController->calendarCategories().keyString() );
	c.writeInt( "Duration", mDurationSpin ? mDurationSpin->value() : 1 );
	c.writeString( "ProjectFilters", mController->projectFilters().keyString() );
	c.writeBool( "ShowMyEventsOnly", mController->showMyEventsOnly() );
	c.writeBool( "ShowGlobalEvents", mController->showGlobalEvents() );
	c.popSection();
}

void ScheduleView::setupEventCategoryActions( QMenu * menu )
{
	if( mCalCatActions.isEmpty() ) {
		CalendarCategoryList ccl = mAllCalendarCategories = CalendarCategory::select().sorted("name");
		CalendarCategoryList cur = mController->calendarCategories();
		mAllCategoriesAction = checkableAction( "All", this, (ccl-cur).isEmpty(), this, SLOT( allCalendarCategories() ) );
		mClearCategoriesAction = checkableAction( "None", this, (ccl-cur).isEmpty(), this, SLOT( clearCalendarCategories() ) );
		foreach( CalendarCategory cc, ccl ) {
			QAction * a = new QAction( /*QIcon( "images/event_category_" + (*it).name() + ".png" ),*/ cc.name(), this );
			connect( a, SIGNAL( triggered( bool ) ), SLOT( calActionTriggered() ) );
			a->setCheckable( true );
			a->setChecked( cur.contains( cc ) );
			mCalCatActions += qMakePair(a,cc);
		}
	}
	menu->addAction( mAllCategoriesAction );
	menu->addAction( mClearCategoriesAction );
	menu->addSeparator();
	foreach( CalCatAction p, mCalCatActions )
		menu->addAction( p.first );
}

void ScheduleView::calActionTriggered()
{
	QObject * s = sender();
	if( s && s->inherits( "QAction" ) ) {
		foreach( CalCatAction p, mCalCatActions ) {
			if( p.first == s ) {
				CalendarCategoryList ccl = mController->calendarCategories();
				if( ccl.contains( p.second ) )
					ccl -= p.second;
				else
					ccl += p.second;
				mController->setShowCalendarCategories( ccl );
				break;
			}
		}
	}
	mAllCategoriesAction->setChecked( (mAllCalendarCategories - mController->calendarCategories()).isEmpty() );
	mClearCategoriesAction->setChecked( mController->calendarCategories().isEmpty() );
}

void ScheduleView::allCalendarCategories()
{
	QPair<QAction*,CalendarCategory> p;
	foreach( p, mCalCatActions )
		p.first->setChecked( true );
	mAllCategoriesAction->setChecked( true );
	mClearCategoriesAction->setChecked( false );
}

void ScheduleView::clearCalendarCategories()
{
	QPair<QAction*,CalendarCategory> p;
	foreach( p, mCalCatActions )
		p.first->setChecked( false );
	mAllCategoriesAction->setChecked( false );
	mClearCategoriesAction->setChecked( true );
}

void ScheduleView::setupProjectFilterActions( QMenu * menu )
{
	if( mProjectFilterActions.isEmpty() ) {
		mAllProjectFilters = Project::select("fkeyprojectstatus=?", VarList() << ProjectStatus::recordByName("Production").key()).sorted("name");
		ProjectList cur = mController->projectFilters();
		bool allEventsChecked = (mAllProjectFilters & cur).size() == mAllProjectFilters.size() && mController->showGlobalEvents();
		mAllEventsAction = checkableAction( "All Events", this, allEventsChecked, this, SLOT( showAllEventsTriggered(bool) ) );
		if( allEventsChecked ) mAllEventsAction->setEnabled( false );
		mClearEventFiltersAction = checkableAction( "None", this, cur.isEmpty() && !mController->showGlobalEvents(), this, SLOT( clearEventFiltersTriggered(bool)));
		mGlobalEventsAction = checkableAction( "Other Events (not project related)", this, mController->showGlobalEvents(), this, SLOT( globalEventsActionTriggered(bool) ) );
		foreach( Project p, mAllProjectFilters ) {
			QAction * action = checkableAction(p.name(),this, cur.contains(p), this, SLOT( projectFilterActionTriggered(bool) ) );
			action->setProperty( "project", qVariantFromValue<Record>(p) );
			mProjectFilterActions += action;
		}
	}
	menu->addAction( mAllEventsAction );
	menu->addAction( mClearEventFiltersAction );
	menu->addSeparator();
	menu->addAction( mGlobalEventsAction );
	menu->addSeparator();
	foreach( QAction * action, mProjectFilterActions )
		menu->addAction( action );
}

void ScheduleView::projectFilterActionTriggered(bool checked)
{
	QObject * s = sender();
	if( s && s->inherits( "QAction" ) ) {
		QAction * a = qobject_cast<QAction*>(s);
		Project p = qVariantValue<Record>(a->property("project"));
		ProjectList cur = mController->projectFilters();
		if( checked )
			cur += p;
		else
			cur -= p;
		mController->setProjectFilters( cur );
		bool allEventsChecked = ( (mAllProjectFilters - cur).isEmpty() && mController->showGlobalEvents() );
		mAllEventsAction->setChecked( allEventsChecked );
		mAllEventsAction->setEnabled( !allEventsChecked );
		mClearEventFiltersAction->setChecked( cur.isEmpty() && !mController->showGlobalEvents() );
	}
}

void ScheduleView::showAllEventsTriggered( bool showAllEvents )
{
	if( showAllEvents ) {
		mGlobalEventsAction->setChecked( true );
		mClearEventFiltersAction->setChecked( false );
		foreach( QAction * action, mProjectFilterActions )
			action->setChecked( true );
		mController->setProjectFilters( mAllProjectFilters );
		mController->setShowGlobalEvents( true );
	}
}

void ScheduleView::clearEventFiltersTriggered( bool clearEvents )
{
	if( clearEvents ) {
		mGlobalEventsAction->setChecked( false );
		foreach( QAction * action, mProjectFilterActions )
			action->setChecked( false );
		mController->setProjectFilters( ProjectList() );
		mController->setShowGlobalEvents( false );
		mAllEventsAction->setChecked( false );
	}
}

void ScheduleView::globalEventsActionTriggered( bool showGlobalEvents )
{
	mController->setShowGlobalEvents( showGlobalEvents );
	bool allEventsChecked = ( (mAllProjectFilters - mController->projectFilters()).isEmpty() && mController->showGlobalEvents() );
	mAllEventsAction->setChecked( allEventsChecked );
	mAllEventsAction->setEnabled( !allEventsChecked );
	mClearEventFiltersAction->setChecked( mController->projectFilters().isEmpty() && !mController->showGlobalEvents() );
}

QToolBar * ScheduleView::toolBar( QMainWindow * win )
{
	if( mToolBar ) 
		mToolBar->clear();
	else
		mToolBar = win->addToolBar( "Schedule ToolBar" );

	mToolBar->setIconSize( QSize(20,20) );

	mToolBar->addAction( mNewTimeSheet );
	if( User::hasPerms( "Schedule", true ) )
		mToolBar->addAction( mNewSchedule );
	//if( User::hasPerms( "Event", true ) )
		mToolBar->addAction( mNewEvent );
	mToolBar->addSeparator();
	mToolBar->addAction( mWeekAction );
	mToolBar->addAction( mMonthAction );
	mToolBar->addAction( mMonthFlatAction );
	mToolBar->addAction( mYearAction );
	mToolBar->addSeparator();
	mToolBar->addAction(mShowRecursiveAction);
	mToolBar->addAction(mShowWeekendsAction);
	mToolBar->addSeparator();
	mToolBar->addAction(mShowTimeSheetsAction);
	mToolBar->addAction(mShowSchedulesAction);
	mToolBar->addAction(mShowCalendarEntriesAction);

	mToolBar->addSeparator();

	mToolBar->addAction( mMyEventsOnlyAction );

	mDurationAction = mToolBar->addWidget( mDurationContainer );
	mDurationAction->setVisible( mScheduleWidget->displayMode() == ScheduleWidget::MonthFlat );

	if( !mProjectFiltersButton ) {
		mProjectFiltersButton = new QToolButton( mToolBar );
		mProjectFiltersButton->setToolTip( "Event Project Filters" );
		mProjectFiltersButton->setIcon( QIcon( "images/project.png" ) );
		mProjectFiltersButton->setPopupMode(QToolButton::InstantPopup);
		QMenu * menu = new QMenu( mProjectFiltersButton );
		mProjectFiltersButton->setMenu( menu );
		setupProjectFilterActions( menu );
	}
	mToolBar->addWidget( mProjectFiltersButton );

	if( !mEventCategoryFiltersButton ) {
		mEventCategoryFiltersButton = new QToolButton( mToolBar );
		mEventCategoryFiltersButton->setToolTip( "Event Categories" );
		mEventCategoryFiltersButton->setIcon( QIcon( "images/event_category_filter.png" ) );
		mEventCategoryFiltersButton->setPopupMode(QToolButton::InstantPopup);
		QMenu * menu = new QMenu( mEventCategoryFiltersButton );
		mEventCategoryFiltersButton->setMenu( menu );
		setupEventCategoryActions( menu );
		//mToolBar->setToolButtonStyle( Qt::ToolButtonTextBesideIcon );
	}
	mToolBar->addWidget( mEventCategoryFiltersButton );
	return mToolBar;
}

void ScheduleView::updateToolbarActionStates()
{
	bool showingCals = mController->showCalendars();
	mMyEventsOnlyAction->setEnabled( showingCals );
	mProjectFiltersButton->setEnabled( showingCals );
	mEventCategoryFiltersButton->setEnabled( showingCals );
}

void ScheduleView::showMyEventsOnly( bool smeo )
{
	mController->setShowMyEventsOnly( smeo );
}

void ScheduleView::updateWeekStartActions()
{
	int current = mScheduleWidget->weekStartDay();
	foreach( QAction * a, mWeekStartDayActions ) {
		a->setCheckable( true );
		a->setChecked( (a->text() == "Saturday" && current == 6)
			|| (a->text() == "Sunday" && current == 0)
			|| (a->text() == "Monday" && current == 1) );
	}
}

void ScheduleView::setupMenu( QMenu * menu )
{
	menu->addAction( mNewTimeSheet );
	if( User::hasPerms( "Schedule", true ) )
		menu->addAction( mNewSchedule );
	//if( User::hasPerms( "Event", true ) )
	menu->addAction( mNewEvent );
	menu->addAction( mPrintAction );
	menu->addSeparator();
	menu->addAction( mWeekAction );
	menu->addAction( mMonthAction );
	menu->addAction( mMonthFlatAction );
	menu->addAction( mYearAction );

	QMenu * startDayMenu = menu->addMenu( "First Day Of The Week" );
	startDayMenu->setIcon( QIcon( "images/first_day_of_the_week.png" ) );
	connect( startDayMenu, SIGNAL( triggered( QAction * ) ), SLOT( startDayActionTriggered( QAction * ) ) );
	mWeekStartDayActions += startDayMenu->addAction( "Saturday" );
	mWeekStartDayActions += startDayMenu->addAction( "Sunday" );
	mWeekStartDayActions += startDayMenu->addAction( "Monday" );
	updateWeekStartActions();

	QMenu * zm = menu->addMenu( "Zoom" );
	zm->setIcon( QIcon( "images/calendar_zoom.png" ) );
	connect( zm, SIGNAL( triggered( QAction * ) ), SLOT( zoomActionTriggered( QAction * ) ) );
	zm->addAction( "%400" );
	zm->addAction( "%200" );
	zm->addAction( "%100" );
	zm->addAction( "%50" );
	zm->addAction( "%25" );

	menu->addSeparator();
	menu->addAction(mShowRecursiveAction);
	menu->addAction(mShowWeekendsAction);
	menu->addAction(mShowTimeSheetsAction);
	menu->addAction(mShowSchedulesAction);
	menu->addAction(mShowCalendarEntriesAction);
	
	menu->addSeparator();

	menu->addAction( mMyEventsOnlyAction );

	QMenu * catMenu = menu->addMenu( "Event Categories" );
	catMenu->setIcon( QIcon( "images/event_category_filter.png" ) );
	setupEventCategoryActions( catMenu );

	QMenu * projectFilterMenu = menu->addMenu( "Event Project Filters" );
	setupProjectFilterActions( projectFilterMenu );
}

void ScheduleView::startDayActionTriggered( QAction * action )
{
	int startDay = 0;
	if( action->text() == "Saturday" )
		startDay = 6;
	else if( action->text() == "Monday" )
		startDay = 1;
	mScheduleWidget->setWeekStartDay( startDay );
	updateWeekStartActions();
}

void ScheduleView::zoomActionTriggered( QAction * action )
{
	int zoom = action->text().mid(1).toInt();
	mScheduleWidget->setZoom( zoom );
}

bool ScheduleView::viewForType( ElementList el, bool )
{
	return (el.size()>0);
}

void ScheduleView::setElementList( ElementList el )
{
	mController->setElementList( el );
}

void ScheduleView::recordTimeSheet()
{
//	QRect r = mScheduleWidget->highlight();
	QDate d( QDate::currentDate() );
//	if( !r.isNull() && r.size() == QSize(1,1) )
//		d = mScheduleWidget->dateAtCell( r.top(), r.left() );
	TimeEntryDialog::recordTimeSheet( this, ElementList(), d );
}

void ScheduleView::newSchedule()
{
//	QRect r = mScheduleWidget->highlight();
	QDate s( QDate::currentDate() );
	QDate e(s);
//	if( !r.isNull() ) {
//		s = mScheduleWidget->dateAtCell( r.top(), r.left() );
//		e = mScheduleWidget->dateAtCell( r.bottom(), r.right() );
//	}
	ScheduleDialog * sd = new ScheduleDialog( this );
	sd->setDateRange( s, e );

	ElementList els = mController->elementList();
	if( els.size() == 1 ) {
		Employee e( els[0] );
		if( e.isRecord() )
			sd->setEmployee( e );
		else
			sd->setElement( els[0] );
	}

	sd->exec();
	delete sd;
}

void ScheduleView::newEvent()
{
//	QRect r = mScheduleWidget->highlight();
	QDate s( QDate::currentDate() );
//	if( !r.isNull() )
//		s = mScheduleWidget->dateAtCell( r.top(), r.left() );
	EventDialog * eventDialog = new EventDialog( this );
	eventDialog->mWhenDateTimeEdit->setDateTime( QDateTime(s) );
	eventDialog->exec();
	delete eventDialog;
}

void ScheduleView::displayModeChange( QAction * act )
{
	int dm = ScheduleWidget::Month;
	if( act == mWeekAction )
		dm = ScheduleWidget::Week;
	else if( act == mMonthAction )
		dm = ScheduleWidget::Month;
	else if( act == mMonthFlatAction )
		dm = ScheduleWidget::MonthFlat;
	else if( act == mYearAction )
		dm = ScheduleWidget::Year;
	mController->setShowTimeSpans( dm != ScheduleWidget::Month );
	if( mDurationAction )
		mDurationAction->setVisible( dm == ScheduleWidget::MonthFlat );
	updateDuration();
	mScheduleWidget->setDisplayMode( dm );
}

void ScheduleView::durationChanged( int duration )
{
	QTimer::singleShot( 200, this, SLOT( updateDuration() ) );
}

void ScheduleView::updateDuration()
{
	int duration = mDurationSpin->value();
	QDate date = mScheduleWidget->date();
	int year = date.year();
	int month = date.month() + duration;
	if( month > 12 ) {
		year++;
		month -= 12;
	}
	mScheduleWidget->setEndDate( QDate( year, month, 1 ).addDays( -1 ) );
}

void ScheduleView::print()
{
	SchedulePrintDialog spd(mScheduleWidget);
	spd.exec();
}

void ScheduleView::timeSheetsAdded( RecordList rl )
{
	TimeSheetList tsl(rl);
	QDate startDate;
	QDate endDate = mScheduleWidget->endDate();
	bool navigate = false, dateChange = false;
	ElementList elements = mController->elementList();
	foreach( TimeSheet ts, tsl ) {
		// We dont care about other users timesheets
		if( ts.user() != User::currentUser() ) continue;

		QDate date = ts.dateTime().date();
		if( startDate < mScheduleWidget->date() ) {
			if( !startDate.isValid() || date < startDate )
				startDate = date;
			dateChange = true;
		}

		if( date > endDate && !startDate.isValid() ) {
			startDate = date;
			dateChange = true;
		}

		// Already showing the asset
		Element e = ts.element();
		if( elements.contains( e ) || elements.contains( ts.user() ) ) continue;

		// Could be showing the asset via recursive listing.
		while( e.isRecord() ) {
			e = e.parent();
			if( elements.contains( e ) ) break;
		}
		
		if( !e.isRecord() )
			navigate = true;
	}

	if( dateChange )
		mScheduleWidget->setDate( startDate );
	if( !mShowTimeSheetsAction->isChecked() ) {
		mShowTimeSheetsAction->setChecked( true );
		mController->setShowTimeSheets( true );
	}
}


void ScheduleView::schedulesAdded( RecordList rl )
{
	ScheduleList sl(rl);
	QDate startDate;
	QDate endDate = mScheduleWidget->endDate();
	bool navigate = false, dateChange = false;
	ElementList elements = mController->elementList();
	ElementList navTo;
	foreach( Schedule s, sl ) {
		// We dont care about other users timesheets
		if( s.createdByUser() != User::currentUser() ) continue;

		QDate date = s.date();
		if( startDate < mScheduleWidget->date() ) {
			if( !startDate.isValid() || date < startDate )
				startDate = date;
			dateChange = true;
		}

		if( date > endDate && !startDate.isValid() ) {
			startDate = date;
			dateChange = true;
		}

		// Already showing the asset
		Element e = s.element();
		if( elements.contains( e ) || elements.contains( s.user() ) ) continue;

		// Could be showing the asset via recursive listing.
		while( e.isRecord() ) {
			e = e.parent();
			if( elements.contains( e ) ) break;
		}
		
		if( !e.isRecord() ) {
			navigate = true;
			navTo += s.element();
		}
	}

	if( !mShowSchedulesAction->isChecked() ) {
		mShowSchedulesAction->setChecked( true );
		mController->setShowSchedules( true );
	}
	if( dateChange )
		mScheduleWidget->setDate( startDate );
}
