
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef SIREN_THUMBNAIL_LOADER_H
#define SIREN_THUMBNAIL_LOADER_H

#include <qstring.h>
#include <qimage.h>
#include <qrect.h>
#include <qmap.h>
#include <qpixmap.h>
#include "thumbnail.h"

struct ThumbnailEntry
{
	QSize size;
	QPixmap pixmap;
};

struct ThumbnailSource
{
	QList<ThumbnailEntry> entries;
	bool contains( const QSize & size ) {
		foreach( ThumbnailEntry te, entries )
			if( te.size == size )
				return true;
		return false;
	}
	QPixmap get( const QSize & size ) {
		foreach( ThumbnailEntry te, entries )
			if( te.size == size )
				return te.pixmap;
		return QPixmap();
	}
	void set( const QSize & size, const QPixmap & pix ){
		foreach( ThumbnailEntry te, entries )
			if( te.size == size ) {
				te.pixmap = pix;
				return;
			}
		ThumbnailEntry ne;
		ne.size = size;
		ne.pixmap = pix;
		entries += ne;
	}
};

typedef QMap<QString, ThumbnailSource> PixCache;
typedef QMap<QString, ThumbnailSource>::Iterator PixCacheIter;

class ThumbnailLoader
{
public:

	static QString thumbnailCachePath( const QString & path, const QSize & size = QSize() );
	static void clear( const QString & fileName );
	static QPixmap load( const Thumbnail & tn, const QSize & retSize );

protected:

	static PixCache * mCache;
};


#endif // THUMBNAIL_LOADER_H

