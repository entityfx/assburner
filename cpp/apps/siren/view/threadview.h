
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef THREAD_VIEW_H
#define THREAD_VIEW_H

#include <qpixmap.h>

#include "element.h"
#include "thread.h"
#include "ui_threadviewui.h"
#include "user.h"
#include "viewbase.h"

class QMenu;
class QPushButton;
class ThreadViewInternal;
 
class ThreadView : public ViewBase
{
Q_OBJECT
public:

	ThreadView( QWidget * parent=0 );

	void setElementList( ElementList elements );

	bool viewForType( ElementList el, bool );

	ThreadViewInternal * internal() { return mInternal; }
protected:

	ThreadViewInternal * mInternal;
};

struct ThreadCache : public CacheBase
{
	Record r;
	QString c0, c1, c2, c3, c4;
	QColor bg, fg;
	QIcon icon;
	void setup( const Record & rr, const QModelIndex & = QModelIndex() );
	RecordList children( const QModelIndex & i );
	QVariant data ( const QModelIndex & i, int role ) const;
	Qt::ItemFlags flags( const QModelIndex &  ) const;
	bool setData ( const QModelIndex &, const QVariant &, int = Qt::EditRole );
	Record getRecord() const;
	int cmp( const ThreadCache & other, const QModelIndex & a, const QModelIndex & b, bool );
};

typedef RecordModelImp<TreeNodeT<ThreadCache> > ThreadModelBase;

class ThreadModel : public ThreadModelBase
{
Q_OBJECT
public:
	ThreadModel( QObject * parent );

public slots:
	void threadNotifyAddedOrRemoved( RecordList );
	
public:
	bool Thread;
};

class ThreadViewInternal : public QWidget, public Ui::ThreadViewUI
{
Q_OBJECT
public:
	ThreadViewInternal( QWidget * parent=0 );

	void setElementList( ElementList elements );

	bool viewForType( ElementList el, bool );
	
public slots:

	void slotAddNote( const Element & element = Element(), const Thread & replyTo = Thread() );

	void slotEditNote( const Thread & );
	
	void showMenu( const QPoint &, const Record &, RecordList );

	void showRecursiveToggled( bool showRecursive );

	void itemSelected( const Record & );
	
	void linkClicked( const QUrl & );
	
	void unreadFilterChanged();
	
	void showLastDaysToggled( bool );
	void showLastDaysChanged( const QString & );

	void readConfig();
protected:
	bool event( QEvent * );

	int mLastDays;
	bool mShowRecursive;
	bool mIgnoreUpdates;
	bool mUserView;
	ElementList mElements;
	
	QString mCurrentRT;
};

#endif // THREAD_VIEW_H

