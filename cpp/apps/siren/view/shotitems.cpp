
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qrect.h>
#include <qpixmap.h>
#include <qpainter.h>
#include <qdatetime.h>
#include <qheaderview.h>
#include <qtextdocument.h>
#include <qtextedit.h>

#include "iniconfig.h"
#include "blurqt.h"

#include "shotitems.h"
#include "thumbnailloader.h"
#include "thumbnailbutton.h"

#include "thumbnail.h"
#include "shot.h"
#include "task.h"
#include "employee.h"
#include "assettype.h"

static const ColumnStruct shot_columns [] =
{
	{ "#", "Number",         20,	0,	false },			//0
	{ "Preview", "Preview",    	120,	1,	false },			//1
	{ "Shot Name", "ShotName", 	120,	2,	false },			//2
	{ "VFX #", "VFXNumber",    	 30,	3,	false },			//3
	{ "Status", "Status",		 		50,	4,	false },			//4
	{ "Frames", "Frames", 		  20,	5,	false },			//5
	{ "Camera Info", "CameraInfo", 		220,	6,	false },//6
	{ "2D Artists", "2D",		 		140,	7,	false },			//7
	{ "3D Artists", "3D",		 		140,	8,	false },			//8
	{ "Description", "Description",		180,	9,	false },//9
	{ "VFX Notes", "VFXNotes",	180,	10,	false },			//10
	{ 0, 0, 0, 0, false }
};

template<class T> void setupColumns( T * tw, QHeaderView * hdr, const ColumnStruct columns [] )
{
	IniConfig & Config = userConfig();
	int cnt = 0;
	QStringList labels;
	for( cnt=0; columns[cnt].name; ++cnt );
	QVector<int> indexVec(cnt);
	for( int i=0; i<cnt; i++ ) {
		labels << QString::fromLatin1(columns[i].name);
		indexVec[i] = Config.readInt( columns[i].iniName + QString("Index"), columns[i].defaultPos );
	}
	tw->setHeaderLabels( labels );
	hdr->setStretchLastSection(false);
	for( int n=0; n<cnt; n++ ) {
		for( int i=0; i<cnt; i++ )
			if( indexVec[i] == n )
				hdr->moveSection( hdr->visualIndex(i), n );
	}
	hdr->resizeSections(QHeaderView::Stretch);
	for( int n=0; n<cnt; n++ ) {
		int size = Config.readInt( columns[n].iniName + QString("Size"), columns[n].defaultSize );
		hdr->resizeSection( n, size );
	}
	for( int n=0; n<cnt; n++ ) {
		bool hidden = Config.readBool( columns[n].iniName + QString("Hidden"), columns[n].defaultHidden );
		hdr->setSectionHidden( n, hidden );
	}
	hdr->setResizeMode( QHeaderView::Interactive );
}

void saveColumns( QHeaderView * hdr, const ColumnStruct columns [] )
{
	IniConfig & Config = userConfig();
	for( int i=0; columns[i].name; i++ )
		Config.writeInt( columns[i].iniName + QString("Size"), hdr->sectionSize( i ) );
	for( int i=0; columns[i].name; i++ )
		Config.writeInt( columns[i].iniName + QString("Index"), hdr->visualIndex( i ) );
	for( int i=0; columns[i].name; i++ )
		Config.writeBool( columns[i].iniName + QString("Hidden"), hdr->isSectionHidden( i ) );
}

template<class T> void setupTreeView( QTreeView * tv, const QString & group, const QString & key, const ColumnStruct columns [] )
{
	IniConfig & cfg = userConfig();
	cfg.pushSection( group );
	setupColumns( (T*)tv->model(), tv->header(), columns );
	int sc = cfg.readInt(key + "SortColumn", 1);
	Qt::SortOrder order(Qt::SortOrder(cfg.readInt(key + "SortOrder",Qt::AscendingOrder)));
	tv->header()->setSortIndicator(sc,order);
	tv->model()->sort(sc,order);
	tv->setAlternatingRowColors( false );
	cfg.popSection();
}

void saveTreeView( QTreeView * tv, const QString & group, const QString & key, const ColumnStruct columns [] )
{
	IniConfig & cfg = userConfig();
	cfg.pushSection( group );
	saveColumns( tv->header(), columns );
	cfg.writeInt( key + "SortColumn", tv->header()->sortIndicatorSection() );
	cfg.writeInt( key + "SortOrder", tv->header()->sortIndicatorOrder() );
	cfg.popSection();
}

void setupShotView( QTreeView * lv )
{
	setupTreeView<ShotModel>(lv,"Siren_Display_Prefs","Shot",shot_columns);
}

void saveShotView( QTreeView * lv )
{
	saveTreeView(lv,"Siren_Display_Prefs","Shot",shot_columns);
}

QSize MultiLineDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	const QAbstractItemModel *model = index.model();
	QVariant value = model->data(index, Qt::FontRole);
	QFont fnt = value.isValid() ? qvariant_cast<QFont>(value) : option.font;
	QString text = model->data(index, Qt::DisplayRole).toString().trimmed();
	QFontMetrics fontMetrics(fnt);
	return fontMetrics.size( 0, text );
}

QSize WrapTextDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QSize newSize = QItemDelegate::sizeHint( option, index );
	newSize.setHeight(90);
	return newSize;

/*	// enable this code in order to make the row heights variable based on the data for each shot
	if( index.isValid() && (index.column() == 9 || index.column() == 10) ) {
		const QAbstractItemModel * model = index.model();
		QVariant value = model->data(index, Qt::FontRole);
		QFont fnt = value.isValid() ? qvariant_cast<QFont>(value) : option.font;
		QFontMetrics fontMetrics(fnt);

		QVariant displayData = model->data(index, Qt::DisplayRole);
		QRect bound = fontMetrics.boundingRect( QRect(0,0,option.rect.width(), 1000 ), Qt::TextWordWrap | Qt::TextDontClip, displayData.toString() );
		return QSize(bound.width(), bound.height()/3);
	}
	else {
		const QAbstractItemModel * model = index.model();
		QVariant value = model->data(index, Qt::FontRole);
		QFont fnt = value.isValid() ? qvariant_cast<QFont>(value) : option.font;
	}
	return QItemDelegate::sizeHint( option, index );
*/
}

void WrapTextDelegate::paint( QPainter * p, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	int x1, y1, x2, y2; //(x1, y1) is the upper-left corner, (x2, y2) the lower-right
	option.rect.getCoords(&x1, &y1, &x2, &y2);
	p->save();
	
	int lineWidths = 4;
	// draw a line below each row
	p->setPen( QPen( QBrush("#9CC"), lineWidths) );
	p->drawLine(x1, y2, x2, y2);
	// draw a line between each column
	p->setPen( QPen( QBrush("#CCC"), lineWidths) );
	p->drawLine( x2, y1+((y2-y1)/5), x2, y2-((y2-y1)/5));
	
	p->restore();
	
	if( index.isValid() && (index.column() == 9 || index.column() == 10) ) {
		QVariant displayData = index.model()->data(index, Qt::DisplayRole);
		QRect newRect(x1+5, y1+5, x2-x1-10, y2-y1-10);
		QRect fullSize;
		
		p->save();
		QItemDelegate::drawBackground( p, option, index );
		if( option.state & QStyle::State_Selected )
			p->setPen( option.palette.highlightedText().color() );
		else
			p->setPen( option.palette.text().color() );
		p->drawText( newRect, Qt::TextWordWrap, displayData.toString(), &fullSize );
		
		if( fullSize.height() > newRect.height() ) {
			// add an indicator to the bottom of the text to show that there is additional text
			p->save();
			p->setPen( QPen(Qt::NoPen) );
			if( option.state & QStyle::State_Selected )
				p->setBrush( option.palette.highlight() );
			else
				p->setBrush( option.palette.base() );
			p->drawRect(newRect.x(), newRect.y()+newRect.height()-15, newRect.width(), 15);
			p->restore();
			
			QRect elipses(x1, y2-23, x2-x1, 20);
			QFont newFont( p->font() );
			newFont.setWeight( QFont::Bold );
			p->setFont( newFont );
			p->drawText( elipses, Qt::AlignHCenter, "..." );
		}
		p->restore();
		return;
	}
	else {
		QStyleOptionViewItem newOption = QStyleOptionViewItem(option);
		newOption.displayAlignment = Qt::AlignHCenter | Qt::AlignVCenter;
		newOption.decorationAlignment = Qt::AlignHCenter | Qt::AlignVCenter;
		return QItemDelegate::paint( p, newOption, index );
	}
}

void WrapTextDelegate::setEditorData( QWidget * editor, const QModelIndex & index ) const
{
	qWarning( "WrapTextDelegate::setEditorData called" );
	QTextEdit * textEdit = static_cast<QTextEdit*>(editor);
	QVariant displayData = index.model()->data(index, Qt::DisplayRole);
	textEdit->setPlainText(displayData.toString());
}

QWidget * WrapTextDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	qWarning( "WrapTextDelegate::createEditor called" );
	QTextEdit * editor = new QTextEdit( index.data().toString(), parent);
	return editor;
}

void WrapTextDelegate::updateEditorGeometry(QWidget *editor,
      const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
	qWarning( "WrapTextDelegate::updateEditorGeometry called" );
	editor->setGeometry(option.rect);
}

void WrapTextDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                    const QModelIndex &index) const
{
	qWarning( "WrapTextDelegate::setModelData called" );
	QTextEdit * textEdit = static_cast<QTextEdit*>(editor);
	
	if ( index.column() == 2 ) {
		// set shot name
		Shot s = ((ShotModel*)model)->getRecord(index);
		s.setName( textEdit->toPlainText() );
		s.commit();
		model->setData(index, QVariant(textEdit->toPlainText()), Qt::EditRole );
	}
	else if ( index.column() == 3 ) {
		// set VFX #
		Task vfx = ShotModel::getCache(index)->mVfxTask;
		vfx.setName( textEdit->toPlainText() );
		vfx.commit();
		model->setData(index, QVariant(textEdit->toPlainText()), Qt::EditRole );
	}
	else if( index.column() == 6 ) {
		// set camera info
		Shot s = ((ShotModel*)model)->getRecord(index);
		s.setCameraInfo( textEdit->toPlainText() );
		s.commit();
		model->setData(index, QVariant(textEdit->toPlainText()), Qt::EditRole );
	}
	else if( index.column() == 9 ) {
		// set shot description
		Shot s = ((ShotModel*)model)->getRecord(index);
		s.setDescription( textEdit->toPlainText() );
		s.commit();
		model->setData(index, QVariant(textEdit->toPlainText()), Qt::EditRole );
	}
	else if( index.column() == 10 ) {
		// set VFX description
		Task vfx = ShotModel::getCache(index)->mVfxTask;
		vfx.setDescription( textEdit->toPlainText() );
		vfx.commit();
		model->setData(index, QVariant(textEdit->toPlainText()), Qt::EditRole );
	}
}

QVariant civ( const QColor & c )
{
	if( c.isValid() )
		return QVariant(c);
	return QVariant();
}

void ShotCache::setup( const Record & r, const QModelIndex & ) {
	mShot = r;
	mPreview = ThumbnailLoader::load(mShot.thumbnail(), QSize(120,90));

  mVfxTask = Task();
  foreach( Element e, mShot.children( Task::type(), false ) )
    if( e.assetType().name() == "Compositing" )
      users2d = e.users();
    else if( e.assetType().name() == "FX" ) {
      users3d = e.users();
      mVfxTask = Task(e);
    }

	mFrames = QString::number( mShot.frameEnd()-mShot.frameStart()+1 );

  mUsers2dString.clear();
  foreach( User u, users2d )
    mUsers2dString += Employee(u).fullName() + "\n";
  mUsers3dString.clear();
  foreach( User u, users3d )
    mUsers3dString += Employee(u).fullName() + "\n";
}

QVariant ShotCache::data( const QModelIndex & i, int role ) const {
	int col = i.column();
	if( role == Qt::DisplayRole ) {
		switch( col ) {
			case 0: return QString::number(i.row()+1);
			case 1: return "";
			case 2: return mShot.name();
			case 3: return mVfxTask.name();
			case 4: return mShot.status();
			case 5: return mFrames;
			case 6: return mShot.cameraInfo();
			case 7: return mUsers2dString;
			case 8: return mUsers3dString;
			case 9: return mShot.description();
			/*
			case 9: {
				QTextDocument td;
				td.setPlainText( mShot.description() );
				td.setTextWidth(160);
				return td.toPlainText();
				}
				*/
			case 10: return mVfxTask.description();
		}
	}
	else if( role == Qt::DecorationRole ) {
		switch( col ) {
			case 1: return mPreview;
		}
	}
	else if( role == Qt::SizeHintRole && col == 9 ) {
				QTextDocument td;
				td.setPlainText( mShot.description() );
				td.setTextWidth(160);
				return td.size();
	}
	return QVariant();
}

QString ShotCache::sortKey( const QModelIndex & i ) const
{
	int column = i.column();
	if( column==9 ){
		QString ret;
		ret.sprintf("%04i", data(i,Qt::DisplayRole).toUInt());
		return ret;
	}
	return data(i,Qt::DisplayRole).toString().toUpper();
}

int ShotCache::cmp( const ShotCache & other, const QModelIndex & a, const QModelIndex & b, bool )
{
	QString ska = sortKey( a ), skb = other.sortKey( b );
	return ska == skb ? 0 : ( ska > skb ? 1 : -1 );
}

Qt::ItemFlags ShotCache::flags( const QModelIndex & i ) { 
	int col = i.column();
	if( col == 9 || col == 10 || col == 2 || col == 3 || col == 6 ) {
		return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable ); 
	} else {
		return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled ); 
	}
}
Record ShotCache::getRecord() { return mShot; }
