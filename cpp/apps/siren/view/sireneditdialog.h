
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef SIREN_EDIT_DIALOG_H
#define SIREN_EDIT_DIALOG_H

#include <QDialog>
#include "ui_editdialog.h"

#include "element.h"
#include "project.h"
#include "shotgroup.h"

class SirenEditDialog : public QDialog, public Ui::EditDialog
{
Q_OBJECT
public:
	SirenEditDialog( QWidget * parent=0 );
	virtual void accept();
	void setElementList( ElementList );
	Element current();

public slots:
	void nextShot();
	void prevShot();
	void newShot( const Project &, const ShotGroup & );
	void dupShot();

protected:
	ElementList commitList;
	Element currentElement;
};

#endif // SIREN_EDIT_DIALOG_H

