
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Siren.
 *
 * Siren is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Siren is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siren; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef SIREN_MAIN_WINDOW_H
#define SIREN_MAIN_WINDOW_H

#include <qmainwindow.h>
#include <qshortcut.h>

#include "project.h"

#include "ui_sirenmainwindow.h"

class QMenu;
class QAction;

class SirenMainWindow : public QMainWindow, public Ui::SirenMainWindow
{
Q_OBJECT
public:
	SirenMainWindow(QWidget * parent=0);
	~SirenMainWindow();

	virtual void closeEvent( QCloseEvent * );
	
public slots:
	void populateMenus();
	void populateProjectMenu();
	void projectThumbnailUpdated( Record, Record );
	void populateTreeMenu();
	void populateSceneCombo();

	void showProjectMenu(const QPoint &);
	void showTreeMenu(const QPoint &);

	void sirenNewProject();
	void sirenNewShot();
	void sirenDeleteShot();

	void sirenEditProject();
	void sirenEditShot();
	void treeEditItem(const QModelIndex &);
	void sirenEditElements( ElementList );

	void setCurrentProject(const Project &);
	void refreshView();
	void currentSceneChanged(const QString &);
	void sceneUpdated( Record );

	void changeFont();
	void initSchedule();
	void initNotes();
	void selectFilter(QAction *);
	void filterShots();
	void print();

protected slots:

protected:
	QMenu* mFileMenu;
	QMenu* mTreeMenu;
	QMenu* mProjectMenu;
	QMenu* mSearchMenu;

	QAction* mNewProjectAction;
	QAction* mNewAssetAction;
	QAction* mNewTaskAction;
	QAction* mNewShotAction;
	
	QShortcut* shortcutNew;

	QAction* mEditProjectAction;
	QAction* mEditShotAction;

	QAction* mDeleteShotAction;

	QAction* mFontAction;
	QAction* mScheduleAction;
	QAction* mPrintAction;
	QAction* mQuitAction;
	
	QAction* mSearchShotName;
	QAction* mSearchShotDesc;
	QAction* mSearchVfxNotes;
	QAction* mSearchAll;

	Project mCurrentProject;
	ShotGroup mCurrentScene;

	bool mFilterShotsRunning;
	QMap<QString, int> mFilterFields;
};

#endif // MAIN_WINDOW_H

