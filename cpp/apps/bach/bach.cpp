
#include "connection.h"
#include "database.h"
#include "schema.h"
#include "strings.h"
#include "bach.h"

//#include <valgrind/callgrind.h>

Schema * bachSchema()
{
	static Schema * _schema = 0;
	if( !_schema ) {
		_schema = new Schema();
	}
	return _schema;
}

Database * blurDb()
{
	return Database::current( bachSchema() );
}

#include "bachasset.h"
#include "bachbucket.h"
#include "bachbucketmap.h"
#include "bachkeyword.h"
#include "bachkeywordmap.h"
#include "bachnamespace.h"


void createConnection()
{
	static bool connectionCreated = false;
	if( connectionCreated || !isConfigInitialized() )
		return;
	connectionCreated = true;
	Database::setCurrent( new Database( bachSchema(), Connection::createFromIni( config(), "Database" ) ) );
}

//#include <sys/time.h>

void initializeSchema()
{
	static bool classesLoaded = false;
	if( classesLoaded )
		return;
	classesLoaded = true;
	//CALLGRIND_START_INSTRUMENTATION;
	//timespec ts;
	//timespec ts2;
	//LOG_TRACE;
	//clock_gettime(CLOCK_REALTIME, &ts);
	buildStrings();
	BachAsset::schema();
	BachBucket::schema();
	BachBucketMap::schema();
	BachKeyword::schema();
	BachKeywordMap::schema();
	BachNamespace::schema();
	
	//clock_gettime(CLOCK_REALTIME, &ts2);
	//LOG_TRACE;
	//printf( "Loading schema took %i us\n", int(((ts2.tv_sec - ts.tv_sec) * 1000000000LL + ts2.tv_nsec - ts.tv_nsec) / 1000) );
	//CALLGRIND_STOP_INSTRUMENTATION;
}


void blurqt_loader()
{
	initializeSchema();
	createConnection();
}

