#!/usr/bin/python2.5

from PyQt4.QtCore import *
from PyQt4.QtSql import *
from blur.Stone import *
from Bach import *
import os
import sys
import time
import pprint

app = QCoreApplication.instance()
if app is None:
  try:
    args = sys.argv
  except:
    args = []
  app = QCoreApplication(args)

initConfig("bach.ini", "/tmp/pybach.log")

initStone(sys.argv)
bach_loader()

