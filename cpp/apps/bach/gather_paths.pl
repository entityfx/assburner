use strict;

use Digest::MD4 qw(md4_hex);
use DBD::Pg;
use Encode;

my $dbh = DBI->connect("dbi:Pg:dbname=bach;host=sql01.drd.int;port=5432;", "barry", "sonja", {AutoCommit => 1});
$dbh->do("set client_encoding=SQL_ASCII");
my $insertSql =qq|INSERT INTO bachasset (width,height,exif,path,imported,cacheprefix,directory,filesize,filetype) VALUES (?,?,?,?,now(),?,?,?,2);\n|;
my $sth = $dbh->prepare($insertSql);

my $updateSql =qq|UPDATE bachasset SET width=?,height=?,exif=? WHERE path = ?|;
my $sth2 = $dbh->prepare($updateSql);

while (my $path = <STDIN>) {
        chomp $path;
        my $realPath = $path;
				my $directory = `dirname "$realPath"`;

        my $width = 0;
        my $height = 0;
        my $mimeType;
        my $mimeMajor;
        my $exif = "";
        open(EXIF,"exiftool \"$realPath\"|");
        while(my $line = <EXIF>) {
					#Image Width                     : 1504
					#Image Height                    : 1000
                if( $line =~ /Image Width\s*:\s*(.*)/i ) { $width = $1 }
                if( $line =~ /Image Height\s*:\s*(.*)/i ) { $height = $1 }
                if( $line =~ /Mime Type\s*:\s*(.*)/i ) {
									$mimeType = $1;
									($mimeMajor) = split("/", $mimeType);
								}

                $exif .= $line;
        }
        close EXIF;

				my $digest = md4_hex($realPath);
				my $first = substr($digest, 0, 1);
				my $second = substr($digest, 1, 1);
				my $cachePrefix = "/$first/$second/";

				print "inserting: $path\n";
        my $rv = $sth->execute($width, $height, encode("UTF-8", $exif), $path, $cachePrefix, $directory, -s $path);
				unless( $rv ) {
					$sth2->execute($width, $height, encode("UTF-8", $exif), $path);
				}

				# if there is a BACHCACHEPATH set, generate a fullsize thumbnail
				if( defined $ENV{BACHCACHEPATH} ) {
					my $thumbnail = $ENV{BACHCACHEPATH} . "/" . $path . "_raw.png";
					my $thumbDir = `dirname "$thumbnail"`;
					unless( -e $thumbDir ) { system(qq|mkdir -p "$thumbDir"|) }
					if( $mimeMajor eq 'video' ) {
						# use ffmpeg
						system(qq|/drd/software/ext/ffmpeg/lin64/bin/ffmpeg -i "$path" -an -ss 00:00:03 -an -r 1 -vframes 1 -y "$thumbnail"|);
					} elsif( $mimeMajor eq 'image' ) {
						# use ImageMagick
						system(qq|/drd/software/ext/imageMagick/lin64/current/bin/convert -quality 90 -resize 1024x1024 "$path" "$thumbnail"|);
					}
				}
}
#$dbh->commit();

