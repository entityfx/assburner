
#ifndef __BACH_H__
#define __BACH_H__

#include <qobject.h>

#ifdef BACH_MAKE_DLL
#define BACH_EXPORT Q_DECL_EXPORT
#else
#define BACH_EXPORT Q_DECL_IMPORT
#endif

namespace Stone {
	class Database;
	class Schema;
}

using namespace Stone;

/**
 * \defgroup Classes Classes - auto-generated classes to access database tables
 * \details Classes are defined using the app @ref classmaker
 * All fields defined have mutators, and indexes have static methods to retrieve a @ref RecordList of @ref stone objects.
 * Additional methods for a database class can be defined in the base/ directory.
 */
BACH_EXPORT void blurqt_loader();

BACH_EXPORT void createConnection();
BACH_EXPORT void initializeSchema();

BACH_EXPORT Schema * classesSchema();
BACH_EXPORT Database * blurDb();

#endif // __BACH_H
 
