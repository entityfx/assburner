
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Bach.
 *
 * Bach is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Bach is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bach; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <QPixmap>
#include <QLabel>

#include <qfile.h>
#include <qurl.h>
#include <qmessagebox.h>
#include <qapplication.h>
#include <qprogressdialog.h>

#include "bachassetlistview.h"
#include "bachitems.h"

//-------------------------------------------------------------------------------------------------
BachAssetListView::BachAssetListView( QWidget * parent )
:	RecordListView( parent )
,	mDragDropHelper( this )
{
	setAcceptDrops(false);
	setFlow(QListView::LeftToRight);
	setWrapping(true);
	setUniformItemSizes(true);
	setLayoutMode(QListView::Batched);
	setResizeMode(QListView::Adjust);
	setSelectionRectVisible(true);
}

//-------------------------------------------------------------------------------------------------
void BachAssetListView::dragEnterEvent( QDragEnterEvent * /*event*/ )
{
}

//-------------------------------------------------------------------------------------------------
void BachAssetListView::dragMoveEvent( QDragMoveEvent * /*event*/ )
{
}

//-------------------------------------------------------------------------------------------------
void BachAssetListView::dropEvent( QDropEvent * /*event*/ )
{
}
//-------------------------------------------------------------------------------------------------
void BachAssetListView::mousePressEvent(QMouseEvent * event)
{
    if ( mDragDropHelper.mousePressEvent( event ) )
    	return;
    RecordListView::mousePressEvent( event );
}


//-------------------------------------------------------------------------------------------------
void BachAssetListView::mouseReleaseEvent(QMouseEvent * event)
{
    if ( mDragDropHelper.mouseReleaseEvent( event ) )
    	return;
    RecordListView::mouseReleaseEvent( event );
}

//-------------------------------------------------------------------------------------------------
void BachAssetListView::mouseMoveEvent(QMouseEvent * event)
{
	if ( mDragDropHelper.mouseMoveEvent( event ) )
		return;
	RecordListView::mousePressEvent( event );
}
