//-------------------------------------------------------------------------------------------------
/*
 * BachFolderBrowserTreeView.h
 *
 *  Created on: Jun 24, 2009
 *      Author: david.morris
 */

#ifndef BACHFOLDERBROWSERTREEVIEW_H_
#define BACHFOLDERBROWSERTREEVIEW_H_

#include <QtGui>

class QDirModel;

//-------------------------------------------------------------------------------------------------
/*
 *
 */
class BachFolderBrowserTreeView : public QTreeView
{
Q_OBJECT;
//-------------------------------------------------------------------------------------------------
public:
//-------------------------------------------------------------------------------------------------
	BachFolderBrowserTreeView( QWidget * a_Parent );

	const QDirModel * getDirModel() const { return m_Model; }
	QDirModel * getDirModel() { return m_Model; }

public slots:
	void onCollapsed( const QModelIndex & index );
	void onExpanded( const QModelIndex & index );

//-------------------------------------------------------------------------------------------------
private:
	QDirModel * m_Model;
};

#endif /* BACHFOLDERBROWSERTREEVIEW_H_ */
