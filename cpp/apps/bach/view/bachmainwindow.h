
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Bach.
 *
 * Bach is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Bach is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bach; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#ifndef BACH_MAIN_WINDOW_H
#define BACH_MAIN_WINDOW_H

#include <qmainwindow.h>
#include <qprogressdialog.h>
#include <qthread.h>
#include <qtimer.h>
#include <qslider.h>

#include "ui_bachmainwindow.h"
#include "bachitems.h"

class QMenu;
class QAction;
class BachBucket;

class BachMainWindow : public QMainWindow, public Ui_BachMainWindow
{
Q_OBJECT

public:
	BachMainWindow(QWidget * parent=0);
	~BachMainWindow();

	virtual void closeEvent( QCloseEvent * );
	BachAssetList selection() const;

	QSize thumbSize() const;

	static QString cacheRoot();
        static QSize tnSize();

public slots:
	void populateMenus();
	void populateTreeMenu();
	void showTreeMenu(const QPoint &);
	void treeEditItem(const QModelIndex &);

	void refreshView();
	void openFiles() const;
	void editTags();
	void showFiles() const;
	void copyPaths();
	void copyImage();
	void removeFromBucket();

	void selectionChanged();
	void collectionViewSelectionChanged();
	void collectionViewDoubleClicked();
	void foldersViewDoubleClicked( const QModelIndex & a_Idx );

	void changeFont();

	void generateAllThumbs();
	void populateCache();

	void tabChanged(int);
	void doubleClicked();

	void doTagSearch();
	void doBucketSearch( const BachBucket & bb );
	void doDirectorySearch( const QString & a_Directory );
	bool doPreSearch();
	void doPostSearch();

	void cancelSearch();
	void updateSearchResults();
	void searchComplete();
	void searchButtonPressed();

	void collapseToSequence();

	void collectionAddBtnPressed(bool);
	void collectionNewBtnPressed(bool);
	void collectionDelBtnPressed(bool);
	void collectionRefreshBtnPressed(bool);

        void calculateTnSize(int);

private:
	QMenu* mTreeMenu;
	QMenu* mFileMenu;
	QMenu* mViewMenu;

	QAction* mFontAction;
	QAction* mGenThumbsAction;
	QAction* mPopCacheAction;
	QAction* mPrintAction;
	QAction* mQuitAction;
	QAction* mOpenAction;
	QAction* mEditAction;
	QAction* mShowAction;
	QAction* mCopyPathAction;
	QAction* mCopyImageAction;
	QAction* mRemoveFromBucketAction;

	QAction* mDblClickToPlayAction;
	QAction* mDblClickToEditAction;

	QAction* mCollapseToSequenceAction;

	int mResults;
	int mResultsShown;
	int mMissing;
	RecordSuperModel * mThumbnailModel;

	BachAssetList mFound;
	BachAssetIter mUpdateIter;
	QTime mSearchTime;
	bool mSearchInProgress;
	bool mSearchUpdateInProgress;
	bool mSearchCanceled;

	void initThumbSlider();
	QTimer * mSearchTimer;
	QString _cacheRoot();
	QSlider * mThumbSlider;
};

#endif // MAIN_WINDOW_H

