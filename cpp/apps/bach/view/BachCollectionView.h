//-------------------------------------------------------------------------------------------------
/*
 * BachCollectionView.h
 *
 *  Created on: Jun 18, 2009
 *      Author: david.morris
 */

#ifndef BACHCOLLECTIONVIEW_H_
#define BACHCOLLECTIONVIEW_H_

#include <qevent.h>
#include <qwidget.h>
#include <qitemdelegate.h>

#include "recordlistview.h"
#include "recordsupermodel.h"
#include "bachbucket.h"


//-------------------------------------------------------------------------------------------------
class BachCollectionView : public RecordListView
{
Q_OBJECT

public:
	BachCollectionView( QWidget * parent );
	void refresh();

protected:
	void dragEnterEvent( QDragEnterEvent * event );
	void dragLeaveEvent( QDragEnterEvent * event );
	void dragMoveEvent( QDragMoveEvent * event );
	void dropEvent( QDropEvent * event );
private:
	RecordSuperModel * mCollectionsModel;
	BachBucket mCurrentDropTarget;
};

//-------------------------------------------------------------------------------------------------
struct BachBucketItem : public RecordItem
{
	BachBucket mBachBucket;
	int mMappingsCount;

	void setup( const Record & r, const QModelIndex & );
	QVariant modelData( const QModelIndex & i, int role ) const;
	QString sortKey( const QModelIndex & i ) const;
	int compare( const QModelIndex & a, const QModelIndex & b, int, bool );
	Qt::ItemFlags modelFlags( const QModelIndex & );
	Record getRecord();
};

//-------------------------------------------------------------------------------------------------
typedef TemplateRecordDataTranslator<BachBucketItem> BachBucketTranslator;

#endif /* BACHCOLLECTIONVIEW_H_ */
