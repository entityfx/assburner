
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Bach.
 *
 * Bach is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Bach is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bach; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qrect.h>
#include <qpixmap.h>
#include <qpainter.h>
#include <qdatetime.h>
#include <qheaderview.h>
#include <qtextdocument.h>
#include <qtextedit.h>
#include <qapplication.h>

#include "iniconfig.h"
#include "blurqt.h"
#include "imagesequencewidget.h"

#include "bachthumbnailloader.h"
#include "bachitems.h"

#include "bachasset.h"
#include "bachmainwindow.h"

#include "utils.h"

static const ColumnStruct asset_columns [] =
{
	{ "#", "Number",         20,	0,	false },			//0
	{ "Preview", "Preview",    	120,	1,	false },			//1
	{ "Info", "Info", 	120,	2,	false },			//2
	{ 0, 0, 0, 0, false }
};

void setupAssetTree( RecordTreeView * lv )
{
  IniConfig & cfg = userConfig();
  cfg.pushSubSection( "AssetList" );
  lv->setupTreeView(cfg,asset_columns);
  cfg.popSection();
}

void saveAssetTree( RecordTreeView * lv )
{
  IniConfig & cfg = userConfig();
  cfg.pushSubSection( "AssetList" );
  lv->saveTreeView(cfg,asset_columns);
  cfg.popSection();
}


QSize ThumbDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QSize hint = BachMainWindow::tnSize();
		hint.rheight() += 2;
		hint.rwidth() += 2;
		return hint;
}

void ThumbDelegate::paint( QPainter * p, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	int x1, y1, x2, y2; //(x1, y1) is the upper-left corner, (x2, y2) the lower-right
	option.rect.getCoords(&x1, &y1, &x2, &y2);

        if( index.isValid() && index.column() == 1 )
        {
                //BachAsset ba = ((RecordSuperModel*)(index.model()))->getRecord(index);
                if( option.state & QStyle::State_Selected ) {
									p->fillRect(option.rect, option.palette.brush(QPalette::Normal, QPalette::Highlight));
                  p->drawRect( x1,y1,x2,y2 );
                } else {
									QItemDelegate::drawBackground( p, option, index );
								}
                QPixmap icon(index.data(Qt::DecorationRole).value<QPixmap>());
                QRect bounds(icon.rect());
                bounds.moveCenter(option.rect.center());
                p->drawPixmap( bounds, icon  );
        }
}

void ThumbDelegate::setEditorData( QWidget * editor, const QModelIndex & index ) const
{
	if( index.column() == 2 ) {
		QTextEdit * textEdit = static_cast<QTextEdit*>(editor);
		QVariant displayData = index.model()->data(index, Qt::DisplayRole);
		textEdit->setPlainText(displayData.toString());
	}
}

QWidget * ThumbDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem & /*option*/, const QModelIndex & index ) const
{
	//qWarning( "WrapTextDelegate::createEditor called" );
	if( index.column() == 1 ) {
		BachAsset ba = ((RecordSuperModel*)(index.model()))->getRecord(index);
		if( ba.path().toLower().endsWith(".mov") || ba.path().toLower().endsWith(".avi" ) ) {
			ImageSequenceWidget * editor = new ImageSequenceWidget( parent );
			editor->setShowControls(false);
			editor->setLoop(true);
			editor->setInputFile( ba.path() );
			editor->playClicked();
			return editor;
		}
	}
	return new QWidget(parent);
}

void ThumbDelegate::updateEditorGeometry(QWidget *editor,
      const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
	qWarning( "WrapTextDelegate::updateEditorGeometry called" );
	editor->setGeometry(option.rect);
}

QSize WrapTextDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QSize newSize = QItemDelegate::sizeHint( option, index );
	newSize.setHeight(190);
        return newSize;
}

void WrapTextDelegate::paint( QPainter * p, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	int x1, y1, x2, y2; //(x1, y1) is the upper-left corner, (x2, y2) the lower-right
	option.rect.getCoords(&x1, &y1, &x2, &y2);
	p->save();

	int lineWidths = 4;
	// draw a line below each row
	p->setPen( QPen( QBrush("#9CC"), lineWidths) );
	p->drawLine(x1, y2, x2, y2);
	// draw a line between each column
	p->setPen( QPen( QBrush("#CCC"), lineWidths) );
	p->drawLine( x2, y1+((y2-y1)/5), x2, y2-((y2-y1)/5));

	p->restore();

	if( index.isValid() && index.column() == 2 )
	{
		BachAsset ba = ((RecordSuperModel*)(index.model()))->getRecord(index);

		QRect pathRect(x1+5, y1+5, x2-x1-10, ((y2-y1)/3)-10);
		QRect tagsRect(pathRect.x(), pathRect.y()+pathRect.height()+10, x2-x1-10, y2-y1-10);
		QRect fullSize;

		p->save();
		QItemDelegate::drawBackground( p, option, index );
		if( option.state & QStyle::State_Selected )
			p->setPen( option.palette.highlightedText().color() );
		else
			p->setPen( option.palette.text().color() );

		p->drawText( pathRect, Qt::TextWordWrap, ba.path(), &fullSize );
		p->drawText( tagsRect, Qt::TextWordWrap, ba.tags(), &fullSize );

		p->setPen( QPen( QBrush("#9CC"), 2) );
		p->drawLine(x1+10, y1+((y2-y1)/3), x2-10, y1+((y2-y1)/3));

		//QRect dateRect(tagsRect.bottomRight().x()-40,tagsRect.bottomRight().y()-14, tagsRect.bottomRight().x(), tagsRect.bottomRight.y());

		QFont curFont = option.font;
		p->setFont( QFont(curFont.family(), curFont.pointSize()-2, curFont.weight(), true ) );
		curFont.setPointSize( curFont.pointSize()-2 );
		p->setPen( QColor("#CCC") );

		QRect sizeRect(x1+20,y2-20,x1+100,y2-4);
		p->drawText( sizeRect, Qt::TextWordWrap, QString::number(ba.width())+"x"+QString::number(ba.height()), &fullSize );

		p->setFont(option.font);

		p->restore();
		return;
	}
	else
	{
		QStyleOptionViewItem newOption = QStyleOptionViewItem(option);
		newOption.displayAlignment = Qt::AlignHCenter | Qt::AlignVCenter;
		newOption.decorationAlignment = Qt::AlignHCenter | Qt::AlignVCenter;
		return QItemDelegate::paint( p, newOption, index );
	}
}

void WrapTextDelegate::setEditorData( QWidget * editor, const QModelIndex & index ) const
{
	//qWarning( "WrapTextDelegate::setEditorData called" );
	if( index.column() == 2 ) {
		QTextEdit * textEdit = static_cast<QTextEdit*>(editor);
		QVariant displayData = index.model()->data(index, Qt::DisplayRole);
		textEdit->setPlainText(displayData.toString());
	}
}

QWidget * WrapTextDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem & /*option*/, const QModelIndex & index ) const
{
	//qWarning( "WrapTextDelegate::createEditor called" );
	if( index.column() == 1 ) {
		BachAsset ba = ((RecordSuperModel*)(index.model()))->getRecord(index);
		if( ba.path().toLower().endsWith(".mov") || ba.path().toLower().endsWith(".avi" ) ) {
			ImageSequenceWidget * editor = new ImageSequenceWidget( parent );
			editor->setShowControls(false);
			editor->setLoop(true);
			editor->setInputFile( ba.path() );
			editor->playClicked();
			return editor;
		}
	} else if ( index.column() == 2 ) {
		QTextEdit * editor = new QTextEdit( index.data().toString(), parent);
		return editor;
	}
	return new QWidget(parent);
}

void WrapTextDelegate::updateEditorGeometry(QWidget *editor,
      const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
	qWarning( "WrapTextDelegate::updateEditorGeometry called" );
	editor->setGeometry(option.rect);
}

void WrapTextDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                    const QModelIndex &index) const
{
	qWarning( "WrapTextDelegate::setModelData called" );
	QTextEdit * textEdit = static_cast<QTextEdit*>(editor);

	if ( index.column() == 2 ) {
		BachAsset ba = ((RecordSuperModel*)model)->getRecord(index);
		ba.setTags( textEdit->toPlainText() );
		ba.commit();
		model->setData(index, QVariant(textEdit->toPlainText()), Qt::EditRole );
	}
}

QVariant civ( const QColor & c )
{
	if( c.isValid() )
		return QVariant(c);
	return QVariant();
}

void BachAssetItem::setup( const Record & r, const QModelIndex & ) {
	mBachAsset = r;
	mPreviewLoaded = false;
        //mPreview = ThumbnailLoader::load( BachMainWindow::cacheRoot(), mBachAsset.path(), BachMainWindow::tnSize(), false );
}

QVariant BachAssetItem::modelData( const QModelIndex & i, int role ) const {
	int col = i.column();
	if( role == Qt::DisplayRole ) {
		//DBG( ( QString::number( i.row() ) + ": Tags: " + mBachAsset.tags() ) );
		switch( col ) {
			case 0: return QString::number(i.row()+1);
			case 1: return "";
			case 2: return mBachAsset.tags();
		}
	}
	else if( role == Qt::DecorationRole ) {
		switch( col ) {
			case 1:
                                return ThumbnailLoader::load( BachMainWindow::cacheRoot(), mBachAsset.path(), BachMainWindow::tnSize(), false );
                                break;
		}
	}
	return QVariant();
}

QString BachAssetItem::sortKey( const QModelIndex & i ) const
{
	return modelData(i,Qt::DisplayRole).toString().toUpper();
}

int BachAssetItem::compare( const QModelIndex & a, const QModelIndex & b, int,  bool )
{
	QString ska = sortKey( a ), skb = BachAssetTranslator::data(b).sortKey( b );
	return ska == skb ? 0 : ( ska > skb ? 1 : -1 );
}

Qt::ItemFlags BachAssetItem::modelFlags( const QModelIndex & i ) {
	int col = i.column();
	if( col == 1 || col == 2 ) {
		return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable );
	} else {
		return Qt::ItemFlags( Qt::ItemIsSelectable | Qt::ItemIsEnabled );
	}
}
Record BachAssetItem::getRecord() { return mBachAsset; }

