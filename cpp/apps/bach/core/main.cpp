
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of Bach.
 *
 * Bach is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Bach is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Bach; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <stdlib.h>

#include <qapplication.h>
#include <qsplashscreen.h>
#include <qdir.h>
#include <QTcpSocket>
#include <stdio.h>

#include "blurqt.h"
#include "stonegui.h"
#include "iniconfig.h"
#include "process.h"
#include "database.h"
#include "table.h"
#include "freezercore.h"
#include "updatemanager.h"

#include "bachmainwindow.h"
#include "ffimagesequenceprovider.h"

extern void bach_loader();

#ifdef Q_OS_WIN
const char * PID_FILE="C:\\Bach\\siren.pid";
const char * PROCESS_NAME="siren.exe";
#else
const char * PID_FILE="/tmp/bach.pid";
const char * PROCESS_NAME="bach";
#endif // Q_OS_WIN

int main( int argc, char * argv[] )
{
	QApplication a( argc, argv );
	bool showSplash = true, allowDupWindow=false;

#ifdef Q_OS_WIN
	if( QDir::current() != QDir( "C:/blur/bach" ) )
		QDir::setCurrent( "C:/blur/bach" );
#endif // Q_OS_WIN
		
	initConfig( "bach.ini" );
	initStone( argc, argv );

#ifdef Q_OS_WIN
	initUserConfig( "h:/public/" + getUserName() + "/bach.ini" );
#else
	initUserConfig( QDir::homePath() + "/.bach.ini" );
#endif


	/* This will create the worker thread
	 * and connect to the database, must
	 * be done after QApplication construction,
	 * and before MainWindow construction */
	bach_loader();
	initStoneGui();
	//IniConfig & cfg = config();
	QString url;

	for( int i = 1; i<argc; i++ ){
		QString arg( argv[i] );
		//bool hasNext = i+1<argc;
		if( arg == "-h" || arg == "--help" )
		{
			printf( (QString("Bach v")).toLatin1() );
			printf( "Options:" );
			printf( "-no-splash" );
			printf( "\tDon't show the splash screen\n" );
			printf( "-cwd" );
			printf( "\tSets the current working directory to c:/Bach\n" );
			printf( "-generateAllThumbs" );
			printf( "\tGenerates thumbnails for every asset, and populates caches\n" );
			printf( "-dup\tAllows opening multiple windows, instead of bringing already open window to font\n" );
			printf( stoneOptionsHelp().toLatin1() );
			return 0;
		} else
		if( arg == "-no-splash" )
			showSplash = false;
		else if( arg == "-cwd" )
			QDir::setCurrent("c:/Bach/");
		else if( arg == "-dup" ) {
			allowDupWindow = true;
		} else {
			printf(("got arg: " + arg).toLatin1());
			url = arg;
		}
	}
	FreezerCore::setDatabaseForThread( bachDb(), Connection::createFromIni( config(), "Database" ) );
	UpdateManager::instance();

	int pid = pidFromFile(PID_FILE);
	//  Valid PID    Make sure pid is not another prog, don't kill ourself
	if( !allowDupWindow && (pid > 0) && isRunning(pid, PROCESS_NAME) && (pid != processID()) ){
		printf("Bach is already running with pid %i", processID());
		// connect to running Bach and send it the URL
		printf( ("Telling running Bach("+ QString::number(pid) +") to go to: " + url).toLatin1() );
		QTcpSocket * socket = new QTcpSocket();
		socket->connectToHost( "127.0.0.1", 31104 );
		QTextStream os(socket);
		os << url << "\n";
		socket->flush();
		socket->close();
		delete socket;
		shutdown();
		return 0;
	}

	// Write new client_pid.txt
	if( !pidToFile(PID_FILE) ){
		printf((QString("Couldn't write pid to ") + PID_FILE).toLatin1());
	}

	int result;
	// Scoped so that MainWindow destructer is called before shutdown()
	// so that the settings saved in the blur destructor are saved to file
	{
		/* Show splash screen unless -no-splash option is given */
		QSplashScreen * splash=0;
		if( showSplash ){
			QPixmap pixmap( ":/images/splash.png" );
			splash = new QSplashScreen( pixmap );
			splash->show();
		}

		// These tables should always keep a reference to any
		// loaded records with the key cache, so that they
		// can be loaded by primary key multiple times
		// without hitting the database
		//Database::current()->setUndoEnabled( true );
#ifdef USE_FFMPEG
		registerFFImageSequenceProviderPlugin();
#endif


		BachMainWindow mw;
		IniConfig & uc = userConfig();
		uc.pushSection( "Bach_Display_Prefs" );
		QStringList fg = uc.readString( "FrameGeometry" ).split(',');
		uc.popSection();
		if( fg.size()==4 ){
			mw.resize( QSize( fg[2].toInt(), fg[3].toInt() ) );
			mw.move( QPoint( fg[0].toInt(), fg[1].toInt() ) );
		}
		mw.show();

		/* Finish the splash screen when MainWindow is shown */
		if( splash )
			splash->finish( &mw );

		/* Enter event loop */
		result = a.exec();
	}

	shutdown();

	return result;
}

