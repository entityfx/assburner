

#ifndef _JOB_BURNER_TESTER_H_
#define _JOB_BURNER_TESTER_H_

class Slave;

class JobBurnerTester
{
public:
	virtual ~JobBurnerTester(){}
	virtual void test(Slave * slave, int & success, int & fail)=0;
};

#endif // _JOB_BURNER_TESTER_H_
