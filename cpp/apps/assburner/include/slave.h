
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */


#ifndef SLAVE_H
#define SLAVE_H

#include <qobject.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qtimer.h>
#include <qpointer.h>

#include "host.h"
#include "hoststatus.h"
#include "job.h"
#include "jobassignment.h"
#include "jobtask.h"
#include "spooler.h"
#include "service.h"

class QMessageBox;
class QProcess;
class QTimer;

class Service;

class Idle;
class JobBurner;
class KillDialog;

/// \ingroup Assburner
/// \brief Creates a SysLog entry for the error and sets the HostService::sysLog (fkeysyslog) to it.
void host_error( const QString & text, const QString & method, const Service & service );

/// \ingroup Assburner
/// @{

/**
 * \brief Controls Assburner Rendering
 * Keeps track of host's status. 
 * Queries the database for new job assignments.
 * Updates the database when the user sets
 * assburner offline.  Starts copies and burners.
 */

class Slave : public QObject
{
Q_OBJECT
public:
	Slave(bool useGui=true, bool autoRegister=false, bool inOwnLogonSession=false, uint burnOnlyJobAssignmentKey=0, QObject * parent=0);
	~Slave();

	bool isInitialized() { return mInitialized; }

	HostStatus hostStatus() const;
	Host host() const;

	/// Returns the status field of the host status record.
	QString status() const;

	// Returns the current state we are in depending on the jobs that are being executed
	Job::HostCancelModeEnum hostCancelMode();
	
	/// Runs a program specified by 'assburnerInstallerPath'
	/// in the config table, then quits.
	void clientUpdate( bool onlineAfterUpdate = true );
	void clientUpdateLegacy( bool onlineAfterUpdate = true );

	QList<JobBurner*> activeBurners();
	JobAssignmentList activeAssignments();
	
	/// Returns the spooler used to download and keep track of files for the jobs
	/// Can be 0 if not using a spool
	Spooler * spooler() { return mSpooler; }

	bool usingGui() const { return mUseGui; }

	bool inOwnLogonSession() { return mInOwnLogonSession; }

	void setInOwnLogonSession( bool iols );

	// returns the path to the log root dir, used if you don't
	// want to log to the database
	QString logRootDir() const;

	QString currentPriorityName() const;

	void reportMappingFailure();
	
	static Slave * instance();
	
	int secondsIdle() const { return mLastIdle; }
	
public slots:
	/// Call via singleshot timer from the ctor
	/// We dont want any db access in the ctor,
	/// because that is called from the mainwindow
	/// ctor, if the db connection gets lost
	/// before the mainwindow is up, then qt4
	/// will exit once the dialog is closed
	///
	/// Can also be called explicitly to avoid needing
	/// to wait for the event loop.  There are no side-effects
	/// from subsequent calls
	void startup();

	/// Updates the status in the database to status
	/// If force is true, any status change that happens before
	/// the new status is committed will be ignored.  If force
	/// is false, the new status will be handled and status will
	/// be ignored.
	bool setStatus( const QString & status, bool force = false );

	void setUseGui( bool );

	/// 
	/// These are the 3 slots that need to be called
	/// from the jobburner to tell the slave what
	/// is going on currently
	/// 
	void slotError( const QString & text );
	void slotBurnFinished();

	/// Interface for gui that is controlling us
	void online();
	void offline();
	void toggle();

	/// This restarts assburner
	void restart( bool needNewLogonSession = false );

	/// This reboots the host machine
	void reboot( QString message );

	/// Shuts down the machine and sets the host's slaveStatus to 'sleeping'
	void goToSleep();

	/// for debugging
	void offlineFromAboutToQuit();

	void showRemapWarning(const QString & drive);

	void setIsMapped( bool );

	void killDialogFinished( int );

signals:
	/// For the gui to show status.
	void statusChange( const QString & status );

	void copyProgressChange( int );

	void activeAssignmentsChange( JobAssignmentList activeAssignments, JobAssignmentList oldActiveAssignments );

protected slots:
	/// Called once every 5 seconds to check database
	/// status
	void loop();

    /// Cleans up the currently running jobs
    void cleanup();

	/// Resets assburner, stopping and cleaning up any currently
	/// running job by calling cleanup.  Status will be set to ready if offline is false
	void reset( bool offline=false );

	/// This restarts assburner when it loses its DB connection
	/// (doesn't return frames, etc)
	void restartFromLostConnectionTimer();

	/// Connected to mIdle's secondsIdle signal
	/// This is used to go offline automatically
	/// after a period of machine inactivity
	void slotSecondsIdle( int );

	/// These are here to moniter the database connection
	/// If we cant connect for a minute, then we'll try to restart
	void slotConnectionLost();
	void slotConnected();

	void execPriorityMode();

	void restoreDefaultMappings();
	
	void recoverFromFailedReboot();
	
protected:
	void handleStatusChange( const QString & status, const QString & oldStatus );

	/// Attempts to start a job burner for the currently assigned job.
	void burn(const JobAssignment &);
	void handleJobAssignmentChange( const JobAssignment & );
	void cleanupJobBurner( JobBurner * burner );
	void setActiveAssignments( JobAssignmentList );

	ServiceList enabledServiceList();

	bool checkForbiddenProcesses();
	QStringList mForbiddenProcesses;
	void loadForbiddenProcesses();

	void pulse();

	/// Update memory usage of running and finished jobs
	void setAvailableMemory();

	bool checkFusionRunning();

	void updatePriorityMode( int );

//	void checkDriveMappingAvailability();


	void checkAllBurnersComplete();
	
	void loadEmbeddedPython();

	bool mInitialized, mInitStarted;
	Host mHost;
	HostStatus mHostStatus;
	QDateTime mLastAssignmentChange;
	JobAssignmentList mActiveAssignments;
	QList<JobBurner*> mActiveBurners;

	QTimer * mTimer;
	int mPulsePeriod, mLoopTime, mQuickLoopTime, mQuickLoopCount;
	Service mService;
	QDateTime mLastPulse;

	Idle * mIdle;
	int mIdleDelay, mLastIdle;

	Spooler * mSpooler;

	bool mOfflineSeeds;

	KillDialog * mKillDialog;
	bool mInsideLoop;

	QTimer * mLostConnectionTimer;

	bool mBackgroundMode;

	QPointer<QMessageBox> mDriveRemapWarningDialog;
	bool mIsMapped;

	bool mUseGui;
	bool mAutoRegister;

	// Specifies that assburner is running in it's own windows logon session
	// and therefore has it's own drive mappings.
	bool mInOwnLogonSession;
	// Indicates if we were previously restarted due to a map failure
	bool mMapFailureRestart, mMapFailureOffline;
	QDateTime mMapFailureOfflineTime;

	bool mParallelBurn;
	bool mBurnOnlyMode;
	uint mBurnOnlyJobAssignmentKey;

	bool mBackgroundModeEnabled;
};

/// @}

#endif // SLAVE_H

