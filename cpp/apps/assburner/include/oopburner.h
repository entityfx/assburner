

#ifndef OOP_BURNER_H
#define OOP_BURNER_H

#include "jobburner.h"

#ifdef Q_OS_WIN
class Win32Process;
#endif

// Out of process Burner
class OOPBurner : public JobBurner
{
public:
	OOPBurner( const JobAssignment & jobAssignment, Slave * slave );
	virtual ~OOPBurner();

	virtual QStringList processNames() const;

	virtual QString executable();

	virtual QString workingDirectory();

	virtual QStringList buildCmdArgs();

	virtual QStringList environment();

	virtual void start();

	virtual void startBurn();

	virtual void cancel();

	virtual void cleanup();

protected:
	virtual void startProcess();

	virtual bool checkup();

	virtual void slotProcessExited();
	virtual void slotProcessStarted();
	virtual void slotProcessError( QProcess::ProcessError );
	
	// Override so that we can disable JobCommandHistory creation that interferes
	// with the actual logging done by the child assburner
	virtual void updateOutput( bool overrideLogging = false );

#ifdef Q_OS_WIN
	Win32Process * mProcess;
#endif
};

#endif // OOP_BURNER_H
