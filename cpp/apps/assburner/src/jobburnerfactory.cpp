
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include "blurqt.h"

#include "job.h"
#include "jobassignment.h"
#include "jobtype.h"

#include "jobburnerfactory.h"
#include "jobburnerplugin.h"


bool JobBurnerFactory::mPluginsLoaded = false;
typedef QMap<QString,JobBurnerPlugin*> PluginMap;
PluginMap JobBurnerFactory::mBurnerPlugins;
typedef PluginMap::Iterator PluginMapIt;

bool JobBurnerFactory::supportsJobType( const JobAssignment & jobAssignment )
{
	JobType jobType = jobAssignment.job().jobType();

	while( jobType.isRecord() ) {
		if( mBurnerPlugins.contains( jobType.name() ) )
			return true;
		jobType = jobType.parentJobType();
	}

	return false;
}

void JobBurnerFactory::registerPlugin( JobBurnerPlugin * bp, bool overrideExisting )
{
	QStringList types = bp->jobTypes();
	foreach( QString t, types )
		if( overrideExisting || !mBurnerPlugins.contains(t) ) {
			mBurnerPlugins[t] = bp;
			LOG_3( "Registering burner for jobtype: " + t );
		}
}

JobBurnerPlugin * JobBurnerFactory::plugin( const QString & jobType )
{
	PluginMapIt it = mBurnerPlugins.find(jobType);
	return it == mBurnerPlugins.end() ? nullptr : *it;
}

JobBurner * JobBurnerFactory::createBurner( const JobAssignment & jobAssignment, Slave * slave, QString * errMsg )
{
	JobType jobType = jobAssignment.job().jobType();

	while( jobType.isRecord() ) {
		JobBurner * jb = createBurner( jobType.name(), jobAssignment, slave, errMsg );
		if( jb ) return jb;
		jobType = jobType.parentJobType();
	}

	return 0;
}

JobBurner * JobBurnerFactory::createBurner( const QString & jobType, const JobAssignment & jobAssignment, Slave * slave, QString * /*errMsg*/ )
{
	JobBurnerPlugin * plug = plugin(jobType);
	return plug ? plug->createBurner(jobAssignment,slave) : nullptr;
}

