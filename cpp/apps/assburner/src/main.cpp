
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include <qdir.h>
#include <qfile.h>
#include <qlibrary.h>
#include <qprocess.h>
#include <qsettings.h>

#include "application.h"
#include "maindialog.h"
#include "common.h"
#include "blurqt.h"
#include "database.h"
#include "freezercore.h"
#include "slave.h"
#include "process.h"
#include "jobburnerplugin.h"
#include "jobburnerfactory.h"
#include "jobburnertester.h"

#include "job.h"
#include "host.h"
#include "hoststatus.h"
#include "svnrev.h"

#include "stonegui.h"
#include "classes.h"

#ifdef Q_OS_WIN
#include <windows.h>
#include <winerror.h>
#include <direct.h>
#include <Werapi.h>

#ifndef KEY_WOW64_64KEY
#define KEY_WOW64_64KEY 0x0100
#endif

const char * ABPSMON = "abpsmon.exe";

static const char * sAssburnerMessageName = "AssburnerMessage";
UINT assburnerMessageId = 0;

/*
// Force a "crash" so that we get a backtrace through drmingw
// qt uses abort in many places when fatal errors are detected
// which can be caused by memory corruption etc.
void abrt_handler(int )
{
	printf("abort handler\n");
	raise(SIGSEGV);
}
*/

static void enableCrashDumps();

#else // !Q_OS_WIN

#include <unistd.h>
const char * ABPSMON = "abpsmon";

#endif

MainDialog * sMainDialog = 0;

// Returns true if there isn't another assburner.exe running
static bool startup( bool usePsmon, bool daemonize, bool useSingleProcessMutex )
{
#ifdef Q_OS_WIN
	assburnerMessageId = RegisterWindowMessageA( sAssburnerMessageName );
	if( useSingleProcessMutex ) {
		hMutex = CreateMutex( NULL, true, L"AssFreezerSingleProcess");
		if (hMutex == NULL) {
			LOG_3( "Error: Couldn't create mutex, exiting" );
			return false;
		}
		if( GetLastError() == ERROR_ALREADY_EXISTS ) {
			LOG_3( "Error: Another process owns the mutex, exiting" );
			if( assburnerMessageId ) {
				QList<int> pids;
				pidsByName( "assburner.exe", &pids );
				foreach( int pid, pids ) {
					QList<WindowInfo> windows = windowInfoByProcess(pid);
					foreach( WindowInfo wi, windows ) {
						LOG_1( "Sending online event from external assburner window" );
						SendNotifyMessageA( wi.hWin, assburnerMessageId, 0, 0 );
					}
				}
			}
			return false;
		}
	}
// Stack traces when compiling with mingw
//	QLibrary excdll( "exchndl.dll" );
//	if( !excdll.load() ) {
//		LOG_1( "Error Loading exchndl.dll(Crash Handler) Error Was: " + excdll.errorString() );
//	}
	//disableWindowsErrorReporting( "assburner.exe" );
	WerRemoveExcludedApplication( L"assburner.exe", true );
	enableCrashDumps();
	disableWindowsErrorReporting();
	//WerSetFlags(WER_FAULT_REPORTING_ALWAYS_SHOW_UI);

	SetErrorMode( SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX );
	setProcessPriorityClass( processID(), HIGH_PRIORITY_CLASS );
	setProcessIoPriority( processID(), 3 );
	
	//signal(SIGABRT,abrt_handler);
#else
	Q_UNUSED(useSingleProcessMutex);
#endif
	if( !daemonize && usePsmon && pidsByName( ABPSMON ) == 0 )
		QProcess::startDetached( ABPSMON );
#ifndef Q_OS_WIN
	if( daemonize )
		return daemon( 0, 0 ) == 0;
#endif
	return true;
}

static QCoreApplication * a = 0;

//
// With qt4 the app freezes upon logout,
// no matter what i do in the mainwindow's
// closeEvent.  This function makes the
// app close properly
//
#ifdef Q_OS_WIN
static bool winEventFilter( void * message, long * result )
{
	// 1 - went from ready/busy state, 2 - went from offline state
	static int wentNoPulseForShutdown = 0;
	MSG * msg = (MSG*)message;
	if( msg->message == WM_QUERYENDSESSION ) {
		Slave * slave = Slave::instance();
		if( slave && slave->isInitialized() && slave->status() != "no-pulse" ) {
			LOG_5( "Recieved WM_QUERYENDSESSION message. Preparing for application exit, setting assburner status to no-pulse." );
			wentNoPulseForShutdown = slave->status() == "offline" ? 2 : 1;
			// Dont go offline when assburner is the one causing the reboot, instead stay at 'reboot' status
			if( slave->status() != "reboot" )
				slave->offlineFromAboutToQuit();
		}
		// Let Qt handle the event itself after this
		return false;
	}
	if( msg->message == WM_ENDSESSION && wentNoPulseForShutdown && ((BOOL)(msg->wParam) == FALSE) ) {
		// This means the shutdown was cancelled, so we go back to previous ready/offline status from no-pulse
		Slave * slave = Slave::instance();
		if( slave ) {
			LOG_5( "Shutdown was cancelled.  Restoring previous assburner status of " + QString( wentNoPulseForShutdown == 1 ? "online." : "offline.") );
			if( wentNoPulseForShutdown == 1 )
				slave->online();
			else if( wentNoPulseForShutdown == 2 )
				slave->offline();
			wentNoPulseForShutdown = 0;
		}
		// Let Qt handle the event itself after this
		return false;
	}
	if( msg->message == assburnerMessageId && (msg->wParam == 0) && (msg->lParam == 0) ) {
		LOG_1( "Got online event from external assburner process" );
		Slave * slave = Slave::instance();
		if( slave && slave->isInitialized() )
			slave->online();
		if( sMainDialog )
			sMainDialog->showAndActivate();
	}
	return false;
}

static void enableCrashDumps() {
	HKEY hkey;
	const char * path = "Software\\Microsoft\\Windows\\Windows Error Reporting\\LocalDumps\\assburner.exe";
	if( RegCreateKeyExA( HKEY_LOCAL_MACHINE, path, 0, 0, 0, KEY_ALL_ACCESS | (isWow64() ? KEY_WOW64_64KEY : 0), 0, &hkey, 0 ) == ERROR_SUCCESS ) {
		DWORD val = 2;
		RegSetValueExA( hkey, "DumpType", 0, REG_DWORD, (const BYTE *)&val, sizeof(DWORD) );
		const char * abPath = "C:\\blur\\assburner\\";
		RegSetValueExA( hkey, "DumpFolder", 0, REG_SZ, (const BYTE *)abPath, strlen(abPath) + 1 );
		RegCloseKey( hkey );
	} else
		LOG_3( "Unable to create/open registry key HKEY_LOCAL_MACHINE\\" + QString::fromLatin1(path) );
}
#endif // Q_OS_WIN

int main(int argc, char * argv[])
{
	bool gui = true;
	bool showSql = false;
	bool createDb = false;
	bool verifyDb = false;
	bool autoRegister = false;
	bool usePsmon = true;
	bool daemonize = false;
	bool inNewLogonSession = false;
	bool burnOnlyMode = false;
	// Burn Only param
	int jobAssignmentKey = 0;
	int startupDelay = 0;
	QStringList testJobTypes;
#ifdef Q_OS_WIN
	QDir::setCurrent( QFileInfo(currentExecutableFilePath()).path() );
	int loginTries = 1, loginRetryDelay = 10;
	QString configFile = "assburner.ini";
#else
	QString configFile = "/etc/assburner.ini";
#endif
	for( int i = 1; i<argc; i++ ){
		QString arg( argv[i] );
		if( arg == "-h" || arg == "--help" )
		{
			printf( "Options:\n" );
			printf( "-config <file>" );
			printf( "\tLocation of configuration .ini file\n" );
			printf( "-nogui" );
			printf( "\tRun without a gui, for headless slaves\n" );
			printf( "-show-sql" );
			printf( "\tOutputs all executed sql to stdout\n" );
			printf( "-create-database" );
			printf( "\tCreates all missing tables in the database, then exits\n" );
			printf( "-verify-database" );
			printf( "\tVerifies all tables in the database, then exits\n" );
			printf( "-output-schema FILE" );
			printf( "\tOutputs the database schema in xml format to FILE\n" );
			printf( "-db-host HOST" );
			printf( "\tSet the database host to HOST. Can be either a hostname or an ipv4 address\n" );
			printf( "-db-port PORT" );
			printf( "\tSet the database port to PORT\n" );
			printf( "-db-user USER" );
			printf( "\tSet the database username to USER\n" );
			printf( "-db-password PASS" );
			printf( "\tSet the database password to PASS\n" );
			printf( "-auto-register" );
			printf( "\tIf no Host record exists, create it\n" );
			printf( "-no-psmon" );
			printf( "\tDo not run the abpsmon utility, responsible for restarting AB ourself\n" );
			printf( "-startup-delay DELAY_SECONDS\n" );
			printf( "\tInstructs assburner to sleep for DELAY_SECONDS before actual startup begins.  Useful for starting assburner from the windows registry.\n" );
			printf( "-login-retries LOGIN_RETRIES\n" );
			printf( "\tInstructs assburner to try LOGIN_RETRIES times to login as the 'run as' user before giving up.  Useful for starting assburner from the windows registry.\n" );
			printf( "-login-retry-delay DELAY_SECONDS\n" );
			printf( "\tInstructs assburner to sleep for DELAY_SECONDS after each failed login attempt, before retrying.  Useful for starting assburner from the windows registry.\n" );
#ifndef Q_OS_WIN
			printf( "-daemonize" );
			printf( "\tDetach from terminal, this option also disables the gui and the psmon utility.\n" );
			printf( "-test-jobtypes" );
			printf( "\tComma separated list of jobtypes to test.  Then exit.\n" );
#endif
			return 0;
		}
		else if( arg == "-show-sql" )
			showSql = true;
		else if( arg == "-create-database" )
			createDb = true;
		else if( arg == "-verify-database" )
			verifyDb = true;
		else if( arg == "-output-schema" && (i+1 < argc) )
			return 0;
		else if( arg == "-nogui" )
			gui = false;	
		else if( arg == "-auto-register" )
			autoRegister = true;	
		else if( arg == "-no-psmon" )
			usePsmon = false;
		else if( arg == "-daemonize" ) {
			daemonize = true;
			usePsmon = gui = false;
		}
		else if( arg == "-config" )
			configFile = argv[i+1];
		else if( arg == "-in-new-logon-session" )
			inNewLogonSession = true;
		else if( arg == "-burn" ) {
			++i;
			if( i >= argc ) {
				printf( "-burn requires a job assignment key as the next argument\n" );
				return 1;
			}
			burnOnlyMode = inNewLogonSession = true;
			gui = autoRegister = usePsmon = daemonize = false;
			jobAssignmentKey = QString(argv[i]).toInt();
		} else if( arg == "-startup-delay" ) {
			startupDelay = qMax(0,QString(argv[i+1]).toInt());
			i++;
#ifdef Q_OS_WIN
		} else if( arg == "-login-retries" ) {
			loginTries = qMax(1, QString(argv[i+1]).toInt() + 1);
			i++;
		} else if( arg == "-login-retry-delay" ) {
			loginRetryDelay = qMax(0,QString(argv[i+1]).toInt());
			i++;
#endif
		} else if( arg == "-test-jobtype" ) {
			testJobTypes += QString(argv[i+1]).split(",");
			usePsmon = false;
			daemonize = false;
			i++;
		}
	}

	if( startupDelay > 0 )
#ifdef Q_OS_WIN
		Sleep( startupDelay * 1000 );
#else
		sleep( startupDelay );
#endif
	
	if( gui )
		a = new Application( argc, argv );
	else
		a = new CoreApplication( argc, argv );

	// Use default log file from config file unless we are in burn only mode
	QString logFile;
	if( burnOnlyMode )
		logFile = "assburner_burn_" + QString::number( jobAssignmentKey ) + ".log";

#ifdef Q_OS_WIN
	initConfig( configFile, logFile );
	initUserConfig( QDir::homePath() + "/.ab.ini" );
#else
	initConfig( configFile );
	config().readFromFile( "/etc/db.ini", false );
	initUserConfig( QDir::homePath() + "/.ab.ini" );
#endif

#ifdef Q_OS_WIN
	if( !inNewLogonSession ) {
		IniConfig ini(config());
		ini.pushSection( "LogonSession" );
		if ( ini.readBool( "UseNewSession", false ) ) {
			while(loginTries-- > 0) {
				Win32Process * proc = new Win32Process();
				proc->setLogonFlag( Win32Process::LogonWithProfile );
				QString user = ini.readString( "Username" ), domain = ini.readString( "Domain" );
				proc->setLogon( user, ini.readString( "Password" ), domain );
				proc->start( "c:\\blur\\assburner\\assburner.exe", QStringList() << "-in-new-logon-session" );
				if( proc->isRunning() ) {
					if( !domain.isEmpty() ) user += "/" + domain;
					LOG_1( "Assburner started in new logon session as user " + user + ", pid is " + QString::number( proc->pid()->dwProcessId ) );
					return 0;
				}
				LOG_5( "Unable to start assburner in new logon session, trying hack with hardcoded domain" );
				
				delete proc;
				
				if( loginRetryDelay > 0 ) {
#ifdef Q_OS_WIN
					Sleep( loginRetryDelay * 1000 );
#else
					sleep( loginRetryDelay );
#endif
				}
			}
			// Out of tries, return 1 for failure
			return 1;
		}
		ini.popSection();
	}
#endif // Q_OS_WIN

	bool do_start = startup(usePsmon,daemonize,!burnOnlyMode);

	IniConfig cfg(config());
	cfg.pushSection("Assburner");

	QString appName = cfg.readString("ApplicationName","Assburner");
	a->setApplicationName( appName );
	LOG_1( appName+" version " + VERSION + ", build " + SVN_REVSTR + " starting" );

	for( int i = 1; i<argc; i++ ){
		QString arg( argv[i] );
		bool hasNext = i+1<argc;
		if( arg == "-db-host" && hasNext ) {
			cfg.pushSection( "Database" );
			cfg.writeString( "Host", argv[++i] );
			cfg.popSection();
		} else
		if( arg == "-db-port" && hasNext ) {
			cfg.pushSection( "Database" );
			cfg.writeString( "Port", argv[++i] );
			cfg.popSection();
		} else
		if( arg == "-db-user" && hasNext ) {
			cfg.pushSection( "Database" );
			cfg.writeString( "User", argv[++i] );
			cfg.popSection();
		} else
		if( arg == "-db-password" && hasNext ) {
			cfg.pushSection( "Database" );
			cfg.writeString( "Password", argv[++i] );
			cfg.popSection();
		}
	}

	blurqt_loader();
	initStoneGui( false );

	if( showSql ) {
		showSql = true;
		Database::current()->setEchoMode(
			Database::EchoSelect
			| Database::EchoUpdate
			| Database::EchoInsert
			| Database::EchoDelete );
	}
	if( createDb ) {
		printf( "VALIDATING/CREATING Tables" );
		printf( Database::current()->createTables() ? "Success" : "Failure" );
		return 0;
	}
	if( verifyDb ) {
		printf( "VALIDATING Tables" );
		printf( Database::current()->verifyTables() ? "Success" : "Failure" );
		return 0;
	}

	if( usePsmon && !do_start ) {
		Host h = Host::currentHost();
		HostStatus hs = h.hostStatus();
		QString s = hs.slaveStatus();
		if( s == "offline" || s == "stopping" || s == "restart" ) {
			hs.returnSlaveFrames("starting");
		}
		return 1;
	}

	Slave * slave = new Slave(gui, autoRegister, inNewLogonSession, jobAssignmentKey);

	if( testJobTypes.size() ) {
		// Ensure slave has started, it runs it's startup in a singleshot timer
		slave->startup();
		int success = 0, fail = 0;
		foreach( QString jobType, testJobTypes ) {
			JobBurnerPlugin * plugin = JobBurnerFactory::plugin(jobType);
			if( !plugin ) {
				printf( "Unable to test jobtype '%s', no plugin registered\n", qPrintable(jobType) );
				fail++;
				continue;
			}
			JobBurnerTester * tester = plugin->createTester(jobType);
			if( !tester ) {
				printf( "Unable to test jobtype '%s', plugin has no tests\n", qPrintable(jobType) );
				fail++;
				continue;
			}
			int s, f;
			tester->test(slave,s,f);
			success += s;
			fail += f;
			delete tester;
		}
		printf( "Ran %i tests.  %i Passes, %i Failures\n", success+fail, success, fail );
		return fail;
	}
	
	int res = 0;
	if( gui ) {
#ifdef Q_OS_WIN
		a->setEventFilter( winEventFilter );
#endif // Q_OS_WIN

		LOG_3( "Gui mode, creating dialog");
		sMainDialog = new MainDialog(slave);
		// sMainDialog->show();
		// Don't show so that client-updates when status offline don't pop up md
		// The maindialog will automatically showAndActivate itself the first time
		// it's status goes to ready
		((QApplication*)a)->setQuitOnLastWindowClosed(false);
	}
	LOG_3( "starting event loop" );
	res = a->exec();
	LOG_5( "a->exec() returned " + QString(res) );

	if( gui ) {
		// Slave deleted by MainDialog
		delete sMainDialog;
	} else {
		delete slave;
	}

#ifdef Q_OS_WIN
	LOG_5( "Closing mutex" );
	if( hMutex )
		CloseHandle( hMutex );
#endif

	// Host will stay at restart status until the new instance
	// sets it's status to online
	// 666 exit code means an internal error (uncaught exception) occurred
	// so we will let abpsmon restart us.
	if( usePsmon && (res != 666) && Host::currentHost().hostStatus().slaveStatus() != "restart" )
		killAll( ABPSMON );

	LOG_5( "Calling shutdown()" );
	shutdown();

	return res;
}

