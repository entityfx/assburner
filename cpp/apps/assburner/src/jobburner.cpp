
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include <qapplication.h>
#include <qprocess.h>
#include <qtimer.h>
#include <qregexp.h>
#include <qdir.h>

#include "changeset.h"
#include "config.h"
#include "database.h"
#include "path.h"
#include "process.h"

#include "hostservice.h"
#include "jobassignmentstatus.h"
#include "joberror.h"
#include "jobtype.h"
#include "jobtypemapping.h"
#include "joboutput.h"
#include "jobcommandhistory.h"
#include "jobservice.h"
#include "location.h"
#include "mapping.h"
#include "project.h"
#include "service.h"
#include "syslog.h"

#include "jobburner.h"
#include "slave.h"
#include "spooler.h"

#ifdef Q_OS_WIN
#include "windows.h"
#endif // Q_OS_WIN

JobBurner::JobBurner( const JobAssignment & jobAssignment, Slave * slave, int options )
: QObject( slave )
, mSlave( slave )
, mCmd( 0 )
, mJob( jobAssignment.job() )
, mJobAssignment( jobAssignment )
, mLoaded( false )
, mTaskTimeWarned( false )
, mOutputTimer( 0 )
, mStatsTimer( 0 )
, mCheckupTimer( 0 )
, mState( StateNew )
, mOptions( Options(options) )
, mCurrentCopy( 0 )
, mMappingDelayCount( 0 )
, mLogFilesReady( false )
, mStdOutFile( 0 )
, mStdErrFile( 0 )
, mProcessId( 0 )
, mMemoryPriorityLowered( false )
, mCleanupStage( CleanupStart )
{
	mOutputTimer = new QTimer( this );
	connect( mOutputTimer, SIGNAL( timeout() ), SLOT( updateOutput() ) );

	mStatsTimer = new QTimer( this );
	connect( mStatsTimer, SIGNAL( timeout() ), SLOT( checkStats() ) );

	mCheckupTimer = new QTimer( this );
	connect( mCheckupTimer, SIGNAL( timeout() ), SLOT( checkup() ) );

	/* Ensure we are actually assigned some tasks to work on */
	mTaskAssignments = mJobAssignment.jobTaskAssignments().sorted(JobTaskAssignment::c.Key);
	if( mTaskAssignments.isEmpty() ) {
		jobErrored( QString("JobAssignment has no assigned tasks, Job Assignment key: %1").arg(mJobAssignment.key()) );
		return;
	}

	// get any changed Job params ( like environment, command, logging, etc )
	mJob.reload();

	/* Make sure each of the tasks are still valid, some could have already been unassigned or cancelled */
	/* Also verify that the jobtask record matches the assignment */
	mTasks = mTaskAssignments.jobTasks(Index::UseSelect);
	foreach( JobTaskAssignment jta, mTaskAssignments ) {
		JobTask task = jta.jobTask();
		if( jta.jobAssignmentStatus().status() != "ready" || task.status() != "assigned" || task.host() != Host::currentHost() ) {
			LOG_1( QString("JobTask no longer assigned, discarding. keyJobTask: %1  keyJobTaskAssignment: %2  jobtask status: %3 jobtaskassignment status: %4")
				.arg(task.key()).arg(jta.key()).arg(task.status()).arg(jta.jobAssignmentStatus().status()) );
			mTaskAssignments -= jta;
			mTasks -= task;
		}
	}

	if( mTasks.isEmpty() ) {
		LOG_1( "No Tasks still assigned, cancelling the burn" );
		cancel();
		return;
	}

	mTaskList = compactNumberList( mTasks.frameNumbers() );

	//connect( this, SIGNAL( fileGenerated(const QString &) ), SLOT( syncFile(const QString &) ) );
}

JobBurner::~JobBurner()
{
	if( mState == StateStarted )
		cleanup();

	if( !mSlave->logRootDir().isEmpty() && mStdOutFile && mStdErrFile ) {
		mStdOutFile->flush();
		mStdOutFile->close();
		mStdErrFile->flush();
		mStdErrFile->close();
	}
	delete mCmd;
	delete mOutputTimer;
	delete mStatsTimer;
	delete mCheckupTimer;
	delete mCurrentCopy;
	delete mStdOutFile;
	delete mStdErrFile;
}

JobAssignment JobBurner::jobAssignment() const
{
	return mJobAssignment;
}

Slave * JobBurner::slave() const
{
	return mSlave;
}

Job JobBurner::job() const
{
	return mJob;
}

QString JobBurner::executable()
{
	jobErrored( "JobBurner::executable() Not Implemented" );
	return QString();
}

QStringList JobBurner::buildCmdArgs()
{
	return QStringList();
}

QStringList JobBurner::splitEnvironment(const QString & env)
{
	// ; is used to separate each key/value pair, but can also be used inside a value.
	// Rather than requiring an escape sequence we just consider each section to be part of
	// the previous value if it doesn't contain an =. If there is no previous value then
	// it is discarded.
	QStringList envSplit;
	foreach( QString kv, env.split(';') ) {
		if( !kv.contains( "=" ) ) {
			if( envSplit.size() )
				envSplit.back() += ";" + kv;
		} else
			envSplit.append( kv );
	}
	return envSplit;
}

QMap<QString,QString> JobBurner::splitEnvironmentKeyVal(QStringList env)
{
	QMap<QString,QString> ret;
	foreach( QString kv, env ) {
		int equalsPos = kv.indexOf('=');
		if( equalsPos > 0 )
			ret[kv.left(equalsPos).toUpper()] = kv.mid(equalsPos+1);
		else if( equalsPos < 0 )
			ret[kv.toUpper()] = QString();
	}
	return ret;
}

QStringList JobBurner::mergeEnvironment(QStringList existing, QStringList toMerge)
{
	QStringList ret;
	QMap<QString,QString> kvExisting = splitEnvironmentKeyVal(existing), kvToMerge = splitEnvironmentKeyVal(toMerge);
	for( QMap<QString,QString>::iterator kv = kvToMerge.begin(); kv != kvToMerge.end(); ++kv ) {
		QString val = kv.value();
		QRegExp repl( "%([^%]+)%");
		while( repl.indexIn( val ) >= 0 ) {
			QString keyToReplace = repl.cap(1).toUpper();
			QString valToReplace = kvExisting.value(keyToReplace,QString());
			val.replace( "%" + keyToReplace + "%", valToReplace, Qt::CaseInsensitive );
		}
		kvExisting[kv.key()] = val;
	}
	for( QMap<QString,QString>::iterator kv = kvExisting.begin(); kv != kvExisting.end(); ++kv )
		ret += kv.key() + "=" + kv.value();
	return ret;
}

QStringList JobBurner::customEnvironment()
{
	return splitEnvironment(mJob.environment());
}

QStringList JobBurner::environment()
{
	QStringList customEnv = customEnvironment();
	if( customEnv.isEmpty() )
		return customEnv;
	return mergeEnvironment(QProcess::systemEnvironment(), customEnv);
}

qint32 JobBurner::processId()
{
	if( !mProcessId && mCmd )
		mProcessId = qprocessId( mCmd );
	return mProcessId;
}

QString JobBurner::workingDirectory()
{
	return QString();
}

QString JobBurner::burnDir() const
{
	return mCurrentCopy ? mCurrentCopy->directory() : QDir::currentPath();
}

void JobBurner::connectProcess( QProcess * process )
{
	mCmd = process;
	connect( process, SIGNAL( readyReadStandardOutput() ), SLOT( slotReadStdOut() ) );
	if( mOptions & OptionMergeStdError ) {
		process->setProcessChannelMode( QProcess::MergedChannels );
	} else {
		if( !(mOptions & OptionIgnoreStdError ) )
			connect( process, SIGNAL( readyReadStandardError() ), SLOT( slotReadStdError() ) );
	}
	connect( process, SIGNAL( started() ), SLOT( slotProcessStarted() ) );
	connect( process, SIGNAL( finished(int) ), SLOT( slotProcessExited() ) );
	connect( process, SIGNAL( error( QProcess::ProcessError ) ), SLOT( slotProcessError( QProcess::ProcessError ) ) );
}

void JobBurner::startProcess()
{
	bool executableIsCommandString = mOptions & OptionExecutableIsCommandString;
	QStringList args;
	
	QString cmd = executable();
	if( mState != StateNew ) return;
	
	QString wholeCmd = cmd;
	
	QString wd = workingDirectory();
	if( mState != StateNew ) return;

	if( !executableIsCommandString ) {
		args = buildCmdArgs();
		if( mState != StateNew ) return;
		wholeCmd += " " + args.join(" ");
	}
	
	mCmd = new QProcess( this );
	connectProcess( mCmd );
	if( mState != StateNew ) return;
	
	if( !wd.isEmpty() )
		mCmd->setWorkingDirectory( wd );

	mJobCommandHistory.setCommand( wholeCmd );
	mJobAssignment.setCommand( wholeCmd );
	logMessage( "Starting command: " + wholeCmd + (wd.isEmpty() ? QString() : QString(" in directory " + wd)) );

	QStringList env = environment();
	if( !env.isEmpty() ) {
		mCmd->setEnvironment( env );
		logMessage( "With env: \n\t" + env.join("\n\t") );
	}

	if( cmd.isEmpty() ) {
		jobErrored( "Job has no command to run.  If intentional then the burner should override startBurn and not call startProcess." );
		return;
	}
	
	if( executableIsCommandString )
		mCmd->start( cmd );
	else
		mCmd->start( cmd, args );
}

void JobBurner::start()
{
	if (mSlave->spooler())
		startCopy();
	else {
		mBurnFile=mJob.fileName();
		startBurn();
	}
}

void JobBurner::startCopy()
{
	mCurrentCopy = new SpoolRef(mSlave->spooler()->startCopy( mJob ));
	connect( mCurrentCopy, SIGNAL( completed() ), SLOT( slotCopyFinished() ) );
	connect( mCurrentCopy, SIGNAL( errored( const QString &, int ) ), SLOT( slotCopyError( const QString &, int ) ) );
	connect( mCurrentCopy, SIGNAL( copyProgressChange( int ) ), mSlave, SIGNAL( copyProgressChange( int ) ) );
	connect( mCurrentCopy, SIGNAL( cancelled() ), SLOT( cancel() ) );
	mCurrentCopy->start();
}

void JobBurner::slotCopyFinished()
{
	mBurnFile = mCurrentCopy->burnFile();
	mCurrentCopy->disconnect( this );
	startBurn();
}

void JobBurner::slotCopyError( const QString & error, int )
{
	mCurrentCopy->disconnect( this );
	jobErrored( error );
}

void JobBurner::startBurn()
{
	QString err;
	if( mSlave->inOwnLogonSession() || mSlave->host().allowMapping() ) {
		MappingList mappings = mJob.mappings();
		Win32ShareManager * shareMan = Win32ShareManager::instance();
		mMappingRef = shareMan->map( mappings, mSlave->inOwnLogonSession(), &err );

		// If mapping fails, then we cannot render this job at this time
		// return the job assignment and go to ready state.
		if( mappings.size() && !mMappingRef.isValid() ) {
			
			// Allow up to one and a half minutes for a previos render to release
			// it's mappings.  At some point this should be smarter...
			if( err.startsWith( "Mapping conflict" ) && mMappingDelayCount < 10 ) {
				LOG_1( "Delaying burner start for 10 seconds due to temporary mapping conflict." );
				mMappingDelayCount++;
				QTimer::singleShot( 10000, this, SLOT(startBurn()) );
				return;
			}
			
			jobErrored( err );
			mSlave->reportMappingFailure();
			return;
		}
	
		mSlave->setIsMapped( mMappingRef.isValid() );
	
		logMessage( "Mappings: " + shareMan->currentMappingsInfoString() );
	}

	// If we fail to set status to busy that means a new status has arrived that is
	// telling assburner to do something else, we stop here as cleanup will get called
	if( !mSlave->setStatus( "busy" ) )
		return;

	mStart = QDateTime::currentDateTime();

	startProcess();
	if( mState != StateNew ) return;

	logMessage( "startBurn: pid: " + QString::number(processId()) );
	mState = StateStarted;
	// Sets JCH.fkeyjob and commits
	updateOutput();
	mJobAssignment.setJobAssignmentStatus( JobAssignmentStatus("busy") );
	mJobAssignment.setColumnLiteral( JobAssignment::c.Started, "now()"  );
	mJobAssignment.commit();
	
	mOutputTimer->start( 30000 );
	mStatsTimer->start( 10000 );
	updatePriority( mSlave->currentPriorityName() );
}

QStringList JobBurner::processNames() const
{
    return QStringList();
}

bool JobBurner::taskStart( int task, const QString & outputName, int secondsSinceStarted )
{
	return taskStart(task, outputName, "now()" + QString(secondsSinceStarted > 0 ? " - '" + QString::number(secondsSinceStarted) + " seconds'::interval" : ""));
}

bool JobBurner::taskStart( int task, const QString & outputName, const QDateTime & startTime )
{
	return taskStart(task, outputName, startTime.isNull() ? QString() : QString("'%1'::timestamp").arg(startTime.toString(Qt::ISODate)));
}

bool JobBurner::taskStart( int task, const QString & outputName, const QString & startTime )
{
	if( mState != StateStarted && mState != StateNew ) {
		LOG_1( "Can only be called during StateStarted or StateNew, current state is: " + QString::number(mState) );
		return false;
	}
	
	if( !mLoaded ) {
		mJobAssignment.setColumnLiteral( JobAssignment::c.FirstTaskStarted, "now()" ).commit();
		mLoaded = true;
	}
	// Check if this is a duplicate taskStart
	RunningTaskList::iterator it = findRunningTaskIt(task);
	if( it != mRunningTasksList.end() ) {
		RunningTask rt = *it;
		// TODO: see if started should be updated based on secondsSinceStart
		return true;
	}

	RunningTask rt;
	rt.started = QDateTime::currentDateTime();
	rt.startedCpuTime = processCpuInfo( processId(), /*recursive=*/true ).totalTime();
	rt.taskNumber = task;
	
	Expression e = (JobTask::c.FrameNumber == ev_(task)) && (JobTask::c.Job == mJob);

	if( !outputName.isEmpty() ) {
		JobOutput jo = JobOutput::recordByJobAndName( mJob, outputName );
		if( jo.isRecord() ) {
			e &= JobTask::c.JobOutput == jo;
			rt.output = jo;
		}
	}

	rt.tasks = JobTask::select( e );
	rt.taskAssignments = rt.tasks.jobTaskAssignments();
	
	if( rt.tasks.isEmpty() ) {
		LOG_1( QString("JobBurner::slotTaskStart: Couldn't find jobtask record where fkeyjob=%1 and frame=%2").arg(mJob.key()).arg(task) );
		return false;
	}
	
	foreach( JobTaskAssignment jta, rt.taskAssignments ) {
		JobTask jt = jta.jobTask();
		if( !bool(mOptions & OptionAllowUnassignedTaskUpdates) && (jt.status() != "assigned" || jt.host() != Host::currentHost() || jta.jobAssignmentStatus().status() != "ready") ) {
			// If we are starting a task that is no longer assigned to us, then some of our
			// tasks have been re-assigned.  We currently are forced to quit and return the remaining
			// frames that are assigned to us, because none of the burners can alter the task
			// list after the first task has been started.  Someday we may be able to do this with
			// certain burners.  But the assignment process will probably be improved by then anyway.
			LOG_3(QString("JobBurner::taskStart() no longer assigned to task(%1, %2), returning").arg(task).arg(outputName));
			jobFinished();
			return false;
		}
	}

	LOG_3( "Starting task " + QString::number(task) + " task ids: " + rt.tasks.keyString() );
	rt.tasks.setStatuses( "busy" );
	rt.tasks.setHosts( Host::currentHost() );
	rt.tasks.setColumnLiteral( "startedts", startTime.isEmpty() ? "now()" : startTime );
	rt.taskAssignments.setJobAssignmentStatuses( JobAssignmentStatus("busy") );
	rt.taskAssignments.setColumnLiteral( "started", startTime.isEmpty() ? "now()" : startTime );
	updateOutput();
	rt.taskAssignments.commit();
	rt.tasks.commit();
	mRunningTasksList.append(rt);
	return true;
}

JobBurner::RunningTaskList::iterator JobBurner::findRunningTaskIt( int taskNumber, bool searchDoneTasks )
{
	RunningTaskList & list(searchDoneTasks ? mDoneTasksList : mRunningTasksList);
	RunningTaskList::iterator it = list.begin();
	while( it != list.end() ) {
		const RunningTask & rt = *it;
		if( rt.taskNumber == taskNumber )
			return it;
		++it;
	}
	return it;
}

void JobBurner::taskDone( int task, const QDateTime & endTime )
{
	taskDone(task, endTime.isNull() ? QString() : QString("'%1'::timestamp").arg(endTime.toString(Qt::ISODate)));
}

void JobBurner::taskDone( int task, const QString & endTime )
{
	if( mState != StateStarted ) {
		LOG_1( "Can only be called during StateStarted, current state is: " + QString::number(mState) );
		return;
	}

	RunningTaskList::iterator it = findRunningTaskIt(task);
	bool isAlreadyDone = false;
	if( it == mRunningTasksList.end() ) {
		// See if it was already reported done
		it = findRunningTaskIt(task,true);
		if( it == mDoneTasksList.end() ) {
			jobErrored( "No record of " + QString::number(task) + " starting" );
			return;
		}

		// This is acceptable, we will update the endedts below
		// some renderers(fusion) will report a task done multiple times
		logMessage( "taskDone called multiple times for task " + QString::number(task) );
		isAlreadyDone = true;
	}

	RunningTask rt = *it;
	
	bool needMemoryCheck = false;
	foreach( JobTask jt, rt.tasks )
		if( jt.memory() == 0 ) needMemoryCheck = true;
	if( needMemoryCheck )
		checkMemory();

	Interval cpuTime = processCpuInfo( processId(), /*recursive=*/true ).totalTime() - rt.startedCpuTime;

	LOG_3( "Task Done " + QString::number(task) + " task ids: " + rt.tasks.keyString() );
	
	if( rt.taskAssignments.size() && !ChangeSet::current().isValid() ) {
		QSqlQuery q = Database::current()->exec("SELECT jobtaskassignments_completed( ARRAY[" + rt.taskAssignments.keyString() + "], '" + cpuTime.toString() + "'::interval, NULL )");
		bool success = q.next() && q.value(0).toBool();
		
		if( !success )
			cancel();
	
	} else {
		rt.tasks.setStatuses( "done" );
		rt.tasks.setColumnLiteral( JobTask::c.Endedts, endTime.isEmpty() ? "now()" : endTime );
		rt.tasks.setCpuTimes( cpuTime );
		rt.tasks.commit();
		rt.taskAssignments.setJobAssignmentStatuses( JobAssignmentStatus("done") );
		rt.taskAssignments.setColumnLiteral( JobTaskAssignment::c.Ended, "now()" );
		rt.taskAssignments.setCpuTimes( cpuTime );
		rt.taskAssignments.commit();
	}
	
	if( !isAlreadyDone ) {
		mRunningTasksList.erase( it );
		mDoneTasksList.append( rt );
	}

	// Reset this, so that we can keep track of last frame finished until next frame started time
	mLastTaskEnded = QDateTime::currentDateTime();
	
	// Warn for exceeding max task time for every offending task
	mTaskTimeWarned = false;
}

QString JobBurner::assignedTasks() const
{
	return mTaskList;
}

JobTaskList JobBurner::assignedTaskList() const
{
	return mTasks;
}

JobTaskAssignmentList JobBurner::assignedTaskAssignments()
{
	return mTaskAssignments;
}

JobTaskList JobBurner::currentTasks() const
{
	JobTaskList ret;
	foreach( RunningTask rt, mRunningTasksList )
		ret += rt.tasks;
	return ret;
}

JobTaskAssignmentList JobBurner::currentTaskAssignments() const
{
	JobTaskAssignmentList ret;
	foreach( RunningTask rt, mRunningTasksList )
		ret += rt.taskAssignments;
	return ret;
}

JobCommandHistory JobBurner::jobCommandHistory() const
{
	return mJobCommandHistory;
}

bool JobBurner::taskStarted() const
{
	return mRunningTasksList.size();
}

QDateTime JobBurner::startTime() const
{
	return mStart;
}

QDateTime JobBurner::taskStartTime() const
{
	QDateTime ret;
	foreach( RunningTask rt, mRunningTasksList ) {
		if( !ret.isValid() || rt.started < ret )
			ret = rt.started;
	}
	return ret;
}

bool JobBurner::exceededMaxTime()
{
	if( mLoaded ) {
		if( mJob.maxTaskTime() == 0 ) return false;
		// Check since last task ended if another hasn't started
		QDateTime stime = taskStarted() ? taskStartTime() : mLastTaskEnded;
		QDateTime cdt( QDateTime::currentDateTime() );
		if( stime.secsTo( cdt ) > int(mJob.maxTaskTime()) * Config::getFloat( "assburnerTimeoutCutoffThreshold", 1.5 ) )
			return true;
		if( mLowCpuStart.isNull() && stime.secsTo( cdt ) > int(mJob.maxTaskTime()) ) {
			// Check CPU usage
			Interval pci = processCpuInfo( processId(), /* recursive= */true ).totalTime();
			// Use mLastCpuTime2 to guarantee we at least have a full stats interval
			double cpuPerc = (pci - mLastCpuTime2) / Interval(mLastStatsTime2,cdt);
			// Less than 10% then start the test
			if( cpuPerc < .1 ) {
				mLowCpuStart = cdt;
				mLowCpuTime = pci;
				// Give it 5% of it's max task time to prove it's using cpu (or maybe it's writing out the frame and not using any cpu)
				mLowCpuDecide = cdt.addSecs( ceil(mJob.maxTaskTime() * 0.05) );
				return false;
			}
			if( !mTaskTimeWarned ) {
				logError( mJob, "Task exceeded max task time of " + Interval( mJob.maxTaskTime() ).toDisplayString() + ".  Job using %" + QString::number(cpuPerc*100.0) + " cpu, not halting yet" );
				mTaskTimeWarned = true;
			}
		} else if( !mLowCpuStart.isNull() && cdt >= mLowCpuDecide ) {
			Interval pci = processCpuInfo( processId(), /* recursive= */true ).totalTime();
			// Use mLastCpuTime2 to guarantee we at least have a full stats interval
			double cpuPerc = (pci - mLowCpuTime) / Interval(mLowCpuStart,cdt);
			// Less than 10% then cut the job off
			if( cpuPerc < .1 )
				return true;
			mLowCpuStart = mLowCpuDecide = QDateTime();
			mLowCpuTime = Interval();
		}
		return false;
	}
	return mJob.maxLoadTime() > 0 && mStart.secsTo( QDateTime::currentDateTime() ) > mJob.maxLoadTime();
}

bool JobBurner::checkup()
{
	if( mState == StateStarted && exceededMaxTime() ) {
		// reload and check again in case they changed it
		mJob.reload();
		if( exceededMaxTime() ) {
			QString action( mLoaded ? "Task" : "Load" );
			QString msg = action + " exceeded max " + action + " time of " + Interval( mLoaded ? mJob.maxTaskTime() : mJob.maxLoadTime() ).toDisplayString();
			if( !mLoaded )
				saveScreenShot();
			jobErrored( msg, /*timeout=*/ true );
			return false;
		}
	}
	return true;
}

JobError JobBurner::logError( const Job & j, const QString & msg, const JobTaskList & tasks, bool timeout, JobCommandHistory jch )
{
	JobError je;
	Host host( Host::currentHost() );
	// Try to find an existing job error record for this error and host
	{
		VarList v;
		v += j.key();
		v += msg;
		v += host.key();
		JobErrorList jel = JobError::select( "fkeyJob=? and message=? and fkeyhost=? and cleared=false", v );
		if( jel.size() >= 1 )
			je = jel[0];
	}
	
	je.setJob( j );
	je.setMessage( msg );
	if( tasks.size() )
		je.setFrames( compactNumberList( expandNumberList( je.frames() ) + tasks.frameNumbers() ) );
	je.setColumnLiteral( "lastOccurrence", "now()" );
	je.setErrorTime( QDateTime::currentDateTime().toTime_t() );
	je.setHost( host );
	je.setCount( je.count() + 1 );
	je.setTimeout( timeout );
	je.setJobCommandHistory( jch );
	je.commit();
	
	LOG_1( QString("Job %1[%2] errored %3: %4").arg(j.name()).arg(j.key()).arg(tasks.size() ? (" on tasks " + compactNumberList(tasks.frameNumbers())) : "").arg(msg) );
	
	return je;
}

void JobBurner::jobErrored( const QString & msg, bool timeout )
{
	updateOutput(true);
	JobError je = logError( mJob, msg, currentTasks(), timeout, mJobCommandHistory );

	JobAssignmentStatus errorStatus("error");
	mJobAssignment.setJobError( je );
	mJobAssignment.setJobAssignmentStatus( errorStatus );
	mJobAssignment.setColumnLiteral( JobAssignment::c.Ended, "now()" );
	mJobAssignment.commit();

	currentTaskAssignments().setJobErrors(je).setJobAssignmentStatuses(errorStatus).commit();

	if( mState == StateNew || mState == StateStarted ) {
		LOG_TRACE
		mState = StateError;
		emit errored( msg );
		cleanup();
	}
}

void JobBurner::jobFinished()
{
	LOG_TRACE
	// Only allow jobFinished if we haven't errored or cancelled
	// We allow StateNew because a burner may errantly call jobFinished
	// and expect that to do the cleanup, even though no tasks have
	// been successfully finished, or they are updating the tasks
	// without using JobBurner::taskStart/taskDone...OOPBurner, though
	// it now marks the state as StateStarted.
	if( mState == StateStarted || mState == StateNew ) {
		mState = StateDone;
		mJobAssignment.setJobAssignmentStatus( JobAssignmentStatus("done") );
		mJobAssignment.setColumnLiteral( JobAssignment::c.Ended, "now()" );
		mJobAssignment.commit();
		emit finished();
	}
	// We now call cleanup again no matter if we were already errored or cancelled
	// This allows a cancelled burner to delay or halt cleanup until some event occurs
	// then call jobFinished to resume cleanup.  Since cleanup keeps track of what stages
	// have been previously completed, this has no side-effects
	cleanup();
}

void JobBurner::cancel()
{
	LOG_TRACE
	if( mState != StateDone && mState != StateError ) {
		mState = StateCancelled;
		mJobAssignment.setJobAssignmentStatus( JobAssignmentStatus( "cancelled" ) );
		mJobAssignment.setColumnLiteral( JobAssignment::c.Ended, "now()" );
		mJobAssignment.commit();
		emit cancelled();
		cleanup();
	}
}

void JobBurner::cleanup()
{
	LOG_TRACE
	do {
		int delay = cleanupStage( mCleanupStage );
		
		if( mCleanupStage == CleanupDone ) {
			emit cleaned();
			return;
		}
		// Negative delay means cleanup is halted at the current stage until cleanup is called again
		if( delay < 0 )
			return;
		
		if( delay > 0 ) {
			QTimer::singleShot(delay, this, SLOT(cleanup()));
			LOG_3( QString("Cleanup delayed for %1 ms").arg(delay) );
			return;
		} else
			mCleanupStage = (CleanupStage)(int(mCleanupStage)+1);

	} while (true);
}

int JobBurner::cleanupStage( CleanupStage stage )
{
	LOG_3( "Stage: " + QString::number(int(stage)) );
	switch( stage ) {
		case CleanupStart:
			// Nothing is done during CleanupStart, this is used to
			// let the stack unwind, which will let the rest of the output
			// be logged
			mCleanupStage = (CleanupStage)(int(mCleanupStage)+1);
			return 5; // ms
			
		case CleanupCopy:
			if( mCurrentCopy ) {
				mCurrentCopy->cancel();
				mCurrentCopy->disconnect( this );
			}
			break;
		
		case CleanupStopProcess:
			if( processId() )
				mProcessIds = processChildrenIds( processId(), /* recursive = */ true );

			if( mCmd ) {
				mCmd->disconnect( this );
				mCmd->terminate();
			}
			break;
			
		case CleanupKillProcess:
		{
			qint32 ourPid = processID();
			foreach( qint32 id, mProcessIds )
				if( id != ourPid )
					killProcess( id );
			break;
		}
		case CleanupCheckupTimers:
			if( mCheckupTimer )
				mCheckupTimer->stop();

			if( mStatsTimer )
				mStatsTimer->stop();

			if( mOutputTimer )
				mOutputTimer->stop();
			break;
			
		case CleanupOutput:
			// We need ensure any extra output after jobErrored is called is
			// logged, even if logging is not enabled for this job.
			updateOutput(mState == StateError);
			break;
		
		case CleanupDone:
			mMappingRef.release();
			deleteLater();
			break;
	}
	return 0;
}

void JobBurner::updateOutput( bool overrideLogging )
{
	if( !overrideLogging && !mJob.loggingEnabled() ) return;

	if( mJobCommandHistory.isRecord() ) {
		if( mSlave->logRootDir().isEmpty() ) {
			// Append fields to limit amount of data uploaded
			VarList vl;
			QStringList colUpdates;
			if( !mStdOut.isEmpty() ) {
				vl += mStdOut;
				colUpdates += "\"stdout\" = \"stdout\" || E?";
			}
			if( !mStdErr.isEmpty() ) {
				vl += mStdErr;
				colUpdates += "\"stderr\" = \"stderr\" || E?";
			}
			if( colUpdates.size() ) {
				vl += mJobCommandHistory.key();
				Database::current()->exec( "UPDATE jobcommandhistory SET " + colUpdates.join(", ") + " WHERE keyjobcommandhistory = ?", vl );
			}
			
		} else {
			// write to log file
			if(!mLogFilesReady)
				openLogFiles();
			mStdOutFile->write(mStdOut.toUtf8());
			mStdErrFile->write(mStdErr.toUtf8());
		}
	} else {
		mJobCommandHistory.setHost( Host::currentHost() );
		mJobCommandHistory.setJob( mJob );
		if( mSlave->logRootDir().isEmpty() ) {
			// Make sure the fields get set to empty strings(not null), so that a later || works
			mJobCommandHistory.setStdOut( mStdOut.isNull() ? QString("") : mStdOut );
			mJobCommandHistory.setStdErr( mStdErr.isNull() ? QString("") : mStdErr );
			mJobCommandHistory.setStdOut( mStdOut.isNull() ? QString("") : mStdOut );
			mJobCommandHistory.setStdErr( mStdErr.isNull() ? QString("") : mStdErr );
		} else {
			if(!mLogFilesReady)
				openLogFiles();
			mStdOutFile->write(mStdOut.toUtf8());
			mStdErrFile->write(mStdErr.toUtf8());
		}
		mJobCommandHistory.commit();
		// Caller expected to commit mJobAssignment.  This should happen
		// right away when the job is started, if logging is enabled.
		// Otherwise it will happen when an error is recorded
		mJobAssignment.setJobCommandHistory(mJobCommandHistory);
	}
	mStdOut.clear();
	mStdErr.clear();
}

static QPair<QString,QString> dp( const QString & a, const QString & b )
{ return qMakePair<QString,QString>(a,b); }

void JobBurner::getCurrentInfo( QList<QPair<QString,QString> > & td )
{
	QDateTime cdt(QDateTime::currentDateTime());
	Job j = job();
	td += dp("Name",j.name());
	td += dp("Key",QString::number(j.key()));
	td += dp("Type",j.jobType().name());
	td += dp("Project",j.project().name());
	td += dp("Services",j.jobServices().services().services().join(","));
	td += dp("Elapsed Time",Interval(startTime(), cdt).toDisplayString());
	td += dp("Current Memory Usage",QString::number( mLastMemoryInfo.currentSize / float(1024) ) + "Mb");
	td += dp("Peak Memory Usage",QString::number( mLastMemoryInfo.maxSize / float(1024) ) + "Mb");
	if( taskStarted() ) {
		td += dp("Current Task(s)","-");
		td += dp("Elapsed Time", Interval(taskStartTime(), cdt).toDisplayString());
		JobTaskList tasks = currentTasks();
		td += dp("Task Key(s)", tasks.keyString());
		QStringList taskNumbers;
		foreach( int tn, tasks.frameNumbers() )
			taskNumbers += QString::number(tn);
		td += dp("Frame Numbers", taskNumbers.join(","));
		td += dp("Task Names", tasks.labels().filter( QRegExp(".+") ).join(","));
	}
	td += dp("Command", jobCommandHistory().command());
}

void JobBurner::checkStats()
{
	if( mCmd ) {
		mLastStatsTime2 = mLastStatsTime;
		mLastStatsTime = QDateTime::currentDateTime();
		checkMemory();
		checkCpuTime();
	}
}

static int totalMemoryKb()
{
	static int sTotalMemoryKb = 0;
	if( !sTotalMemoryKb )
		sTotalMemoryKb = systemMemoryInfo().totalMemory;
	return sTotalMemoryKb;
}

void JobBurner::checkMemory()
{
	quint32 pid = processId();
	mLastMemoryInfo = processMemoryInfo( pid, /*recursive=*/ true );
	const ProcessMemInfo & pmi = mLastMemoryInfo;
	uint mem = 0, maxMem = 0;
	if( pmi.caps & ProcessMemInfo::MaxSize )
		maxMem = pmi.maxSize;
	if( pmi.caps & ProcessMemInfo::CurrentSize )
		mem = pmi.currentSize;
	
#ifdef Q_OS_WIN
	if( !mMemoryPriorityLowered && mem / double(totalMemoryKb()) > 0.9 ) {
		LOG_3( "Process is using the majority of system ram, lowering it's memory priority to prevent swapping of assburner" );
		mMemoryPriorityLowered = true;
		foreach( int pid, processChildrenIds(processId(),true) )
			setProcessMemoryPriority( pid, 4 );
	}
#endif

	maxMem = qMax(maxMem,mJobCommandHistory.memory());
	LOG_3("Process " + QString::number(processId()) + " is using memory: " + QString::number(mem) + "Kb");
	mJobCommandHistory.setMemory(maxMem);
	mJobAssignment.setMaxMemory(maxMem);
	
	// Some tasks will have their memory updated multiple times before
	// finishing.  We need to keep the largest value recorded while 
	// that task was running.  We don't trust the JobTask's value because
	// it's possible that it is left over from a previous run of that task.
	// So always use the JobTaskAssignment's value to compute the max.
	JobTaskAssignmentList taskAssignments = currentTaskAssignments();
	if( taskAssignments.size() ) {
		mem = qMax(mem,taskAssignments[0].memory());
		taskAssignments.setMemories(mem).commit();
	}
	currentTasks().setMemories(mem).commit();
	
	
	if( mJob.maxMemory() > 0 && mem > mJob.maxMemory() ) {
		// reload and check again in case they changed it
		mJob.reload();
		if( mJob.maxMemory() > 0 && mem > mJob.maxMemory() ) {
			QString msg = "Process exceeded max memory of " + QString::number( mJob.maxMemory() );
			jobErrored( msg );
			return;
		}
	}
}

void JobBurner::checkCpuTime()
{
	mLastCpuTime2 = mLastCpuTime;
	mLastCpuTime = processCpuInfo( processId(), /*recursive=*/ true ).totalTime();
	mJobAssignment.setCpuTime( mLastCpuTime ).commit();
	if( mLastStatsTime2.isValid() )
		LOG_3( "cpu time: %" + QString::number(100.0 * ((mLastCpuTime - mLastCpuTime2) / Interval(mLastStatsTime2,mLastStatsTime))) );
}

void JobBurner::addIgnoreRegEx( const QString & ignoreRegEx )
{
	mIgnoreREs += QRegExp(ignoreRegEx);
}

void JobBurner::logMessage( const QString & msg, QProcess::ProcessChannel channel )
{
	QString ts = QDateTime::currentDateTime().toString("hh:mm:ss") + ": ";
	QStringList lines = msg.split("\n");
	foreach( QString line, lines ) {
		if( line.trimmed().isEmpty() ) continue;
		foreach( QRegExp re, mIgnoreREs )
			if( line.contains( re ) )
				continue;
		QString log = ts + line + "\n";
		if( channel == QProcess::StandardOutput ) {
			if( mJob.loggingEnabled() || mStdOut.size() < 1024 * 1024 * 10 )
				mStdOut += log;
		} else {
			if( mJob.loggingEnabled() || mStdErr.size() < 1024 * 1024 * 10 )
				mStdErr += log;
		}
		LOG_3( QString(channel == QProcess::StandardOutput ? "STDOUT: " : "STDERR: ") + (line.endsWith("\n") ? line.left(line.size()-1) : line) );
	}
}

void JobBurner::syncFile( const QString & path )
{
	QFile::setPermissions( path, QFile::ReadUser | QFile::WriteUser | QFile::ReadGroup | QFile::WriteGroup | QFile::ReadOther | QFile::WriteOther );

	// copy this file to all locations that the Host isn't in	
	// this is a first pass to get things working, and will
	// make it more generic with some refactoring
	Host curHost = Host::currentHost();

	QString destPath(path);
	if( curHost.location().name() == "VCO" ) {
		destPath = "rsync://pollux/root";
	} else if ( curHost.location().name() == "SMO" ) {
		destPath = "rsync://northlic01/root";
	}
	QString cmd = "rsync -a --numeric-ids --relative "+path+" "+destPath;
	logMessage("syncFile(): "+cmd);
	QString result = backtick(cmd);
	logMessage("syncFile(): "+result);
}

void JobBurner::copyFrameNth( int frame, int frameNth, int frameRangeEnd, bool missingOnly )
{
	int frameStart = frame + 1;
	int frameEnd = qMin( frameRangeEnd, frame + frameNth - 1 );

	LOG_3( QString("Copying frames to fill nth frame hole %1 to %2").arg(frameStart).arg(frameEnd) );
	QString curFrame = mJob.outputPath();
	QRegExp pe( "^(.*)(\\.\\w\\w\\w)$" );
	if( curFrame.contains( pe ) ) {
		QString path = pe.cap(1), ext = pe.cap(2);
		curFrame = path + QString::number(frame).rightJustified(4,'0') + ext;
		for( int i = frameStart; i<=frameEnd; i++ ) {
			QString newFrame = path + QString::number(i).rightJustified(4,'0') + ext;
			if( !missingOnly || !QFile::exists( newFrame ) )
				Path::copy( curFrame, newFrame );
			LOG_1( QString("M7B: Copying %1 to %2").arg(curFrame).arg(newFrame) );
		}
	} else {
		LOG_1( "M7B: Couldn't parse outputpath for nth frame copy: " + mJob.outputPath() );
	}
}

#ifdef Q_OS_WIN
DWORD getPrio( const QString & name )
{
	QString n = name.toLower();
	if( n == "low" || n == "idle" )
		return IDLE_PRIORITY_CLASS;
	else if( n == "belownormal" )
		return BELOW_NORMAL_PRIORITY_CLASS;
	else if( n == "normal" )
		return NORMAL_PRIORITY_CLASS;
	else if( n == "abovenormal" )
		return ABOVE_NORMAL_PRIORITY_CLASS;
	else if( n == "high" )
		return HIGH_PRIORITY_CLASS;
	else if( n == "realtime" )
		return REALTIME_PRIORITY_CLASS;
	return NORMAL_PRIORITY_CLASS;
}
#endif // Q_OS_WIN

void JobBurner::updatePriority( const QString & priorityName )
{
#ifdef Q_OS_WIN
	DWORD prio = getPrio( priorityName);
	foreach( QString processName, processNames() )
		setProcessPriorityClassByName( processName, prio );
#else
	Q_UNUSED(priorityName);
#endif
}

void JobBurner::slotReadStdOut()
{
	if( mState == StateError || mState == StateDone || mState == StateCancelled ) return;
	deliverOutput( QProcess::StandardOutput );
	LOG_TRACE
}

void JobBurner::slotReadStdError()
{
	if( mState == StateError || mState == StateDone || mState == StateCancelled ) return;
	deliverOutput( QProcess::StandardError );
	LOG_TRACE
}

bool JobBurner::checkIgnoreLine( const QString & line )
{
	foreach( QRegExp re, mIgnoreREs )
		if( line.contains( re ) )
			return true;
	return false;
}

void JobBurner::deliverOutput( QProcess::ProcessChannel channel )
{
	if( mCmd ) {
		mCmd->setReadChannel( channel );
		if( mOptions & OptionProcessIOReadAll ) {
			slotProcessOutput( mCmd->readAll(), channel );
		} else {
			QTime timeLimit;
			timeLimit.start();
			while( mCmd->canReadLine() ) {
				/* With a chatty program, a busy cpu, and slow python output processing, this
				 * loop can continue for minutes, causing a no-pulse condition.  Ensure that
				 * we exit the loop and let other events process at least every 5 seconds */
				if( timeLimit.elapsed() > 5000 ) {
					QTimer::singleShot( 50, this, channel == QProcess::StandardOutput ? SLOT(slotReadStdOut()) : SLOT(slotReadStdError()) );
					break;
				}
				QString line = QString::fromLatin1( mCmd->readLine() );
				if( checkIgnoreLine( line ) ) continue;
				logMessage( line, channel );
				/* Continue logging, but don't pass subsequent lines to slotProcessOutputLine
				 * if we are already done/errored/cancelled */
				if( mState == StateError || mState == StateDone || mState == StateCancelled )
					continue;
				slotProcessOutputLine( line, channel );
			}
		}
	}
}

void JobBurner::slotProcessOutput( const QByteArray & output, QProcess::ProcessChannel channel )
{
	QStringList lines = QString::fromLatin1( output ).split('\n');
	foreach( QString line, lines ) {
		if( !checkIgnoreLine( line ) ) {
			logMessage(line, channel);
			slotProcessOutputLine( line, channel );
		}
	}
}

void JobBurner::slotProcessOutputLine( const QString & line, QProcess::ProcessChannel )
{
	foreach( QRegExp re, mErrorREs )
		if( line.contains( re ) ) {
			jobErrored( line );
			return;
		}
}

void JobBurner::slotProcessExited()
{
	LOG_TRACE;
	slotReadStdOut();
	slotReadStdError();
	if( (mState != StateDone) && (mState != StateError) && (mState != StateCancelled) )
		jobErrored( "process exited unexpectedly" );
	LOG_TRACE;
}

void JobBurner::slotProcessStarted()
{
	// This will cache mProcessId, so it can be used even after the process is deleted, for cleanup purposes
	QString pidFile = burnDir() + "pid.txt", pidTxt = QString::number(processId());
	writeFullFile( pidFile, pidTxt );
	logMessage( "Wrote pid " + pidTxt + " to file " + pidFile );
}

QString JobBurner::stringForProcessError( QProcess::ProcessError error )
{
	if( error == QProcess::FailedToStart )
		return "Failed To Start";
	else if( error == QProcess::Crashed )
		return "The process crashed some time after starting successfully.";
	else if( error == QProcess::Timedout )
		return "Timed Out";
	else if( error == QProcess::WriteError )
		return "Error Writing To Process";
	else if( error == QProcess::ReadError )
		return "An error occurred when attempting to read from the process.";
	return "An unknown error occurred.";
}

void JobBurner::slotProcessError( QProcess::ProcessError error )
{
	LOG_TRACE;
	slotReadStdOut();
	slotReadStdError();
	if( mState != StateDone && mState != StateCancelled )
		jobErrored( "Got QProcess error: (error code #" + QString::number( (int)error ) + ") " + stringForProcessError( error ));
	LOG_TRACE;
}

void JobBurner::openLogFiles()
{
	QString stdOutPath = mSlave->logRootDir() + "/" + QString::number(mJob.key()).right(3) + "/" + QString::number(mJob.key())+"_"+Host::currentHost().name()+"_"+QString::number(QCoreApplication::applicationPid()) + ".out";
	LOG_3("opening log file: "+stdOutPath);
	mStdOutFile = new QFile(stdOutPath);
	if(!mStdOutFile->open( QIODevice::Append | QIODevice::Text ))
		LOG_3("error opening log file: "+stdOutPath + " error: "+QString::number(mStdOutFile->error()));

	QString stdErrPath = mSlave->logRootDir() + "/" + QString::number(mJob.key()).right(3) + "/" + QString::number(mJob.key())+"_"+Host::currentHost().name()+"_"+QString::number(QCoreApplication::applicationPid()) + ".err";
	LOG_3("opening log file: "+stdErrPath);
	mStdErrFile = new QFile(stdErrPath);
	if(!mStdErrFile->open( QIODevice::Append | QIODevice::Text ))
		LOG_3("error opening log file: "+stdErrPath + " error: "+QString::number(mStdErrFile->error()));

	mJobCommandHistory.setStdOut( stdOutPath );
	mJobCommandHistory.setStdErr( stdErrPath );
	mLogFilesReady = true;
}


QList<qint32> JobBurner::processIds() const
{
	return mProcessIds;
}

void JobBurner::saveScreenShot()
{
	
}
