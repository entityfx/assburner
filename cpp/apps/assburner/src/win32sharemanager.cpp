
#include "path.h"

#include "host.h"
#include "notification.h"

#include "win32sharemanager.h"

Win32ShareManager::Win32ShareManager()
{}

static Win32ShareManager * sInstance = 0;

Win32ShareManager * Win32ShareManager::instance()
{
	if( !sInstance )
		sInstance = new Win32ShareManager();
	return sInstance;
}

bool Win32ShareManager::hasConflict( MappingList mappings )
{
	bool success = true;
	foreach( Mapping mapping, mappings )
	{
		MapInfo & mi = mCurrentMappings[mapping.mount()];
		if( mi.refs > 0 && mi.mapping != mapping ) {
			success = false;
			break;
		}
	}
	return success;
}

// currently assuming no conflicts
Win32ShareManager::MappingRef Win32ShareManager::map( MappingList mappings, bool forceUnmount, QString * _err )
{
	MappingRef ref;

	if( mappings.isEmpty() )
		return ref;

	bool mappingSuccess = true;
	ref = MappingRef( this, new QAtomicInt(), QString() );
	QString _mappings;
	foreach( Mapping mapping, mappings ) {
		QString driveLetter = mapping.mount();
		MapInfo & mi = mCurrentMappings[driveLetter];
		if( mi.refs == 0 ) {
			mi.mapping = mapping;
			// If we are in our own logon session then we will force the unmount
			// This needs to be accounted for when we start to use multiple burners
			// simultaneously
			QString err;
			// Mapping::map will log success at level 3, and debug info at level 5
			if( !mapping.map( forceUnmount, &err, &mi.info ) ) {
				QString msg = "Unable to map drive: " + Host::currentHost().name() + " " + err;
				LOG_1( err );
				Notification::create( "Assburner", "Map Failure", msg );
				mappingSuccess = false;
				if( _err ) *_err = err;
				break;
			}
		} else if( mi.mapping != mapping ) {
			mappingSuccess = false;
			QString msg = "Mapping conflict.  Drive " + driveLetter + " is already mapped to " + driveMapping( driveLetter[0].toLatin1() );
			LOG_5( msg );
			if( _err ) *_err = msg;
			break;
		}
		mi.refs++;
		ref.mMappings += driveLetter;
	}

	if( !mappingSuccess )
		ref.release();

	return ref;
}

void Win32ShareManager::unmap( QString mappings )
{
	for( int i=0; i < mappings.size(); i++ ) {
		QString driveLetter = QString(mappings[i]);
		MapInfo & mi = mCurrentMappings[driveLetter];
		mi.refs--;
		if( mi.refs == 0 ) {
			LOG_3( "Mapping to " + driveLetter + " released" );
		}
	}
}

QString Win32ShareManager::currentMappingsInfoString() const
{
	QStringList ret;
	for( QHash<QString,MapInfo>::const_iterator it = mCurrentMappings.constBegin(), end = mCurrentMappings.constEnd(); it != end; ++it ) {
		ret += it.value().info;
	}
	return ret.join(", ");
}

Win32ShareManager::MappingRef::MappingRef( Win32ShareManager * manager, QAtomicInt * ref, QString mappings )
: mRef( ref )
, mManager(manager)
, mMappings( mappings )
{
	if( ref )
		ref->ref();
}

Win32ShareManager::MappingRef::MappingRef( const MappingRef & other )
: mRef( other.mRef )
, mManager( other.mManager )
, mMappings( other.mMappings )
{
	if( mRef )
		mRef->ref();
}

Win32ShareManager::MappingRef & Win32ShareManager::MappingRef::operator=(const MappingRef & other)
{
	if( other.mRef != mRef ) {
		release();
		mRef = other.mRef;
		mMappings = other.mMappings;
		mManager = other.mManager;
		if( mRef ) mRef->ref();
	}
	return *this;
}

Win32ShareManager::MappingRef::MappingRef()
: mRef( 0 )
, mManager( 0 )
{}

Win32ShareManager::MappingRef::~MappingRef()
{
	release();
}

void Win32ShareManager::MappingRef::release()
{
	if( mRef && !mRef->deref() && mManager ) {
		mManager->unmap( mMappings );
		delete mRef;
	}
	mRef = 0;
}
