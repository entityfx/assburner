
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
 */

#include "Python.h"

#include <qapplication.h>
#include <qdir.h>
#include <qfileinfo.h>
#include <qmessagebox.h>
#include <qprocess.h>
#include <qtcpserver.h>
#include <qtimer.h>

#include "builtinburnerplugin.h"
#include "common.h"
#include "idle.h"
#include "killdialog.h"
#include "maindialog.h"
#include "mapwarningdialog.h"
#include "oopburner.h"
#include "slave.h"
#include "spooler.h"
#include "svnrev.h"

#include "blurqt.h"
#include "database.h"
#include "path.h"
#include "process.h"
#include "remotelogserver.h"

#include "config.h"
#include "employee.h"
#include "group.h"
#include "groupmapping.h"
#include "hostservice.h"
#include "hoststatus.h"
#include "job.h"
#include "jobassignmentstatus.h"
#include "jobburner.h"
#include "jobburnerfactory.h"
#include "jobtype.h"
#include "jobtypemapping.h"
#include "jobhistory.h"
#include "joberror.h"
#include "jobstatus.h"
#include "mapping.h"
#include "notification.h"
#include "notificationevent.h"
#include "syslog.h"
#include "user.h"
#include "usermapping.h"
#include "usergroup.h"


#ifdef Q_OS_WIN
#include "windows.h"
#include "reason.h"
#endif // Q_OS_WIN

static Slave * sInstance = nullptr;

Slave::Slave( bool gui, bool autoRegister, bool inOwnLogonSession, uint burnOnlyJobAssignmentKey, QObject * parent )
: QObject( parent )
, mInitialized( false )
, mInitStarted( false )
, mTimer( nullptr )
, mPulsePeriod( 60 )
, mIdle( nullptr )
, mIdleDelay( -1 )
, mLastIdle( 0 )
, mSpooler( nullptr )
, mOfflineSeeds( false )
, mKillDialog( nullptr )
, mInsideLoop( false )
, mLostConnectionTimer( nullptr )
, mBackgroundMode( false )
, mIsMapped( false )
, mUseGui( gui )
, mAutoRegister( autoRegister )
, mInOwnLogonSession( inOwnLogonSession )
, mMapFailureRestart( false )
, mMapFailureOffline( false )
, mParallelBurn( false )
, mBurnOnlyMode( burnOnlyJobAssignmentKey != 0 )
, mBurnOnlyJobAssignmentKey( burnOnlyJobAssignmentKey )
{
	QTimer::singleShot( 10, this, SLOT( startup() ) );
	if( !sInstance )
		sInstance = this;
}

Slave::~Slave()
{
	LOG_TRACE
	// Cleans up the burners
	cleanup();
	mTimer->disconnect(this);
	delete mTimer;
	mTimer = nullptr;
	delete mLostConnectionTimer;
	mLostConnectionTimer = nullptr;
	if( sInstance == this )
		sInstance = nullptr;
	LOG_TRACE
}

Slave * Slave::instance()
{
	return sInstance;
}

void Slave::startup()
{
	if( mInitStarted ) return;
	mInitStarted = true;
	
	// Ensure we are connected to the database before loading
	// the burner plugins
	Database::current()->connection()->checkConnection();
	connect( Database::current()->connection(), SIGNAL( connectionLost() ), SLOT( slotConnectionLost() ) );
	connect( Database::current()->connection(), SIGNAL( connected() ), SLOT( slotConnected() ) );
	
	// Environment and host list are no default select to help speed up queries
	// but since we always need the environment in assburner, we modify the schema
	Job::c.Environment->setFlag(Field::NoDefaultSelect, false);
	
	// Host table is preloaded as an optimization for most apps, but assburner
	// should never need more than it's own host record
	Host::schema()->setPreloadEnabled(false);

	mHost = Host::currentHost();

	// We dont allow the spooler to cleanup files in burn only mode
	// because it will delete spool files out from under the parent process
	mSpooler = new Spooler( this, /*allowCleanup = */ !mBurnOnlyMode );

	if( !mHost.isRecord() ) {
		if( mAutoRegister ) {
			LOG_3( "no host record, auto-registering" );
			mHost = Host::autoRegister();
		} else {
			LOG_3( "no host record, uh oh!" );
			exit(-1);
		}
	} else
		mHost.updateHardwareInfo();

	mHostStatus = mHost.hostStatus();

	if( !mHostStatus.isRecord() ) {
		if( mAutoRegister ) {
			LOG_3( "No host status record(are all your triggers installed?), creating one" );
			mHostStatus.setHost( mHost );
			mHostStatus.setOnline( 1 );
			mHostStatus.commit();
		} else {
			LOG_3( "no host status record, uh oh!" );
			exit(-1);
		}
	}

	registerBuiltinBurners();
	loadEmbeddedPython();

	// The rest of the initialization is only for normal mode where we moniter and manipulate
	// the hosts status.  In burn only mode we simply execute a job and exit
	if( mBurnOnlyMode ) {
		mInitialized = true;
		burn( JobAssignment(mBurnOnlyJobAssignmentKey) );
		return;
	}

	mService = Service::ensureServiceExists("Assburner");

	// Start remote log server and set port in our Assburner HostService record
	RemoteLogServer * rls = new RemoteLogServer();
	HostService hs = HostService::recordByHostAndService( mHost, mService );
	if( hs.isRecord() && rls->tcpServer()->isListening() ) {
		hs.setRemoteLogPort( rls->tcpServer()->serverPort() );
		hs.commit();
	}

	// Set host version string.
	mHost.setAbVersion( "v" + QString(VERSION) + " r" + QString(SVN_REVSTR));
	mHost.commit();

	loadForbiddenProcesses();

	mTimer = new QTimer( this );
	connect( mTimer, SIGNAL( timeout() ), SLOT( loop() ) );

	// Default 5000ms, min 200ms, max 600,000ms - 10 minutes
	mLoopTime = qMax( 200, qMin( 1000 * 60 * 10, Config::getInt( "assburnerLoopTime", 5000 ) ) );

	// Default mLoopTime / 2,  min 100ms, max mLoopTime
	mQuickLoopTime = qMax( 100, qMin( mLoopTime, Config::getInt( "assburnerQuickLoopTime", mLoopTime / 2 ) ) );

	// Default 60 seconds, min 10 seconds.
	mPulsePeriod = qMax( 10, Config::getInt( "assburnerPulsePeriod", 60 ) );

	mOfflineSeeds = Config::getBool( "assburnerOfflineSeeds", false );

	IniConfig & c = config();
	c.pushSection( "Assburner" );
	mParallelBurn = c.readBool( "ParallelBurn", false );
	c.popSection();

	c.pushSection( "BackgroundMode" );
	mBackgroundModeEnabled = c.readBool( "Enabled", true );
	c.popSection();
	
	c.pushSection( "Mapping" );
	mMapFailureRestart = c.readBool( "MapFailureRestart", false );
	c.popSection();
	
	// Pulse right away.
	pulse();

	// We need to set our host status to offline when assburner quits
	connect( qApp, SIGNAL( aboutToQuit() ), SLOT( offlineFromAboutToQuit() ) );

	if( mUseGui ) {
		mIdle = new Idle( this );
		connect( mIdle, SIGNAL( secondsIdle( int ) ), SLOT( slotSecondsIdle( int ) ) );
		mIdle->start();
	}

	reset(/*offline=*/ mHostStatus.slaveStatus() == "client-update-offline" );

	mInitialized = true;
	LOG_TRACE
}

#ifdef Q_OS_WIN
extern "C" void initsip(void);
extern "C" void init_Stone(void);
extern "C" void init_Classes(void);
#endif
extern "C" void initAssburner(void);

// Return the named attribute object from the named module.
// Returns a NEW reference(PyObject_GetAttrString)
static PyObject * getModuleAttr(const char *module, const char *attr)
{
#if PY_VERSION_HEX >= 0x02050000
    PyObject *mod = PyImport_ImportModule(module);
#else
    PyObject *mod = PyImport_ImportModule(const_cast<char *>(module));
#endif

    if (!mod)
    {
        PyErr_Print();
        return nullptr;
    }

#if PY_VERSION_HEX >= 0x02050000
    PyObject *obj = PyObject_GetAttrString(mod, attr);
#else
    PyObject *obj = PyObject_GetAttrString(mod, const_cast<char *>(attr));
#endif

    Py_DECREF(mod);

    if (!obj)
    {
        PyErr_Print();
        return nullptr;
    }

    return obj;
}

void Slave::loadEmbeddedPython()
{
	LOG_5( "Loading Python" );
	/*
	 * This structure specifies the names and initialisation functions of
	 * the builtin modules.
	 */
	struct _inittab builtin_modules[] = {
#ifdef Q_OS_WIN
		{"sip", initsip},
		{"blur._Stone",init_Stone},
		{"blur._Classes",init_Classes},
#endif
		{"blur.Assburner", initAssburner},
		{NULL, NULL}
	};

	PyImport_ExtendInittab(builtin_modules);
	
	Py_Initialize();

	const char * builtinModuleImportStr = 
#ifdef Q_OS_WIN
		"import imp,sys\n"
		"class MetaLoader(object):\n"
		"\tdef __init__(self):\n"
		"\t\tself.modules = {}\n"

		"\t\tself.modules['blur._Stone'] = imp.load_module('blur._Stone',None,'',('','',imp.C_BUILTIN))\n"
		"\t\tself.modules['blur._Classes'] = imp.load_module('blur._Classes',None,'',('','',imp.C_BUILTIN))\n"

		"\t\tself.modules['blur.Assburner'] = imp.load_module('blur.Assburner',None,'',('','',imp.C_BUILTIN))\n"
		"\tdef find_module(self,fullname,path=None):\n"
//		"\t\tprint 'MetaLoader.find_module: ', fullname, path\n"
		"\t\tif fullname in self.modules:\n"
		"\t\t\treturn self.modules[fullname]\n"
//		"\t\tprint 'MetaLoader.find_module: Returning None'\n"
		"sys.meta_path.append(MetaLoader())\n"
		"import blur\n"
		"blur.RedirectOutputToLog()\n";
#else
		"import imp,sys\n"
		"class MetaLoader(object):\n"
		"\tdef __init__(self):\n"
		"\t\tself.modules = {}\n"
//  blur.Stone and blur.Classes dynamically loaded
		"\t\tself.modules['blur.Assburner'] = imp.load_module('blur.Assburner',None,'',('','',imp.C_BUILTIN))\n"
		"\tdef find_module(self,fullname,path=None):\n"
//		"\t\tprint 'MetaLoader.find_module: ', fullname, path\n"
		"\t\tif fullname in self.modules:\n"
		"\t\t\treturn self.modules[fullname]\n"
//		"\t\tprint 'MetaLoader.find_module: Returning None'\n"
		"sys.meta_path.append(MetaLoader())\n"
		"import blur\n"
		"blur.RedirectOutputToLog()\n";
#endif
	PyRun_SimpleString(builtinModuleImportStr);

	LOG_5( "Loading python plugins" );

	PyObject * sys_path = getModuleAttr("sys", "path");

	if (!sys_path) {
		LOG_1( "Python Initialization Failure: Failed to get sys.path" );
		return;
	}
	
	QString dir = QDir::currentPath();
	
	// Convert the directory to a Python object with native separators.
#if QT_VERSION >= 0x040200
	dir = QDir::toNativeSeparators(dir);
#else
	dir = QDir::convertSeparators(dir);
#endif

#if PY_MAJOR_VERSION >= 3
	// This is a copy of qpycore_PyObject_FromQString().

	PyObject *dobj = PyUnicode_FromUnicode(0, dir.length());

	if (!dobj)
	{
		PyErr_Print();
		return;
	}

	Py_UNICODE *pyu = PyUnicode_AS_UNICODE(dobj);

	for (int i = 0; i < dir.length(); ++i)
		*pyu++ = dir.at(i).unicode();
#else
	PyObject *dobj = PyString_FromString(dir.toLatin1().constData());

	if (!dobj)
	{
		PyErr_Print();
		return;
	}
#endif

	// Add the directory to sys.path.
	int rc = PyList_Append(sys_path, dobj);
	Py_DECREF(dobj);
	Py_DECREF(sys_path);
	
	if (rc < 0)
	{
		PyErr_Print();
		return;
	}

	PyObject *plug_mod = PyImport_ImportModule("plugins");
	if (!plug_mod)
	{
		PyErr_Print();
		return;
	}
	Py_DECREF(plug_mod);
	
	PyEval_SaveThread();
	
}

void Slave::pulse()
{
	// Assburner pulse
	QDateTime cdt( QDateTime::currentDateTime() );
	if( !mLastPulse.isValid() || mLastPulse.secsTo(cdt) >= mPulsePeriod ) {
		LOG_3( "Emitting pulse at " + cdt.toString(Qt::ISODate) );
		// Update current available memory for the host
		mHostStatus.setAvailableMemory( systemMemoryInfo().freeMemory / 1024 /* Kb to Mb */ ).commit();
		mService.pulse();
		mLastPulse = cdt;
	}
}

void Slave::setAvailableMemory()
{
	LOG_3("Updating available memory.");

	int avail = mHostStatus.host().memory() * 1024;

	foreach( JobBurner * burner, mActiveBurners ) {
		int mem_job_needs = burner->jobAssignment().job().maxMemory();
		if ( mem_job_needs == 0 )
			mem_job_needs = burner->jobAssignment().job().jobStatus().averageMemory();

		avail -= mem_job_needs;
	}

	avail = qMax( avail, 0 );
	mHostStatus.setAvailableMemory( avail / 1024 );
	mHostStatus.commit();
}

bool Slave::checkFusionRunning()
{
// This function returns incorrect value in Mac OSX PPC and possibly others, so defaulting to false
#ifdef Q_OS_MAC
	return false;
#else
	/*
	// don't check for fusion running if we're in the middle of a batch job
	if (mJob.isRecord() && mJob.jobType().name() == "Batch") {
		return false;
	}

	// don't check for fusion running if we're in the middle of a batch job
	if (mJob.isRecord() && mJob.jobType().name() == "Fusion") {
		return false;
	}

	// if Fusion4 or 5 flow rendering, go offline
	bool fusionRunning = false;
	if (pidsByName("eyeonServer.exe") > 0) {
		LOG_3("Slave::checkFusionRunning deteced eyeon Fusion5 render process, going offline");
		fusionRunning = true;
	}
	if (pidsByName("Overseer.exe") > 0 || pidsByName("overseer.exe") > 0 ) { 
		LOG_3("Slave::checkFusionRunning detected eyeon Fusion4 render process, going offline");
		fusionRunning = true;
	}
	return fusionRunning;
	*/
#endif
	return false;
}

void Slave::loop()
{
	if( mInsideLoop ) return;
	mInsideLoop = true;

	LOG_TRACE;

	// Fetch new status from the database
	QString oldStatus = mHostStatus.slaveStatus();
	mHostStatus.reload();
	QString status = mHostStatus.slaveStatus();

	pulse();

	handleStatusChange( status, oldStatus );

	if( mHostStatus.lastAssignmentChange() != mLastAssignmentChange ) {
		mLastAssignmentChange = mHostStatus.lastAssignmentChange();

		Expression assignmentsByKey;
		if( mActiveAssignments.size() )
			assignmentsByKey = JobAssignment::c.Key.in(mActiveAssignments);

		JobAssignmentList assignments = JobAssignment::select( JobAssignment::c.Host == mHost && (assignmentsByKey || JobAssignment::c.JobAssignmentStatus == JobAssignmentStatus("ready")) );
		
		foreach( JobAssignment ja, assignments )
			handleJobAssignmentChange( ja );
	}
	
	// Sometimes our status goes to assigned but the assignment gets canceled
	// before the burner gets created, we need to ensure we go back to ready
	if( status == "assigned" && mActiveAssignments.size() == 0 )
		setStatus("ready");

	// Decrement the mQuickLoopCount each time through the loop,
	// when it reaches 0, go back to normal loop time
	if( mQuickLoopCount > 0 ) {
		mQuickLoopCount--;
		if( mQuickLoopCount == 0 )
			mTimer->start( mLoopTime );
	}

	mInsideLoop = false;

	if( mMapFailureOffline && status == "offline" && mMapFailureOfflineTime.secsTo( QDateTime::currentDateTime() ) > (30 * 60) /* 30 minutes */ )
		online();
}

void Slave::handleStatusChange( const QString & status, const QString & oldStatus )
{
	if( (status == "assigned" || status == "copy" || status == "busy") && checkFusionRunning() ) {
		offline();
		mIdleDelay = 1;
		mInsideLoop = false;
		return;
	}

	// Return if nothing has changed
	if( oldStatus == status ) {
		LOG_6("oldStatus same as mHost.slaveStatus, returning");
		mInsideLoop = false;
		return;
	}

	LOG_3( "Got new status: " + status );

	if( status == "assigned" || status == "busy" || status == "copy" ) {
		// Do nothing
	}
	else if ( status == "client-update" ) {
		emit statusChange( "busy" );
		clientUpdateLegacy( oldStatus != "offline" );
	} else if( status == "offline" || status == "stopping" ) {
		// When we get set offline remotely, don't go online again
		// Until someone presses Start the burn, or until
		// we are set online remotely.  The idledelay
		// will be reset when we go online again
		mIdleDelay = -1;
		offline();
	} else if( status == "starting" ) {
		reset();
	} else if( status == "restart" ) {
		restart();
	} else if( status == "restart-when-done" ) {
		if( oldStatus == "ready" )
			restart();
		else
			return;
	} else if( status == "reboot" ) {
		reboot("Assburner rebooting due to remote status change");
	} else if( status == "reboot-when-done" ) {
		if( oldStatus == "ready" )
			reboot("Assburner rebooting due to remote status change");
		else
			return;
	} else if( status == "sleep" ) {
		goToSleep();
	} else if( status == "busy" ) {
		// When using OOPBurner the child process sets the status to busy
		emit copyProgressChange( 100 );
		emit statusChange( status );
	} else {
		emit statusChange( "error" );
		// Go to ready if status is unknown.
		reset();
	}
}

void Slave::restartFromLostConnectionTimer()
{
	LOG_TRACE

	if (mLostConnectionTimer)
		mLostConnectionTimer->disconnect(this);

	cleanup();

	LOG_5("calling qApp->disconnect(this)");
	qApp->disconnect( this );

	LOG_5("calling qApp->quit()");
	qApp->quit();

	LOG_5("calling exit");
	exit(-1);
}

void Slave::restart( bool needNewLogonSession )
{
	// Cleanup any current jobs
	cleanup();
	// Return any job assignments
	mHostStatus.returnSlaveFrames("restart");
	// Disconnect this, so that we dont set the status to offline
	// the status will stay at restart
	qApp->disconnect( this );

#ifdef Q_OS_WIN
	CloseHandle( hMutex );
#endif

	QStringList args = QCoreApplication::instance()->arguments();
	if( !needNewLogonSession )
		args.removeAll( "-in-new-logon-session" );
	QProcess::startDetached( "assburner.exe", args );

	qApp->quit();
}

void Slave::reboot( QString message )
{
	if( message.isEmpty() )
		message = "Assburner rebooting, reason unknown";
	
	LOG_TRACE

	// Cleanup any current jobs
	cleanup();

	// Return any job assignments
	LOG_5("calling mHostStatus.returnSlaveFrames()");
	mHostStatus.returnSlaveFrames( "reboot" );
	LOG_5("call to mHostStatus.returnSlaveFrames() done");

	// Disconnect this, so that we dont set the status to offline
	// the status will stay at restart
	LOG_5("calling qApp->disconnect(this)");
	qApp->disconnect( this );
	
	// If reboot fails we notify IT
	if( !systemShutdown(/*reboot=*/true, message) ) {
		LOG_1( "System reboot failed" );
		Notification::create( "Assburner", "Reboot Failure", mHost.name() + " failed to reboot" );
		setStatus( "offline" , true );
		return;
	}
	QTimer::singleShot( 60 * 1000, this, SLOT( recoverFromFailedShutdown() ) );
}

void Slave::recoverFromFailedReboot()
{
	LOG_1( "System reboot timed out, sending error notification and going online" );
	Notification::create( "Assburner", "Reboot Failure", mHost.name() + " failed to reboot after 60 seconds" );
	online();
}

void Slave::goToSleep()
{
	LOG_TRACE
	// Cleanup any current jobs
	cleanup();

	// Return any job assignments
	if( !setStatus( "sleeping" ) )
		return;

	// Disconnect this, so that we dont set the status to offline
	// the status will stay at sleeping
	LOG_5("calling qApp->disconnect(this)");
	qApp->disconnect( this );

	if( systemShutdown() ) {
		killAll( "abpsmon.exe" );
		LOG_5("calling qApp->quit()");
		qApp->quit();
	} else
		reset();
}

void Slave::clientUpdate( bool onlineAfterUpdate )
{
	// Go offline right now
	if( mTimer )
		mTimer->stop();

	// Dont want offline to get called again when we quit
	qApp->disconnect( this );

	if( onlineAfterUpdate )
		setStatus( "client-update" );
	else
		setStatus( "client-update-offline" );

	LOG_5( "quitting" );
	qApp->quit();
}

void Slave::clientUpdateLegacy( bool onlineAfterUpdate )
{
	// Go offline right now
	offline();
	if( mTimer )
		mTimer->stop();

	// Dont want offline to get called again when we quit
	qApp->disconnect( this );

	if( !onlineAfterUpdate )
		setStatus( "client-update-offline" );

	// Reload the config entry just in case it has changed
	Config c = Config::recordByName( "assburnerInstallerPath" );
	c.reload();

	QString install = c.value();
	//QFileInfo fi( install );
	//QString tp( "c:\\temp\\" + fi.fileName() );
	//Path::copy( install, tp );
	LOG_3( "running " + install );
	QProcess::startDetached( install );
	LOG_5( "calling quit()" );
	qApp->quit();
}

QList<JobBurner*> Slave::activeBurners()
{
	return mActiveBurners;
}

JobAssignmentList Slave::activeAssignments()
{
	return mActiveAssignments;
}

void Slave::burn( const JobAssignment & jobAssignment )
{
	Job job = jobAssignment.job();
	LOG_3( QString("Starting the burn: JobType=%1, Job.keyJob=%2, Job.name=%3").arg(job.jobType().name()).arg(job.key()).arg(job.name()) );

	JobBurner * burner = nullptr;
	QString errorMessage;
	if( JobBurnerFactory::supportsJobType( jobAssignment ) ) {
#ifdef Q_OS_WIN
		if( !mBurnOnlyMode && !job.userName().isEmpty() ) {
			burner = new OOPBurner( jobAssignment, this );
		} else
#endif
		{
			burner = JobBurnerFactory::createBurner( jobAssignment, this, &errorMessage );
		}
	} else
		errorMessage = QString("JobType %1 not supported by this host").arg( job.jobType().name() );
	
	if( !burner ) {
		errorMessage = QString("Couldn't Create Burner: ") + errorMessage;
		LOG_1( errorMessage );
		JobError je = JobBurner::logError( job, errorMessage, JobTaskList(), false, JobCommandHistory() );

		JobAssignment(jobAssignment)
		  .setJobError( je )
		  .setJobAssignmentStatus( JobAssignmentStatus( "error" ) )
		  .setColumnLiteral( JobAssignment::c.Ended, "now()" )
		  .commit();

		checkAllBurnersComplete();
		return;
	}

	/* If a JobBurner decides during construction that something is wrong, it may
	 * error or cancel itself.   Since burners always clean up after themselves
	 * the pointer will no longer be valid after we return to the event loop,
	 * so we can simply return nullptr. */
	JobBurner::State state = burner->state();
	if( state == JobBurner::StateCancelled || state == JobBurner::StateError )
		return;
	
	connect( burner, SIGNAL( errored( const QString & ) ), SLOT( slotError( const QString & ) ) );
	connect( burner, SIGNAL( finished() ), SLOT( slotBurnFinished() ) );
	connect( burner, SIGNAL( cancelled() ), SLOT( slotBurnFinished() ) );
	// This is only useful in the off-chance that a buggy burner has not
	// called one of the above signals, which may even be impossible.
	connect( burner, SIGNAL( cleaned() ), SLOT( slotBurnFinished() ) );

	mActiveBurners += burner;
	setActiveAssignments( mActiveAssignments + jobAssignment );
	burner->start();
	emit statusChange( "busy" );
}

void Slave::setActiveAssignments( JobAssignmentList activeAssignments )
{
	JobAssignmentList oldActiveAssignments( mActiveAssignments );
	mActiveAssignments = activeAssignments;
	emit activeAssignmentsChange( activeAssignments, oldActiveAssignments );
}

void Slave::handleJobAssignmentChange( const JobAssignment & ja )
{
	QString status = ja.jobAssignmentStatus().status();
	if( status == "ready" && !mActiveAssignments.contains(ja) ) {
		burn(ja);
	}
	else if( status == "cancelled" ) {
		foreach( JobBurner * burner, mActiveBurners ) {
			if( burner->jobAssignment() == ja ) {
				cleanupJobBurner(burner);
				break;
			}
		}
	}
}


Host Slave::host() const
{
	return mHost;
}

HostStatus Slave::hostStatus() const
{
	return mHostStatus;
}

QString Slave::status() const
{
	return mHostStatus.slaveStatus();
}

Job::HostCancelModeEnum Slave::hostCancelMode()
{
	uint ret = 0;
	foreach( JobBurner * burner, mActiveBurners )
		ret = qMax(ret,burner->job().hostCancelMode());
	return Job::HostCancelModeEnum(ret);
}

static bool burnerDone( JobBurner * burner )
{
	JobBurner::State state = burner->state();
	return (state == JobBurner::StateDone || state == JobBurner::StateError || state == JobBurner::StateCancelled);
}

/*
 * This is the only function that removes burners from the mActiveBurners list.
 * When the size of the list goes to zero, reset is called, setting the slave
 * status to ready, and calling returnSlaveFrames, which ensures all assignments
 * are reset(cancelled).
 *
 * Because of this, we have to be sure we aren't calling reset more than once
 * otherwise we will have a race condition where the we could be assigned between
 * the first call and subsequent calls, hence the initial check and return if
 * the list is already empty.
 */
void Slave::checkAllBurnersComplete()
{
	if( mActiveBurners.size() == 0 )
		return;
	bool allBurnersComplete = true;
	foreach( JobBurner * burner, mActiveBurners ) {
		if( burnerDone(burner) ) {
			mActiveBurners.removeAll(burner);
			setActiveAssignments( mActiveAssignments - burner->jobAssignment() );
		} else
			allBurnersComplete = false;
	}
	if( allBurnersComplete )
		reset();
}

void Slave::slotBurnFinished()
{
	// If we have a successful burn, reset the map failure restart flag
	mMapFailureRestart = false;
	
	checkAllBurnersComplete();
}

void Slave::slotError( const QString & msg )
{
	checkAllBurnersComplete();
}

void Slave::reportMappingFailure()
{
	if( mMapFailureRestart ) {
		// We already had a map failure restart, so lets go offline
		offline();
		Notification::create( "Assburner", "Map Failure", mHost.name() + ": Setting Host Offline for repeated mapping failure after restart" );
		mMapFailureOfflineTime = QDateTime::currentDateTime();
		mMapFailureOffline = true;
	} else {
		IniConfig & cfg = config();
		cfg.pushSection( "Mapping" );
		cfg.writeBool( "MapFailureRestart", true );
		cfg.popSection();
		restart( /*needNewLogonSession =*/ true );
	}
}

bool Slave::checkForbiddenProcesses()
{
	if( mUseGui && !mKillDialog ) {
		QStringList procsToKill;
		LOG_3( "Checking Forbidden process list: " + mForbiddenProcesses.join(",") );
		foreach( QString procName, mForbiddenProcesses )
			if( pidsByName( procName ) > 0 )
				procsToKill << procName;

		if( procsToKill.size() > 0 ) {
			LOG_3( "Showing killdialog, forbidden processes running: " + procsToKill.join(",") );
			mKillDialog = new KillDialog(procsToKill);
			mKillDialog->setModal( true );
			connect( mKillDialog, SIGNAL( finished( int ) ), SLOT( killDialogFinished( int ) ) );
			mKillDialog->show();
			return false;
		}
	}
	return !bool(mKillDialog);
}

void Slave::killDialogFinished( int status )
{
	bool goOnline = ( status == QDialog::Accepted );
	LOG_3( "status == " + QString::number( status ) + (goOnline ? ", going online" : ", staying offline") );
	mKillDialog->deleteLater();
	mKillDialog = nullptr;
	if( goOnline )
		reset();
}

void Slave::cleanupJobBurner( JobBurner * burner )
{
	LOG_TRACE
	burner->disconnect(this);
	burner->cancel();
	mActiveBurners.removeAll(burner);
	setActiveAssignments( mActiveAssignments - burner->jobAssignment() );
	setAvailableMemory();
	if( mActiveBurners.size() == 0 && this->status() == "ready" )
		emit statusChange( "ready" );
}

// Cleans up any currently active job(s)
void Slave::cleanup()
{
	foreach( JobBurner * burner, mActiveBurners ) {
		cleanupJobBurner(burner);
	}
}

// Resets assburner to offline or online
void Slave::reset( bool offline )
{
	// This will be called when the burner(s) are done
	if( mBurnOnlyJobAssignmentKey ) {
		qApp->exit(0);
		return;
	}

	if( mKillDialog && !offline ) {
		LOG_5( "Forcing reset status to offline while kill dialog is active" );
		offline = true;
	}

	LOG_5("Current status is " + QString(status()) + ". Called with offline==" + QString(offline ? "true" : "false"));

	if( status() == "restart-when-done" ) {
		restart();
		return;
	}
	if( status() == "reboot-when-done" ) {
		reboot("Assburner rebooting due to remote status change");
		return;
	}

	cleanup();

	if( !offline ) {
		IniConfig & c = config();
		c.pushSection( "Assburner" );
		mIdleDelay = c.readInt( "OnlineAfterIdleMinutes", -1 );
		c.popSection();
		if( mHost.allowMapping()
		 && !mInOwnLogonSession
		 && mUseGui
		 && mHostStatus.slaveStatus() == "offline" 
		 && Employee(User::currentUser()).isRecord() 
		 && userConfig().readBool( "WarnBeforeMapping", true ) ) {
			MapWarningDialog * mwd = new MapWarningDialog(qobject_cast<QWidget*>(parent()));
			int result = mwd->exec();
			delete mwd;
			if( result == QDialog::Rejected ) {
				this->offline();
				return;
			}
		}
	}

	// Do this before the exclusive row lock, because we want to hold that lock
	// for as little time as possible.
	pulse();

	// Since we are going to return the frames no matter what, there is no reason to do
	// this inside a transaction.
	mHostStatus.returnSlaveFrames(offline ? "offline" : "ready");
	emit statusChange(status());
	
	if( offline && !mInOwnLogonSession && mHost.allowMapping() )
		restoreDefaultMappings();

	// No need for quick loop time if we are going offline
	if( !offline ) {
		// Anytime we are reset, use quick loop time to check if we get new
		// status soon.  If we don't, then fall back to regular loop time.
		mQuickLoopCount = 2;
		mTimer->start( mQuickLoopTime );
	}

	LOG_5( "Returning" );
}

QString Slave::currentPriorityName() const
{
	QString ret;
	if( mBackgroundModeEnabled ) {
		IniConfig & c = config();
		c.pushSection( "BackgroundMode" );
		QString key(mBackgroundMode ? "BackgroundPriority" : "ForegroundPriority");
		QString def(mBackgroundMode ? "idle" : "normal");
		ret = c.readString( key, def );
		c.popSection();
	}
	return ret;
}

void Slave::execPriorityMode()
{
	QString priorityName = currentPriorityName();
	if( !priorityName.isEmpty() ) {
		LOG_3( "Setting priority to: " + priorityName );
		foreach( JobBurner * burner, mActiveBurners )
			burner->updatePriority( priorityName );
	}
}

void Slave::updatePriorityMode( int secondsIdle )
{
	LOG_6( "seconds idle: " + QString::number(secondsIdle) );
	IniConfig & c = config();
	c.pushSection( "BackgroundMode" );
	if( c.readBool( "Enabled", true ) ) {
		int cutoff = c.readInt( "CutoffTime", 60 );
		if( (secondsIdle < cutoff) != mBackgroundMode ) {
			mBackgroundMode = (secondsIdle < cutoff);
			LOG_3( "Setting background mode: " + QString(mBackgroundMode ? "true" : "false") );
			execPriorityMode();
		}
	}
	c.popSection();
}

void Slave::slotSecondsIdle( int idle )
{
	// If a user starts using a host and assburner is offline because of a map failure
	// we don't want to go online without the user expecting it.  If the user was already
	// using the host when it went offline(mLastIdle will be < 30), then we can go back
	// online.
	if( mMapFailureOffline && mLastIdle > 30 && idle < mLastIdle )
		mMapFailureOffline = false;
	mLastIdle = idle;
    updatePriorityMode(idle);
	if( mIdleDelay > 0 && idle > 60 * mIdleDelay && mHost.hostStatus().slaveStatus() == "offline" ) {
		if( checkFusionRunning() )
			mIdleDelay = 1;
		else
			reset();
	}
}

void Slave::online()
{
	LOG_TRACE
	// Avoid a reset if we are already online
	QString currentStatus = status();
	if( currentStatus == "ready" || currentStatus == "assigned" || currentStatus == "busy" )
		return;
	checkForbiddenProcesses();
	reset( false );
	LOG_TRACE
}

void Slave::offlineFromAboutToQuit()
{
	if( status() == "no-pulse" ) return;
	LOG_5("calling real offline method");
	offline();
	setStatus( "no-pulse", true );
}

void Slave::offline()
{
	reset( true );
}

void Slave::toggle()
{
	LOG_TRACE
	if (status() == "offline")
		online();
	else
		offline();
}

bool Slave::setStatus( const QString & nextStatus, bool force )
{
	LOG_3( "Setting Status: " + nextStatus );
	QString status = mHostStatus.slaveStatus();
	Database::current()->beginTransaction();
	// Locks the row, and updates mHost
	mHostStatus.reload( true /* Lock For Update */ );
	if( !mHostStatus.isRecord() ) {
		LOG_1( "HostStatus record is now missing!!!!!!" );
		Database::current()->rollbackTransaction();
		qApp->exit(1);
		return false;
	}
	QString newStatus = mHostStatus.slaveStatus();
	// Ignore external change to busy, we'll get updated to busy from a oopburner before it finishes
	if( newStatus != status && newStatus != nextStatus && !force && newStatus != "busy" ) {
		// Send offline as last status if that's what we were trying to do
		// That way if client-update is the newStatus, it'll go offline after the
		// client update is done
		Database::current()->commitTransaction();
		if( !mBurnOnlyMode )
			handleStatusChange( newStatus, nextStatus == "offline" ? nextStatus : status );
		return false;
	}

	if( nextStatus == "busy" )
		emit copyProgressChange(100);
	
	mHostStatus.setSlaveStatus( nextStatus );
	mHostStatus.commit();
	Database::current()->commitTransaction();
	emit statusChange( nextStatus );
	LOG_5( "returning after setting status " + nextStatus );
	return true;
}

void Slave::setUseGui( bool gui )
{
	mUseGui = gui;
}

void Slave::slotConnectionLost()
{
	if (mTimer) {
		mTimer->stop();
	}
	if( mLostConnectionTimer ) return;
	mLostConnectionTimer = new QTimer( this );
	connect( mLostConnectionTimer, SIGNAL( timeout() ), SLOT( restartFromLostConnectionTimer() ) );
	mLostConnectionTimer->start( 1000 * 60 * 15 ); // 15 min
	LOG_5("mLostConnectinTimer created, started. returning from slotConnectionLost()");
}

void Slave::slotConnected()
{
	LOG_TRACE
	if (mTimer) {
		mTimer->start(mLoopTime);
	}
	if( mLostConnectionTimer ) {
		LOG_5( "mLostConnectionTimer->stopped, deleting");
		mLostConnectionTimer->stop();
		delete mLostConnectionTimer;
		mLostConnectionTimer = nullptr;
	}
	LOG_TRACE
}

void Slave::restoreDefaultMappings()
{
	if( mIsMapped && !mInOwnLogonSession ) {
		if( mIsMapped 
		 		&& Employee(User::currentUser()).isRecord() 
				&& userConfig().readBool( "WarnBeforeReMapping", true ) 
				&& mUseGui ) {
			ReMapWarningDialog * mwd = new ReMapWarningDialog(qobject_cast<QWidget*>(parent()));
			mwd->exec();
			delete mwd;
		}
		User u = User::currentUser();
		GroupList gl = u.userGroups().groups();
		MappingList mappings;
		foreach( Group g, gl )
			mappings += g.groupMappings().mappings();
	
		QString err;
		// Restore the group mappings, then the user mappings
		// The user mappings are done second so they can override
		// the group mappings
		for( int i=0; i<2; i++ ) {
			foreach( Mapping mapping, mappings ) {
				if( !mapping.map(true, &err) )
					LOG_1( err );
			}
			mappings = u.userMappings().mappings();
		}
		mIsMapped = false;
	}
}

void Slave::showRemapWarning( const QString & drive )
{
	if( mUseGui && !mDriveRemapWarningDialog ) {
		mDriveRemapWarningDialog = new QMessageBox( "Unable to re-map drive " + drive,
			"Assburner is unable to re-map drive " + drive + " because it is being used by another program.\n"
			"Please close any programs using this drive.",
			QMessageBox::Warning, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton );
		mDriveRemapWarningDialog->show();
		mDriveRemapWarningDialog->setAttribute( Qt::WA_DeleteOnClose, true );
	}
}

void host_error( const QString & text, const QString & method, const Service & service )
{
	Host host = Host::currentHost();
	SysLog sl;
	sl.setMessage( text );
	sl.set_class( "Assburner" );
	sl.setCount( 1 );
	sl.setCreated( QDateTime::currentDateTime() );
	sl.setHost( host );
	sl.setValue( "fkeySysLogRealm", 6 ); // Render
	sl.setValue( "fkeySysLogSeverity", 4 ); // Major
	sl.setLastOccurrence( QDateTime::currentDateTime() );
	sl.setMethod( method );
	sl.commit();
	HostService hs = HostService::recordByHostAndService( host, service );
	hs.setSysLog( sl );
	hs.commit();
	return;
}

ServiceList Slave::enabledServiceList()
{
	return Service::select(Service::c.Enabled==true && Service::c.Key.in(HostService::c.Host==mHost && HostService::c.Enabled==true));
}

void Slave::loadForbiddenProcesses()
{
	mForbiddenProcesses.clear();
	ServiceList enabledServices = enabledServiceList();
	foreach( QString forbiddenProcesses, enabledServices.forbiddenProcesses() )
		foreach( QString procName, forbiddenProcesses.split(",") )
			if( !procName.isEmpty() )
				mForbiddenProcesses << procName;
}
/*
void Slave::checkDriveMappingAvailability()
{
	ServiceList enabledServices = enabledServiceList();
	foreach( 
}*/

void Slave::setInOwnLogonSession( bool iols )
{
	mInOwnLogonSession = iols;
}

void Slave::setIsMapped( bool isMapped )
{
	mIsMapped |= isMapped;
}

QString Slave::logRootDir() const
{
	return Config::getString("assburnerLogRootDir");
}

