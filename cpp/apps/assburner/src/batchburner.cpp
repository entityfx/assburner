
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 * $Id$
  */

#ifdef COMPILE_BATCH_BURNER

#include <qdir.h>
#include <qfileinfo.h>
#include <qlibrary.h>

#ifdef Q_OS_WIN
#include <windows.h>
typedef BOOL /*WINAPI*/ (*ExtWow64DisableWow64FsRedirection) ( PVOID *OldValue );
typedef BOOL /*WINAPI*/ (*ExtWow64RevertWow64FsRedirection) ( PVOID OldValue );
ExtWow64DisableWow64FsRedirection getWow64DisableWow64FsRedirection()
{
    static ExtWow64DisableWow64FsRedirection cpwl = (ExtWow64DisableWow64FsRedirection)QLibrary::resolve( "kernel32", "Wow64DisableWow64FsRedirection" );
    return cpwl;
}
ExtWow64RevertWow64FsRedirection getWow64RevertWow64FsRedirection()
{
    static ExtWow64RevertWow64FsRedirection cpwl = (ExtWow64RevertWow64FsRedirection)QLibrary::resolve( "kernel32", "Wow64RevertWow64FsRedirection" );
    return cpwl;
}
#else
#include <unistd.h>
#endif

#include "database.h"
#include "path.h"
#include "process.h"
#include "user.h"

#include "jobbatch.h"

#include "batchburner.h"
#include "slave.h"

BatchBurner::BatchBurner( const JobAssignment & jobAssignment, Slave * slave )
: JobBurner( jobAssignment, slave, OptionExecutableIsCommandString | OptionMergeStdError )
{}

BatchBurner::~BatchBurner()
{}

QString BatchBurner::executable()
{
	JobBatch jb(mJob);
	QString cmd = jb.cmd().replace( "${JOBFILE}", burnFile() );
	if( jb.passSlaveFramesAsParam() )
		cmd += " " + assignedTasks();
#ifndef Q_OS_WIN
	if ( jb.runasSubmitter() ) {
		cmd = "/bin/su " + mJob.user().name() + " -c \"" + cmd + "\"";
	}
#endif
	return cmd;
}

QString BatchBurner::workingDirectory()
{
	return burnDir();
}

void BatchBurner::slotProcessStarted()
{
	foreach (int taskNumber, expandNumberList(assignedTasks())){
		taskStart( taskNumber );
	}
}

void BatchBurner::startProcess()
{
#ifdef Q_OS_WIN
	bool disableWow64Redirect = isWow64() && JobBatch(mJob).disableWow64FsRedirect();
	PVOID oldVal;
	if( disableWow64Redirect ) {
		ExtWow64DisableWow64FsRedirection func_p = getWow64DisableWow64FsRedirection();
		if( !func_p || !getWow64RevertWow64FsRedirection() ) {
			jobErrored( "Failed to find Wow64DisableWow64FsRedirection and Wow64RevertWow64FsRedirection" );
			return;
		}
		if( !func_p( &oldVal ) ) {
			jobErrored( "Failed to disable Wow64 Filesystem redirection" );
			return;
		}
	}
#endif
	JobBurner::startProcess();
#ifdef Q_OS_WIN
	if( disableWow64Redirect )
		getWow64RevertWow64FsRedirection()( oldVal );
#endif
	mCheckupTimer->start( 15 * 1000 );
}

void BatchBurner::slotProcessExited()
{
	if( mCmd ) {
		if( mCmd->exitCode() == 0 ) {
			foreach ( JobTask task,  currentTasks() ) {
				taskDone( task.frameNumber() );
			}
			jobFinished();
		} else
			jobErrored( "Batch script exited with result: " + QString::number( mCmd->exitCode() ) );
	} else
		jobErrored( "mCmd is null, contact render farm admin." );
	JobBurner::slotProcessExited();
}

void BatchBurner::cleanup()
{
#ifndef Q_OS_WIN
	killAll("maya.bin");
	// sleep here so we don't stomp on exiting maya's own cleanup
	sleep(2);
	logMessage("cleanup() removing files from /usr/tmp/ and /tmp/");
	QProcess::execute("sh -c \"rm -f /usr/tmp/fb*\"");
	QProcess::execute("sh -c \"rm -f /usr/tmp/*.ma\"");
	QProcess::execute("sh -c \"rm -f /usr/tmp/*.mel\"");
	QProcess::execute("sh -c \"rm -f /usr/tmp/*.iff\"");
	QProcess::execute("sh -c \"rm -rf /usr/tmp/mayaDiskCache*\"");
	QProcess::execute("sh -c \"rm -rf /usr/tmp/Shake*\"");
	QProcess::execute("sh -c \"rm -rf /usr/tmp/nuke*\"");
#endif
	JobBurner::cleanup();
}

#endif
