
#include <qmetaobject.h>

#include "process.h"

#include "jobtype.h"

#include "slave.h"
#include "oopburner.h"

OOPBurner::OOPBurner( const JobAssignment & jobAssignment, Slave * slave )
: JobBurner( jobAssignment, slave )
#ifdef Q_OS_WIN
, mProcess( 0 )
#endif
{}

OOPBurner::~OOPBurner()
{}

QStringList OOPBurner::processNames() const
{
	return QStringList() <<
#ifdef Q_OS_WIN
		"assburner.exe";
#else
		"assburner";
#endif
}

QString OOPBurner::executable()
{
	return workingDirectory() +
#ifdef Q_OS_WIN
		"assburner.exe";
#else
		"assburner";
#endif
}

QString OOPBurner::workingDirectory()
{
#ifdef Q_OS_WIN
	return "c:/blur/assburner/";
#else
	return "";
#endif
}

QStringList OOPBurner::buildCmdArgs()
{
	return QStringList() << "-burn" << QString::number(mJobAssignment.key());
}

QStringList OOPBurner::environment()
{
	return QStringList();
}

void OOPBurner::startProcess()
{
#ifdef Q_OS_WIN
	mProcess = new Win32Process(this);
	connect( mProcess, SIGNAL(started()), SLOT(slotProcessStarted()) );
	connect( mProcess, SIGNAL(error(QProcess::ProcessError)), SLOT(slotProcessError(QProcess::ProcessError)) );
	connect( mProcess, SIGNAL(finished(int, QProcess::ExitStatus)), SLOT(slotProcessExited()) );
	if( mState == StateError ) return;
	QString cmd = executable();
	if( mState == StateError ) return;
	QStringList args = buildCmdArgs();
	if( mState == StateError ) return;
	QString wholeCmd = cmd + " " + args.join(" ");
	QString wd = workingDirectory();
	if( mState == StateError ) return;
	QString userString = mJob.domain().isEmpty() ? mJob.userName() : mJob.domain() + "/" + mJob.userName();
	LOG_5( "Starting command: " + wholeCmd + (wd.isEmpty() ? QString() : QString(" in directory " + wd)) + " as user " + userString );
	if( !wd.isEmpty() )
		mProcess->setWorkingDirectory( workingDirectory() );
	QStringList env = environment();
	if( !env.isEmpty() )
		mProcess->setEnvironment( env );
	mProcess->setLogon( mJob.userName(), mJob.password(), mJob.domain() );
	
	if( mProcess->start( cmd, args ) )
		mState = StateStarted;
	// else errored signal will set state to StateError
#else
	JobBurner::startProcess();
#endif
}

void OOPBurner::start()
{
	// Go directly to startBurn, skip the copy step as this will
	// be done by the spawned assburner
	startBurn();
}

void OOPBurner::startBurn()
{
	// Skip drive mapping, JCH setup, memory and output timers
	mStart = QDateTime::currentDateTime();
	startProcess();
}

// For client update jobs mimic Jobburner's implementation
// but don't cancel the JobAssignment as we will rely on the 
// external assburner process to do any JobAssignment cleanup
void OOPBurner::cancel()
{
	if( mJob.jobType().name() == "ClientUpdate" )
	{
		LOG_TRACE
		if( mState != StateDone && mState != StateError ) {
			mState = StateCancelled;
			emit cancelled();
			cleanup();
		}
	} else
		JobBurner::cancel();
}

void OOPBurner::cleanup()
{
	JobBurner::cleanup();
}

bool OOPBurner::checkup()
{
	return true;
}

void OOPBurner::slotProcessExited()
{
	// We can always report success as any error should have already been reported
	jobFinished();
}

void OOPBurner::slotProcessStarted()
{
#ifdef Q_OS_WIN
	LOG_5( "Process started with pid: " + QString::number(qpidToId(mProcess->pid())) );
#endif
	// For client update job type, we call Slave::clientUpdate, which will exit assburner
	// so that the installers can complete successfully, since they have to overwrite assburner
	// itself and dlls in uses
	if( mJob.jobType().name() == "ClientUpdate" )
		mSlave->clientUpdate();
}

void OOPBurner::slotProcessError( QProcess::ProcessError perr )
{
#ifdef Q_OS_WIN
	if( mProcess ) {
		if( mState != StateDone && mState != StateCancelled ) {
			QString error = "Got Win32Process error: " + stringForProcessError( perr );
			QString detail = mProcess->errorString();
			if( !detail.isEmpty() )
				error += "\n" + detail;
			jobErrored( error );
		}
	} else
#endif
		JobBurner::slotProcessError( perr );
}

void OOPBurner::updateOutput(bool)
{}

