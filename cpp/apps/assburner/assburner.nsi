; DESCRIPTION: Assburner installer script
; (C) Blur Studio 2005

!include assburner-svnrev.nsi
!define MUI_PRODUCT "Assburner"
!define MUI_VERSION "v1.3.${MUI_SVNREV}"

Name "${MUI_PRODUCT} ${MUI_VERSION} ${PLATFORM}"

#!include "MUI.nsh"
!include "..\..\..\nsis\SoftwareINI.nsh"

; Name of resulting executable installer
!define OUT_FILE "ab_install_${MUI_SVNREV}_${PLATFORM}.exe"
OutFile "${OUT_FILE}"
InstallDir "C:\blur\assburner\"

Page directory
Page instfiles

#SilentInstall silent

Section "install"
	Processes::KillProcess "abpsmon.exe"
	Processes::KillProcess "assburner.exe"
	StrCmp $R0 "0" skipsleep
	Sleep 3000
	skipsleep:
	Delete $INSTDIR\*.dll
	SetOutPath $INSTDIR
	File assburner.exe
	File psmon\abpsmon.exe
	File assburner.ini
	File assburner_version.txt
	File runScriptJob.ms
	File runScriptJobDebug.ms
	File houdini_ifd_gen.py
	File mantra_render.py
	File Max2014_fix_perms.bat
	File SetACL.exe
;	File c:\mingw\bin\exchndl.dll
	File ..\..\lib\stone\stone.dll
	File ..\..\lib\stonegui\stonegui.dll
	File ..\..\lib\classes\classes.dll
	File ..\..\lib\qjson\qjson4.dll
	File ..\..\..\binaries\libpq.dll
	CreateShortCut "$DESKTOP\Assburner.lnk" "$INSTDIR\assburner.exe" ""
	CreateShortCut "$QUICKLAUNCH\Assburner.lnk" "$INSTDIR\assburner.exe" ""
	File ..\..\..\binaries\unzip.exe
	File ..\..\..\binaries\zip.exe
	File ..\..\..\binaries\unziplicense.txt
	File ..\..\..\binaries\Tail.exe
;	File c:\boost\lib\boost_thread-mgw-mt-1_32.dll
;	File c:\boost\lib\boost_filesystem-mgw-1_32.dll
;	File c:\boost\lib\boost_date_time-mgw-1_32.dll
	SetOutPath $INSTDIR\plugins
	File /r "plugins\*.*"
	; delete old assburner3 dir!!!
	RMDir /r "c:\max5\assburner3"
	; Delete everything from spool directory, then re-create it
	; RMDir /r "$INSTDIR\spool"
	CreateDirectory "$INSTDIR\spool"
	; Disable windows error reporting for assburner
	DeleteRegKey HKLM "SOFTWARE\Microsoft\PCHealth\ErrorReporting\ExclusionList\assburner.exe"
	WriteRegDWORD HKLM "SOFTWARE\Microsoft\PCHealth\ErrorReporting\ExclusionList" "assburner.exe" 1
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayName" "${MUI_PRODUCT} (remove only)"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "UninstallString" "$INSTDIR\uninstall.exe"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayVersion" "${MUI_VERSION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "Publisher" "Blur Studio"
	WriteUninstaller "$INSTDIR\uninstall.exe"
	# Update software.ini so we can track what software is installed on each system
	!insertmacro UpdateSettingsINI ${BLUR_SOFTWARE_INI} "Assburner" "${MUI_VERSION}" "${OUT_FILE}"
SectionEnd

Section "Uninstall"
	;Delete Files and directory
	RMDir /r "$INSTDIR\*.*"
	RMDir "$INSTDIR"
	
	Delete "$DESKTOP\Assburner.lnk"
	Delete "$QUICKLAUNCH\Assburner.lnk"
	
	;Delete Uninstaller And Unistall Registry Entries
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\${MUI_PRODUCT}"
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}"
SectionEnd

; No longer start assburner automatically
;Section
	;SetOutPath $INSTDIR
	;ExecShell "" "$INSTDIR\assburner.exe"	
;SectionEnd
