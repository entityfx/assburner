
include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/python.pri)

INCLUDEPATH+=../../../lib/stone/include

unix {
	INCLUDEPATH+=/usr/include/stone
}

LIBS+=-L../../../lib/stone -lstone

SOURCES += \
	psmon.cpp

win32:LIBS += -lpsapi -lMpr -lws2_32 -lgdi32

CONFIG += qt warn_on
QT-= gui
QT+= network

TARGET=abpsmon

win32 {
	debug {
		CONFIG += console
	}
}

DESTDIR=./

unix {
	target.path=/usr/bin/
	INSTALLS += target
}
macx:CONFIG-=app_bundle
