
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from blur.defaultdict import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, os

class MantraBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.RenderStarted = False
		self.MantraDir = None
		
	# Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList() << "mantra.exe"
	
	def getServiceDir(self,service,serviceRegEx):
		services = self.job().jobServices().services().filter('service',QRegExp(serviceRegEx))
		if len(services) == 0:
			raise "Unable to find %s service for job to determine render executable" % service
		if len(services) > 1:
			raise "Multiple %s services listed, unable to determine which %s installation to use" % (service,service)
		sw = services[0].software()
		if sw.isRecord():
			return sw.installedPath()
		return None
	
	def getMantraDir(self):
		if self.MantraDir is None:
			try:
				self.MantraDir = self.getServiceDir('Mantra','^Mantra')
			except: pass
			if self.MantraDir is None:
				self.MantraDir = self.getServiceDir('Houdini','^Houdini')
			if not self.MantraDir or not os.path.exists(self.MantraDir):
				raise Exception( "Could not find directory for Mantra" )
		return self.MantraDir

	def mantraExecutable(self):
		try:
			return self.getMantraDir() + '/bin/mantra.exe'
		except:
			traceback.print_exc()
			self.jobErrored( "Exception getting Mantra path: " + traceback.format_exc() )
	
	def executable(self):
		return 'python.exe'
	
	def buildCmdArgs(self):
		args = QStringList()
		args << 'mantra_render.py'
		args << self.mantraExecutable()
		args << self.job().fileName()
		tasks = self.assignedTasks()
		if '-' in tasks:
			(startFrame,endFrame) = str(self.assignedTasks()).split('-')
			args << startFrame << endFrame
			self.EndFrame = int(endFrame)
		else:
			# Single frame
			self.EndFrame = int(tasks)
			args << tasks << tasks
		return args
	
	def startProcess(self):
		JobBurner.startProcess(self)
		self.checkupTimer().start(1000) # Every Second

	def slotProcessOutputLine(self,line,channel):
		startMatch = re.match( r'Starting Frame (\d+).', line )
		doneMatch = re.match( r'Frame (\d+) complete.', line )
		errorMatch = re.match( r'Error: (.+)$', line )
		if startMatch:
			self.CurrentFrame = int(startMatch.group(1))
			self.taskStart(self.CurrentFrame)
			
		if doneMatch:
			frame = int(doneMatch.group(1))
			if frame != self.CurrentFrame:
				self.jobErrored('Got frame done message for frame {frameDone} when expecting {frameCurrent}'.format( frameDone=frame, frameCurrent=self.CurrentFrame ))
				return
			self.taskDone(frame)

		if errorMatch:
			self.jobErrored(line)
		
	def slotProcessExited(self):
		if self.CurrentFrame == self.EndFrame:
			self.jobFinished()
		else:
			self.jobErrored('Mantra exited unexpectedly')

class MantraBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return ['Mantra']

	def createBurner(self,jobAssignment,slave):
		return MantraBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( MantraBurnerPlugin() )

