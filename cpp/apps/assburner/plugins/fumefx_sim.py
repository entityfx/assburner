
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, os, sys, shutil

localDrive = 'D:/'

# W:/a/b/c.txt -> D:/W/a/b/c.txt
def toLocalPath(path,localDrive=localDrive):
	if len(path) < 2: return path
	return localDrive + path[0] + '/' + path[2:]

# D:/W/a/b/c.txt -> W:/a/b/c.txt
def toNetworkPath(path):
	if len(path) < 4: return path
	return path[3] + ':' + path[4:]

fumeDbSetup = False
def setupLocalFumeDb(dbFile=localDrive + '/fume_assburner.db'):
	global fumeDbSetup
	if fumeDbSetup: return
	fumeDbSetup = True
	schemaFile = os.path.join( os.path.dirname(os.path.abspath(__file__)),'fume_local.schema')
	schema = createPythonTypesFromSchema( schemaFile, sys.modules[__name__] )[0]
	if not schema:
		raise Exception("Failed to load fume local schema at: " + schemaFile)
	conn = Connection.create('QSQLITE')
	conn.setDatabaseName(dbFile)
	db = Database(schema, conn)
	if not conn.checkConnection():
		raise Exception( "Unable to connect to local fume database at: " + dbFile )
	db.createTables()
	Database.setCurrent(db,False)

def moveToVersion(filePath):
	i = 1
	base, ext = os.path.splitext(filePath)
	while os.path.exists(base + str(i) + ext):
		i += 1
	os.rename( filePath, base + str(i) + ext)

class FumeFXSimBurner(MaxScriptBurner):
	def __init__(self,jobAssignment,slave):
		MaxScriptBurner.__init__(self,jobAssignment,slave,JobBurner.OptionMergeStdError|JobBurner.OptionAllowUnassignedTaskUpdates)
		job = jobAssignment.job()
		self.FumeLogFilePath = job.fumeLogFilePath()
		self.FumeLogFilePos = 0
		self.NeedCleanupDelay = False
		self.LocalSim = bool(job.jobServices().filter( JobService.c.Service == Service.recordByName('FumeFX_SIM_Local') ).size())
		self.IsPostSim = len(self.FumeLogFilePath) == 0
		self.KeepLocal = None # Set when we parse the env variables

		# If this is not none then it will be a path without drive letter, extension, or frame number, such as
		# /Project/Cache/FileName_.
		# which can be used to match against the fumedata records in the local database, once their drive letter and ext are stripped
		self.DependentFilesPath = None
		if self.LocalSim:
			setupLocalFumeDb()
			self.FumeLogFilePath = toLocalPath(self.FumeLogFilePath)
			existing = (FumeData.c.FkeyJob == job.key()).select()
			fd = self.FumeData = existing and existing[0] or FumeData()
			fd.setLatestChange(QDateTime.currentDateTime())
			fd.setFkeyJob(job.key())
			fd.setOutputPath(job.outputPath())
		
		self.CustomEnv = self.splitEnvironment(job.environment())

		# List of (src,dst,isCache) tuples
		self.FailedCopies = []
		if self.maxVersion() == 2012:
			self.FumeConfigFilePath = Config.getString('assburnerFumeFxPathTemp','C:/Max2012_x64/3ds Max 2012/plugins/Afterworks/fumefx_3.50/FumeFX.ini')
		else:
			self.FumeConfigFilePath = self.maxDir() + '/plugins/AfterWorks/FumeFX/FumeFX.ini'

		self.UseFumeLog = not self.IsPostSim and Config.getBool('assburnerFumeFxUseLog',False)
		self.StartedFirstTask = False
		self.SimmedOneTask = False
		# Sometimes when opening the log file the size is reported as less than our current position.  Instead of erroring we'll just wait
		# and try again.  If it happens too many times in a row we abort the sim and report an error
		self.LogFileReadTries = 0
		self.MaxLogFileReadTries = 5

	def cleanupFumeData(self,fumeData):
		cleanedBytes = 0
		outputPath = toLocalPath(fumeData.outputPath())
		outputFileInfo = QFileInfo(outputPath)
		matchFile = outputFileInfo.baseName()
		fileLeft = False

		for fn in outputFileInfo.dir().entryList():
			fn = unicode(fn)
			# Starts with the base name and has at most 4 digits, a dot, and 3 or 4 char extension
			if fn.startswith(matchFile) and len(fn) - len(matchFile) <= 9:
				fi = QFileInfo(outputPath + '/' + fn)
				if QFile.remove(fi.filePath()):
					cleanedSize += fi.size()
				else:
					fileLeft = True

		if fileLeft:
			fumeData.setTotalSizeMb( fumeData.totalSizeMb() - (cleanedBytes / (1024 * 1024)) )
			fumeData.commit()
		else:
			fumeData.remove()

		return cleanedBytes / (1024 * 1024)

	def checkLocalDiskSpace(self,neededGigs = 500):
		freeMegs = availableDriveSpace(localDrive) / (1024 * 1024)
		toPurgeMegs = (neededGigs * 1024) - freeMegs
		preserveJobKeys = []
		while toPurgeMegs > 0:
			fumeDatas = (not_(FumeData.c.FkeyJob.in_(preserveJobKeys))).orderBy(FumeData.c.LatestChange,Expression.Ascending).limit(10).select()
			if len(fumeDatas) == 0:
				self.logError( self.job(), "Unable to cleanup local drive to desired free space: %iGb, No more fume data to delete.  Free space: %iGb" % (neededGigs, freeMegs/1024) )
				break
			for fd in fumeDatas:
				job = Job.table().record(fd.fkeyJob())
				# No matter what we aren't going to try to clean up a job more than once
				preserveJobKeys.append(fd.fkeyJob())
				if job.isRecord() and job.status() != 'deleted':
					continue
				# Don't delete files this job depends on
				if self.DependentFilesPath and unicode(fd.outputPath())[2:].startswith(self.DependentFilesPath):
					continue
				cleaned = self.cleanupFumeData(fd)
				toPurgeMegs -= cleaned
				freeMegs += cleaned
				if toPurgeMegs < 0:
					break

	def updateFumeLogSetting(self):
		ini = IniConfig(self.FumeConfigFilePath)
		ini.pushSection('General')
		if ini.readString('SaveSimLog') == 'Yes':
			self.logMessage( 'Fume Config Already has [General] SaveSimLog=Yes in ' + self.FumeConfigFilePath )
			return True
		ini.writeString('SaveSimLog','Yes')
		ini.popSection()
		if ini.writeToFile():
			self.logMessage( 'Fume Config Already has [General] SaveSimLog=Yes in ' + self.FumeConfigFilePath )
			return True
		self.jobErrored( 'Unable to save [General] SaveSimLog=Yes in Fume Config File at: ' + self.FumeConfigFilePath )
		return False

	# If network=False, then it'll return the path for a local sim
	def resumeFile(self,network=True):
		return unicode(self.job().outputPath()[:-3] + 'fdc')

	def customEnvironment(self):
		ret = MaxScriptBurner.customEnvironment(self)
		if self.LocalSim and self.CustomEnv:
			ret += self.CustomEnv
		return ret

	@staticmethod
	def splitEnvironmentKeyVal(env):
		return {unicode(k):unicode(v) for k,v in MaxScriptBurner.splitEnvironmentKeyVal(env).iteritems()}

	def clearLocalOutputFiles(self):
		outputPath = toLocalPath(self.job().outputPath())
		outputFileInfo = QFileInfo(outputPath)
		matchFile = outputFileInfo.baseName()
		outputDir = outputFileInfo.dir().path()
		for fn in outputFileInfo.dir().entryList():
			fn = unicode(fn)
			# Starts with the base name and has at most 4 digits, a dot, and 3 or 4 char extension
			if fn.startswith(matchFile) and len(fn) - len(matchFile) <= 9:
				fi = QFileInfo(outputDir + '/' + fn)
				if not QFile.remove(fi.filePath()):
					self.jobErrored( "Unable to remove existing fume file: " + fi.filePath() )
					return False
		return True

	# Returns True if the space check has already been done
	def checkLocalDependent(self):

		env = self.splitEnvironmentKeyVal(self.CustomEnv)

		self.KeepLocal = ('FUME_KEEP_LOCAL' in env) or JobDep.recordsByDep(self.job())

		if not 'FUME_DEPENDENT_PATH' in env:
			self.logMessage( 'No FUME_DEPENDENT_PATH environment variable' )
			return False

		fdp = env['FUME_DEPENDENT_PATH']
		depSize = 0
		path, fileName = os.path.split(fdp)
		# Full path minus drive letter and colon
		self.DependentFilesPath = fdp[2:]
		fnRE = re.compile(r'^%s\d{4}\.%s' % (fileName[:-1],env['FUME_FILETYPE'][1:]))
		for fn in os.listdir(path):
			if fnRE.match(fn):
				src = os.path.join(path,fn)
				dst = toLocalPath(src)
				src_st = os.stat(src)
				try:
					dst_st = os.stat(dst)
				except:
					dst_st = None
				depSize += src_st.size - (dst_st or 0)

		if self.checkLocalDiskSpace(depSize + 500):
			self.logMessage( "Attempting to copy dependent files local" )
			needSpaceCheck = False
			try:
				copiedSize = 0
				for fn in os.listdir(path):
					if fnRE.match(fn):
						src = os.path.join(path,fn)
						dst = toLocalPath(src)
						src_st = os.stat(src)
						try:
							dst_st = os.stat(dst)
						except:
							dst_st = None
						if dst_st is None or src_st.size != dst_st.size or src_st.mtime != dst_st.mtime:
							if shutil.copyfile(src,dst):
								copiedSize += src_st.size - (dst_st and dst_st.size or 0)

				existing = (FumeData.c.OutputPath == (fdp + 'png')).select()
				fd = existing and existing[0] or FumeData()
				fd.setLatestChange(QDateTime.currentDateTime())
				fd.setFkeyJob(self.job().key())
				fd.setOutputPath(fdp + 'png')
				fd.setTotalSizeMb(fd.totalSizeMb() + (copiedSize/(1024*1024)))
				fd.commit()
				self.logMessage( "Successfully copied %i MB of dependent files to local drive in path: %s" % (copiedSize/(1024*1024), path))
				self.logMessage( "Setting FUME_DEPENDENT_PATH env variable to: " + toLocalPath(fdp) )
				env['FUME_DEPENDENT_PATH'] = toLocalPath(fdp)
				self.CustomEnv = ['%s=%s' % (k,v) for k,v in env.iteritems()]
			except IOError:
				pass
		else:
			self.logMessage( "Not copying dependent files local, not enough space!" )
			return False
		return True

	def startProcess(self):
		if self.UseFumeLog:
			if not self.updateFumeLogSetting():
				return

			logFileInfo = QFileInfo(self.FumeLogFilePath)
			if self.LocalSim and not QDir( logFileInfo.absolutePath() ).exists():
				if not QDir().mkpath(logFileInfo.absolutePath()):
					self.jobErrored( 'Unable to create local output path: ' + logFileInfo.absolutePath() )
					return

			if logFileInfo.exists() and not QFile.rename( self.FumeLogFilePath, '%s.%s.txt' % (self.FumeLogFilePath[:-4], QDateTime.currentDateTime().toString( "yyyy-MM-ddThh.mm.ss")) ):
				self.logMessage( 'Unable to move away existing log file, setting FumeLogFilePos to the end of the file instead' )
				self.FumeLogFilePos = logFileInfo.size()
			self.logMessage( 'Fume log file being read from: ' + self.FumeLogFilePath )

		if self.LocalSim:
			if not self.clearLocalOutputFiles():
				return

			needSpaceCheck = self.checkLocalDependent()

			if needSpaceCheck:
				self.checkLocalDiskSpace()

			if not self.IsPostSim:
				resumeNetwork = self.resumeFile()
				resumeLocal = toLocalPath(resumeNetwork)
				if os.path.exists(resumeNetwork) and (not os.path.exists(resumeLocal) or os.path.getsize(resumeNetwork) != os.path.getsize(resumeLocal)):
					shutil.copyfile(resumeNetwork,resumeLocal)

		MaxScriptBurner.startProcess(self)

	def cleanupStage(self,stage):
		if stage == JobBurner.CleanupCopy and self.NeedCleanupDelay:
			self.NeedCleanupDelay = False
			status = self.job().jobStatus()
			# If possible compute a reasonable time to allow for fume to finish pausing
			if status.tasksDone() > 0 and status.tasksAverageTime() > 0:
				# tasksAverageTime is in seconds(legacy)
				delay = (Interval(status.tasksAverageTime()) * 1.25 + Interval('10 minutes')).toString()
			else:
				delay = '2 hours'
			self.logMessage( 'Delaying cleanup until Fume has finished pausing, maximum ' + delay )
			return Interval.fromString(delay)[0].seconds() * 1000
		elif stage == JobBurner.CleanupOutput:
			for src,dest,fileType in self.FailedCopies:
				self.syncFile(src,dest,fileType,'During Cleanup: ')
			self.FailedCopies = []

			if self.LocalSim and not self.IsPostSim:
				resumeNetwork = self.resumeFile()
				resumeLocal = toLocalPath(resumeNetwork)
				if os.path.exists(resumeLocal):
					# Copy resume file if job didn't completely finish for whatever reason
					if self.job().reload().status() != 'done':
						try:
							if os.path.exists(resumeNetwork):
								moveToVersion(resumeNetwork)
						except WindowsError as we:
							if we.winerror == 32: # File being used by another process, wait a minute and try again
								return 60*1000
						shutil.copyfile(resumeLocal, resumeNetwork)
						self.FumeData.setTotalSizeMb( self.FumeData.totalSizeMb() + os.path.getsize(resumeLocal) / (1024*1024) )
						self.FumeData.commit()
					else:
						QFile.remove(resumeLocal)
		return MaxScriptBurner.cleanupStage(self,stage)

	def cancel(self):
		# If we haven't started or there has been an error then we don't worry about hitting the pause button
		self.job().reload()
		if self.state() == JobBurner.StateStarted and self.job().status() == 'suspended':
			# If the job gets suspended we need to simulate pressing the Pause button
			self.logMessage('Burn was cancelled, attempting to pause the sim by simulating a mouse click on the pause button')
			try:
				from win32gui import SetForegroundWindow, ClientToScreen
				hwndList = getWindowsByName( "FumeFX:" )
				for hwnd in hwndList:
					SetForegroundWindow(hwnd)
					self.logMessage('Found the "FumeFX:" window and set it to the foreground. Sending mouse click to the pause button.')
					from blur.win32sendinput import simulateMouseMove, simulateMouseClick
					# Move mouse relative to fume window
					x, y = ClientToScreen(hwnd, (190, 435))
					# Simulate mouse click at the right position
					simulateMouseMove( x, y, relative = False )
					simulateMouseClick()
					self.NeedCleanupDelay = True
			except:
				self.logMessage('Failed to find the "FumeFX Dynamics: Status" window, unable to pause the sim')
		MaxScriptBurner.cancel(self)

	def checkup(self):
		# Check for error dialogs, such as Error in ntdll.dll
		pid = self.processId()
		if pid:
			match, windowTitle = findMatchingWindow( pid, ['error'], True, False ) # (processId, matching text list, child processes = True, case sensitive = False)
			if match:
				self.jobErrored( "Found window: " + windowTitle + ", cancelling the burn" )
				return False

		# This code is used to detect the dialog that asks if the simulation should be resumed.
		# An exception is thrown(and silently discarded) if the window is not found
		try:
			import win32ui
			hwnd = win32ui.FindWindow(None, "FumeFX")
			hwnd.SetForegroundWindow()
			self.logMessage('Found the "FumeFX" window and set it to the foreground. Sending Enter key to resume the sim.')
			from blur.win32sendinput import simulateKeyPress
			# Sends an enter key stroke by default
			simulateKeyPress()
		except: pass

		if self.UseFumeLog:
			self.feedLogToProcessOutput()

		return MaxScriptBurner.checkup(self)

	def feedLogToProcessOutput(self):
		# Because of some strange interaction between windows network paths
		# and QFile, we have to open and seek to the current position in the
		# log file each time we want to check if there is more data to read,
		# otherwise QFile sometimes gets stuck where .atEnd continually returns
		# True even though there is more data in the file to read
		fumeLogFile = QFile( self.FumeLogFilePath )
		if not fumeLogFile.open( QFile.ReadOnly ):
			Log( "FumeFXSimBurner.feedLogToProcessOutput: Log file missing" )
			# Log file not created yet
			return
		if fumeLogFile.size() < self.FumeLogFilePos:
			self.LogFileReadTries += 1
			self.logMessage( 'The Fume log file has been unexpectedly truncated.  Expected size >= %i, got size %i' % (self.FumeLogFilePos,fumeLogFile.size()) )
			if self.LogFileReadTries > self.MaxLogFileReadTries:
				self.jobErrored( 'The fume log file continues to be truncated, aborting' )
			return
		if not fumeLogFile.seek(self.FumeLogFilePos):
			self.jobErrored( 'Seek to current position in the fume log file failed' )
			return
		# Reset the bad size/position counter
		self.LogFileReadTries = 0
		while not fumeLogFile.atEnd():
			txt = fumeLogFile.readAll()
			self.FumeLogFilePos = fumeLogFile.pos()
			self.slotProcessOutput( txt, QProcess.StandardOutput )

	def syncFile(self,src,dest,fileType,prefix=''):
		if QFile.exists( src ):
			if QFile.exists(dest) and not QFile.remove(dest):
				self.logError( self.job(), '%sUnable to remove existing %s file before copying the new one: %s' % (prefix,fileType,dest) )
				self.FailedCopies.append( (src,dest,fileType) )
			elif not QFile.copy( src, dest ):
				self.FailedCopies.append( (src,dest,fileType) )
				self.logError( self.job(), '%sUnable to copy %s file to the network drive %s from %s' % (prefix,fileType,dest,src) )
			elif not self.KeepLocal:
				QFile.remove(src)
		elif fileType != 'preview':
			self.logError( self.job(), 'Expected %s file not found at: %s' % (fileType, src) )

	def syncPostSimFiles(self):
		env = self.splitEnvironmentKeyVal(self.CustomEnv)
		filetype = env.get('FUME_FILETYPE','fxd')
		localOutputPath = toLocalPath(self.job().outputPath())
		lopfi = QFileInfo(localOutputPath)
		baseName = lopfi.baseName()
		for fn in lopfi.dir().entryList():
			fn = unicode(fn)
			if fn.startswith(baseName) and fn.endswith(filetype):
				cacheSrc = lopfi.path() + '/' + fn
				cacheDst = toNetworkPath(cacheSrc)
				self.syncFile(cacheSrc,cacheDst,'cache')

	def processMaxScriptMessage(self,line):
		# Post sims do not respond to fume log output, so we just let the normal
		# Maxscript process handle things. We do need to sync though when all is done,
		# if the job is running on a local drive.
		if self.IsPostSim:
			if line.startsWith('starting'):
				self.taskStart(self.assignedTaskList()[0].frameNumber())
			elif line.startsWith('success'):
				if self.LocalSim:
					self.syncPostSimFiles()
				self.taskDone(self.currentTasks()[0].frameNumber())
		else:
			# We handle taskStart/taskDone below by reading the fume log file, so ignore this
			if line.startsWith('starting') or line.startsWith('finished'):
				return True
			if line.startsWith('success') and not self.StartedFirstTask:
				self.jobErrored('Fume exited without completing any tasks')
				return False
		return MaxScriptBurner.processMaxScriptMessage(self,line)

	# For local sims, we need to set dependent jobs that are also local
	# to run on the same host.
	def jobFinished(self):
		if self.LocalSim:
			for dep in JobDep.recordsByDep(self.job()).jobs():
				if dep.jobServices().filter( JobService.c.Service == Service.recordByName('FumeFX_SIM_Local') ).size():
					dep.setHostList( Host.currentHost().name() ).commit()
		MaxScriptBurner.jobFinished(self)

	def taskDone( self, frameNumber ):
		if self.LocalSim and not self.IsPostSim:
			env = self.splitEnvironmentKeyVal(self.CustomEnv)
			filetype = env.get('FUME_FILETYPE','fxd').strip('.')
			previewDst = makeFramePath(self.job().outputPath(),frameNumber)
			previewSrc = toLocalPath(previewDst)
			prvFileInfo = QFileInfo(previewSrc)
			cacheDst = previewDst[:-prvFileInfo.suffix().size()] + filetype
			cacheSrc = toLocalPath(cacheDst)
			cacheFileInfo = QFileInfo(cacheSrc)
			self.FumeData.setTotalSizeMb(self.FumeData.totalSizeMb() + (prvFileInfo.size() + cacheFileInfo.size()) / (1024*1024) )
			self.FumeData.setLatestChange(QDateTime.currentDateTime()).commit()
			# Copy Cache File
			self.syncFile( cacheSrc, cacheDst, 'cache' )
			self.syncFile( previewSrc, previewDst, 'preview' )
			self.checkLocalDiskSpace()
			if self.KeepLocal:
				size = os.path.getsize(cacheSrc)
				if os.path.exists(previewSrc):
					size = os.path.getsize(previewSrc)
				self.FumeData.setTotalSizeMb( self.FumeData.totalSizeMb() + size / (1024 * 1024) )
				self.FumeData.commit()

		self.SimmedOneTask = True
		MaxScriptBurner.taskDone(self,frameNumber)

	def slotProcessOutputLine(self,line,channel):
		Log( "FumeFXSimBurner.slotReadOutput() called, ready to read output" )
		error = False

		# Job started
		if re.match( 'Starting simulation', line ):
			self.RenderStarted = True
			# Mark the first frame as started immediately so that the task time is accounted for
			#self.taskStart(self.AssignedTasks[0])

		# Frames Rendered
		frameStart = re.match( r'^Frame:\s(-?\d+)', line )
		frameDone = re.match( r'^Frame time:\s(\d+\:\d\d\.\d\d)', line )

		if frameStart:
			frame = int(frameStart.group(1))
			self.taskStart(frame)

			# Fume jobs may end up starting in the middle of the task list
			# automatically cancel previous tasks
			if not self.StartedFirstTask:
				JobTask.select( (JobTask.c.Job == self.job()) & (JobTask.c.FrameNumber < frame) & JobTask.c.Status.in_(['new','assigned']) ).setHosts(Host()).setStatuses('cancelled').commit()
				self.StartedFirstTask = True

		if frameDone:
			curTasks = self.currentTasks()
			if curTasks:
				self.taskDone(curTasks[0].frameNumber())

		# Job completion
		if re.match( '^Simulation Time:', line ) or re.match( '^Simulation Paused', line ) or re.match( '^Simulation ended', line ):
			self.jobFinished()
			return

		if error:
			self.jobErrored( line )

class FumeFXSimBurnerPlugin(JobBurnerPlugin):
	def __init__(self):
		JobBurnerPlugin.__init__(self)

	def jobTypes(self):
		return QStringList('FumeFXSim')

	def createBurner(self,jobAssignment,slave):
		Log( "FumeFXSimBurnerPlugin.createBurner() called, Creating FumeFXSimBurner" )
		return FumeFXSimBurner(jobAssignment,slave)


plugin = FumeFXSimBurnerPlugin()
JobBurnerFactory.registerPlugin( plugin )
