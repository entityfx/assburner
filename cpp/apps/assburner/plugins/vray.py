
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from blur.defaultdict import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, sys

# Returns a string list of args
def vrimgConvertArgs( inputFile, frameNumber, outputDescJson ):
	for v in outputDescJson.array():
		channelDesc = v.toObject()
		outputPath = channelDesc['path'].toString()
		bitDepth = channelDesc['bitDepth'].toString()
		channel = channelDesc['channel'].toString()

		args = []
		args.append( inputFile )
		args.append( makeFramePath(outputPath,frameNumber) )
		args += ['-channel',channel]
		if bitDepth == '16':
			args.append('-half')
		
		p = QProcess

class VrayBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.VrayDir = None
		self.vrimgOutputDesc = jobAssignment.job().vrimgOutputDesc()

        # Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList() << "vray.exe"
	
	def getVrayDir(self):
		if self.VrayDir is None:
			
			vrayServices = self.job().jobServices().services().filter('service',QRegExp('^Vray'))
			if len(fusionServices) == 0:
				raise "Unable to find vray service for job to determine render executable"
			if len(fusionServices) > 1:
				raise "Multiple vray services listed, unable to determine which fusion installation to use"
			
			vrayService = vrayServices[0]
			
			sw = vrayService.software()
			if sw.isRecord():
				self.VrayDir = sw.installedPath()
			else:
				raise ("Vray service %s has no attached software record" % vrayService.debug())
			
			if self.VrayDir.isEmpty():
				raise ("Empty or Invalid Fusion Path listed under config key %s" % configKey)
		
		return self.VrayDir
	
	def executable(self):
		if sys.platform == 'win32':
			return 'vray.exe'
		return 'vray'
		
	def buildCmdArgs(self):
		args = QStringList()
		args << "-sceneFile=" + self.burnFile()
		args << "-autoClose=1"
		args << "-rtEngine=%i" % self.job().renderEngine()
		args << "-frames=" + self.assignedTasks.replace(',',';')
		return args
	
	def startProcess(self):
		JobBurner.startProcess(self)
		self.checkupTimer().start(1000) # Every Second

	def slotProcessOutputLine(self,line,channel):
		JobBurner.slotProcessOutputLine(self,line,channel)
                
                if 'error: Cannot create output image file' in line:
                    self.jobErrored(line)
                    return

		if False:
			self.taskDone(frame)
			if self.vrimgOutputDesc:
				vrimgConvert(outputFrame, self.vrimgOutputDesc)
		
		if False:
			self.jobFinished()
			

class VrayBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return QStringList('Vray')

	def createBurner(self,jobAssignment,slave):
		Log( "VrayBurnerPlugin.createBurner() called, Creating VrayBurner" )
		return VrayBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( VrayBurnerPlugin() )
