from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, sys

class Cinema4DBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.CurrentFrame = None
		self.Cinema4DDir = self.Cinema4DExe = ''
		self.Software = None
		self.PreR13 = False
		self.NeedCleanupDelay = True
		
		# Pre R13 variables needed for detecting frames
		self.FileServerClockSkew = 0
		self.RenderStarted = False
		self.JobDone = False
		
	def executable(self):
		return self.workingDirectory() + self.Cinema4DExe
	
	def software(self):
		if self.Software is None:
			c4dservices = self.job().jobServices().services().filter('service',QRegExp('cinema4d'))
			if c4dservices.size() != 1:
				self.jobErrored( 'Unable to find single c4d service for job to determine the correct cinema4d version to run' )
				return ''
			
			c4dservice = c4dservices[0]
			sw = c4dservice.software()
			if sw.isRecord():
				self.Software = sw
			else:
				self.jobErrored( "Cinema 4D service %s has no associated software record" % c4dservice.service() )
		return self.Software
	
	def workingDirectory(self):
		if not self.Cinema4DDir:
			sw = self.software()
			if sw is not None:
				self.Cinema4DDir = sw.installedPath()
				self.Cinema4DExe = sw.executable()
		return self.Cinema4DDir
	
	def buildCmdArgs(self):
		args = QStringList()

		if self.software().version() < 13:
			self.PreR13 = True
		
			# Hack for how c4d handles output paths, S:/frame_02.tif will output S:/frame_02_0001.tif, S:/frame_02_0002.tif, etc
			opfi = QFileInfo(self.job().outputPath())
			op = opfi.completeBaseName()
			if op.size() and op.at(op.size()-1).isDigit():
				op += '_'
			self.OutputPath = opfi.path() + QDir.separator() + op + "." + opfi.suffix()
		
		if not QFileInfo(self.workingDirectory()).isDir():
			self.jobErrored( "Cinema4d dir does not exist at: " + self.workingDirectory() )
			return args
		self.logMessage ("Cinema4d dir exists at %s " % self.workingDirectory())
		
		args << '-nogui'
		args << '-render'
		args << self.burnFile().replace('/','\\')
		args << '-frame'
		if self.job().videoMode():
			args << str(self.job().videoStartFrame()) << str(self.job().videoEndFrame())
			self.StartFrame = self.job().videoStartFrame()
			self.FrameCount = self.job().videoEndFrame() - self.StartFrame + 1
		else:
			try:
				frameList = expandNumberList( self.assignedTasks() )[0]
				self.StartFrame = frameList[0]
				self.EndFrame = frameList[-1]
				args << str(self.StartFrame) << str(self.EndFrame)
			except:
				self.jobErrored('Unable to parse frame list: ' + str(self.assignedTasks()) )
		
		if self.PreR13:
			self.calculateClockSkew()

		return args
		
	# Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList() << self.Cinema4DExe
	
	def startProcess(self):
		Log( "Cinema4DBurner::startBurn() called" )
		JobBurner.startProcess(self)
		self.checkupTimer().start(1000)
		Log( "Cinema4DBurner::startBurn() done" )
	
	def cleanupStage(self,stage):
		Log( "Cinema4DBurner.cleanupStage() called" )
		if stage == JobBurner.CleanupCopy and self.NeedCleanupDelay:
			self.NeedCleanupDelay = False
			delay = '10 seconds'
			self.logMessage( "Cinema4DBurner.cleaupStage() waiting " + delay )
			return Interval.fromString(delay)[0].asOrder(Interval.Milliseconds)
		ret = JobBurner.cleanupStage(self,stage)
		if stage == JobBurner.CleanupKillProcess:
			if self.Cinema4DExe:
				killAll(self.Cinema4DExe)
		return ret
		
	def checkup(self):
		Log( "Cinema4DBurner::checkup() called" )
		if self.PreR13:
			ret = self.preR13Checkup()
			if ret is not None:
				return ret
		return JobBurner.checkup(self)
	
	def slotProcessStarted(self):
		Log( "Cinema4DBurner::slotStarted" )
		#self.injectUnbufferedOutputDll()
		if self.PreR13:
			self.startFrame( self.StartFrame )
	
	def slotProcessExited(self):
		if self.PreR13:
			if not self.JobDone:
				self.checkup()
			if self.JobDone:
				self.jobFinished()
				return
		JobBurner.slotProcessExited(self)

	def slotProcessOutputLine(self,line,channel):
		Log( "Cinema4dBurner.slotReadOutput() called, ready to read output" )
		
		if re.search( 'Enter Registration Data', line ):
			self.jobErrored( 'Cinema4d Spewing registration requests, needs registered on this host' )
			return
			
		mo = re.search( 'Rendering frame (\\d+) at <(.+)>', line )
		if mo:
			frame = int(mo.group(1))
			startTimeString = mo.group(2)
			startTime = QDateTime.fromString(startTimeString, 'ddd MMM dd hh:mm:ss yyyy')
			if self.job().videoMode():
				if self.taskStarted():
					self.CurrentFrame = frame
					progress = (self.CurrentFrame - self.StartFrame) / self.FrameCount
					self.currentTasks().setProgresses( int( 100.0 * progress ) ).commit()
				else:
					self.taskStart( 1 )
			else:
				if self.CurrentFrame is not None:
					self.taskDone( self.CurrentFrame, startTime )
				self.CurrentFrame = frame
				self.taskStart( self.CurrentFrame, QString(), startTime )

		mo = re.search( 'Rendering successful', line )
		if mo:
			if self.job().videoMode():
				self.currentTasks().setProgresses(100).commit()
				self.taskDone(1)
			else:
				self.taskDone(self.CurrentFrame)
			self.jobFinished()
		
		JobBurner.slotProcessOutputLine(self,line,channel)

	# Pre R13 Code for detecting frame output files

	def calculateClockSkew(self):
		try:
			# Determine the file server clock skew
			tasks = JobTask.select( "fkeyjob=? AND jobtask=? LIMIT 1", [QVariant(self.job().key()),QVariant(self.StartFrame)] )
			if tasks.size() == 1:
				path = QFileInfo(self.taskPath( tasks[0] )).path() + '/clock_skew_test.txt'
				csf = QFile(path)
				if not csf.open( QIODevice.WriteOnly ):
					self.jobErrored('Unable to open clock skew test file at %s' % path)
					return args
				csf.write('clock skew test')
				csf.close()
				self.FileServerClockSkew = QDateTime.currentDateTime().secsTo( QFileInfo(path).lastModified() )
				self.logMessage('File server clock skew set to %i' % self.FileServerClockSkew)
				QFile.remove(path)
			else:
				self.jobErrored('Unable to find a task with frameNumber %i to determine fileserver clock skew' % self.StartFrame)
				return args
		except:
			self.jobErrored('Exception occured while trying to determine file server clock skew\n%s'%traceback.format_exc())

	def startTimeUnskewed(self):
		return self.startTime().addSecs( self.FileServerClockSkew )

	def taskPath(self,task):
		path = None
		if task.jobOutput().isRecord() and RangeFileTracker(task.jobOutput().fileTracker()).isRecord():
			path = task.jobOutput().fileTracker().filePath(task.frameNumber())
		else:
			path = makeFramePath( self.OutputPath, task.frameNumber() )
		return path

	def startFrame(self,frameNumber):
		if not self.taskStart(frameNumber):
			return False
		self.CurrentFrame = frameNumber
		self.OutputPaths = {}
		for task in self.currentTasks():
			# RangeFileTracker::filePath( int frameNumber ), returns full frame path
			path = self.taskPath(task)
			if path is not None:
				self.logMessage( 'Adding path to check for current frame output ' + path )
				self.OutputPaths[path] = False
			else:
				self.jobErrored( 'Unable to get valid output path for frame %i, ID: %i' % (task.frameNumber(), task.key()) )
				return False
		return True

	def preR13Checkup(self):
		if self.JobDone: return True
		while self.CurrentFrame is not None:
			allFramesDone = True
			for path, checked in self.OutputPaths.iteritems():
				if not checked:
					self.logMessage( "Checking Path: " + path )
					allFramesDone = False
					fi = QFileInfo( path )
					if not fi.exists():
						self.logMessage( "File not found" )
						break
					if not fi.isFile():
						self.logMessage( "Path does not point to a file" )
						break
					if not Path.checkFileFree( path ):
						self.logMessage( "File is not free" )
						break
					if not (fi.lastModified() > self.startTimeUnskewed()):
						self.logMessage( "File is old, last modified: %s" % fi.lastModified().toString() )
						break
					# Mark this frame as finished
					allFramesDone = True
					self.OutputPaths[path] = True
					self.logMessage( 'Frame Output Finished: ' + path )
			
			if allFramesDone:
				self.logMessage( 'All outputs finished for frame: ' + str(self.CurrentFrame) )
				self.taskDone( self.CurrentFrame )
				if self.CurrentFrame == self.EndFrame:
					# Give 5 seconds for the object buffer passes to be written since we dont
					# know about them or check for them yet
					self.JobDone = True
					QTimer.singleShot( 30000, self, SLOT('jobFinished()') )
					return True
				if not self.startFrame( self.CurrentFrame + 1 ):
					return False
			else:
				break
		return None

class Cinema4DBurnerPlugin(JobBurnerPlugin):
	def __init__(self):
		JobBurnerPlugin.__init__(self)
		if sys.platform == 'win32':
			disableWindowsErrorReporting( 'cinema4d.exe' )
		
	def jobTypes(self):
		return QStringList('Cinema4D') << 'Cinema4D R11'

	def createBurner(self,jobAssignment,slave):
		Log( "Cinema4DBurnerPlugin::createBurner() called, Creating Cinema4DBurner" )
		return Cinema4DBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( Cinema4DBurnerPlugin() )
