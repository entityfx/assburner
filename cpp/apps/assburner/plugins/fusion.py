from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from blur.defaultdict import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, sys

class FusionBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.RenderStarted = False
		self.FusionDir = None
		self.UseRenderSlave = Config.getBool( 'assburnerFusionUseRenderSlave', False )
		self.OutputsReportedByFrame = DefaultDict(0)
		self.OutputCount = self.job().outputCount()
		# Only when we get a Render Completed message do we set this to a larger value
		# This value will give a 5 second delay if the burn is canceled, usually enough time
		# for any finished frame to write out, but short enough to hopefully avoid a write
		# conflict on subsequent frames
		self.CleanupDelayCount = 1
		
	# Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList() << "fusion.exe"
	
	def getFusionDir(self):
		if self.FusionDir is None:
			
			fusionServices = self.job().jobServices().services().filter('service',QRegExp('^Fusion'))
			if len(fusionServices) == 0:
				raise "Unable to find fusion service for job to determine render executable"
			if len(fusionServices) > 1:
				raise "Multiple fusion services listed, unable to determine which fusion installation to use"
			
			fusionService = fusionServices[0]
			
			sw = fusionService.software()
			if sw.isRecord():
				self.FusionDir = sw.installedPath()
			else:
				configKey = 'assburner%sRenderDir' % fusionService.service()
				self.FusionDir = Config.getString( configKey, "" )
			
			if self.FusionDir.isEmpty():
				raise ("Empty or Invalid Fusion Path listed under config key %s" % configKey)
		
		return self.FusionDir
	
	def environment(self):
		# Set the license server environment variable if it exists in the database
		fusionLicenseServer = Config.getString('fusionLicenseServer')
		if not fusionLicenseServer.isEmpty():
			# Remove existing license server env var, if it exists
			envs = QProcess.systemEnvironment().filter( '^(?!EYEON_LICENSE_FILE=)' )
			# Add new one
			envs << ('EYEON_LICENSE_FILE=@' + fusionLicenseServer)
			return envs
		return QStringList()
	
	def executable(self):
		try:
			exe = "ConsoleSlave.exe"
			if self.UseRenderSlave:
				exe = "RenderSlave.exe"
			return self.getFusionDir() + exe
		except:
			traceback.print_exc()
			self.jobErrored( "Unknown exception getting fusion path: " + traceback.format_exc() )
		
	def buildCmdArgs(self):
		args = QStringList()
		args << self.burnFile()
		if self.UseRenderSlave:
			args << "/log" << self.getLogFilePath() << "/cleanlog"
			args << "/verbose" << "/quit"
#		args << "/status"
		(self.AssignedTasks, valid) = expandNumberList(self.assignedTasks())
		if not valid:
			self.jobErrored( 'Invalid task list: ' + self.assignedTasks() )
		if not self.job().allFramesAsSingleTask():
			args << "/frames" << self.assignedTasks().replace("-","..")
		return args
	
	def getLogFilePath(self):
		self.FusionLogPath = self.burnDir() + "fusion_log.txt"
		self.FusionLogFile = None
		return self.FusionLogPath
	
	def startProcess(self):
		Log( "FusionBurner::startBurn() called" )
		JobBurner.startProcess(self)
		self.checkupTimer().start(1000) # Every Second
		Log( "FusionBurner::startBurn() done" )

	def cleanupStage(self,stage):
		# Delay up to 5 * 20 seconds
		if stage == JobBurner.CleanupStopProcess and self.CleanupDelayCount > 0 and self.process() and self.process().state() != QProcess.NotRunning:
			self.CleanupDelayCount -= 1
			return 5 * 1000 # 5 seconds
		if stage == JobBurner.CleanupStopProcess:
			killAllWindows( 'Error in ntdll.dll' )
		return JobBurner.cleanupStage(self,stage)

	def feedLogToProcessOutput(self):
		Log( "FusionBurner::feedLogToProcessOutput" )
		if self.FusionLogFile is None:
			self.FusionLogFile = QFile( self.FusionLogPath )
			if not self.FusionLogFile.open( QFile.ReadOnly ):
				Log( "FusionBurner::feedLogToProcessOutput: Log file missing" )
				self.FusionLogFile = None
				# Log file not created yet
				return
		while self.FusionLogFile.canReadLine():
			self.slotProcessOutput( self.FusionLogFile.readLine(), QProcess.StandardOutput )
	
	def checkup(self):
		# Check for error dialogs, such as Error in ntdll.dll
		pid = self.processId()
		if pid:
			match, windowTitle = findMatchingWindow( pid, ['error'], True, False ) # (processId, matching text list, child processes = True, case sensitive = False)
			if match:
				self.jobErrored( "Found window: " + windowTitle + ", cancelling the burn" )
				return False

		if self.UseRenderSlave:
			self.feedLogToProcessOutput()
		
		return JobBurner.checkup(self)

	def slotProcessOutputLine(self,line,channel):
		Log( "FusionBurner::slotReadOutput() called, ready to read output" )
		error = False
		
		# Job started
		if re.match( 'Render started', line ) or re.match( 'Rendering Comp, frames', line ):
			self.RenderStarted = True
			# Mark the first frame as started immediately so that the task time is accounted for
			self.taskStart(self.AssignedTasks[0])
			
		# Frames Rendered
		mo = re.match( r'^Rendered frame (\d+) \(\d+\sof\s\d+\), took ([\d\.]+)', line )
		if mo and not self.job().allFramesAsSingleTask():
			frame = int(mo.group(1))
			taskTime = int(round(float(mo.group(2))))
			
			self.OutputsReportedByFrame[frame] += 1
			if self.OutputsReportedByFrame[frame] == 1:
				
				# If we havent marked the previous task as done, do so now.  
				# This happens if not all outputs have the full render range.
				if self.currentTasks().size() and self.currentTasks()[0].frameNumber() != frame:
					self.taskDone(self.currentTasks()[0].frameNumber())
					self.taskStart(frame, '', taskTime)
			
			if self.OutputsReportedByFrame[frame] >= self.OutputCount:
				self.taskDone(frame)
				try:
					self.taskStart(self.AssignedTasks[self.AssignedTasks.index(frame)+1])
				except: pass
			return
		
		# Job completion
		if re.match( '^Render completed', line ):
			if self.job().allFramesAsSingleTask():
				self.taskDone(self.AssignedTasks[0])
			# If we havent marked the previous task as done, do so now.  
			# This happens if not all outputs have the full render range.
			elif self.currentTasks().size():
				self.taskDone(self.currentTasks()[0].frameNumber())
			# When we finish a job we give 20 * 5 seconds for fusion to quit, in order to ensure
			# the frames have all been written
			self.CleanupDelayCount = 20
			self.jobFinished()
			return
		
		# This is a special error that we only report once per job
		if re.search( 'cannot get .+ at time', line ):
			# We won't be getting any 'Rendered frame' messages from this failed saver
			# so we can decrement the output count that is expected per frame
			#self.OutputCount -= 1
			#if not self.job().reportedSaverError():
			#	JobBurner.logError( self.job(), 'Please fix your bad saver: ' + line )
			#	self.job().setReportedSaverError( True ).commit()
			error = True
		
		# Ignore failed to load errors as a hold previous setup
		# will produce them but still render fine
		#if re.search( 'failed to (load|write)', line ):
		#	error = True
		if re.search( 'failed to write', line ):
			error = True
		if re.search( 'failed at time', line ):
			error = True
		if re.search( 'Render failed at', line ):
			error = True
		if re.search( 'License Error:', line ):
			error = True
		if re.search( 'ERROR: Comp failed to load.', line ):
			error = True
		
		# Hmm, gives this if invalid file name, i wonder about other errors...
		# Also gives this during normal render, but gives the render started
		# output first
		if re.match( '^Listening for commands...', line ):
			if not self.RenderStarted:
				self.jobErrored( "Unknown Error During Fusion Rendering\n" )
				return
		
		if error:
			self.jobErrored( line )

from blur import blurFusion

avi_codec_dict = {
	'Cinepack' : 0,
	'Indeo Video R3.2' : 1,
#	'Indeo Video R3.2' : 2, # Fusion has these two listed the same...
	'Indeo Video R4.5' : 3,
	'Indeo Video R5.10' : 4,
	'Intel IYUV' : 5,
	'Microsoft Video 1' : 6,
	'DivX 6.0' : 7,
	'Techsmith Screen Capture' : 8,
	'Uncompressed' : 9
	}

qt_codec_dict = {
	'MPEG-4 Video' : 'MPEG-4 Video_mp4v',
	'Sorenson Video' : 'Sorenson Video 3_SVQ1',
	'Sorenson Video 3' : 'Sorenson Video 3_SVQ3',
	'Cinepack' : ''
	}

class FusionVideoMakerBurner( FusionBurner ):
	def __init__(self,job,slave):
		FusionBurner.__init__(self,job,slave)
		
	# Returns a frame path with 0000 frame, ex. 'c:/temp/test_jpg0120.jpg' -> 'c:/temp/test_jpg0000.jpg'
	def zeroFramePath(self,framePath):
		inputPathBase = framePathBaseName( framePath )[0] # returns a_frame_sequence_.jpeg
		return Path(framePath).dirPath() + '/' + makeFramePath( inputPathBase, 0 ) # G:/temp/a_frame_sequence_0000.jpeg
	
	def burnFile(self):
		return self.BurnFile
	
	def startProcess(self):
		compTemplatePath = "C:/blur/assburner/plugins/fusion_video_maker_template.comp"
		self.BurnFile = self.burnDir() + '/fusion_video_maker.comp'
		frameStart = self.job().sequenceFrameStart()
		frameEnd = self.job().sequenceFrameEnd()
		
		# Parse Comp Template
		comp = blurFusion.parseEyeonFile(compTemplatePath)
		
		# Setup global and render range
		comp.GlobalRange = blurFusion.FusionTable(Array=[frameStart,frameEnd])
		comp.RenderRange = blurFusion.FusionTable(Array=[frameStart,frameEnd])
		comp.CurrentTime = frameStart
		
		# Get input and output paths in fusion format
		zeroPath = self.zeroFramePath(self.job().inputFramePath()) # Should be something like G:/temp/a_frame_sequence_0111.jpeg
		outputPath = self.job().outputPath()
		comp.OutputClips = blurFusion.FusionTable(Array=[str(outputPath).replace('\\','/')])
		
		# Setup Input Frames
		clip = comp.Tools.Loader1.Clips[0]
		clip.Filename = str(zeroPath).replace('/','\\')
		clip.FormatID = blurFusion.getFusionFormat(QFileInfo(zeroPath).suffix())
		clip.TrimIn = 0
		clip.TrimOut = frameEnd - frameStart
		clip.GlobalStart = clip.StartFrame = frameStart
		clip.GlobalEnd = frameEnd
		clip.Length = frameEnd - frameStart + 1
		
		# Setup Video Output
		comp.Tools.Saver1.Inputs.Clip.Value.Filename = str(outputPath).replace( "\\", "/" )
		
		if outputPath.toLower().endsWith( 'avi' ):
			codec = 7 # DivX as default
			if str(self.job().codec()) in avi_codec_dict:
				codec = avi_codec_dict[str(self.job().codec())]
			comp.Tools.Saver1.Inputs["AVIFormat.Compression"].Value = codec
		elif outputPath.toLower().endsWith( 'mov' ):
			comp.Tools.Saver1.Inputs.Clip.Value.FormatID = "QuickTimeMovies"
			comp.Tools.Saver1.Inputs.OutputFormat.Value[0] = "QuickTimeMovies"
			# TODO - Support Multiple codecs
			codec = "Sorenson Video 3"
			if self.job().codec() in qt_codec_dict:
				codec = self.job().codec()
			if codec == 'Cinepack':
				del comp.Tools.Saver1.Inputs["QuickTimeMovies.Compression"]
			else:
				comp.Tools.Saver1.Inputs["QuickTimeMovies.Compression"].Value[0] = qt_codec_dict[codec]
		
		if QFile( self.BurnFile ).exists():
			QFile.remove( self.BurnFile )
		
		blurFusion.writeEyeonFile( comp, self.BurnFile )
		
		FusionBurner.startProcess(self)
	
	def cleanup(self):
		#QFile.remove( self.BurnFile )
		FusionBurner.cleanup(self)

class FusionBurnerPlugin(JobBurnerPlugin):
	def __init__(self):
		JobBurnerPlugin.__init__(self)
		if sys.platform == 'win32':
			disableWindowsErrorReporting( "fusion.exe" )
			disableWindowsErrorReporting( "consoleslave.exe" )
			disableWindowsErrorReporting( "renderslave.exe" )

	def jobTypes(self):
		return QStringList('Fusion') << 'FusionVideoMaker'

	def createBurner(self,jobAssignment,slave):
		Log( "FusionBurnerPlugin::createBurner() called, Creating FusionBurner" )
		jobTypeName = jobAssignment.job().jobType().name()
		if jobTypeName == 'Fusion':
			return FusionBurner(jobAssignment,slave)
		elif jobTypeName == 'FusionVideoMaker':
			return FusionVideoMakerBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( FusionBurnerPlugin() )
