

from blur.Stone import Interval
from blur.Classes import Host, Config
from blur.Assburner import Slave
from PyQt4.QtCore import QObject, QTimer, QTime, QDateTime
from PyQt4.QtGui import QMessageBox
import sys

# Taken from python's 'uptime' module.
# BSD license
def _uptime_windows():
	import ctypes
	"""
	Returns uptime in seconds or None, on Windows. Warning: may return
	incorrect answers after 49.7 days on versions older than Vista.
	"""
	if hasattr(ctypes, 'windll') and hasattr(ctypes.windll, 'kernel32'):
		lib = ctypes.windll.kernel32
	else:
		try:
			# Windows CE uses the cdecl calling convention.
			lib = ctypes.CDLL('coredll.lib')
		except (AttributeError, OSError):
			return None

	if hasattr(lib, 'GetTickCount64'):
		# Vista/Server 2008 or later.
		lib.GetTickCount64.restype = ctypes.c_uint64
		return lib.GetTickCount64() / 1000.
	if hasattr(lib, 'GetTickCount'):
		# WinCE and Win2k or later; gives wrong answers after 49.7 days.
		lib.GetTickCount.restype = ctypes.c_uint32
		return lib.GetTickCount() / 1000.
	return None

# Returns the windows uptime in a blur.Stone.Interval object
def getWindowsUptime():
	#import win32pdh
	#path = win32pdh.MakeCounterPath( ( None, 'System', None, None, 0, 'System Up Time') )
	#query = win32pdh.OpenQuery()
	#handle = win32pdh.AddCounter( query, path )
	#win32pdh.CollectQueryData( query )
	#seconds = win32pdh.GetFormattedCounterValue( handle, win32pdh.PDH_FMT_LONG | win32pdh.PDH_FMT_NOSCALE )[ 1 ]
	#return Interval(seconds)
	return Interval(_uptime_windows())

# Automatically reboots the host after completing a burn if the uptime is larger than an interval
# specified by 'assburnerRebootInterval' in the Config table, or 48 hours if that's not a valid interval
class AutoRebooter(QObject):
	def __init__(self,slave):
		QObject.__init__(self,slave)
		self.Slave = slave
		self.Slave.statusChange.connect( self.checkReboot )
		self.WarningBox = None
		self.SchduledRebootTime = None
		defaultRebootIntervalStr = '48 hours'
		(self.RebootInterval,valid) = Interval.fromString( Config.getString( 'assburnerRebootInterval', defaultRebootIntervalStr ) )
		if not valid:
			self.RebootInterval = Interval.fromString( defaultRebootIntervalStr )[0]
		self.printStatus()

	def printStatus(self):
		print "Auto Reboot Interval:", self.RebootInterval.toString(), "Current Uptime:", getWindowsUptime().toString()

	def checkReboot(self,slaveStatus):
		if slaveStatus != 'ready':
			return
		self.printStatus()
		hour = QTime.currentTime().hour()
		if getWindowsUptime() > self.RebootInterval:
			# Any user activity in the last hour will give a 10 minute warning before rebooting
			if self.Slave.secondsIdle() < 60 * 60:
				print "Uptime exceeded reboot interval, user activity detected, showing reboot warning"
				self.warnReboot()
				return
			print "Uptime exceeded reboot interval, rebooting"
			self.Slave.reboot("Assburner rebooting due to uptime exceeding reboot interval")

	def updateWarningDialog(self):
		timeLeft = Interval( QDateTime.currentDateTime(), self.ScheduledRebootTime )
		if timeLeft <= Interval(0):
			self.WarningBox.setWindowTitle( "Too late, rebooting" )
			self.Slave.reboot("Assburner rebooting due to uptime exceeding reboot interval")
		self.WarningBox.setWindowTitle( "Reboot Scheduled in " + timeLeft.toDisplayString() )
	
	def cancelReboot(self):
		self.RebootWarningTimer.stop()
		self.RebootWarningTimer.deleteLater()
		self.RebootWarningTimer = None
		self.WarningBox.deleteLater()
		self.WarningBox = None
		self.RebootInterval = getWindowsUptime() + Interval.fromString( '12 hours' )[0]
	
	def warnReboot(self):
		if self.WarningBox:
			return
		self.ScheduledRebootTime = QDateTime.currentDateTime().addSecs(10 * 60)
		self.WarningBox = QMessageBox()
		self.WarningBox.rejected.connect( self.cancelReboot )
		self.WarningBox.addButton( 'Delay reboot for 12 hours', QMessageBox.RejectRole )
		self.WarningBox.setIcon( QMessageBox.Warning )
		self.WarningBox.setText( 'This computer is scheduled to reboot because it has been online for more than ' + self.RebootInterval.toDisplayString() )
		self.WarningBox.setSizeGripEnabled( True )
		self.updateWarningDialog()
		self.WarningBox.show()
		self.RebootWarningTimer = QTimer(self)
		self.RebootWarningTimer.timeout.connect(self.updateWarningDialog)
		self.RebootWarningTimer.start(1000)
		
AutoRebootInstance = None

# Only enable the rebooter for non-user windows hosts
if sys.platform == "win32" and not Host.currentHost().user().isRecord():
	AutoRebootInstance = AutoRebooter( Slave.instance() )

