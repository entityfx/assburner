
from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback, os

class NukeBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.Job = jobAssignment.job()
		self.CustomEnv = self.splitEnvironment(self.Job.environment())
		self.Slave = slave
		self.NukeService = None
		self.NukeSoftware = None
		
		self.CurrentFrame = None
		self.FramePendingComplete = None
		self.TotalRenderedFrames = 0

		self.LastRenderedFrame = None

		self.OutputsExpected = self.Job.jobOutputs().size()
		self.OutputsReported = 0

		self.renderStarted = re.compile(r"Read nuke script:")
		self.frameDone = re.compile(r"Frame (-?\d+) \((\d+) of (\d+)\)")
		self.imageWriteMessage = re.compile(r"^Writing .*? (took)?")
		self.jobDone = QRegExp("^Total render time:")
		self.errors = []
		self.errors.append(QRegExp("^Error:"))
		self.errors.append(QRegExp("Read-only file system"))
		self.errors.append(QRegExp("Command exited with non-zero status"))
		self.errors.append(QRegExp("No such file or directory"))
		self.errors.append(QRegExp("License failure"))
		self.errors.append(QRegExp("Read.+:.+0 bytes"))
		self.errors.append(QRegExp("Read.+:.+ not a \w+ file"))
		self.errors.append(QRegExp("couldn't read file"))
		self.errors.append(QRegExp("Can't write"))
		self.errors.append(QRegExp("cannot be executed for multiple views"))
		self.errors.append(QRegExp("Non-existent channel used for mask"))
		self.errors.append(QRegExp("Can't read"))
		self.errors.append(QRegExp("Read.+:.+ Truncated"))
		
		self.ErrorDialogTitles = ['Optical Flares License']
		
	def getNukeService(self):
		if self.NukeService is None:
			nukeServices = self.job().jobServices().services().filter('service',QRegExp('^Nuke'))
			if len(nukeServices) == 0:
				raise "Unable to find Nuke service for job to determine render executable"
			if len(nukeServices) > 1:
				raise "Multiple Nuke services listed, unable to determine which Nuke installation to use"
			self.NukeService = nukeServices[0]
		return self.NukeService
	
	def getNukeSoftware(self):
		if self.NukeSoftware is None:
			service = self.getNukeService()
			self.NukeSoftware = service.software()
			if not self.NukeSoftware.isRecord():
				self.jobErrored( "service " + service.service() + " has no software entry" );
		return self.NukeSoftware

	def getNukeDir(self):
		sw = self.getNukeSoftware()
		return sw.installedPath()
	
	def workingDirectory(self):
		return self.getNukeDir()

	def slotProcessExited(self):
		if self.TotalRenderedFrames == 0:
			self.jobErrored("Nuke exited without rendering any frames. Check write node")
		elif self.FinishedMessageFound:
			Log( "NukeBurner::slotProcessExit() jobFinished()" )
			self.jobFinished()
		else:
			self.jobErrored("Nuke exited unexpectedly")

	def executable(self):
		#timeCmd = "/usr/bin/time --format=baztime:real:%e:user:%U:sys:%S:iowait:%w ";
		#cmd = timeCmd + "/bin/su %s -c \"$NUKE/Nuke$NUKE_MAJOR " % self.Job.user().name()
		try:
			return self.getNukeDir() + self.getNukeSoftware().executable()
		except:
			traceback.print_exc()
			self.jobErrored( "Unknown exception getting nuke path: " + traceback.format_exc() )

	@staticmethod
	def splitEnvironmentKeyVal(env):
		return {unicode(k):unicode(v) for k,v in JobBurner.splitEnvironmentKeyVal(env).iteritems()}

	def buildCmdArgs(self):
		env = self.splitEnvironmentKeyVal(self.CustomEnv)
		renderAll = False
		if 'NUKE_RENDERABLE' in env:
			renderable = env['NUKE_RENDERABLE']
		else:
			renderAll = True

		args = QStringList()
		# verbose
		args << "-V" << "2"
		if renderAll:
			# render all in script
			args << "-x"
		else:
			# render specified nodes
			args << "-X" << renderable
		# terminal only
		# behavior change between Nuke5 and Nuke6
		#args << "-t"
		# full-size - no proxy
		args << "-f"
		# cache limit
		args << "-c"
		args << "25G"
		# # of threads
		args << "-m"
		args << str(Host.currentHost().cpus())
		# frame range
		# Nuke supports a single continuous sequence per -F option
		for frameRange in self.assignedTasks().split(','):
			args << "-F" << frameRange
		args << self.burnFile()
		return args

	def startProcess(self):
		Log( "NukeBurner::startBurn() called" )
		JobBurner.startProcess(self)
		self.Frames = self.assignedTaskList().frameNumbers()
		self.CurrentFrame = None

		self.logMessage("starting render - expecting %s outputs" % self.OutputsExpected)
	#        self.taskStart(self.CurrentFrame)
		self.checkupTimer().start( 2 * 60 * 1000 )
		Log( "NukeBurner::startBurn() done" )

	def slotProcessOutputLine(self,line,channel):
		#Log( "NukeBurner::slotReadOutput() called, ready to read output" )
		# Frame status
		if self.jobDone.indexIn(line) >= 0:
			self.FinishedMessageFound = True

		# Render started
		if self.renderStarted.search(line):
			self.CurrentFrame = self.Frames.pop(0)
			self.taskStart(self.CurrentFrame)
		
		# Nuke reported an image write began or completed
		elif self.imageWriteMessage.match(line):
			# look for "took" -- if it's found, the image is finished rendering
			if self.imageWriteMessage.match(line).group(1):
				self.TotalRenderedFrames += 1
				self.OutputsReported += 1
				self.logMessage( "NukeBurner::slotReadOutput() output detected %s of %s" % (self.OutputsReported, self.OutputsExpected) )

		# Nuke reported it finished rendering a frame
		elif self.frameDone.match(line):
			if self.OutputsReported == self.OutputsExpected:
				self.logMessage( "NukeBurner::slotReadOutput() all outputs done for frame" )
			elif self.OutputsReported > self.OutputsExpected:
				self.jobErrored( "Unexpected outputs generated" )
			else:
				self.jobErrored( "Fewer outputs than expected generated" )
			
			reportedFrame = int(self.frameDone.match(line).group(1))
			if reportedFrame != self.CurrentFrame:
				self.jobErrored( "Nuke rendered a different frame than expected" )
			else:
				self.taskDone(self.CurrentFrame)
			
			# reset outputs reported and increment frame
			self.OutputsReported = 0
			if self.Frames:
				self.CurrentFrame = self.Frames.pop(0)
				self.taskStart(self.CurrentFrame)

		else:
			for e in self.errors:
				if line.contains(e):
					self.jobErrored( line )

	def checkup(self):
		found, title = findMatchingWindow( self.processId(), self.ErrorDialogTitles, True, False )
		if found:
			self.jobErrored( "Found window: " + title + ", cancelling the burn")
		return JobBurner.checkup(self)


class NukeBurnerPlugin(JobBurnerPlugin):
	def jobTypes(self):
		return ['Nuke']

	def createBurner(self,jobAssignment,slave):
		return NukeBurner(jobAssignment,slave)

JobBurnerFactory.registerPlugin( NukeBurnerPlugin(), True )

