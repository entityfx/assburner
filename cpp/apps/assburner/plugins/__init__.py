
import sys

from blur.logredirect import RedirectOutputToLog
RedirectOutputToLog()

import aftereffects
import auto_reboot
import cinema4d
import clientupdate
import fusion
import hardware_info
import houdini
import mantra
import nuke
import realflow
import tempfile_cleaner
import vray_spawner
import xsi_v5

if sys.platform=='win32':
	import fumefx_sim
