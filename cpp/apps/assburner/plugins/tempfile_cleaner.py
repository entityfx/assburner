
import os, sys, time, platform, re
from PyQt4.QtCore import *

class TempFilesDeleteObj(QObject):
	def __init__(self,parent=None):
		QObject.__init__(self,parent)
		self.Timer = QTimer(self)
		self.Timer.timeout.connect( self.slotDeleteTempFiles )
		self.Timer.start( 60 * 60 * 1000 )
		self.Thread = None
		QTimer.singleShot( 5000, self.slotDeleteTempFiles )

	def slotDeleteTempFiles(self):
		import threading
		if self.Thread and self.Thread.isAlive():
			return
		self.Thread = threading.Thread(target=deleteTempFiles,kwargs=dict(verbose=False))
		
def deleteTempFiles(days=1.5, delete=True, verbose=True):
	print "Deleting Temp Files"
	
	try:
		ostype = os.uname()[0]
	except:
		ostype = "Other,but,probably,Windows"

	if ostype == "Linux":
		TempFiles_chk_Linux(days, delete, verbose)
	elif ostype == "Darwin":
		TempFiles_chk_OSX(days, delete, verbose)
	else:
		TempFiles_chk_Win(days, delete, verbose)

# Check for files and folders on unix/osx
def nix_check(name,st = None):
	excludeStrings = [ 'spool', 'sock', 'socket', 'ssh', 'agent', 'mds', 'img' ]
	for item in excludeStrings:
		if item in name.lower():
			return False
	return True

def TempFiles_chk_Linux(days=3,delete=False,verbose=False):
	hitList = [ '/Local/temp_maya',
				'/Local/tmp',
				'/var/tmp',
				'/usr/tmp' ]
	hitList = [(item,nix_check,nix_check) for item in hitList]
	deleteScan(hitList,days,delete,verbose)

def TempFiles_chk_OSX(days=3,delete=False,verbose=False):
	hitList = [ 
		'/var/root/Library/Caches/Adobe/After Effects CS3',
		'/var/folders' ]
	
	hitList = [(item,nix_check,nix_check) for item in hitList]
	deleteScan(hitList,days,delete,verbose)

def TempFiles_chk_Win(days,delete=False,verbose=False):
	if sys.getwindowsversion()[0] != 6: #win7 check
		basedir = r'C:\Documents and Settings'
		childdir = r'Local Settings\Temp'
	else:
		basedir = r'C:\Users'
		childdir = r'AppData\Local\Temp'

	def deleteFileCheck(name, st):
		# Always try to delete vraylog.txt, no matter the age
		if os.path.basename(name) == 'vraylog.txt':
			return True
		if (time.time() - st.st_atime) < days * 86400:
			return False
		return True
	
	excludeList = ['Default','Default User','NetworkService','Administrator','LocalService','All Users','LocalXSIWorkgroup','Public']
	# User temp folders
	dirList = [(os.path.join(basedir,name,childdir),deleteFileCheck,None) for name in os.listdir(basedir) if name not in excludeList]
	# C:/temp/ folder, with regex
	dirList.append( (r'C:\temp',lambda a, b: re.compile('.*(log|icecache|tmp)$').match(a),None) )
	dirList.append( (r'C:\max2010_x64\Must_Be_Run_From_This_Folder\temp', lambda a, b: re.compile('.*tmp$').match(a), None) )
	
	# We check age in our deleteFileCheck function above, so pass None here
	deleteScan(dirList, None, delete, verbose)

# Deletes a file if it's at least minAge seconds old, and calling delCheck with the fileName
# as an argument returns True if delCheck is not None
# Returns the total bytes deleted
def checkDelete(fileName, minAge = None, delCheck = None, verbose = True, delete = True):
	try:
		stats = os.stat(fileName)
	except:
		return 0
	# Make sure it passes the user defined test, if provided
	if delCheck is not None:
		if not delCheck( fileName, stats ):
			return 0
	# Make sure the file is old enough
	if minAge is not None:
		if (time.time() - stats.st_atime) < minAge:
			return 0
	fileSize = stats.st_size
	# Delete if it's not a dry run
	if not delete and verbose:
		print 'File Matches Delete Criteria:', fileName
	if delete:
		try:
			if verbose:
				print 'Deleting File', fileName
			os.remove(fileName)
		except:
			if verbose:
				print 'Could not delete:', fileName
			fileSize = 0
	return fileSize

# Folders is a list, each item in the list must be a string or a tuple with 3 elements (folder, delFileCheck, delFolderCheck)
# delFileCheck and/or delFolderCheck can be None
def deleteScan(dirList, days=3,delete=False,verbose=False):
	minAgeToDelete = None
	if days is not None:
		minAgeToDelete = days * 86400
	foldersToDelete = []
	totalSpaceFreed = 0
	
	for dir in dirList:
		delFileCheck, delFolderCheck = None, None
		if isinstance(dir,tuple):
			dir, delFileCheck, delFolderCheck = dir
		if os.path.exists(dir):
			if verbose:
				print 'Searching...', dir
			for root, dirs, files in os.walk(dir):
				if verbose:
					print 'Scanning...', root
				if root != dir and (delFolderCheck is None or delFolderCheck(root)):
					if verbose and not delete:
						print "Directory matches delete criteria:", root
					foldersToDelete.append(root)
				for filename in files:
					totalSpaceFreed += checkDelete( os.path.join(root, filename), minAgeToDelete, delFileCheck, verbose, delete )
			if dir in foldersToDelete:
				foldersToDelete.remove(dir)

	if delete:
		foldersToDelete.reverse()
		for folderName in foldersToDelete:
			try:
				if verbose:
					print 'Removing Directory', folderName
				os.rmdir(folderName)
			except:
				if verbose:
					print 'Couldn\'t remove dir:', folderName

	print "The following space %s freed: %i MB" % ( {False : "would be", True : "has been"}[delete], int(totalSpaceFreed / (1024 * 1024)) )

_TDFO_ = TempFilesDeleteObj()

if __name__ == '__main__' and hasattr(sys,'argv'):
	delete=False
	if '-delete' in sys.argv:
		delete = True
	deleteTempFiles(delete=delete,verbose=True)
