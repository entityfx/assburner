from blur.Stone import *
from blur.Classes import *
from blur.Assburner import *
from PyQt4.QtCore import *
from PyQt4.QtSql import *
import re, traceback

class RealFlowBurner(JobBurner):
	def __init__(self,jobAssignment,slave):
		JobBurner.__init__(self,jobAssignment,slave)
		self.RenderStarted = False
		self.CurrentFrame = None
		self.JobDone = False
		self.RealFlowService = None
		self.RealFlowSoftware = None
		self.RealFlowDir = None

	# Names of processes to kill after burn is finished
	def processNames(self):
		return QStringList() << self.executable()
	
	def getRealFlowService(self):
		if self.RealFlowService is None:
			realflowServices = self.job().jobServices().services().filter('service',QRegExp('^RealFlow'))
			if len(realflowServices) == 0:
				raise "Unable to find RealFlow service for job to determine render executable"
			if len(realflowServices) > 1:
				raise "Multiple RealFlow services listed, unable to determine which RealFlow installation to use"
			self.RealFlowService = realflowServices[0]
		return self.RealFlowService
	
	def getRealFlowSoftware(self):
		if self.RealFlowSoftware is None:
			service = self.getRealFlowService()
			self.RealFlowSoftware = service.software()
			if not self.RealFlowSoftware.isRecord():
				self.jobErrored( "service " + service.service() + " has no software entry" );
		return self.RealFlowSoftware

	def getRealFlowDir(self):
		sw = self.getRealFlowSoftware()
		return sw.installedPath()
	
	def workingDirectory(self):
		return self.getRealFlowDir()
	
	def executable(self):
		try:
			return self.getRealFlowDir() + self.getRealFlowSoftware().executable()
		except:
			traceback.print_exc()
			self.jobErrored( "Unknown exception getting realflow path: " + traceback.format_exc() )
		
	def buildCmdArgs(self):
		# We should be set up with continuos frame assignments
		tasks = self.assignedTasks().split("-")
		self.StartFrame = int(tasks[0])
		if len(tasks) == 2:
			self.EndFrame = int(tasks[1])
		elif len(tasks) == 1:
			self.EndFrame = int(tasks[0])
		# C:\Program Files (x86)\Next Limit\x64\RealFlow\RealFlow.exe -nogui -range 1 100 -mesh -useCache  "emitter.flw" 
		args = QStringList()
		# -nogui for realflow.exe, not for realflownode.exe
		if self.executable() == 'realflow.exe':
			args << "-nogui"
		args << "-range"
		endFrame = self.EndFrame
		# It seems that the end frame is not rendered unless the range is only one frame
		# So we pass an extra frame
		if endFrame > self.StartFrame:
			endFrame += 1
		args << str(self.StartFrame) << str(endFrame)
		if self.job().simType() == 'Mesh':
			args << "-mesh"
		elif self.job().simType() == 'Sim':
			args << "-useCache"
		args << self.burnFile()
		return args
	
	def startProcess(self):
		Log( "RealFlowBurner::startBurn() called" )
		JobBurner.startProcess(self)
		self.BurnProcess = self.process()
		self.Started = QDateTime.currentDateTime()
		self.checkupTimer().start(2 * 60 * 1000) # Every two minutes
		Log( "RealFlowBurner::startBurn() done" )
	
	
#C:\Program Files (x86)\Next Limit\x64\RealFlow4>RealFlow -nogui -range 1 2 "G:\F
#errari\01_Shots\Sc005\S0001.00\FX\LavaSplash\LavaSplashPreset\LavaSplashPreset-V
#3.flw"
#'import site' failed; use -v for traceback
#-----------------------------------------------------------------
  #1998-2007 (c) Next Limit - RealFlow(32bits) v4.3.8.0124
#-----------------------------------------------------------------

#Initializing Parameter Blocks ...
#Checking license (use -license to enter a valid license)...
#Initializing expressions engine ...
#Initializing Real Impact world ...
#Initializing Simulation Events dispatcher ...

#Loading plugins ...
#Loading workspace G:\Ferrari\01_Shots\Sc005\S0001.00\FX\LavaSplash\LavaSplashPre
#set\LavaSplashPreset-V3.flw ...

#>12:27:33: RealFlow error: Couldn't install license server. The UDP port 2222is
#used by another application. Probably another RealFlow instance.

#>>> Reading SD file
#>>> 50%
#>>> 100%



#>12:27:33: Workspace version: 2046
#>>> Setting up scene
#>>> 14%
#>>> 28%
#>>> 42%
#>>> 57%
#>>> 71%
#>>> 85%
#>>> 100%



#Using 2 threads for this simulation ...
#..................................................
#>12:27:51: Frame 2 finished.

#>12:27:51: Elapsed time: (0h0m18s)

#>12:27:51: =========================================

	def slotProcessOutputLine(self,line,channel):
		Log( "RealFlowBurner::slotReadOutput() called, ready to read output" )
		error = False
		
		# Job started
		if re.search( '100%', line ):
			self.RenderStarted = True
			#self.taskStart(self.StartFrame)
		
		started = re.search( r'Frame (\d+) started', line )
		if started:
			frame = int(started.group(1))
			if frame > self.CurrentFrame:
				if self.CurrentFrame == self.StartFrame:
					self.taskDone(self.CurrentFrame)
				self.taskStart(frame)
				self.CurrentFrame = frame
		
		# Frames Rendered
		finished = re.search( r'Frame finished', line )
		#mo = re.search( r'Frame (\d+) finished.', line )
		if finished:
			#frame = int(mo.group(1))
			current = self.currentTasks()
			
			# We should always have a current task when we get a frame finished message
			# because we call taskStart above when we get the 100% line
			if len(current) != 1:
				self.jobErrored( "Got unexpected frame finished line" )
				return
			
			# Special case the fact that we often don't get a Frame finished line for the first
			# frame specified on the command line.
			# Actually with RealFlow2013 we get a bunch of Frame 0 Start/Frame finished messages
			current = current[0]
			if current.frameNumber() != self.CurrentFrame:
				self.jobErrored( "self.CurrentFrame != currentTasks frame number" )
				return
				
			if self.CurrentFrame > self.StartFrame:
				self.taskDone(current.frameNumber())
			
			# If endframe==startframe, we can end up with multiple finished messages
			# so we won't set jobfinished until the process exits
			if self.CurrentFrame == self.EndFrame and self.CurrentFrame > self.StartFrame:
				self.JobDone = True
				QTimer.singleShot( 30000, self, SLOT('jobFinished()') )
		
		# TIFFOpen: T:/Groundhog/Cache_RealFlow/_Effects/Underwater/displacement/HY_Domain02_v01_03_displacement_00600.tif: Cannot open.
		if 'Cannot open.' in line:
			self.jobErrored(line)
		
		JobBurner.slotProcessOutputLine(self,line,channel)

	def slotProcessExited(self):
		if self.CurrentFrame == self.EndFrame == self.StartFrame:
			self.JobDone = True
		if not self.JobDone:
			self.checkup()
		if self.JobDone:
			self.jobFinished()
			return
		JobBurner.slotProcessExited(self)

class RealFlowScriptBurner(RealFlowBurner):
	def __init__(self,jobAssignment,slave):
		RealFlowBurner.__init__(self,jobAssignment,slave)
		self.ScriptPath = None
	
	def buildCmdArgs(self):
		# C:\Program Files (x86)\Next Limit\x64\RealFlow\RealFlowNode.exe -script realflowscript.rfs
		args = QStringList()
		# -nogui for realflow.exe, not for realflownode.exe
		if self.executable() == 'realflow.exe':
			args << "-nogui"
		args << '-script' << self.getScriptPath()
		return args
	
	# Take JobRealFlow.script and put it into a file after substituting some variables
	def getScriptPath(self):
		if self.ScriptPath is None:
			# The file will be deleted when the QTemporaryFile object is deleted
			# which will happen when the RealFlowScriptBurner is deleted
			self.ScriptFile = QTemporaryFile("c:\\temp\\realflowXXXXXX.rfs")
			if not self.ScriptFile.open():
				self.jobErrored( 'Unable to open script temporary file for writing' )
				return ''
			self.ScriptPath  = self.ScriptFile.fileName()
			txt = self.job().script()
			txt.replace( "%BURN_DIR%", self.burnDir() + "/" );
			txt.replace( "%BURN_FILE%", self.burnFile() );
			txt.replace( "%START_FRAME%", str(self.StartFrame) )
			txt.replace( "%END_FRAME%", str(self.EndFrame) )
			QTextStream(self.ScriptFile) << txt
			self.ScriptFile.close()
		return self.ScriptPath
	
class RealFlowBurnerPlugin(JobBurnerPlugin):
	def __init__(self):
		JobBurnerPlugin.__init__(self)

	def jobTypes(self):
		return (QStringList('RealFlow') << 'RealFlowScript')

	def createBurner(self,jobAssignment,slave):
		Log( "RealFlowBurnerPlugin::createBurner() called, Creating RealFlowBurner" )
		job = jobAssignment.job()
		if job.jobType().name() == 'RealFlow':
			if job.simType() == 'Script':
				return RealFlowScriptBurner(jobAssignment,slave)
			return RealFlowBurner(jobAssignment,slave)
		return None

JobBurnerFactory.registerPlugin( RealFlowBurnerPlugin() )
