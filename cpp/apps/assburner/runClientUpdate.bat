
REM - Give time for assburner to exit
ping -n 5 127.0.0.1 > NUL

REM - Run the actual client-update batch file, pipe output to a file
call %{clientUpdateBatchPath} > client_update_output.txt 2>&1

REM - Record success or failure with error messages to the database
IF ERRORLEVEL 1 GOTO ERROR               REM - Catch all return codes >= 1
IF ERRORLEVEL 0 GOTO SUCCESS             REM - Catch 0 success return code
GOTO ERROR                               REM - Catch negative return codes

:SUCCESS
c:\python27\python -m blur.clientupdateutil success %{taskKey} client_update_output.txt
GOTO END

:ERROR
c:\python27\python -m blur.clientupdateutil error %{taskKey} client_update_output.txt %ERRORLEVEL%

:END

REM - Start assburner
c:\blur\assburner\assburner.exe
