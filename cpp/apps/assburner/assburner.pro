
TARGET=assburner

include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/python.pri)
include($$(PRI_SHARED)/classes.pri)
include($$(PRI_SHARED)/stonegui.pri)
include($$(PRI_SHARED)/stone.pri)

DEFINES += COMPILE_AFTER_EFFECTS_BURNER
DEFINES += COMPILE_BATCH_BURNER
DEFINES += COMPILE_MAX_BURNER
win32:DEFINES += COMPILE_MAXSCRIPT_BURNER

DEFINES += COMPILE_MAYA_BURNER
DEFINES += COMPILE_MENTALRAY_BURNER
DEFINES += COMPILE_RENDERMAN_BURNER
DEFINES += COMPILE_RIBGEN_BURNER
DEFINES += COMPILE_SHAKE_BURNER
DEFINES += COMPILE_SYNC_BURNER
#DEFINES += COMPILE_AUTODESK_BURNER

INCLUDEPATH+=include
INCLUDEPATH+=idle

win32:QMAKE_LFLAGS+=/FORCE:MULTIPLE
#QMAKE_CXXFLAGS+=/Z7
#QMAKE_LFLAGS+=/DEBUG

SOURCES += \
	src/application.cpp \
	src/batchburner.cpp \
	src/builtinburnerplugin.cpp \
	src/common.cpp \
	src/jobburnerfactory.cpp \
	src/jobburner.cpp \
	src/killdialog.cpp \
	src/maindialog.cpp \
	src/main.cpp \
	src/mapwarningdialog.cpp \
	src/aftereffectsburner.cpp \
	src/autodeskburnburner.cpp \
	src/maxburner.cpp \
	src/maxscriptburner.cpp \
	src/mayaburner.cpp \
	src/rendermanburner.cpp \
	src/ribgenburner.cpp \
	src/shakeburner.cpp \
	src/syncburner.cpp \
	src/settingsdialog.cpp \
	src/slave.cpp \
	src/spooler.cpp \
	src/abstractdownload.cpp \
	src/wincopy.cpp \
	src/win32sharemanager.cpp

HEADERS += \
	include/application.h \
	include/batchburner.h \
	include/builtinburnerplugin.h \
	include/common.h \
	include/jobburnerfactory.h \
	include/jobburnerplugin.h \
	include/jobburner.h \
	include/mapwarningdialog.h \
	include/aftereffectsburner.h \
	include/autodeskburnburner.h \
	include/maxburner.h \
	include/maxscriptburner.h \
	include/mayaburner.h \
	include/rendermanburner.h \
	include/ribgenburner.h \
	include/shakeburner.h \
	include/syncburner.h \
	include/killdialog.h \
	include/maindialog.h \
	include/settingsdialog.h \
	include/slave.h \
	include/spooler.h \
	include/abstractdownload.h \
	include/wincopy.h \
	include/win32sharemanager.h

SOURCES += src/oopburner.cpp
HEADERS += include/oopburner.h

FORMS += \
	ui/killdialogui.ui \
	ui/maindialogui.ui \
	ui/mapwarningdialogui.ui \
	ui/settingsdialogui.ui

DEPENDPATH+=src include ui

RESOURCES += assburner.qrc

# Idle - taken from Psi
include( "idle/idle.pri" )

# Python modules
{
    LIBS+=-LsipAssburner -lpyAssburner
	win32 {
	    LIBS+=-L../../lib/classes/sipClasses -lpy_Classes
		LIBS+=-L../../lib/stone/sipStone -lpy_Stone
		LIBS+=-L../../lib/sip/siplib -lsip
	}
}

win32 {
	LIBS += -lpsapi -lMpr -lws2_32 -lgdi32 -luser32 -lWer
}
 
unix:!macx {
	LIBS += -lX11 -lXss -L/usr/X11R6/lib -L/usr/X11R6/lib64
}

CONFIG += qt thread warn_on rtti exceptions
QT+=network sql xml
RC_FILE = assburner.rc

# CONFIG += console

win32 {
	debug {
		CONFIG += console
	}
}

target.path=/usr/local/bin
INSTALLS += target
