
from optparse import OptionParser
import sys

parser = OptionParser(usage="usage: %prog [options] hip_name rop_name output_file start_frame end_frame", version="%prog 1.0")
options, args = parser.parse_args()

if len(args) < 5:
	parser.error('At least one of the mandatory arguments are missing.')
elif len(args) > 5:
	parser.error('Too many arguments provided.')
else:
	hipFile = args[0]
	ropPath = args[1]
	outputFile = args[2]
	startFrame = args[3]
	endFrame = args[4]

def generateIfds(hipFile, ropPath, outPath, startFrame, endFrame):
	hou.hipFile.load(hipFile)
	rop = hou.node(ropPath)
	if not rop:
		raise ValueError('Given path does not exist: {rop}'.format(rop=ropPath))

	# Make sure the rop is set to write ifds.
	rop.parm('soho_outputmode').set(1)

	# Set to render a single frame from soho.
	rop.parm('trange').set('off')

	# Set the ifd output path.
	rop.parm('soho_diskfile').set(outPath)

	for f in range(int(startFrame), (int(endFrame) + 1)):
		# Set the frame.  This might be overkill since we're baking the single
		# frame into the frange parm, but better to be safe.
		hou.setFrame(f)

		# ...and then hit the "Render Locally" button.
		print 'Starting Frame {num}.'.format(num=str(f))
		sys.stdout.flush()
		rop.render()
		print 'Frame {num} complete.'.format(num=str(f))
		sys.stdout.flush()

# The scene file, the path to the output node/rop (rop is "render operator"),
# the output path for the ifd file(s) ($F4 means 4-padded frame number, ie: 0001),
# and a frame list.
generateIfds(hipFile, ropPath, outputFile, startFrame, endFrame)


