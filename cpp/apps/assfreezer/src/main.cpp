
/*
 *
 * Copyright 2003 Blur Studio Inc.
 *
 * This file is part of RenderLine.
 *
 * RenderLine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * RenderLine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RenderLine; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Python.h"

#include <qapplication.h>
#include <qdir.h>
#include <qfile.h>
#include <qhostaddress.h>
#include <qlibrary.h>
#include <qmessagebox.h>
#include <qpalette.h>
#include <qsettings.h>

#include "blurqt.h"
#include "database.h"
#include "freezercore.h"
#include "schema.h"
#include "process.h"
#include "path.h"

#include "stonegui.h"

#include "classes.h"
#include "host.h"
#include "hoststatus.h"
#include "user.h"
#include "jobassignment.h"
#include "jobassignmentstatus.h"

#include "afcommon.h"
#include "joblistwidget.h"
#include "mainwindow.h"

#ifdef Q_OS_WIN
#include "windows.h"
#include <winerror.h>
#include <direct.h>
#include <Werapi.h>
#endif

#ifdef Q_OS_WIN
BOOL CALLBACK AFEnumWindowsProc( HWND hwnd, LPARAM otherProcessId )
{
	int pid = (int)otherProcessId;
	int winPid;
	GetWindowThreadProcessId( hwnd, (DWORD*)&winPid );
	LOG_5( "Found window with process id: " + QString::number( winPid ) );
	if( winPid == pid ) {
		LOG_5( "Raising window" );
		if( IsIconic( hwnd ) )
			OpenIcon( hwnd );
		SetForegroundWindow( hwnd );
	//	SetWindowPos( hwnd, HWND_TOP, 0, 0, 0, 0, SWP_ASYNCWINDOWPOS | SWP_NOMOVE | SWP_NOSIZE );
	}
	return true;
}

void enableCrashDumps() {
	HKEY hkey;
	const char * path = "Software\\Microsoft\\Windows\\Windows Error Reporting\\LocalDumps\\assfreezer.exe";
	if( RegCreateKeyExA( HKEY_LOCAL_MACHINE, path, 0, 0, 0, KEY_ALL_ACCESS | (isWow64() ? KEY_WOW64_64KEY : 0), 0, &hkey, 0 ) == ERROR_SUCCESS ) {
		DWORD val = 2;
		RegSetValueExA( hkey, "DumpType", 0, REG_DWORD, (const BYTE *)&val, sizeof(DWORD) );
		const char * abPath = "C:\\blur\\assfreezer\\";
		RegSetValueExA( hkey, "DumpFolder", 0, REG_SZ, (const BYTE *)abPath, strlen(abPath) + 1 );
		RegCloseKey( hkey );
	} else
		LOG_3( "Unable to create/open registry key HKEY_LOCAL_MACHINE\\" + QString::fromLatin1(path) );
}

HANDLE hMutex;
#endif

#ifdef Q_OS_WIN
extern "C" void initsip(void);
extern "C" void init_Stone(void);
extern "C" void init_Classes(void);
extern "C" void init_Stonegui(void);
extern "C" void init_Classesui(void);
extern "C" void initAssfreezer(void);
#endif

// Return the named attribute object from the named module.
// Returns a NEW reference(PyObject_GetAttrString)
PyObject * getModuleAttr(const char *module, const char *attr)
{
#if PY_VERSION_HEX >= 0x02050000
    PyObject *mod = PyImport_ImportModule(module);
#else
    PyObject *mod = PyImport_ImportModule(const_cast<char *>(module));
#endif

    if (!mod)
    {
        PyErr_Print();
        return 0;
    }

#if PY_VERSION_HEX >= 0x02050000
    PyObject *obj = PyObject_GetAttrString(mod, attr);
#else
    PyObject *obj = PyObject_GetAttrString(mod, const_cast<char *>(attr));
#endif

    Py_DECREF(mod);

    if (!obj)
    {
        PyErr_Print();
        return 0;
    }

    return obj;
}

void loadPythonPlugins()
{
	LOG_5( "Loading Python" );

#ifdef Q_OS_WIN
	/*
	 * This structure specifies the names and initialisation functions of
	 * the builtin modules.
	 */
	struct _inittab builtin_modules[] = {
		//{"sip", initsip},
		{"blur._Stone",init_Stone},
		{"blur._Classes",init_Classes},
		{"blur._Stonegui",init_Stonegui},
		{"blur._Classesui",init_Classesui},
		{"blur.Assfreezer", initAssfreezer},
		{NULL, NULL}
	};

	PyImport_ExtendInittab(builtin_modules);
#endif
	
	Py_Initialize();
	PyEval_InitThreads();
	
	// The following is required because blur is a regular python module, 
	// so to get the builtin(statically linked into assfreezer) binary
	// modules to work correctly we have to play some tricks.
	const char * builtinModuleImportStr = 
#ifdef Q_OS_WIN
		"import imp,sys\n"
		"class MetaLoader(object):\n"
		"\tdef __init__(self):\n"
		"\t\tself.modules = {}\n"

		"\t\tself.modules['blur._Stone'] = imp.load_module('blur._Stone',None,'',('','',imp.C_BUILTIN))\n"
		"\t\tself.modules['blur._Classes'] = imp.load_module('blur._Classes',None,'',('','',imp.C_BUILTIN))\n"
		"\t\tself.modules['blur._Stonegui'] = imp.load_module('blur._Stonegui',None,'',('','',imp.C_BUILTIN))\n"
		"\t\tself.modules['blur._Classesui'] = imp.load_module('blur._Classesui',None,'',('','',imp.C_BUILTIN))\n"
		"\t\tself.modules['blur.Assfreezer'] = imp.load_module('blur.Assfreezer',None,'',('','',imp.C_BUILTIN))\n"
		
		"\tdef find_module(self,fullname,path=None):\n"
//		"\t\tprint 'MetaLoader.find_module: ', fullname, path\n"
		"\t\tif fullname in self.modules:\n"
		"\t\t\treturn self.modules[fullname]\n"
//		"\t\tprint 'MetaLoader.find_module: Returning None'\n"
		"sys.meta_path.append(MetaLoader())\n"
		"import blur.logredirect\n"
		"blur.logredirect.RedirectOutputToLog()\n";
#else
		"import blur.logredirect\n"
		"blur.logredirect.RedirectOutputToLog()\n";
#endif
	PyRun_SimpleString(builtinModuleImportStr);

	LOG_5( "Loading python plugins" );

	PyObject * sys_path = getModuleAttr("sys", "path");

	if (!sys_path) {
		LOG_1( "Python Initialization Failure: Failed to get sys.path" );
		return;
	}
	
	
	QDir pluginsParentDir = QDir::current();
#ifndef Q_OS_WIN
	if( !pluginsParentDir.exists("plugins") )
		pluginsParentDir = QDir("/usr/share/assfreezer/");
#endif
	if( pluginsParentDir.exists("plugins") ) {
	
		QString pluginsParentPath = pluginsParentDir.absolutePath();
		// Convert the directory to a Python object with native separators.
#if QT_VERSION >= 0x040200
		pluginsParentPath = QDir::toNativeSeparators(pluginsParentPath);
#else
		pluginsParentPath = QDir::convertSeparators(pluginsParentPath);
#endif

#if PY_MAJOR_VERSION >= 3
		// This is a copy of qpycore_PyObject_FromQString().

		PyObject *dobj = PyUnicode_FromUnicode(0, pluginsParentPath.length());

		if (!dobj)
		{
			PyErr_Print();
			return;
		}

		Py_UNICODE *pyu = PyUnicode_AS_UNICODE(dobj);

		for (int i = 0; i < dir.length(); ++i)
			*pyu++ = pluginsParentPath.at(i).unicode();
#else
		PyObject *dobj = PyString_FromString(pluginsParentPath.toLatin1().constData());

		if (!dobj)
		{
			PyErr_Print();
			return;
		}
#endif

		// Add the directory to sys.path.
		int rc = PyList_Append(sys_path, dobj);
		Py_DECREF(dobj);
		Py_DECREF(sys_path);
		
		if (rc < 0)
		{
			PyErr_Print();
			return;
		}

		PyObject *plug_mod = PyImport_ImportModule("plugins");
		if (!plug_mod)
		{
			PyErr_Print();
			return;
		}
		Py_DECREF(plug_mod);
	}
	else
		LOG_1( "Unable to find python plugins folder" );
	
	PyEval_SaveThread();
}

int main( int argc, char * argv[] )
{
	int result=0;

#ifdef Q_OS_WIN
	enableCrashDumps();
	hMutex = CreateMutex( NULL, true, L"AssFreezerSingleProcessMutex");
	if (hMutex == NULL) {
		LOG_5( "Error: Couldn't create mutex, exiting" );
		return false;
	}
	if( GetLastError() == ERROR_ALREADY_EXISTS ) {
		LOG_5( "Error: Another process owns the mutex, exiting" );
		QList<int> pids;
		if( pidsByName( "assfreezer.exe", &pids ) ) {
			int otherProcessId = pids[0];
			if( otherProcessId == processID() ) {
				if( pids.size() < 2 )
					return false;
				otherProcessId = pids[1];
			}
			LOG_5( "Trying to find window with process pid of " + QString::number( otherProcessId ) );
			EnumWindows( AFEnumWindowsProc, LPARAM(otherProcessId) );
		}
		return false;
	}

	//QLibrary excdll( "exchndl.dll" );
	//if( !excdll.load() ) {
	//	qWarning( excdll.errorString().toLatin1().constData() );
	//}
	//disableWindowsErrorReporting( "assfreezer.exe" );
	WerRemoveExcludedApplication( L"assfreezer.exe", true );
	enableCrashDumps();
	SetErrorMode( SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX );

#endif

	QApplication a(argc, argv);

#ifdef Q_OS_WIN
	QString configFile = "assfreezer.ini";
#else
	QString configFile = "/etc/assfreezer.ini";
#endif

	initConfig( configFile );

#ifdef Q_OS_WIN
	bool skipPreferencesSave = false;
	QString userConfigPath = "h:/public/" + getUserName() + "/Blur/assfreezer.ini";
	QString userConfigBackupPath = QDir::homePath() + "/assfreezer.ini";
	initUserConfig( userConfigPath );
	if( !QFile::exists( userConfigPath ) ) {
		// Since H: is network path, we will warn the user if their usual config is not found
		QSettings regCheck( "HKEY_CURRENT_USER\\Software\\Assfreezer", QSettings::NativeFormat );
		if( regCheck.value( "LastUserConfigPath" ).toString() == userConfigPath && !QFile::exists( userConfigBackupPath ) ) {
			QMessageBox::warning( 0, "Assfreezer Preferences File Is Missing", "The previously saved preferences file for user " + getUserName() + " was not found at " + userConfigPath +
			", nor was the local back file found.\nAssfreezer will run with default preferences.  No preferences will be saved at the end of the session." );
			skipPreferencesSave = true;
		} else
			userConfig().readFromFile( userConfigBackupPath );
	}
#else
	initUserConfig( QDir::homePath() + "/.assfreezer" );
#endif

	initStone( argc, argv );
 	blurqt_loader();
	initStoneGui();
/*
//	qApp->setStyle( new TardStyle );
	QPalette p;// = qApp->palette();
	p.setColor( QPalette::Background, QColor( 150, 150, 150 ) );
	p.setColor( QPalette::Foreground, QColor( 255, 255, 255 ) );
	p.setColor( QPalette::Base, QColor( 170, 170, 170 ) );
	p.setColor( QPalette::Text, Qt::black );
	p.setColor( QPalette::Button, QColor( 120, 120, 120 ) );
	p.setColor( QPalette::ButtonText, QColor( 220, 220, 220 ) );
	p.setColor( QPalette::Highlight, QColor( 140, 140, 190 ) );
	p.setColor( QPalette::HighlightedText, QColor( 210, 210, 210 ) );
	p.setColor( QPalette::Light, QColor( 150, 150, 150 ) );
	p.setColor( QPalette::Midlight, QColor( 135, 135, 135 ) );
	p.setColor( QPalette::Dark, QColor( 60, 60, 60 ) );
	p.setColor( QPalette::Mid, QColor( 100, 100, 100 ) );
	p.setColor( QPalette::Shadow, QColor( 60, 60, 60 ) );

	QApplication::setPalette( p );
*/
	{
		JobList showJobs;
		bool showTime = false;
	
		for( int i = 1; i<argc; i++ ){
			QString arg( argv[i] );
			if( arg == "-h" || arg == "--help" )
			{
				LOG_5( QString("AssFreezer v") + VERSION );
				LOG_5( "Options:" );
				LOG_5( "-current-render" );
				LOG_5( "\tShow the current job that is rendering on this machine\n" );
				LOG_5( "-show-time" );
				LOG_5( "\tOutputs summary of time executed for all sql statement at program close\n" );
				LOG_5( "-user USER" );
				LOG_5( "\tSet the logged in user to USER: Requires Admin Privs" );
				LOG_5( stoneOptionsHelp() );
				return 0;
			}
			else if( arg == "-show-time" )
				showTime = true;
			else if( arg == "-current-render" ) {
				showJobs = Job::select( Job::c.Key.in(JobAssignment::c.Host == Host::currentHost() && JobAssignment::c.JobAssignmentStatus.between(ev_(1),ev_(3))) );
			}
			else if( arg == "-user" && (i+1 < argc) ) {
				QString impersonate( argv[++i] );
				if( User::hasPerms( "User", true ) ) // If you can edit users, you can login as anyone
					User::setCurrentUser( impersonate );
			}
			else if( arg == "-no-opengl" ) {
				options.mUseOpenGL=false;
			}
		}
		
		// Share the database across threads, each with their own connection
		Connection * threadConn = Connection::createFromIni( config(), "Database" );
		threadConn->connect( threadConn, SIGNAL( connectionLost2() ), ConnectionWatcher::connectionWatcher(), SLOT( connectionLost() ), Qt::DirectConnection );
		FreezerCore::setDatabaseForThread( blurDb(), threadConn );
		{
			QDir schemaDir = QDir::current();
			schemaDir.cd( "schemas" );
			if( schemaDir.exists() ) {
				QStringList schemas = schemaDir.entryList( QStringList() << "*.xml" );
				foreach( QString schemaFile, schemas )
					classesSchema()->mergeXmlSchema( schemaDir.path() + "/" + schemaFile );
			}
					
			MainWindow m;
			IniConfig cfg(userConfig());
			cfg.pushSection( "MainWindow" );
			QStringList fg = cfg.readString( "FrameGeometry", "" ).split(',');
			QString windowState = cfg.readString( "WindowState", "Normal" );
			cfg.popSection();
			if( fg.size()==4 ) {
				m.resize( QSize( fg[2].toInt(), fg[3].toInt() ) );
				m.move( QPoint( fg[0].toInt(), fg[1].toInt() ) );
			}
			if( windowState == "Maximized" )
				m.showMaximized();
			
			if( showJobs.size() )
				m.jobPage()->setJobList( showJobs );
			m.show();
			a.processEvents();
			loadPythonPlugins();
			result = a.exec();
			if( showTime ){
				Database * tm = Database::current();
				LOG_5( 			"                  Sql Time Elapsed" );
				LOG_5(			"|   Select  |   Update  |  Insert  |  Delete  |  Total  |" );
				LOG_5( 			"-----------------------------------------------" );
				LOG_5( QString(	"|     %1    |     %2    |    %3    |    %4    |    %5   |\n")
					.arg( tm->elapsedSqlTime( Table::SqlSelect ) )
					.arg( tm->elapsedSqlTime( Table::SqlUpdate ) )
					.arg( tm->elapsedSqlTime( Table::SqlInsert ) )
					.arg( tm->elapsedSqlTime( Table::SqlDelete ) )
					.arg( tm->elapsedSqlTime() )
				);
				LOG_5( 			"                  Index Time Elapsed" );
				LOG_5(			"|   Added  |   Updated  |  Incoming  |  Deleted  |  Search  |  Total  |" );
				LOG_5( 			"-----------------------------------------------" );
				LOG_5( QString(	"|     %1     |     %2    |    %3    |    %4   |    %5    |   %6    |\n")
					.arg( tm->elapsedIndexTime( Table::IndexAdded ) )
					.arg( tm->elapsedIndexTime( Table::IndexUpdated ) )
					.arg( tm->elapsedIndexTime( Table::IndexIncoming ) )
					.arg( tm->elapsedIndexTime( Table::IndexRemoved ) )
					.arg( tm->elapsedIndexTime( Table::IndexSearch ) )
					.arg( tm->elapsedIndexTime() )
				);
				tm->printStats();
			}
		}
	}
#ifdef Q_OS_WIN
	if( skipPreferencesSave )
		userConfig().setFileName( QString() );
	else
		// Make local backup.  shutdown() call below will save to network location
		userConfig().writeToFile( userConfigBackupPath );
#endif // Q_OS_WIN
	FreezerCore::shutdown();
	PyGILState_Ensure();
	Py_Finalize();
	delete Database::current();
	delete classesSchema();
	shutdown();
#ifdef Q_OS_WIN
	CloseHandle( hMutex );
#endif
	return result;
}

