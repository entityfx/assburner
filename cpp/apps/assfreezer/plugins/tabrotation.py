
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from blur.Assfreezer import *

startText = "Start Tab Rotation..."
stopText = "Stop Tab Rotation"

class RotateAction( QAction ):
	def __init__(self, mainWindow):
		QAction.__init__(self,startText,mainWindow)
		self.MainWindow = mainWindow
		self.Timer = QTimer(self)
		self.triggered.connect( self.toggle )
		self.Timer.timeout.connect( self.MainWindow.showNextView )
		self.Interval = 15
		
	def toggle(self):
		if self.text() == startText:
			(self.Interval,okay) = QInputDialog.getInt( self.MainWindow, 'Choose interval', 'Set the interval for tab rotation in seconds', self.Interval, 1)
			if okay:
				self.Timer.start(self.Interval * 1000)
				self.setText( stopText )
		elif self.text() == stopText:
			self.Timer.stop()
			self.setText( startText )

class TabRotationMenuPlugin( AssfreezerMenuPlugin ):
	def executeMenuPlugin(self, menu):
		# Ensure we only add one action, this could be called more than once
		for action in menu.actions():
			if isinstance(action,RotateAction):
				return
		menu.addAction( RotateAction(menu.parent().window()) )

AssfreezerMenuFactory.instance().registerMenuPlugin( TabRotationMenuPlugin(), "Assfreezer_Tools_Menu" )

