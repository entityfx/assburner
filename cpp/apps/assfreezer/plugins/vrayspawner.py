
import sys

from PyQt4.QtCore import Qt
from PyQt4.QtGui import *
from PyQt4.uic import loadUi

from blur.Stone import *
from blur.Classes import *
from blur.Stonegui import *
from blur.Assfreezer import *
#from blur.absubmit import Submitter

import os

menuEntryText = 'Submit Vray Spawner Job'

class VraySpawnerDialog( QDialog ):
	def __init__(self,parent,hostList):
		QDialog.__init__(self,parent)
		self.HostList = hostList
		print self.HostList.names().join(',')
		loadUi(os.path.join(os.path.dirname(__file__),"vrayspawnerdialog.ui"),self)
		# Force em to choose a project
		self.mProjectCombo.setShowSpecialItem(False)
		self.mButtonBox.accepted.connect(self.submit)
		self.mMaxVersionCombo.setColumn('service')
		self.mMaxVersionCombo.setItems( Service.select(Service.c.Service.regexSearch('^MAX')) )
		
	def submit(self):
		print 'VraySpawnerDialog.submit'
		job = JobVraySpawner()
		job.setJobType(JobType('VraySpawner'))
		job.setName( self.mJobNameEdit.text() )
		job.setMaxHosts( self.mMaxHostsSpin.value() )
		job.setProject( self.mProjectCombo.project() )
		job.setPriority( self.mPrioritySpin.value() )
		job.setAutoFinishOnIdle( self.mAutoFinishCpuCheck.isChecked() )
		job.setPacketType('preassigned')
		job.setUser(User.currentUser())
		if self.mAutoFinishElapsedCheck.isChecked():
			job.setAutoFinishAfterDuration( self.mAutoFinishElapsed.interval() )
		job.setStatus('submit')
		job.commit()
		tasks = JobTaskList()
		for host in self.HostList:
			tasks.append( JobTask().setHost(host).setJob(job).setStatus('new') )
		tasks.commit()
		js = JobService()
		js.setJob(job)
		js.setService(self.mMaxVersionCombo.current())
		js.commit()
		job.setStatus('verify')
		job.commit()

class VraySpawnerSubmitMenuPlugin( AssfreezerMenuPlugin ):
	def executeMenuPlugin(self, menu):
		action = QAction( "Assign Vray Spawner", menu )
		action.triggered.connect(lambda: self.submit(menu))
		self.appendAction( menu, "Create Job", action )
		
	def submit(self,menu):
		vsd = VraySpawnerDialog(menu.parent(),menu.mHostList.hostTreeView().selection())
		vsd.show()

menuPlugin = VraySpawnerSubmitMenuPlugin()
AssfreezerMenuFactory.instance().registerMenuPlugin( menuPlugin, "AssfreezerHostMenu" )
