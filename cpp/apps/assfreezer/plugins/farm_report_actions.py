

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from blur.Assfreezer import *
import sip

class FarmReportAction( QAction ):
	def __init__(self, parent, parentWindow):
		QAction.__init__(self,"Reports...",parent)
		self.ParentWindow = parentWindow
		self.triggered.connect( self.showReportsDialog )
		
	def showReportsDialog(self):
		from farm_stats.reportselectordialog import ReportSelectorDialog
		rsd = ReportSelectorDialog(self.ParentWindow)
		#sip.transferto(rsd,None)
		rsd.show()

class FarmReportMenuPlugin( AssfreezerMenuPlugin ):
	def executeMenuPlugin(self, menu):
		# Ensure we only add one action, this could be called more than once
		for action in menu.actions():
			if isinstance(action,FarmReportAction):
				return
		self.Action = FarmReportAction(menu,menu.parent().window())
		menu.addAction( self.Action )

plugin = FarmReportMenuPlugin()
AssfreezerMenuFactory.instance().registerMenuPlugin( plugin, "Assfreezer_Tools_Menu" )

