
import os, sys

from PyQt4.QtCore import Qt
from PyQt4.QtGui import *

from blur.Assfreezer import *

class HostIPMIMenuPlugin( AssfreezerMenuPlugin ):
	MenuName = "IPMI"
	
	def executeMenuPlugin(self, menu):
		sepCount = 0
		insertBeforeAction = None
		for action in menu.actions():
			if action.text() == self.MenuName:
				return
			if action.isSeparator():
				sepCount+=1
			elif sepCount == 2:
				insertBeforeAction = action
				break
		
		sub = menu.addMenu( self.MenuName )
		if insertBeforeAction:
			menu.insertMenu( insertBeforeAction, sub )
		sub.addAction( "Power On", lambda: self.action('start') )
		sub.addAction( "Power Off", lambda: self.action('stop') )
		sub.addAction( "Soft Reboot", lambda: self.action('reboot') )
		sub.addAction( "Power Cycle", lambda: self.action('powerCycle') )
		self.Parent = menu.parent()
	
	def hosts(self):
		return self.Parent.mHostListWidget.hostTreeView.selection()
	
	def action(self,action):
		for h in self.hosts():
			try:
				getattr(h.ipmi(),action)()
			except: pass
		
plugin = HostIPMIMenuPlugin()
AssfreezerMenuFactory.instance().registerMenuPlugin( plugin, "AssfreezerHostMenu" )

