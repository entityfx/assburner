
from PyQt4.QtGui import QMainWindow, QSplitter, QTabWidget, QSizePolicy, QDockWidget
from projectweightgui import ScheduleView
from resourceview import ResourceTree
from projectview import WorkloadTree
from main import *

class FarmPlannerWindow(QMainWindow):
	def __init__(self,parent=None):
		QMainWindow.__init__(self,parent)
		#loadUi(os.path.join( os.path.dirname(__file__), "projectweightscheduledialogui.ui" ),self)
		
		def addDocked( name, func, area ):
			dock = QDockWidget(name,self)
			dock.setWidget(func(dock))
			self.addDockWidget(area,dock)
		
		self.ScheduleView = ScheduleView(self)
		self.ScheduleView.setSizePolicy( QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding )
		self.setCentralWidget(self.ScheduleView)
		
		def createHostTree(parent):
			self.ResourceTree = ResourceTree(resourcepool(),parent)
			return self.ResourceTree
		addDocked( 'Hosts', createHostTree, Qt.LeftDockWidgetArea )

		def createProjectTree(parent):
			self.WorkloadTree = WorkloadTree(workpool(),parent)
			#self.WorkloadTree.selectedProjectsChanged.connect( self.ScheduleView.Scene.setProjectsShown )
			self.WorkloadTree.selectedWorkloadsChanged.connect( self.ScheduleView.Scene.setSelectedWorkloads )
			self.WorkloadTree.AddProjectWeightRowAction.triggered.connect( self.addProjectWeightRow )
			self.WorkloadTree.AddProjectRenderRowAction.triggered.connect( self.addProjectRenderRow )

			return self.WorkloadTree
		addDocked( 'Projects', createProjectTree, Qt.BottomDockWidgetArea )
		
		mb = self.menuBar()
		#self.FileMenu = mb.addMenu( 'File' )
		#self.ToolsMenu = mb.addMenu( 'Tools' )
		#self.OptionsMenu = mb.addMenu( 'Options' )
		#self.ViewMenu = mb.addMenu( 'View' )
		
		self.showMaximized()
		#self.ProjectCombo.setShowSpecialItem( False )
		#self.ProjectCombo.setStatusFilters( ProjectStatusList(ProjectStatus.recordByName( "Production" )) )
	
	def addProjectWeightRow(self):
		for pr in self.WorkloadTree.selection():
			self.ScheduleView.Scene.addProjectWeightRow(pr.project())

	def addProjectRenderRow(self):
		for pr in self.WorkloadTree.selection():
			self.ScheduleView.Scene.addProjectRenderRow(pr.project())
	
	def addProject(self):
		project = self.ProjectCombo.project()
		if not project in self.ScheduleView.Scene.WeightManager.projects():
			self.ScheduleView.Scene.WeightManager.addProject(project)
	
	def checkAlertOverWeight(self):
		if self.isOverWeight():
			QMessageBox.critical(self,'Combined weighting over 100%','Combined weightings over 100% are not allowed.')
			return True
		return False
		
	def isOverWeight(self):
		return self.ScheduleView.scene().TotalWeightRow.OverWeight


if __name__=="__main__":
	from PyQt4.QtGui import QApplication
	from PyQt4 import QtOpenGL
	app = QApplication([])
	f = QtOpenGL.QGLFormat.defaultFormat()
	f.setSampleBuffers(True)
	QtOpenGL.QGLFormat.setDefaultFormat(f)
	import blur.quickinit
	mainWindow = FarmPlannerWindow()
	mainWindow.show()
	app.exec_()
