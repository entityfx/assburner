
import numpy
import itertools
import bisect
from PyQt4.QtCore import QDate

def enum(*sequential, **named):
	enums = dict(zip(sequential, range(len(sequential))), **named)
	return type('Enum', (), enums)

# Represents a series of values, one per Interval
class Series(object):
	Intervals = enum( 'Hour', 'Day', 'Week', 'Month' )
	# This defines how a series is converted to different interval types
	# Sum series are divided or multiplied when converted to smaller or larger
	# intervals, while averages are kept the same, or set to an average of the pieces
	# respectively.
	Modes = enum( 'Sum', 'Avg' )
	
	def __init__(self,startMarker,endMarker,mode=Modes.Sum,data=None,constant=0.0):
		self.Start = None if startMarker is None else self.normalizeMarker(startMarker)
		self.End = None if endMarker is None else self.normalizeMarker(endMarker)
		self.AbsStart = None if self.Start is None else self.toAbsolute(self.Start)
		self.Data = None
		self.Constant = constant
		self.Mode = mode
		self.Size = None
		# If we have a start and an end, then verify they are proper, otherwise set to null
		if self.Start is not None and self.End is not None:
			self.Size = self.toAbsolute(self.End) - self.toAbsolute(self.Start) + 1
			if self.Size > 0:
				self.Data = data
			else:
				self.Data = self.Constant = self.Start = self.End = self.Size = None
		else:
			self.Size = None
	
	def getConstant(self,key):
		if isinstance(key,slice):
			start,stop,step = key.indices(self.Size)
			if step != 1:
				raise KeyError()
			return numpy.repeat(self.Constant,stop-start)
		if isinstance(key,(int,long)):
			if key >= 0 and key < self.Size:
				return self.Constant
			return 0.0
		key = self.toAbsolute(self.normalizeMarker(key))
		start = self.AbsStart
		end == self.toAbsolute(self.End)
		if start is not None and key < start:
			return 0.0
		if end is not None and key > end:
			return 0.0
		return self.Constant

	# Slices are only supported if it's a closed series and the slice falls inside the actual data
	def __getitem__(self,key):
		# Check for null
		if self.Data is None:
			return self.getConstant(key)
		
		if not isinstance(key,(int,long,slice)):
			key = self.toAbsolute(self.normalizeMarker(key)) - self.AbsStart
		try:
			return self.Data[key]
		except IndexError:
			return 0.0
	
	def __setitem__(self,key,val):
		if not isinstance(key,(int,long,slice)):
			key = self.toAbsolute(self.normalizeMarker(key)) - self.AbsStart
		try:
			self.Data[key] = val
		except IndexError:
			return None
	
	@classmethod
	def null(cls):
		return cls(None,None)
	
	def isNull(self):
		return self.Size is None
	
	def isEmpty(self):
		return self.isNull() or self.Data is None and self.Constant is None
	
	def fillConstant(self,constant):
		if self.Size:
			self.Data = numpy.repeat(constant,self.Size)
		
	def fillZeros(self):
		if self.Size:
			self.Data = numpy.zeros(self.Size)
		
	def fillOnes(self):
		if self.Size:
			self.Data = numpy.ones(self.Size)

	def toInterval(self,interval):
		if self.Interval == interval:
			return self
		elif self.Interval > interval:
			raise NotImplemented()
		else:
			raise NotImplemented()

	def sum(self,data,offset):
		if self.Size and data is not None:
			self.Data[offset:offset+data.size] += data
	
	def __add__(self,other):
		return sum((self,other))
	
	def __div__(self,other):
		assert(self.Interval == other.Interval)
		assert(self.Start == other.Start)
		assert(self.End == other.End)
		return type(self)(self.Start,self.End,self.Mode, self.Data / other.Data)
	
	@classmethod
	def shiftMarker(cls,marker,offset):
		return cls.fromAbsolute(cls.toAbsolute(marker)+offset)
	
	def movingAverage(self,cnt):
		ret = type(self)(self.Start,self.End)
		# Constant moving average is the same constant
		if self.Data is None and self.Constant is not None:
			ret.Constant = self.Constant
			return ret
		ret.fillZeros()
		for i in range(cnt):
			ret.Data[i:] += self.Data[0:self.Size-i]
		ret.Data /= float(cnt)
		return ret
			
	# Chops the series to fit into start-end, but does not expand the series
	def trim(self,start,end):
		newStart = max(start,self.Start)
		newEnd = min(end,self.End)
		if newStart > newEnd:
			return type(self)(None,None,self.Mode)
		ns_abs = self.toAbsolute(newStart)
		offset = ns_abs - self.toAbsolute(self.Start)
		if self.Data is not None:
			return type(self)(newStart,newEnd,self.Mode,self.Data[offset:offset + self.toAbsolute(newEnd) - ns_abs + 1])
		elif self.Constant:
			return type(self)(newStart,newEnd,self.Mode,constant = self.Constant)
	
	# Chops and expands with zeros to exactly fit start-end
	def fit(self,start,end):
		if start > self.End or end < self.Start:
			return type(self)(start,end,self.Mode)
		ret = type(self)(start,end,self.Mode)
		ret.fillZeros()
		offset = self.toAbsolute(start) - self.toAbsolute(self.Start)
		roffset = 0
		if offset < 0:
			roffset = -offset
			offset = 0
		if self.Data is not None:
			ret.sum(self.Data[offset:offset+ret.Size-roffset],roffset)
		elif self.Constant:
			ret.Data[roffset:min(roffset+self.Size,ret.Size)] += self.Constant
		return ret
	
	def firstNonZero(self):
		if self.Data is not None:
			nzIndexes = numpy.nonzero(self.Data > 0)[0]
			if not len(nzIndexes):
				return self.Start
			idx = nzIndexes[0]
			return self.fromAbsolute(self.toAbsolute(self.Start)+idx)
		return self.Start

	def lastNonZero(self):
		if self.Data is not None:
			nzIndexes = numpy.nonzero(self.Data > 0)[0]
			if not len(nzIndexes):
				return self.End
			idx = nzIndexes[-1]
			return self.fromAbsolute(self.toAbsolute(self.Start)+idx)
		return self.End
	
	def range(self):
		try:
			return self.normalizeMarker(self.Start), self.normalizeMarker(self.End)
		except:
			print self.__dict__
			raise
	
	@staticmethod
	def fromIntervalAndRange(interval,range,mode):
		iv = Series.Intervals
		toCls = {iv.Hour : HourSeries, iv.Day : DaySeries, iv.Week : WeekSeries, iv.Month : MonthSeries}
		return toCls[interval](range[0],range[1],mode)

	@staticmethod
	def normalizeMarker(marker):
		return marker

	@staticmethod
	def toAbsolute(marker):
		return marker is not None and marker or 0
	
class HourSeries(Series):
	Interval = Series.Intervals.Hour
	@staticmethod
	def normalizeMarker(marker):
		d, t = marker.date(), marker.time()
		return QDateTime(QDate(d.year(), d.month(), d.day()), QTime(t.hour(),0))
	@staticmethod
	def toAbsolute(marker):
		return QDateMixin.toAbsolute(marker.date()) * 24 + marker.time().hour()
	
class DaySeries(Series):
	Interval = Series.Intervals.Day
	@classmethod
	def toAbsolute(cls,marker):
		n = cls.normalizeMarker(marker)
		return n.toJulianDay()
	@staticmethod
	def fromAbsolute(abs):
		return QDate.fromJulianDay(abs)
	@staticmethod
	def normalizeMarker(marker):
		if marker.isNull():
			return None
		return marker

def yearWeekToDate(week,year):
	# These two lines should get us within a week
	date = QDate(year,1,1).addDays((week-1) * 7)
	date = date.addDays(1-date.dayOfWeek())
	# Then a single call to Qt's efficient weekNumber will get to the correct one
	check = date.weekNumber()
	if (week,year) > check:
		date = date.addDays(7)
	elif (week,year) < check:
		date = date.addDays(-7)
	return date

# Julian years, ie starting with 1
yearsWith53Weeks = [4, 9, 15, 20, 26, 32, 37, 43, 48,
54, 60, 65, 71, 76, 82, 88, 93, 99,
105, 111, 116, 122, 128, 133, 139, 144,
150, 156, 161, 167, 172, 178, 184, 189, 195,
201, 207, 212, 218, 224, 229, 235, 240, 246,
252, 257, 263, 268, 274, 280, 285, 291, 296,
303, 308, 314, 320, 325, 331, 336, 342, 348,
353, 359, 364, 370, 376, 381, 387, 392, 398]

def julianWeekNumber(week,year):
	# Julian Calendar starts at year 1
	year -= 1
	# 52 Weeks in every year, plus another for leap years
	# 71 leep years per 400 years starting at julian year 1
	ret = year * 52 + (year // 400) * 71
	# Then add leep years in the current 400 year block
	ret += bisect.bisect_left(yearsWith53Weeks,(ret % 400) + 1)
	return ret + week - 1

class WeekSeries(Series):
	Interval = Series.Intervals.Week
	@staticmethod
	def normalizeMarker(marker):
		return yearWeekToDate(*marker.weekNumber())
	@staticmethod
	def toAbsolute(marker):
		return julianWeekNumber(*marker.weekNumber())

class MonthSeries(Series):
	Interval = Series.Intervals.Month
	@staticmethod
	def normalizeMarker(marker):
		return QDate(marker.year(),marker.month(),1)
	@staticmethod
	def toAbsolute(marker):
		return marker.year() * 12 + marker.month()

# If interval is not None each series is converted to that interval,
# else each is converted to the shorted interval
def normalizeIntervals(series,interval = None):
	to_interval = interval or min([s.Interval for s in series])
	return [s.toInterval(to_interval) for s in series]

# Returns a sum of all the series.  Series are normalized
# before summing.
# Series need not share the same time range.
# Open series do not contribute to the time range expansion
def sum(series,interval=None):
	validSeries = [s for s in series if not s.isNull()]
	if not validSeries:
		if series:
			return type(series[0]).null()
		return Series.null()
	series = normalizeIntervals(validSeries,interval)
	ranges = [s.range() for s in series if not s.isNull()]
	flat = list(itertools.chain(*ranges))
	rrange = min(flat), max(flat)
	result = Series.fromIntervalAndRange(series[0].Interval,rrange,series[0].Mode)
	result.fillZeros()
	for i in range(len(series)):
		s = series[i]
		r = ranges[i]
		offset = s.toAbsolute(r[0]) - s.toAbsolute(rrange[0])
		if s.Data is not None:
			result.Data[offset:offset+s.Data.size] += s.Data
		elif s.Constant is not None:
			l = s.toAbsolute(r[1]) - s.toAbsolute(r[0]) + 1
			result.Data[offset:offset+l] += s.Constant
	return result

if __name__ == "__main__":
	from PyQt4.QtCore import QDate
	print yearWeekToDate(2014,1)
	today = QDate.currentDate()
	ago1 = today.addDays(-10)
	ago2 = today.addDays(-20)
	s1 = DaySeries(ago1,today)
	s1.fillConstant( 2.0 )
	s2 = DaySeries(ago2, today)
	s2.fillConstant( 3.0 )
	s3 = sum([s1,s2])
	print s3.Data
	s4 = s3.trim( ago2.addDays(1), today.addDays(1) )
	print s4.Data
	print s1.Size, s2.Size, s3.Size, s4.Size
	s5 = s3.fit( ago1.addDays(2), today.addDays(3) )
	print s5.Data
	
	ma = DaySeries( ago1, today, data = numpy.array( [ 1.,2.,3.,4.,5.,6.,5.,4.,3.,2.,1.] ) )
	print ma.Data
	print ma.movingAverage( 3 ).Data
	
