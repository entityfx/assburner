
import sys, math

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4.QtOpenGL import *

from main import profile

class Colors(object):
	def __init__(self):
		def cpSetAlpha(col,alpha):
			ret = QColor(col)
			ret.setAlpha(alpha)
			return ret
		# TODO - Read/Write from Ini
		self.Outline = QColor( 120, 120, 120 )
		self.Inline = QColor( 215, 215, 215 )
		#self.Weekend = QColor(Qt.gray)
		#self.Weekday = QColor(Qt.green)
		self.Weekend = QColor(Qt.gray).lighter(150)
		self.Weekday = QColor(Qt.transparent) #QColor(Qt.white)
		self.LabelText = QColor( 0, 0, 0, 140 )
		self.MonthHeaderBackground = QColor( 200, 200, 200 )
		self.ProjectWeight = QColor( 20, 20, 225, 200 )
		self.ProjectWeightOverage = QColor( 225, 0, 0, 220 )
		self.ProjectWeightLine = QColor( 230, 100, 100, 190 )
		self.ProjectWeightROLine = QColor( 50, 50, 50, 190 )
		self.TimelineCurrentPen = Qt.NoPen
		self.TimelineCurrentBrush = QColor( 0, 200, 0, 128 )
		self.TimelineHandlePen = QColor( 0, 255, 0 )
		self.TimelineHandleBrush = cpSetAlpha(self.TimelineHandlePen,128)
		
		self.DayWidthCutoff = 50
		self.MonthWidthCutoff = 50
		
colors = Colors()


clamp = lambda x, l, u: min(u,max(l,x))
swap = lambda a, b: (b,a)

def brushForDate(date):
	col = None
	if date.dayOfWeek() > 5:
		col = colors.Weekend
	else:
		col = colors.Weekday
	if date == QDate.currentDate():
		col = col.dark(140)
	return col

def startOfYear(date):
	return QDate(date.year(),1,1)

def nextYear(date):
	return QDate(date.year()+1,1,1)

def nextQuarter(date):
	qua = date.month() / 3 + 1
	year = date.year()
	if qua >= 4:
		qua = 0
		year += 1
	return QDate(year, qua * 3 + 1, 1)
	
class RowUtilityMixin(object):
	def dateTimeToX(self,dateTime):
		if dateTime < QDateTime(self.DateStart):
			return 0
		return QDateTime(self.DateStart).secsTo(dateTime) * self.DayWidth / (60.0 * 60.0 * 24.0)

	def dateToX(self,date):
		return min(self.DateStart.daysTo(date) * self.Width / float(self.DayCount), self.Width)

	def dateMidDayToX(self,date):
		dateTime = QDateTime(date).addSecs( 60 * 60 * 12 )
		return self.dateTimeToX( dateTime )

	def xToDate(self,x):
		return self.DateStart.addDays( x * self.DayCount / self.Width )

	def closestDate(self, x):
		return self.xToDate(x)

	def valueToY(self,value,scaleMax):
		return self.Height - ((value * self.Height) / float(scaleMax))

	def createLabel(self, text, pos = (0.0,0.0)):
		self.LabelItem = QGraphicsTextItem(text,self)
		self.LabelItem.setPos( QPointF( pos[0], pos[1] ) )
		ptf = self.LabelItem.font()
		ptf.setPixelSize( 32 )
		self.LabelItem.setFont( ptf )
		self.LabelItem.setDefaultTextColor( colors.LabelText )

	def size(self):
		return QSize(self.Width, self.Height+2)
		
	def boundingRect(self):
		return QRectF(QPointF(0,0),QSizeF(self.size()))

	def row(self):
		return self
	
	def timelineBrowser(self):
		return self.scene().TimelineBrowser
	
	def header(self):
		return self.scene().Header
	
class Scale(QGraphicsItemGroup):
	Left = 1
	Right = 2
	
	def __init__(self,parent,units,side=Right,divider=None):
		QGraphicsItemGroup.__init__(self,parent)
		self.Side = side
		self.Units = units
		self.ScaleMax = 1.0
		self.ScaleMakers = []
		self.Divider = divider
		parent.addScale(self)
	
	def setScaleMax(self,scaleMax):
		self.ScaleMax = scaleMax
		self.reset()
	
	def displayValue(self):
		val = self.ScaleMax
		if self.Divider:
			val /= self.Divider
		return ("%.2f" % val).rstrip('0').rstrip('.')
	
	def recalculate(self):
		sm = 1.0
		for maker in self.ScaleMakers:
			sm = max(sm, maker.maxValue())
		self.ScaleMax = sm
	
	def reset(self):
		for item in self.childItems():
			self.scene().removeItem(item)
		self.MaxLabel = QGraphicsTextItem( "%s %s" % (self.displayValue(), self.Units), self )
		if self.Side == Scale.Right:
			self.setPos( self.parentItem().boundingRect().width() - self.MaxLabel.boundingRect().width(), 0 )

class Legend(QGraphicsItemGroup):
	def __init__(self,parent):
		QGraphicsItemGroup.__init__(self,parent)
		self.setZValue(1)
		self.divider = None
		
	def setItems(self,items,divider):
		for item in self.childItems():
			self.scene().removeItem(item)
		pos = 0
		valueItems = []
		maxLeft = 0.0
		for i, item in enumerate(items):
			y = i * 15
			name, pen, brush, value = isinstance(item,tuple) and item or (item,None,None,None)
			textItem = QGraphicsSimpleTextItem( name, self )
			textItem.setPos( 20.0, y )
			maxLeft = max(20.0 + textItem.boundingRect().width(),maxLeft)
			if value is not None:
				if divider:
					value /= divider
				valueItem = QGraphicsSimpleTextItem( ("%.2f" % value).rstrip('0').rstrip('.'), self )
				valueItem.setY( y )
				valueItems.append(valueItem)
				colorBoxItem = QGraphicsRectItem( 0, 0, 12, 12, self )
				if pen:
					colorBoxItem.setPen(pen)
				if brush:
					colorBoxItem.setBrush(brush)
				colorBoxItem.setPos( 0.0, y + 2.0 )
		maxLeft += 15
		for valueItem in valueItems:
			valueItem.setX( maxLeft )
	
class RatioScale(Scale):
	def __init__(self,parent,ratioOfScale,units,side=Scale.Left):
		Scale.__init__(self,parent,units,side)
		self.RatioOfScale = ratioOfScale
	
	def displayValue(self):
		return "%.2f" % (self.ScaleMax / self.RatioOfScale.ScaleMax)

class RowBase(RowUtilityMixin,QGraphicsItem):
	def __init__(self,startDate=None,parentItem=None, height=300, name=None):
		QGraphicsItem.__init__(self,parentItem)
		self.Height = height
		self.Name = name
		self.Width = 1
		if startDate is None:
			startDate = QDate.currentDate()
		self.DateStart = startDate
		self.DateEnd = startDate.addDays(1)
		self.DayWidth = 1
		self.DayCount= 1
		self.setAcceptHoverEvents(True)
		
	def setWidth(self,width):
		self.Width = width
		self.prepareGeometryChange()
		self.resetLayout()
	
	def dateRange(self):
		return (self.DateStart, self.DateEnd)
	
	def setDateRange(self,dateStart,dateEnd):
		self.DateStart = dateStart
		self.DateEnd = dateEnd
		self.DayCount = self.DateStart.daysTo(self.DateEnd)
		self.resetLayout()
	
	def resetLayout(self):
		self.DayWidth = self.Width / self.DateStart.daysTo(self.DateEnd)
		self.reset(resetAllUsers=True)
		self.update()

	def reset(self,resetAllUsers=False):
		pass
	
	def dayCount(self):
		return self.DateStart.daysTo(self.DateEnd) + 1

	def sizeHint(self,which,constraint):
		if which == Qt.MinimumSize:
			width = 50
		else:
			if constraint.isValid():
				width = constraint.width()
			else:
				width = 9999
		return QSizeF(width, self.Height+2)
	
class GraphRow(RowBase):
	def __init__(self,startDate=None,parentItem=None, height=200, name=None):
		RowBase.__init__(self,startDate,parentItem,height,name)
		self.Scales = []
		self.Legend = Legend(self)
		self.Legend.setPos( 5, 5 )
		self.Legend.setItems(self.Name and [self.Name] or [],None)
		self.NeedReset = True
		
	def addScale(self,scale):
		self.Scales.append(scale)
		if scale.Side == Scale.Left:
			self.Legend.setY( 22 )
	
	def users(self):
		return [gi for gi in self.childItems() if isinstance(gi,RowUser)]
	
	def hoverLeaveEvent( self, event ):
		self.Legend.setItems(self.Name and [self.Name] or [],None)
		QGraphicsItem.hoverLeaveEvent(self,event)
		
	def hoverMoveEvent( self, event ):
		pos = event.pos()
		date = self.xToDate(pos.x())
		vals = []
		vals.append( '%sDate: %s' % (self.Name and self.Name + ' ' or '', date.toString()) )
		for user in self.users():
			for v in user.values(date):
				if v[3] is not None and v[3] > 0.0:
					vals.append( v )
		self.Legend.setItems(vals,self.Scales and self.Scales[0].Divider or None)
		QGraphicsItem.hoverMoveEvent(self,event)

	def scheduleReset(self):
		self.NeedReset = True
		self.scene().scheduleRecalcCheck()

	def checkReset(self, force=False):
		if self.NeedReset or force:
			self.reset()
			self.NeedReset = False
	
	def reset(self,resetAllUsers=False):
		for scale in self.Scales:
			scale.recalculate()
		for scale in self.Scales:
			scale.reset()
		for user in self.users():
			user.checkReset(force=resetAllUsers)

	def paint(self, painter, option, widget):
		painter.setRenderHint( QPainter.Antialiasing, False )
		painter.setBrush(Qt.NoBrush)
		
		painter.setPen(colors.Inline)
		for x in self.header().lines():
			painter.drawLine( x, 0, x, self.Height )

		painter.setPen(colors.Outline)
		painter.drawRect( QRect( 0, 0, self.DayWidth * (self.DateStart.daysTo(self.DateEnd)+1), self.Height ) )
	
class RowUser(RowUtilityMixin,QGraphicsItemGroup):
	def __init__(self,parentRow):
		QGraphicsItemGroup.__init__(self,parentRow)
		self.ParentRow = parentRow
		self.NeedReset = True
		QTimer.singleShot( 0, self.reset )
		
	@property
	def Height(self):
		return self.ParentRow.Height
	
	@property
	def DateStart(self):
		return self.ParentRow.DateStart
	
	@property
	def DateEnd(self):
		return self.ParentRow.DateEnd
	
	@property
	def DayWidth(self):
		return self.ParentRow.DayWidth

	@property
	def Width(self):
		return self.ParentRow.Width
	
	def values(self,date):
		return None
	
	def row(self):
		return self.ParentRow
	
	def dayCount(self):
		return self.ParentRow.dayCount()
	
	def size(self):
		return self.ParentRow.size()
	
	def boundingRect(self):
		return self.ParentRow.boundingRect()
	
	def scheduleReset(self):
		self.NeedReset = True
		self.row().scheduleReset()

	def checkReset(self,force=False):
		if self.NeedReset or force:
			self.reset()
			self.NeedReset = False

	def reset(self,resetAllUsers=False):
		pass
	
class Header(RowBase):
	def __init__(self,startDate=None,parentItem=None):
		RowBase.__init__(self,startDate,parentItem,height=25)
		self.Lines = []
		self.LastLineDates = None
		self.LastLineDayWidth = None
		
	def lines(self):
		if self.LastLineDates != self.dateRange() or self.DayWidth != self.LastLineDayWidth:
			self.generateLines()
		return self.Lines
	
	def generateLines(self):
		self.paint(None,None,None)
		
	def paint(self, painter, option, widget):
		if painter:
			def drawBox( x, width, brush ):
				painter.setPen( colors.Outline )
				painter.setBrush( brush )
				painter.drawRect( QRect( x, 0, width, self.Height ) )
				painter.setPen(Qt.black)
				
			def drawDayBox(x,date):
				drawBox( x, self.DayWidth, brushForDate(date) )
				if date.dayOfWeek() == 1:
					painter.drawText( QRect( x, 0, self.DayWidth, 0 ), Qt.AlignCenter | Qt.AlignVCenter, str(date.day()) )
			
			def drawWeekBox(x,width,date):
				drawBox( x, width, colors.MonthHeaderBackground )
				text = str(date.weekNumber()[0])
				painter.drawText( QRect( x, 0, width, self.Height ), Qt.AlignCenter | Qt.AlignVCenter, text )
			
			def drawMonthBox(x,width,date,showYear):
				drawBox( x, width, colors.MonthHeaderBackground )
				if x == 0 and width < 40: return True
				if width > 80:
					text = QDate.longMonthName(date.month())
				else:
					text = QDate.shortMonthName(date.month())
				if showYear or date.month() == 1:
					if width > 80:
						text += ", " + str(date.year())
					else:
						text = str(date.year())
				painter.drawText( QRect( x, 0, width, self.Height ), Qt.AlignCenter | Qt.AlignVCenter, text )
				return False
			
			def drawQuarterBox(x,width,date,showYear):
				drawBox( x, width, colors.MonthHeaderBackground )
				if x == 0 and width < 40: return True
				quarter = (date.month() / 3)+1
				text = 'Q%i' % quarter
				if showYear or quarter == 1:
					if width > 80:
						text += ", " + str(date.year())
					else:
						text = str(date.year())
				painter.drawText( QRect( x, 0, width, self.Height ), Qt.AlignCenter | Qt.AlignVCenter, text )
				return False
		
			painter.setRenderHint( QPainter.Antialiasing, False )
		else:
			drawDayBox = drawWeekBox = drawMonthBox = drawQuarterBox = lambda *args: None
		
		self.Lines = []
		firstText = True
		date = self.DateStart
		while date <= self.DateEnd:
			x = self.dateToX(date)
			# Draw days
			if self.DayWidth > colors.DayWidthCutoff:
				date = date.addDays(1)
				drawDayBox( x, date )
			
			# Draw Weeks
			#elif self.DayWidth * 7 > 50:
				#if date.dayOfWeek() > 1:
					#date = date.addDays(1-date.dayOfWeek())
				#nextDate = date.addDays(7)
				#drawWeekBox( x, self.dateToX(nextDate) - x + 1, date )
				#date = nextDate
			
			# Draw Months
			elif self.DayWidth * 30 > colors.MonthWidthCutoff:
				if date.day() > 1:
					date = date.addDays(1-date.day())
				nextDate = date.addMonths(1)
				width = self.dateToX(nextDate) - x + 1
				firstText = drawMonthBox(x, width, date, firstText)
				date = nextDate
			
			# Quarters
			else:
				nextDate = nextQuarter(date)
				width = self.dateToX(nextDate) - x + 1
				firstText = drawQuarterBox(x, width, date, firstText)
				date = nextDate
				
			if x > 0:
				self.Lines.append(x)
				
		self.LastLineDates = self.dateRange()
		self.LastLineDayWidth = self.DayWidth

class TimelineBrowser(RowUtilityMixin,QGraphicsItem):
	class MoveType:
		Null, Current, Start, End = range(4)
	
	def __init__(self,parent,dateStart,dateEnd,currentStart,currentEnd):
		QGraphicsItem.__init__(self,parent)
		self.Width = 0
		self.Height = 45
		self.MoveStartPos = None
		self.MoveOffset = None
		self.MoveType = TimelineBrowser.MoveType.Null
		self.AttachedRows = []
		self.setTimelineRange(dateStart,dateEnd)
		self.setCurrentRange(currentStart,currentEnd)
		self.setAcceptHoverEvents(True)
	
	def addAttachedRow( self, row ):
		self.AttachedRows.append(row)
		row.setDateRange(self.CurrentDateStart,self.CurrentDateEnd)
	
	def removeAttachedRow( self, row ):
		self.AttachedRows.remove(row)

	def currentRange(self):
		return (self.CurrentDateStart,self.CurrentDateEnd)
	
	def setCurrentRange(self,dateStart,dateEnd):
		self.CurrentDateStart = clamp(dateStart,self.DateStart,self.DateEnd)
		self.CurrentDateEnd = clamp(dateEnd,self.CurrentDateStart,self.DateEnd)
		def updateRows():
			map( lambda row: row.setDateRange(dateStart,dateEnd), self.AttachedRows )
		#import cProfile
		#cProfile.runctx('updateRows()',globals(),locals())
		updateRows()
		self.updateCurrentRects()
	
	def timelineRange(self):
		return (self.DateStart,self.DateEnd)
	
	def setTimelineRange(self,dateStart,dateEnd):
		# Ensure dateStart < dateEnd
		dateStart,dateEnd = sorted((dateStart,dateEnd))
		if dateStart == dateEnd:
			dateEnd = dateEnd.addDays(1)
		self.DateStart = dateStart
		self.DateEnd = dateEnd
		self.DayCount = self.DateStart.daysTo(self.DateEnd)
		
	def setWidth(self, width):
		self.Width = width
		self.updateCurrentRects()
	
	def updateCurrentRects(self):
		centerY = self.Height / 2.0
		left = self.dateToX(self.CurrentDateStart)
		right = self.dateToX(self.CurrentDateEnd)
		self.CurrentRect = QRectF( left, 0.0, right - left, self.Height )
		calcHandleRect = lambda x: QRectF( x - 6, centerY - 12, 12, 24 )
		self.LeftHandleRect = calcHandleRect(left)
		self.RightHandleRect = calcHandleRect(right)
		self.update()

	def sizeHint(self,which,constraint):
		if which == Qt.MinimumSize:
			width = 50
		else:
			if constraint.isValid():
				width = constraint.width()
			else:
				width = 9999
		return QSizeF(width, self.Height)
	
	def yearToX(self,year):
		return self.dateToX(QDate(year,1,1))
	
	def hoverEnterEvent( self, event ):
		QGraphicsItem.hoverEnterEvent(self,event)
		
	def hoverLeaveEvent( self, event ):
		QGraphicsItem.hoverLeaveEvent(self,event)

	def hoverMoveEvent( self, event ):
		pos = event.pos()
		if self.LeftHandleRect.contains(pos) or self.RightHandleRect.contains(pos):
			self.setCursor( Qt.SizeHorCursor )
		elif self.CurrentRect.contains( event.pos() ):
			self.setCursor( Qt.OpenHandCursor )
		else:
			self.setCursor( Qt.ArrowCursor )
		QGraphicsItem.hoverMoveEvent(self,event)

	def mousePressEvent(self,evt):
		pos = evt.pos()
		if self.LeftHandleRect.contains(pos):
			self.MoveType = TimelineBrowser.MoveType.Start
		elif self.RightHandleRect.contains(pos):
			self.MoveType = TimelineBrowser.MoveType.End
		elif self.CurrentRect.contains(pos):
			self.MoveType = TimelineBrowser.MoveType.Current
			self.setCursor(Qt.ClosedHandCursor)
		if self.MoveType != TimelineBrowser.MoveType.Null:
			self.MoveStartPos = pos.x()
			if self.MoveType == TimelineBrowser.MoveType.End:
				self.MoveOffset = self.MoveStartPos - self.dateToX(self.CurrentDateEnd)
			else:
				self.MoveOffset = self.MoveStartPos - self.dateToX(self.CurrentDateStart)
		else:
			QGraphicsItem.mousePressEvent(self,evt)
	
	def mouseMoveEvent(self,evt):
		x = clamp(evt.pos().x() - self.MoveOffset,0,self.Width-1)
		newDate = self.xToDate(x)
		if self.MoveType == TimelineBrowser.MoveType.Start:
			self.setCurrentRange(newDate,self.CurrentDateEnd)
		elif self.MoveType == TimelineBrowser.MoveType.End:
			self.setCurrentRange(self.CurrentDateStart,newDate)
		elif self.MoveType == TimelineBrowser.MoveType.Current:
			offset = min(self.CurrentDateStart.daysTo(newDate),self.CurrentDateEnd.daysTo(self.DateEnd))
			if offset != 0:
				self.setCurrentRange(self.CurrentDateStart.addDays(offset),self.CurrentDateEnd.addDays(offset))
		QGraphicsItem.mouseMoveEvent(self,evt)
		
	def mouseReleaseEvent(self,evt):
		self.setCursor( Qt.OpenHandCursor )
		QGraphicsItem.mouseReleaseEvent(self,evt)

	def paint(self,painter,option,widget):
		def drawYear(year,x,width):
			painter.setPen( QPen(colors.Outline,0) )
			painter.setBrush( colors.MonthHeaderBackground )
			painter.drawRect( QRect( x, 0, width, self.Height ) )
			painter.setPen( Qt.black )
			painter.drawText( QRect( x, 0, width, self.Height ), Qt.AlignCenter | Qt.AlignVCenter, str(year) )

		def drawHandle(rect):
			painter.setPen( colors.TimelineHandlePen )
			painter.setBrush( colors.TimelineHandleBrush )
			painter.drawRoundedRect( rect, 4, 4 )
		
		dayCount = self.DateStart.daysTo(self.DateEnd)
		curYear = startOfYear(self.DateStart).year()
		endYear = startOfYear(self.DateEnd).year()
		curPos = 0
		while curYear <= endYear:
			nextYear = curYear + 1
			nextPos = self.yearToX(nextYear)
			width = nextPos - curPos + 1
			if width > 50:
				drawYear(curYear,curPos,width)
			curYear = nextYear
			curPos = nextPos
	
		painter.setPen( colors.TimelineCurrentPen )
		painter.setBrush( colors.TimelineCurrentBrush )
		painter.drawRect( self.CurrentRect )
		drawHandle( self.LeftHandleRect )
		drawHandle( self.RightHandleRect )

class RowLayoutItem(QGraphicsLayoutItem):
	def __init__(self,row,layoutParent=None,):
		QGraphicsLayoutItem.__init__(self,layoutParent)
		self.setGraphicsItem(row)
		self.setMinimumHeight(row.Height+2)
		self.setMaximumHeight(row.Height+2)
		self.setPreferredHeight(row.Height+2)
		self.setSizePolicy( QSizePolicy.Expanding, QSizePolicy.Fixed )
		
	def setGeometry(self,rect):
		self.graphicsItem().setPos(rect.topLeft())
		self.graphicsItem().setWidth(rect.width())
	
	def sizeHint(self,which,constraint):
		return self.graphicsItem().sizeHint(which,constraint)

# Move to common or split off common functionality
class Scene(QGraphicsScene):
	def __init__(self):
		QGraphicsScene.__init__(self)
		cd = QDate.currentDate()
		# First day of this month
		self.StartDate = QDate.currentDate().addMonths(-6)
		self.Layout = QGraphicsLinearLayout(Qt.Vertical)
		self.Layout.setSpacing(0)
		#self.Layout.setSpacing(2)
		end = QDate(2016,1,1).addDays(-1)
		self.TimelineBrowser = TimelineBrowser(None,QDate(2006,1,1),end,self.StartDate,min(self.StartDate.addMonths(18),end))
		self.addRow(self.TimelineBrowser)
		self.Header = Header(self.StartDate)
		self.addRow(self.Header)
		
		self.CheckRecalcScheduled = False
		
	def setStartDate(self,startDate):
		for tl in self.rows():
			tl.setStartDate(startDate)
		
	def rows(self,base=RowBase):
		for item in self.items():
			if isinstance(item,base):
				yield item
	
	def graphRows(self):
		return self.rows(GraphRow)
	
	def scheduleRecalcCheck(self):
		if not self.CheckRecalcScheduled:
			self.CheckRecalcScheduled = True
			QTimer.singleShot( 0, self.checkRecalc )
			
	def checkRecalc(self):
		self.CheckRecalcScheduled = False
		for row in self.graphRows():
			row.checkReset()
			
	def reset(self):
		for row in self.rows():
			row.reset(True)
	
	def resize(self,size=QSize()):
		if size.isValid():
			hint = self.Layout.effectiveSizeHint( Qt.MinimumSize, QSizeF( size.width(), -1 ) )
			self.Layout.setGeometry( QRectF( 0.0, 0.0, size.width(), hint.height() ) )
		else:
			hint = self.Layout.effectiveSizeHint( Qt.MinimumSize, QSizeF( -1, -1 ) )
			self.Layout.setGeometry( QRectF( 0.0, 0.0, self.Layout.geometry().width(), hint.height() ) )
		# Needed to shrink the scene rect back to the size of the items in the scene
		#hint = self.Layout.effectiveSizeHint( Qt.MinimumSize, QSizeF( size.width(), -1 ) )
		#self.Layout.setGeometry( QRectF( 0.0, 0.0, hint.width(), hint.height() ) )
		self.setSceneRect(self.itemsBoundingRect())
		
	def addRow(self,row):
		self.Layout.insertItem(max(0,self.Layout.count()-2),RowLayoutItem(row,self.Layout))
		self.addItem(row)
		if row != self.TimelineBrowser:
			self.TimelineBrowser.addAttachedRow(row)
#		self.Layout.invalidate()
		self.resize()
#		self.invalidate()
		
	def findLayoutItem(self,row):
		for i in range(self.Layout.count()):
			li = self.Layout.itemAt(i)
			if li.graphicsItem() == row:
				return li
	
	def removeRow(self,row):
		li = self.findLayoutItem(row)
		if li:
			self.Layout.removeItem(li)
		self.TimelineBrowser.removeAttachedRow(row)
		self.removeItem(row)
#		self.Layout.invalidate()
		self.resize()
#		self.invalidate()
		
# Move to common
class View(QGraphicsView):
	def __init__(self,scene,parentWidget = None):
		QGraphicsView.__init__(self,scene,parentWidget)
		self.Scene = scene
		if True: #sys.platform == 'linux2':
			glWidget = QGLWidget()
			self.setViewport( glWidget )
		self.setRenderHint(QPainter.Antialiasing,True)
		#self.setRenderHint(QPainter.HighQualityAntialiasing,True)
		#self.setHorizontalScrollBarPolicy( Qt.ScrollBarAlwaysOff )
	
	def relayoutScene(self):
		#width = QApplication.instance().desktop().screenGeometry(self).width()
		self.scene().resize(QSize(self.viewport().width() - 10,self.viewport().height()))
	
	def resizeEvent(self, evt):
		self.relayoutScene()
		QGraphicsView.resizeEvent(self,evt)
