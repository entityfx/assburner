/*
 *
 * Copyright 2003, 2004 Blur Studio Inc.
 *
 * This file is part of the Resin software package.
 *
 * Assburner is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Assburner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Resin; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <qfile.h>
#include <qtextstream.h>
#include <qdir.h>

#include "blurqt.h"
#include "schema.h"
#include "tableschema.h"
#include "indexschema.h"
#include "field.h"
#include "sourcegen.h"
#include "path.h"

//#define HOLDGIL "/HoldGIL/"
#define HOLDGIL 

struct StringInfo
{
	int id;
	StringInfo * substringOf;
	int offset;
	QString str;
};

typedef QHash<QString,StringInfo*> StringInfoHash;
typedef QList<StringInfo*> StringInfoList;

StringInfoHash stringInfoByString;
StringInfoList stringInfos;
StringInfoList stringInfosIdSorted;

// Returns the string id
int addString(const QString & str)
{
	StringInfoHash::iterator it = stringInfoByString.find(str);
	if( it == stringInfoByString.end() ) {
		StringInfo * si = new StringInfo;
		si->substringOf = 0;
		si->offset = 0;
		si->str = str;
		si->id = stringInfoByString.size();
		stringInfoByString[str] = si;
		return si->id;
	}
	return (*it)->id;
}

bool sortByLength(const StringInfo * si1, const StringInfo * si2)
{
	return si1->str.size() > si2->str.size();
}

bool sortById(const StringInfo * si1, const StringInfo * si2)
{
	return si1->id < si2->id;
}

void processStrings()
{
	stringInfos = stringInfoByString.values();
	qSort(stringInfos.begin(), stringInfos.end(), sortByLength);
	int offset = 0;
	for( int i=0, end=stringInfos.size(); i<end; i++ ) {
		StringInfo * si = stringInfos[i];
		for( int n=0, nend=i; n < nend; ++n ) {
			StringInfo * larger = stringInfos[n];
			if( larger->substringOf ) continue;
			// There should be no duplicates, so if the size is the same then si can't be a substring of larger
			if( larger->str.size() == si->str.size() ) continue;
			int idx = larger->str.indexOf(si->str);
			if( idx >= 0 ) {
				si->substringOf = larger;
				si->offset = larger->offset + idx;
				break;
			}
		}
		if( !si->substringOf ) {
			si->offset = offset;
			offset += si->str.size();
		}
	}
	stringInfosIdSorted = stringInfos;
	qSort(stringInfosIdSorted.begin(), stringInfosIdSorted.end(), sortById);
}

int stringId(const QString & str)
{
	return stringInfoByString[str]->id;
}

QString getStringCode(const QString & str)
{
	return "_getString(" + QString::number(addString(str)) + ")";
}

int stringCount()
{
	return stringInfoByString.size();
}

QString stringData()
{
	int totalSize = 0;
	for( StringInfoList::iterator it = stringInfos.begin(), end = stringInfos.end(); it != end; ++it ) {
		StringInfo * si = *it;
		if( si->substringOf ) continue;
		totalSize += si->str.size();
	}
	QString data;
	data.reserve(totalSize + (totalSize / 80) * 3); // 80 char lines with two quotes and a newline
	int linePos = 0;
	data += "\"";
	for( StringInfoList::iterator it = stringInfos.begin(), end = stringInfos.end(); it != end; ++it ) {
		StringInfo * si = *it;
		if( si->substringOf ) continue;
		int strPos = 0;
		while( strPos < si->str.size() ) {
			if( si->str.size() - strPos + linePos >= 80 ) {
				int cnt = 80 - linePos;
				data.append(si->str.midRef(strPos,cnt));
				data.append("\"\n\"");
				strPos += cnt;
				linePos = 0;
			} else {
				int cnt = si->str.size() - strPos;
				data.append(si->str.midRef(strPos));
				linePos += cnt;
				strPos += cnt;
			}
		}
	}
	data += "\"";
	return data;
}

QString stringOffsets()
{
	QString ret;
	bool needComma = false;
	int n = 0;
	for( StringInfoList::iterator it = stringInfosIdSorted.begin(), end = stringInfosIdSorted.end(); it != end; ++it ) {
		StringInfo * si = *it;
		int offset = si->offset;
		if( needComma ) {
			ret += ",";
			if( (++n % 40) == 0 )
				ret += "\n";
		}
		ret += QString::number(offset);
		needComma = true;
	}
	return ret;
}

QString stringLengths()
{
	QString ret;
	bool needComma = false;
	int n = 0;
	for( StringInfoList::iterator it = stringInfosIdSorted.begin(), end = stringInfosIdSorted.end(); it != end; ++it ) {
		StringInfo * si = *it;
		if( needComma ) {
			ret += ",";
			if( (++n % 40) == 0 )
				ret += "\n";
		}
		ret += QString::number(si->str.size());
		needComma = true;
	}
	return ret;
}


QString ucf( const QString & orig )
{
	return orig.mid( 0, 1 ).toUpper() + orig.mid( 1 );
}

QString lcf( const QString & orig )
{
	return orig.mid( 0, 1 ).toLower() + orig.mid( 1 );
}

bool inheritsElement( TableSchema * table )
{
	while( table ) {
		if( table->tableName() == "Element" )
			return true;
		table = table->parent();
	}
	return false;
}

QString findFile( const QString & fileName )
{
	if( QFile::exists( fileName ) )
		return fileName;
	QString tmp = "/usr/share/classmaker/" + fileName;
	if( QFile::exists( tmp ) )
		return tmp;
	tmp = "/usr/lib/stone/" + fileName;
	if( QFile::exists( tmp ) )
		return tmp;
	return QString();
}

QString readFile( const QString & fileName )
{
	QString path = findFile(fileName);
	return path.isEmpty() ? path : readFullFile( findFile( fileName ) );
}

void write( QString & input,  QString output )
{
	if( readFile( output ) != input ) {
		if( !writeFullFile( output, input ) )
			LOG_1( "Couldn't open " + output + " for writing" );
	}
}

QString commentifyDocString( const QString & docString, int indentLevel = 0 )
{
	QString indentString;
	for( int i=0; i<indentLevel; i++ )
		indentString += "\t";

	QString outString(indentString +"/**\n");
	// Unix \n, windows \n\r, mac \r
	QStringList lines = docString.split( QRegExp( "[\n\r]\r?" ) );
	QString commentStart("/*"), commentEnd("*/");
	foreach( QString line, lines ) {
		line = line.replace( commentStart, "" );
		line = line.replace( commentEnd, "" );
		outString += indentString + "  * " + line + "\n";
	}
	outString += indentString + "  **/\n";
	return outString;
}

QString retify( const QString & type )
{
	return QString("RET(%1)").arg(type);
}

QString fieldArgName( const QString & fieldName )
{
	QString ret(fieldName);
	ret[0] = ret[0].toLower();
	return ret;
}

static void writeClass( TableSchema * table, const QString & path, int writeWhat )
{
	Path autoPath( path + "/autocore/" );
	Path sipPath( path + "/sip/" );
	
	if( !autoPath.dirExists() && !autoPath.mkdir(false) ) {
		LOG_5( "Couldn't create autocore folder at " + autoPath.path() );
		return;
	}

	if( !sipPath.dirExists() && !sipPath.mkdir(false) ) {
		LOG_5( "Couldn't create sip folder at " + sipPath.path() );
		return;
	}

	bool hasElementBase = inheritsElement( table );
	QString defaultLookup;
	
	// base
	QString name = table->className();
	QString tblname = table->tableName();
	
	QString base = table->parent() ? table->parent()->className() : "Record";
	FieldList fields = table->ownedFields();
	
	// classDefines
	QString classDefines;
	QString classHeaders;

	QString baseHeader, baseFunctions, baseSip;
	if( QFile::exists( path + "/base/" + name.toLower() + "base.h" ) )
		baseHeader = "#include \"base/tl__base.h\"";
	
	QString sipBase( path + "/base/" + name.toLower() + "base.sip" );
	if( QFile::exists( sipBase ) )
		baseSip = readFile( sipBase );
	
	foreach( Field * f, fields )
	{
		if( !f->flag( Field::ForeignKey ) || !f->foreignKeyTable()->useCodeGen() )
			continue;
		QString fk = f->foreignKey();
		if( fk == "ElementType" ) {
			classDefines += "#include \"elementtypelist.h\"\n";
			classDefines += "#include \"elementtype.h\"\n";
		} else {
			classDefines += "class " + fk + "List;\n";
			classDefines += "class " + fk + ";\n";
		}
		classHeaders += "#include \"" + fk.toLower() + ".h\"\n";
		classDefines += "\n\n";
	}
	
	QString elementHeaders;
	if( inheritsElement( table ) )
		elementHeaders = "#include \"user.h\"\n#include \"elementtype.h\"\n";
	
	QString indexHeaders;
	IndexSchemaList indexes = table->indexes();
	foreach( IndexSchema * index, indexes )
	{
		foreach( Field * f, index->columns() )
			if( f->flag( Field::ForeignKey ) && f->foreignKeyTable()->useCodeGen() )
				indexHeaders += "#include \"" + f->foreignKey().toLower() + ".h\"\n";
	}
	
	
	// methodDefs
	QStringList memberCtorList, schemaFieldDefList, schemaIndexDefList;
	QString methodDefs, sipMethodDefs, methods, listMethodDefs, sipListMethodDefs, listMethods, memberVars, memberCtors, setCode, getCode;
	QString getColumn, setColumn, addFields;
	QString schemaFieldDecls, schemaIndexDecls, schemaArgs;

	schemaArgs = getStringCode(tblname) + "," + getStringCode(name);
	
	foreach( Field * f, table->fields() ) {
		QString camelMeth = f->methodName();
		camelMeth[0] = camelMeth[0].toUpper();
		if( f->flag( Field::PrimaryKey ) )
			camelMeth = "Key";
		QString fullCamelMeth = "t__::c." + camelMeth;
		schemaFieldDefList += "0";
		schemaFieldDecls += "\t\tStaticFieldExpression " + camelMeth + ";\n";
		// Field comes from parent, we aren't creating the field below so we have
		// to get it from the parent
		if( f->table() != table )
			addFields += "\t\t" + fullCamelMeth + ".setup(b__::c." + camelMeth + ",sSchema);\n";
	}
	
	foreach( Field * f, fields )
	{
		QString fn = f->name();
		QString meth = f->methodName();
		QString pmeth = f->pluralMethodName();
		QString disp = f->displayName();
		QString camelMeth = meth;
		camelMeth[0] = camelMeth[0].toUpper();
		if( f->flag( Field::PrimaryKey ) )
			camelMeth = "Key";
		camelMeth = "t__::c." + camelMeth;
		QString idx = camelMeth;
		
		if( f->flag( Field::PrimaryKey ) ){
			addFields += "\t\t" + camelMeth + ".setup(new Field( sSchema, " + getStringCode(fn) + ", Field::UInt, Field::Flags(Field::PrimaryKey | Field::Unique | Field::NotNull), " + getStringCode("key") + " ),sSchema);\n";
			continue;
		}

		if( !f->docs().isEmpty() )
			methodDefs += commentifyDocString( f->docs(), 1 );
		addFields += "\t\t" + camelMeth + ".setup(";
		if( f->flag( Field::ForeignKey ) ) {
			QString fkt = f->foreignKey();
			QString fkType = f->foreignKeyTable()->useCodeGen() ? fkt : "Record";
			methodDefs += "\t" + retify(fkType) + " " + lcf( meth ) + "(int lookupMode = Index::UseSelect|Index::PartialSelect|Index::UseCache) const;\n";
			methodDefs += "\tRET(t__) & set" + ucf( meth ) + "( const " + fkType + " & );\n";
			sipMethodDefs += "\t" + retify(fkType) + " " + lcf( meth ) + "(int lookupMode = Index::UseSelect|Index::PartialSelect|Index::UseCache) const  throw(SqlException,LostConnectionException,PythonException) " HOLDGIL ";\n";
			sipMethodDefs += "\tRET(t__) & set" + ucf( meth ) + "( const " + fkType + " & );\n";
			methods += fkType + " t__::" + lcf( meth ) + "(int lookupMode) const\n{\n";
			methods += "\treturn foreignKey( " + idx + ", lookupMode );\n";
			methods += "}\n\n";
			methods += "t__ & t__::set" + ucf( meth ) + "( const " + fkType + " & val )\n{\n";
			methods += "\tRecord::setForeignKey( " + idx + ", val );\n\treturn *this;\n";
			methods += "}\n\n";
			listMethodDefs += "\t" + retify(fkType + "List") + " " + lcf( pmeth ) + "(int lookupMode = Index::UseSelect|Index::PartialSelect|Index::UseCache) const;\n";
			listMethodDefs += "\tRET(t__List) & set" + ucf( pmeth ) + "( const " + fkType + " & );\n";
			sipListMethodDefs += "\t" + retify(fkType + "List") + " " + lcf( pmeth ) + "(int lookupMode = Index::UseSelect|Index::PartialSelect|Index::UseCache) const  throw(SqlException,LostConnectionException,PythonException) " HOLDGIL ";\n";
			sipListMethodDefs += "\tRET(t__List) & set" + ucf( pmeth ) + "( const " + fkType + " & );\n";
			listMethods += fkType + "List t__List::" + lcf( pmeth ) + "(int lookupMode) const\n{\n";
			listMethods += "\treturn RecordList::foreignKey( " + idx + ", lookupMode );\n}\n\n";
			listMethods += "t__List & t__List::set" + ucf( pmeth ) + "( const " + fkType + " & val )\n{\n";
			listMethods += "\tRecordList::setForeignKey( " + idx + ", val );\n";
			listMethods += "\treturn *this;\n";
			listMethods += "}\n\n";
			// Field Name, Foreign Key Table Name, Flags, hasIndex, indexDeleteMode
			addFields += QString( "new Field( sSchema, %1, %2, %3, %4, %5 ),sSchema);\n" )
						.arg( getStringCode(fn) ) 
						.arg( getStringCode(fkt) )
						.arg( f->flagString() )
						.arg( f->hasIndex() ? "true": "false" )
						.arg( "Field::" + f->indexDeleteModeString() );
			if( f->hasIndex() ) {
				addFields += "\t\tif( " + camelMeth + "->index() ) (t__::i." + f->index()->name() + " = " + camelMeth + "->index())->setUseCache("
				+ QString( f->index()->useCache() ? "true" : "false" ) + ");\n";
			}
		} else {
			if( f->flag(Field::DefaultLookup) && f->type() == Field::String && f->index() ) {
				QString dflName = "Field" + name + "." + fn;
				if( defaultLookup.size() ) {
					LOG_1( "Conflicting default lookup found: " + dflName + " conflicts with existing " + defaultLookup );
				} else {
					defaultLookup = dflName;
					QString def = "\tt__( const QString & );\n";
					methodDefs += def;
					sipMethodDefs += def;
					methods += "t__::t__( const QString & arg )\n{\n";
					methods += "\t*this = t__::recordBy" + f->index()->name() + "(arg);\n}\n\n";
					def = "\tt__List( QStringList args );\n";
					listMethodDefs += def;
					sipListMethodDefs += def;
					listMethods += "t__List::t__List( QStringList args )\n{\n";
					listMethods += "\t*this = t__::table()->indexFromSchema(t__::i." + f->index()->name() + ")->recordsByIndexMulti(QList<QVariant>() << QVariant(args));\n}\n\n";
				}
			}
			methodDefs += "\t" + f->typeString() + " " + lcf( meth ) + "() const;\n";
			methodDefs += "\tRET(t__) & set" + ucf( meth ) + "( const " + f->typeString() + " & );\n";
			sipMethodDefs += "\t" + f->typeString() + " " + lcf( meth ) + "() const throw(SqlException,LostConnectionException,PythonException) " HOLDGIL ";\n";
			sipMethodDefs += "\tRET(t__) & set" + ucf( meth ) + "( const " + f->typeString() + " & );\n";
			methods += f->typeString() + " t__::" + lcf( meth ) + "() const\n{\n";
			methods += "\treturn " + f->typeFromVariantCode().arg( "Record::getValue( " + idx + " )") + ";\n";
			methods += "}\n\n";
			methods += "t__ & t__::set" + ucf( meth ) + "( const " + f->typeString() + " & val )\n{\n";
			methods += "\tRecord::setValue( " + idx + ", " + f->variantFromTypeCode().arg("val") + " );\n\treturn *this;\n";
			methods += "}\n\n";
			listMethodDefs += "\t" + f->listTypeString() + " " + lcf( pmeth ) + "() const;\n";
			listMethodDefs += "\tRET(t__List) & set" + ucf( pmeth ) + "( const " + f->typeString() + " & );\n";
			sipListMethodDefs += "\t" + f->listTypeString() + " " + lcf( pmeth ) + "() const throw(SqlException,LostConnectionException,PythonException) " HOLDGIL ";\n";
			sipListMethodDefs += "\tRET(t__List) & set" + ucf( pmeth ) + "( const " + f->typeString() + " & );\n";
			listMethods += f->listTypeString() + " t__List::" + lcf( pmeth ) + "() const\n{\n";
			listMethods += "\t" + f->listTypeString() + " ret;\n";
			listMethods += "\tif( d )\n";
			listMethods += "\t\tfor( QList<RecordImp*>::Iterator it = d->mList.begin(); it != d->mList.end(); ++it )\n";
			listMethods += "\t\t\tret += t__(*it)." + lcf( meth ) + "();\n\treturn ret;\n}\n\n";
			listMethods += "t__List & t__List::set" + ucf( pmeth ) + "( const " + f->typeString() + " & val )\n{\n";
			listMethods += "\tRecordList::setValue( " + idx + ", " + f->variantFromTypeCode().arg("val") + " );\n";
			listMethods += "\treturn *this;\n";
			listMethods += "}\n\n";
			addFields += QString( "new Field( sSchema, %1, %2, %3, %4 ),sSchema);\n" )
						.arg( getStringCode(fn) )
						.arg( "Field::" + f->variantTypeString() )
						.arg( f->flagString() )
						.arg( getStringCode(f->flag( Field::PrimaryKey ) ? QString("key") : meth) );
			if( f->hasIndex() ) {
				addFields += "\t\t" + camelMeth + "->setHasIndex(true);\n\t";
				addFields += "(t__::i." + f->index()->name() + " = " + camelMeth + "->index())->setUseCache("
				+ QString( f->index()->useCache() ? "true" : "false" ) + ");\n";
			}

		}
		if( f->generatedDisplayName() != disp )
			addFields += "\t\t" + camelMeth + "->setDisplayName( " + getStringCode(disp) + " );\n";
	}
	
	if( !memberCtorList.isEmpty() )
		memberCtors = ": " + memberCtorList.join( "\n ," );

	// indexDefs
	QString indexDefs, sipIndexDefs, indexMethods, addIndexes, addIdxCols, indexCtors;
	foreach( IndexSchema * index, indexes )
	{
		QString indexDef, sipIndexDef;
		FieldList fl = index->columns();
		
		if( fl.size() == 0 || ( fl.size() == 1 && fl[0]->flag(Field::PrimaryKey) ) )
			continue;

		if( index->defaultTableLookup() ) {
			if( fl.size() != 1 ) {
				LOG_1( "Index marked as default table lookup but has more than one column: " + index->name() + " on " + name );
			} else if ( fl[0]->type() != Field::String ) {
				LOG_1( "Index marked as default table lookup but the column type is not string: " + index->name() + " on " + name );
			} else {
				QString dflName = "Index " + index->name() + " on " + name;
				if( defaultLookup.size() ) {
					LOG_1( "Conflicting default lookup found: " + dflName + " conflicts with existing " + defaultLookup );
				} else {
					defaultLookup = dflName;
					QString def = "\tt__( const QString & );\n";
					methodDefs = def + methodDefs;
					sipMethodDefs = def + sipMethodDefs;
					methods = "t__::t__( const QString & arg )\n{\n\t*this = t__::recordBy"
						+ index->name() + "(arg);\n}\n\n" + methods;
					def = "\tt__List( QStringList args );\n";
					listMethodDefs = def + listMethodDefs;
					sipListMethodDefs = def + sipListMethodDefs;
					listMethods = "t__List::t__List( QStringList args )\n{\n"
						 "\t*this = t__::table()->indexFromSchema(t__::i."
						+ index->name()
						+ ")->recordsByIndexMulti(QList<QVariant>() << QVariant(args));\n}\n\n"
						+ listMethods;
				}
			}
		}
		
		schemaIndexDecls += "\t\tIndexSchema * " + index->name() + ";\n";
		schemaIndexDefList += "0";
		
		if( index->holdsList() )
			indexDef += "\tstatic RET(t__List) recordsBy" + index->name() + "( ";
		else
			indexDef += "\tstatic RET(t__) recordBy" + index->name() + "( ";

		QStringList args, argsWOTypes;
		if( !index->field() )
			addIndexes += "\tt__::i." + index->name() + "= new IndexSchema( " + getStringCode(index->name()) + ", sSchema, "
				+ QString(index->holdsList() ? "true, " : "false, ") + QString(index->useCache() ? "true" : "false")
				+ QString(index->defaultTableLookup() ? ", true );\n" : ");\n");

		for( FieldIter it = fl.begin(); it != fl.end(); ++it ){
			Field * f = *it;
			QString fn = fieldArgName(f->name());
			if( f->flag( Field::ForeignKey ) ) {
				QString type = f->foreignKeyTable()->useCodeGen() ? f->foreignKey() : "Record";
				args += "const " + type + " &" + fn;
			} else {
				args += "const " + f->typeString() + " &" + fn;
			}
			argsWOTypes += fn;
			if( !index->field() ) {
				QString fieldName = f->methodName();
				fieldName[0] = fieldName[0].toUpper();
				addIdxCols += "\tt__::i." + index->name() + "->addField( " + f->table()->className() + "::c." + fieldName + " );\n";
			}
		}
		indexDef += args.join(", ") + ", int lookupMode = Index::UseSelect|Index::PartialSelect|Index::UseCache )";
		sipIndexDef = indexDef + " throw(SqlException,LostConnectionException,PythonException)";
		indexDefs += indexDef + ";\n";
		sipIndexDefs += sipIndexDef + ";\n";

		if( index->holdsList() ){
			indexMethods += "t__List t__::recordsBy" + index->name() + "( " + args.join(", ") + ", int lookupMode )\n{\n";
		} else {
			indexMethods += "t__ t__::recordBy" + index->name() + "( " + args.join(", ") + ", int lookupMode )\n{\n";
		}
	
		indexMethods += "\tQList<QVariant> args;\n";
	
		foreach( Field * f, fl ) {
			QString toVar = fieldArgName(f->name());
			if( f->flag( Field::ForeignKey ) )
				toVar = "qVariantFromValue<Record>("+toVar+")";
			else
				toVar = "QVariant(" + toVar + ")";
			indexMethods += "\targs += " + toVar + ";\n";
		}
	
		indexMethods += "\treturn table()->indexFromSchema( t__::i." + index->name() + " )->recordsByIndex( args, lookupMode )";
		if( !index->holdsList() )
			indexMethods += "[0]";
		indexMethods += ";\n}\n\n";
	}
	
	Schema * schema = table->schema();
	TableSchemaList tl = schema->tables();
	foreach( TableSchema * t, tl ) {
		// No reverse accessors to ourself, or to tables that don't have a c++ class
		if( t == table || !t->useCodeGen() ) continue;
		FieldList cl = t->ownedFields();
		foreach( Field * f, cl ) {
			if( f->foreignKeyTable() != table || !f->flag( Field::ReverseAccess ) || !f->index() ) continue;
			QString ret = t->className();
			QString method = lcf( t->className() );
			if( !f->flag( Field::Unique ) ) {
				ret += "List";
				method = pluralizeName(method);
			}
			classDefines += "class " + ret + ";\n";
			classDefines += "class " + ret + "List;\n";
			classHeaders += "#include \"" + t->className().toLower() + ".h\"\n";
			methodDefs += "\t" + retify(ret) + " " + method + "( int lookupMode=Index::UseCache|Index::PartialSelect|Index::UseSelect ) const;\n";
			sipMethodDefs += "\t" + retify(ret) + " " + method + "( int lookupMode=Index::UseCache|Index::PartialSelect|Index::UseSelect ) const throw(SqlException,LostConnectionException,PythonException);\n";
			methods += ret + " t__::" + method + "( int lookupMode ) const\n{\n";
			methods += "\treturn " + t->className() + QString(f->flag( Field::Unique ) ? "::recordBy" : "::recordsBy") + f->index()->name() + "( ";
			methods += "*this, lookupMode );\n}\n";

			QString listMethod = method;

			if( f->flag( Field::Unique ) ) {
				ret += "List";
				listMethod = pluralizeName(listMethod);
			}

			listMethodDefs += "\t" + retify(ret) + " " + listMethod + "( int lookupMode=Index::UseCache|Index::PartialSelect|Index::UseSelect );\n";
			sipListMethodDefs += "\t" + retify(ret) + " " + listMethod + "( int lookupMode=Index::UseCache|Index::PartialSelect|Index::UseSelect ) throw(SqlException,LostConnectionException,PythonException);\n";
			
			listMethods += ret + " t__List::" + listMethod + "( int lookupMode )\n{\n";
			listMethods += "\tIndex * idx = " + t->className() + "::table()->indexFromField( \"" + f->name() + "\" );\n";
			listMethods += "\treturn idx ? idx->recordsByIndexMulti( getValue( " + table->field(table->primaryKeyIndex())->table()->className() + "::c.Key ), lookupMode ) : " + t->className() + "List();\n}\n\n";
		}
	}

	QString preload;
	if( !table->projectPreloadColumn().isEmpty() )
		preload = "\tsSchema->setProjectPreloadColumn( \"" + table->projectPreloadColumn() + "\" );\n";
	if( table->isPreloadEnabled() )
		preload += "\tsSchema->setPreloadEnabled( true );\n";
	if( !table->expireKeyCache() )
		preload += "\tsSchema->setExpireKeyCache( false );\n";
	if( table->logEvents() )
		preload += "\tsSchema->setLogEvents( true );\n";
	
	QString elementHacks, elementMethods;
	
	if( hasElementBase ) {
		elementHacks += "\tstatic ElementType type();\n";
		elementMethods += "ElementType t__::type()\n{\n\tstatic int mType=0;\n";
		elementMethods += "\tif( !mType )\n\t\tmType = ElementType::recordByName( \"t__\" ).key();\n";
		elementMethods += "\treturn ElementType(mType);\n}\n";
	}
	if( table->tableName() == "Element" )
	{
		listMethodDefs += "\tElementList children( const ElementType & et = ElementType(), bool recursive = false );\n";
		listMethodDefs += "\tElementList children( const ElementTypeList & et, bool recursive = false );\n";
		listMethods += "ElementList ElementList::children( const ElementType & et, bool recursive )\n{\n";
		listMethods += "\tElementList ret;\n\tfor( ElementIter it = begin(); it != end(); ++it )\n";
		listMethods += "\t\tret += (*it).children( et, recursive );\n\treturn ret;\n}\n";
		listMethods += "ElementList ElementList::children( const ElementTypeList & etl, bool recursive )\n{\n";
		listMethods += "\tElementList ret;\n\tfor( ElementIter it = begin(); it != end(); ++it )\n";
		listMethods += "\t\tret += (*it).children( etl, recursive );\n\treturn ret;\n}\n";
	}

	QString schemaFieldDefs = schemaFieldDefList.join(",\n\t");
	QString schemaIndexDefs = schemaIndexDefList.join(",\n\t");
	addFields = addIndexes + addFields;

	if( writeWhat & WriteCpp ) {
		QString temp = readFile( "templates/autocore.h" );
		temp.replace( "<%SCHEMAFIELDDECLS%>", schemaFieldDecls );
		temp.replace( "<%SCHEMAINDEXDECLS%>", schemaIndexDecls );
		temp.replace( "<%METHODDEFS%>", methodDefs );
		temp.replace( "<%INDEXDEFS%>", indexDefs );
		temp.replace( "<%ELEMENTHACKS%>", elementHacks );
		temp.replace( "<%ELEMENTHEADERS%>", elementHeaders );
		temp.replace( "<%CLASSDEFS%>", classDefines );
		temp.replace( "<%CLASSDOCS%>", commentifyDocString( table->docs() + "\n\\ingroup Classes" ) );
		temp.replace( "<%CLASSHEADERS%>", classHeaders );
		temp.replace( "<%BASEHEADER%>", baseHeader );
		temp.replace( "t__", name );
		temp.replace( "tu__", name.toUpper() );
		temp.replace( "tl__", name.toLower() );
		temp.replace( "tlcf__", lcf( name ) );
		temp.replace( "b__", base );
		temp.replace( "bu__", base.toUpper() );
		temp.replace( "bl__", base.toLower() );
		temp.replace( "snu__", schema->name().toUpper() );
		temp.replace( "snl__", schema->name().toLower() );
		temp.replace( QRegExp( "RET\\(([^\\)]+)\\)" ), "\\1" );

		write( temp, path + "/autocore/" + name.toLower() + ".h" );

		temp = readFile( "templates/autocore.cpp" );
		temp.replace( "<%SCHEMAFIELDDEFS%>", schemaFieldDefs );
		temp.replace( "<%SCHEMAINDEXDEFS%>", schemaIndexDefs );
		temp.replace( "<%SCHEMA_ARGS%>", schemaArgs );
		temp.replace( "<%CLASSHEADERS%>", classHeaders );
		temp.replace( "<%ELEMENTHEADERS%>", elementHeaders );
		temp.replace( "<%CLASSDEFS%>", classDefines );
		temp.replace( "<%SETPARENT%>", table->parent() ? "sSchema->setParent( b__::schema() );\n" : "" );
		temp.replace( "<%ADDINDEXCOLUMNS%>", addIdxCols );
		temp.replace( "<%ADDFIELDS%>", addFields );
		temp.replace( "<%PRELOAD%>", preload );
		temp.replace( "<%METHODS%>", methods );
		temp.replace( "<%ELEMENTMETHODS%>", elementMethods );
		temp.replace( "<%INDEXMETHODS%>", indexMethods );
		temp.replace( "<%INDEXHEADERS%>", indexHeaders );
		temp.replace( "<%BASEHEADER%>", baseHeader );
		temp.replace( "<%BASEFUNCTIONS%>", baseFunctions );
		temp.replace( "t__", name );
		temp.replace( "tu__", name.toUpper() );
		temp.replace( "tl__", name.toLower() );
		temp.replace( "tlcf__", lcf( name ) );
		temp.replace( "b__", base );
		temp.replace( "bu__", base.toUpper() );
		temp.replace( "bl__", base.toLower() );
		temp.replace( "snu__", schema->name().toUpper() );
		temp.replace( "snl__", schema->name().toLower() );
		write( temp, path + "/autocore/" + name.toLower() + ".cpp" );

		temp = readFile( "templates/autolist.h" );
		temp.replace( "<%INDEXDEFS%>", indexDefs );
		temp.replace( "<%ELEMENTHACKS%>", elementHacks );
		temp.replace( "<%ELEMENTHEADERS%>", elementHeaders );
		temp.replace( "<%CLASSDEFS%>", classDefines );
		temp.replace( "<%CLASSHEADERS%>", classHeaders );
		temp.replace( "<%LISTDEFS%>", listMethodDefs );
		temp.replace( "t__", name );
		temp.replace( "tu__", name.toUpper() );
		temp.replace( "tl__", name.toLower() );
		temp.replace( "tlcf__", lcf( name ) );
		temp.replace( "b__", base );
		temp.replace( "bu__", base.toUpper() );
		temp.replace( "bl__", base.toLower() );
		temp.replace( "snu__", schema->name().toUpper() );
		temp.replace( "snl__", schema->name().toLower() );
		temp.replace( QRegExp( "RET\\(([^\\)]+)\\)" ), "\\1" );
		write( temp, path + "/autocore/" + name.toLower() + "list.h" );

		temp = readFile( "templates/autolist.sip" );
		temp.replace( "<%INDEXDEFS%>", indexDefs );
		temp.replace( "<%ELEMENTHACKS%>", elementHacks );
		temp.replace( "<%ELEMENTHEADERS%>", elementHeaders );
		temp.replace( "<%CLASSDEFS%>", classDefines );
		temp.replace( "<%CLASSHEADERS%>", classHeaders );
		temp.replace( "<%LISTDEFS%>", sipListMethodDefs );
		temp.replace( QRegExp( "RET\\(([^\\)]+List)\\)" ), "Mapped\\1" );
		temp.replace( "t__", name );
		temp.replace( "tu__", name.toUpper() );
		temp.replace( "tl__", name.toLower() );
		temp.replace( "tlcf__", lcf( name ) );
		temp.replace( "b__", base );
		temp.replace( "bu__", base.toUpper() );
		temp.replace( "bl__", base.toLower() );
		temp.replace( "snu__", schema->name().toUpper() );
		temp.replace( "snl__", schema->name().toLower() );
		temp.replace( QRegExp( "RET\\(([^\\)]+)\\)" ), "Mapped\\1" );
		write( temp, path + "/sip/" + name.toLower() + "list.sip" );

		temp = readFile( "templates/autolist.cpp" );
		temp.replace( "<%ELEMENTHEADERS%>", elementHeaders );
		temp.replace( "<%CLASSDEFS%>", classDefines );
		temp.replace( "<%CLASSHEADERS%>", classHeaders );
		temp.replace( "<%METHODS%>", methods );
		temp.replace( "<%LISTMETHODS%>", listMethods );
		temp.replace( "t__", name );
		temp.replace( "tu__", name.toUpper() );
		temp.replace( "tl__", name.toLower() );
		temp.replace( "tlcf__", lcf( name ) );
		temp.replace( "b__", base );
		temp.replace( "bu__", base.toUpper() );
		temp.replace( "bl__", base.toLower() );
		temp.replace( "snu__", schema->name().toUpper() );
		temp.replace( "snl__", schema->name().toLower() );
		write( temp, path + "/autocore/" + name.toLower() + "list.cpp" );

		processStrings();
		temp = readFile( "templates/strings.cpp" );
		temp.replace( "<%STRING_DATA%>", stringData() );
		temp.replace( "<%STRING_COUNT%>", QString::number(stringCount()) );
		temp.replace( "<%STRING_OFFSETS%>", stringOffsets() );
		temp.replace( "<%STRING_LENGTHS%>", stringLengths() );
		write( temp, path + "/autocore/strings.cpp" );

		temp = readFile( "templates/strings.h" );
		write( temp, path + "/autocore/strings.h" );
	}
	
	if( writeWhat & WriteSip ) {
		QString temp = readFile( "templates/autocore.sip" );
		temp.replace( "<%SCHEMAFIELDDECLS%>", schemaFieldDecls );
		temp.replace( "<%SCHEMAINDEXDECLS%>", schemaIndexDecls );
		temp.replace( "<%METHODDEFS%>", sipMethodDefs );
		temp.replace( "<%INDEXDEFS%>", sipIndexDefs );
		temp.replace( "<%ELEMENTHACKS%>", elementHacks );
		temp.replace( "<%ELEMENTHEADERS%>", elementHeaders );
		temp.replace( "<%CLASSDEFS%>", classDefines );
		temp.replace( "<%CLASSHEADERS%>", classHeaders );
		temp.replace( "<%BASEHEADER%>", baseSip );
		temp.replace( QRegExp( "RET\\(([^\\)]+List)\\)" ), "Mapped\\1" );
		temp.replace( "t__", name );
		temp.replace( "tu__", name.toUpper() );
		temp.replace( "tl__", name.toLower() );
		temp.replace( "tlcf__", lcf( name ) );
		temp.replace( "b__", base );
		temp.replace( "bu__", base.toUpper() );
		temp.replace( "bl__", base.toLower() );
		temp.replace( "snu__", schema->name().toUpper() );
		temp.replace( "snl__", schema->name().toLower() );
		temp.replace( QRegExp( "RET\\(([^\\)]+)\\)" ), "Mapped\\1" );
		write( temp, path + "/sip/" + name.toLower() + ".sip" );
	}
}

void writeSource( Schema * schema, const QString & path, int writeWhat )
{
	LOG_1( "writeSource: Writing sources to " + path );
	TableSchemaList tables = schema->tables();
	QStringList headers, sources, classes;
	TableSchemaList nonCodeGen;
	QString autoSip;
	QString sn = schema->name();
	QString snl = sn.toLower(), snu = sn.toUpper();
	
	foreach( TableSchema * t, tables ) {
		if( !t->useCodeGen() ) {
			nonCodeGen += t;
			continue;
		}
		writeClass( t, path, writeWhat );
		QString lct = t->className().toLower();
		if( writeWhat & WriteCpp ) {
			classes += t->className();
			headers += "\tautocore/" + lct + ".h";
			headers += "\tautocore/" + lct + "list.h";
			sources += "\tautocore/" + lct + ".cpp";
			sources += "\tautocore/" + lct + "list.cpp";
			if( QFile::exists( path + "/base/" + lct + "base.cpp" ) )
				sources += "\tbase/" + lct + "base.cpp";
		}
		
		if( writeWhat & WriteSip ) {
			autoSip += "%Include " + lct + ".sip\n";
			autoSip += "%Include " + lct + "list.sip\n";
		}
	}

	if( writeWhat & WriteNgSchema )
		schema->writeXmlSchema( path + "/ng_schema.xml", 0, nonCodeGen );
	
	if( writeWhat & WriteCpp ) {
		sources += "\tautocore/strings.cpp";
		headers += "\tautocore/strings.h";
		
		QString autoPri = "SOURCES += \\\n" + sources.join( " \\\n") + "\n\n";
		autoPri += "HEADERS += \\\n" + headers.join( " \\\n") + "\n\n";
		autoPri += "INCLUDEPATH+=autocore include\n\n";
		
		write( autoPri, path + "/auto.pri" );
		
		QString includes;
		for( QStringList::Iterator it = classes.begin(); it != classes.end(); ++it )
			includes += "#include \"" + (*it).toLower() + ".h\"\n";

		QString libcpp = readFile( "templates/autolib.cpp" );
		libcpp.replace( "s__", sn );
		libcpp.replace( "sl__", snl );
		libcpp.replace( "su__", snu );
		libcpp.replace( "<%INCLUDES%>", includes );
		libcpp.replace( "<%INIT_TABLE_SCHEMAS%>", classes.join( "::schema();\n\t" ) + "::schema();\n\t" );
		write( libcpp, path + "/" + snl + ".cpp" );
		
		QString libh = readFile( "templates/autolib.h" );
		libh.replace( "s__", sn );
		libh.replace( "sl__", snl );
		libh.replace( "su__", snu );
		write( libh, path + "/" + snl + ".h" );
	}

	if( writeWhat & WriteSip ) {
		QString libsip = readFile( "templates/autolib.sip" );
		libsip.replace( "s__", sn );
		libsip.replace( "sl__", snl );
		libsip.replace( "su__", snu );
		write( libsip, path + "/sip/" + snl + ".sip" );
	
		write( autoSip, path + "/sip/auto.sip" );
	}
}

