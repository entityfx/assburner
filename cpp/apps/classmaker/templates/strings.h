
extern QString * qstrings;

void buildStrings();

inline QString & _getString(int id)
{
	return qstrings[id];
}

void freeStrings();