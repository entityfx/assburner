
#ifndef tu___H
#define tu___H

#include <qstring.h>
#include <qdatetime.h>
#include <qlist.h>
#include <qvariant.h>
#include <qimage.h>

#include <qjsondocument.h>

#include "snl__.h"
#include "expression.h"
#include "interval.h"
#include "intlist.h"
#include "joinedselect.h"
#include "stringmap.h"
#include "table.h"
#include "bl__.h"

using namespace Stone;

#define HEADER_FILES
<%BASEHEADER%>
#undef HEADER_FILES

class t__Schema;
class t__List;

typedef QList<QVariant> VarList;

/// @cond
<%CLASSDEFS%>
/// @endcond

<%CLASSDOCS%>
class snu___EXPORT t__ : public b__
{
public:
	/*
	 * Default constructor.  Creates a valid, uncommited t__ record.
	 */
	t__();

	/*
	 * Looks up the t__ record with primary key \param key
	 */
	t__( uint key, int lookupMode = Index::UseSelect | Index::UseCache );

	/*
	 * Constructs a shallow copy of \param other
	 */
	t__( const t__ & other ) : b__( other ) {}

#ifdef Q_COMPILER_RVALUE_REFS
	t__( t__ && other ) : b__( std::move(other) ) {}
#endif

	/*
	 * Constructs a shallow copy of \param r
	 * If r is not a derived from t__, then
	 * the record will be invalid.
	 */
	t__( const Record & r )
	: b__( r.mImp, false )
	{ checkImpType(); }

#ifdef Q_COMPILER_RVALUE_REFS
	t__( Record && r )
	: b__( std::move(r) )
	{ checkImpType(); }
#endif
	
	t__( RecordImp * imp, bool checkType = true )
	: b__( imp, false )
	{
		if( checkType ) checkImpType();
	}
	
	/*
	 * Returns a copy of this record, with the primary key
	 * set to 0.
	 */
	t__ copy() const { return t__( Record::copy() ); }

	t__ & operator=( const t__ & other );


<%METHODDEFS%>

	static t__List select( const QString & where = QString(), const VarList & args = VarList() );
	static t__List select( const Expression & exp );
	// Usage
	// Employee::join<UserGroup>().join<Group>().select()
	template<typename T> static JoinedSelect join( QString condition = QString(), JoinType joinType = InnerJoin, bool ignoreResults = false, const QString & alias = QString() )
	{ return table()->join( T::table(), condition, joinType, ignoreResults, alias ); }
	
<%INDEXDEFS%>

<%ELEMENTHACKS%>

	struct _c {
<%SCHEMAFIELDDECLS%>
	};
	static _c c;
	
	struct _i {
<%SCHEMAINDEXDECLS%>
	};
	static _i i;

#define CLASS_FUNCTIONS
<%BASEHEADER%>
#undef CLASS_FUNCTIONS

public:
	static Table * table();
	static TableSchema * schema();
private:
	void checkImpType();
};

#include "tl__list.h"

#endif // tu___H

