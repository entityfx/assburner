
#include <qvariant.h>

#include "snl__.h"
#include "field.h"

#include "strings.h"

#include "tl__.h"
#include "tl__table.h"
#include "tl__list.h"

#include "bl__table.h"

<%CLASSHEADERS%>
<%ELEMENTHEADERS%>

t__Schema::t__Schema( Schema * schema )
: TableSchema( schema, <%SCHEMA_ARGS%> )
{
}
