
#ifndef tu___LIST_H
#define tu___LIST_H

#include <qstring.h>
#include <qstringlist.h>
#include <qlist.h>

#include "snl__.h"
#include "interval.h"
#include "stringmap.h"
#include "bl__list.h"
#include "recordlist_p.h"
#include "tl__.h"

namespace Stone {
class Record;
class RecordImp;
class TableSchema;
};

using namespace Stone;

class t__;
class t__Iter;
<%CLASSDEFS%>

class snu___EXPORT t__Iter : public b__Iter
{
public:
	t__Iter( const RecordList & rl, bool end=false ) : b__Iter(rl,end) {}
	t__Iter( const ImpIter & ii ) : b__Iter(ii) {}
	t__Iter(const RecordIter & other) : b__Iter(other) {}
#ifdef Q_COMPILER_RVALUE_REFS
	t__Iter(RecordIter && other) : b__Iter(std::move(other)) {}
#endif

	t__ operator * () { return b__Iter::operator*(); }

	t__Iter & operator=(const t__Iter & o) { b__Iter::operator=(o); return *this; }
	t__Iter & operator=(const Record & r) { b__Iter::operator=(r); return *this; }

	friend class t__List;
};

class snu___EXPORT t__List : public b__List
{
public:
	t__List() : b__List() {}
	t__List( const t__List & other ) : b__List(other,schema()) {}
	t__List( const RecordList & rl, TableSchema * ts = 0 ) : b__List(rl,ts?ts:schema()) {}
#ifdef Q_COMPILER_RVALUE_REFS
	t__List( t__List && other ) : b__List(std::move(other)) {}
#endif
	t__List( const Record & r, TableSchema * ts = 0 ) : b__List(r,ts?ts:schema()) {}
	
	//virtual ~t__List(){}

	t__List & operator=( const RecordList & rl ) { b__List::operator=(rl); return *this; }
#ifdef Q_COMPILER_RVALUE_REFS
	t__List & operator=( RecordList && rl ) { b__List::operator=(std::move(rl)); checkDetach(schema()); return *this; }
#endif
	t__List & operator=( const t__List & rl ) { b__List::operator=(rl); return *this; }
#ifdef Q_COMPILER_RVALUE_REFS
	t__List & operator=( t__List && rl ) { b__List::operator=(std::move(rl)); return *this; }
#endif
	
	t__ operator []( uint i ) const { return b__List::operator[](i); }

	t__List & operator += ( const Record & r ) { b__List::operator+=(r); return *this; }
	t__List & operator += ( const RecordList & rl ) { b__List::operator+=(rl); return *this; }

	//void insert( t__Iter, const t__ & );

	t__Iter at( uint i ) { return b__List::at(i); }

	t__List slice( int start, int end = INT_MAX, int step = 1 ) { return b__List::slice(start,end,step); }
	
	t__Iter find( const Record & r ) { return b__List::find(r); }

	using b__List::remove;
	t__Iter remove( const t__Iter & iter ) { return b__List::remove(iter); }

	t__Iter begin() const { return b__List::begin(); }

	t__Iter end() const { return b__List::end(); }

	t__List filter( const QString & column, const QVariant & value, bool keepMatches = true ) const
		{ return t__List( RecordList::filter( column, value, keepMatches ) ); }
	t__List filter( const QString & column, const QRegExp & re, bool keepMatches = true ) const
		{ return t__List( RecordList::filter( column, re, keepMatches ) ); }
	t__List filter( const Expression & exp, bool keepMatches = true ) const
		{ return t__List( RecordList::filter( exp, keepMatches ) ); }

	t__List sorted( const QString & c, bool a = true ) const { return t__List( RecordList::sorted( c, a ) ); }
	t__List sorted( Field * f, bool a = true ) const { return t__List( RecordList::sorted( f, a ) ); }
	
		/// Returns a new list with all duplicate records removed.
	t__List unique() const { return t__List( RecordList::unique() ); }

	/// Returns a new list with the same contents as this, in reversed order.
	t__List reversed() const { return t__List( RecordList::reversed() ); }
	
	/// Returns a new list, with the contents of this list after calling
	/// Record::reload() on each record.
	t__List reloaded() const { return t__List( RecordList::reloaded() ); }

	virtual Table * table() const
	{
		return t__::table();
	}

	virtual TableSchema * schema() const
	{
		return t__::schema();
	}

<%LISTDEFS%>

	typedef t__Iter Iter;
	// for Qt foreach compat
	typedef t__Iter const_iterator;
	friend class t__Iter;
protected:
	t__List( const ImpList &, Table * t = 0 );

};

#endif // tu___LIST_H

