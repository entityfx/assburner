
#ifndef __su___H__
#define __su___H__

#include <qobject.h>

#ifdef su___MAKE_DLL
#define su___EXPORT Q_DECL_EXPORT
#else
#define su___EXPORT Q_DECL_IMPORT
#endif

namespace Stone {
	class Database;
	class Schema;
}

using namespace Stone;

/**
 * \defgroup Classes Classes - auto-generated classes to access database tables
 * \details Classes are defined using the app @ref classmaker
 * All fields defined have mutators, and indexes have static methods to retrieve a @ref RecordList of @ref stone objects.
 * Additional methods for a database class can be defined in the base/ directory.
 */
su___EXPORT void blurqt_loader();

su___EXPORT void createConnection();
su___EXPORT void initializeSchema();

su___EXPORT Schema * classesSchema();
su___EXPORT Database * blurDb();

#endif // __su___H
 
