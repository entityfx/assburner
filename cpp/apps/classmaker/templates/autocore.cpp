

#include "tl__.h"
#include "strings.h"

<%CLASSHEADERS%>
<%ELEMENTHEADERS%>
<%INDEXHEADERS%>

t__::t__()
: b__( t__::table()->emptyImp(), false )
{
}

t__::t__( uint key, int lookupMode )
: b__( table()->record( key, lookupMode ).mImp, false )
{
	if( !mImp )
		*this = t__();
}

void t__::checkImpType()
{
	Record::checkImpType( t__::schema() );
}

t__ & t__::operator=( const t__ & other ) {
	Record::operator=( other );
	checkImpType();
	return *this;
}

<%METHODS%>
<%INDEXMETHODS%>
<%ELEMENTMETHODS%>
t__List t__::select( const QString & where, const VarList & args )
{
	return table()->select( where, args );
}

t__List t__::select( const Expression & exp )
{
	return table()->select( exp );
}

Table * t__::table() {
	return t__::schema()->table();
}

TableSchema * t__::schema() {
	static TableSchema * sSchema = 0;
	if( !sSchema ) {
		sSchema = new TableSchema(snl__Schema(), <%SCHEMA_ARGS%>);
		<%SETPARENT%>
		sSchema->setUseCodeGen( true );
<%ADDFIELDS%><%ADDINDEXCOLUMNS%><%PRELOAD%>
#define TABLE_CTOR
<%BASEHEADER%>
#undef TABLE_CTOR
	}
	return sSchema;
}

struct t__::_c t__::c = {
	<%SCHEMAFIELDDEFS%>
};

struct t__::_i t__::i = {
	<%SCHEMAINDEXDEFS%>
};
