
TARGET=absubmit

include($$(PRI_SHARED)/common.pri)
include($$(PRI_SHARED)/python.pri)
include($$(PRI_SHARED)/classes.pri)
include($$(PRI_SHARED)/stone.pri)
include($$(PRI_SHARED)/absubmit.pri)

INCLUDEPATH += .out include

SOURCES += \
	src/main.cpp

win32:LIBS += -lWer

CONFIG += qt thread warn_on
QT += xml sql network

target.path=$$(PREFIX)/bin/
INSTALLS += target
