
from framewidget import FrameWidget
from servicewidget import ServiceWidget
from filewidget import FileWidget
from blur.Classes import *
from blur.Assfreezer import *
from blur.absubmit import *

class ABSubmitJobServiceBridge(JobServiceBridge):
	def __init__(self, submitCollection):
		JobServiceBridge.__init__(self)
		self.SubmitCollection = submitCollection
	
	def getJobServices( self, job ):
		Log( 'getJobServices' )
		ret = JobServiceList()
		for service in self.SubmitCollection.services(job):
			print 'Has service ' + service.service()
			ret += JobService().setService(service).setJob(job)
		return ret
		
	def removeJobServices( self, job, jobServices ):
		for js in jobServices:
			self.SubmitCollection.removeService(job,js.service())
	
	def applyJobServices( self, job, jobServices ):
		Log( 'applyJobServices' )
		currentServices = self.SubmitCollection.services(job)
		for js in jobServices:
			if not js.service() in currentServices:
				self.SubmitCollection.addService(job,js.service())

class BatchJobTypePlugin(JobTypePlugin):
	def __init__(self):
		JobTypePlugin.__init__(self)
		
	def jobType(self):
		return JobType.recordByName('Batch')
	
	def createJob(self,submitCollection):
		job = JobBatch().setName('New Batch Job').setPacketType('preassigned').setPriority(20).setJobType(self.jobType())
		submitCollection.addJob(job)
		submitCollection.addService(job,Service.recordByName('Assburner'))
		return job
	
	def submitPage( self, pageName, parent, submitCollection ):
		return SubmitPageFactory.instance().submitPage(pageName,parent,submitCollection)
	
	def submitPages(self):
		print 'Here'
		return ['Settings','Tasks','Services','Files']

class BuiltinSubmitPagePlugin(SubmitPagePlugin):
	def __init__(self):
		SubmitPagePlugin.__init__(self)
		self.SubmitCollectionByWidget = {}
		
	def submitPages(self):
		return ['Tasks','Files','Settings','Services']

	def submitPage(self, pageName, parent, submitCollection):
		print 'submitPage',pageName,parent,submitCollection
		if pageName == 'Tasks':
			return FrameWidget(parent,submitCollection)
		elif pageName == 'Files':
			return FileWidget(parent,submitCollection)
		elif pageName == 'Settings':
			jsw = JobSettingsWidget(parent,JobSettingsWidget.SubmitJobs)
			self.SubmitCollectionByWidget[jsw] = submitCollection
			jsw.customJobSettingsWidgetCreated.connect(self.customJobSettingsWidgetCreated)
			return jsw
		elif pageName == 'Services':
			return ServiceWidget(parent,submitCollection)
		
	def customJobSettingsWidgetCreated(self,cjsw):
		cjsw.setJobServiceBridge( ABSubmitJobServiceBridge(self.SubmitCollectionByWidget[cjsw.jobSettingsWidget()]) )

SubmitPageFactory.instance().registerPlugin( BuiltinSubmitPagePlugin() )
JobTypePluginFactory.instance().registerPlugin( BatchJobTypePlugin() )
