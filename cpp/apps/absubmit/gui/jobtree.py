
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from blur.Stone import *
from blur.Classes import *
from blur.Stonegui import *

class JobTranslator(RecordDataTranslator):
	def __init__(self,treeBuilder):
		RecordDataTranslator.__init__(self,treeBuilder)
		self.setRecordColumnList( QStringList('name') )
		self.setColumnEditable( 0, True )
		
	def recordData(self,job,index,role):
		if role == Qt.DisplayRole and index.column() == 1:
			return job.jobType().name()
		return RecordDataTranslator.recordData(self,job,index,role)

	def recordChildren(self,job,index):
		deps = self.model().SubmitCollection.deps(job)
		return deps.deps()
	
	def recordFlags(self,job,index):
		flags = RecordDataTranslator.recordFlags(self,job,index)
		if index.model().Mode == JobModel.DependencyMode:
			flags |= Qt.ItemIsDragEnabled | Qt.ItemIsDropEnabled
		return flags
	
class DependencyTreeBuilder(RecordTreeBuilder):
	def __init__(self,model):
		ModelTreeBuilder.__init__(self,model)
		self.Trans = JobTranslator(self)
		self.model().SubmitCollection.JobDepAddedSignal.connect(self.depAdded)
		self.model().SubmitCollection.JobDepRemovedSignal.connect(self.depRemoved)
		
	def depAdded(self,jobDep):
		# Remove from top-level if it's now a dependency
		for idx in self.model().findIndexes(RecordList(jobDep.dep())):
			if not idx.parent().isValid():
				self.model().remove(idx)
		for idx in self.model().findIndexes(RecordList(jobDep.job())):
			self.model().append( jobDep.dep(), idx )
		
	def depRemoved(self,jobDep):
		for parentIndex in self.model().findIndexes( RecordList(jobDep.job()) ):
			self.model().remove( jobDep.dep(), False, parentIndex )
		if self.model().SubmitCollection.deps( jobDep.dep() ).isEmpty():
			self.model().append( jobDep.dep() )
			
class ParentTreeBuilder(ModelTreeBuilder):
	def __init__(self,model):
		ModelTreeBuilder.__init__(self,model)
		self.Trans = JobTranslator(self)
	
	def hasChildren(self,parentIndex,model):
		return model.SubmitCollection.jobChildren(model.record(parentIndex)).size()
	
	def loadChildren(self,parentIndex,model):
		model.append( model.SubmitCollection.jobChildren(model.record(parentIndex)), parentIndex )

	def jobParentChanged(self,job,parent):
		for idx in self.model().findIndexes(RecordList(job)):
			if parent.isRecord() and idx.parent().isValid():
				self.model().remove(idx)
		self.model().append( job, self.model().findIndex(parent) )
		
class JobModel(RecordSuperModel):
	DependencyMode = 1
	ParentMode = 2
	
	DropSignal = pyqtSignal(Job)
	
	def __init__(self,mode,submitCollection,parent=None):
		RecordSuperModel.__init__(self,parent)
		self.setHeaderLabels( ['Job Name','Job Type'] )
		self.SubmitCollection = submitCollection
		self.Mode = mode
		self.TreeBuilder = {1:DependencyTreeBuilder,2:ParentTreeBuilder}[mode](self)
		self.setTreeBuilder(self.TreeBuilder)
		self.setRootList( submitCollection.jobs() )
		submitCollection.JobAddedSignal.connect( self.append )
		submitCollection.JobRemovedSignal.connect( self.jobRemoved )
	
	def jobRemoved(self,job):
		self.remove( RecordList(job) )

	def dropMimeData( self, mimeData, action, row, column, parentIndex ):
		if parentIndex.isValid():
			parent = Job(self.getRecord(parentIndex))
			if parent.isValid():
				self.DropSignal.emit(Job(parent))
				return True
		return RecordSuperModel.dropMimeData(self,mimeData,action,row,column,parentIndex);

class JobTreeView(RecordTreeView):
	def __init__(self,mode,submitCollection,parent):
		RecordTreeView.__init__(self,parent)
		self.SubmitCollection = submitCollection
		self.Mode = mode
		self.Model = JobModel(mode,submitCollection,self)
		self.setModel(self.Model)
		
		self.setSelectionMode(self.ExtendedSelection);
		
		if self.Mode == JobModel.DependencyMode:
			self.setDragEnabled(True)
			self.setAcceptDrops(True)
			self.setDropIndicatorShown(True)
			self.setRootIsDecorated(True)
			
		self.connect( self, SIGNAL( 'showMenu( const QPoint &, const Record &, RecordList )' ), self.slotShowMenu )
		
		self.ExpectingDrop = False
		self.Model.DropSignal.connect( self.dropSlot )
	
	def slotShowMenu(self,pos,underMouse,selection):
		menu = QMenu(self)
		removeAction = None
		addJobMenu = menu.addMenu( 'Add Job' )
		addJobMenu.triggered.connect( self.slotAddJobActionTriggered )
		for jobType in JobType.select().sorted('name'):
			addJobMenu.addAction( jobType.name() )
		if selection.size():
			removeAction = menu.addAction( 'Remove Selected Job(s)' )
		action = menu.exec_(pos)
		if action and action == removeAction:
			for job in selection:
				self.SubmitCollection.removeJob( job )
		
	def slotAddJobActionTriggered(self,action):
		job = self.SubmitCollection.createJob( JobType.recordByName( action.text() ) )
		if job:
			idx = self.findIndex(job)
			self.setSelection( RecordList(job) )
			if idx.isValid():
				self.edit(idx)
			
	def dropEvent( self, dropEvent ):
		if dropEvent.source() == self:
			self.ExpectingDrop = True
			RecordTreeView.dropEvent(self,dropEvent)
			self.ExpectingDrop = False
		
	def dropSlot( self, droppedOnJob ):
		if self.ExpectingDrop:
			dragged = self.selection()
			for job in dragged:
				jd = JobDep()
				jd.setJob( droppedOnJob )
				jd.setDep( job )
				self.SubmitCollection.addJobDep( jd )

