
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.uic import *
from blur.Stone import *
from blur.Classes import *
from blur.Classesui import *
from blur.Stonegui import *
from blur.Assfreezer import *

class FrameTranslator(RecordDataTranslator):
	def __init__(self,treeBuilder):
		RecordDataTranslator.__init__(self,treeBuilder)
		self.setRecordColumnList( ['frameNumber','host','status'] )
		self.setColumnEditable( 1, True )
		
	def recordData(self,job,index,role):
		return RecordDataTranslator.recordData(self,job,index,role)

class FrameModel(RecordSuperModel):
	def __init__(self,parent=None):
		RecordSuperModel.__init__(self,parent)
		self.FrameTranslator = FrameTranslator(self.treeBuilder())
		self.setHeaderLabels( ['Frame Number','Host','Status'] )

class FrameTree(RecordTreeView):
	def __init__(self,parent=None):
		RecordTreeView.__init__(self,parent)
		self.Model = FrameModel(self)
		self.setModel(self.Model)
		
		self.setSelectionMode(self.ExtendedSelection);
		
		self.connect( self, SIGNAL( 'showMenu( const QPoint &, const Record &, RecordList )' ), self.slotShowMenu )
	
	def setTasks(self,tasks):
		self.Model.setRootList(tasks)
		
	def slotShowMenu( self, pos, record, selection ):
		menu = QMenu(self)
		suspend = menu.addAction( "Set Suspended" )
		remove = menu.addAction( "Remove" )
		result = menu.exec_(pos)
		if result == suspend:
			JobTaskList(selection).setStatuses( 'suspended' )
			self.Model.updated(selection)
			pass
		if result == remove:
			pass
		pass

class FrameWidget(QWidget):
	def __init__(self,parent,submitCollection):
		QWidget.__init__(self,parent)
		loadUi('ui/framewidgetui.ui',self)
		self.FrameTree = FrameTree(self)
		self.Layout.addWidget(self.FrameTree)
		self.SubmitCollection = submitCollection
		self.CurrentJobs = JobList()
		self.setEnabled( False )
		
		self.mSetupFrameNthButton.clicked.connect( self.setupFrameNth )
		self.mSelectPreassignedHostsButton.clicked.connect( self.selectPreassignedHosts )
		self.mFrameListEdit.returnPressed.connect( self.slotFrameListEdited )
	
	def selectPreassignedHosts( self ):
		hosts = HostList()
		for task in self.FrameTree.model().rootList():
			if task.host().isRecord():
				hosts += task.host()
		services = ServiceList()
		for job in self.CurrentJobs:
			services += self.SubmitCollection.services( job )
		services = services.unique()
		hosts = hosts.unique();
		hs = HostSelector( self )
		hs.setHostList( hosts )
		hs.setServiceFilter( services )
		if hs.exec_() == QDialog.Accepted:
			hosts = hs.hostList()
			for job in self.CurrentJobs:
				i = 0
				tasks = JobTaskList()
				for host in hosts:
					task = JobTask()
					task.setJob( job )
					task.setStatus( 'new' )
					i += 1
					task.setFrameNumber( i )
					task.setHost( host )
					tasks += task
				job.setMinTaskNumber( 1 )
				job.setMaxTaskNumber( i )
				job.setFrameNth( 0 )
				job.setFrameNthMode( 0 )
				job.setPacketType( 'preassigned' )
				self.SubmitCollection.setTasks( job, tasks )
		self.updateFrameTree()
		
			
	def setSelectedJobs(self, jobs):
		self.CurrentJobs = jobs
		self.setEnabled( jobs.size() > 0 )
		preassignedCount = 0
		frameLists = []
		for job in self.CurrentJobs:
			if job.packetType() == 'preassigned':
				preassignedCount += 1
			tasks = self.SubmitCollection.tasks(job)
			frameList = compactNumberList( tasks.frameNumbers() )
			if not frameList in frameLists:
				frameLists.append(frameList)
		if preassignedCount > 0:
			if preassignedCount < jobs.size():
				self.mModeStack.setCurrentWidget( self.PacketTypeConflictPage )
			else:
				self.mModeStack.setCurrentWidget( self.mPreassignedPage )
		else:
			self.mModeStack.setCurrentWidget( self.mFrameListPage )
		self.updateFrameTree()
		print frameLists
		
	def updateFrameTree(self):
		tasks = JobTaskList()
		for job in self.CurrentJobs:
			tasks += self.SubmitCollection.tasks(job)
		self.FrameTree.setTasks( tasks )
	
	def updateFrameListEdit(self):
		taskNumbers = []
		for task in self.FrameTree.model().rootList():
			if not task.frameNumber() in taskNumbers:
				taskNumbers.append(task.frameNumber())
		self.mFrameListEdit.setText( compactNumberList( taskNumbers ) )
	
	def changeFrameList(self,newTaskNumbers,frameNth = 0,frameNthMode = 0):
		for job in self.CurrentJobs:
			tasks = self.SubmitCollection.tasks(job)
			taskNumbers = tasks.frameNumbers()
			
			# Remove unneeded tasks
			for task in JobTaskList(tasks):
				if not task.frameNumber() in newTaskNumbers:
					tasks.remove(task)
			
			minTask, maxTask = None, None
			# Add new tasks
			for taskNumber in newTaskNumbers:
				if minTask is None or taskNumber < minTask:
					minTask = taskNumber
				if maxTask is None or taskNumber > maxTask:
					maxTask = taskNumber
				if not taskNumber in taskNumbers:
					task = JobTask()
					task.setJob( job )
					task.setFrameNumber( taskNumber )
					task.setStatus( 'new' )
					tasks += task
			
			print 'Update tasks for job', job, ' to ', tasks
			job.setMinTaskNumber( minTask )
			job.setMaxTaskNumber( maxTask )
			job.setFrameNth( frameNth )
			job.setFrameNthMode( frameNthMode )
			self.SubmitCollection.setTasks( job, tasks )
	
	def slotFrameListEdited(self):
		# TODO - Validator
		newTaskNumbers = expandNumberList( self.mFrameListEdit.text() )[0]
		self.changeFrameList( newTaskNumbers )
		self.updateFrameTree()
	
	def setupFrameNth(self):
		if self.CurrentJobs.size():
			job = self.CurrentJobs[0]
			fnd = FrameNthDialog(self,job.minTaskNumber(), job.maxTaskNumber(), job.frameNth(), job.frameNthMode())
			result = fnd.exec_()
			if result == QDialog.Accepted:
				frameNthStart, frameNthEnd, frameNth, frameNthMode = fnd.getSettings()
				frames = []
				for i in range(frameNthStart,frameNthEnd):
					if ((i-frameNthStart) % frameNth) == 0:
						frames.append(i)
				self.changeFrameList(frames,frameNth,frameNthMode)
				self.updateFrameTree()
				self.updateFrameListEdit()
		