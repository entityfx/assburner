
from PyQt4.QtGui import *
from PyQt4.uic import *
from jobtree import *
from blur.Assfreezer import *
from blur.absubmit import *
from submitcollection import *
from framewidget import *
from servicewidget import *
from filewidget import *

class MainWindow(QMainWindow):
	class PageInfo:
		def __init__(self,page,name):
			self.Page = page
			self.Name = name
			self.Index = None
			
	def __init__(self,parent=None):
		QMainWindow.__init__(self,parent)
		loadUi('ui/mainwindowui.ui',self)
		self.Pages = []
		self.SubmitCollection = SubmitCollectionImp(self)
		print dir(self.SubmitCollection.JobAddedSignal)
		self.setupJobViews()
		self.JobSettingsTabWidget.currentChanged.connect(self.jobSettingsTabChanged)
		self.setupMenu()
		self.jobViewTabChanged(self.JobOverviewTabWidget.currentIndex())
		self.InSelectionChanged = False
		
	def setupJobViews(self):
		self.JobParentChildView = JobTreeView(JobModel.ParentMode,self.SubmitCollection,self.mJobParentChildPage)
		layout = QVBoxLayout(self.mJobParentChildPage)
		layout.setContentsMargins(2,2,2,2)
		layout.addWidget(self.JobParentChildView)
		
		self.JobDependencyView = JobTreeView(JobModel.DependencyMode,self.SubmitCollection,self.mJobDependencyPage)
		layout = QVBoxLayout(self.mJobDependencyPage)
		layout.setContentsMargins(2,2,2,2)
		layout.addWidget(self.JobDependencyView)
		
		self.JobOverviewTabWidget.currentChanged.connect( self.jobViewTabChanged )
		
	def createPage(self,pageName):
		page = SubmitPageFactory.instance().submitPage(pageName,None,self.SubmitCollection)
		if page:
			pageInfo = self.PageInfo(page,pageName)
			self.Pages.append( pageInfo )
			return pageInfo
		return None
	
	def showPage(self,pageInfo,index):
		print 'showPage',pageInfo.Name,index
		self.JobSettingsTabWidget.insertTab(index,pageInfo.Page,pageInfo.Name)
		pageInfo.Index = index
		
	def hidePage(self,pageInfo):
		if pageInfo.Index:
			self.JobSettingsTabWidget.remove( pageInfo.Index )
			pageInfo.Index = None
		
	def setupMenu(self):
		menuBar = self.menuBar()
		fileMenu = menuBar.addMenu( 'File' )
		fileMenu.addAction( 'New' ).triggered.connect( self.new )
		fileMenu.addAction( 'Save' ).triggered.connect( self.save )
		fileMenu.addAction( 'Load' ).triggered.connect( self.load )
		fileMenu.addAction( 'Merge' ).triggered.connect( self.merge )
		fileMenu.addAction( 'Submit' ).triggered.connect( self.SubmitCollection.submit )
		fileMenu.addAction( 'Quit' ).triggered.connect( QCoreApplication.instance().quit )
	
	def new(self):
		self.SubmitCollection.clear()
	
	def save( self ):
		ret = QFileDialog.getSaveFileName(self,'Save Submission', QString(), 'Saved Submission (*.absub)')
		if ret:
			if not ret.endsWith( '.absub' ):
				ret = ret + '.absub'
			self.SubmitCollection.saveToFile( ret )
		
	def load( self, spacer, clear = True ):
		print "Clear=",clear
		ret = QFileDialog.getOpenFileName(self,{True:'Open',False:'Merge'}[clear] + ' Saved Submission', QString(), 'Saved Submission (*.absub)')
		if ret:
			if clear:
				self.SubmitCollection.clear()
			self.SubmitCollection.loadFromFile( ret )
	
	def merge( self ):
		self.load( clear = False )
		
	def currentSettingsWidget(self):
		return self.JobSettingsTabWidget.currentWidget()
	
	def updateSettingsTab(self,jobs):
		assert(isinstance(jobs,RecordList))
		cur = self.currentSettingsWidget()
		if cur:
			cur.setSelectedJobs(JobList(jobs))
	
	def supportedPages(self,jobs):
		jobTypes = JobTypeList()
		for job in jobs:
			jobTypes.append( job.jobType() )
		jobTypes = jobTypes.unique()
		pages = None
		allPages = []
		for jobType in jobTypes:
			supportedPages = []
			plugin = JobTypePluginFactory.instance().plugin( jobType.name() )
			if plugin:
				supportedPages = plugin.submitPages()
				allPages += supportedPages
			if pages is None:
				pages = set(supportedPages)
			else:
				pages = set(supportedPages) & pages
		if pages is None:
			return []
		retPages = []
		for page in allPages:
			if page in pages:
				retPages.append(page)
		return retPages
	
	def findOrCreatePage(self,pageName):
		print 'findOrCreatePage', pageName
		for pageInfo in self.Pages:
			if pageInfo.Name == pageName:
				return pageInfo
		return self.createPage(pageName)
		
	def updatePages(self,jobs):
		pages = self.supportedPages(jobs)
		self.JobSettingsTabWidget.blockSignals(True)
		print 'Setting pages to', ','.join(pages)
		for pageInfo in self.Pages:
			if pageInfo.Name not in pages and pageInfo.Index:
				self.hidePage(pageInfo)
		i = 0
		for page in pages:
			pageInfo = self.findOrCreatePage(page)
			if pageInfo:
				if pageInfo.Index is None:
					self.showPage(pageInfo,i)
				i += 1
		self.JobSettingsTabWidget.blockSignals(False)
	
	def jobSelection(self):
		sel = self.CurrentJobTree.selection()
		if sel.isEmpty():
			sel = RecordList(self.CurrentJobTree.current())
		return sel
	
	def jobSelectionChanged(self,ignored=None):
		sel = self.jobSelection()
		self.updatePages(sel)
		self.updateSettingsTab(sel)
		
	def jobSettingsTabChanged(self,idx):
		self.updateSettingsTab(self.jobSelection())
		
	def jobViewTabChanged(self,idx):
		curPage = self.JobOverviewTabWidget.widget(idx)
		try:
			self.JobDependencyView.selectionChanged.disconnect( self.jobSelectionChanged )
		except: pass
		try:
			self.JobParentChildView.selectionChanged.disconnect( self.jobSelectionChanged )
		except: pass
		
		if curPage == self.mJobParentChildPage:
			self.CurrentJobTree = self.JobParentChildView
		else:
			self.CurrentJobTree = self.JobDependencyView
	
		self.CurrentJobTree.selectionChanged.connect( self.jobSelectionChanged )
		self.jobSelectionChanged()
		
	def customJobSettingsWidgetCreated(self,cjsw):
		cjsw.setJobServiceBridge( ABSubmitJobServiceBridge(self.SubmitCollection) )
