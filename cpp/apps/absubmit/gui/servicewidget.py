
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from blur.Stone import *
from blur.Classes import *
from blur.Stonegui import *
from blur.defaultdict import DefaultDict

class ServiceTranslator(RecordDataTranslator):
	def __init__(self,treeBuilder):
		RecordDataTranslator.__init__(self,treeBuilder)
		self.setRecordColumnList( ['service','description'] )
		
	def recordData(self,service,index,role):
		if index.column() == 0 and role == Qt.CheckStateRole:
			return index.model().checkState(service)
		return RecordDataTranslator.recordData(self,service,index,role)

	def setRecordData(self,service,index,value,role):
		if index.column() == 0 and role == Qt.CheckStateRole:
			index.model().setCheckState( service, value.toInt()[0] )
			return True
		return RecordDataTranslator.setRecordData(self,service,index,value,role)
	
	def recordFlags(self,service,index):
		flags = RecordDataTranslator.recordFlags(self,service,index)
		if index.column() == 0:
			flags |= Qt.ItemIsUserCheckable
			if self.model().isTristate(service):
				flags |= Qt.ItemIsTristate
		return flags
	
class ServiceModel(RecordSuperModel):
	def __init__(self,parent,submitCollection):
		RecordSuperModel.__init__(self,parent)
		self.setHeaderLabels( ['Service Name','Description'] )
		self.Trans = ServiceTranslator(self.treeBuilder())
		self.setRootList( Service.select() )
		self.ServiceStatusDict = DefaultDict(Qt.Unchecked)
		self.SubmitCollection = submitCollection
		self.CurrentJobs = JobList()
		
	def setSelectedJobs(self,jobs):
		self.CurrentJobs = jobs
		jobCountByService = DefaultDict(0)
		for job in jobs:
			for service in self.SubmitCollection.services(job):
				jobCountByService[service] += 1
		self.ServiceStatusDict = DefaultDict(Qt.Unchecked)
		for service, count in jobCountByService.iteritems():
			val = Qt.Checked
			if count == 0:
				val = Qt.Unchecked
			elif count < jobs.size():
				val = Qt.PartiallyChecked
			self.ServiceStatusDict[service] = val
	
	def isTristate(self,service):
		return self.ServiceStatusDict[service] == Qt.PartiallyChecked
	
	def checkState(self,service):
		return self.ServiceStatusDict[service]
	
	def setCheckState(self,service,checkState):
		self.ServiceStatusDict[service] = checkState
		for job in self.CurrentJobs:
			if checkState == Qt.Checked:
				self.SubmitCollection.addService(job,service)
			else:
				self.SubmitCollection.removeService(job,service)
		
class ServiceTree(RecordTreeView):
	def __init__(self,parent,submitCollection):
		RecordTreeView.__init__(self,parent)
		self.Model = ServiceModel(self,submitCollection)
		self.setModel(self.Model)
		
		self.setSelectionMode(self.ExtendedSelection);
		
		self.connect( self, SIGNAL( 'showMenu( const QPoint &, const Record &, RecordList )' ), self.slotShowMenu )
	
	def slotShowMenu( self, pos, record, selection ):
		menu = QMenu(self)
		suspend = menu.addAction( "Set Suspended" )
		remove = menu.addAction( "Remove" )
		result = menu.exec_(pos)
		if result == suspend:
			pass
		if result == remove:
			pass
		pass

class ServiceWidget(QWidget):
	def __init__(self,parent,submitCollection):
		QWidget.__init__(self,parent)
		self.ServiceTree = ServiceTree(self,submitCollection)
		self.Layout = QVBoxLayout(self)
		self.Layout.addWidget(self.ServiceTree)
		self.setEnabled( False )
		
		self.SubmitCollection = submitCollection
		
	def setSelectedJobs(self, jobs):
		self.setEnabled( jobs.size() > 0 )
		self.ServiceTree.model().setSelectedJobs( jobs )
		