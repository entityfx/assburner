
import sys
from PyQt4.QtCore import *
from PyQt4.QtXml import *
from blur.Stone import *
from blur.Classes import *
from blur.defaultdict import DefaultDict
from blur.absubmit import Submitter, JobTypePluginFactory
import blur.absubmit as absubmit

class RecordCopyRelations:
	def __init__(self):
		self.CopiedMap = {}
	
	def copy(self, records):
		isSingle = False
		_class = records.__class__
		if isinstance(records,Record):
			isSingle = True
			records = RecordList(records)
		ret = RecordList()
		for record in records:
			# Dont copy twice!
			copy = Record()
			if record.imp() in self.CopiedMap:
				copy = self.CopiedMap[record.imp()]
			else:
				copy = record.copy()
				self.CopiedMap[record.imp()] = copy
			if isSingle:
				return copy
			ret.append(copy)
		return _class(ret)
	
	def finalize(self):
		for record in self.CopiedMap.values():
			ts = record.table().schema()
			for field in ts.fields():
				if field.type() & Field.ForeignKey:
					fkey = record.foreignKey( field.pos() )
					if fkey.isValid() and fkey.imp() in self.CopiedMap:
						record.setForeignKey( field.pos(), self.CopiedMap[fkey.imp()] )

class JobSubmitData:
	def __init__(self):
		self.Job = None
		self.Tasks = JobTaskList()
		self.Services = ServiceList()
		self.ExtraData = {}
		self.Files = []
		self.Parent = Job()
		
class SubmitCollectionImp(absubmit.SubmitCollection):
	
	JobAddedSignal = pyqtSignal(Job)
	JobRemovedSignal = pyqtSignal(Job)
	JobDepAddedSignal = pyqtSignal(JobDep)
	JobDepRemovedSignal = pyqtSignal(JobDep)
	JobParentChangedSignal = pyqtSignal(Job,Job)
	
	def __init__(self,parent=None):
		absubmit.SubmitCollection.__init__(self,parent)
		self.JobDataList = []
		self.JobDepList = []
		
	def jobs(self):
		ret = JobList()
		for jsd in self.JobDataList:
			ret += jsd.Job
		return ret
	
	def jobData(self,job):
		for jsd in self.JobDataList:
			if jsd.Job == job:
				return jsd
		return JobSubmitData()
	
	def addJob(self,job):
		jsd = JobSubmitData()
		jsd.Job = job
		self.JobDataList.append(jsd)
		self.JobAddedSignal.emit(job)
		return job
	
	def removeJob(self,job):
		for jsd in self.JobDataList:
			if jsd.Job == job:
				self.JobDataList.remove( jsd )
				self.JobRemovedSignal.emit( job )
				break
		for dep in self.JobDepList[:]:
			if dep.job() == job or dep.dep() == job:
				self.JobDepList.remove(dep)
	
	def jobParent(self, job):
		return self.jobData(job).Parent
	
	def setJobParent(self, job, parent):
		if self.jobData(job).Parent != parent:
			self.jobData(job).Parent = parent
			self.JobParentChangedSignal.emit(job,parent)
				
	def jobChildren(self, parent):
		ret = JobList()
		for jd in self.JobDataList:
			if jd.Parent == parent:
				ret.append( jd.Job )
		return ret

	def tasks(self,job):
		return self.jobData(job).Tasks
	
	def setTasks(self,job,tasks):
		self.jobData(job).Tasks = tasks
		
	def services(self,job):
		return self.jobData(job).Services
	
	def addService(self,job,service):
		jd = self.jobData(job)
		if not service in jd.Services:
			jd.Services += service
	
	def setServices(self,job,services):
		jd = self.jobData(job)
		jd.Services = services
	
	def removeService(self,job,service):
		self.jobData(job).Services.remove(service)
		
	def files(self,job):
		return self.jobData(job).Files
	
	def setFiles(self,job,files):
		self.jobData(job).Files = files
	
	def createJob(self,jobType):
		if jobType.isRecord():
			job = None
			plugin = JobTypePluginFactory.instance().plugin( jobType.name() )
			if plugin:
				job = plugin.createJob(self)
				return job
			else:
				table = Database.current().tableByName( 'Job' + jobType.name() )
				if table:
					job = table.load()
					job.setJobType( jobType )
					job.setName( 'New Job' )
					self.addJob( job )
					return job
		return None
		
	def addJobDep(self,jobDep):
		self.JobDepList.append(jobDep)
		self.JobDepAddedSignal.emit(jobDep)

	def removeJobDep(self,jobDep):
		self.JobDepList.remove(jobDep)
		self.JobDepRemovedSignal.emit(jobDep)
	
	def deps(self, job):
		ret = JobDepList()
		for jd in self.JobDepList:
			if jd.job() == job:
				ret += jd
		return ret
	
	def jobsWithDep(self, job):
		ret = JobDepList()
		for jd in self.JobDepList:
			if jd.dep() == job:
				ret += jd
		return ret
		
	def clear(self):
		for job in self.jobs():
			self.removeJob(job)
	
	def saveToDom(self):
		records = RecordList()
		for job in self.jobs():
			records += job
			records += self.deps(job)
			for service in self.services(job):
				js = JobService()
				js.setJob(job)
				js.setService(service)
				records += js
			records += self.tasks(job)
		saver = RecordXmlSaver()
		nodes = saver.addRecords(records)
		doc = saver.document()
		for job,node in zip(self.jobs(),nodes):
			for file in self.files(job):
				fileNode = doc.createElement("File")
				node.appendChild(fileNode)
				fileNode.appendChild(doc.createTextNode(file))
		return saver.document()
	
	def loadFromDom(self, domDoc):
		loader = RecordXmlLoader(domDoc)
		records = loader.records()
		domMap = loader.recordElementMap()
		for job in JobList( records ):
			self.addJob(job)
			node = domMap[job]
			print node
			fileNodes = node.elementsByTagName('File')
			for i in range(fileNodes.count()):
				fileNode = fileNodes.at(i)
				fileName = fileNode.firstChild().toText().data()
				if fileName:
					print "Found filename:", fileName
					self.jobData(job).Files.append(fileName)
		
		for record in records:
			jt = JobTask(record)
			js = JobService(record)
			jd = JobDep(record)
			if jt.isValid():
				self.jobData(jt.job()).Tasks += jt
			elif js.isValid():
				self.jobData(js.job()).Services += js.service()
			elif jd.isValid():
				self.JobDepList.append(jd)
		
	def saveToFile(self,fileName):
		writeFullFile( fileName, self.saveToDom().toString() )
		
	def loadFromFile(self,fileName):
		doc = QDomDocument()
		_errorMessage = QString()
		errorLine, errorColumn = 0, 0
	
		file = QFile( fileName )
		if not file.open( QIODevice.ReadOnly ):
			Log( "Couldn't Open File (" + fileName + ")" )
			return False
		
		if not doc.setContent( file )[0]:
			Log( "Couldn't parse xml: line " + QString.number(errorLine) + " column " + QString.number(errorColumn) + " message " + _errorMessage )
			file.close()
			return False
		
		file.close();
		self.loadFromDom( doc )
	
	
	# Returns path to zip file in a temp dir
	def zipFiles(self,fileList):
		# Create the temp directory and copy all files there
		path = Path( QDir.tempPath() + QDir.separator() + "batch_temp" + QDir.separator() )

		if not path.mkdir():
			Log( "Couldn't create temp directory: " + path.path() )
			return None
		
		error = False
		for file in fileList:
			if not Path.copy( file, path.path() ):
				Log( "Failed to copy " + file + " to " + path.path() )
				error = True
				break
		
		# Remove the temp directory if we had any problems
		if error:
			path.remove()
			return None
		
		# Create the archive
		archivePath = QDir.tempPath() + QDir.separator() + "batcharchive.zip"
		
		p = QProcess()
		if sys.platform == 'win32':
			p.start( "c:/blur/zip.exe -j " + archivePath + " " + path.path() )
		else:
			p.start( "zip -jr " + archivePath + " " + path.path() )
			
		p.waitForFinished()
		if p.exitCode() != 0:
			Log( "Error creating archive, return value was " + QString.number( p.exitCode() ) + ", output was: " + QString.fromLatin1( p.readAllStandardOutput() ) );
			return None
			
		# Remove dir
		path.remove();
		
		if error:
			return None

		return archivePath
	
	def copy(self):
		ret = SubmitCollection(self.parent())
		rcr = RecordCopyRelations()
		for jd in self.JobDataList:
			job = rcr.copy(jd.Job)
			ret.addJob(job)
			ret.setJobParent(rcr.copy(jd.Parent))
			ret.setTasks(job,rcr.copy(self.tasks(jd.Job)))
			ret.setServices(job,self.services(jd.Job))
			for dep in self.deps(jd.Job):
				ret.addJobDep(rcr.copy(dep))
			ret.setFiles(job,self.files(jd.Job))
		rcr.finalize()
		return ret
			
			
	def submit(self,copyMade=False):
		# We don't submit/commit the actual records, but copies, so that
		# the records in the gui remain uncommitted, ready for future
		# commits
		if not copyMade:
			return self.copy().submit(True)
		
		submitters = []
		
		# Fetch a key for each job, so that jobdep commits work properly
		for jd in self.JobDataList:
			jd.Job.key(True)
		
		for jd in self.JobDataList:
			job = jd.Job
			
			# If the job has a parent, that means it will be submitted by the parent when it runs
			# We commit the job as a placeholder and the deps
			if jd.Parent:
				job.setStatus( 'submit' )
				job.commit()
				self.deps(job).commit()
			else:
				Log( 'Submitting job ' + job.name() )
				sub = Submitter(self)
				if jd.Files:
					fileName = None
					if len(jd.Files) > 1:
						fileName = self.zipFiles(jd.Files)
					else:
						fileName = jd.Files[0]
					job.setFileName( fileName )
				sub.setJob( job )
				sub.addTasks( self.tasks(job) )
				sub.addServices( self.services(job) )
				deps = self.deps(job)
				if deps.size():
					sub.addJobDeps( deps )
				sub.submit()
				submitters.append(sub)
		print 'Waiting for submit to complete'
		for sub in submitters:
			sub.waitForFinished()
		print 'Submit Completed'
	