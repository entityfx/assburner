
import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from blur.Stone import *
from blur.Classes import *
from blur.absubmit import *

from mainwindow import MainWindow

def main():
	if sys.platform == 'win32':
		os.chdir("c:\\blur\\absubmit\\gui\\")
	app = QApplication(sys.argv)
	initConfig("../absubmit.ini","absubmit_gui.log")
	if sys.platform == 'win32':
		cp = "h:/public/" + getUserName() + "/Blur";
		if not QDir( cp ).exists():
			cp = "C:/Documents and Settings/" + getUserName();
		initUserConfig( cp + "/absubmit_gui.ini" );
	else:
		initUserConfig( QDir.homePath() + "/.absubmit_gui" );

	initStone(sys.argv)
	blurqt_loader()
	
	mw = MainWindow()
	mw.show()
	return app.exec_()

if __name__ == '__main__':
	main()
