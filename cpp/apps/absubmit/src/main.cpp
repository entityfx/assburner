
#include <QCoreApplication>
#include <QFile>
#include <QRegExp>
#include <QFileInfo>
#include <qlibrary.h>
#include <qlist.h>
#include <qtimer.h>

#include "blurqt.h"
#include "database.h"
#include "freezercore.h"
#include "process.h"

#include "svnrev.h"
#include "submitter.h"

#ifdef Q_OS_WIN
#include "windows.h"
#include <winerror.h>
#include <direct.h>
#include <Werapi.h>
#endif

void printHelp()
{
	printf( "Options:\n" );
	printf( "Please visit the following URL for doco:\n" );
	printf( "\thttp://rover.deimos.org/mediawiki/index.php/Absubmit\n" );
	exit(0);
}

#ifdef Q_OS_WIN
void enableCrashDumps() {
	HKEY hkey;
	const char * path = "Software\\Microsoft\\Windows\\Windows Error Reporting\\LocalDumps\\absubmit.exe";
	if( RegCreateKeyExA( HKEY_LOCAL_MACHINE, path, 0, 0, 0, KEY_ALL_ACCESS | (isWow64() ? KEY_WOW64_64KEY : 0), 0, &hkey, 0 ) == ERROR_SUCCESS ) {
		DWORD val = 2;
		RegSetValueExA( hkey, "DumpType", 0, REG_DWORD, (const BYTE *)&val, sizeof(DWORD) );
		const char * abPath = "C:\\blur\\absubmit\\";
		RegSetValueExA( hkey, "DumpFolder", 0, REG_SZ, (const BYTE *)abPath, strlen(abPath) + 1 );
		RegCloseKey( hkey );
	} else
		LOG_3( "Unable to create/open registry key HKEY_LOCAL_MACHINE\\" + QString::fromLatin1(path) );
}
#endif

int main(int argc, char * argv[])
{
	QCoreApplication a(argc, argv);

#ifdef Q_OS_WIN
	WerRemoveExcludedApplication( L"absubmit.exe", true );
	enableCrashDumps();
	SetErrorMode( SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX );

	initConfig( "c:\\blur\\absubmit\\absubmit.ini", "C:\\blur\\absubmit\\absubmit.log" );
#else
	initConfig( "/etc/absubmit.ini", "/var/log/ab/absubmit.log" );
#endif // Q_OS_WIN
/*
#ifdef Q_OS_WIN
	QLibrary excdll( "exchndl.dll" );
	if( !excdll.load() )
		LOG_1( "Unable to load exchndl.dll, error was: " + excdll.errorString() );
#endif
*/

	QStringList argList = a.arguments();
	int argCount = argList.size();
	if( argCount == 1 || argList.contains("--help") || argList.contains("-h") )
		printHelp();

	LOG_1("AB Submit Version " + SVN_REVSTR + " starting");

	QMap<QString,QString> argMap;
	for( int i=1; i< argCount; i+=2 ) {
		if( i+1 >= argCount ) break;
		argMap[argList[i]] = argList[i+1];
		LOG_3( "Setting key " + argList[i] + " to " + argList[i+1] );
	}

	initStone( argc, argv );

	blurqt_loader();

	Connection * conn = blurDb()->connection();
	if( !conn->checkConnection() )
		return writeErrorFile( "Unable to connect to the database, Connection String Was: " + conn->connectString() + "  Error Was: " + conn->lastErrorText() );

	Submitter * submitter = new Submitter();
	submitter->applyArgs( argMap );
	submitter->setExitAppOnFinish( true );

	QTimer::singleShot( 0, submitter, SLOT( submit() ) );

	int result = a.exec();

	LOG_1("absubmit done. returning value '" + QString::number(result) + "' to caller");
	shutdown();

	return result;
}

