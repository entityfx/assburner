#
import sys
print sys.version
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.uic import *
from blur.Stone import *
from blur.Classes import *
from blur.Classesui import HostSelector
from blur.absubmit import Submitter
from blur.logredirect import RedirectOutputToLog
import os
import time
import re

class NukeRenderDialog(QDialog):
	def __init__(self,parent=None):
		QDialog.__init__(self,parent)
		loadUi("nukerenderdialogui.ui",self)
		self.mAutoPacketSizeCheck.toggled.connect(self.autoPacketSizeToggled)
		self.mChooseFileNameButton.clicked.connect(self.chooseFileName)
		self.mAllHostsCheck.toggled.connect(self.allHostsToggled)
		self.mWriteAllCheck.toggled.connect(self.allWritesToggled)
		self.mHostListButton.clicked.connect(self.showHostSelector)
		self.layout().setSizeConstraint(QLayout.SetFixedSize);
		self.mProjectCombo.setSpecialItemText( 'None' )
		self.mProjectCombo.setStatusFilters( ProjectStatusList(ProjectStatus.recordByName( 'Production' )) )
		self.OutputPath = None
		self.HostList = ''
		self.Service = 'Nuke9'
		self.returnStatus = 1
		self.loadSettings()

	def initFields(self, nukeScript, startFrame, endFrame, writers):
		self.treeWidget.clear()
		invalid = False
		for w in writers:
			item = QTreeWidgetItem()
			item.setText(0, unicode(w['name']))
			item.setText(1, unicode(w['path']))
			self.treeWidget.addTopLevelItem(item)
			if not w['valid']:
				item.setDisabled(True)
				invalid = True
			elif w['selected']:
				item.setSelected(True)

		if invalid:
			QMessageBox.warning(self, 'Invalid Outputs', 'One or more Write nodes has an output path or filename that is invalid.  These Write nodes will not be available for submission.' )

		self.mFileNameEdit.setText(nukeScript)
		self.mJobNameEdit.setText(os.path.basename(nukeScript))
		self.mFrameStartEdit.setText(startFrame)
		self.mFrameEndEdit.setText(endFrame)

	def loadSettings(self):
		c = userConfig()
		c.pushSection( "LastSettings" )
		project = Project.recordByName( c.readString( "Project" ) )
		if project.isRecord():
			self.mProjectCombo.setProject( project )
		aps = c.readBool( "AutoPacketSize", True )
		self.mAutoPacketSizeCheck.setChecked( aps )
		if not aps:
			self.mPacketSizeSpin.setValue( c.readInt( "PacketSize", 10 ) )
		self.mFileNameEdit.setText( c.readString( "FileName" ) )
		self.mFrameStartEdit.setText( c.readString( "FrameList" ) )
		self.mJabberErrorsCheck.setChecked( c.readBool( "JabberErrors", False ) )
		self.mJabberCompletionCheck.setChecked( c.readBool( "JabberCompletion", False ) )
		self.mEmailErrorsCheck.setChecked( c.readBool( "EmailErrors", False ) )
		self.mEmailCompletionCheck.setChecked( c.readBool( "EmailCompletion", False ) )
		self.mPrioritySpin.setValue( c.readInt( "Priority", 10 ) )
		self.mDeleteOnCompleteCheck.setChecked( c.readBool( "DeleteOnComplete", False ) )
		self.mSubmitSuspendedCheck.setChecked( c.readBool( "SubmitSuspended", False ) )
		c.popSection()

	def saveSettings(self):
		c = userConfig()
		c.pushSection( "LastSettings" )
		c.writeString( "Project", self.mProjectCombo.project().name() )
		c.writeBool( "AutoPacketSize", self.mAutoPacketSizeCheck.isChecked() )
		c.writeInt( "PacketSize", self.mPacketSizeSpin.value() )
		c.writeString( "FileName", self.mFileNameEdit.text() )
		c.writeString( "FrameList", self.mFrameStartEdit.text() )
		c.writeBool( "JabberErrors", self.mJabberErrorsCheck.isChecked() )
		c.writeBool( "JabberCompletion", self.mJabberCompletionCheck.isChecked() )
		c.writeBool( "EmailErrors", self.mEmailErrorsCheck.isChecked() )
		c.writeBool( "EmailCompletion", self.mEmailCompletionCheck.isChecked() )
		c.writeInt( "Priority", self.mPrioritySpin.value() )
		c.writeBool( "DeleteOnComplete", self.mDeleteOnCompleteCheck.isChecked() )
		c.writeBool( "SubmitSuspended", self.mSubmitSuspendedCheck.isChecked() )
		c.popSection()

	def autoPacketSizeToggled(self,autoPacketSize):
		self.mPacketSizeSpin.setEnabled(not autoPacketSize)

	def allHostsToggled(self,allHosts):
		self.mHostListButton.setEnabled( not allHosts )

	def allWritesToggled(self,allWrites):
		self.treeWidget.setEnabled( not allWrites )

	def showHostSelector(self):
		hs = HostSelector(self)
		hs.setServiceFilter( ServiceList(Service.recordByName(self.Service)) )
		hs.setHostList( self.HostList )
		if hs.exec_() == QDialog.Accepted:
			self.HostList = hs.hostStringList()
		del hs

	def chooseFileName(self):
		fileName = QFileDialog.getOpenFileName(self,'Choose Scene To Render', QString(), 'Nuke Scene (*.nk)' )
		if not fileName.isEmpty():
			self.mFileNameEdit.setText(fileName)
		
	def checkFrameList(self):
		(frames, valid) = expandNumberList( self.mFrameStartEdit.text() )
		return valid

	def packetSize(self):
		if self.mAutoPacketSizeCheck.isChecked():
			return 0
		return self.mPacketSizeSpin.value()

	def buildNotifyString(self,jabber,email):
		ret = ''
		if jabber or email:
			ret = getUserName() + ':'
			if jabber:
				ret += 'j'
			if email:
				ret += 'e'
		return ret

	# Returns tuple (notifyOnErrorString,notifyOnCompleteString)
	def buildNotifyStrings(self):
		return (
			self.buildNotifyString(self.mJabberErrorsCheck.isChecked(), self.mEmailErrorsCheck.isChecked() ),
			self.buildNotifyString(self.mJabberCompletionCheck.isChecked(), self.mEmailCompletionCheck.isChecked() ) )

	def buildSubmitArgs(self):
		sl = {}
		sl['job'] = self.mJobNameEdit.text()
		sl['jobType'] = "Nuke"
		#sl['noCopy'] = 'true'
		sl['packetType'] = 'continuous'
		sl['priority'] = str(self.mPrioritySpin.value())
		sl['user'] = getUserName()
		sl['packetSize'] = str(self.packetSize())
		sl['fileName'] = self.mFileNameEdit.text()
		sl['frameList'] = self.mFrameStartEdit.text() + "-" + self.mFrameEndEdit.text()
		notifyError, notifyComplete = self.buildNotifyStrings()
		sl['notifyOnError'] = notifyError
		sl['notifyOnComplete'] = notifyComplete
		sl['deleteOnComplete'] = str(int(self.mDeleteOnCompleteCheck.isChecked()))
		if self.mProjectCombo.project().isRecord():
			sl['projectName'] = self.mProjectCombo.project().name()
		if not self.mAllHostsCheck.isChecked() and len(self.HostList):
			sl['hostList'] = str(self.HostList)

		sl['services'] = self.Service
		if self.mSubmitSuspendedCheck.isChecked():
			sl['submitSuspended'] = '1'
		Log("Applying Absubmit args: %s" % str(sl))
		return sl

	def accept(self):
		if self.mJobNameEdit.text().isEmpty():
			QMessageBox.critical(self, 'Missing Job Name', 'You must choose a name for this job' )
			return

		if not QFile.exists( self.mFileNameEdit.text() ):
			QMessageBox.critical(self, 'Invalid File', 'You must choose an existing Nuke scene' )
			return

		if not self.checkFrameList():
			QMessageBox.critical(self, 'Invalid Frame List', 'Frame Lists are comma separated lists of either "XXX", or "XXX-YYY"' )
			return

		self.saveSettings()
		
		# Disable ui to prevent accidental multiple submission
		self.setEnabled(False)
		self.update()
		
		# give the ui time to disable before submitting
		QTimer.singleShot(10, self.submit())

	def submit(self):
		jobArgs = self.buildSubmitArgs()
		env = {}

		if self.mWriteAllCheck.isChecked():
			selectedItems = [i for i in self.treeWidget.findItems("*", Qt.MatchWildcard)
				if not i.isDisabled()]
		else:
			selectedItems = [i for i in self.treeWidget.selectedItems() if not i.isDisabled()]
		nSelected = len(selectedItems)

		# no outputs selected
		if not nSelected:
			QMessageBox.critical(self, 'No Outputs Selected', 'You must specify at least one output to render.')
			return
		
		# single output selected
		if nSelected == 1:
			# save write node's name in env, and path in outputPath
			env['NUKE_RENDERABLE'] = selectedItems[0].text(0)
			jobArgs['outputPath'] = selectedItems[0].text(1)
		
		# multiple outputs selected
		else:
			env['NUKE_RENDERABLE'] = ''
			for (i, out) in enumerate(selectedItems):
				# Write node output path
				jobArgs['outputPath'+str(i)] = out.text(1)
				# Write node name
				env['NUKE_RENDERABLE'] += ',' + str(out.text(0))

			# drop the preceeding comma
			env['NUKE_RENDERABLE'] = env['NUKE_RENDERABLE'][1:]

		# format and append to args the custom environment
		jobArgs['environment'] = ";".join(
			["{key}={val}".format(key=key, val=val) for \
					key, val in env.items()]
		)
		submitter = Submitter(self)
		self.connect(submitter, SIGNAL('submitSuccess()'), self.submitSuccess)
		self.connect(submitter, SIGNAL('submitError( const QString & )'), self.submitError)
		submitter.applyArgs(jobArgs)
		submitter.submit()

	def submitSuccess(self):
		Log( 'Submission Finished Successfully' )
		self.returnStatus = 0
		QDialog.accept(self)

	def submitError(self,errorMsg):
		QMessageBox.critical(self, 'Submission Failed', 'Submission Failed With Error: ' + errorMsg)
		Log( 'Submission Failed With Error: ' + errorMsg )
		QDialog.reject(self)
#
print("nuke2ABSubmit()")
app = QApplication(sys.argv)
print("nuke2ABSubmit(): initialize db connection")

os.chdir(os.path.abspath(os.path.dirname(__file__)))
initConfig( 'C:/blur/absubmit/absubmit.ini', 'nukesubmit.log' )
initStone(sys.argv)
RedirectOutputToLog()

if sys.platform == 'win32':
	cp = 'h:/public/' + getUserName() + '/Blur'
	if not QDir( cp ).exists():
		cp = 'C:/Documents and Settings/' + getUserName()
else:
	cp = '~'
initUserConfig( cp + "/fusionsubmit.ini" );

blurqt_loader()
args = sys.argv
thisScript = args.pop(0)
nukeScript = args.pop(0)
firstFrame = args.pop(0)
lastFrame = args.pop(0)

writers = []
i = 0
for (j,v) in enumerate(args):
	switch = j%4
	if switch == 0: # name
		writers.append({})
		writers[i]['name'] = v
	elif switch == 1: # path
		writers[i]['path'] = v
	elif switch == 2: # view
		writers[i]['view'] = v
	else: # selected
		writers[i]['selected'] = (v == "True")
		i += 1

nFrames = int(lastFrame) - int(firstFrame) + 1
padding = max(len(str(nFrames)), 4)
dPadding = '%{0}d'.format(str(padding).zfill(2))
hPadding = '#' * padding
pPattern = re.compile(r'.*(?:(?:'+dPadding+')|'+hPadding+')\.[a-zA-Z0-9]+')

createAllDir = False
skipAllDir = False

for w in writers:
	path = w['path']
	w['valid'] = True

	# check path exists
	dirname = os.path.dirname(path)
	
	# if not, we'll prompt the user and create missing directories
	if not os.path.exists(dirname):
		createDir = False
		# If the user hasn't clicked yes to all or no to all
		if (not createAllDir) and (not skipAllDir):
			r = QMessageBox.question(None, 'Create Directory?',
					'The path specified by {0} does not exist.'.format(w['name']) +
					'\n\nCreate directory {0} ?'.format(dirname),
					QMessageBox.Yes|QMessageBox.No|QMessageBox.YesToAll|QMessageBox.NoToAll
				)

			if r == QMessageBox.Yes or r == QMessageBox.YesToAll:
				createDir = True
			
			if r == QMessageBox.YesToAll:
				createAllDir = True
			elif r == QMessageBox.NoToAll:
				skipAllDir = True

		if createAllDir or createDir:
			os.makedirs(dirname)
		else:
			w['valid'] = False

	# replace view
	path = path.replace('%V', w['view'])
	
	# check formatting
	# TODO check output path here to ensure correct destination (?)
	w['valid'] &= (pPattern.match(path) != None)

	# replace padding (assuming it follows correct formatting)
	path = path.replace(dPadding, '').replace(hPadding, '')
	w['path'] = path

dialog = NukeRenderDialog()
dialog.initFields(nukeScript, firstFrame, lastFrame, writers)
dialog.show()
app.exec_()
import sys
sys.exit(dialog.returnStatus)
