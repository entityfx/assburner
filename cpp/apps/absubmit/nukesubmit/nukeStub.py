
import sys
import time
import os
import subprocess
import nuke
import nukescripts
import subprocess

def launchNuke2ABSubmit():
	print("nukeStub(): saving file")
	try:
		nuke.scriptSave()
	except:
		nuke.message("nuke.scriptSave raised an exception.  Continuing with submission anyway.")

	writeNodes = [i for i in nuke.allNodes() if i.Class() == "Write" and
		not i.knob('disable').value()]

	# If we're in Nuke Assist we will never have an active
	# writer since it isn't allowed to write frames to disk,
	# we should not block submission in that case.  In all
	# other cases (Nuke, NukeX) we should require an active
	# writer.
	if len(writeNodes) < 1 and not nuke.env['assist']:
		print("nukeStub(): No Write Nodes")
		nuke.message("Please enable at least one write node.")
		return

	print("nukeStub(): launch submitter dialog")
	if sys.platform == 'win32':
		cwd = os.getcwd()
		submitCmd = r'cd C:/ && C:/python27_64/python.exe "C:/Program Files/Nuke9.0v3/plugins/nukesubmit/nuke2AB.py"'
		os.chdir(cwd)
	else:
		try:
			abSubPath=os.environ["ABSUBMIT"]
		except:
			# hardcoding a path if no environment var is found. 
			os.environ["ABSUBMIT"]="/opt/ab/bin"
			abSubPath=os.environ["ABSUBMIT"]
		submitCmd = "LD_LIBRARY_PATH="+os.environ["ABSUBMIT"]+"/../lib"
		if os.uname()[0] == "Darwin":
			submitCmd += " DYLD_LIBRARY_PATH="+os.environ["ABSUBMIT"]+"/../lib"
		submitCmd += " env python "+os.environ["ABSUBMIT"]+"/nukesubmit/nuke2AB.py"

	# root.name holds the path to the nuke script
	submitCmd += " %s" % nuke.value("root.name")
	submitCmd += " %s" % nuke.Root.firstFrame(nuke.root())
	submitCmd += " %s" % nuke.Root.lastFrame(nuke.root())
	
	for i in writeNodes:
		if ' ' in i.knob('views').value():
			nuke.message("Unable to render multiple views per Write Node.")
			return

		submitCmd += " {name} {path} {view} {selected}".format(
				name=i.name(),
				path=i.knob('file').value(),
				view=i.knob('views').value(),
				selected=i.isSelected()
			)

	print( "nukeStub(): %s" % submitCmd )
	if os.system(submitCmd) == 0:
		nuke.message("Submission successful.")
	else:
		nuke.message("Submission failed.")

