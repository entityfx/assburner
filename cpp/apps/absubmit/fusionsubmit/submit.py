#!/usr/bin/python

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.uic import *
from blur.Stone import *
from blur.Classes import *
from blur.Classesui import HostSelector
from blur.absubmit import Submitter
from blur.logredirect import RedirectOutputToLog
import sys
import time
import os.path
import random
import re

class FusionRenderDialog(QDialog):
	def __init__(self,parent=None):
		QDialog.__init__(self,parent)
		loadUi("fusionrenderdialogui.ui",self)
		self.connect( self.mAutoPacketSizeCheck, SIGNAL('toggled(bool)'), self.autoPacketSizeToggled )
		self.connect( self.mChooseFileNameButton, SIGNAL('clicked()'), self.chooseFileName )
		self.connect( self.mAllHostsCheck, SIGNAL('toggled(bool)'), self.allHostsToggled )
		self.connect( self.mHostListButton, SIGNAL('clicked()'), self.showHostSelector )
		self.mFrameNthButton.clicked.connect(self.setupFrameNth)
		self.layout().setSizeConstraint(QLayout.SetFixedSize);
		self.mProjectCombo.setSpecialItemText( 'None' )
		self.mProjectCombo.setStatusFilters( ProjectStatus.select( "projectstatus IN ('Production','Bid','Pre-Production')" ) )
		self.mFusionVersionCombo.addItems( self.getFusionVersions().services() )
		self.OutputPath = None
		self.HostList = ''
		self.Services = []
		self.Deps = None
		self.FrameNth = None
		self.FrameWidth = None
		self.FrameHeight = None
		self.loadSettings()
		
	def loadSettings(self):
		c = userConfig()
		c.pushSection( "LastSettings" )
		project = Project.recordByName( c.readString( "Project" ) )
		if project.isRecord():
			self.mProjectCombo.setProject( project )
		aps = c.readBool( "AutoPacketSize", True )
		self.mAutoPacketSizeCheck.setChecked( aps )
		if not aps:
			self.mPacketSizeSpin.setValue( c.readInt( "PacketSize", 10 ) )
		self.mFileNameEdit.setText( c.readString( "FileName" ) )
		self.mFrameListEdit.setText( c.readString( "FrameList" ) )
		self.mSequentialRadio.setChecked( c.readString( "PacketType", "random" ) == "sequential" )
		self.mJabberErrorsCheck.setChecked( c.readBool( "JabberErrors", False ) )
		self.mJabberCompletionCheck.setChecked( c.readBool( "JabberCompletion", False ) )
		self.mEmailErrorsCheck.setChecked( c.readBool( "EmailErrors", False ) )
		self.mEmailCompletionCheck.setChecked( c.readBool( "EmailCompletion", False ) )
		
		# Priority always starts at 10, per SA request. <jbee>
		self.mPrioritySpin.setValue(10)
		
		self.mDeleteOnCompleteCheck.setChecked( c.readBool( "DeleteOnComplete", False ) )
		self.mSubmitSuspendedCheck.setChecked( c.readBool( "SubmitSuspended", False ) )
		c.popSection()
		
	def saveSettings(self):
		c = userConfig()
		c.pushSection( "LastSettings" )
		c.writeString( "Project", self.mProjectCombo.project().name() )
		c.writeBool( "AutoPacketSize", self.mAutoPacketSizeCheck.isChecked() )
		c.writeInt( "PacketSize", self.mPacketSizeSpin.value() )
		c.writeString( "FileName", self.mFileNameEdit.text() )
		c.writeString( "FrameList", self.mFrameListEdit.text() )
		c.writeString( "PacketType", {True : "sequential", False: "random"}[self.mSequentialRadio.isChecked()] )
		c.writeBool( "JabberErrors", self.mJabberErrorsCheck.isChecked() )
		c.writeBool( "JabberCompletion", self.mJabberCompletionCheck.isChecked() )
		c.writeBool( "EmailErrors", self.mEmailErrorsCheck.isChecked() )
		c.writeBool( "EmailCompletion", self.mEmailCompletionCheck.isChecked() )
		c.writeBool( "DeleteOnComplete", self.mDeleteOnCompleteCheck.isChecked() )
		c.writeBool( "SubmitSuspended", self.mSubmitSuspendedCheck.isChecked() )
		c.popSection()
	
	def getFusionVersions(self):
		return Service.select("service ~ E'^Fusion\\\\d'")
	
	def autoPacketSizeToggled(self,autoPacketSize):
		self.mPacketSizeSpin.setEnabled(not autoPacketSize)
	
	def allHostsToggled(self,allHosts):
		self.mHostListButton.setEnabled( not allHosts )
	
	def showHostSelector(self):
		hs = HostSelector(self)
		hs.setServiceFilter( ServiceList(self.getFusionService()) )
		hs.setHostList( self.HostList )
		if hs.exec_() == QDialog.Accepted:
			self.HostList = hs.hostStringList()
		del hs
	
	def setupFrameNth(self):
		from framenthdialog import FrameNthDialog
		self.FrameNth = FrameNthDialog.setupFrameNth(self,self.FrameNth,self.mFrameListEdit)
		
	def chooseFileName(self):
		fileName = QFileDialog.getOpenFileName(self,'Choose Flow To Render', QString(), 'Fusion Flows (*.comp)' )
		if not fileName.isEmpty():
			self.mFileNameEdit.setText(fileName)
		
	def checkFrameList(self):
		if self.FrameNth:
			return True
		(frames, valid) = expandNumberList( self.mFrameListEdit.text() )
		return valid
	
	def packetTypeString(self):
		if self.mSequentialRadio.isChecked():
			return 'sequential'
		return 'random'
	
	def packetSize(self):
		if self.mAutoPacketSizeCheck.isChecked():
			return 0
		return self.mPacketSizeSpin.value()
	
	def buildNotifyString(self,jabber,email):
		ret = ''
		if jabber or email:
			ret = getUserName() + ':'
			if jabber:
				ret += 'j'
			if email:
				ret += 'e'
		return ret
	
	# Returns tuple (notifyOnErrorString,notifyOnCompleteString)
	def buildNotifyStrings(self):
		return (
			self.buildNotifyString(self.mJabberErrorsCheck.isChecked(), self.mEmailErrorsCheck.isChecked() ),
			self.buildNotifyString(self.mJabberCompletionCheck.isChecked(), self.mEmailCompletionCheck.isChecked() ) )
	
	def checkOutputPaths(self):
		missingPaths = QStringList()
		for i in range(0,self.mOutputPathCombo.count()):
			path = self.mOutputPathCombo.itemText(i)
			dir = QFileInfo(path).dir()
			if not dir.exists():
				missingPaths.append(dir.path())
		if missingPaths:
			mb = QMessageBox(self)
			mb.setWindowTitle( "Missing Output Paths" )
			mb.setText( "The following output paths are missing:\n" + missingPaths.join("\n") )
			acceptButton = mb.addButton( "Create Paths And Submit", QMessageBox.AcceptRole )
			cancelButton = mb.addButton( "Abort Submission", QMessageBox.RejectRole )
			mb.setDefaultButton( acceptButton )
			mb.exec_()
			if mb.clickedButton() == acceptButton:
				# Attempt to create the paths
				for path in missingPaths:
					if not QDir().mkpath( path ):
						QMessageBox.critical( self, "Unable to create output path", "Submission has been aborted because the following output path could not be created:\n" + path )
						return False
			else:
				# Abort Submission
				return False
		return True
	
	def buildAbsubmitArgs(self):
		sl = {}
		sl['jobType'] = 'Fusion'
		sl['packetType'] = self.packetTypeString()
		sl['priority'] = str(self.mPrioritySpin.value())
		sl['user'] = getUserName()
		sl['packetSize'] = str(self.packetSize())
		# Gui in minutes, field in seconds
		sl['maxtasktime'] = str(self.mMaxTaskSpin.value() * 60)
		sl['maxloadtime'] = str(self.mMaxLoadSpin.value() * 60)
		if self.mAllFramesAsSingleTaskCheck.isChecked():
			sl['allframesassingletask'] = 'true'
			sl['frameList'] = str('1')
		else:
			if self.FrameNth:
				sl['frameStart'], sl['frameEnd'], sl['frameNth'], sl['frameFill'] = map( str, self.FrameNth )
			else:
				sl['frameList'] = self.mFrameListEdit.text()
		sl['fileName'] = self.mFileNameEdit.text()
		notifyError, notifyComplete = self.buildNotifyStrings()
		sl['notifyOnError'] = notifyError
		sl['notifyOnComplete'] = notifyComplete
		sl['job'] = self.mJobNameEdit.text()
		sl['deleteOnComplete'] = str(int(self.mDeleteOnCompleteCheck.isChecked()))
		if self.mProjectCombo.project().isRecord():
			sl['projectName'] = self.mProjectCombo.project().name()
		if not self.mOutputPathCombo.currentText().isEmpty():
			sl['outputPath'] = str(self.mOutputPathCombo.currentText())
		if not self.mAllHostsCheck.isChecked() and len(self.HostList):
			sl['hostList'] = str(self.HostList)
		sl['outputCount'] = str(self.mOutputPathCombo.count())
		self.Version = str(self.mFusionVersionCombo.currentText())
		if self.Version:
			fusionService = self.getFusionService()
			if fusionService.isRecord():
				self.Services.append( str(fusionService.service()) )
		if len(self.Services):
			sl['services'] = ','.join(self.Services)
		if self.mSubmitSuspendedCheck.isChecked():
			sl['submitSuspended'] = '1'
		if self.Deps:
			sl['deps'] = self.Deps
		if self.FrameWidth:
			sl['frameWidth'] = str(self.FrameWidth)
		if self.FrameHeight:
			sl['frameHeight'] = str(self.FrameHeight)
		Log("Applying Absubmit args: %s" % str(sl))
		return sl
	
	def setFusionVersionFromString(self,versionStr,platform='x86'):
		# Cut off to Major.Minor version, ex. 5.21 -> 5.2
		version = '5.3'
		if versionStr.startswith( 'Fusion' ):
			version = versionStr[6:]
		else:
			version = versionStr
		if len(version) > 3:
			version = version[:3]
		serviceName = 'Fusion' + version
		if (platform == 'IA64' or platform == 'X64') and version != '5.3':
			serviceName += 'x64'
		service = Service.recordByName( serviceName )
		if service.isRecord():
			self.setFusionService(service)
		else:
			Log( "Unable to find service for fusion version: " + serviceName )
			
	def setFusionService(self,service):
		idx = self.mFusionVersionCombo.findText(service.service())
		if idx >= 0:
			self.mFusionVersionCombo.setCurrentIndex(idx)
		else:
			Log( "No combo box entry for service " + service.service() )
		
	def getFusionService(self):
		return Service.recordByName( self.mFusionVersionCombo.currentText() )
	
	def accept(self):
		if self.mJobNameEdit.text().isEmpty():
			QMessageBox.critical(self, 'Missing Job Name', 'You must choose a name for this job' )
			return
		
		if not QFile.exists( self.mFileNameEdit.text() ):
			QMessageBox.critical(self, 'Invalid File', 'You must choose an existing fusion flow' )
			return
		
		if not self.checkFrameList():
			QMessageBox.critical(self, 'Invalid Frame List', 'Frame Lists are comma separated lists of either "XXX", or "XXX-YYY"' )
			return
		
		if not self.checkOutputPaths():
			return
		
		self.setEnabled( False )
		self.update()
		QTimer.singleShot( 10, self.submit )
		
	def submit(self):
		self.saveSettings()

		if self.mDeleteFramesBeforeSubmitCheck.isChecked():
			for loop in (range(self.mOutputPathCombo.count())):
				tFileName = str(self.mOutputPathCombo.itemText(loop))
				tFileDelete = os.path.dirname(tFileName) + "/*" + os.path.splitext(os.path.basename(tFileName))[1]
				self.__specialDeleteMsg(tFileDelete)
			
		submitter = Submitter(self)
		submitter.applyArgs( self.buildAbsubmitArgs() )

		if self.mBuildProxiesGroup.isChecked():
			submitter.submit()
			if submitter.waitForFinished() == submitter.Error:
				self.submitError(submitter.errorText())
			else:
				self.submitProxyBuilder(submitter.job().key())
				self.submitSuccess()
		else:
			self.connect( submitter, SIGNAL( 'submitSuccess()' ), self.submitSuccess )
			self.connect( submitter, SIGNAL( 'submitError( const QString & )' ), self.submitError )
			submitter.submit()
	
	def submitSuccess(self):
		Log( 'Submission Finished Successfully' )
		QDialog.accept(self)
		
	def submitError(self,errorMsg):
		QMessageBox.critical(self, 'Submission Failed', 'Submission Failed With Error: ' + errorMsg)
		Log( 'Submission Failed With Error: ' + errorMsg )
		QDialog.reject(self)

	def submitProxyBuilder(self, dependId):
		runLossy = self.mLossyProxyCheck.isChecked()
		runLossless = self.mLosslessProxyCheck.isChecked()
		if not runLossy and not runLossless:
			return
		outputs = [str(self.mOutputPathCombo.itemText(i)) for i in range(self.mOutputPathCombo.count())]
		mediaFiles = []
		# If the output path exists as is, use it.  If it does not then
		# we can assume it's missing a frame number, so we'll attach the
		# first frame number of the frameList.  If it doesn't exist and
		# it DOES appear to have a frame number, we will assume that the
		# comp will produce the frame and it will exist when this job
		# runs.
		for output in outputs:
			if os.path.exists(output):
				mediaFiles.append(output)
			else:
				base, ext = os.path.splitext(output)
				# Fusion allows for #### definition of how much fill the
				# output file path contains for frame numbers.  We don't
				# care about that, so we ditch it and treat it as if there
				# was no frame number provided.
				base = re.sub(r'[#]+$', '', base)
				if re.search(r'\d+$', base):
					mediaFiles.append(output)
				else:
					frames, valid = expandNumberList(self.mFrameListEdit.text())
					if valid == True:
						firstFrame = frames[0]
					else:
						continue
					mediaFiles.append(
						'{0}{1}{2}'.format(
							base,
							str(firstFrame).zfill(4),
							ext,
						)
					)
		if mediaFiles:
			useMediaFiles = []
			alreadyFound = []
			# We want the png of each output if there is one, otherwise
			# fall back on jpg output.  We only want one file type for
			# each base output, so if they're running out png AND jpg
			# with the same file basename, we take the png and skip the
			# jpg.
			searchOrder = ['.png','.jpg']
			for searchExt in searchOrder:
				for mediaFile in mediaFiles:
					base, ext = os.path.splitext(mediaFile)
					if ext.lower() == searchExt:
						if base not in alreadyFound:
							alreadyFound.append(base)
							useMediaFiles.append(mediaFile)
			import blurdev
			from blur3d.lib.legacy.LibProxyBuilder import LibProxyBuilder
			# Get the project fps.
			pq = ProjectResolution.c.Project == Project.recordByName(self.mProjectCombo.currentText())
			pq &= ProjectResolution.c.PrimaryOutput == True
			pq = pq.limit(1)
			prl = pq.select()
			if prl.isEmpty():
				fps = 30.0
			else:
				if prl[0].isRecord():
					fps = prl[0].fps()
				else:
					fps = 30.0
			proxyTypes = []
			if runLossy:
				proxyTypes.append('lossy')
			if runLossless:
				proxyTypes.append('lossless')
			for proxyType in proxyTypes:
				for mediaFile in useMediaFiles:
					lpb = LibProxyBuilder()
					lpb.mediaFile = mediaFile
					lpb.fps = fps
					lpb.runType = 'imageSeq'
					lpb.proxyType = proxyType
					lpb.submitBuildProxyToFarm(
						projectName=str(self.mProjectCombo.currentText()),
						dependIds=[dependId],
					)

#================================================================================================

	def __sendMessage ( self, msgPath , msg ):
		
		# we use a random number and username to create an unique file
		msgFileNamePath = msgPath # TBR: hard coded path!! what happens with offsite people!
		msgFileNameFile = getUserName() + str( random.randint(0, 65535) )
		msgFileNameExtTemp = ".tmp"
		msgFileNameExt = ".msg"
		
		msgFile = file (msgFileNamePath + msgFileNameFile + msgFileNameExtTemp, "w") 

		if msgFile:
			msgFile.write ( msg + "\n" )
			msgFile.close()
			
			os.rename( (msgFileNamePath + msgFileNameFile + msgFileNameExtTemp) , (msgFileNamePath + msgFileNameFile + msgFileNameExt) )
			
			return True
		
		return False

	def __specialDeleteMsg ( self, inPathToDelete ):
		#----------------------------------------------------------------------------
		#	Prototype:
		#				function syncPCDirectory ( inPathToDelete ):
		#
		#	Remarks:
		#				syncs pc directories
		#	Parameters:
		#				<string> inPathToDelete 
		#	Returns:
		#				<bool> true if the message to IT could be sent, false otherwise
		#----------------------------------------------------------------------------
		
		msgPath = '//thor/spool/new/'
		#pathToDelete = blurFile.ConvertToUNC ( inPathToDelete )
		pathToDelete = inPathToDelete.replace("G:", "//thor/animation").replace("Q:", "//cougar/compOutput").replace("S:", "//cheetah/renderOutput").replace("U:", "//goat/renderOutput")
		msg = "     {                   \n\
				action => rm,		\n\
				data => 						\n\
				{								\n\
					dir =>  '%s',		    	\n\
					verbose => 1				\n\
				}, 								\n\
				info => { user => '%s' }	   	\n\
			}"
		localName = "fusion-job"
		if sys.platform == 'win32':
			localName = getUserName()
		msg = msg % ( pathToDelete , localName)
		return self.__sendMessage ( msgPath , msg )
	
if __name__ == "__main__":
	os.chdir(os.path.abspath(os.path.dirname(__file__)))
	app = QApplication(sys.argv)
	initConfig("../absubmit.ini","fusionsubmit.log")
	initStone(sys.argv)
	RedirectOutputToLog()
	cp = 'h:/public/' + getUserName() + '/Blur'
	if not QDir( cp ).exists():
		cp = 'C:/Documents and Settings/' + getUserName()
	initUserConfig( cp + "/fusionsubmit.ini" );
	blurqt_loader()
	dialog = FusionRenderDialog()
	platform = 'x86'
	version = 'Fusion5.3'
	haveJobName = False
	Log( "Parsing args: " + ','.join(sys.argv) )
	for i, key in enumerate(sys.argv[1::2]):
		val = sys.argv[(i+1)*2]
		if key == 'fileName':
			dialog.mFileNameEdit.setText(val)
			if not haveJobName:
				dialog.mJobNameEdit.setText(QFileInfo(val).completeBaseName())
			path = Path(val)
			if path.level() >= 1:
				p = Project.recordByName( path[1] )
				if p.isRecord():
					dialog.mProjectCombo.setProject( p )
		elif key == 'jobName':
			dialog.mJobNameEdit.setText(val)
			haveJobName = True
		elif key == 'frameList':
			dialog.mFrameListEdit.setText(val)
		elif key == 'services':
			dialog.Services += val.split(',')
		elif key == 'outputs':
			dialog.mOutputPathCombo.addItems( QString(val).split(',') )
		elif key == 'version':
			version = val
		elif key == 'platform':
			platform = val
		elif key == 'deps':
			dialog.Deps = val
		elif key == 'frameWidth':
			dialog.FrameWidth = val
		elif key == 'frameHeight':
			dialog.FrameHeight = val
		elif key == 'defaultProxyOff':
			dialog.mBuildProxiesGroup.setChecked(False)
	dialog.setFusionVersionFromString(version,platform)
	ret = dialog.exec_()
	shutdown()
	sys.exit( ret )
