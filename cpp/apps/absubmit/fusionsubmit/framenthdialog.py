
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.uic import *
from blur.Stone import expandNumberList

class FrameNthDialog(QDialog):
	def __init__(self,parent=None):
		QDialog.__init__(self,parent)
		loadUi("framenthdialogui.ui",self)
		
	@staticmethod
	def setupFrameNth(parent, frameNth, frameListEdit):
		dialog = FrameNthDialog(parent)
		start, end, nth, fill = 0, 0, 2, False
		if frameNth is not None:
			start, end, nth, fill = frameNth
		elif frameListEdit:
			frameListEdit.RestoreText = frameListEdit.text()
			frames,valid = expandNumberList(frameListEdit.text())
			if valid:
				start = frames[0]
				end = frames[-1]
		dialog.mFrameStartSpin.setValue(start)
		dialog.mFrameEndSpin.setValue(end)
		dialog.mFrameNthSpin.setValue(nth)
		
		if dialog.exec_() == QDialog.Accepted:
			start = dialog.mFrameStartSpin.value()
			end = dialog.mFrameEndSpin.value()
			nth = dialog.mFrameNthSpin.value()
			fill = { Qt.Checked : 1, Qt.Unchecked : 0 }[dialog.mFrameFillCheck.checkState()]
			frameNth = (start, end, nth, fill)
			if frameListEdit:
				frameListEdit.setText( "Nth frame from %i to %i by %i" % frameNth[0:3] )
				frameListEdit.setReadOnly(True)
			return frameNth
		if frameListEdit:
			if hasattr(frameListEdit,'RestoreText'):
				frameListEdit.setText(frameListEdit.RestoreText)
			frameListEdit.setReadOnly(False)
		return None
		
		
if __name__ == "__main__":
	app = QApplication([])
	print FrameNthDialog.setupFrameNth(None,None,None)
	