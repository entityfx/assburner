#!/usr/bin/python

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.uic import *
from blur.Stone import *
from blur.Classes import *
from blur.Classesui import HostSelector
from blur.absubmit import Submitter
import sys, os, traceback

def toWinPath(aePath):
	if len(aePath) >= 3:
		return aePath[1] + ":" + aePath[2:]
	return aePath

class AfterEffectsSubmitDialog(QDialog):
	def __init__(self,parent=None):
		QDialog.__init__(self,parent)
		loadUi("aftereffectssubmitdialogui.ui",self)
		self.connect( self.mAutoPacketSizeCheck, SIGNAL('toggled(bool)'), self.autoPacketSizeToggled )
		self.connect( self.mAllHostsCheck, SIGNAL('toggled(bool)'), self.allHostsToggled )
		self.connect( self.mHostListButton, SIGNAL('clicked()'), self.showHostSelector )
		self.mProjectCombo.setSpecialItemText( 'None' )
		self.mProjectCombo.setStatusFilters( ProjectStatusList(ProjectStatus.recordByName( 'Production' )) )
		self.OutputPath = None
		self.HostList = ''
		self.readOptionsFile()
		self.Submitters = []
		self.mPrioritySpin.setValue(10)

	def readOptionsFile(self):
		try:
			file = open("current_options.txt","r")
			lines = file.read().split('\n')

			# Get Version in format 7.0x244, chop the x244 part
			self.Version = lines[0]
			self.Version = self.Version[0:self.Version.index('x')]
			if self.Version.count('.') > 1:
				# 10.0.2 -> 10.0
				self.Version = self.Version[:self.Version.rfind('.')]

			# Get filename and convert from AE style /c/path/ to c:/path/
			fileName = toWinPath(lines[1])
			if not QFile(fileName).exists():
				QMessageBox.warning( self, 'File not found', 'The file supplied by the After Effects submission script was not found.' )
				Log( "AE File not found: " + fileName )
				self.close()
				return

			self.mFileNameEdit.setText( fileName )
			self.mJobNameEdit.setText(QFileInfo(fileName).completeBaseName())
			path = Path(fileName)
			if path.level() >= 1:
				p = Project.recordByName( path[1] )
				if p.isRecord():
					self.mProjectCombo.setProject( p )

			compsByName = {}
			# Parse the passes
			for line in lines[2:]:
				if line.count('; ') == 3:
					(compName, frameStart, frameEnd, outputPath) = line.split('; ')
					path = toWinPath(outputPath)
					if compName in compsByName:
						twi = compsByName[compName]
					else:
						twi = QTreeWidgetItem( self.mPassTree, QStringList() << compName << str(int(float(frameStart))) << str(int(float(frameEnd))) )
						twi.setFlags( Qt.ItemIsUserCheckable | Qt.ItemIsEditable | Qt.ItemIsEnabled )
						twi.setCheckState( 0, Qt.Checked )
						compsByName[compName] = twi
					outputItem = QTreeWidgetItem( twi, QStringList() << path )
					outputItem.setFirstColumnSpanned( True )
					twi.setExpanded(True)
			self.mPassTree.setColumnWidth(0,300)
		except:
			traceback.print_exc()
			QMessageBox.critical( self, "Missing Required Information", "There was an error gathering required information for job submission.  Please contact your System Admin." )
			QTimer.singleShot( 0, self, SLOT('close()') )

	def getJobTypeName(self):
		return 'AfterEffects10'

	def getJobType(self):
		return JobType.recordByName(self.getJobTypeName())

	def getService(self):
		return "After_Effects_CC_2014"
		# return self.getJobType().service()

	def autoPacketSizeToggled(self,autoPacketSize):
		self.mPacketSizeSpin.setEnabled(not autoPacketSize)

	def allHostsToggled(self,allHosts):
		self.mHostListButton.setEnabled( not allHosts )

	def showHostSelector(self):
		hs = HostSelector(self)
		hs.setServiceFilter( ServiceList(self.getService()) )
		hs.setHostList( self.HostList )
		if hs.exec_() == QDialog.Accepted:
			self.HostList = hs.hostStringList()
		del hs

	def packetSize(self):
		if self.mAutoPacketSizeCheck.isChecked():
			return 0
		return self.mPacketSizeSpin.value()

	def buildNotifyString(self,jabber,email):
		ret = ''
		if jabber or email:
			ret = getUserName() + ':'
			if jabber:
				ret += 'j'
			if email:
				ret += 'e'
		return ret

	# Returns tuple (notifyOnErrorString,notifyOnCompleteString)
	def buildNotifyStrings(self):
		return (
			self.buildNotifyString(self.mJabberErrorsCheck.isChecked(), self.mEmailErrorsCheck.isChecked() ),
			self.buildNotifyString(self.mJabberCompletionCheck.isChecked(), self.mEmailCompletionCheck.isChecked() ) )

	def absubmitPath(self):
		return 'c:/blur/absubmit/absubmit.exe'

	def setupSubmitter(self,submitter,compItem):
		sl = {}
		sl['jobType'] = self.getJobTypeName()
		#sl['noCopy'] = 'true'
		sl['packetType'] = 'continuous'
		sl['priority'] = str(self.mPrioritySpin.value())
		sl['user'] = getUserName()
		sl['packetSize'] = str(self.packetSize())
		sl['frameList'] = (compItem.text(1) + "-" + compItem.text(2))
		sl['fileName'] = self.mFileNameEdit.text()
		notifyError, notifyComplete = self.buildNotifyStrings()
		sl['notifyOnError'] = notifyError
		sl['notifyOnComplete'] = notifyComplete
		sl['job'] = (self.mJobNameEdit.text() + "_" + compItem.text(0))
		sl['deleteOnComplete'] = str(int(self.mDeleteOnCompleteCheck.isChecked()))
		sl['comp'] = compItem.text(0)
		if self.mProjectCombo.project().isRecord():
			sl['projectName'] = self.mProjectCombo.project().name()
		sl['outputPath'] = compItem.child(0).text(0).replace(QRegExp('\\[#+\\]'),'')
		if not self.mAllHostsCheck.isChecked() and len(self.HostList):
			sl['hostList'] = str(self.HostList)
		if self.mSubmitSuspendedCheck.isChecked():
			sl['submitSuspended'] = 'true'
		submitter.applyArgs( sl )
		for i in range(compItem.childCount()):
			submitter.addJobOutput( compItem.child(i).text(0).replace(QRegExp('\\[#+\\]'),'') )
		return sl

	def accept(self):
		if self.mJobNameEdit.text().isEmpty():
			QMessageBox.critical(self, 'Missing Job Name', 'You must choose a name for this job' )
			return

		cmd = self.absubmitPath()
		for i in range(self.mPassTree.topLevelItemCount()):
			item = self.mPassTree.topLevelItem(i)
			if( item.checkState(0) == Qt.Checked ):
				Log("Preparing submitter for pass %s" % item.text(0))
				submitter = Submitter(self)
				self.connect( submitter, SIGNAL( 'submitSuccess()' ), self.submitSuccess )
				self.connect( submitter, SIGNAL( 'submitError( const QString & )' ), self.submitError )
				self.setupSubmitter(submitter,item)
				self.Submitters.append(submitter)
		self.submitNext()

	def submitNext(self):
		if not self.Submitters:
			QDialog.accept(self)
			return

		self.CurrentSubmitter = self.Submitters.pop()
		self.CurrentSubmitter.submit()

	def submitSuccess(self):
		Log( 'Submission of %s completed successfully' % self.CurrentSubmitter.job().name() )
		self.submitNext()

	def submitError(self,errorMsg):
		QMessageBox.critical(self, 'Submission Failed', 'Submission Failed With Error: ' + errorMsg)
		Log( 'Submission Failed With Error: ' + errorMsg )
		QDialog.reject(self)

if __name__ == "__main__":
	os.chdir(os.path.abspath(os.path.dirname(__file__)))
	app = QApplication(sys.argv)
	initConfig("../absubmit.ini","aftereffects.log")
	blurqt_loader()
	dialog = AfterEffectsSubmitDialog()
	dialog.show()
	app.exec_()
