
function isSecurityPrefSet()
{
	var securitySetting = app.preferences.getPrefAsLong("Main Pref Section",
					"Pref_SCRIPTING_FILE_NETWORK_SECURITY");
	return (securitySetting == 1);
}

function runAbsubmitScript()
{
	var submitScript = new File( "C:/blur/absubmit/aftereffectssubmit/submit.py" );
	submitScript.execute();
}

function writeRenderPasses( optionsFile )
{
	for(i=1; i<=app.project.renderQueue.numItems; ++i ) {
		var rqi = app.project.renderQueue.item(i);
		if( rqi instanceof RenderQueueItem ) {
			if(rqi.status == RQItemStatus.QUEUED ) { // statuses are QUEUED UNQUEUED NEEDS_OUTPUT WILL_CONTINUE
				rqi.logType = LogType.ERRORS_AND_PER_FRAME_INFO;
				var comp = rqi.comp;
				var timeStart = (rqi.timeSpanStart + comp.displayStartTime) * comp.frameRate;
				var timeEnd = (rqi.timeSpanStart + rqi.timeSpanDuration + comp.displayStartTime) * comp.frameRate;
				timeStart = Math.floor( timeStart + app.project.displayStartFrame );
				timeEnd = Math.floor( timeEnd + app.project.displayStartFrame );
				for(v=1; v <= rqi.numOutputModules; ++v ) {
					var output = rqi.outputModules[v];
					var outputFile = output.file;
					optionsFile.writeln(comp.name + "; " + timeStart + "; " + timeEnd + "; " + File.decode(outputFile.fullName));
					//alert( comp.name + " frameStart: " + timeStart + " frameEnd: " + timeEnd + " output path: " + File.decode(outputFile.fullName) );
				}
			}
		}
	}
}

function submitJob()
{
	var confirmation = confirm("Your current file will be saved! Continue?");
	if (!confirmation) {
		return;
	}
	var optionsFilePath = "C:/blur/absubmit/aftereffectssubmit/current_options.txt";
	var optionsFile = new File( optionsFilePath );
	if( optionsFile.open("w") ) {
		optionsFile.writeln(app.version);
		optionsFile.writeln(app.project.file);
		writeRenderPasses( optionsFile );
		optionsFile.close();
		app.project.save();
		runAbsubmitScript();
	} else {
		alert( "Unable to open options file for writing at: " + optionsFilePath );
	}
}

if( isSecurityPrefSet() ) {
	submitJob();
} else {
	alert( "Assburner Submission requires ability to write to files.  Please enable the option for scripts to write to files and network in the Main Preferences" );
}





