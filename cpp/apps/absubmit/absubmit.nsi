
; DESCRIPTION: Assburner Submit installer script
; (C) Blur Studio 2005

!include absubmit-svnrev.nsi
!define MUI_PRODUCT "Assburner Submit Silent Installer"
!define MUI_VERSION "v1.3.${MUI_SVNREV}"

Name "${MUI_PRODUCT} ${MUI_VERSION} ${PLATFORM}"

#!include "MUI.nsh"

; Name of resulting executable installer
!define OUT_FILE "absubmit_install_${MUI_SVNREV}_${PLATFORM}.exe"
OutFile "${OUT_FILE}"

InstallDir "C:\\blur\\absubmit\\"

!include "..\..\..\nsis\SoftwareINI.nsh"

#!define MUI_FINISHPAGE
#!define MUI_FINISHPAGE_NOREBOOTSUPPORT
#!define MUI_HEADERBITMAP "assburner.bmp"

; The icon for the installer title bar and .exe file
;!define MUI_ICON "myapp.ico"
; I want to use my product logo in the installer
;!define MUI_HEADERIMAGE
; Here is my product logo
;!define MUI_HEADERIMAGE_BITMAP "myapp.bmp"
; I want the logo to be on the right side of the installer, not the left
;!define MUI_HEADERIMAGE_RIGHT
; I've written a function that I want to be called when the user cancels the installation
;!define MUI_CUSTOMFUNCTION_ABORT myOnAbort
; Override the text on the Finish Page
#!define MUI_FINISHPAGE_TITLE "Assburner Installation Complete"
#!define MUI_FINISHPAGE_TEXT "\n\r\nClick Finish now to close the installation program."
#SilentInstall silent
#!insertmacro MUI_PAGE_INSTFILES ; File installation page

Section "install"
	RMDir /r "$INSTDIR\*.*"
	RMDir "$INSTDIR"
    SetOutPath $INSTDIR
	File absubmit.exe
	File absubmit.ini
	File absubmit_version.txt
	File ..\..\lib\stone\stone.dll
	File ..\..\lib\classes\classes.dll
	File ..\..\lib\absubmit\absubmit.dll
	File ..\..\lib\qjson\qjson4.dll
	File /r aftereffectssubmit
	File /r fusionsubmit
	File /r fusionvideomakersubmit
	File /r cinema4d
	File /r realflow
	File /r gui
	File /r nukesubmit
	SetOutPath "C:\\blur\\"
	File ..\..\..\binaries\zip.exe

	IfFileExists "C:\\Program Files\\Side Effects Software\\Houdini 12.5.469\\houdini\\otls\\*.*" HoudiniExists PastHoudini
	HoudiniExists:
		SetOutPath "C:\\Program Files\\Side Effects Software\\Houdini 12.5.469\\houdini\\otls\\"
		File houdini\OPcustom.otl
	PastHoudini:

	IfFileExists "C:\\Program Files\\Nuke7.0v8\\plugins\\*.*" NukeExists PastNuke
	NukeExists:
		SetOutPath "C:\\Program Files\\Nuke7.0v8\\plugins\\"
		File nukesubmit\menu.py
		File /r nukesubmit
	PastNuke:

	IfFileExists "C:\\Maxon\\Cinema 4D R10\\plugins\\*.*" Cinema4DExists PastCinema4D
	Cinema4DExists:
		SetOutPath "C:\\Maxon\\Cinema 4D R10\\plugins\\"
		File cinema4d\blur_render_submit.cof
	PastCinema4D:

	IfFileExists "C:\\Program Files\\Adobe\\Adobe After Effects CC\\Support Files\\*.*" AfterEffectsCCExists PastAfterEffectsCCExists
	AfterEffectsCCExists:
		SetOutPath "C:\\Program Files\\Adobe\\Adobe After Effects CC\\Support Files\\Scripts\\"
		File "aftereffectssubmit\Assburner Submit.jsx"
	PastAfterEffectsCCExists:

	# Register the uninstaler only if a cmd line arg was not passed in
	ClearErrors
	${GetParameters} $R1
	${GetOptions} $R1 "/noUninstallerReg" $R2
	IfErrors 0 makeInstaller
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayName" "${MUI_PRODUCT} (remove only)"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "UninstallString" "$INSTDIR\uninstall.exe"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "Publisher" "Blur Studio"
		WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}" "DisplayVersion" "${MUI_VERSION}"
	makeInstaller:
		WriteUninstaller "$INSTDIR\uninstall.exe"
	
	# Update software.ini so we can track what software is installed on each system
	!insertmacro UpdateSettingsINI ${BLUR_SOFTWARE_INI} "Absubmit" "${MUI_VERSION}" "${OUT_FILE}"
SectionEnd

Section "Uninstall"
	;Delete Files and directory
	RMDir /r "$INSTDIR\*.*"
	RMDir "$INSTDIR"

	;Delete Uninstaller And Unistall Registry Entries
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\${MUI_PRODUCT}"
	DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${MUI_PRODUCT}"  
SectionEnd

