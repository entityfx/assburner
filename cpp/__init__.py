
import os

if not 'PRI_SHARED' in os.environ:
	os.environ['PRI_SHARED'] = os.path.join(os.path.dirname(os.path.abspath(__file__)),'qmake_shared')

from . import lib
from . import apps
from . import docs
