mkdir snapshot
rsync --delete --exclude=*.o --exclude=ffmpeg --exclude=ImageMagick* --exclude=.out --exclude=*pyc --exclude=*.so* --exclude=.svn --exclude=trax --exclude=headshots --exclude=autocore --exclude=**cpp/plugins** --exclude=**apps/dist** --exclude=**apps/activex** -rtv ./cpp snapshot/
rsync -rtv --delete --exclude=trax --exclude=.svn --exclude=*pyc ./python snapshot/
rsync -rtv --delete --exclude=trax --exclude=.svn ./binaries snapshot/
rsync -rtv --delete --exclude=trax --exclude=.svn ./sql snapshot/
rsync -rtv --delete --exclude=trax --exclude=.svn COPYING snapshot/
rsync -rtv --delete --exclude=trax --exclude=.svn build.py snapshot/

tar -cvf snapshot.tar snapshot/
gzip snapshot.tar
